<?php
switch ($_SERVER['SERVER_ADDR']) {
    case '127.0.0.1':

        error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE | E_STRICT | E_DEPRECATED);
        error_reporting(E_ALL ^ E_NOTICE);
        ini_set('display_errors', 1);

        defined('YII_DEBUG') or define('YII_DEBUG', true);
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

        $yii = dirname(__FILE__) . '/framework/yii.php';
        $config = dirname(__FILE__) . '/protected/config/development_front.php';
        break;

    case 'eukanuba1':

        error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE | E_STRICT | E_DEPRECATED);
        error_reporting(E_ALL ^ E_NOTICE);
        ini_set('display_errors', 1);

        defined('YII_DEBUG') or define('YII_DEBUG', true);
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

        $yii = dirname(__FILE__) . '/framework/yii.php';
        $config = dirname(__FILE__) . '/protected/config/development_front.php';
        break;

    default:

        defined('YII_DEBUG') or define('YII_DEBUG', false);
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

        $yii = dirname(__FILE__) . '/framework/yii.php';
        $config = dirname(__FILE__) . '/protected/config/production_front.php';
        break;
}

require_once($yii);
Yii::createWebApplication($config)->runEnd('front');
