<?php

class BackEndController extends Controller
{
    public $layout = '//layouts/main';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('*'),
                'actions' => array('login', 'install'),
            ),
            array('allow',
                'roles' => array('administrator'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function setSuccess($message)
    {
        return Yii::app()->user->setFlash('success', $message);
    }

    public function setInfo($message)
    {
        return Yii::app()->user->setFlash('info', $message);
    }

    public function setWarning($message)
    {
        return Yii::app()->user->setFlash('warning', $message);
    }

    public function setError($message)
    {
        return Yii::app()->user->setFlash('error', $message);
    }

    public function actionInstall()
    {
        /** @var $auth CDbAuthManager */
        $auth = Yii::app()->authManager;

        $auth->clearAll();

        $auth->createOperation('viewTender');
        $auth->createOperation('createTender');
        $auth->createOperation('updateTender', '', 'return Yii::app()->user->model->canUpdateTender($params["tender"]);');

        $task = $auth->createTask('manageTender');
        $task->addChild('viewTender');
        $task->addChild('createTender');
        $task->addChild('updateTender');


        $auth->createOperation('createBid', '', 'return Yii::app()->user->model->canCreateBid($params["tender"]);');
        $auth->createOperation('updateBid', '', 'return Yii::app()->user->model->canUpdateBid($params["bid"]);');
        $auth->createOperation('cancelBid', '', 'return Yii::app()->user->model->canCancelBid($params["bid"]);');
        $auth->createOperation('acceptBid', '', 'return Yii::app()->user->model->canAcceptBid($params["bid"]);');

        $task = $auth->createTask('manageBid');
        $task->addChild('createBid');
        $task->addChild('updateBid');
        $task->addChild('cancelBid');
        $task->addChild('acceptBid');


        $auth->createOperation('createEmployee');
        $auth->createOperation('updateEmployee', '', 'return Yii::app()->user->model->canUpdateEmployee($params["employee"]);');
        $auth->createOperation('deleteEmployee', '', 'return Yii::app()->user->model->canDeleteEmployee($params["employee"]);');
        $task = $auth->createTask('manageCompany', '', 'return Yii::app()->user->model->canManageCompany($params["profile"]);');
        $task->addChild('createEmployee');
        $task->addChild('updateEmployee');
        $task->addChild('deleteEmployee');


        $role = $auth->createRole('unconfirmedUser', '', 'return !Yii::app()->user->isGuest && (empty(Yii::app()->user->model->email) || Yii::app()->user->model->status == User::STATUS_NOT_CONFIRMED);');
        $role->addChild('manageTender');

        $role = $auth->createRole('user', '', 'return !Yii::app()->user->isGuest && (!empty(Yii::app()->user->model->email) && Yii::app()->user->model->status == User::STATUS_CONFIRMED);');
        $role->addChild('manageTender');
        $role->addChild('manageBid');

        $role = $auth->createRole('userWithProfile', '', 'return !Yii::app()->user->isGuest && !empty(Yii::app()->user->model->profile);');
        $role->addChild('user');

        $role = $auth->createRole('company', '', 'return !Yii::app()->user->isGuest && !empty(Yii::app()->user->model->profile) && Yii::app()->user->model->isCompany;');
        $role->addChild('userWithProfile');
        $role->addChild('manageCompany');

        $role = $auth->createRole('administrator', '', 'return Yii::app()->endName == "back" && !Yii::app()->user->isGuest && Yii::app()->user->id == 1;');

        $auth->save();
    }
}