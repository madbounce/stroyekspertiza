<?php

Yii::import('zii.widgets.grid.CGridView');

class CustomGridView extends CGridView
{
    public $template = '{items} {pager}';
    public $hideHeader = true;
    public $pager = array('class' => 'CustomPager');
    public $htmlOptions = array('class' => 'searchresultswr');
    public $itemsHtmlOptions = array(
        'class' => 'searchresultstbl podriadchiki',
        'width' => '100%',
        'border' => 0,
        'cellspacing' => 0,
        'cellpadding' => 0,
    );

    public function renderItems()
    {
        if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
        {
            echo CHtml::openTag('table', $this->itemsHtmlOptions);
            $this->renderTableHeader();
            ob_start();
            $this->renderTableBody();
            $body=ob_get_clean();
            $this->renderTableFooter();
            echo $body; // TFOOT must appear before TBODY according to the standard.
            echo CHtml::closeTag('table');
        }
        else
            $this->renderEmptyText();
    }
}
