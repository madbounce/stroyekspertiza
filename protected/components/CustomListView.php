<?php

Yii::import('zii.widgets.CListView');

class CustomListView extends CListView
{
    public $template = "{items}\n{pager}";

    public $pager = array('class' => 'CustomPager');

    public $pagerCssClass = 'custom-pager';
}
