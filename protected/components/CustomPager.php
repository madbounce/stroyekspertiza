<?php

class CustomPager extends CLinkPager {

    const TYPE_BLUE = 0;
    const TYPE_GREEN = 1;

    public $type = self::TYPE_BLUE;

    public function run() {
        $buttons = $this->createPageButtons();

        if (empty($buttons))
            return;

        if ($this->type == self::TYPE_BLUE)
            echo CHtml::tag('div', array('class' => 'nav'), implode("\n", $buttons));
        else if ($this->type == self::TYPE_GREEN)
            echo CHtml::tag('div', array('class' => 'paginator'), implode("\n", $buttons));
    }

    protected function createPageButtons()
    {
        if ($this->getPageCount() <= 1) return array();

        $buttons = array();
        for ($i = 0; $i < $this->getPageCount(); $i++) {
            if ($this->type == self::TYPE_BLUE)
                $buttons[] = $i == $this->getCurrentPage() ? CHtml::tag('span', array(), $i + 1) : CHtml::link($i + 1, $this->createPageUrl($i));
            else if ($this->type == self::TYPE_GREEN)
                $buttons[] = $i == $this->getCurrentPage() ? CHtml::link($i + 1, null, array('class' => 'selected')) : CHtml::link($i + 1, $this->createPageUrl($i));
        }

        return $buttons;
    }
}