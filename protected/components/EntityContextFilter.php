<?php

class EntityContextFilter extends CFilter
{
    public $entity;
    public $var = 'id';
    public $controllerVar;

    protected function preFilter($filterChain)
    {
        if (empty($this->entity))
            throw new CException('EntityContextFilter.entity property must be set.');

        if (empty($this->controllerVar))
            $this->controllerVar = lcfirst($this->entity);

        $entityId = null;
        if (isset($_GET[$this->var]))
            $entityId = $_GET[$this->var];
        else if (isset($_POST[$this->var]))
            $entityId = $_POST[$this->var];

        $entity = $this->entity;
        if ($this->var == 'id')
            $model = $entity::model()->findByPk($entityId);
        else
            $model = $entity::model()->findByAttributes(array($this->var => $entityId));

        if (empty($model))
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $filterChain->controller->{$this->controllerVar} = $model;

        return true;
    }
}
