<?php

class FrontEndController extends Controller
{
    public $layout = '//layouts/main';

    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'transparent' => true,
                'testLimit' => 5
            ),
        );
    }

    public function setMessage($header, $message)
    {
        return Yii::app()->user->setFlash('message', array('header' => $header, 'message' => $message));
    }

    public function forbidden()
    {
        throw new CHttpException(403, Yii::t('app', 'У вас недостаточно прав для выполнения указанного действия.'));
    }
}