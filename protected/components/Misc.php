<?php

class Misc
{
    public static function t($message, $params = array())
    {
        return Yii::t('app', $message, $params);
    }

    //Возвращает массив для заполнения месяца
    public static function monthes(){
        return array (
            '01' => Yii::t('app','Январь'),
            '02' => Yii::t('app','Февраль'),
            '03' => Yii::t('app','Март'),
            '04' => Yii::t('app','Апрель'),
            '05' => Yii::t('app','Май'),
            '06' => Yii::t('app','Июнь'),
            '07' => Yii::t('app','Июль'),
            '08' => Yii::t('app','Август'),
            '09' => Yii::t('app','Сентябрь'),
            '10' => Yii::t('app','Октябрь'),
            '11' => Yii::t('app','Ноябрь'),
            '12' => Yii::t('app','Декабрь')
        );
    }

    //Возвращает массив для заполнения года
    public static function years($yearBegin = 1971, $yearEnd = null){
        $yearsArray = array();
        if($yearEnd === null)
            $yearEnd = date('Y');

        for ($i = $yearEnd; $i >= $yearBegin; $i--) {
            $yearsArray[$i] = $i;
        }
        return $yearsArray;
    }
}
