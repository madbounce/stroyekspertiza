<?php

class WebUser extends CWebUser
{
    private $_model;

    function getModel()
    {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user;
    }

    protected function loadUser($id = null)
    {
        if (empty($this->_model))
            $this->_model = User::model()->findByPk($id);

        return $this->_model;
    }
}
