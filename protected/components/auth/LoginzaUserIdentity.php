<?php

class LoginzaUserIdentity extends CUserIdentity
{
    private $_id;

    public function getId()
    {
        return $this->_id;
    }

    public function __construct()
    {
    }

    public function authenticate($model = null)
    {
        if (empty($model->identity) || empty($model->provider))
            return false;

        $user = User::model()->findByAttributes(array('identity' => $model->identity, 'provider' => $model->provider));

        if (empty($user)) {
            $user = new User(User::SCENARIO_SOCIAL_REGISTER);
            $user->identity = $model->identity;
            $user->provider = $model->provider;
            $user->email = $model->email;
            $user->first_name = $model->first_name;
            $user->last_name = $model->last_name;
            $user->status = !empty($model->email) ? User::STATUS_CONFIRMED : User::STATUS_NOT_CONFIRMED;

            if (!$user->save()) return false;
        }

        $this->_id = $user->id;
        return true;
    }
}
