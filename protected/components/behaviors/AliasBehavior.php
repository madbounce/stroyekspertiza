<?php

class AliasBehavior extends CActiveRecordBehavior
{
    public $attributeCallback;
    public $aliasAttribute = 'alias';

    protected function setAlias()
    {
        $owner = $this->owner;

        $attributeCallback = $this->attributeCallback;
        $attribute = $attributeCallback($owner);

        $url = UrlTransliterate::cleanString($attribute);

        $criteria = new CDbCriteria;
        $criteria->addCondition(":attribute = :url");
        $criteria->params = array(':attribute' => $this->aliasAttribute, ':url' => $url);
        if (!$owner->isNewRecord) {
            $criteria->addCondition('t.id <> :id');
            $criteria->params[':id'] = $owner->primaryKey;
        }

        $count = $owner::model()->count($criteria);

        if ($count > 0)
            $url .= '-' . $count;

        $owner::model()->updateByPk($owner->primaryKey, array($this->aliasAttribute => $url));
    }

    public function afterSave($event)
    {
        $this->setAlias();
    }
}
