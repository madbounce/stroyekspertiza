<?php

class LocaleHelper
{
    private static $yiiTojQueryFormatConversion = array(
        'yyyy' => 'yy',
        'yy' => 'y',
        'MMMM' => 'MM',
        'MMM' => 'M',
        'MM' => 'mm',
        'M' => 'm',
    );

    public static function getJQueryDateFormat($width = 'medium')
    {
        return strtr(Yii::app()->getLocale()->getDateFormat($width),
            self::$yiiTojQueryFormatConversion);
    }

    private static $localeIds = array(
        'en' => 'en_GB',
        'ru' => 'ru_RU',
    );

    public static function getLocaleId($language)
    {
       return isset(self::$localeIds[$language]) ? self::$localeIds[$language] : 'en_GB';
    }
}
