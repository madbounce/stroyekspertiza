<?php

include_once Yii::getPathOfAlias('application.components.helpers.phpmorphy.src.common') . '.php';

class MorphologyHelper
{
    protected static $_morphy;

    protected static function getInstance()
    {
        if (self::$_morphy == null) {
            self::$_morphy = new phpMorphy(Yii::getPathOfAlias('application.components.helpers.phpmorphy.dicts'), 'ru_RU', array(
                'storage' => PHPMORPHY_STORAGE_FILE,
            ));
        }

        return self::$_morphy;
    }

    public static function filter($text)
    {
        return preg_replace('/[^\p{L}\p{N} ]+/u', '', $text);
    }

    public static function toBaseForm($text)
    {
        $morphy = self::getInstance();

        $text = self::filter($text);
        $text = explode(' ', $text);

        $result = array();
        foreach ($text as $word) {
            $base = $morphy->getBaseForm(mb_strtoupper($word, 'utf-8'));
            $result[] = isset($base[0]) ? $base[0] : null;
        }

        $result = array_filter($result);
        return implode(' ', $result);
    }

    public static function addBaseForm($text)
    {
        $base = self::toBaseForm($text);
        return self::filter($text) . ' ' . $base;
    }

    public static function findMAllForms($word){
        $morphy = self::getInstance();
        return  $morphy->getAllForms(mb_strtoupper($word, 'utf-8'));
    }
}
