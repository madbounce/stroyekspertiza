<?php

class CCustomPager extends CLinkPager {

    public function init()
    {
        if($this->nextPageLabel===null)
            $this->nextPageLabel=Yii::t('yii','Еще →');
        if($this->prevPageLabel===null)
            $this->prevPageLabel=Yii::t('yii','← Назад');

        parent::init();
    }

    public function run() {
        $buttons = $this->createPageButtons();

        if (empty($buttons))
            return;

        $currentPage = $this->getCurrentPage();
        $pageCount = $this->getPageCount();

        echo CHtml::link($this->prevPageLabel, $currentPage > 0 ? $this->createPageUrl($currentPage - 1) : 'javascript:void(0);', array('class' => 'prev'));
        echo CHtml::tag('ul', array(), implode("\n", $buttons));
        echo CHtml::link($this->nextPageLabel, $currentPage + 1 < $pageCount ? $this->createPageUrl($currentPage + 1) : 'javascript:void(0);', array('class' => 'next'));
    }

    protected function createPageButtons()
    {
        if ($this->getPageCount() <= 1) return array();

        $currentPage = $this->getCurrentPage();
        $pageCount = $this->getPageCount();
        $maxRange = 3;


        $buttons = array();
        if (2 * $maxRange + 1 > $pageCount) {
            for ($i = 0; $i < $this->getPageCount(); $i++) {
                $buttons[] = CHtml::tag('li', $i == $this->getCurrentPage() ? array('class' => 'active') : array(), CHtml::link($i + 1, $this->createPageUrl($i)));
            }
        } else {
            if ($currentPage - $maxRange <= 0) {
                for ($i = 0; $i < 2 * $maxRange; $i++) {
                    $buttons[] = CHtml::tag('li', $i == $this->getCurrentPage() ? array('class' => 'active') : array(), CHtml::link($i + 1, $this->createPageUrl($i)));
                }
                $buttons[] = CHtml::tag('li', array(), '...');
                $buttons[] = CHtml::tag('li', array(), CHtml::link($pageCount, $this->createPageUrl($pageCount-1)));
            } else if ($currentPage + $maxRange >= $pageCount - 1) {
                $buttons[] = CHtml::tag('li', array(), CHtml::link('1', $this->createPageUrl(0)));
                $buttons[] = CHtml::tag('li', array(), '...');
                for ($i = $pageCount - 2 * $maxRange; $i < $pageCount; $i++) {
                    $buttons[] = CHtml::tag('li', $i == $this->getCurrentPage() ? array('class' => 'active') : array(), CHtml::link($i + 1, $this->createPageUrl($i)));
                }
            } else {
                $buttons[] = CHtml::tag('li', array(), CHtml::link('1', $this->createPageUrl(0)));
                $buttons[] = CHtml::tag('li', array(), '...');
                for ($i = $currentPage - $maxRange; $i <= $currentPage + $maxRange; $i++) {
                    $buttons[] = CHtml::tag('li', $i == $this->getCurrentPage() ? array('class' => 'active') : array(), CHtml::link($i + 1, $this->createPageUrl($i)));
                }
                $buttons[] = CHtml::tag('li', array(), '...');
                $buttons[] = CHtml::tag('li', array(), CHtml::link($pageCount, $this->createPageUrl($pageCount-1)));
            }
        }
        return $buttons;
    }
}