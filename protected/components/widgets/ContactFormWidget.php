<?php

class ContactFormWidget extends CWidget
{
    public $action;
    public $hideErrorMessage = true;

    public function run()
    {
        $model = new ContactForm();
        $this->render('contactForm', array('model' => $model));
    }
}
