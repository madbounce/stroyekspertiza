<?php
Yii::import('ext.jquery-gmap.*');
class ContactGMapWidget extends CWidget
{
    public $address;
    public $width = 251;
    public $height = 195;

    public function run()
    {
        if (empty($this->address))
            throw new CException('ContactGMapWidget.address property must be set.');

        $gmap = new EGmap3Widget();

        $options = array(
            'scaleControl' => false,
            'streetViewControl' => false,
            'zoom' => 14,
            'mapTypeId' => EGmap3MapTypeId::ROADMAP,
            'zoomControlOptions' => array(
                'style' => EGmap3ZoomControlStyle::SMALL,
            ),
        );

        $gmap->setOptions($options);
        $gmap->width = $this->width;
        $gmap->height = $this->height;

        // create the marker
        $marker = new EGmap3Marker(array(
            'title' => $this->address,
            'draggable' => false,
        ));

        $marker->address = $this->address;
        $gmap->add($marker);
        $marker->centerOnMap();
        $gmap->renderMap();
    }
}
