<?php

Yii::import('zii.widgets.jui.CJuiDialog');

class DialogWidget extends CJuiDialog
{
    public $title;
    public $autoOpen = false;
    public $close = false;
    public $closeable = true;

    public function init()
    {
        $this->themeUrl = Yii::app()->theme->baseUrl . '/css/jqueryui';
        $this->theme = 'iams';

        $this->options = CMap::mergeArray($this->options, array(
            'title' => $this->title,
            'autoOpen' => $this->autoOpen,
            'modal' => true,
            'draggable' => false,
            'resizable' => false,
            'minHeight' => 0,
            'show' => array('effect' => 'fade', 'duration' => 200),
            'hide' => array('effect' => 'fade', 'duration' => 200),
        ));

        if ($this->close) {
            $this->options['open'] = 'js:function(event, ui) {
                var timeout = setTimeout("$(\"#' . $this->id . '\").dialog(\"close\")", ' . $this->close . ');
                 $(".ui-widget-overlay").bind("click", function() { $("#' . $this->id . '").dialog("close"); clearTimeout(timeout); });
            }';
        } else if ($this->closeable) {
            $this->options['open'] = 'js:function(event, ui) {
                 $(".ui-widget-overlay").bind("click", function() { $("#' . $this->id . '").dialog("close"); });
            }';
        } else {
            $this->options['closeOnEscape'] = false;
            $this->options['open'] = 'js:function(event, ui) {
                 $(".ui-dialog-titlebar-close").hide();
            }';
        }

        parent::init();
    }
}
