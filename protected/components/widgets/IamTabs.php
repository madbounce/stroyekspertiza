<?php

class IamTabs extends CTabView
{
    public $ulClass = 'tabs';

    protected function renderHeader()
    {
        echo "<ul class=\"" . $this->ulClass . "\">\n";
        foreach($this->tabs as $id=>$tab)
        {
            $tab['itemClass'] = isset($tab['itemClass']) ? $tab['itemClass'] : '';
            $title=isset($tab['title'])?$tab['title']:'undefined';
            $active=$id===$this->activeTab?' class="active"' : '';
            $url=isset($tab['url'])?$tab['url']:"#{$id}";
            echo "<li class='".$tab['itemClass']."'><a href=\"{$url}\"{$active}>{$title}</a></li>\n";
        }
        echo "</ul>\n";
    }
}