<?php

class LikeWidget extends CWidget
{
    public $url;

    public function init()
    {
        if (empty($this->url))
            throw new CException('LikeWidget.url property must be set.');

        Yii::app()->clientScript->registerScriptFile('http://vk.com/js/api/share.js?11', CClientScript::POS_HEAD);
        //Yii::app()->clientScript->registerScriptFile('//vk.com/js/api/openapi.js?58', CClientScript::POS_HEAD);
        //Yii::app()->clientScript->registerScript('vk-init', 'VK.init({apiId: 3182151, onlyWidgets: true});', CClientScript::POS_HEAD);
    }

    public function run()
    {
        $this->render('like');
    }
}
