<?php

class MessagePopupWidget extends CWidget
{
    public function run()
    {
        $this->render('messagePopup', array(
            'autoOpen' => Yii::app()->user->hasFlash('message'),
            'message' => Yii::app()->user->getFlash('message'),
        ));
    }
}
