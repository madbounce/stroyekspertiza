<?php

class NutritionSlider extends CWidget
{

    public $condition = null;
    public function run()
    {
        $criteria = new CDbCriteria();
        if($this->condition !== null){
            $criteria->addCondition($this->condition);
        }
        $model = Articles::model()->findAll($criteria);
        $this->registerJs();
        $this->render('nutritionSlider', array('model' => $model));
    }

    public function registerJs(){
        $carouselJs=<<<JS
         jQuery('.belt2 .ubelt2').jcarousel({
            scroll: 2,
            initCallback: mycarousel_initCallback1,
            buttonNextHTML: null,
            buttonPrevHTML: null
        });
JS;

		$startcarouselJs1=<<<JS
		$("#car2").css("display","inherit");
		carousel2 = new Carousel(".belt2 .ubelt2", 
        { scroll: 2, initCallback: mycarousel_initCallback1,
            buttonNextHTML: null,
            buttonPrevHTML: null,
            itemFallbackDimension: 300,
            wrap : 'last'
        });
JS;

        $js = <<<JS
         function mycarousel_initCallback1(carousel) {
        jQuery('#imgnext1').bind('click', function() {
            carousel.next();
            return false;
        });

        jQuery('#imgprev1').bind('click', function() {
            carousel.prev();
            return false;
        });
    };
JS;


        $cs = Yii::app()->clientScript;
//        $cs->registerScript('jcarousel1', $carouselJs, CClientScript::POS_END);
		$cs->registerScript('jcarousel1', $startcarouselJs1, CClientScript::POS_READY);
        $cs->registerScript('NutritionSlider', $js, CClientScript::POS_END);

        $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.jcarousel.js');
        $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/jcarousel.css');

    }
}
