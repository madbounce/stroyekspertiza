<?php

class ProductSlider extends CWidget
{

    public $condition = null;
    public function run()
    {
        $criteria = new CDbCriteria();
        if($this->condition !== null){
            $criteria->addCondition($this->condition);
        }
        $model = Catalog::model()->findAll($criteria);
        $this->registerJs();
        $this->render('productSlider', array('model' => $model));
    }


	
	public function registerJs(){
		$initCarouselJs=<<<JS
		var Carousel = function(carouselElementID, carouselCfg) {
         this.init(carouselElementID, carouselCfg);
		};
		
		Carousel.prototype = {
		   init: function(id, cfg) {
			  var config = {
				 prevButtonStateHandler: null,
				 nextButtonStateHandler: null
			  };
		
			  for (var key in cfg) {
				 if (!cfg.hasOwnProperty(key)) { continue; }
				 config[key] = cfg[key];
			  }
		
			  this.carousel = jQuery(id).jcarousel(config);
		
		   },
		
		   handlePrevButtonState: function(type, args) {
				var enabling = args[0];
				var leftImage = args[1];
				if(enabling) {
					leftImage.src = "images/left-enabled.gif";    
				} else {
					leftImage.src = "images/left-disabled.gif";    
				}
		   },
		
		   handleNextButtonState: function(type, args) {
				var enabling = args[0];
				var rightImage = args[1];
		
				if(enabling) {
					rightImage.src = "images/right-enabled.gif";
				} else {
					rightImage.src = "images/right-disabled.gif";
				}
			}
		};
		
		var carousel, carousel2;
JS;

		$startcarouselJs=<<<JS
		carousel = new Carousel(".d-carousel .carousel", 
        { scroll: 3, initCallback: mycarousel_initCallback,
            buttonNextHTML: null,
            buttonPrevHTML: null });
JS;
		
        $carouselJs=<<<JS
         jQuery('.d-carousel .carousel').jcarousel({
            scroll: 3,
            initCallback: mycarousel_initCallback,
            buttonNextHTML: null,
            buttonPrevHTML: null
        });
JS;
        $js = <<<JS
         function mycarousel_initCallback(carousel) {
        jQuery('#imgnext').bind('click', function() {
            carousel.next();
            return false;
        });

        jQuery('#imgprev').bind('click', function() {
            carousel.prev();
            return false;
        });
    };
JS;


        $cs = Yii::app()->clientScript;
//        $cs->registerScript('jcarousel', $carouselJs, CClientScript::POS_END);
		$cs->registerScript('jcarousel', $initCarouselJs, CClientScript::POS_END);
		$cs->registerScript('jcarousel', $startcarouselJs, CClientScript::POS_READY);
        $cs->registerScript('ProductSlider', $js, CClientScript::POS_END);

        $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.jcarousel.js');
        $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/jcarousel.css');

    }
}
