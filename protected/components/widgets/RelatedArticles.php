<?php

class RelatedArticles extends CWidget
{
    public $limit = 5;
    public $category_id = 20;

    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.material_category_id =' . $this->category_id);
        $criteria->limit = $this->limit;

        $items = Articles::model()->findAll($criteria);

        if (!empty($items))
            $this->render('relatedArticles', array('items' => $items));
    }
}
