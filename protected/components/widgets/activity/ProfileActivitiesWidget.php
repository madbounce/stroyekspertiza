<?php

class ProfileActivitiesWidget extends CWidget
{
    public $model;

    public function run()
    {
        if (empty($this->model))
            throw new CException('ProfileActivitiesWidget.model property must be set.');

        $this->render('profileActivities', array(
            'items' => $this->model->activities,
        ));
    }
}
