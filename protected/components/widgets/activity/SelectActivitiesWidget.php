<?php

class SelectActivitiesWidget extends CInputWidget
{
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        if (isset($this->htmlOptions['id']))
            $id = $this->htmlOptions['id'];
        else
            $this->htmlOptions['id'] = $id;
        if (isset($this->htmlOptions['name']))
            $name = $this->htmlOptions['name'];

        if ($this->hasModel()) {
            $this->render('selectActivities', array(
                'items' => Activity::model()->cache(60)->parent()->with(array('activities.activities' => array('alias' => 'subractivities')))->findAll(),
                'selectedItemIds' => CHtml::listData($this->model->activities, 'id', 'id'),
                'id' => $id,
                'name' => $name,
            ));
        } else
            throw new CException('SelectActivitiesWidget.model and SelectActivitiesWidget.attribute must be set.');
    }
}
