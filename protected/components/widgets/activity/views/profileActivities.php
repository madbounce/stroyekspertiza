<div class="activities-list">
    <h2><?php echo Yii::t('profile', 'Сферы деятельности'); ?></h2>
    <ul class="activ-list">
        <?php foreach ($items as $item): ?>
            <li>
                <?php // TODO Ссылка на поиск по сфере ?>
                <?php echo CHtml::link(CHtml::encode(Yii::t('activity', $item->name)), '#', array('style' => 'text-decoration: none')); ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>