<?php
// TODO Копипаст из старого проекта, проверить и переписакть если будет время
Yii::app()->clientScript->registerScript('chb-list-specializations', "
$('.plus').live('click', function () {
    $(this).removeClass('plus');
    $(this).addClass('minus');
    //$(this).parent().siblings('input:checkbox').attr('checked', true);
    $(this).parent().siblings('div.pad_scope_choice').slideDown();
    return false;
});
$('.minus').live('click', function () {
    $(this).removeClass('minus');
    $(this).addClass('plus');
    //$(this).parent().siblings('input:checkbox').attr('checked', false);
    $(this).parent().siblings('div.pad_scope_choice').slideUp();
    return false;
});
$('.parent-chb').click(function () {
    inp = $(this).parent('div.tender-item').find('div.pad_scope_choice').find('input');
    var checked = $(this).attr('checked');

    $.each(inp, function () {
        if (checked == 'checked')
            $(this).attr('checked', true);
        else
            $(this).attr('checked', false);

    });
});
$('.child-chb').click(function () {
    inp = $(this).parent('div.pad_scope_choice').find('input');
    i = 0;
    j = 0;
    $.each(inp, function () {
        if ($(this).attr('checked'))
            j++;
        else
            i++;
    });
    if (i == inp.length) {
        parentinp = $(this).parent().parent().find('input.parent-chb')
        $(parentinp).attr('checked', false);
    }
    if (j == inp.length) {
        parentinp = $(this).parent().parent().find('input.parent-chb')
        $(parentinp).attr('checked', true);
    }

});
");
?>

<div class="sfersblocks">
    <?php $parts = array_chunk($items, 3, true); ?>
    <?php foreach ($parts as $items): ?>
        <div class="scope_choice">
        <?php foreach ($items as $item): ?>
                <div class="tender-item">
                    <?php echo CHtml::checkBox($name . "[]", in_array($item->id, $selectedItemIds), array('value' => $item->id, 'class' => 'parent-chb')); ?>
                    <?php echo CHtml::label(CHtml::link(CHtml::encode(Yii::t('activity', $item->name)), '#', array('class' => !empty($item->activities) ? 'plus' : '')), $id . "_$item->id"); ?>
                    <br>

                    <?php if (!empty($item->activities)): ?>

                    <div class="pad_scope_choice">
                        <?php foreach ($item->activities as $item2): ?>
                            <?php echo CHtml::checkBox($name . "[]", in_array($item2->id, $selectedItemIds), array('value' => $item2->id, 'class' => 'child-chb')); ?>
                            <?php echo CHtml::label(CHtml::encode(Yii::t('activity', $item2->name)), $id . "_$item2->id"); ?>
                            <br>
                        <?php endforeach; ?>
                    </div>

                    <?php endif; ?>
                </div>
        <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</div>