<?php

class LoginUiDialogWidget extends CWidget
{
    public $visible = true;
    public $dialogOptions; //jquery-ui dialog Options
    public $hideErrorMessage = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        //jquery-ui dialog default Options
        $defaultOptions = array(
            'id' => 'dialog_login_block',
            'title' =>  Misc::t('Вход'),
            'htmlOptions' => array('class' => 'common-popup block_auth'),
            'options' => array(
                'autoOpen' => false,
                'modal' => true,
                'width' => 525,
                'resizable' => false,
                'draggable' => false,
                'position' => 'center',
                'create' => 'js:function(event, ui){
                    $(this).dialog().children(".ui-dialog-wrapper").show();
                 }'

            )
        );

        // Merge with user set options if array is provided
        if ($this->dialogOptions) {
            if (is_array($this->dialogOptions)) {
                $this->dialogOptions = CMap::mergeArray(
                    $defaultOptions, $this->dialogOptions);
            } else
                throw new CException(Misc::t('Опции должны быть массивом'));
        } else
            $this->dialogOptions = $defaultOptions;

        //Register Js
        $this->registerJs();

        $model = new LoginForm;
        $this->render('loginUiDialog', array('model' => $model));
    }

    private function registerJs()
    {
        Yii::app()->clientScript->registerScript('LoginUiDialogWidget', '
            $( "#dialog-login, .dialog-login" ).click(function( event ) {
                $( "#dialog_login_block" ).dialog( "open" );
                event.preventDefault();
            });
            $( "#dialog-registration_2" ).click(function( event ) {
                $( "#dialog_login_block" ).dialog( "close" );
                $( "#dialog_registration_block" ).dialog( "open" );
                event.preventDefault();
		    });
        ');
    }
}
