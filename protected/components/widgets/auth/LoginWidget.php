<?php

class LoginWidget extends CWidget
{
    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model = new LoginForm;
        $this->render('login', array('model' => $model));
    }
}
