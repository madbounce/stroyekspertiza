<?php

class LoginzaWidget extends CWidget
{
    protected $tokenUrl = 'https://loginza.ru/api/widget?';
    protected $returnUrl = '/auth/loginzaLogin';
    protected $providers = array(
        'vkontakte' => 'vk.png',
        'facebook' => 'fb.png',
        'twitter' => 'tw.png',
        'mailruapi' => 'mail.png'
    );

    public function run()
    {
        Yii::app()->clientScript->registerScriptFile('http://loginza.ru/js/widget.js', CClientScript::POS_END);
        $this->render('loginzaWidget');
    }
}