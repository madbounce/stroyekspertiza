<?php

class RegisterUiDialogWidget extends CWidget
{
    public $visible = true;
    public $dialogOptions; //jquery-ui dialog Options
    public $hideErrorMessage = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        //jquery-ui dialog default Options
        $defaultOptions = array(
            'id' => 'dialog_registration_block',
            'title' => Misc::t('Регистрация'),
            'options' => array(
                'autoOpen' => false,
                'modal' => true,
                'width' => 525,
                'resizable' => false,
                'draggable' => false,
                'position' => 'center',
                'create' => 'js:function(event, ui){
                    $(this).dialog().children(".ui-dialog-wrapper").show();
                 }'
            )
        );

        // Merge with user set options if array is provided
        if ($this->dialogOptions) {
            if (is_array($this->dialogOptions)) {
                $this->dialogOptions = CMap::mergeArray(
                    $defaultOptions, $this->dialogOptions);
            } else
                throw new CException(Misc::t('Опции должны быть массивом'));
        } else
            $this->dialogOptions = $defaultOptions;

        //Register Js
        $this->registerJs();

        $model = new User(User::SCENARIO_REGISTER);
        $this->render('registerUiDialog', array('model' => $model));
    }

    private function registerJs()
    {
        Yii::app()->clientScript->registerScript('RegisterUiDialogWidget', '
            $( "#dialog-registration, .dialog-registration" ).click(function( event ) {
                $( "#dialog_registration_block" ).dialog( "open" );
                event.preventDefault();
            });
		    $( ".already_registered" ).click(function(event){
		        $( "#dialog_registration_block" ).dialog( "close" );
		        $( "#dialog_login_block" ).dialog( "open" );
		        event.preventDefault();
		    })
        ');
    }
}
