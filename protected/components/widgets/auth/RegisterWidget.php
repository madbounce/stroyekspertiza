<?php

class RegisterWidget extends CWidget
{
    public $visible = true;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        $model = new User(User::SCENARIO_REGISTER);
        $this->render('register', array('model' => $model));
    }
}