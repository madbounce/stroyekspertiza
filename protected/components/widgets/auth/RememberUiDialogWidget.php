<?php

class RememberUiDialogWidget extends CWidget
{
    public $visible = true;
    public $dialogOptions; //jquery-ui dialog Options
    public $hideErrorMessage = false;

    public function run()
    {
        if ($this->visible) {
            $this->renderContent();
        }
    }

    protected function renderContent()
    {
        //jquery-ui dialog default Options
        $defaultOptions = array(
            'id' => 'dialog_recovery_block',
            'title' => Misc::t('Восстановление пароля'),
            'options' => array(
                'autoOpen' => false,
                'modal' => true,
                'width' => 525,
                'resizable' => false,
                'draggable' => false,
                'position' => 'center',
                'create' => 'js:function(event, ui){
                    $(this).dialog().children(".ui-dialog-wrapper").show();
                 }'
            )
        );

        // Merge with user set options if array is provided
        if ($this->dialogOptions) {
            if (is_array($this->dialogOptions)) {
                $this->dialogOptions = CMap::mergeArray(
                    $defaultOptions, $this->dialogOptions);
            } else
                throw new CException(Misc::t('Опции должны быть массивом'));
        } else
            $this->dialogOptions = $defaultOptions;

        //Register Js
        $this->registerJs();

        $model = new User(User::SCENARIO_REGISTER);
        $this->render('rememberUiDialog', array('model' => $model));
    }

    private function registerJs()
    {
        Yii::app()->clientScript->registerScript('RememberUiDialogWidget', '
            $( "#dialog-recovery" ).click(function( event ) {
                $(".ui-widget-overlay").click();
                $( "#dialog_recovery_block" ).dialog( "open" );
                event.preventDefault();
            });
        ');
    }
}
