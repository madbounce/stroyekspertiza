<div id="login-popup" class="common-popup block_auth">
    <div class="str_auth"></div>
    <h3 class="auth-title">
        <?php echo Misc::t('Авторизуйтесь в системе через свою любимую социальную сеть'); ?>:
    </h3>

    <div class="shares removBackground">
        <?php $this->widget('LoginzaWidget'); ?>
    </div>

    <div id="auth">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'action' => array('/auth/login'),
            'htmlOptions' => array('class' => 'form_auth'),
        )); ?>

        <div class="inputs_auth">
            <?php echo $form->textField($model, 'username', array(
                'placeholder' => Misc::t('E-mail'),
                'tabindex' => 0,
            )); ?>
            <br/>
            <?php echo $form->passwordField($model, 'password', array(
                'placeholder' => Misc::t('Пароль'),
                'tabindex' => 0,
            )); ?>
        </div>

        <?php echo CHtml::submitButton(Misc::t('Войти'), array('class' => 'but_enter')); ?>

        <div class="clear"></div>
        <div class="lnk_auth">
            <?php echo CHtml::link(Misc::t('Забыли пароль?'), '#', array('onclick' => "$('#auth').hide(); $('#pass-recovery').show(); return false;")); ?>
        </div>

        <div class="remember_me_check">
            <?php echo $form->checkBox($model, 'rememberMe');?>
            <span><?php echo $form->labelEx($model, 'rememberMe'); ?></span>
        </div>

        <?php $this->endWidget(); ?>

    </div>

    <div id="pass-recovery" style="display:none;">
        <?php
        /**
         * @var $reminderForm ReminderForm
         * @var $form CActiveForm
         */
        $reminderForm = new ReminderForm;
        $form = $this->beginWidget('CActiveForm', array(
            'action' => array('/auth/reminder'),
            'id' => 'pass-recovery-f',
            'htmlOptions' => array('class' => 'pass-recovery'),
        ));
        ?>

        <?php echo CHtml::label('Введите ваш e-mail', CHtml::activeId($reminderForm, 'email')); ?>
        <div class="inputs_auth">
            <?php echo $form->textField($reminderForm, 'email', array(
                'placeholder' => Misc::t('E-mail'),
            )); ?>
        </div>

        <?php echo CHtml::submitButton(Misc::t('Выслать'), array('class' => 'but_send')); ?>

        <div class="clear"></div>
        <p><?php echo Misc::t('Письмо с дальнейшими действиями по восстановле- нию пароля будет выслано вам на почту'); ?></p>
        <?php $this->endWidget(); ?>
    </div>

</div>