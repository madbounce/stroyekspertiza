<?php foreach ($this->providers as $provider => $image): ?>
    <?php $image = CHtml::image(Yii::app()->theme->baseUrl . '/images/' . $image, '', array('width' => '30', 'height' => 30)); ?>
    <?php $params = array(
        'token_url' => Yii::app()->createAbsoluteUrl($this->returnUrl),
        'lang' => Yii::app()->language,
        'provider' => $provider,
    ); ?>
    <?php echo CHtml::link($image, $this->tokenUrl . http_build_query($params)); ?>
<?php endforeach; ?>