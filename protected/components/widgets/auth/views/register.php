<div id="register-popup" class="common-popup block_auth">
    <div class="str_auth"></div>
    <h3 class="auth-title">
        <?php echo Misc::t('Авторизуйтесь в системе через свою любимую социальную сеть'); ?>:
    </h3>

    <div class="shares removBackground">
        <?php $this->widget('LoginzaWidget'); ?>
    </div>

    <div id="auth">
        <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'register-form',
        'action' => array('/auth/register'),
        'htmlOptions' => array('class' => 'form_auth'),
    )); ?>

        <div class="inputs_register">
            <?php echo Misc::t('E-mail'); ?><span class="red">*</span>:
            <div class="clear"></div>
            <?php echo $form->textField($model, 'email'); ?>
            <br/>

            <?php echo Misc::t('Пароль'); ?><span class="red">*</span>:
            <div class="clear"></div>
            <?php echo $form->passwordField($model, 'password'); ?>
            <br/>

            <?php echo Misc::t('Пароль еще раз'); ?><span class="red">*</span>:
            <div class="clear"></div>
            <?php echo $form->passwordField($model, 'password_repeat'); ?>
            <br/>

            <?php echo Misc::t('Имя'); ?><span class="red">*</span>:
            <div class="clear"></div>
            <?php echo $form->textField($model, 'first_name'); ?>
            <br/>

            <?php echo Misc::t('Фамилия'); ?><span class="red">*</span>:
            <div class="clear"></div>
            <?php echo $form->textField($model, 'last_name'); ?>
            <br/>

            <?php echo Misc::t('Контактный телефон'); ?>:
            <div class="clear"></div>
            <?php echo $form->textField($model, 'contact_phone'); ?>
            <br/>

            <?php echo Misc::t('Skype'); ?>:
            <div class="clear"></div>
            <?php echo $form->textField($model, 'skype'); ?>
            <br/>

            <?php echo Misc::t('ICQ'); ?>:
            <div class="clear"></div>
            <?php echo $form->textField($model, 'icq'); ?>
            <br/>

            <div class="cap_pop" style="width: 135px;float:left;">
                <div>
                    <?php $this->widget('CCaptcha', array(
                        'showRefreshButton' => true,
                        'buttonLabel' => Misc::t('Обновить картинку'),
                        'captchaAction' => '/auth/captcha'
                    )); ?>
                </div>
            </div>
            <div class="cap_fild" style="float:left;width:170px;">
                <div class="gray">
                    <?php echo Misc::t('Введите символы с изображения'); ?><span class="red">*</span>:
                </div>
                <?php echo $form->textField($model, 'captchaCode', array('class' => 'input_txt verCodeRegister captcha')); ?>
                <div class="capcha_error"><?php echo $form->error($model, 'captchaCode', array('class' => 'ltr_error')); ?></div>
            </div>
        </div>

        <div class="clear"></div>
        <?php echo CHtml::submitButton(Misc::t('Зарегистрироваться'), array('class' => 'but_register')); ?>

        <?php $this->endWidget(); ?>
    </div>
</div>