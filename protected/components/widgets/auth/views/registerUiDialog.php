<?php $this->beginWidget('DialogWidget', $this->dialogOptions); ?>
<div class="ui-dialog-wrapper" style="display: none">
    <div class="error_message" style="display: none"><?php echo Misc::t('Пожалуйста, проверьте правильность введенной вами информации.'); ?></div>
    <div class="login_form">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'register-form',
            'action' => array('/auth/register'),
            'htmlOptions' => array('class' => 'form_auth'),
            'enableAjaxValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => false,
                'afterValidate'=>"js:function(form, data, hasError) {
                    if (hasError) {
                        $(form).parent().parent().find('.error_message').show();
                        //$('.captcha-img').trigger('click');
                        return false;
                    } else {
                        $(form).parent().parent().find('.error_message').hide();
                        return true;
                    }
                }",
            )
        )); ?>
        <div class="form-line">
            <?php echo $form->labelEx($model, 'email', array('label' => Misc::t('Ваш e-mail'))); ?>
            <span class="form-field"><?php echo $form->emailField($model, 'email', array(
                'class' => 'text_input',
                //'placeholder' => Misc::t('Email')
            )); ?></span>
            <?php echo $form->error($model, 'email', array('hideErrorMessage' => false));?>
        </div>

        <div class="form-line">
            <?php echo $form->labelEx($model, 'first_name'); ?>
            <span class="form-field"><?php echo $form->textField($model, 'first_name', array(
                'class' => 'text_input',
                //'placeholder' => Misc::t('Имя')
            )); ?></span>
            <?php echo $form->error($model, 'first_name', array('hideErrorMessage' => $this->hideErrorMessage));?>
        </div>

        <div class="form-line">
            <?php echo $form->labelEx($model, 'last_name'); ?>
            <span class="form-field"><?php echo $form->textField($model, 'last_name', array(
                'class' => 'text_input',
                //'placeholder' => Misc::t('Фамилия')
            )); ?></span>
            <?php echo $form->error($model, 'last_name', array('hideErrorMessage' => $this->hideErrorMessage));?>
        </div>

        <div class="form-line">
            <?php echo $form->labelEx($model, 'password'); ?>
            <span class="form-field"><?php echo $form->passwordField($model, 'password', array(
                'class' => 'text_input',
                //'placeholder' => Misc::t('Пароль')
            )); ?></span>
            <?php echo $form->error($model, 'password', array('hideErrorMessage' => $this->hideErrorMessage));?>
        </div>

        <div class="form-line">
            <?php echo $form->labelEx($model, 'password_repeat'); ?>
            <span class="form-field"><?php echo $form->passwordField($model, 'password_repeat', array(
                'class' => 'text_input',
                //'placeholder' => Misc::t('Повторите пароль')
            )); ?></span>
            <?php echo $form->error($model, 'password_repeat', array('hideErrorMessage' => $this->hideErrorMessage));?>
        </div>

        <p class="note"><span class="required">*</span> - <?php echo Misc::t('обязательные поля для заполнения'); ?></p>

        <div class="form-line">
             <span class="form-field">
                 <?php $offerLink = CHtml::link(Misc::t('договора оферты'), array('/page/default/view', 'name' => 'offer'), array('target' => '_blank')); ?>
                 <label class="check_label" for="<?php echo CHtml::activeId($model, 'offer'); ?>">
                     <?php echo $form->checkBox($model, 'offer');?><?php echo Misc::t('Я соглашаюсь с условиями') . ' '. $offerLink ?>
                 </label>
             </span>
             <?php echo $form->error($model, 'offer', array('hideErrorMessage' => $this->hideErrorMessage));?>
        </div>

        <?php echo CHtml::submitButton(Misc::t('Регистрация'), array('class' => 'login_btn')); ?>

        <a href="#" class="already_registered"><?php echo Misc::t('Уже зарегистрированы?'); ?></a>
        <?php $this->endWidget(); ?>
    </div>
    <div class="right_dialog_block">
        <p><?php echo Misc::t('Вы так же можете воспользоваться своим логином на одном из популярных сайтов'); ?>:</p>
        <?php $this->widget('LoginzaWidget'); ?>
    </div>
</div>
<?php $this->endWidget('DialogWidget'); ?>

