<?php $this->beginWidget('DialogWidget', $this->dialogOptions); ?>
<div class="ui-dialog-wrapper" style="display:none">
    <div class="login_form">
        <?php
        /**
         * @var $reminderForm ReminderForm
         * @var $form CActiveForm
         */
        $reminderForm = new ReminderForm;
        $form = $this->beginWidget('CActiveForm', array(
            'action' => array('/auth/reminder'),
            'id' => 'reminder-form',
            'htmlOptions' => array('class' => 'pass-recovery'),
            'enableAjaxValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => false
            )
        ));
        ?>
        <div class="form-line">
            <?php echo $form->label($model, 'email', array('label' =>Misc::t('Ваш e-mail или логин'), 'style' => 'display: block')); ?>
            <?php echo $form->error($reminderForm, 'email', array('hideErrorMessage' => $this->hideErrorMessage,  'class' => 'red_note'))?>
            <?php echo $form->emailField($reminderForm, 'email', array(
                //'placeholder' => Misc::t('E-mail'),
                'class' => 'text_input'
                ));
            ?>
        </div>
        <?php echo CHtml::submitButton(Misc::t('Отправить'), array('class' => 'login_btn', 'style' => 'margin-bottom: 15px;')); ?>
        <?php $this->endWidget(); ?>
    </div>
</div>
<?php $this->endWidget('DialogWidget'); ?>
