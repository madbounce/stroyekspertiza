<?php

class EmployeeWidget extends CWidget
{
    public $type;

    public function init()
    {
        $this->registerScripts();
    }

    public function run()
    {
        $this->render('employee');
    }

    protected function registerScripts()
    {
        Yii::app()->clientScript->registerScript(__CLASS__ . $this->id . "-create", '
        var create = function() {
            ' . CHtml::ajax(array(
            'url' => array('/employee/create'),
            'dataType' => 'json',
            'type' => 'post',
            'success' => 'js:function(data) {
                    if (data.success) {
                        $("#employee-create-dialog").dialog("close");
                    } else {
                        $("#employee-create-dialog-content").html(data.data);
                        $("#employee-create-dialog").dialog("open");
                    }
                }',
        )) . '
        };

        $("a.employee-create").live("click", function() {
            create();
            return false;
        });
        ', CClientScript::POS_READY);

        Yii::app()->clientScript->registerScript(__CLASS__ . $this->id . "-update", '
        var update = function(id) {
            ' . CHtml::ajax(array(
                'url' => array('/employee/update'),
                'data' => array('id' => 'js:id'),
                'dataType' => 'json',
                'type' => 'post',
                'success' => 'js:function(data) {
                    if (data.success) {
                        $("#employee-update-dialog").dialog("close");
                    } else {
                        $("#employee-update-dialog-content").html(data.data);
                        $("#employee-update-dialog").dialog("open");
                    }
                }',
            )) . '
        };

        $("a.employee-update").live("click", function() {
            var id = $(this).attr("data-id");
            update(id);
            return false;
        });
        ', CClientScript::POS_READY);

        Yii::app()->clientScript->registerScript(__CLASS__ . $this->id . "-delete", '
        $("a.employee-delete").live("click", function() {
            $("#employee-delete-dialog a.yes").attr("data-id", $(this).attr("data-id"));
            $("#employee-delete-dialog").dialog("open");
            return false;
        });
        $("#employee-delete-dialog a.yes").live("click", function() {
            var id = $(this).attr("data-id");
            var url = ' . CJavaScript::encode(CHtml::normalizeUrl(array('/employee/delete'))) . ' + "/" + id;
            window.location.href = url;
            return false;
        });
        $("#employee-delete-dialog a.no").live("click", function() {
            $("#employee-delete-dialog").dialog("close");
            return false;
        });
        ', CClientScript::POS_READY);
    }
}