<?php $this->beginWidget('DialogWidget', array(
    'id' => 'employee-create-dialog',
    'title' => '<h1 class="popup-title title" style="font-size: 20px; text-align: center;">' . Yii::t('profile', 'Пригласить сотрудника') . '</h1>',
    'options' => array('width' => 320),
)); ?>

<div id="employee-create-dialog-content" class="inviteblock">
</div>

<?php $this->endWidget(); ?>


<?php $this->beginWidget('DialogWidget', array(
    'id' => 'employee-update-dialog',
    'title' => '<h1 class="popup-title title" style="font-size: 20px; text-align: center;">' . Yii::t('profile', 'Редактирование сотрудника') . '</h1>',
    'options' => array('width' => 320),
)); ?>

<div id="employee-update-dialog-content" class="inviteblock">
</div>

<?php $this->endWidget(); ?>

<?php $this->beginWidget('DialogWidget', array(
    'id' => 'employee-delete-dialog',
    'title' => '<h1 class="popup-title title" style="font-size: 20px; text-align: center;">' . Yii::t('profile', 'Увольнение сотрудника') . '</h1>',
    'options' => array('width' => 320),
)); ?>

<div id="employee-delete-dialog-content" class="inviteblock">
    <?php echo Yii::t('profile', 'Вы действительно хотите уволить этого сотрудника?'); ?>
    <div class="buttonswrap pop">
        <?php echo CHtml::link(Yii::t('profile', 'Да'), '#', array('class' => 'chose yes')) ?>
        <?php echo CHtml::link(Yii::t('profile', 'Нет'), '#', array('class' => 'otklonit no')) ?>
    </div>
</div>

<?php $this->endWidget(); ?>