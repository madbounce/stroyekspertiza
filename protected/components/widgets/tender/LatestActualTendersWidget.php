<?php

class LatestActualTendersWidget extends CWidget
{
    public $count = 6;

    public function run()
    {
        $tenders = array();

        if (Yii::app()->user->isGuest) {
            $tenders = Tender::model()->isPublic()->isActual()->findAll(array('limit' => $this->count));
        } else {
            if (Yii::app()->user->checkAccess('userWithProfile'))
                $tenders = Tender::model()->isPublic()->isActual()->hasActivities(CHtml::listData(Yii::app()->user->model->profile->activities, 'id', 'id'))->findAll(array('limit' => $this->count));

            $count = count($tenders);
            if ($count < $this->count) {
                $tenders = array_merge($tenders, Tender::model()->isPublic()->isActual()->except(CHtml::listData($tenders, 'id', 'id'))->findAll(array('limit' => $this->count - $count)));
            }
        }

        $this->render('latestActualTenders', array('tenders' => $tenders));
    }
}
