<h1>
    <?php echo CHtml::link(Yii::t('tender', 'Актуальные публичные тендеры'), '#'); ?>
</h1>

<div class="list_actual_tender">
    <hr>

    <?php foreach ($tenders as $tender): ?>
        <div class="item">
            <div class="name_tyender">
                <?php echo CHtml::link(CHtml::encode($tender->title), array('/tender/view', 'id' => $tender->id)); ?>
            </div>
            <div class="price">
                <span class="gray"><?php echo Yii::t('tender', 'Максимальный бюджет'); ?></span><br>
                <?php echo $tender->formattedCost; ?>
            </div>
        </div>
    <?php endforeach; ?>

    <div class="all_link">
        <?php echo CHtml::link(Yii::t('tender', 'Все тендеры'), '#', array('class' => 'small_btn green_btn')); ?>
    </div>
</div>