<?php

Yii::import('image.components.widgets.*');

class AvatarUploadWidget extends UploadWidget
{
    public $tag = 'avatar';
    public $view = 'avatar';
    public $action = array('/upload/avatar');
    public $multiple = false;

    protected function renderContent()
    {
        echo '<div class="' . $this->id . '">';
        echo '<div class="uploaded-images">';
        $this->renderUploadedImages($this->getImages());
        echo '</div>';
        echo '</div>';
    }
}
