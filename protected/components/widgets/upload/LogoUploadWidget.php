<?php

Yii::import('image.components.widgets.*');

class LogoUploadWidget extends UploadWidget
{
    public $tag = 'logo';
    public $view = 'logo';
    public $multiple = false;

    protected function renderContent()
    {
        echo '<div class="' . $this->id . '">';
        echo '<div class="uploaded-images">';
        $this->renderUploadedImages($this->getImages());
        echo '</div>';
        echo '</div>';
    }
}