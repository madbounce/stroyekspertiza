<div style="margin-bottom: 20px;" class="man_avatar">
    <div id="imgcont">
        <?php if (!empty($images)): ?>
            <?php echo CHtml::image(Yii::app()->baseUrl . ImageHelper::thumb(124, 124, $images[0]->url, array('method' => 'adaptiveResize'))); ?>
        <?php else: ?>
            <?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/pic_avatar.gif', '', array('width' => 124, 'height' => 124, 'class' => 'upload-button')); ?>
        <?php endif; ?>
    </div>

    <?php echo CHtml::link(Yii::t('image', 'Загрузить'), '#', array('class' => 'upload-button green')); ?><br>
    <?php if (!empty($images)): ?>
        <?php echo CHtml::link(Yii::t('image', 'Удалить'), '#', array('class' => 'delete-link red', 'data-id' => $images[0]->id)); ?>
    <?php endif; ?>

    <div class="clear"></div>
</div>
