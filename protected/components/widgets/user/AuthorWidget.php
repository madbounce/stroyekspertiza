<?php

class AuthorWidget extends CWidget
{
    public $model;

    public function run()
    {
        $this->render('author', array('model' => $this->model));
    }
}
