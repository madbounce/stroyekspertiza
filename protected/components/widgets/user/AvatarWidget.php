<?php

class AvatarWidget extends CWidget
{
    public $model;
    public $width;
    public $height;

    public $alt = 'avatar';
    public $htmlOptions = array();

    public function run()
    {
        if (!empty($this->model->avatar))
            echo CHtml::image($this->model->avatar->thumb($this->width, $this->height), $this->alt, $this->htmlOptions);
        // TODO else blank image
    }
}
