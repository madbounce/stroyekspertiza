<?php

class EmailWidget extends CWidget
{
    public function run()
    {
        $model = Yii::app()->user->model;
        $model->scenario = User::SCENARIO_UPDATE_EMAIL;
        $this->render('email', array('model' => $model));
    }
}
