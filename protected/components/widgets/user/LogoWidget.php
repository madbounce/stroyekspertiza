<?php

class LogoWidget extends CWidget
{
    public $model;
    public $width;
    public $height;

    public $alt = 'logo';
    public $htmlOptions = array();

    public function run()
    {
        if (!empty($this->model->logo))
            echo CHtml::image($this->model->logo->thumb($this->width, $this->height), $this->alt, $this->htmlOptions);
        // TODO else blank image
    }
}
