<?php

// TODO При открытии диалог не всегда отображается в центре екрана
class PortfolioWidget extends CWidget
{
    const TYPE_PRIVATE = 0;
    const TYPE_PUBLIC = 1;

    public $model;
    public $type = self::TYPE_PRIVATE;

    public function run()
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.jcarousel.min.js', CClientScript::POS_HEAD);

        Yii::app()->clientScript->registerScript('portfolio-dialog', '
        $("a.portfolio-show-all").live("click", function() {
            ' . CHtml::ajax(array(
                'url' => array('/user/portfolio'),
                'dataType' => 'html',
                'data' => array('id' => $this->model->id),
                'success' => 'js:function(result) {
                    $("#portfolio-dialog #content").html(result);
                    $("#portfolio-dialog").dialog("open");

                    $("#portfolio-carousel").jcarousel({
                        wrap:"circular",
                        scroll:1,
                        visible:1,
                        buttonNextHTML:null,
                        buttonPrevHTML:null,
                        initCallback:function(carousel) {
                            jQuery(".jcarousel-control a").bind("click", function () {
                                carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
                                $(".jcarousel-control a").each(function () {
                                    $(this).removeClass("active");
                                });
                                $(this).addClass("active");
                                return false;
                            });
                        }
                    });
                }'
            )) .'
            return false;
        });
        $("a.portfolio-show").live("click", function() {
            var id = $(this).attr("data-id");
            ' . CHtml::ajax(array(
                'url' => array('/user/portfolio'),
                'dataType' => 'html',
                'data' => array('id' => $this->model->id, 'portfolio_id' => 'js:id'),
                'success' => 'js:function(result) {
                        $("#portfolio-dialog #content").html(result);
                        $("#portfolio-dialog").dialog("open");
                    }'
            )) .'
            return false;
        });
        ', CClientScript::POS_READY);
        $this->render('portfolio', array('model' => $this->model));
    }
}
