<?php

class RegionWidget extends CInputWidget
{
    public $addRegion = true;

    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        if ($this->hasModel()) {
            $this->registerScripts($id);

            $currentRegion = Region::model()->findByPk($this->model->{$this->attribute});

            $this->render('region', array(
                'items' => Region::model()->parent()->with(array('regions.regions' => array('alias' => 'subregions')))->findAll(),
                'currentRegion' => $currentRegion,
                'model' => new Region,
            ));
        } else
            throw new CException('RegionWidget.model and RegionWidget.attribute must be set.');
    }

    protected function registerScripts($id)
    {
        Yii::app()->clientScript->registerScript(__CLASS__ . "#{$this->id}-region-select", '
            $("#' . $this->id . ' a.level").live("click", function() {
                $(this).next("ul.level").slideDown();
                $("#' . $this->id . ' li.level").removeClass("current");
                $(this).parents("li.level").addClass("current");
                $("#' . $this->id . ' li:not(.current) > ul").slideUp();
                $("#' . $this->id . ' li.current > ul").slideDown();
                $("#' . $id . '").val($(this).attr("data-id"));
                return false;
            });
            $("#' . $this->id . ' input.select-region").click(function() {
                $(".' . $this->id . ' span.selected-region").html($("#' . $this->id . ' a.level[data-id=" + $("#' . $id . '").val() + "]").html());
                $(".' . $this->id . ' a.change-region").html($(".' . $this->id . ' a.change-region").attr("data-title"));
                $("#' . $this->id . '").dialog("close");
                return false;
            });
            $("#' . $this->id . ' a.level[data-id=' . $this->model->{$this->attribute} . ']").click();

            $("a.add-region").live("click", function() {
                $("div.select-region").hide();
                $("div.add-region").show();
                return false;
            });
            $("a.select-region").live("click", function() {
                $("div.add-region").hide();
                $("div.select-region").show();
                return false;
            });
            $("#' . $this->id . ' .add-region input[type=submit]").live("click", function() {
                ' . CHtml::ajax(array(
                    'url' => array('/region/create'),
                    'data' => 'js:$("#' . $this->id . ' .add-region input, #' . $this->id . ' .add-region select").serialize()',
                    'dataType' => 'json',
                    'type' => 'post',
                    'success' => 'js:function(data) {
                        if (data.success) {
                            $("#' . $id . '").val(data.id);
                            $(".' . $this->id . ' span.selected-region").html(data.name);
                            $(".' . $this->id . ' a.change-region").html($(".' . $this->id . ' a.change-region").attr("data-title"));
                            $("#' . $this->id . '").dialog("close");
                        }
                    }',
                )) . '
                return false;
            });
            ', CClientScript::POS_READY);
    }
}
