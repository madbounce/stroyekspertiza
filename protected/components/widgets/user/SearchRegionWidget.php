<?php

class SearchRegionWidget extends CInputWidget
{
    public function run()
    {
        list($name, $id) = $this->resolveNameID();

        if ($this->hasModel()) {
            $this->registerScripts($id);

            $currentRegion = Region::model()->findByPk($this->model->{$this->attribute});

            $this->render('searchRegion', array(
                'items' => Region::model()->parent()->with(array('regions.regions' => array('alias' => 'subregions')))->findAll(),
                'currentRegion' => $currentRegion,
                'model' => new Region,
            ));
        } else
            throw new CException('RegionWidget.model and RegionWidget.attribute must be set.');
    }

    protected function registerScripts($id)
    {
        Yii::app()->clientScript->registerScript(__CLASS__ . "#{$this->id}-region-select", '
            $("#' . $this->id . ' a.level").live("click", function() {
                $(this).next("ul.level").slideDown();
                $("#' . $this->id . ' li.level").removeClass("current");
                $(this).parents("li.level").addClass("current");
                $("#' . $this->id . ' li:not(.current) > ul").slideUp();
                $("#' . $this->id . ' li.current > ul").slideDown();
                $("#' . $id . '").val($(this).attr("data-id"));
                return false;
            });
            $("#' . $this->id . ' input.select-region").click(function() {
                $(".' . $this->id . ' a.change-region").html($("#' . $this->id . ' a.level[data-id=" + $("#' . $id . '").val() + "]").html());
                $("#' . $this->id . '").dialog("close");
                return false;
            });
            $("#' . $this->id . ' a.level[data-id=' . $this->model->{$this->attribute} . ']").click();
            ', CClientScript::POS_READY);
    }
}
