<?php

class UpdateInfoWidget extends CWidget
{
    public function run()
    {
        $model = Yii::app()->user->model;
        $model->scenario = User::SCENARIO_UPDATE_INFO;
        $this->render('updateInfo', array('model' => $model));
    }
}
