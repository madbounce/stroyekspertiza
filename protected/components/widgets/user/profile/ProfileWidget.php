<?php

class ProfileWidget extends CWidget
{
    const TYPE_PRIVATE = 0;
    const TYPE_PUBLIC = 1;

    public $user;
    public $type = self::TYPE_PRIVATE;

    public function run()
    {
        $model = $this->type == self::TYPE_PRIVATE ? Yii::app()->user->model : $this->user;
        $this->render('profile', array(
            'model' => $model,
        ));
    }
}
