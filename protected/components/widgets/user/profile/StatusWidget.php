<?php

class StatusWidget extends CInputWidget
{
    public $grid;

    public function run()
    {
        if ($this->hasModel()) {
            list($name, $id) = $this->resolveNameID();

            $this->registerScripts($id);
            $this->render('status', array(
                'items' => array(
                    Helper::STATUS_ACTUAL => Yii::t('app', 'Актуальные'),
                    Helper::STATUS_ENDED => Yii::t('app', 'Завершенные'),
                    Helper::STATUS_CANCELED => Yii::t('app', 'Отмененные'),
                ),
            ));
        } else
            throw new CException('StatusWidget.model and StatusWidget.attribute must be set.');
    }

    protected function registerScripts($id)
    {
        Yii::app()->clientScript->registerScript(__CLASS__ . '-' . $this->id, '
            $("#' . $this->id . ' a.status-link").live("click", function() {
                $("input#' . $id . '").val($(this).attr("data-id"));
                var link = $(this);
                $.fn.yiiGridView.update("' . $this->grid . '", {
                    data: $("#' . $this->id . ' input").serialize(),
                    complete: function(jqXHR, status) {
                        if (status == "success"){
                            $("#' . $this->id . ' a.status-link").parent("li").removeClass("current");
                            link.parent("li").addClass("current");
                        }
                    }
                });
                return false;
            });
        ', CClientScript::POS_READY);
    }
}
