<script type="text/javascript">
    $(function() {
        var viewModel = function() {
            this.hidden = ko.observable(<?php echo CJavaScript::encode(Yii::app()->user->getState('profileHidden', false)); ?>);

            this.changeVisibility = function() {
                this.hidden(!this.hidden());
            <?php echo CHtml::ajax(array(
                'url' => array('/profile/changeVisibility'),
            )); ?>
            };
        };

        ko.applyBindings(new viewModel());
    });
</script>

<div class="block_man" data-bind="visible: !hidden()">
    <div class="floatLeft">
        <?php $this->widget('AvatarWidget', array('model' => $model, 'width' => 124, 'height' => 124)); ?>
    </div>

    <div class="floatLeft block_man_info">
        <big><?php echo CHtml::encode($model->full_name); ?></big>
        <div class="mar15">
            <?php if (isset($model->profile) && !empty($model->profile->region)): ?>
                <?php echo Yii::t('profile', 'регион: :region', array(':region' => CHtml::encode($model->profile->region->name))); ?><br>
            <?php endif; ?>

            <?php if (!empty($model->email)): ?>
                <?php echo Yii::t('profile', 'e-mail: :email', array(':email' => CHtml::encode($model->email))); ?><br>
            <?php endif; ?>

            <?php if (!empty($model->contact_phone)): ?>
                <?php echo Yii::t('profile', 'телефон: :phone', array(':phone' => CHtml::encode($model->contact_phone))); ?><br>
            <?php endif; ?>

            <!--<?php //echo Yii::t('profile', 'регион:'); ?>&nbsp;<?php //$this->widget('ProfileRegionWidget', array('model' => $model, 'attribute' => 'region_id')); ?>-->
        </div>

        <?php echo CHtml::link(Yii::t('profile', 'Редактировать<br> личную информацию'), '#', array('onclick' => '$("#update-info-dialog").dialog("open"); return false;')); ?>
    </div>
    <div class="clear"></div>
</div>

<div class="block_man littleHeader" data-bind="visible: hidden()" style="display:block;">
    <?php $this->widget('AvatarWidget', array('model' => $model, 'width' => 28, 'height' => 28)); ?>
    <div class="maninfo ">
        <div class="maninfoname"><?php echo CHtml::encode($model->full_name); ?></div>
        <?php echo CHtml::link(Yii::t('profile', 'Редактировать<br> личную информацию'), '#', array('onclick' => '$("#update-info-dialog").dialog("open"); return false;')); ?>
    </div>
    <div class="clear"></div>
</div>

<div style="display: none">
    <?php $this->widget('UpdateInfoWidget'); ?>
</div>