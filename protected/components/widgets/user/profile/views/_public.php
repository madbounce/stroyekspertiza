<?php if ($model->id != Yii::app()->user->id): ?>
    <?php // TODO Кнопки ?>
    <div class="lnk_edit_profile">
        <?php echo CHtml::link(Yii::t('profile', 'Добавить в подрядчики'), '#', array('class' => 'but_blue')); ?><br>
        <?php echo CHtml::link(Yii::t('profile', 'Предложить услуги'), '#', array('class' => 'but_green')); ?><br>
        <?php echo CHtml::link(Yii::t('profile', 'Пригласить в тендер'), '#', array('class' => 'but_grey')); ?>
    </div>
<?php endif; ?>

<div class="block_man block_man_fl_l" data-bind="visible: !hidden()">
    <div class="man_rating_public">3.5<?php // TODO Рейтинг ?></div>

    <div class="floatLeft">
        <?php $this->widget('LogoWidget', array('model' => $model->profile, 'width' => 124, 'height' => 124)); ?>
    </div>

    <div class="floatLeft block_man_info">
        <?php if ($model->isFreelancer): ?>
            <big><?php echo CHtml::encode($model->full_name); ?></big>
            <div class="mar15">
                <?php if (!empty($model->profile->region)): ?>
                    <?php echo Yii::t('profile', 'регион: :region', array(':region' => CHtml::encode($model->profile->region->name))); ?><br>
                <?php endif; ?>

                <?php if (!empty($model->profile->website)): ?>
                    <?php echo Yii::t('profile', 'сайт: :website', array(':website' => CHtml::encode($model->profile->website))); ?><br>
                <?php endif; ?>

                <?php if (!empty($model->email)): ?>
                    <?php echo Yii::t('profile', 'e-mail: :email', array(':email' => CHtml::encode($model->email))); ?><br>
                <?php endif; ?>

                <?php if (!empty($model->profile->phone)): ?>
                    <?php echo Yii::t('profile', 'телефон: :phone', array(':phone' => CHtml::encode($model->profile->phone))); ?><br>
                <?php endif; ?>
            </div>
        <?php elseif ($model->isCompany): ?>
            <big><?php echo CHtml::encode($model->profile->name); ?></big>
            <div class="mar15">
                <?php if (!empty($model->profile->region)): ?>
                    <?php echo Yii::t('profile', 'регион: :region', array(':region' => CHtml::encode($model->profile->region->name))); ?><br>
                <?php endif; ?>

                <?php if (!empty($model->profile->website)): ?>
                    <?php echo Yii::t('profile', 'сайт: :website', array(':website' => CHtml::encode($model->profile->website))); ?><br>
                <?php endif; ?>

                <?php if (!empty($model->profile->email)): ?>
                    <?php echo Yii::t('profile', 'e-mail: :email', array(':email' => CHtml::encode($model->profile->email))); ?><br>
                <?php endif; ?>

                <?php if (!empty($model->profile->phone)): ?>
                    <?php echo Yii::t('profile', 'телефон: :phone', array(':phone' => CHtml::encode($model->profile->phone))); ?><br>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>