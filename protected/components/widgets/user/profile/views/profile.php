<div class="block_manprofile" data-bind="css: { 'closed': hidden }">

    <?php if ($this->type == ProfileWidget::TYPE_PRIVATE): ?>
        <div class="lnk_turn" id="hideHeaderLink" data-bind="visible: !hidden()">
            <?php echo CHtml::link(Yii::t('profile', 'Свернуть'), '#', array('data-bind' => 'click: changeVisibility')); ?>
        </div>
        <div class="lnk_turn_razv" id="showHeaderLink" data-bind="visible: hidden()">
            <?php echo CHtml::link(Yii::t('profile', 'Развернуть'), '#', array('data-bind' => 'click: changeVisibility')); ?>
        </div>
    <?php endif; ?>

    <div class="fix_page">
        <?php if ($this->type == ProfileWidget::TYPE_PRIVATE): ?>
            <?php $this->render('_private', array('model' => $model)); ?>
        <?php else: ?>
            <?php $this->render('_public', array('model' => $model)); ?>
        <?php endif; ?>
    </div>
</div>

<div class="block_profile<?php echo $this->type == ProfileWidget::TYPE_PUBLIC ? '_public' : ''; ?>">
    <?php if ($this->type == ProfileWidget::TYPE_PRIVATE): ?>
        <div class="fix_page">

            <?php
                $items = array(
                    array('label' => Yii::t('profile', 'Профиль'), 'url' => array('/profile/index')),
                    array('label' => Yii::t('profile', 'Мои тендеры (:count)',
                        array(':count' => $model->isCompany ? count($model->tenders) + count($model->profile->tenders) : count($model->tenders))),
                        'url' => array('/profile/tenders')),
                );

                if (Yii::app()->user->checkAccess('userWithProfile')) {
                    $items[] = array('label' => Yii::t('profile', 'Мои заявки (:count)', array(':count' => count($model->bids))), 'url' => array('/profile/bids'));
                }
            ?>

            <?php $this->widget('zii.widgets.CMenu', array(
                'items' => $items,
                'activeCssClass' => 'current',
                'htmlOptions' => array(
                    'class' => 'menu_profile'
                ),
            )); ?>

            <div class="lnk_create">
                <?php echo CHtml::link(Yii::t('profile', 'Создать тендер'), array('/tender/create')); ?>
            </div>
            <div class="clear"></div>
        </div>
    <?php endif; ?>
</div>