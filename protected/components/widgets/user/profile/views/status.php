<ul id="<?php echo $this->id; ?>" class="menu_profile gray">
    <?php foreach ($items as $id => $item): ?>
        <?php $link = CHtml::link($item, '#', array('class' => 'status-link', 'data-id' => $id)); ?>

        <?php if ($id == $this->model->{$this->attribute}): ?>
            <?php echo CHtml::tag('li', array('class' => 'current'), $link); ?>
        <?php else: ?>
            <?php echo CHtml::tag('li', array(), $link); ?>
        <?php endif; ?>
    <?php endforeach; ?>

    <?php echo CHtml::activeHiddenField($this->model, $this->attribute); ?>
</ul>