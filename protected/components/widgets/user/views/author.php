<div class="tenderrighttopblock">
    <div class="company_rating">
        <div class="man_rating">3.5<?php // TODO Рейтинг ?></div>
    </div>

    <?php if ($model->isCompany): ?>
        <div class="tendercompanytop">
            <?php $this->widget('LogoWidget', array('model' => $model->profile, 'width' => 74, 'height' => 65)); ?>
            <?php echo CHtml::link(CHtml::encode($model->profile->name), Helper::profileUrl($model->profile), array('class' => 'name-tendercompany')); ?>
        </div>

        <div class="tendercompanybottom">
            <?php $this->widget('AvatarWidget', array('model' => $model, 'width' => 76, 'height' => 76)); ?>
            <div class="tendercompanyinfo">
                <p>
                    <?php echo CHtml::encode($model->full_name); ?>
                    <?php if (!empty($model->company_post)): ?>
                        <span class="small"><?php echo CHtml::encode($model->company_post); ?></span>
                    <?php endif; ?>
                </p>
                <p>
                    <?php if (!empty($model->contact_phone)): ?>
                        <?php echo CHtml::encode($model->contact_phone); ?>
                    <?php endif; ?>
                    <?php echo CHtml::mailto(CHtml::encode($model->email), $model->email); ?>
                </p>
            </div>
        </div>
    <?php else: ?>
        <div class="tendercompanybottom">
            <?php if ($model->isFreelancer): ?>
                <?php $this->widget('LogoWidget', array('model' => $model->profile, 'width' => 76, 'height' => 76)); ?>
            <?php else: ?>
                <?php $this->widget('AvatarWidget', array('model' => $model->profile, 'width' => 76, 'height' => 76)); ?>
            <?php endif; ?>
            <div class="tendercompanyinfo">
                <p>
                    <?php echo CHtml::encode($model->full_name); ?>
                    <?php if (!empty($model->profile->about)): ?>
                        <span class="small"><?php echo CHtml::encode($model->profile->about); ?></span>
                    <?php endif; ?>
                </p>
                <p>
                    <?php if (!empty($model->contact_phone)): ?>
                        <?php echo CHtml::encode($model->contact_phone); ?>
                    <?php endif; ?>
                    <?php echo CHtml::mailto(CHtml::encode($model->email), $model->email); ?>
                </p>
            </div>
        </div>
    <?php endif; ?>
</div>