<?php $this->beginWidget('DialogWidget', array(
    'id' => 'email-dialog',
    'title' => '<h1 class="popup-title title" style="font-size: 20px; text-align: center;">' . Yii::t('profile', 'Введите Ваш E-mail') . '</h1>',
    'autoOpen' => true,
    'closeable' => false,
)); ?>

<div class="inviteblock">

    <?php /** @var $form CActiveForm */ ?>
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'email-form',
        'action' => array('/profile/updateEmail'),
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )); ?>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'email', array('class' => 'l1')); ?>
        <?php echo $form->textField($model, 'email', array('class' => 'input200')); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <?php echo CHtml::link(Yii::t('profile', 'сохранить'), '#', array('class' => 'green_btn', 'submit' => '')) ?>

    <?php $this->endWidget(); ?>

</div>

<?php $this->endWidget(); ?>