<?php if (!empty($model->portfolios)): ?>

    <?php if ($this->type == PortfolioWidget::TYPE_PUBLIC): ?>
        <?php echo CHtml::link(Yii::t('profile', 'Все работы'), '#', array('class' => 'all_work portfolio-show-all')); ?>
    <?php endif; ?>
    <h2><?php echo Yii::t('profile', 'Портфолио'); ?></h2>

    <?php foreach ($model->portfolios(array('limit' => 5, 'order' => 'create_time DESC')) as $portfolio): ?>
    <div class="block_portfolio">
        <?php if ($this->type == PortfolioWidget::TYPE_PRIVATE): ?>
            <?php echo CHtml::image($portfolio->thumb(300, 137)); ?>
            <?php echo CHtml::link(CHtml::encode($portfolio->description), '#', array('class' => 'portfolio-show', 'data-id' => $portfolio->id)); ?>
            <div class="clear"></div>
        <?php else: ?>
            <?php echo CHtml::image($portfolio->thumb(300, 137)); ?>
            <div class="clear"></div>
            <?php echo CHtml::link(CHtml::encode($portfolio->description), '#', array('class' => 'portfolio-show', 'data-id' => $portfolio->id)); ?>
            <div class="clear"></div>
        <?php endif; ?>
    </div>
    <?php endforeach; ?>

    <?php $this->beginWidget('DialogWidget', array(
        'id' => 'portfolio-dialog',
        'title' => '',
        'autoOpen' => false,
        'options' => array('width' => 'auto', 'position' => 'js:["middle", 60]')
    )); ?>

    <div id="content" style="width: 533px;"></div>

    <?php $this->endWidget(); ?>

<?php endif; ?>