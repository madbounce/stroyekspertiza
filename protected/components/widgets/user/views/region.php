<span class="<?php echo $this->id; ?>">
    <span data-id="<?php echo !empty($currentRegion) ? $currentRegion->id : ''; ?>" class="selected-region">
        <?php echo !empty($currentRegion) ? Yii::t('region', CHtml::encode($currentRegion->name)) : ''; ?>
    </span>

    <?php echo CHtml::link(Yii::t('profile', !empty($currentRegion) ? 'сменить' : 'выбрать'), '#', array(
        'data-id' => !empty($currentRegion) ? $currentRegion->id : '',
        'onclick' => '$("#' . $this->id . '").dialog("open"); return false;',
        'class' => 'lnk_change_region change-region',
        'data-title' => Yii::t('profile', 'сменить регион')
    )); ?>

    <?php echo CHtml::activeHiddenField($this->model, $this->attribute, $this->htmlOptions); ?>
</span>

<div style="display: none">

    <?php $this->beginWidget('DialogWidget', array(
        'id' => $this->id,
        'title' => '<h1 class="popup-title title" style="font-size: 20px; text-align: center;">' . Yii::t('profile', 'Выберите регион') . '</h1>',
        'options' => array(
            'position' => 'js:["middle", 60]'
        ),
    )); ?>

    <div class="select-region">
        <ul class="menu_daidjest">
            <?php foreach ($items as $item): ?>
                <li class="first_level_it level">
                    <?php echo CHtml::link(Yii::t('region', $item->name), '#', array('class' => 'first_level countryLink level', 'data-id' => $item->id)); ?>
                    <ul class="second_dj level">
                        <?php foreach ($item->regions as $item2): ?>
                        <li class="second_level_it level">
                            <?php echo CHtml::link(Yii::t('region', $item2->name), '#', array('class' => 'second_level regionLink level', 'data-id' => $item2->id)); ?>
                            <ul class="third_dj level">
                                <?php foreach ($item2->regions as $item3): ?>
                                    <li class="third_level_it level">
                                        <?php echo CHtml::link(Yii::t('region', $item3->name), '#', array('class' => 'third_level cityLink level', 'data-id' => $item3->id)); ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ul>

        <br>
        <?php echo CHtml::button(Yii::t('profile', 'Выбрать'), array('class' => 'select-region small_btn green_btn')); ?>
        <?php if ($this->addRegion): ?>
            <?php echo CHtml::link(Yii::t('profile', 'Свой вариант'), '#', array('class' => 'add-region')); ?>
        <?php endif; ?>
    </div>

    <?php if ($this->addRegion): ?>
        <div class="add-region" style="display: none;">
            <div class="formline1">
                <?php echo CHtml::activeLabelEx($model, 'parent_id', array('class' => 'l1')); ?>
                <?php echo CHtml::activeDropDownList($model, 'parent_id', Region::getItemOptions(Misc::t('Нет')), array('encode' => false, 'class' => 'input200')); ?>
            </div>

            <div class="formline1">
                <?php echo CHtml::activeLabelEx($model, 'name', array('class' => 'l1')); ?>
                <?php echo CHtml::activeTextField($model, 'name', array('class' => 'input200')); ?>
                <?php echo CHtml::error($model, 'name'); ?>
            </div>

            <br/>
            <?php echo CHtml::submitButton(Yii::t('profile', 'Выбрать'), array('class' => 'small_btn green_btn')); ?>
            <?php echo CHtml::link(Yii::t('profile', 'К выбору региона'), '#', array('class' => 'select-region')); ?>
        </div>
    <?php endif; ?>

    <?php $this->endWidget(); ?>

</div>