<?php $this->beginWidget('DialogWidget', array(
    'id' => 'update-info-dialog',
    'title' => '<h1 class="popup-title title" style="font-size: 20px; text-align: center;">' . Yii::t('profile', 'Введите личные данные') . '</h1>',
    'options' => array('width' => 320),
)); ?>

<?php $this->controller->renderPartial('//profile/_updateInfo', array('model' => $model)); ?>

<?php $this->endWidget(); ?>