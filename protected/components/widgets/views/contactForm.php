<div class="error_message" style="width: 926px; display: none"><?php echo Misc::t('Пожалуйста, заполните все поля формы.'); ?></div>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'contact-form',
    'action' => $this->action,
    'htmlOptions' => array('class' => 'contact-form'),
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
        'afterValidate'=>"js:function(form, data, hasError) {
                    if (hasError) {
                        $(form).parent().parent().find('.error_message').show();
                        return false;
                    } else {
                        $(form).parent().parent().find('.error_message').hide();
                        return true;
                    }
                }"
    )
)); ?>
    <div class="left_form_block">
        <div class="contact-form-line">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('class' => 'input_text', 'placeholder' => Misc::t(''))); ?>
            <?php echo $form->error($model, 'name', array('hideErrorMessage' => $this->hideErrorMessage)); ?>
        </div>
        <div class="contact-form-line">
            <?php echo $form->labelEx($model, 'email'); ?>
            <?php echo $form->emailField($model, 'email', array('class' => 'input_text', 'placeholder' => Misc::t(''))); ?>
            <?php echo $form->error($model, 'email', array('hideErrorMessage' => $this->hideErrorMessage)); ?>
        </div>
        <div class="contact-form-line">
            <?php echo $form->labelEx($model, 'phone'); ?>
            <?php echo $form->textField($model, 'phone', array('class' => 'input_text', 'onfocus'=>'if (this.placeholder==\'+7 (xxx) xxx-xxxx\')this.placeholder=\'\';', 'onblur'=>'if (this.placeholder!=\'+7 (xxx) xxx-xxxx\')this.placeholder=\'+7 (xxx) xxx-xxxx\';', 'placeholder' => Misc::t('+7 (xxx) xxx-xxxx'))); ?>
            <?php echo $form->error($model, 'phone', array('hideErrorMessage' => $this->hideErrorMessage)); ?>
        </div>
    </div>
    <div class="right_form_block">

        <div class="contact-form-line input_block department-drop">
            <?php echo $form->labelEx($model, 'department'); ?>
            <?php echo $form->dropDownList($model, 'department', ContactForm::getDepartmens(), array('class' => 'select', 'empty' => Misc::t(''))); ?>
            <?php echo $form->error($model, 'department', array('hideErrorMessage' => $this->hideErrorMessage)); ?>
        </div>

        <div class="contact-form-line input_block">
            <?php echo $form->labelEx($model, 'reason'); ?>
            <?php echo $form->textField($model, 'reason', array('class' => 'input_text', 'placeholder' => Misc::t(''))); ?>
            <?php echo $form->error($model, 'reason', array('hideErrorMessage' => $this->hideErrorMessage)); ?>
        </div>

        <div class="contact-form-line big_input_block">
            <?php echo $form->labelEx($model, 'text'); ?>
            <?php echo $form->textArea($model, 'text', array('placeholder' => Misc::t(''))); ?>
            <?php echo $form->error($model, 'text', array('hideErrorMessage' => $this->hideErrorMessage)); ?>
        </div>

        <p class="note"><?php echo Misc::t('все поля обязательны для заполнения'); ?></p>
        <?php echo CHtml::submitButton(Misc::t('Отправить'), array('class' => 'send_btn')); ?>
    </div>
<?php $this->endWidget(); ?>