<?php
echo '<div class="language_block">';
foreach (Yii::app()->UrlManager->listLanguage() as $language => $languageUrl) {
    if (Yii::app()->language == $language) {
        echo CHtml::link('', $languageUrl, array('class' => 'active ' . $language . '_flag'));
    } else {
        echo CHtml::link('', $languageUrl, array('class' => $language . '_flag'));
    }
}
echo '</div>';

?>