<div class="likebox" style="overflow: hidden; float: right;">
    <div class="facebook" style="overflow: hidden;margin: 2px 6px; float: left;">
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/<?php echo LocaleHelper::getLocaleId(Yii::app()->language); ?>/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-like" data-href="<?php echo $this->url; ?>" data-send="false" data-width="450" data-show-faces="false" data-layout="button_count"></div>
    </div>
    <div class="vkontakte" style="overflow: hidden;   margin: 1px 10px 2px 0; float: left;">
        <!-- VKOntakte -->
        <script type="text/javascript"><!--
        document.write(VK.Share.button(false,{type: "button", text: "<?php echo Misc::t('Мне нравится')?>"}));
        --></script>
    </div>
    <div class="twitter" style="overflow: hidden; margin: 2px 5px; float: left; width: 100px;">
        <a href="https://twitter.com/share" class="twitter-share-button" data-lang="<?php echo Yii::app()->language; ?>"><?php echo Misc::t('Твитнуть')?></a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    </div>
    <div class="google-plus" style="overflow: hidden;  margin: 2px 5px; float: left;">
        <g:plusone size="medium"></g:plusone>
    </div>
    <!-- Google + -->
    <script type="text/javascript" src="https://apis.google.com/js/plusone.js">
        {lang: 'ru'}
    </script>
</div>