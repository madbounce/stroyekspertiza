<?php $this->beginWidget('DialogWidget', array(
    'id' => 'message-dialog',
    'title' => !empty($message['header']) ? CHtml::encode($message['header']) : '',
    'autoOpen' => $autoOpen,
    'options' => array(
        'width' => 525
    ),
    'close' => 5000,
)); ?>

<div class="message_text">
    <?php if (!empty($message['message'])): ?>
        <?php echo $message['message']; ?>
    <?php endif; ?>
</div>

<?php $this->endWidget(); ?>