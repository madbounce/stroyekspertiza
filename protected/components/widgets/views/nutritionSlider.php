<?php if($model):?>
<div class="backforwardpage2 interest">
    <h2 class="heading">Интересные статьи
            <span>
             <a href="javascript:void(0)" title="Предыдущие статьи">
                 <img title="Предыдущие статьи" id="imgprev1" style="cursor: pointer;" src="/images/n_backpage.gif" alt="Предыдущие товары">
             </a>
                <a href="javascript:void(0)" title="Следующие статьи">
                    <img title="Следующие статьи" id="imgnext1" style="cursor: pointer;" src="/images/n_nextpage.gif" alt="Следующие товары">
                </a>
            </span>

    </h2>
</div>

<div class="rolloversection" style="position: relative;display:none;" id="car2">

    <div class="stepcarousel_new" id="mygallery2">
        <div class="belt2">
        <ul class="carousel ubelt2"  style="overflow: visible !important;">
            <?php foreach($model as $data):?>
            <li>
            <div class="panel_1">
            	
                <? $url = '/article/view/id/' . $data->slug; //Yii::app()->createUrl('/catalog/default/view', array('id' => $data->slug))?>
                <div class="img"><a href="<?=$url?>">
                					<?php
									$name = $data->title;
									$photo = $data->getPhoto(260, 120, array('method' => 'resize'));
									if (!empty($photo))
										$image = CHtml::image($photo, $name, array('width' => 260, 'height' => 120));
									else
										$image = CHtml::image(Yii::app()->theme->baseUrl . '/images/no2.jpg', '', array('width' => 260, 'height' => 120));
					
									echo $image;
									?>
                                 </a></div>
                <div class="descr">
                    <h6><?=$name?></h6>
                    <p><? echo str_replace(array('<p>','</p>',),'',truncate_str(strip_tags($data->content),100)).' ...';?></p>
                    
                </div>
                <a href="<?=$url?>">Читать &raquo;</a>
            </div>
            </li>
			<?php endforeach; ?>
        </ul>
        </div>
    </div>
    <div class="clear-all">&nbsp;</div>
</div>

<?php endif;?>