<?php if($model):?>
<div class="backforwardpage2">
    <h2 class="heading">Рекомендуемые товары
            <span>
             <a href="javascript:void(0)" title="Предыдущие товары">
                 <img title="Предыдущие товары" id="imgprev" style="cursor: pointer;" src="/images/n_backpage.gif" alt="Предыдущие товары">
             </a>
                <a href="javascript:void(0)" title="Следующие товары">
                    <img title="Следующие товары" id="imgnext" style="cursor: pointer;" src="/images/n_nextpage.gif" alt="Следующие товары">
                </a>
            </span>

    </h2>
</div>

<div class="d-carousel">
    <ul class="carousel product-list"">
        <?php foreach($model as $data):?>
        <li>
            <?
            $name = substr($data->name,4);
            $url = Yii::app()->createUrl('/catalog/default/view', array('url' => $data->url));
            ?>
            <div class="productcontainer" alt="<?=$name?>" title="<?=$name?>" onclick="window.location.href='<?php echo $url; ?>'">
                <!-- ссылкa с англ.сайта -->
                <?php
                $photo = $data->getPhoto(280, 0, array('method' => 'resize'));
                if (!empty($photo))
                    $image = CHtml::image($photo, $name, array('width' => 118, 'height' => 161));
                else
                    $image = CHtml::image(Yii::app()->theme->baseUrl . '/images/no2.jpg', '', array('width' => 118, 'height' => 161));

                echo $image;
                ?>

                <a href="<?=$url?>" alt="<?=$name?>" title="<?=$name?>"><strong><?=$name?></strong></a>
                <div class="hovercontent">
                    <div>
                        <a href="<?=$url?>" alt="<?=$name?>" title="<?=$name?>"><strong><?=$name?></strong></a>
                        <br><br>
                        <?=$data->composition?>
                        <br><br>
                        <a href="<?=$url?>" alt="<?=$name?>" title="<?=$name?>">
                            Подробнее&nbsp;
                            <img src="/images/cat_arrow_2.gif" alt="">
                        </a>
                    </div>
                </div>
            </div>

        </li>
        <?php endforeach; ?>
    </ul>
</div>
<?php endif;?>