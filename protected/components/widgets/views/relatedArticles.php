<div class="yellow-box">
    <div>
        <p><strong>Статьи о кошках</strong></p>
        <ul>
            <?php foreach($items as $data):?>
            <li>
                <?php echo CHtml::link($data->title, '/article/view/id/' . $data->slug, array('title' => $data->title))?>
            </li>
            <?php endforeach;?>
        </ul>
    </div>
</div>