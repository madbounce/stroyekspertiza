<?php
return array (
  'viewTender' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'createTender' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'updateTender' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => 'return Yii::app()->user->model->canUpdateTender($params["tender"]);',
    'data' => NULL,
  ),
  'manageTender' => 
  array (
    'type' => 1,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'viewTender',
      1 => 'createTender',
      2 => 'updateTender',
    ),
  ),
  'createBid' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => 'return Yii::app()->user->model->canCreateBid($params["tender"]);',
    'data' => NULL,
  ),
  'updateBid' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => 'return Yii::app()->user->model->canUpdateBid($params["bid"]);',
    'data' => NULL,
  ),
  'cancelBid' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => 'return Yii::app()->user->model->canCancelBid($params["bid"]);',
    'data' => NULL,
  ),
  'acceptBid' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => 'return Yii::app()->user->model->canAcceptBid($params["bid"]);',
    'data' => NULL,
  ),
  'manageBid' => 
  array (
    'type' => 1,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'createBid',
      1 => 'updateBid',
      2 => 'cancelBid',
      3 => 'acceptBid',
    ),
  ),
  'createEmployee' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'updateEmployee' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => 'return Yii::app()->user->model->canUpdateEmployee($params["employee"]);',
    'data' => NULL,
  ),
  'deleteEmployee' => 
  array (
    'type' => 0,
    'description' => '',
    'bizRule' => 'return Yii::app()->user->model->canDeleteEmployee($params["employee"]);',
    'data' => NULL,
  ),
  'manageCompany' => 
  array (
    'type' => 1,
    'description' => '',
    'bizRule' => 'return Yii::app()->user->model->canManageCompany($params["profile"]);',
    'data' => NULL,
    'children' => 
    array (
      0 => 'createEmployee',
      1 => 'updateEmployee',
      2 => 'deleteEmployee',
    ),
  ),
  'unconfirmedUser' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => 'return !Yii::app()->user->isGuest && (empty(Yii::app()->user->model->email) || Yii::app()->user->model->status == User::STATUS_NOT_CONFIRMED);',
    'data' => NULL,
    'children' => 
    array (
      0 => 'manageTender',
    ),
  ),
  'user' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => 'return !Yii::app()->user->isGuest && (!empty(Yii::app()->user->model->email) && Yii::app()->user->model->status == User::STATUS_CONFIRMED);',
    'data' => NULL,
    'children' => 
    array (
      0 => 'manageTender',
      1 => 'manageBid',
    ),
  ),
  'userWithProfile' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => 'return !Yii::app()->user->isGuest && !empty(Yii::app()->user->model->profile);',
    'data' => NULL,
    'children' => 
    array (
      0 => 'user',
    ),
  ),
  'company' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => 'return !Yii::app()->user->isGuest && !empty(Yii::app()->user->model->profile) && Yii::app()->user->model->isCompany;',
    'data' => NULL,
    'children' => 
    array (
      0 => 'userWithProfile',
      1 => 'manageCompany',
    ),
  ),
  'administrator' => 
  array (
    'type' => 2,
    'description' => '',
//    'bizRule' => 'return Yii::app()->endName == "back" && !Yii::app()->user->isGuest && Yii::app()->user->id == 1;',
	'bizRule' => 'return Yii::app()->endName == "back" && !Yii::app()->user->isGuest;',
    'data' => NULL,
  ),
);
