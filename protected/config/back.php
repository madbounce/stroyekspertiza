<?php

return CMap::mergeArray(
    require(dirname(__FILE__) . '/main.php'),
    array(
        'name' => 'Панель управления',

        'homeUrl' => array('/'),

        'preload' => array('bootstrap'),

        'components' => array(
            'urlManager' => array(
				'class' => 'CUrlManager',
                'urlFormat' => 'path',
				'showScriptName' => false,
                'rules' => array(
                    'backend' => '/',

                    'backend/<module>/<controller:\w+>/<id:\d+>' => '<module>/<controller>/view',
                    'backend/<module>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                    'backend/<module>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',

                    'backend/<controller:\w+>/<id:\d+>' => '<controller>/view',
                    'backend/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    'backend/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                ),
            ),
            'user' => array(
                'stateKeyPrefix' => 'back',
            ),
            'bootstrap' => array(
                'class' => 'ext.bootstrap.components.Bootstrap',
            ),
        ),
        'params' => array(
           'pageSize' => 15
        ),
    )
);