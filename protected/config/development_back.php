<?php

return CMap::mergeArray(
    require(dirname(__FILE__) . '/back.php'),
    array(
        'modules' => array(
            'gii' => array(
                'class' => 'system.gii.GiiModule',
                'password' => '1234',
                'ipFilters' => array('127.0.0.1', '::1'),
                'generatorPaths' => array('bootstrap.gii'),
            ),
        ),
        'components' => array(
            'db' => array(
                'connectionString' => 'mysql:host=localhost;dbname=stroy',
                'emulatePrepare' => true,
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8',
                'tablePrefix' => '',
                'enableProfiling' => true,
                'enableParamLogging' => true,
            ),
            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    array(
                        'class' => 'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
                        'ipFilters' => array('127.0.0.1'),
                    ),
                ),
            ),
        ),
    )
);