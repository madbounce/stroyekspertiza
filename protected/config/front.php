<?php

return CMap::mergeArray(
    require(dirname(__FILE__).'/main.php'),
    array(
        'name' => 'СтройЭкспертиза',

        'theme' => 'stroy/front',

        'localeDataPath' => dirname(__FILE__) . '/i18n',

        'components' => array(
            'messages' => array(
                'class' => 'CDbMessageSource',
                'forceTranslation' => true,
                'onMissingTranslation' => array('TranslateModule', 'missingTranslation'),
                'sourceMessageTable' => 'translate_source_message',
                'translatedMessageTable' => 'translate_message',
            ),

            'urlManager' => array(
				'class' => 'CUrlManager',
                'urlFormat' => 'path',
				'showScriptName' => false,
				'rules'=>require(dirname(__FILE__).'/rules.php'),

            ),
            'user' => array(
                'stateKeyPrefix' => 'front',
                'loginUrl' => array('auth/login'),
            ),
	   'bootstrap' => array(
                'class' => 'ext.bootstrap.components.Bootstrap',
            ),
        ),
        'params' => array(
            'pageSize' => 15
        ),
    )
);