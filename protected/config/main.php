<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',

    'preload' => array('log', 'translate'),

    'import' => array(
        'application.models.*',
        'application.models.dictionaries.*',
        'application.components.*',

        'application.components.auth.*',
        'application.components.helpers.*',
        'application.components.widgets.*',
        'application.components.widgets.auth.*',
        'application.components.widgets.user.*',
        'application.components.widgets.user.profile.*',
        'application.components.widgets.activity.*',
        'application.components.widgets.upload.*',
        'application.components.widgets.tender.*',
        'application.components.widgets.employee.*',

        'application.models.auth.*',
        'application.models.profile.*',
        'application.models.user.*',

        'application.modules.translate.*',

        'application.modules.image.models.*',
        'application.modules.image.components.*',

        'application.modules.portfolio.models.*',
        'application.modules.portfolio.components.*',

        'application.modules.news.models.*',
        'application.modules.news.components.*',

        'application.modules.search.*',
        'application.modules.search.models.*',
        'application.modules.search.components.*',

        'application.modules.page.*',
        'application.modules.page.models.*',
        'application.modules.page.components.*',

	    'application.modules.banner.*',
        'application.modules.banner.models.*',
        'application.modules.banner.components.*',

	    'application.modules.seo.*',
        'application.modules.seo.models.*',
        'application.modules.seo.components.*',

        'application.modules.newsletter.*',
        'application.modules.newsletter.models.*',
        'application.modules.newsletter.components.*',

        'application.modules.expert.*',
        'application.modules.expert.models.*',
        'application.modules.expert.components.*',

        'application.modules.config.models.*',

        'application.modules.faq.*',
        'application.modules.faq.models.*',

        'application.modules.articles.*',
        'application.modules.articles.models.*',
        'application.modules.articles.components.*',

        'application.modules.rating.models.*',
    ),

    'modules' => array(
        'image' => array(
            'imagePathAlias' => 'webroot.images',
            'tmpPathAlias' => 'webroot.images.tmp'
        ),
        'translate',
	    'seo',
	    'pictures',
        'news',
	    'banner',
        'page',
        'comment',
        'filesaver',
        'portfolio',
        'mail',
        'config',
        'search',
        'newsletter',
        'expert', // Вопрос/Ответ Експерта
        'faq',
        'articles',
        'rating',
    ),

    'components' => array(
        'clientScript' => array(
            'class' => 'application.extensions.NLSClientScript',
        ),
        'search' => array(
            'class' => 'application.components.DGSphinxSearch',
            'server' => '5.9.9.9',
            'port' => 9312,
            'maxQueryTime' => 3000,
            'enableProfiling'=>0,
            'enableResultTrace'=>0,
            'fieldWeights' => array(
                //'name' => 10000,
                'keywords' => 100,
            ),
        ),
        'user' => array(
            'allowAutoLogin' => true,
            'class' => 'WebUser',
        ),
        'authManager' => array(
            'class' => 'PhpAuthManager',
            'defaultRoles' => array('unconfirmedUser', 'user', 'userWithProfile', 'company', 'administrator'),
            'showErrors' => true
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
        'mail' => array(
            'class' => 'mail.extensions.yii-mail.YiiMail',
        ),
        'cache' => array(
            'class' => 'CFileCache',
        ),
    ),

    'behaviors' => array(
        'runEnd' => array(
            'class' => 'application.components.behaviors.WebApplicationEndBehavior',
        ),
    ),

    'params' => array(
        'siteName' => 'СтройЭкспертиза',
        'siteDomain' => 'stroy.i-d-web.com',
        'adminEmail' => 'alexvnsky@gmail.com',
        'defaultLanguage' => 'ru'
    ),

    'language' => 'ru',
    'sourceLanguage' => 'ru',
);