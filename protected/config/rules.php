<?php

return array(
	'/' => 'site/index',

    'ru-RU/call-an-expert.jspx' => 'site/contactus',

    'products/view/<url>' => 'catalog/default/view',
    'products/<type>/<subtype>' => 'catalog/default/index',
    'products/<type>' => 'catalog/default/index',
	'products' => 'catalog/default/catalog',
	
	'login' => '/auth/login',
	'logout' => '/auth/logout',

	'site/buymap/<city_id>' => 'site/buymap',

	'search/<searchText>' => 'search/default/index',
	'search' => '/search/default/index',

	//'page/<name:\w+>' => 'page/default/view',
    'faq' => 'faq/default/index',
    'faq' => 'faq/default/index/cId/1',
    'faq/<name>' => 'faq/default/view',
    'faq/<name>' => 'faq/default/view/cId/1',

    'faq_world' => 'faq/default/index/cId/2',
    'faq_world/<name>' => 'faq/default/view/cId/2',

//    'images/common/pdf/dog/<name>' => '/page/default/pdf',
    'page/dog_breeds_details/dog-breeds-listing.jspx' => 'page/default/dog_breeds_listing',
    'page/dog-breeds-details.jspx?breed=<name>' => 'page/default/dog_breeds_details',
    'page/<name>/<key>' => 'page/default/index',
	'page/<name>' => 'page/default/index',
	'users/<alias>' => 'user/view',

    'articles/view/<url>' => 'articles/default/view',
    'articles/search/<type>/<subtype>' => 'articles/default/search',
    'articles/search/<type>' => 'articles/default/search',
    'articles/search/' => 'articles/default/search',
    'articles/<type>' => 'articles/default/index',
    'articles' => 'articles/default/catalog',


    'rating/expert/<type:\w+>/<key>' => 'rating/default/expert',
    'rating/expertInfo/<type:\w+>/<key>' => 'rating/default/expertInfo',
    'rating/<view:\w+>/<id:\d+>' => 'rating/default/info',
    'rating/<key>' => 'rating/default/confirm',


	'<module>/<controller:\w+>/<id:\d+>' => '<module>/<controller>/view',
	'<module>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
	'<module>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',

	'<controller:\w+>/<id:\d+>' => '<controller>/view',
	'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
	'<controller:\w+>/<action:\w+>' => '<controller>/<action>',

    'gii'=>'gii',
    'gii/<controller:\w+>'=>'gii/<controller>',
    'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
);
