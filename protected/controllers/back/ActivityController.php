<?php

class ActivityController extends BackEndController
{
    public function actionCreate()
    {
        $model = new Activity;

        $this->performAjaxValidation($model);

        if (isset($_POST['Activity'])) {
            $model->attributes = $_POST['Activity'];
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $model = new Activity('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['Activity']))
            $model->attributes = $_GET['Activity'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionEditable()
    {
        if (isset($_POST['pk']) && isset($_POST['name']) && isset($_POST['value'])) {
            $model = $this->loadModel($_POST['pk']);
            $model->name = $_POST['value'];
            if ($model->save()) {
                echo CJSON::encode(array('success' => true));
                Yii::app()->end();
            }

            echo CJSON::encode(array('success' => false, 'msg' => $model->getError($_POST['name'])));
            Yii::app()->end();
        }

        echo CJSON::encode(array('success' => false));
        Yii::app()->end();
    }

    public function loadModel($id)
    {
        $model = Activity::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'activity-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
