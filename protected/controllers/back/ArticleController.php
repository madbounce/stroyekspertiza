<?php

class ArticleController extends BackEndController
{
    public $layout='//layouts/main_content';

    public function actions(){
        return array(
            'uploadImage' => array(
                'class' => 'application.modules.image.components.UploadAction',
                'entity' => 'Articles',
                'tag' => 'image',
                'directory' => 'articles',
                'view' => 'application.modules.image.components.widgets.views.imagesCatalog',
                'multiple' => true,
            ),
            'redactorUploadImage' => array(
                'class' => 'application.extensions.redactorjs.ERedactorUploadAction',
            ),
            'sortableCategory' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'ArticleCategory'
            )
        );
    }


    public function actionView($id)
    {
		$model = $this->loadModel($id);
		 
        $this->render('view', array(
            'model' => $model,
        ));
    }

	public function actionCreate()
	{
		$model=new Articles;

		if(isset($_POST['Articles']))
		{
			$model->attributes=$_POST['Articles'];

			$model->create_time = time();
			$model->update_time = time();

			if($model->save())
			{
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Articles']))
		{
			$model->attributes=$_POST['Articles'];
			$model->update_time = time();

			if($model->save())
			{
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $model = new Articles('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['Articles']))
            $model->attributes = $_GET['Articles'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCategories()
    {
        $model = new ArticleCategory('search');
        $model->unsetAttributes();
        if (isset($_GET['ArticleCategory']))
            $model->attributes = $_GET['ArticleCategory'];

        $this->render('admincategory', array(
            'model' => $model,
        ));
    }

	public function actionCreateCategory()
	{
		$model=new ArticleCategory;

		if(isset($_POST['ArticleCategory']))
		{
			$model->attributes=$_POST['ArticleCategory'];
			$model->parent_id = 0;

			if($model->save())
			{
				$this->redirect(array('categories'));
			}
		}

		$this->render('createcategory',array(
			'model'=>$model,
		));
	}

	public function actionUpdateCategory($id)
	{
		$model=$this->loadCatModel($id);

		if(isset($_POST['ArticleCategory']))
		{
			$model->attributes=$_POST['ArticleCategory'];

			if($model->save())
			{
				$this->redirect(array('categories'));
			}
		}

		$this->render('updatecategory',array(
			'model'=>$model,
		));
	}

    public function actionEditable()
    {
        if (isset($_POST['pk']) && isset($_POST['name']) && isset($_POST['value'])) {
            $model = $this->loadCatModel($_POST['pk']);
            $model->title = $_POST['value'];

            if ($model->save()) {
                echo CJSON::encode(array('success' => true));
                Yii::app()->end();
            }

            echo CJSON::encode(array('success' => false, 'msg' => $model->getError($_POST['name'])));
            Yii::app()->end();
        }

        echo CJSON::encode(array('success' => false));
        Yii::app()->end();
    }

    public function actionDeleteCategory($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadCatModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('categories'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function loadModel($id)
    {
        $model = Articles::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadCatModel($id)
    {
        $model = ArticleCategory::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
