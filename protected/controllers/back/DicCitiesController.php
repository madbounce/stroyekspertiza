<?php

class DicCitiesController extends BackEndController
{
    public $layout = '//layouts/main_catalog';

    public function actions(){
        return array(
            'sortable' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'DicCities'
            )
        );
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        if (isset($_POST['DicCities'])) {
            $model->attributes = $_POST['DicCities'];
            if ($model->save()) {
                if (isset($_POST['more']))
                    $this->redirect(array('create'));
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $model = new DicCities;

        $this->performAjaxValidation($model);

        if (isset($_POST['DicCities'])) {
            $model->attributes = $_POST['DicCities'];
            if ($model->save()) {
                if (isset($_POST['more']))
                    $this->redirect(array('create'));
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $model = new DicCities('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['DicCities']))
            $model->attributes = $_GET['DicCities'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionEditable()
    {
        if (isset($_POST['pk']) && isset($_POST['name']) && isset($_POST['value'])) {
            $model = $this->loadModel($_POST['pk']);
            $model->title = $_POST['value'];
            if ($model->save()) {
                echo CJSON::encode(array('success' => true));
                Yii::app()->end();
            }

            echo CJSON::encode(array('success' => false, 'msg' => $model->getError($_POST['name'])));
            Yii::app()->end();
        }

        echo CJSON::encode(array('success' => false));
        Yii::app()->end();
    }

    public function loadModel($id)
    {
        $model = DicCities::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cities-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionSortDictionary(){
        $criteria = new CDbCriteria();
        $criteria->order = 'title ASC';
        $models = DicCities::model()->findAll($criteria);
        $i = 1;
        foreach($models as $model){
            $model->sort = $i;
            $model->save(false);
            $i++;
        }
        exit('ok');
    }
}
