<?php

class FiltersController extends BackEndController
{
    public $layout = '//layouts/main_user';

    public function actions(){
        return array(
            'sortable' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'Filters'
            )
        );
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        if (isset($_POST['Filters'])) {
            $model->attributes = $_POST['Filters'];
            if ($model->save()) {
                if (isset($_POST['more']))
                    $this->redirect(array('create'));
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $model = new Filters;
        $this->performAjaxValidation($model);
        
        if(isset($_GET['region']) && ($_GET['region'] != '')){
            $model->value .= "&region=".(int) $_GET['region'];
        }
        
        if(isset($_GET['city']) && ($_GET['city'] != '')){
            $model->value .= "&city=".$_GET['city'];
        }

        if(isset($_GET['age']) && ($_GET['age'] != '')){
            $model->value .= "&age=".(int) $_GET['age'];
        }
        
        if(isset($_GET['dateFrom']) && ($_GET['dateFrom'] != '')){
            $model->value .= "&dateFrom=".$_GET['dateFrom'];
        }
        
        if(isset($_GET['dateTo']) && ($_GET['dateTo'] != '')){
            $model->value .= "&dateTo=".$_GET['dateTo'];
        }

        if(isset($_GET['petFoodBuyWhen']) && ($_GET['petFoodBuyWhen'] != '')){
            $model->value .= "&petFoodBuyWhen=".$_GET['petFoodBuyWhen'];
        }

        if(isset($_GET['wetFoodFreq']) && ($_GET['wetFoodFreq'] != '')){
            $model->value .= "&wetFoodFreq=".$_GET['wetFoodFreq'];
        }

        if(isset($_GET['dryFoodFreq']) && ($_GET['dryFoodFreq'] != '')){
            $model->value .= "&dryFoodFreq=".$_GET['dryFoodFreq'];
        }

        if(isset($_GET['foodbuy']) && ($_GET['foodbuy'] != '')){
            $model->value .= "&foodbuy=".$_GET['foodbuy'];
        }

        if(!empty($model->value)){
            $model->value = substr($model->value, 1);
            if ($filter = Filters::model()->find(array('order'=>'id DESC'))){
                $newId = (int) $filter->id + 1;
            } else{
                $newId = 1;
            }    
            $model->name = 'Фильтр '.$newId;
        }
        
        if (isset($_POST['Filters'])) {
            $model->attributes = $_POST['Filters'];
            if ($model->save()) {
                if (isset($_POST['more']))
                    $this->redirect(array('create'));
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $model = new Filters('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['Filters']))
            $model->attributes = $_GET['Filters'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionEditable()
    {
        if (isset($_POST['pk']) && isset($_POST['name']) && isset($_POST['value'])) {
            $model = $this->loadModel($_POST['pk']);
            $model->title = $_POST['value'];
            if ($model->save()) {
                echo CJSON::encode(array('success' => true));
                Yii::app()->end();
            }

            echo CJSON::encode(array('success' => false, 'msg' => $model->getError($_POST['name'])));
            Yii::app()->end();
        }

        echo CJSON::encode(array('success' => false));
        Yii::app()->end();
    }

    public function loadModel($id)
    {
        $model = Filters::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'filters-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}