<?php

class MenuController extends BackEndController
{
    public $layout = '//layouts/main_config';

    public function actions(){
        return array(
            'toggle' => 'ext.jtogglecolumn.ToggleAction',
            'sortable' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'Menu'
            )
        );
    }

    public function actionTreeView($id) {
//		die(print_r($_GET));
		$this->layout = '//layouts/empty';
		$model=Menu::model()->findByPk($id);
		$flag = false;
		if(isset($_GET['url']) && $_GET['url']) {
			$model->setAttribute('url' ,$_GET['url']);
			$model->save();
			$flag = true;
		}
		$this->render('_ifrm', array(
            'model' => $model,
			'flag'=>$flag,
        ));
	}
	
	public function actionTree()
	{
		Yii::import('application.extensions.SimpleTree.SimpleTreeWidget');    
		SimpleTreeWidget::performAjax();
	}
		
	public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        //$this->performAjaxValidation($model);

        if (isset($_POST['Menu'])) {
            $model->attributes = $_POST['Menu'];
			$model->url = $_POST['Menu']['url'];
			//$model->parent_id = $_POST['Menu']['parent_id'];
			//die(var_dump($_POST['Menu']));
            if ($model->save()) {
                if (isset($_POST['more']))
                    $this->redirect(array('create'));
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }
	
	public function actionCreate()
    {
        $model = new Menu;

        $this->performAjaxValidation($model);

        if (isset($_POST['Menu'])) {
            $model->attributes = $_POST['Menu'];
            if ($model->save()) {
                if (isset($_POST['more']))
                    $this->redirect(array('create'));
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $model = new AllMenu('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['AllMenu']))
            $model->attributes = $_GET['AllMenu'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

	public function actionAdmin()
    {
        $model = new Menu('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['Menu']))
            $model->attributes = $_GET['Menu'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionEditable()
    {
        if (isset($_POST['pk']) && isset($_POST['name']) && isset($_POST['value'])) {
            $model = $this->loadModel($_POST['pk']);
            $model->name = $_POST['value'];
            if ($model->save()) {
                echo CJSON::encode(array('success' => true));
                Yii::app()->end();
            }

            echo CJSON::encode(array('success' => false, 'msg' => $model->getError($_POST['name'])));
            Yii::app()->end();
        }

        echo CJSON::encode(array('success' => false));
        Yii::app()->end();
    }

    public function loadModel($id)
    {
        $model = Menu::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'region-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
