<?php

class RegionController extends BackEndController
{
    public $layout = '//layouts/main_config';

    public function actions(){
        return array(
            'toggle' => 'ext.jtogglecolumn.ToggleAction',
            'sortable' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'Region'
            )
        );
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        if (isset($_POST['Region'])) {
            $model->attributes = $_POST['Region'];
            if ($model->save()) {
                if (isset($_POST['more']))
                    $this->redirect(array('create'));
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }
	
	public function actionCreate()
    {
        $model = new Region;

        $this->performAjaxValidation($model);

        if (isset($_POST['Region'])) {
            $model->attributes = $_POST['Region'];
            if ($model->save()) {
                if (isset($_POST['more']))
                    $this->redirect(array('create'));
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

	public function actionAdmin()
    {
        $model = new Region('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['Region']))
            $model->attributes = $_GET['Region'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionEditable()
    {
        if (isset($_POST['pk']) && isset($_POST['name']) && isset($_POST['value'])) {
            $model = $this->loadModel($_POST['pk']);
            $model->name = $_POST['value'];
            if ($model->save()) {
                echo CJSON::encode(array('success' => true));
                Yii::app()->end();
            }

            echo CJSON::encode(array('success' => false, 'msg' => $model->getError($_POST['name'])));
            Yii::app()->end();
        }

        echo CJSON::encode(array('success' => false));
        Yii::app()->end();
    }

    public function actionToggle($id,$attribute) {
        $model = Region::model()->findByPk($id);
        if($model){
            if($model->approved == 1)
                $model->approved = 0;
            else
                $model->approved = 1;
        }
        $model->save(false);
    }

    public function loadModel($id)
    {
        $model = Region::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'region-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
