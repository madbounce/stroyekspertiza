<?php

class ShopController extends BackEndController
{
    public $layout = '//layouts/main_catalog';

    public function actions(){
        return array(
            'sortable' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'Shop'
            )
        );
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        if (isset($_POST['Shop'])) {
            $model->attributes = $_POST['Shop'];
            if ($model->save()) {
                if (isset($_POST['more']))
                    $this->redirect(array('create'));
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $model = new Shop;

        $this->performAjaxValidation($model);

        if (isset($_POST['Shop'])) {
            $model->attributes = $_POST['Shop'];
            if ($model->save()) {
                if (isset($_POST['more']))
                    $this->redirect(array('create'));
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $model = new Shop('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['Shop']))
            $model->attributes = $_GET['Shop'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionEditable()
    {
        if (isset($_POST['pk']) && isset($_POST['name']) && isset($_POST['value'])) {
            $model = $this->loadModel($_POST['pk']);
            $model->title = $_POST['value'];
            if ($model->save()) {
                echo CJSON::encode(array('success' => true));
                Yii::app()->end();
            }

            echo CJSON::encode(array('success' => false, 'msg' => $model->getError($_POST['name'])));
            Yii::app()->end();
        }

        echo CJSON::encode(array('success' => false));
        Yii::app()->end();
    }

    public function loadModel($id)
    {
        $model = Shop::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'shop-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function getExportData(&$data=array()){
        $criteria = new CDbCriteria();
        $criteria->with = array('city');
        $criteria->order = 'city.sort ASC, t.sort ASC';
        $models = Shop::model()->findAll($criteria);
        if($models){
            foreach ($models as $m) {
                $data[$m->id] = array(
                    $m->id,
                    ($m->city) ? $m->city->title : '',
                    ($m->title) ? $m->title : '',
                    ($m->address) ? $m->address : '',
                    ($m->latitude) ? $m->latitude : '',
                    ($m->longitude) ? $m->longitude : '',
                );
            }
        }

        return $data;
    }

    public function actionExport()
    {
        set_time_limit(0);

        Yii::import('application.vendors.PHPExcel.PHPExcel');
        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('IAMS - Good for life');

        $header = array(
            'ID',
            'Город',
            'Название',
            'Адрес',
            'Широта',
            'Долгота',
        );

        $dimensions = array(
            'A' => 4,
            'B' => 18,
            'C' => 25,
            'D' => 100,
            'E' => 12,
            'F' => 12,
        );

        $sheet = $excel->getActiveSheet();

        foreach ($dimensions as $column => $width)
            $sheet->getColumnDimension($column)->setWidth($width);

        $sheet->getStyle("A1:F1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle("A:F")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A1:F1")->getFont()->setBold(true);
        foreach ($header as $column => $item)
            $sheet->setCellValueByColumnAndRow($column, 1, $item);

        $data = $this->getExportData();
        $raw = 2;
        foreach ($data as $item) {
            foreach ($item as $column => $value) {
                $sheet->setCellValueByColumnAndRow($column, $raw, $value);
            }
            $raw++;
        }

        header('Content-Type: application/vnd.ms-excel');
        //header('Content-Disposition: attachment;filename="' . "iams_shops_" . date("d-m-Y H:i", time()) . ".xlsx" .  '"');
        header('Content-Disposition: attachment;filename="' . "iams_shops_" . date("d-m-Y H:i", time()) .  '"');
        header('Cache-Control: max-age=0');
        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $writer->save('php://output');
    }


    protected function importData($data)
    {
        $shops = array();

        foreach ($data as $item) {
            //$shop['id'] = $item[0];
            $shop['city'] = trim($item[0]);
            $shop['title'] = trim($item[1]);
            $shop['address'] = trim($item[2]);
            //$shop['latitude'] = floatval($item[4]);
            //$shop['longitude'] = floatval($item[5]);
            array_push($shops, $shop);
        }

        $messages = array();

        $i = 1;
        foreach ($shops as $shop) {
            /*if (empty($shop['id']) || !is_numeric($shop['id'])) {
                array_push($messages, "<span style='color: red'>Ошибка — не найден ID магазина. Скорее всего файл имеет неверный формат.</span><br>");
                break;
            }*/
            $i++;
            if(empty($shop['city']) || empty($shop['title']) || empty($shop['address'])){
                array_push($messages, "<span style='color: red'>Ошибка при добавлении магазина 'строка - {$i}'. Скорее всего не заполнена одна из трех колонок (город, название, адрес).</span><br>");
                continue;
            }



            //$model = Shop::model()->findByPk($shop['id']);

            //if (empty($model)) {
                $model = new Shop();
            /*} else {
                //Проверяем действительно ли что то изменилось
                $changed = $model->title != $shop["title"] ||
                           $model->address != $shop["address"] ||
                           $model->latitude != $shop["latitude"] ||
                           $model->longitude != $shop["longitude"];
                if(!$changed)
                    continue;
            }*/

            //$model->id = $shop["id"];
            $model->title = $shop["title"];
            $model->address = $shop["address"];
            //$model->latitude = $shop["latitude"];
            //$model->longitude = $shop["longitude"];

            $city = DicCities::model()->find('title=:title', array(':title' => $shop["city"]));
            if (empty($city)) {
                $city = new DicCities();
                $city->title = $shop["city"];
                $city->save(false);
            }
            $model->city_id = $city->id;
            /*$status = $model->isNewRecord ? 'добавлен' : 'изменен';
            if ($model->save()) {
                array_push($messages, "<span style='color: green'><b>Успешно {$status} </b> элемент '{$model->title}' (id - {$model->id}).</span><br>");
            } else {
                foreach ($model->getErrors() as $e) {
                    array_push($messages, "<span style='color: red'>Ошибка при добавлении/изменении элемента (артикул {$model->id}) — {$e[0]}</span><br>");
                }
            }*/
            if ($model->save()) {
                array_push($messages, "<span style='color: green'><b>Успешно добавлен </b> магазин '{$model->title}' (id - {$model->id}).</span><br>");
            } else {
                foreach ($model->getErrors() as $e) {
                    array_push($messages, "<span style='color: red'>Ошибка при добавлении магазина (название {$model->title}) — {$e[0]}</span><br>");
                }
            }

        }

        return $messages;
    }

    public function actionImport()
    {
        set_time_limit(0);

        $model = new ImportForm;
        if (isset($_POST['ImportForm'])) {
            if ($model->validate()) {
                $file = $model->getFile();

                Yii::import('application.vendors.PHPExcel.PHPExcel');
                $excel = new PHPExcel();
                $inputFileType = PHPExcel_IOFactory::identify($file);
                $reader = PHPExcel_IOFactory::createReader($inputFileType);
                $reader->setReadDataOnly(true);
                $excel = $reader->load($file);

                /** @var $excel PHPExcel */
                $excel->setActiveSheetIndex(0);
                /** @var $sheet PHPExcel_Worksheet */
                $sheet = $excel->getActiveSheet();

                $data = array();
                $iterator = $sheet->getRowIterator(2);
                foreach($iterator as $row){
                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(false);
                    $item = array();
                    foreach($cellIterator as $cell){
                        array_push($item, trim($cell->getValue()));
                    }
                    array_push($data, $item);
                }

                $messages = $this->importData($data);
                $this->render('importSimple', array('model' => $model, 'messages' => implode("\n", $messages)));
                Yii::app()->end();
            }
        }

        $this->render('importSimple', array('model' => $model));
    }



    //Temp action
    public function actionDeleteDublicates(){
        $shops = Shop::model()->with(array('city'))->findAll();
        foreach($shops as $shop){
           if($shop->title == $shop->city->title)
              $shop->delete();
        }
    }

    public function actionSortDictionary(){
        $criteria = new CDbCriteria();
        $criteria->order = 'title ASC';
        $models = Shop::model()->findAll($criteria);
        $i = 1;
        foreach($models as $model){
            $model->sort = $i;
            $model->save(false);
            $i++;
        }
        exit('ok');
    }


}
