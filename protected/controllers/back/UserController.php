<?php

class UserController extends BackEndController
{
    public $layout='//layouts/main_user';

    public function actionView($id)
    {
       $model = $this->loadModel($id);

       $this->render('view', array(
            'model' => $model,
       ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $model = new User('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionStatistics(){
        $model = new User('statistics');

        $this->render('statistics', array(
            'model' => $model,
        ));
    }

    protected function getExportData(&$data=array(), $usertype){

        $models = User::model()->findAll();
        
        if($models){
             foreach ($models as $m) {
                  $data[$m->id] = array(
                      $m->id,
                      Yii::app()->dateFormatter->format("dd MMMM yyyy", $m->dateRegistered),
                      $m->email,
                      ($m->first_name) ? $m->first_name : '-',
                      ($m->last_name) ? $m->last_name : '-',
                      ($m->address1) ? $m->address1 : '-',
                      ($m->address2) ? $m->address2 : '-',
                      ($m->city) ? $m->city : '-',
                      ($m->d_birth) ? Yii::app()->dateFormatter->format("LLLL yyyy", $m->d_birth) : '-',
                      ($m->region) ? $m->region->title : '-',
                  );
             }
        }

        return $data;
    }

    public function actionExport()
    {
        $usertype = 0;
        if(isset($_GET['id'])){
            $usertype = $_GET['id'];
        }
        
        set_time_limit(0);

        Yii::import('application.vendors.PHPExcel.PHPExcel');
        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('Eukanuba');

        $header = array(
            'ID',
            'Дата регистрации',
            'Email',
            'Имя',
            'Фамилия',
            'Адрес',
            'Адрес 2',
            'Город',
            'Дата рождения',
            'Регион',
        );

        $dimensions = array(
            'A' => 4,
            'B' => 18,
            'C' => 25,
            'D' => 20,
            'E' => 20,
            'F' => 25,
            'G' => 25,
            'H' => 15,
            'I' => 20,
            'J' => 35,
            'K' => 18,
            'L' => 15,
            'M' => 23,
            'N' => 26,
            'O' => 25,
            'P' => 20,
            'Q' => 20,
            'R' => 20,
            'S' => 20,
            'T' => 20,
            'U' => 20,
            'V' => 20,
            'W' => 20,
            'X' => 60,
            'Y' => 60,
            'Z' => 50,
            'AA' => 50,
        );

        $sheet = $excel->getActiveSheet();

        foreach ($dimensions as $column => $width)
            $sheet->getColumnDimension($column)->setWidth($width);

        $sheet->getStyle("A1:AA1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle("A:AA")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle("A1:AA1")->getFont()->setBold(true);
        foreach ($header as $column => $item)
            $sheet->setCellValueByColumnAndRow($column, 1, $item);
        
        $data=array();
        $this->getExportData($data, $usertype);
        $raw = 2;
        foreach ($data as $item) {
            foreach ($item as $column => $value) {
                $sheet->setCellValueByColumnAndRow($column, $raw, $value);
            }
            $raw++;
        }

        //ob_end_clean();
        //ob_start();

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . "users_" . date("d-m-Y H:i", time()) .  '"');
        header('Cache-Control: max-age=0');
        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $writer->save('php://output');
    }
    
    public function actionImport()
    {
        set_time_limit(0);

        $model = new ImportForm;
        if (isset($_POST['ImportForm'])) {
            if ($model->validate()) {
                $file = $model->getFile();

                Yii::import('application.vendors.PHPExcel.PHPExcel');
                $excel = new PHPExcel();
                $inputFileType = PHPExcel_IOFactory::identify($file);
                $reader = PHPExcel_IOFactory::createReader($inputFileType);
                $reader->setReadDataOnly(true);
                $excel = $reader->load($file);

                /** @var $excel PHPExcel */
                $excel->setActiveSheetIndex(0);
                /** @var $sheet PHPExcel_Worksheet */
                $sheet = $excel->getActiveSheet();

                $data = array();
                $iterator = $sheet->getRowIterator(2);
                foreach($iterator as $row){
                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(false);
                    $item = array();
                    foreach($cellIterator as $key => $cell){
						if ($key == 6 )
							array_push($item, date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($cell->getValue())));
						else
                        	array_push($item, trim($cell->getValue()));
                    }
                    array_push($data, $item);
                }

                $messages = $this->importData($data);
                $this->render('importSimple', array('model' => $model, 'messages' => implode("\n", $messages)));
                Yii::app()->end();
            }
        }

        $this->render('importSimple', array('model' => $model));
    }
    
    protected function importData($data)
    {
        $users = array();

        foreach ($data as $item) {
            //$user['id'] = $item[0];
            $user['email'] = trim($item[0]);
            $user['first_name'] = trim($item[1]);
            $user['last_name'] = trim($item[2]);
            $user['address1'] = trim($item[3]);
            $user['address2'] = trim($item[4]);
            $user['city'] = trim($item[5]);
            $user['d_birth'] = $item[6];
            $user['region'] = trim($item[7]);
            array_push($users, $user);
        }

        $messages = array();

        $i = 1;
        foreach ($users as $user) {
            $i++;
            
            if(empty($user['email']) || empty($user['first_name']) || empty($user['last_name'])){
                array_push($messages, "<span style='color: red'>Ошибка при добавлении члена клуба 'строка - {$i}'. Скорее всего не заполнена одна из девяти колонок (город, название, адрес).</span><br>");
                continue;
            }
            
            $region = DicRegions::model()->find('title=:title', array(':title' => $user["region"]));
            if (empty($region)) {
                array_push($messages, "<span style='color: red'>Ошибка при добавлении члена клуба 'строка - {$i}'. Указанный регион не найден.</span><br>");
                continue;
            }               

            $model = new User();

            $model->email = $user["email"];
            $model->address1 = $user["address1"];
            $model->address2 = $user["address2"];
            $model->first_name = $user["first_name"];
            $model->last_name = $user["last_name"];
            $model->city = $user["city"];
            $model->d_birth = date('Y-m-d H:i:s', strtotime(str_replace('/','.',$user["d_birth"])));
            $model->dateRegistered = date('Y-m-d H:i:s', time());

            $model->region_id = $region->id;

            if ($model->save()) {
                array_push($messages, "<span style='color: green'><b>Успешно добавлен </b> член клуба '{$model->email}' (id - {$model->id}).</span><br>");
            } else {
                foreach ($model->getErrors() as $e) {
                    array_push($messages, "<span style='color: red'>Ошибка при добавлении члена клуба (название {$model->email}) — {$e[0]}</span><br>");
                }
            }

        }

        return $messages;
    }    
    
    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
