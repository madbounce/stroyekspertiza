<?php

class ArticleController extends Controller
{
	public $pid=null;
	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','articles','category','search','landing',),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		if ( isset($model->title) ) {
			$this->pageTitle = $model->title;
		}
        Yii::app()->clientScript->registerMetaTag($model->meta_description,'description');
		Yii::app()->clientScript->registerMetaTag($model->meta_keywords,'keywords');
		$this->render('view2', array(
			'model' => $model,
		));
	}
	
	public function actionCategory($type, $id)
	{
		$category = ArticleCategory::model()->findByPk($id);
		//die(var_dump($category->attributes));
		if ( isset($category->title) ) {
			$this->pageTitle = $category->title;
		}
		
		$model = Articles::model()->findAll(array('condition'=>'material_category_id='.$id));
        
		$this->render('view_cat', array(
			'category' => $category,
			'model' => $model,
		));
	}


	/**
	 * Lists all models.
	 */
	public function actionLanding($type = 'cats')
	{
		$this->pageTitle = 'Статьи';

        $pagination = new CPagination(Articles::model()->count());
		$pagination->pageSize = 10;
		$dataProvider = new CActiveDataProvider('Articles', array(
			'pagination'=>$pagination,
			'sort'=>array('defaultOrder'=>'id DESC'),
		));

        $this->render($type, array('dataProvider'=>$dataProvider, 'pages'=>$pagination));
	}
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->pageTitle = 'Статьи';

        $pagination = new CPagination(Articles::model()->count());
		$pagination->pageSize = 10;
		$dataProvider = new CActiveDataProvider('Articles', array(
			'pagination'=>$pagination,
			'sort'=>array('defaultOrder'=>'id DESC'),
		));

        $this->render('index', array('dataProvider'=>$dataProvider, 'pages'=>$pagination));
	}
	
	public function actionSearch($type)
	{
		
		$criteria = new CDbCriteria();

		$age = array();
		$size = array();
		if(isset($_GET['age'])) {
			$age = $_GET['age'];
		}
		
		if(isset($_GET['categories'])) {
			$criteria->addInCondition('id',$_GET['categories']);
		}
		
		if(isset($_GET['size'])) {
			$size = $_GET['size'];
		}
		
		
		
        $criteria->order = 'sort ASC';
		$categories = ArticleCategory::model()->findAll($criteria);
//		$this->pageTitle = Seo::model()->getbyRoute($this->route)->find()->title;

		$criteria = new CDbCriteria;
		$criteria->condition = '1 = 1';
		$criteria->order = 'update_time DESC';
//		$criteria->limit = 5;
		if (isset($_POST['articleQuery']))
		{
			$criteria->addSearchCondition('title', $_POST['articleQuery'], true);
		}

		$articles = array();
		foreach ($categories as $cat)
		{
			$tmp = new CDbCriteria;
			$tmp = $criteria;
			$tmp->addCondition('material_category_id='.$cat->id);
			$model = Articles::model()->findAll($tmp);
			$articles[$cat->id] = $model;
			//if ($cat->id==16) die(var_dump($tmp));
			$tmp->condition = '';
		}
		
		if(Yii::app()->request->isAjaxRequest) {
			echo $this->renderPartial('list',array('categories'=>$categories, 'age'=>$age, 'size'=>$size, 'type'=>$type));
			die();
		}
		
		$this->render('articles',array('categories'=>$categories,'articles'=>$articles, 'age'=>$age, 'size'=>$size, 'type'=>$type));
	}
	
	public function actionArticles($type, $id = '')
	{
        $this->layout = "//layouts/{$type}";
		
		$criteria = new CDbCriteria();
        $criteria->order = 'sort ASC';
		if($id) {
			$criteria->addCondition("id = {$id}");
		}
		if(!$id) {
			if($type=='cats') {
				$criteria->addInCondition('id', array(50,51),'OR');
			} else if($type=='dogs') {
				$criteria->addInCondition('parent_id', array(24,31,47,38),'OR');
			}
		} else {
			$criteria->addCondition("id = {$id}");
		}
		$categories = ArticleCategory::model()->findAll($criteria);
		$this->pageTitle = Seo::model()->getbyRoute($this->route)->find()->title;

		$criteria = new CDbCriteria;
		$criteria->condition = '1 = 1';
		$criteria->order = 'update_time DESC';
//		$criteria->limit = 5;
		if (isset($_POST['articleQuery']))
		{
			$criteria->addSearchCondition('title', $_POST['articleQuery'], true);
		}

		$articles = array();
		foreach ($categories as $cat)
		{
			$tmp = new CDbCriteria;
			if (isset($_POST['articleQuery']))
				$tmp = $criteria;
			$tmp->addCondition('material_category_id='.$cat->id);
			$model = Articles::model()->findAll($tmp);
			$articles[$cat->id] = $model;
		}
		
		$this->render('articles',array('categories'=>$categories,'articles'=>$articles, 'age'=>array(), 'size'=>array(), 'type'=>$type));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		if (is_numeric($id))
			$model = Articles::model()->findByPk($id);
		else
			$model = Articles::model()->find(array("condition"=>"slug = '$id' "));
			
		if($model===null)
			throw new CHttpException(404,'К сожалению, запрашиваемая страница не найдена.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='article-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
