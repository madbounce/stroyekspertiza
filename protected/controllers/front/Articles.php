<?php

class Articles extends CActiveRecord
{
	public $rate;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{articles}}';
	}
	
	protected function beforeSave()
  {
    if (parent::beforeSave())
		{
			if ($this->isNewRecord) {
				$this->update_time = date('Y-m-d H:i:s', time());
				$this->create_time = date('Y-m-d H:i:s', time());
			} else {
				$this->update_time = date('Y-m-d H:i:s', strtotime(str_replace('/','.',$this->update_time)));
			}
		}
		return true;
  }

	public function rules()
	{
		return array(
			array('material_category_id, rubric_id, title, content, author_id', 'required'),
			array('material_category_id, rubric_id, rating_count, author_id, isAuto', 'numerical', 'integerOnly'=>true),
			array('rating_value', 'numerical'),
			array('title', 'length', 'max'=>80),
			array('content_short, image, flash, url', 'length', 'max'=>255),
			array('tags, create_time, update_time, duration,authortext,is_rss,slug', 'safe'),
			array('id, material_category_id, rubric_id, title, content, content_short, image, flash, rating_value, rating_count, tags, create_time, update_time, author_id, isAuto, url, slug', 'safe', 'on'=>'search'),
                   
		);
	}
   
	public function relations()
	{
		return array( 
			'category' => array(self::BELONGS_TO, 'ArticleCategory', 'material_category_id'),
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
		);
	}
  
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'ID'),
			'material_category_id' => Yii::t('app', 'Категория'),
            'rubric_id' => Yii::t('app', 'Рубрика'),
			'title' => Yii::t('app', 'Заголовок Публикации'),
			'slug' => Yii::t('app', 'ЧПУ Публикации'),
			'content' => Yii::t('app', 'Текст Публикации'),
			'content_short' => Yii::t('app', 'Краткое описание'),
			'image' => Yii::t('app', 'Изображение'),
			'flash' => Yii::t('app', 'Flash'),
			'rating_value' => Yii::t('app', 'Rating Value'),
			'rating_count' => Yii::t('app', 'Rating Count'),
			'tags' => Yii::t('app', 'Теги'),
			'create_time' => Yii::t('app', 'Create Time'),
			'update_time' => Yii::t('app', 'Дата'),
			'author_id' => Yii::t('app', 'Автор'),
			'isAuto' => Yii::t('app', 'Is Auto'),
			'url' => Yii::t('app', 'Url'),
            'authortext'=>'Автор текстом',
			'is_rss'=>'отправить в rss',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		//$criteria->compare('id',$this->id);

		//$criteria->compare('material_category_id',$this->material_category_id);

		//$criteria->compare('title',$this->title,true);

		//$criteria->compare('content',$this->content,true);

		//$criteria->compare('content_short',$this->content_short,true);

		//$criteria->compare('image',$this->image,true);

		//$criteria->compare('flash',$this->flash,true);

		//$criteria->compare('rating_value',$this->rating_value);

		//$criteria->compare('rating_count',$this->rating_count);

		//$criteria->compare('tags',$this->tags,true);


		//$criteria->compare('author_id',$this->author_id);

		//$criteria->compare('isAuto',$this->isAuto);

		//$criteria->compare('url',$this->url,true);
		
		$criteria->order= 'update_time desc';
		 
		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
        
	protected function beforeFind() {
		parent::beforeFind();
		$this->getDbCriteria()->order='id DESC';
	
	}
	
	public function getListerConditioner($suff = null)
  	{
      if(!$suff)
      {
          $suff = '_LC_SEARCH';
      }

      yii::import('application.components.portlet.lister.ListerConditioner');
      $lc = new ListerConditioner($this, $suff);
      $lc->orderByFields = array(
          't.update_time' => 'По дате',          
          't.rating_value' => 'По рейтенгу',
      );
			$lc->itemsPerPage=10;      
			$lc->availableIPP = array(10, 20, 30);
		
			$lc->orderBySelected['direction']='DESC';
    	$lc->orderBySelected['field'] = 't.update_time';

      
      return $lc;
  	}
}
