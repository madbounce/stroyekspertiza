<?php

class AuthController extends FrontEndController
{
    public function actionLogin()
    {
        $model = new LoginForm;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate() && $model->login()) {
                $this->redirect(array('/'));
            }
        }

        $this->render('login', array('model' => $model));
    }

    public function actionLoginzaLogin() {
        if (isset($_POST['token'])) {
            $loginza = new LoginzaModel();
            if ($loginza->login($_POST['token']))
                $this->redirect(array('/'));
        }

        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionRegister()
    {
        $model = new User(User::SCENARIO_REGISTER);

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'register-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save()) {
                Yii::app()->getModule('mail')->send($model->email, Config::get('infoEmail'), 'registerConfirmation', array(
                    'siteNameLink' => CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')),
                    'confirmLink' => CHtml::link(Misc::t('ссылке'), Yii::app()->createAbsoluteUrl('/auth/confirm', array('code' => $model->verify_code)))
                ));
                $this->redirect(array('site/page', 'view' => 'registerConfirmationSend'));
            }
        }

        $this->render('register', array('model' => $model));
    }

    public function actionInviteRegister($code)
    {
        $invite = Invite::getByCode($code);
        if (empty($invite))
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $model = new User(User::SCENARIO_REGISTER);
        $model->first_name = $invite->data['first_name'];
        $model->last_name = $invite->data['last_name'];
        $model->email = $invite->data['email'];
        $model->contact_phone = $invite->data['contact_phone'];
        $model->company_post = $invite->data['company_post'];
        $model->profile_id = $invite->profile_id;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'register-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save()) {
                Yii::app()->getModule('mail')->send($model->email, Config::get('infoEmail'), 'registerConfirmation', array(
                    'siteNameLink' => CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')),
                    'confirmLink' => CHtml::link(Misc::t('ссылке'), Yii::app()->createAbsoluteUrl('/auth/confirm', array('code' => $model->verify_code)))
                ));
                $this->redirect(array('site/page', 'view' => 'registerConfirmationSend'));
            }
        }

        $this->render('register', array('model' => $model));
    }

    public function actionConfirm($code)
    {
        if (User::confirm($code))
            $this->redirect(array('site/page', 'view' => 'registerConfirmationSuccess'));
        else
            $this->redirect(Yii::app()->homeUrl);
    }

    public function actionReminder()
    {
        $model = new ReminderForm;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'reminder-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['ReminderForm'])) {
            $model->attributes = $_POST['ReminderForm'];

            if ($model->validate()) {
                $user = User::getByEmail($model->email);
                if (!empty($user) && $user->status == User::STATUS_CONFIRMED) {
                    //$user->generateVerifyCode();
                    $password = $user->generatePassword();
                    if ($user->save()) {
                        Yii::app()->getModule('mail')->send($model->email, Config::get('infoEmail'), 'reminderConfirmation', array(
                            'siteNameLink' => CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')),
                            //'confirmLink' => CHtml::link(Misc::t('ссылке'), Yii::app()->createAbsoluteUrl('/auth/restorePassword', array('code' => $user->verify_code)))
                            'email' => $user->email,
                            'password' => $password
                        ));

                        $this->setMessage(Misc::t('Восстановление пароля!'), '<div class="recovery_text">' . Misc::t('На указанный Вами почтовый ящик отправлено письмо, в котором указаны логин и пароль от Вашего аккаунта!') . '</div>');
                        $this->redirect(Yii::app()->homeUrl);
                    }
                }
            }
        }

        $this->render('reminder', array('model' => $model));
    }

    public function actionRestorePassword($code)
    {
        $user = User::model()->findByAttributes(array('verify_code' => $code));
        if (empty($user))
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $model = new RestorePasswordForm;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'restore-password-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['RestorePasswordForm'])) {
            $model->attributes = $_POST['RestorePasswordForm'];

            if ($model->validate() && $model->restorePassword($user)) {
                $this->setMessage(Misc::t('Восстановление пароля!'), Misc::t('Ваш пароль успешно изменен!'));
                $this->redirect(Yii::app()->homeUrl);
            }
        }

        $this->render('restorePassword', array('model' => $model));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}
