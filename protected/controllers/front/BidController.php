<?php

class BidController extends FrontEndController
{
    public $layout = '//layouts/profile';

    public $tender;
    public $bid;

    public function filters()
    {
        return array(
            'accessControl',
            array(
                'EntityContextFilter + create',
                'entity' => 'Tender',
            ),
            array(
                'EntityContextFilter + view, update, cancel, accept',
                'entity' => 'Bid',
            ),
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'roles' => array('user'),
                'actions' => array('create', 'update', 'view', 'cancel', 'accept'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionCreate()
    {
        if (!Yii::app()->user->checkAccess('createBid', array('tender' => $this->tender)))
            $this->forbidden();

        $model = new Bid;
        $model->phone = Yii::app()->user->model->contact_phone;
        $model->email = Yii::app()->user->model->email;

        $this->performAjaxValidation($model);

        if (isset($_POST['Bid'])) {
            $model->attributes = $_POST['Bid'];
            $model->user_id = Yii::app()->user->id;
            $model->tender_id = $this->tender->id;
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate()
    {
        if (!Yii::app()->user->checkAccess('updateBid', array('bid' => $this->bid)))
            $this->forbidden();

        $model = $this->bid;

        $this->performAjaxValidation($model);

        if (isset($_POST['Bid'])) {
            $model->attributes = $_POST['Bid'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionView()
    {
        $this->render('view', array('model' => $this->bid));
    }

    public function actionCancel()
    {
        if (!Yii::app()->user->checkAccess('cancelBid', array('bid' => $this->bid)))
            $this->forbidden();

        $model = $this->bid;

        $model->canceled = 1;
        $model->save();

        $this->redirect(array('view', 'id' => $model->id));
    }

    public function actionAccept()
    {
        if (!Yii::app()->user->checkAccess('acceptBid', array('bid' => $this->bid)))
            $this->forbidden();

        $this->bid->accept();
        $this->redirect(array('/tender/view', 'id' => $this->bid->tender_id));
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'bid-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
