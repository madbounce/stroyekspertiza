<?php

class EmployeeController extends FrontEndController
{
    public $employee;

    public function filters()
    {
        return array(
            'accessControl',
            array(
                'EntityContextFilter + update, delete',
                'entity' => 'EmployeeUser',
                'controllerVar' => 'employee',
            ),
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('*'),
                'actions' => array('invite'),
            ),
            array('allow',
                'roles' => array('company'),
                'actions' => array('create', 'update', 'delete'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionInvite($code)
    {
        $invite = Invite::getByCode($code);
        if (empty($invite))
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $user = User::model()->findByPk($invite->data['id']);
        if (empty($user))
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        // TODO Разные проверки
        if ($user->isCompany) {

        } elseif ($user->isFreelancer) {
            $user->profile->delete();
        }

        $user->profile_id = $invite->profile_id;
        $user->company_post = $invite->data['company_post'];
        $user->save();

        $this->setMessage(Yii::t('profile', 'Приглашение в компания'),Yii::t('profile', 'Вы приняли приглашение в компанию.'));
        $this->redirect(array('/profile/index'));
    }

    public function actionCreate()
    {
        if (!Yii::app()->user->checkAccess('createEmployee', array('profile' => Yii::app()->user->model->profile)))
            $this->forbidden();

        $model = new EmployeeUser;

        $this->performAjaxValidation($model);

        if (isset($_POST['EmployeeUser'])) {
            $model->attributes = $_POST['EmployeeUser'];

            $user = User::getByEmail($model->email);
            if (empty($user)) {
                $invite = CompanyInvite::create(Yii::app()->user->model->profile, $model->attributes);

                Yii::app()->getModule('mail')->send($model->email, Config::get('infoEmail'), 'companyInviteNewUser', array(
                    'companyLink' => CHtml::link(CHtml::encode(Yii::app()->user->model->profile->name), Helper::profileUrl(Yii::app()->user->model->profile, true)),
                    'userName' => CHtml::encode(Yii::app()->user->model->full_name),
                    'acceptLink' => CHtml::link(Yii::t('app', 'ссылке'), Yii::app()->createAbsoluteUrl('/auth/inviteRegister', array('code' => $invite->code))),
                ));
            } else {
                $data = $model->attributes;
                $data['id'] = $user->id;
                $invite = CompanyInvite::create(Yii::app()->user->model->profile, $data);

                Yii::app()->getModule('mail')->send($model->email, Config::get('infoEmail'), 'companyInviteExistingUser', array(
                    'companyLink' => CHtml::link(CHtml::encode(Yii::app()->user->model->profile->name), Helper::profileUrl(Yii::app()->user->model->profile, true)),
                    'userName' => CHtml::encode(Yii::app()->user->model->full_name),
                    'acceptLink' => CHtml::link(Yii::t('app', 'ссылке'), Yii::app()->createAbsoluteUrl('/employee/invite', array('code' => $invite->code))),
                ));
            }

            $this->setMessage(Yii::t('profile', 'Приглашение сотрудника'),Yii::t('profile', 'Сотрудник был приглашен в Вашу компанию.'));
            $this->redirect(array('/profile/index'));
        }

        echo CJSON::encode(array(
            'success' => false,
            'data' => $this->renderPartial('_create', array('model' => $model), true, true),
        ));
        Yii::app()->end();
    }

    public function actionUpdate()
    {
        if (!Yii::app()->user->checkAccess('updateEmployee', array('profile' => Yii::app()->user->model->profile, 'employee' => $this->employee)))
            $this->forbidden();

        $model = $this->employee;

        $this->performAjaxValidation($model);

        if (isset($_POST['EmployeeUser'])) {
            $model->attributes = $_POST['EmployeeUser'];
            if ($model->save()) {
                $this->setMessage(Yii::t('profile', 'Редактирование сотрудника'),Yii::t('profile', 'Информация о сотруднике обновлена.'));
                $this->redirect(array('/profile/index'));
            }
        }

        echo CJSON::encode(array(
            'success' => false,
            'data' => $this->renderPartial('_update', array('model' => $model), true, true),
        ));
        Yii::app()->end();
    }

    public function actionDelete()
    {
        if (!Yii::app()->user->checkAccess('deleteEmployee', array('profile' => Yii::app()->user->model->profile, 'employee' => $this->employee)))
            $this->forbidden();

        $model = $this->employee;

        // TODO Проверка остались ли администраторы в компании
        if ($model->fire()) {
            $this->setMessage(Yii::t('profile', 'Увольнение сотрудника'),Yii::t('profile', 'Сотрудник :name уволен из компании :company.', array(
                ':name' => $model->full_name,
                ':company' => Yii::app()->user->model->profile->name
            )));

            Yii::app()->getModule('mail')->send($model->email, Config::get('infoEmail'), 'companyFiredUser', array(
                'companyLink' => CHtml::link(CHtml::encode(Yii::app()->user->model->profile->name), Helper::profileUrl(Yii::app()->user->model->profile, true)),
                'user' => CHtml::encode(Yii::app()->user->model->full_name),
            ));
        }

        $this->redirect(array('/profile/index'));
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'employee-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}