<?php

class ProfileController extends FrontEndController
{
    public $layout = '//layouts/profile';

    public function filters()
    {
        return array(
            'accessControl',
            'ajaxOnly + updateRegion'
        );
    }

    public function init()
    {
        parent::init();

        $this->pageTitle = Yii::t('profile', 'Профиль');
    }

    public function actionIndex()
    {
        $this->render('startPage');
    }

    public function actionUnconfirmedUser()
    {
        if (empty(Yii::app()->user->model->email))
            $this->render('hasNoEmail');
        else
            $this->render('confirmEmail');
    }

    public function actionUpdateInfo()
    {
        $model = Yii::app()->user->model;
        $model->scenario = User::SCENARIO_UPDATE_INFO;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'update-info-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save()) {
                if ($model->emailChanged) {
                    $this->sendEmailConfirmation($model);
                    $this->setMessage(Misc::t('Смена E-mail'), Misc::t('На ваш E-mail выслано письмо с инструкциями по подтрерждению нового адреса.'));
                }
                $this->redirect(array('index'));
            }
        }

        $this->render('updateInfo', array('model' => $model));
    }

    public function actionUpdateEmail()
    {
        $model = Yii::app()->user->model;
        $model->scenario = User::SCENARIO_UPDATE_EMAIL;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'email-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save()) {
                if ($model->emailChanged) {
                    $this->sendEmailConfirmation($model);
                    $this->setMessage(Misc::t('Смена E-mail'), Misc::t('На ваш E-mail выслано письмо с инструкциями по подтрерждению нового адреса.'));
                }
                $this->redirect(array('index'));
            }
        }

        // TODO Add view with form
        $this->redirect(array('index'));
    }

    protected function sendEmailConfirmation($model)
    {
        Yii::app()->getModule('mail')->send($model->email, Config::get('infoEmail'), 'emailConfirmation', array(
            'siteNameLink' => CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')),
            'confirmLink' => CHtml::link(Misc::t('ссылке'), Yii::app()->createAbsoluteUrl('/auth/confirm', array('code' => $model->verify_code)))
        ));
    }

    public function actionChangeVisibility()
    {
        $hidden = Yii::app()->user->getState('profileHidden', false);
        $hidden = !$hidden;
        Yii::app()->user->setState('profileHidden', $hidden);
    }
}
