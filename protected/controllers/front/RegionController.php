<?php

class RegionController extends FrontEndController
{
    public $layout = '//layouts/profile';

    public function filters()
    {
        return array(
            'accessControl',
            'ajaxOnly + create'
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'roles' => array('user'),
                'actions' => array('create'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionCreate()
    {
        $model = new Region;

        if (isset($_POST['Region'])) {
            $model->attributes = $_POST['Region'];
            $model->approved = 0;
            if ($model->save()) {
                echo CJSON::encode(array('success' => true, 'id' => $model->id, 'name' => $model->name));
                Yii::app()->end();
            }
        }

        echo CJSON::encode(array('success' => false));
        Yii::app()->end();
    }
}
