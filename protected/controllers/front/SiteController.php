<?php

class SiteController extends FrontEndController
{
    public function filters() {
        return array(
            'accessControl',
        );
    }
 
    public function actions()
    {
        return CMap::mergeArray(parent::actions(), array(
            'page' => array(
                'class' => 'CViewAction',
            ),
			'captcha'=>array(
                'class'=>'CCaptchaAction',
                'transparent' => true,
				'testLimit'=>'1',
                'backColor'=>0xFFFFFF,
                'maxLength'=> 4,
                'minLength'=> 4,
            ),
        ));
    }
	
	public function actionGetCoord() {
//		error_reporting(E_ALL);
        $address = 'Москва';
        $cmd = 'http://maps.googleapis.com/maps/api/geocode/xml?address='.str_replace(' ','+',$address).'&sensor=false';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $cmd);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $xml = simplexml_load_string(curl_exec($ch));

        $status = $xml->status;

        if ($status == 'OK') {
            $longitude = $xml->result->geometry->location->lng;
            $latitude = $xml->result->geometry->location->lat;
            echo "Ok<br>";
        }
	}

    /* Главная */
    public function actionIndex()
    {
        $this->layout = '//layouts/main';

        $page = Page::model()->findByPk(1);
        $this->render('index',array('page'=>$page));
    }

    public function actionError()
    {
        $this->layout = '//layouts/static';

        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

	public function actionCommunity()
	{
		$this->render('community');
	}
	
	public function actionContactUs()
	{
		$model = new Expert(Expert::SCENARIO_CONTACT_US);
        $page = Page::model()->findByAttributes(array('name' => 'contacts'));
		
		if (isset($_POST['Expert']))
		{
			$model->attributes = $_POST['Expert'];
			
			if($_POST['Expert']['step'] == 1) {
                $model->scenario = Expert::SCENARIO_CONTACT_US_STEP1;
                if ($model->validate()){
				    $model->setAttribute('step',2);
                }
			}
			else {
                $model->scenario = Expert::SCENARIO_CONTACT_US;
				if ($model->validate()) {
					
					if($model->save(false)){
	
						$toEmail = Config::get('expertEmail');
						$answerLink = Yii::app()->createAbsoluteUrl('/expert/default/answer', array('code' => $model->verify_code, 'id' => $model->id));
	
						//Отправляем письмо експерту о новом вопросе
						Yii::app()->getModule('mail')->send($toEmail, Config::get('infoEmail'), 'expertMail',
							array(
								'siteNameLink' => CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')),
								'siteName' => Yii::app()->params['siteName'],
								'created_date' => Yii::app()->dateFormatter->format("dd MMMM yyyy HH:mm", time()),
								'firstname' => ($model->firstname) ? $model->firstname : Yii::t('expert', 'Не указана'),
								'lastname' => ($model->lastname) ? $model->lastname : Yii::t('expert', 'Не указано'),
								'address' => ($model->address) ? $model->address : Yii::t('expert', 'Не указан'),
								'region' => ($model->region) ? $model->region->title : Yii::t('expert', 'Не выбран'),
								'city' => ($model->city) ? $model->city : Yii::t('expert', 'Не указан'),
								'birth' => ($model->birth) ? Yii::app()->dateFormatter->format("LLLL yyyy", $model->birth) : Yii::t('expert', 'Не указана'),
								'answer_email' => ($model->answer_email) ? $model->answer_email : Yii::t('expert', 'Не указан'),
								'answer_phone' => ($model->answer_phone) ? $model->answer_phone : Yii::t('expert', 'Не указан'),
								'question' => $model->question,
								'link' => $answerLink,
							)
						);
	
						$this->setMessage(Misc::t('Информация'), Misc::t('Ваше сообщение отправлено. Мы постараемся ответить Вам в ближайшее время.'));
	
						$this->redirect(Yii::app()->createAbsoluteUrl('/site/contactusthank'));
					}
				} else {
                    $model->setAttribute('step',2);
                }
			}
			
		}

		$this->render('contactus',array('model'=>$model, 'page' => $page));
	}

    public function actionConfirm($code)
    {
		$model = User::model()->findByAttributes(array('verify_code' => $code, 'status' => User::STATUS_NOT_CONFIRMED));
        if (isset($model))
		{
			$username = $model->email;
			$password = $model->password;
			if ($model->confirm($code))
			{
				$identity = new UserIdentity($model->email, $model->password);
				$identity->authconfirm();
				if ($identity->errorCode === UserIdentity::ERROR_NONE) {
					Yii::app()->user->login($identity);
					$this->redirect(array('/profile/startPage'));
				}
				else
				{
					$this->redirect(Yii::app()->homeUrl);
				}
			}
			else
				$this->redirect(Yii::app()->homeUrl);
			}
		else
            $this->redirect(Yii::app()->homeUrl);
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

	public function actionSiteMap()
	{
        $menu = Menu::model()->getMenuItems();
		$this->render('sitemap',array('content'=>$menu));
	}
}