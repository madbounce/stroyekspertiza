<?php

class TenderController extends FrontEndController
{
    public $layout = '//layouts/profile';

    public $tender;

    public function filters()
    {
        return array(
            'accessControl',
            array(
                'EntityContextFilter + update, view, cancel',
                'entity' => 'Tender',
            ),
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'roles' => array('user'),
                'actions' => array('create', 'update', 'view', 'cancel'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionCreate()
    {
        $model = new Tender;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tender-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Tender'])) {
            $model->attributes = $_POST['Tender'];
            $model->user_id = Yii::app()->user->id;
            if (isset($_POST['creator'])) {
                if ($_POST['creator'] == Tender::USER_TYPE_COMPANY) {
                    $model->profile_id = Yii::app()->user->model->profile->id;
                }
            }
            if ($model->save()) {
                // TODO Шаг 2
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array('model' => $model));
    }

    public function actionUpdate()
    {
        $model = $this->tender;

        if (!Yii::app()->user->checkAccess('updateTender', array('tender' => $model)))
            $this->forbidden();

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tender-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['Tender'])) {
            $model->attributes = $_POST['Tender'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array('model' => $model));
    }

    public function actionView()
    {
        $this->render('view', array('model' => $this->tender));
    }

    public function actionCancel()
    {
        $model = $this->tender;

        if (!Yii::app()->user->checkAccess('updateTender', array('tender' => $model)))
            $this->forbidden();

        $model->canceled = 1;
        $model->save();

        $this->redirect(array('view', 'id' => $model->id));
    }
}
