<?php

class UploadController extends FrontEndController
{
    public function actions()
    {
        return array(
            'avatar' => array(
                'class' => 'image.components.UploadAction',
                'entity' => 'User',
                'tag' => 'avatar',
                'directory' => 'user/avatars',
                'view' => 'application.components.widgets.upload.views.avatar',
                'multiple' => false,
            ),
            'logoFreelancer' => array(
                'class' => 'image.components.UploadAction',
                'entity' => 'FreelancerProfile',
                'tag' => 'logo',
                'directory' => 'freelancer/logos',
                'view' => 'application.components.widgets.upload.views.logo',
                'multiple' => false,
            ),
            'logoCompany' => array(
                'class' => 'image.components.UploadAction',
                'entity' => 'CompanyProfile',
                'tag' => 'logo',
                'directory' => 'company/logos',
                'view' => 'application.components.widgets.upload.views.logo',
                'multiple' => false,
            ),
            'freelancerPortfolios' => array(
                'class' => 'portfolio.components.PortfolioUploadAction',
                'entity' => 'FreelancerProfile',
                'directory' => 'freelancer/portfolios',
            ),
            'companyPortfolios' => array(
                'class' => 'portfolio.components.PortfolioUploadAction',
                'entity' => 'CompanyProfile',
                'directory' => 'company/portfolios',
            ),
        );
    }
}
