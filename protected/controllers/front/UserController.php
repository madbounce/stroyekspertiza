<?php

class UserController extends FrontEndController
{
    public $layout = '//layouts/user';

    public $user;
    public $profile;

    public function filters()
    {
        return array(
            'accessControl',
            array(
                'EntityContextFilter + portfolio',
                'entity' => 'Profile',
            ),
            array(
                'EntityContextFilter + view',
                'var' => 'alias',
                'entity' => 'Profile',
            ),
            'ajaxOnly + portfolio'
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('*'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    // TODO Move from user to profile
    public function actionView()
    {
        if ($this->profile->isFreelancer)
            $this->render('viewFreelancer', array('model' => $this->profile->user));
        else if ($this->profile->isCompany)
            $this->render('viewCompany', array('model' => $this->profile->user));
        else
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
    }

    public function actionPortfolio($portfolio_id = null)
    {
        if (empty($portfolio_id))
            $this->renderPartial('_portfolioFull', array('model' => $this->profile));
        else {
            $model = PortfolioImage::model()->findByPk($portfolio_id);
            if (!empty($model))
                $this->renderPartial('_portfolio', array('model' => $model));
        }
        Yii::app()->end();
    }
}