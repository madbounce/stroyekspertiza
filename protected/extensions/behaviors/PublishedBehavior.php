<?php

class PublishedBehavior extends CActiveRecordBehavior
{

    public function published()
    {
        if (!property_exists($this->owner, 'published') )
                return $this->owner;

        $this->owner->getDbCriteria()->mergeWith(array(
            'condition' => 'published = "Y"',
        ));
        return $this->owner;
    }


}

?>