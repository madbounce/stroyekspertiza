<?php

class SortBehavior extends CActiveRecordBehavior
{
    public $sortAttribute = 'sort';

    public function beforeSave($event)
    {
        if($this->getOwner()->isNewRecord){
            $sort = Yii::app()->db->createCommand()
                ->select('MAX(t.' . $this->sortAttribute . ')')
                ->from($this->getOwner()->tableName() . ' t')
                ->queryScalar();

            $sort++;
            $this->getOwner()->{$this->sortAttribute} = $sort;
        }

    }
}
?>