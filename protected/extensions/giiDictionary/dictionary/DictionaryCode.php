<?php
class DictionaryCode extends CCodeModel
{
    public $dictionaryName;
    public $multipleFormsTranslate;
    public $accusative;
    public $baseControllerClass = 'BackEndController';
    public $controllerClass;
    public $tableName;
    public $modelName;
    public $modelPath = 'application.models.dictionaries';
    public $controllerPath = 'application.controllers.back';
    public $dictionaryViewPath = 'application.views.back';
    public $sql;

    public function rules()
    {
        return array_merge(parent::rules(), array(
            array('dictionaryName, multipleFormsTranslate, accusative', 'required'),
            array('dictionaryName', 'match', 'pattern'=>'/^\w+$/'),
        ));
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), array(
            'dictionaryName'=>'Имя словаря',
            'multipleFormsTranslate' => 'Перевод множественных форм слова',
            'accusative' => 'Винительный падеж'
        ));
    }

    public function successMessage()
    {
        $output= 'Словарь успешно сгенерирован. '.CHtml::link('Перейти в словарь', '/backend/' . $this->dictionaryName . '/admin', array('target'=>'_blank')).'.';
        return $output.highlight_string($this->sql,true);
    }

    public function prepare()
    {
        $templatePath=$this->templatePath;
        $this->dictionaryName = strtolower($this->dictionaryName);
        $this->controllerClass = ucfirst($this->dictionaryName) . 'Controller';
        $this->modelName = 'Dic' . ucfirst($this->dictionaryName);
        $this->tableName = 'dic_' .  $this->dictionaryName;


        //Если нет директории для словарей - создаем
        if(!file_exists($this->modelPath)){
            mkdir($this->modelPath);
        }
        //Создаем контроллер
        $this->files[]=new CCodeFile(
            Yii::getPathOfAlias($this->controllerPath).'/'.$this->controllerClass.'.php',
            $this->render($templatePath.'/controller.php')
        );

        //Создаем модель
        $this->files[]=new CCodeFile(
            Yii::getPathOfAlias($this->modelPath).'/'. $this->modelName.'.php',
            $this->render($templatePath.'/model.php')
        );

        //Создаем форму
        $this->files[]=new CCodeFile(
            Yii::getPathOfAlias($this->dictionaryViewPath).'/'. strtolower($this->dictionaryName) .'/' . '_form.php',
            $this->render($templatePath.'/form.php')
        );

        //Создаем update view
        $this->files[]=new CCodeFile(
            Yii::getPathOfAlias($this->dictionaryViewPath).'/'.strtolower($this->dictionaryName).'/' . 'update.php',
            $this->render($templatePath.'/update.php')
        );


        //Создаем create view
        $this->files[]=new CCodeFile(
            Yii::getPathOfAlias($this->dictionaryViewPath).'/'.strtolower($this->dictionaryName).'/' . 'create.php',
            $this->render($templatePath.'/create.php')
        );

        //Создаем admin view
        $this->files[]=new CCodeFile(
            Yii::getPathOfAlias($this->dictionaryViewPath).'/'.strtolower($this->dictionaryName).'/' . 'admin.php',
            $this->render($templatePath.'/admin.php')
        );
    }

    public function save()
    {
       if(parent::save()){
           $tableName = Yii::app()->db->tablePrefix . $this->tableName;
           //Create Dictionary Table
           $this->sql = "CREATE TABLE $tableName (
                    id INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                    title VARCHAR( 255 ) NOT NULL ,
                    sort INT( 11 ) NULL ,
                    is_active TINYINT( 1 ) NOT NULL DEFAULT  '1'
                    ) ENGINE = INNODB;";

           $command = Yii::app()->db->createCommand($this->sql);

           return $command->query();
       } else
           return false;
    }
}