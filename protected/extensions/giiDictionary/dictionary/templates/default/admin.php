<?php
$multForms = explode('|', $this->multipleFormsTranslate);

echo "<?php\n"; ?>
$dp = $model->adminSearch();
$countAll = $dp->totalItemCount;
?>

<div class="page-header admin-header">
    <h3><?php echo "<?php"; ?> echo $countAll . ' ' . Yii::t('admin', '<?php echo $this->multipleFormsTranslate; ?>', $countAll);?></h3>
    <?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbButton', array(
        'label' => Yii::t('admin', 'Добавить <?php echo $this->accusative; ?>'),
        'size' => 'small',
        'url' => array('/<?php echo $this->dictionaryName; ?>/create'),
    ));
    ?>
</div>
<div class="page-content">
    <?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbExtendedGridView', array(
        'id' => '<?php echo $this->dictionaryName; ?>-grid',
        'type'=>'striped bordered',
        'dataProvider' => $dp,
        'enableSorting' => false,
        'sortableRows' => true,
        'sortableAjaxSave'=>true,
        'sortableAttribute'=>'sort',
        'sortableAction'=> '/<?php echo $this->dictionaryName; ?>/sortable',
        'template' => '{pager}{items}{pager}',
        'columns' => array(
            array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'title',
                'type'=>'raw',
                'value' => '$data->title',
                'editable' => array(
                    'url' => $this->createUrl('/<?php echo $this->dictionaryName; ?>/editable'),
                    'placement' => 'right',
                    'inputclass' => 'span4',
                    'title' => Yii::t('admin', 'Введите название'),
                    'success' => 'js: function(data) {
                        if(typeof data == "object" && !data.success) return data.msg;
                    }'
                ),
            ),
            array(
                'class' => 'bootstrap.widgets.TbToggleColumn',
                'name' => 'is_active',
                'filter' => array(0 => Yii::t('admin', 'Нет'), 1 => Yii::t('admin', 'Да')),
                'checkedButtonLabel' => Yii::t('admin', 'Сделать неактивным'),
                'uncheckedButtonLabel' => Yii::t('admin', 'Сделать активным'),
                'toggleAction'=>'toggle',
                'htmlOptions' => array(
                    'width' => '60',
                ),
            ),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{delete}'
            ),
        ),
    ));?>
</div>