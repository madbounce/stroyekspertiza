<?php
/**
 * This is the template for generating a dictionary controller class file.
 */
?>
<?php echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass."\n"; ?>
{
    //public $layout = '//layouts/main';
    public $defaultAction = 'admin';

    public function actions(){
        return array(
            'sortable' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => '<?php echo $this->modelName; ?>'
            )
        );
    }

    public function actionUpdate($id){
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        if (isset($_POST['<?php echo $this->modelName; ?>'])) {

            $model->attributes = $_POST['<?php echo $this->modelName; ?>'];

            if ($model->save()) {

                if (isset($_POST['more']))
                    $this->redirect(array('create'));

                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionCreate(){
        $model = new <?php echo $this->modelName; ?>;

        $this->performAjaxValidation($model);

        if (isset($_POST['<?php echo $this->modelName; ?>'])) {

            $model->attributes = $_POST['<?php echo $this->modelName; ?>'];

            if ($model->save()) {

                if (isset($_POST['more']))
                    $this->redirect(array('create'));

                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Неверный запрос. Пожалуйста, не повторяйте этот запрос еще раз.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $model = new <?php echo $this->modelName; ?>('adminSearch');

        $model->unsetAttributes();

        if (isset($_GET['<?php echo $this->modelName; ?>']))
            $model->attributes = $_GET['<?php echo $this->modelName; ?>'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionEditable()
    {
        if (isset($_POST['pk']) && isset($_POST['name']) && isset($_POST['value'])) {
            $model = $this->loadModel($_POST['pk']);
            $model->title = $_POST['value'];

            if ($model->save()) {
                echo CJSON::encode(array('success' => true));
                Yii::app()->end();
            }

            echo CJSON::encode(array('success' => false, 'msg' => $model->getError($_POST['name'])));
            Yii::app()->end();
        }

        echo CJSON::encode(array('success' => false));
        Yii::app()->end();
    }

    public function actionToggle($id,$attribute) {
        $model = <?php echo $this->modelName; ?>::model()->findByPk($id);
        if($model){
            if($model->is_active == 1)
                $model->is_active = 0;
            else
                $model->is_active = 1;
        }
        $model->save(false);
    }

    public function loadModel($id)
    {
        $model = <?php echo $this->modelName; ?>::model()->findByPk($id);

        if ($model === null)
            throw new CHttpException(404, 'Запрошенная страница не существует.');

        return $model;
    }

    protected function performAjaxValidation($model, $form = '<?php echo $this->dictionaryName; ?>-form')
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] == $form) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}