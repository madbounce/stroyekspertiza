<?php
$multForms = explode('|', $this->multipleFormsTranslate);
?>
<div class="page-header">
    <h3><?php echo "<?php"; ?> echo Yii::t('admin', 'Создание <?php echo $multForms[1]?>'); ?></h3>
</div>

<div class="page-content">
    <?php echo "<?php"; ?> echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>