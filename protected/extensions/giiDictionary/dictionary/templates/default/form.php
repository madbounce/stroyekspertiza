<?php echo "<?php\n"; ?>
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => '<?php echo $this->dictionaryName; ?>-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
    ),
    'htmlOptions' => array(
        'class' => 'product'
    ),
));
?>

<?php echo "<?php"; ?> echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo "<?php"; ?> echo $form->textField($model, 'title', array('class' => 'name', 'maxlength' => 255)); ?>
    <div class="checkbox">
        <?php echo "<?php"; ?> echo $form->checkBox($model, 'is_active')?>
        <?php echo "<?php"; ?> echo $form->labelEx($model, 'is_active')?>
    </div>
</div>

<div class="form-actions">
    <?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? Yii::t('admin', 'Создать') : Yii::t('admin', 'Сохранить'),
    )); ?>

    <?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'htmlOptions' => array(
            'name' => 'more',
        ),
        'label' => Yii::t('admin', 'Сохранить и добавить еще'),
    )); ?>
</div>

<?php echo "<?php"; ?> $this->endWidget(); ?>

