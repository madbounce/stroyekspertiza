<h1>Генератор словарей</h1>

<?php $form=$this->beginWidget('CCodeForm', array('model'=>$model));?>

<div class="row">
    <?php echo $form->labelEx($model,'dictionaryName'); ?>
    <?php echo $form->textField($model,'dictionaryName',array('size'=>65)); ?>
    <div style="display: none">Имя словаря - буквы латинского алфавита.</div>
    <?php echo $form->error($model,'dictionaryName'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'multipleFormsTranslate'); ?>
    <?php echo $form->textField($model,'multipleFormsTranslate',array('size'=>65)); ?>
    <div style="display: none">Н-р: город|города|городов</div>
    <?php echo $form->error($model,'multipleFormsTranslate'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'accusative'); ?>
    <?php echo $form->textField($model,'accusative',array('size'=>65)); ?>
    <div style="display: none">Н-р: город (добавить что? город)</div>
    <?php echo $form->error($model,'accusative'); ?>
</div>

<?php $this->endWidget(); ?>