<?php

class ERedactorUploadAction extends CAction
{
    public $directoryAlias = 'webroot.images.redactor';

    public function run()
    {
        $path = Yii::getPathOfAlias($this->directoryAlias);
        if (!is_dir($path))
            mkdir($path, 0777, true);

        $file = CUploadedFile::getInstanceByName('file');

        if(isset($_GET['is_retina']) && ($_GET['is_retina'])){
            $fname = md5(microtime() . $file->name);

            $filename2x = $fname . '@2x.' . $file->getExtensionName();
            $image2x = $path . '/' . $filename2x;
            $file->saveAs($image2x);

            //создаем изображение source
            list($width, $height) = getimagesize($image2x);
            Yii::import('image.extensions.phpThumb.PhpThumbFactory');
            $options = array('jpegQuality' => 100);

            $filename = $fname . '.' . $file->getExtensionName();
            $image = $path . '/' . $filename;


            $thumb = PhpThumbFactory::create($image2x, $options);
            $thumb->resize(intval($width / 2) , 0);
            $thumb->save($image);

            echo CJSON::encode(array(
                'filelink' => self::getUrlOfAlias($this->directoryAlias) . '/' . $filename,
            ));
        } else {
            $filename = md5(microtime() . $file->name) . '.' . $file->getExtensionName();
            $file->saveAs($path . '/' . $filename);
            echo CJSON::encode(array(
                'filelink' => self::getUrlOfAlias($this->directoryAlias) . '/' . $filename,
            ));
        }

    }

    public static function getUrlOfAlias($alias, $includeBase = true)
    {
        if ($path = Yii::getPathOfAlias($alias))
        {
            $webrootPath = Yii::getPathOfAlias('webroot');
            if(strpos($path, $webrootPath) !== FALSE) {
                $path = substr($path, strlen($webrootPath));
            }
            return str_replace('\\', '/', $includeBase ? Yii::app()->baseUrl . $path : $path);
        }

        return false;
    }
}
