<?php

class m131210_185923_add_is_puppy_column extends CDbMigration
{
    public function up()
    {
        $this->addColumn('{{user}}','is_puppy','tinyint(1) default null');
    }

    public function down()
    {
        $this->dropColumn('{{user}}','is_puppy');
    }
}