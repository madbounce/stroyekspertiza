<?php

/**
 * This is the model class for table "activity".
 *
 * The followings are the available columns in table 'activity':
 * @property string $id
 * @property string $parent_id
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Activity $parent
 * @property Activity[] $activities
 * @property Profile[] $profiles
 */
class Activity extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{activity}}';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('parent_id', 'length', 'max' => 10),
            array('name', 'length', 'max' => 255),

            array('id, parent_id, name', 'safe', 'on' => 'adminSearch'),

            array('parent_id', 'default', 'value' => null),
        );
    }

    public function relations()
    {
        return array(
            'parent' => array(self::BELONGS_TO, 'Activity', 'parent_id'),
            'activities' => array(self::HAS_MANY, 'Activity', 'parent_id'),
            'profiles' => array(self::MANY_MANY, 'Profile', 'profile_activity(activity_id, profile_id)'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'parent_id' => Misc::t('Родитель'),
            'name' => Misc::t('Название'),
        );
    }

    public function adminSearch()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('parent_id', $this->parent_id, false);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
    }

    /**
     * General
     */

    public static function getItemOptions($allTitle = null)
    {
        if (empty($allTitle))
            $allTitle = Yii::t('admin', 'Все');

        $models = Activity::model()->parent()->with(array(
            'activities.activities' => array('alias' => 'subactivities'),
            'activities.activities.activities' => array('alias' => 'subactivities2')
        ))->findAll();

        $items = array('' => $allTitle);
        foreach($models as $model)
        {
            $items[$model->id] = $model->name;
        }

        return $items;
    }

    /**
     * Criteria
     */

    public function getParentCriteria($parent = null)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        if (!empty($parent)) {
            $criteria->addCondition("$alias.parent_id = : parent_id");
            $criteria->params = array(':parent_id' => $parent->id);
        } else {
            $criteria->addCondition("$alias.parent_id IS NULL");
        }
        return $criteria;
    }

    /**
     * Scopes
     */

    public function parent($parent = null)
    {
        $this->getDbCriteria()->mergeWith($this->getParentCriteria($parent));
        return $this;
    }
}