<?php

class Articles extends CActiveRecord
{
	public $rate;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{articles}}';
	}
	
	protected function beforeSave()
    {
    if (parent::beforeSave())
		{
			if ($this->isNewRecord) {
				$this->update_time = date('Y-m-d H:i:s', time());
				$this->create_time = date('Y-m-d H:i:s', time());
			} else {
				$this->update_time = date('Y-m-d H:i:s', strtotime(str_replace('/','.',$this->update_time)));
			}
		}
		return true;
    }

    protected function afterSave()
    {
        parent::afterSave();

        Search::updateIndex(get_class($this), $this->id);
    }

	public function rules()
	{
		return array(
			array('material_category_id, title, content', 'required'),
			array('material_category_id,age,size', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('content_short, image', 'length', 'max'=>255),
			array('material_category_id, title, content, content_short, image, create_time, update_time, slug, meta_title, meta_description, meta_keywords, topic, image_alt,age,size,type', 'safe'),
			array('material_category_id, title, content, content_short, image, create_time, update_time, slug,age,size,type', 'safe', 'on'=>'search'),
                   
		);
	}
   
	public function relations()
	{
		return array( 
			'category' => array(self::BELONGS_TO, 'ArticleCategory', 'material_category_id'),
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
		);
	}
  
	public function behaviors()
	{
		return array(
				//'published'=>array('class'=>'ext.behaviors.PublishedBehavior'),
				'SlugBehavior' => array(
				  'class' => 'ext.behaviors.SlugBehavior',
				  'sourceAttribute' => 'title',
				  'slugAttribute' => 'slug',
				  //'mode' => 'translate',
				),
                'image' => array(
                    'class' => 'image.components.ImageBehavior',
                    'tag' => 'image',
                    'multiple' => true,
                ),
			);
	}

	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'ID'),
			'material_category_id' => Yii::t('app', 'Категория'),
            'rubric_id' => Yii::t('app', 'Рубрика'),
			'title' => Yii::t('app', 'Заголовок'),
			'slug' => Yii::t('app', 'Page URL'),
			'content' => Yii::t('app', 'Текст статьи'),
			'content_short' => Yii::t('app', 'Краткое описание'),
			'image' => Yii::t('app', 'Изображение'),
			'flash' => Yii::t('app', 'Flash'),
			'rating_value' => Yii::t('app', 'Rating Value'),
			'rating_count' => Yii::t('app', 'Rating Count'),
			'tags' => Yii::t('app', 'Теги'),
			'create_time' => Yii::t('app', 'Create Time'),
			'update_time' => Yii::t('app', 'Дата'),
			'author_id' => Yii::t('app', 'Автор'),
			'isAuto' => Yii::t('app', 'Is Auto'),
			'url' => Yii::t('app', 'Ссылка'),
            'authortext'=>'Автор текстом',
			'is_rss'=>'отправить в rss',
			'meta_title'=>'Заголовок',
			'meta_description'=>'Описание',
			'meta_keywords'=>'Ключевые слова',
			'topic'=>'topic',
			'age'=>'Возраст',
			'size'=>'Размер',
			'type'=>'Кошка или собака',
			'image_alt'=>'Background Image Alt Tag'
		);
	}


    public function adminSearch()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('id',$this->id);
        //$criteria->compare('material_category_id',$this->material_category_id);
        //$criteria->compare('title',$this->title,true);

        if(isset($_GET['cId'])){
            $cid = (int)$_GET['cId'];
            $criteria->addCondition('t.material_category_id =' . $cid);
        }

        if(isset($_GET['search'])){
            $criteria->addSearchCondition('title', $_GET['search']);
        }

        //$criteria->order= 'update_time desc';

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
            'pagination'=>array('pageSize' => Yii::app()->params->pageSize)
        ));
    }



	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('material_category_id',$this->material_category_id);
		$criteria->compare('title',$this->title,true);
		//$criteria->compare('content',$this->content,true);
		//$criteria->compare('content_short',$this->content_short,true);
		
		$criteria->order= 'update_time desc';
		 
		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}



        
	protected function beforeFind() {
		parent::beforeFind();
		$this->getDbCriteria()->order='id DESC';
	
	}
	
	public function getPhoto($width = 100, $height = 100, $options = null){
        $image = Image::model()->entity('Articles', $this->id)->tag('image')->find();
        if($image){
            $thumbPath = Yii::app()->baseUrl . ImageHelper::thumb($width, $height, $image->url, $options);
            return $thumbPath;
        } else
            return null;
    }
	
	/*
	public function getListerConditioner($suff = null)
  	{
		if(!$suff)
		{
			$suff = '_LC_SEARCH';
		}
	
		yii::import('application.components.portlet.lister.ListerConditioner');
		$lc = new ListerConditioner($this, $suff);
		$lc->orderByFields = array(
			't.update_time' => 'По дате',          
		);
		$lc->itemsPerPage=10;      
		$lc->availableIPP = array(10, 20, 30);
	
		$lc->orderBySelected['direction']='DESC';
		$lc->orderBySelected['field'] = 't.update_time';
	
		return $lc;
  	}
	*/
}
