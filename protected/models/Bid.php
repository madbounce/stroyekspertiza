<?php

/**
 * This is the model class for table "bid".
 *
 * The followings are the available columns in table 'bid':
 * @property string $id
 * @property string $user_id
 * @property string $tender_id
 * @property integer $canceled
 * @property string $cost
 * @property string $currency
 * @property string $term
 * @property string $phone
 * @property string $email
 * @property string $comment
 * @property string $create_time
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property Tender $tender
 * @property User $user
 */
class Bid extends CActiveRecord
{
    public $status;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{bid}}';
    }

    public function rules()
    {
        return array(
            array('user_id, tender_id, cost, currency, term, phone, email, comment', 'required'),
            array('canceled', 'numerical', 'integerOnly' => true),
            array('user_id, tender_id, term', 'length', 'max' => 10),
            array('cost, currency', 'length', 'max' => 16),
            array('phone, email', 'length', 'max' => 255),

            array('id, user_id, tender_id, canceled, cost, currency, term, phone, email, comment, create_time, update_time', 'safe', 'on' => 'search'),

            array('email', 'email'),
            array('user_id, tender_id', 'unsafe'),

            array('status', 'safe', 'on' => 'profileStatusSearch'),
        );
    }

    public function relations()
    {
        return array(
            'tender' => array(self::BELONGS_TO, 'Tender', 'tender_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'user_id' => 'User',
            'tender_id' => 'Tender',
            'canceled' => 'Canceled',
            'cost' => Yii::t('tender', 'Полная стоимость проекта'),
            'currency' => 'Currency',
            'term' => Yii::t('tender', 'Срок выполнения (дней)'),
            'phone' => Yii::t('tender', 'Телефон для связи'),
            'email' => Yii::t('tender', 'E-mail для связи'),
            'comment' => Yii::t('tender', 'Сообщение'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('tender_id', $this->tender_id, true);
        $criteria->compare('canceled', $this->canceled);
        $criteria->compare('cost', $this->cost, true);
        $criteria->compare('currency', $this->currency, true);
        $criteria->compare('term', $this->term, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function profileStatusSearch()
    {
        $criteria = new CDbCriteria;
        $criteria->mergeWith($this->getUserCriteria(Yii::app()->user->id));

        switch ($this->status) {
            case Helper::STATUS_ACTUAL:
                $criteria->mergeWith($this->getActualCriteria());
                break;
            case Helper::STATUS_ENDED:
                $criteria->mergeWith($this->getEndedCriteria());
                break;
            case Helper::STATUS_CANCELED:
                $criteria->mergeWith($this->getCanceledCriteria(true));
                break;
            default:
                $criteria->mergeWith($this->getActualCriteria());
                break;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
            ),
            'CommentableBehavior' => array(
                'class' => 'comment.components.CommentableBehavior',
            ),
            'filesaver' => array(
                'class' => 'filesaver.components.FileSaverBehavior',
                'savelocation' => 'bid',
                'entity' => 'Bid'
            ),
        );
    }

    protected function afterFind()
    {
        parent::afterFind();

        if ($this->canceled || $this->tender->status == Helper::STATUS_CANCELED)
            $this->status = Helper::STATUS_CANCELED;
        else {
            if ($this->tender->status == Helper::STATUS_ACTUAL)
                $this->status = Helper::STATUS_ACTUAL;
            else
                $this->status = Helper::STATUS_ENDED;
        }
    }

    /**
     * General
     */

    public function accept()
    {
        $this->tender->winner_bid_id = $this->id;
        return $this->tender->save();
    }

    public function getFormattedCost()
    {
        return Yii::app()->numberFormatter->formatCurrency($this->cost, $this->currency);
    }

    /**
     * Criteria
     */

    public function getUserCriteria($user_id)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->compare("$alias.user_id", $user_id);
        return $criteria;
    }

    public function getTenderCriteria($tender_id)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->compare("$alias.tender_id", $tender_id);
        return $criteria;
    }

    public function getCanceledCriteria($canceled = false)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->with = array('tender');
        $criteria->addCondition($canceled ? "$alias.canceled = 1" : "$alias.canceled = 0");
        $criteria->addCondition($canceled ? "tender.canceled = 1" : "tender.canceled = 0", 'OR');
        return $criteria;
    }

    public function getActualCriteria()
    {
        $criteria = new CDbCriteria;
        $criteria->with = array('tender:isActual' => array('joinType' => 'RIGHT JOIN'));
        $criteria->together = true;
        $criteria->mergeWith($this->getCanceledCriteria(false));
        return $criteria;
    }

    public function getEndedCriteria()
    {
        $criteria = new CDbCriteria;
        $criteria->with = array('tender:isEnded' => array('joinType' => 'RIGHT JOIN'));
        $criteria->together = true;
        $criteria->mergeWith($this->getCanceledCriteria(false));
        return $criteria;
    }

    /**
     * Scopes
     */

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);
        return array(
            'order' => "$alias.create_time DESC",
        );
    }

    public function byUser($user_id)
    {
        $this->getDbCriteria()->mergeWith($this->getUserCriteria($user_id));
        return $this;
    }

    public function byTender($tender_id)
    {
        $this->getDbCriteria()->mergeWith($this->getTenderCriteria($tender_id));
        return $this;
    }

    public function isActual()
    {
        $this->getDbCriteria()->mergeWith($this->getActualCriteria());
        return $this;
    }

    public function isEnded()
    {
        $this->getDbCriteria()->mergeWith($this->getEndedCriteria());
        return $this;
    }

    public function isCanceled($canceled)
    {
        $this->getDbCriteria()->mergeWith($this->getCanceledCriteria($canceled));
        return $this;
    }
}