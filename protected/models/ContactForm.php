<?php

/**
 * ContactForm class.
 */
class ContactForm extends CFormModel
{
    public $pet;
	public $nickname;
	public $cat_birth;
	public $size;
	public $food;
    public $text;
	public $firstname;
	public $lastname;
	public $address;
	public $city;
	public $country;
    public $email;
    public $phone;
	public $d_birth;
	public $verifyCode;
	
	public static $cat_pets = array (
	'Abyssinian'=>'Abyssinian',
	'American Shorthair'=>'American Shorthair',
	'Bombay'=>'Bombay',
	'Bengal'=>'Bengal',
	'Birman'=>'Birman',
	'British Shorthair'=>'British Shorthair',
	'Burmese'=>'Burmese',
	'Cornish Rex'=>'Cornish Rex',
	'Devon Rex'=>'Devon Rex',
	'Domestic Longhair'=>'Domestic Longhair',
	);


    public function rules()
    {
        return array(
            array('firstname, lastname, email, d_birth, text, address', 'required', 'message'=>'Необходимо заполнить поле {attribute}.'),
            array('email','email'),
            array('address, email, phone', 'length', 'max'=>255),
            array('pet, food, nickname, cat_birth, cat_size, country, city', 'safe'),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
        );

    }

    public function attributeLabels()
    {
        return array(
			'pet'=>Yii::t('app','Какая порода у Вашей кошки'),
			'nickname'=>Yii::t('app','Имя Вашей кошки'),
			'cat_birth'=>Yii::t('app','День рождения Вашей кошки'),
			'size'=>Yii::t('app','Размер'),
			'food'=>Yii::t('app','Чем питается Ваша кошка'),
            'text'=>Yii::t('app','Что Вы хотите рассказать экспертам?'),
			'firstname'=>Yii::t('app','Фамилия'),
			'lastname'=>Yii::t('app','Имя'),
			'address'=>Yii::t('app','Адрес'),
			'city'=>Yii::t('app','Город'),
			'country'=>Yii::t('app','Страна'),
            'email'=>Yii::t('app','E-mail для ответа'),
            'phone'=>Yii::t('app','Телефон для ответа'),
			'd_birth'=>Yii::t('app','Дата рождения'),
			'verifyCode'=>Yii::t('app','Код проверки'),
        );
    }
}