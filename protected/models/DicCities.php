<?php

class DicCities extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{dic_cities}}';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('title', 'length', 'max' => 255),
            array('sort', 'numerical', 'integerOnly'=>true),
        );
    }

    public function behaviors(){
        return array(
            'SortBehavior' => array(
                'class' => 'ext.behaviors.SortBehavior',
            ),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => Yii::t('country', 'Название'),
            'sort' => Yii::t('country', 'Сортировка'),
        );
    }

    public function adminSearch()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('title', $this->title, true);

        $criteria->order = 'sort ASC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10000
            ),

        ));
    }

    public static function all(){
        $criteria = new CDbCriteria();
        $criteria->order = 't.sort ASC';
        return self::model()->findAll($criteria);
    }
	
	public function searchPoint($address='')
	{
	   $address = urldecode($address);
	  
	   $url = "http://maps.google.com/maps/geo?q={$address}&output=json";
	  
	   $ch = curl_init();
	  
	   curl_setopt($ch, CURLOPT_URL, $url);
	   curl_setopt($ch, CURLOPT_HEADER,0);
	   curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
	   curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	  
	   $data = curl_exec($ch);
	   curl_close($ch);
	  
	   $data = CJSON::decode($data);
	   
	   $coord = $data['Placemark'][0]['Point']['coordinates'];
	   return array('lng'=>$coord[0],'lat'=>$coord[1]);
	}
}