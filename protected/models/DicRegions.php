<?php

class DicRegions extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{dic_regions}}';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('title', 'length', 'max' => 255),
            array('sort', 'numerical', 'integerOnly'=>true),
        );
    }

    public function behaviors(){
        return array(
            'SortBehavior' => array(
                'class' => 'ext.behaviors.SortBehavior',
            ),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => Yii::t('country', 'Название'),
            'sort' => Yii::t('country', 'Сортировка'),
        );
    }

    public function adminSearch()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.title', $this->title, true);

        $criteria->order = 't.sort ASC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 99999
            ),

        ));
    }

    public static function listAll(){
        $criteria = new CDbCriteria();
        $criteria->order = 't.sort ASC';
        return CHtml::listData(self::model()->findAll($criteria), 'id', 'title');
    }
}