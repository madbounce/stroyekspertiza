<?php

class Filters extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{filters}}';
    }

    public function rules()
    {
        return array(
            array('name, value', 'required'),
            array('name, value', 'length', 'max' => 255),
        );
    }

//    public function behaviors(){
//        return array(
//            'SortBehavior' => array(
//                'class' => 'ext.behaviors.SortBehavior',
//            ),
//        );
//    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => Yii::t('filters', 'Название'),
            'value' => Yii::t('filters', 'Значение'),
        );
    }

    public function adminSearch()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.name', $this->name, true);

        $criteria->order = 't.name ASC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 99999
            ),

        ));
    }

    public static function listAll(){
        $criteria = new CDbCriteria();
        $criteria->order = 't.name ASC';
        return CHtml::listData(self::model()->findAll($criteria), 'id', 'name');
    }
}