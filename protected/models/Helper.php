<?php

class Helper
{
    const STATUS_ACTUAL = 0;
    const STATUS_ENDED = 1;
    const STATUS_CANCELED = 2;

    public static function getCurrencies()
    {
        return array(
            'RUB' => Yii::t('currency', 'руб'),
            'EUR' => '€',
            'USD' => '$'
        );
    }

    public static function formatCommentTime($time)
    {
        return Yii::app()->dateFormatter->format(Yii::app()->locale->getTimeFormat('short') . ' ' . Yii::app()->locale->getDateFormat('medium'), $time);
    }

    public static function profileUrl(Profile $profile, $absolute = false)
    {
        if (!$absolute)
            return array('/user/view', 'alias' => $profile->alias);
        else
            return Yii::app()->createAbsoluteUrl('/user/view', array('alias' => $profile->alias));
    }
}
