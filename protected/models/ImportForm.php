<?php

class ImportForm extends CFormModel
{
    public $file;

    public function rules()
    {
        return array(
            array('file', 'file', 'types' => 'xls, xlsx', 'allowEmpty' => false, 'message' => Yii::t('entries', 'Файл не выбран или неверный формат файла.')),
        );
    }

    public function attributeLabels()
    {
        return array(
            'file' => Yii::t('entries', 'Загрузка xls, xlsx')
        );
    }

    public function beforeValidate()
    {
        if ($file = CUploadedFile::getInstance($this, 'file'))
            $this->file = $file;

        return parent::beforeValidate();
    }

    public function getFile()
    {
        $path = Yii::app()->runtimePath . '/import';
        if (!is_dir($path))
            mkdir($path, 0700, true);

        $this->file->saveAs($path . '/' . $this->file->name);

        return $path . '/' . $this->file->name;
    }
}