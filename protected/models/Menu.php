<?php

/**
 * This is the model class for table "region".
 *
 * The followings are the available columns in table 'region':
 * @property string $id
 * @property string $parent_id
 * @property string $name
 * @property integer $approved
 *
 * The followings are the available model relations:
 * @property Region $parent
 * @property Region[] $regions
 */
class Menu extends CActiveRecord
{
	public $submenu;
	
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{menu}}';
    }

    public function behaviors(){
        return array(
            'SortBehavior' => array(
                'class' => 'ext.behaviors.SortBehavior',
                'sortAttribute' => 'order'
            ),
        );
    }

    public function rules()
    {
        return array(
            array('name, url, place', 'required'),
			//array('url', 'url'),
            array('approved,width, height', 'numerical', 'integerOnly' => true),
            array('parent_id', 'length', 'max' => 10),
            array('name, url', 'length', 'max' => 255),
            array('image', 'length', 'max' => 50),
            array('place', 'length', 'max' => 20),
            array('id, parent_id, name, approved, url, order, place, lang', 'safe', 'on' => 'adminSearch'),
            array('id, parent_id, name, approved, url, order, place, lang', 'safe'),

            array('parent_id', 'default', 'value' => 0),
            array('approved, width, height, image, submenu', 'unsafe', 'except' => 'adminSearch'),
        );
    }

    public function relations()
    {
        return array(
            'parent' => array(self::BELONGS_TO, 'Menu', 'parent_id'),
            'regions' => array(self::HAS_MANY, 'Menu', 'parent_id','order'=>'regions.order ASC'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
			'url' => Misc::t('Ссылка'),
			'order' => Misc::t('Порядок'),
            'parent_id' => Misc::t('Родительский пункт'),
            'name' => Misc::t('Название'),
            'approved' => Misc::t('Подтвержден'),
			'place' => Misc::t('Положение'),
        );
    }

    public function searchForTree() {
		return Menu::model()->findAll(array('condition'=>'parent_id=0'));
	}
	
	public function adminSearch($place = 'top')
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('approved', $this->approved);
		$criteria->compare('place', $place);

        $criteria->order = 't.order ASC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
    }

    /**
     * General
     */

    public static function getItemOptions($allTitle = null)
    {
        if (empty($allTitle))
            $allTitle = Yii::t('admin', 'Все');

        $models = Menu::model()->parent()->with(array(
            'regions.regions' => array('alias' => 'subregions'),
            'regions.regions.regions' => array('alias' => 'subregions2')
        ))->findAll();

        $items = array('' => $allTitle);
        foreach ($models as $model) {
            $items[$model->id] = $model->name;
            foreach ($model->regions as $model2) {
                $items[$model2->id] = '--' . $model2->name;
            }
        }

        return $items;
    }
	
	/**
	 * Выбор по указанному региону
	*/
	
	public function getItemOptionsSelected()
    {

        $models = Menu::model()->findAll(array('condition' => "t.parent_id = {$this->id}"));

        $items = array();
        foreach ($models as $model) {
            foreach ($model->regions as $model2) {
                $items[$model2->id] = $model2->name;
            }
        }

        return $items;
    }
	
	public function getMenuParentItems()
	{
		$model = Menu::model()->findAll(array('condition'=>"parent_id = 0"));
		
		return $model;
	}

	public function getMenuItems($place = 'top')
	{
		$model = Menu::model()->findAll(array('condition'=>"parent_id=0 AND place='".$place."'",'order'=>'t.order'));
		foreach($model as $mod) {
			$sub = Menu::model()->findAll(array('condition'=>'parent_id='.$mod->id,'order'=>'t.order'));
			if (isset($sub) && $sub)
				$mod->submenu = $sub;
		}
		
		return $model;
	}

    /**
     * Criteria
     */

    public function getParentCriteria($parent = null)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        if (!empty($parent)) {
            $criteria->addCondition("$alias.parent_id = : parent_id");
            $criteria->params = array(':parent_id' => $parent->id);
        } else {
            $criteria->addCondition("$alias.parent_id IS NULL");
        }
        return $criteria;
    }

    public function getApprovedCriteria($approved = true)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->compare("$alias.approved", $approved ? 1 : 0);
        return $criteria;
    }

    /**
     * Scopes
     */

    public function approved($approved = true)
    {
        $this->getDbCriteria()->mergeWith($this->getApprovedCriteria($approved));
        return $this;
    }

    public function parent($parent = null)
    {
        $this->getDbCriteria()->mergeWith($this->getParentCriteria($parent));
        return $this;
    }
}