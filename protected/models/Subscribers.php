<?php

class Subscribers extends CActiveRecord
{
    const SCENARIO_STEP_TWO = 'step-two'; //Второй шаг присоединится
    const SCENARIO_STEP_THREE = 'step-three'; //Третий шаг присоединится

    public $pet1Month;
    public $pet1Year;
    public $pet2Month;
    public $pet2Year;
    public $pet3Month;
    public $pet3Year;
    public $pet4Month;
    public $pet4Year;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{subscribers}}';
	}

	public function rules()
	{
		return array(
			array('unsubscribe_key', 'required'),
            array('dryFoodFreq, wetFoodFreq', 'required', 'on' => self::SCENARIO_STEP_THREE),

			array('type_subscribe, uid, dryFoodFreq, wetFoodFreq, petFoodBuyWhen', 'numerical', 'integerOnly'=>true),
			array('unsubscribe_key', 'length', 'max'=>255),
			array('catCount, petTypeKitten, petTypeCat, petTypePup, petTypeDog, petBuyPlan, havePet, pet1Month, pet1Year, pet2Month, pet2Year, pet3Month, pet3Year, pet4Month, pet4Year, dryFoodBrands, dryFoodBrandsOther', 'safe'),
            array('pet1_birth, pet1_name, pet2_birth, pet2_name, pet3_birth, pet3_name, pet4_birth, pet4_name', 'safe'),
		);

	}

    protected function beforeValidate()
    {

        if ($this->scenario == self::SCENARIO_STEP_TWO) {
            if($this->havePet){
                $count = $this->catCount;
                for($i=1; $i<=$count; $i++){
                    $attributeDate = 'pet' . $i . '_birth';
                    $petMonth = 'pet' . $i . 'Month';
                    $petYear = 'pet' . $i . 'Year';
                    if($this->{$petYear} && $this->{$petMonth})
                        $this->{$attributeDate} = $this->{$petYear} . '-' . $this->{$petMonth} . '-' . '01 00:00:00';
                }
            }
        }

        return parent::beforeValidate();
    }

    protected function afterValidate()
    {

        parent::afterValidate();

        if ($this->scenario == self::SCENARIO_STEP_TWO)
        {
            if($this->havePet){
                $count = $this->catCount;
                for($i=1; $i<=$count; $i++){
                    $validator=CValidator::createValidator('required',$this,'pet' . $i . '_name');
                    $validator->validate($this,array('pet' . $i . '_name'));
                    $validator2=CValidator::createValidator('required',$this,'pet' . $i . '_birth');
                    $validator2->validate($this,array('pet' . $i . '_birth'));
                }
            }
        }

    }

    protected function beforeSave()
    {
        if ($this->scenario == self::SCENARIO_STEP_THREE){
            if($this->dryFoodBrands){
                $dryFoodBrands =  implode(',', $this->dryFoodBrands);
                $this->dryFoodBrands = $dryFoodBrands;
            }
        }

        return parent::beforeSave();
    }

	public function relations()
	{
		return array(
            //'user' => array(self::HAS_ONE, 'User', 'user_id'),
			//'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
            //'activities' => array(self::MANY_MANY, 'Activity', 'subscribers_activity(subscriber_id, activity_id)'),
		);
	}

	public function behaviors()
	{
		return array(
            'CAdvancedArBehavior' => array(
                'class' => 'application.extensions.CAdvancedArBehavior'
            ),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => Misc::t('ID'),
			'email' => Misc::t('Email'),
			'unsubscribe_key' => Misc::t('Unsubscribe Key'),
			'type_subscribe' => Misc::t('Периодичность прихода уведомлений'),
			'uid' => Misc::t('Uid'),
            'catCount' => Misc::t('Количество животных'),
            'petTypeKitten' => Misc::t('Котенок'),
            'petTypeCat' => Misc::t('Кошка'),
            'petTypePup' => Misc::t('Щенок'),
            'petTypeDog' => Misc::t('Собака'),
            'petBuyPlan' => Misc::t('Как скоро появится'),
			'havePet' => Misc::t('Есть кот'),
            'pet1_name' => Misc::t('[1-е] Имя'),
            'pet1_birth' => Misc::t('[1-е] Дата рождения'),
            'pet2_name' => Misc::t('[2-е] Имя'),
            'pet2_birth' => Misc::t('[2-е] Дата рождения'),
            'pet3_name' => Misc::t('[3-е] Имя'),
            'pet3_birth' => Misc::t('[3-е] Дата рождения'),
            'pet4_name' => Misc::t('[4-е] Имя'),
            'pet4_birth' => Misc::t('[4-е] Дата рождения'),

            'dryFoodFreq' => Misc::t('Как часто вы кормите своего питомца сухими кормами Eukanuba?'),
            'wetFoodFreq' => Misc::t('Как часто вы кормите своего питомца влажными кормами Eukanuba?'),
            'dryFoodBrands' => Misc::t('Какие еще сухие корма вы даете своему питомцу?'),
            'petFoodBuyWhen' => Misc::t('Где вы обычно покупаете корм для вашего питомца?'),
		);
	}

    public static function getPetByPlanList()
    {
        return array(
            '1' => Misc::t('Через несколько дней'),
            '2' => Misc::t('Через несколько недель'),
            '3' => Misc::t('Через месяц'),
            '4' => Misc::t('Через 3 месяца'),
        );
    }

    public function getPetByPlan(){
        $arr = self::getPetByPlanList();
        return $arr[$this->petBuyPlan];
    }

    public static function getDryFoodFreqList(){
        return array(
            '1' => Misc::t('Постоянно'),
            '2' => Misc::t('Большую часть времени'),
            '3' => Misc::t('Периодически'),
            '4' => Misc::t('Никогда'),
        );
    }

    public function getDryFoodFreq(){
        $arr = self::getDryFoodFreqList();
        return $arr[$this->dryFoodFreq];
    }

    public static function getWetFoodFreqList(){
        return self::getDryFoodFreqList();
    }

    public function getWetFoodFreq(){
        $arr = self::getDryFoodFreqList();
        return $arr[$this->wetFoodFreq];
    }

    public static function getPetFoodBuyWhenList(){
        /*return array(
            '1' => Misc::t('Специализированный зоомагазин'),
            '2' => Misc::t('Небольшой продуктовый магазин'),
            '3' => Misc::t('Супермаркет'),
            //'4' => Misc::t('Дисконт-центр'),
            '5' => Misc::t('Ветеринарная клиника'),
            '6' => Misc::t('Интернет-магазин'),

        );*/
        return DicFoodbuy::listAll();
    }

    public function getPetFoodBuyWhen(){
        $arr = self::getPetFoodBuyWhenList();
        return $arr[$this->petFoodBuyWhen];
    }

    public static function getDryFoodBrandsList(){
        /*return array(
            '1' => Misc::t('Purina One'),
            '2' => Misc::t('Royal Canin'),
            '3' => Misc::t('Hills'),
            '4' => Misc::t('ProPlan'),
            '5' => Misc::t('Acana'),
            '6' => Misc::t('Orijen'),
            '7' => Misc::t('Whiskas'),
            '8' => Misc::t('Felix'),
            '9' => Misc::t('Kitekat'),
            '10' => Misc::t('Другое')
        );*/
        return DicFoodbrands::listAll();
    }

    public function getDryFoodBrands(){
        $arr = self::getDryFoodBrandsList();

        $vals = explode(',', $this->dryFoodBrands);

        $ret = array();
        if($vals){
            if(is_array($vals)){
                foreach($vals as $val){
                   if($val == 10) {
				   		$ret[] = $arr[$val] . ': ' . $this->dryFoodBrandsOther;
				   }
				   else {
					   $ret[] = $arr[$val];
				   }
                }
            } else
                $ret[] = $arr[$vals];
        }


        return implode(', ', $ret);
    }






    /*
     public function getActivitiesCriteria($activity_ids)
     {
         $criteria = new CDbCriteria;
         if (!empty($activity_ids)) {
             $activity_ids = implode(',', $activity_ids);
             $criteria->with = array(
                 'activities' => array('condition' => "activities.id IN ($activity_ids)"),
             );
             $criteria->together = true;
         }
         return $criteria;
     }

     public function hasActivities($activity_ids)
     {
         $this->getDbCriteria()->mergeWith($this->getActivitiesCriteria($activity_ids));
         return $this;
     }
     */

	public function search($activity_id = null)
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('unsubscribe_key',$this->unsubscribe_key,true);
		$criteria->compare('uid',$this->uid);
        if (!is_null($activity_id))
            $criteria->mergeWith($this->getActivitiesCriteria($activity_id));

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}
