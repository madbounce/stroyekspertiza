<?php

/**
 * This is the model class for table "tender".
 *
 * The followings are the available columns in table 'tender':
 * @property string $id
 * @property string $user_id
 * @property string $profile_id
 * @property string $title
 * @property integer $public
 * @property integer $canceled
 * @property string $description
 * @property string $finish_time
 * @property string $end_time
 * @property string $cost
 * @property string $currency
 * @property string $region_id
 * @property string $winner_bid_id
 * @property string $create_time
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property Bid[] $bids
 * @property Profile $profile
 * @property User $user
 * @property Region $region
 * @property Bid $winnerBid
 * @property Activity[] $activities
 */
class Tender extends CActiveRecord
{
    const USER_TYPE_USER = 0;
    const USER_TYPE_COMPANY = 1;

    public $status;
    public $createdByUser;
    public $createdByCompany;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{tender}}';
    }

    public function rules()
    {
        return array(
            array('user_id, title, description, finish_time, end_time, cost, currency', 'required'),
            array('public, canceled', 'numerical', 'integerOnly' => true),
            array('user_id, profile_id, region_id, winner_bid_id', 'length', 'max' => 10),
            array('title', 'length', 'max' => 255),
            array('cost, currency', 'length', 'max' => 16),

            array('user_id, profile_id', 'unsafe'),
            array('currency', 'in', 'range' => array_keys(Helper::getCurrencies())),
            array('activities', 'safe'),
            array('finish_time, end_time', 'date', 'format' => Yii::app()->locale->dateFormat),
            array('region_id, user_id, profile_id, winner_bid_id', 'default', 'value' => null),

            array('id, user_id, profile_id, title, public, canceled, description, finish_time, end_time, cost, currency, region_id, winner_bid_id, create_time, update_time', 'safe', 'on' => 'adminSearch'),

            array('status', 'safe', 'on' => 'profileStatusSearch'),
        );
    }

    public function relations()
    {
        return array(
            'profile' => array(self::BELONGS_TO, 'Profile', 'profile_id'),
            'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'activities' => array(self::MANY_MANY, 'Activity', 'tender_activity(tender_id, activity_id)'),

            'bids' => array(self::HAS_MANY, 'Bid', 'tender_id'),
            'actualBids' => array(self::HAS_MANY, 'Bid', 'tender_id', 'scopes' => array('isActual')),
            'winnerBid' => array(self::BELONGS_TO, 'Bid', 'winner_bid_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'user_id' => 'User',
            'profile_id' => 'Profile',
            'title' => Yii::t('tender', 'Название тендера'),
            'public' => Yii::t('tender', 'Публичный'),
            'canceled' => Yii::t('tender', 'Отменен'),
            'description' => Yii::t('tender', 'Описание тендера'),
            'finish_time' => Yii::t('tender', 'Дата подведения итогов'),
            'end_time' => Yii::t('tender', 'Дата сдачи проекта'),
            'cost' => Yii::t('tender', 'Полная стоимость проекта'),
            'currency' => 'Currency',
            'region_id' => Yii::t('tender', 'Регион'),
            'winner_bid_id' => 'Winner Bid',

            'activities' => Yii::t('tender', 'Сфера деятельности'),
        );
    }

    public function adminSearch()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('profile_id', $this->profile_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('public', $this->public);
        $criteria->compare('canceled', $this->canceled);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('finish_time', $this->finish_time, true);
        $criteria->compare('end_time', $this->end_time, true);
        $criteria->compare('cost', $this->cost, true);
        $criteria->compare('currency', $this->currency, true);
        $criteria->compare('region_id', $this->region_id);
        $criteria->compare('winner_bid_id', $this->winner_bid_id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function profileStatusSearch($createdByCompany = false)
    {
        $criteria = new CDbCriteria;
        if (!$createdByCompany)
            $criteria->mergeWith($this->getUserCriteria(Yii::app()->user->id));
        else
            $criteria->mergeWith($this->getCompanyCriteria(Yii::app()->user->model->profile->id));

        switch ($this->status) {
            case Helper::STATUS_ACTUAL:
                $criteria->mergeWith($this->getActualCriteria());
                break;
            case Helper::STATUS_ENDED:
                $criteria->mergeWith($this->getEndedCriteria());
                break;
            case Helper::STATUS_CANCELED:
                $criteria->mergeWith($this->getCanceledCriteria(true));
                break;
            default:
                $criteria->mergeWith($this->getActualCriteria());
                break;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors()
    {
        return array(
            'datetimeI18NBehavior' => array(
                'class' => 'application.components.behaviors.DateTimeI18NBehavior'
            ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
            ),
            'CAdvancedArBehavior' => array(
                'class' => 'application.extensions.CAdvancedArBehavior'
            ),
            'CommentableBehavior' => array(
                'class' => 'comment.components.CommentableBehavior',
            ),
            'filesaver' => array(
                'class' => 'filesaver.components.FileSaverBehavior',
                'savelocation' => 'tender',
                'entity' => 'Tender'
            ),
        );
    }

    protected function afterFind()
    {
        parent::afterFind();

        if ($this->canceled)
            $this->status = Helper::STATUS_CANCELED;
        else {
            $currentDate = date_create();
            $finishDate = date_create($this->finish_time);

            if ($currentDate < $finishDate && empty($this->winner_bid_id))
                $this->status = Helper::STATUS_ACTUAL;
            else
                $this->status = Helper::STATUS_ENDED;
        }

        $this->createdByUser = $this->user_id !== null && $this->profile_id === null;
        $this->createdByCompany = $this->profile_id !== null;
    }

    /**
     * General
     */

    public function getStatusText()
    {
        if ($this->status == Helper::STATUS_CANCELED)
            return Yii::t('tender', 'отменен');
        elseif ($this->status == Helper::STATUS_ACTUAL)
            return Yii::t('tender', 'идет сбор заявок');
        elseif ($this->status == Helper::STATUS_ENDED)
            return Yii::t('tender', 'закончился');
    }

    public function getPeriodOfExecution()
    {
        $finishDate = date_create($this->finish_time);
        $endDate = date_create($this->end_time);
        $interval = date_diff($endDate, $finishDate);
        return Yii::t('tender', '{n} день|{n} дня|{n} дней|{n} дня', $interval->days);
    }

    public function getFormattedCost()
    {
        return Yii::app()->numberFormatter->formatCurrency($this->cost, $this->currency);
    }

    /**
     * Criteria
     */

    public function getPublicCriteria($public = true)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addCondition($public ? "$alias.public = 1" : "$alias.public = 0");
        return $criteria;
    }

    public function getCanceledCriteria($canceled = false)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addCondition($canceled ? "$alias.canceled = 1" : "$alias.canceled = 0");
        return $criteria;
    }

    public function getActualCriteria()
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addCondition("$alias.finish_time >= DATE(NOW()) AND $alias.winner_bid_id IS NULL");
        $criteria->mergeWith($this->getCanceledCriteria(false));
        return $criteria;
    }

    public function getEndedCriteria()
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addCondition("$alias.finish_time < DATE(NOW()) OR $alias.winner_bid_id IS NOT NULL");
        $criteria->mergeWith($this->getCanceledCriteria(false));
        return $criteria;
    }

    public function getActivitiesCriteria($activity_ids)
    {
        $criteria = new CDbCriteria;
        if (!empty($activity_ids)) {
            $activity_ids = implode(',', $activity_ids);
            $criteria->with = array(
                'activities' => array('condition' => "activities.id IN ($activity_ids)"),
            );
            $criteria->together = true;
        }
        return $criteria;
    }

    public function getExceptCriteria($except_ids)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addNotInCondition("$alias.id", $except_ids);
        return $criteria;
    }

    public function getUserCriteria($user_id)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->compare("$alias.user_id", $user_id);
        $criteria->addCondition("$alias.profile_id IS NULL");
        return $criteria;
    }

    public function getCompanyCriteria($company_id)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->compare("$alias.profile_id", $company_id);
        return $criteria;
    }

    /**
     * Scopes
     */

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);
        return array(
            'order' => "$alias.create_time DESC",
        );
    }

    public function isPublic()
    {
        $this->getDbCriteria()->mergeWith($this->getPublicCriteria(true));
        return $this;
    }

    public function isActual()
    {
        $this->getDbCriteria()->mergeWith($this->getActualCriteria());
        return $this;
    }

    public function isEnded()
    {
        $this->getDbCriteria()->mergeWith($this->getEndedCriteria());
        return $this;
    }

    public function isCanceled($canceled)
    {
        $this->getDbCriteria()->mergeWith($this->getCanceledCriteria($canceled));
        return $this;
    }

    public function hasActivities($activity_ids)
    {
        $this->getDbCriteria()->mergeWith($this->getActivitiesCriteria($activity_ids));
        return $this;
    }

    public function except($except_ids)
    {
        $this->getDbCriteria()->mergeWith($this->getExceptCriteria($except_ids));
        return $this;
    }

    public function byUser($user_id)
    {
        $this->getDbCriteria()->mergeWith($this->getUserCriteria($user_id));
        return $this;
    }

    public function byCompany($company_id)
    {
        $this->getDbCriteria()->mergeWith($this->getCompanyCriteria($company_id));
        return $this;
    }
}