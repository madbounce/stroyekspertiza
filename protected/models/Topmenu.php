<?php

/**
 * This is the model class for table "region".
 *
 * The followings are the available columns in table 'region':
 * @property string $id
 * @property string $parent_id
 * @property string $name
 * @property integer $approved
 *
 * The followings are the available model relations:
 * @property Region $parent
 * @property Region[] $regions
 */
class Topmenu extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{region}}';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
			array('url', 'url'),
            array('approved', 'numerical', 'integerOnly' => true),
            array('parent_id', 'length', 'max' => 10),
            array('name', 'length', 'max' => 255),

            array('id, parent_id, name, approved, url, lang, order, lr', 'safe', 'on' => 'adminSearch'),

            array('parent_id', 'default', 'value' => null),
            array('approved', 'unsafe', 'except' => 'adminSearch'),
        );
    }

    public function relations()
    {
        return array(
            'parent' => array(self::BELONGS_TO, 'Topmenu', 'parent_id'),
            'topmenus' => array(self::HAS_MANY, 'Topmenu', 'parent_id','order'=>'topmenus.order ASC'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
			'url' => Misc::t('Ссылка'),
			'order' => Misc::t('Порядок'),
            'parent_id' => Misc::t('Родительский регион'),
            'name' => Misc::t('Название'),
            'approved' => Misc::t('Подтвержден'),
        );
    }

    public function adminSearch()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('approved', $this->approved);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
    }

    /**
     * General
     */

    public static function getItemOptions($allTitle = null)
    {
        if (empty($allTitle))
            $allTitle = Yii::t('admin', 'Все');

        $models = Region::model()->parent()->with(array(
            'regions.regions' => array('alias' => 'subregions'),
            'regions.regions.regions' => array('alias' => 'subregions2')
        ))->findAll();

        $items = array('' => $allTitle);
        foreach ($models as $model) {
            $items[$model->id] = $model->name;
            foreach ($model->regions as $model2) {
                $items[$model2->id] = '--' . $model2->name;
            }
        }

        return $items;
    }
	
	/**
	 * Выбор по указанному региону
	*/
	
	public function getItemOptionsSelected()
    {

        $models = Region::model()->findAll(array('condition' => "t.parent_id = {$this->id}"));

        $items = array();
        foreach ($models as $model) {
            foreach ($model->regions as $model2) {
                $items[$model2->id] = $model2->name;
            }
        }

        return $items;
    }

    /**
     * Criteria
     */

    public function getParentCriteria($parent = null)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        if (!empty($parent)) {
            $criteria->addCondition("$alias.parent_id = : parent_id");
            $criteria->params = array(':parent_id' => $parent->id);
        } else {
            $criteria->addCondition("$alias.parent_id IS NULL");
        }
        return $criteria;
    }

    public function getApprovedCriteria($approved = true)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->compare("$alias.approved", $approved ? 1 : 0);
        return $criteria;
    }

    /**
     * Scopes
     */

    public function approved($approved = true)
    {
        $this->getDbCriteria()->mergeWith($this->getApprovedCriteria($approved));
        return $this;
    }

    public function parent($parent = null)
    {
        $this->getDbCriteria()->mergeWith($this->getParentCriteria($parent));
        return $this;
    }
}