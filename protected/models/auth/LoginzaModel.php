<?php

class LoginzaModel
{
    public $identity;
    public $provider;
    public $email;
    public $first_name;
    public $last_name;
    public $photo;

    public function login($token)
    {
        $authData = json_decode(file_get_contents('http://loginza.ru/api/authinfo?token=' . $token), true);
        if (empty($authData['error_type'])) {
            $this->identity = $authData['identity'];
            $this->provider = $authData['provider'];
            $this->email = isset($authData['email']) ? $authData['email'] : '';
            if (isset($authData['name']['full_name'])) {
                $name = explode(' ', $authData['name']['full_name']);
                $this->first_name = isset($name[0]) ? $name[0] : '';
                $this->last_name = isset($name[1]) ? $name[1] : '';
            } else {
                $this->first_name = $authData['name']['first_name'];
                $this->last_name = $authData['name']['last_name'];
            }
            if (isset($authData['photo']))
                $this->photo = $authData['photo'];

            $identity = new LoginzaUserIdentity();
            if ($identity->authenticate($this)) {
                $duration = 3600 * 24 * 30;
                Yii::app()->user->login($identity, $duration);
                return true;
            }
        }
        return false;
    }
}
