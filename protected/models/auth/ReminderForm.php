<?php

class ReminderForm extends CFormModel
{
    public $email;

    public function rules()
    {
        return array(
            array('email', 'length', 'min' => 3, 'max' => 50),
            array('email', 'required'),
            array('email', 'exist', 'className' => 'User', 'attributeName' => 'email', 'message' => Misc::t('Пользователя с таким E-mail не существует.')),
        );
    }

    public function attributeLabels()
    {
        return array(
            'email' => Misc::t('E-mail'),
        );
    }
}
