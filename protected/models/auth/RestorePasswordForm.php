<?php

class RestorePasswordForm extends CFormModel
{
    public $new_password;
    public $new_password_repeat;

    public function rules()
    {
        return array(
            array('new_password, new_password_repeat', 'required'),
            array('new_password_repeat', 'compare', 'compareAttribute' => 'new_password', 'message' => Misc::t('Пароли не совпадают.')),
            array('new_password', 'length', 'min' => 5),
        );
    }

    public function attributeLabels()
    {
        return array(
            'new_password' => Misc::t('Новый пароль'),
            'new_password_repeat' => Misc::t('Повторите новый пароль'),
        );
    }

    public function restorePassword($user)
    {
        $user->changePassword($this->new_password);
        $user->verify_code = null;
        return $user->save();
    }
}
