<?php
class DicFoodbuy extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{dic_foodbuy}}';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('title', 'length', 'max' => 255),
            array('sort, is_active', 'numerical', 'integerOnly'=>true),
        );
    }

    public function behaviors(){
        return array(
            'SortBehavior' => array(
                'class' => 'ext.behaviors.SortBehavior',
            ),
        );
    }

    public function relations()
    {
        return array(

        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => Yii::t('admin', 'Название'),
            'sort' => Yii::t('admin', 'Сортировка'),
            'is_active' => Yii::t('admin', 'Активность'),
        );
    }

    public function adminSearch()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('t.title', $this->title, true);

        $criteria->order = 't.sort ASC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params->pageSize
            ),

        ));
    }

    public static function all(){
        $criteria = new CDbCriteria();
        $criteria->order = 't.sort ASC';
        $criteria->addCondition('t.is_active = 1');
        return self::model()->findAll($criteria);
    }

    public static function listAll(){
        return CHtml::listData(self::all(), 'id', 'title');
    }
}