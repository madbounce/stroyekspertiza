<?php

class CompanyInvite extends Invite
{
    static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function init()
    {
        parent::init();
        $this->type = self::TYPE_COMPANY_EMPLOYEE;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);
        return array(
            'condition' => "$alias.type = " . self::TYPE_COMPANY_EMPLOYEE,
        );
    }

    /**
     * General
     */

    public static function create($profile, $data)
    {
        $model = new CompanyInvite;
        $model->profile_id = $profile->id;
        $model->code = self::generateCode();
        $model->data = $data;

        if ($model->save())
            return $model;
        else
            return null;
    }
}
