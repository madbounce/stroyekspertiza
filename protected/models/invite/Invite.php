<?php

/**
 * This is the model class for table "invite".
 *
 * The followings are the available columns in table 'invite':
 * @property string $id
 * @property string $code
 * @property string $profile_id
 * @property integer $type
 * @property string $data
 *
 * The followings are the available model relations:
 * @property Profile $profile
 */
class Invite extends CActiveRecord
{
    const TYPE_COMPANY_EMPLOYEE = 0;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{invite}}';
    }

    protected function instantiate($attributes)
    {
        $class = get_class($this);
        if ($attributes['type'] == self::TYPE_COMPANY_EMPLOYEE)
            $class = 'CompanyInvite';

        $model = new $class(null);
        return $model;
    }

    public function rules()
    {
        return array(
            array('code, profile_id, type', 'required'),
            array('type', 'numerical', 'integerOnly' => true),
            array('code', 'length', 'max' => 128),
            array('profile_id', 'length', 'max' => 10),
            array('data', 'safe'),
        );
    }

    public function relations()
    {
        return array(
            'profile' => array(self::BELONGS_TO, 'Profile', 'profile_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code' => 'Code',
            'profile_id' => 'Profile',
            'type' => 'Type',
        );
    }

    protected function afterFind()
    {
        parent::afterFind();
        $this->data = unserialize($this->data);
    }


    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            $this->data = serialize($this->data);
            return true;
        }

        return false;
    }


    /**
     * General
     */

    public static function getByCode($code)
    {
        return Invite::model()->findByAttributes(array('code' => $code));
    }

    public static function generateCode()
    {
        return md5(uniqid('', true));
    }

    public static function create($profile, $data)
    {
    }
}