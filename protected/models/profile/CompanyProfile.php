<?php

/**
 * This is the model class for table "profile".
 *
 * The followings are the available columns in table 'profile':
 * @property string $id
 * @property integer $type
 * @property string $about
 * @property string $website
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property string $region_id
 *
 * The followings are the available model relations:
 * @property Region $region
 * @property Activity[] $activities
 * @property User[] $employees
 */
class CompanyProfile extends Profile
{
    static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function init()
    {
        parent::init();
        $this->type = self::TYPE_COMPANY;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);
        return array(
            'condition' => "$alias.type = " . self::TYPE_COMPANY,
        );
    }

    public function rules()
    {
        return array(
            array('type, name', 'required'),
            array('type', 'numerical', 'integerOnly' => true),
            array('website, name, phone, email, address', 'length', 'max' => 255),
            array('region_id', 'length', 'max' => 10),
            array('about', 'safe'),

            array('id, type, about, website, name, phone, email, address, region_id', 'safe', 'on' => 'search'),

            array('website', 'url'),
            array('email', 'email'),
            array('activities', 'safe'),
            array('activities', 'default', 'value' => array()),
            array('region_id', 'default', 'value' => null)
        );
    }

    public function relations()
    {
        return CMap::mergeArray(parent::relations(), array(
            'employees' => array(self::HAS_MANY, 'User', 'profile_id'),
            'tenders' => array(self::HAS_MANY, 'Tender', 'profile_id'),
        ));
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'about' => Yii::t('profile', 'Коротко о компании'),
            'website' => Yii::t('profile', 'Сайт'),
            'name' => Yii::t('profile', 'Наименование компании'),
            'phone' => Yii::t('profile', 'Телефон'),
            'email' => Yii::t('profile', 'E-mail'),
            'address' => Yii::t('profile', 'Адрес компании'),
            'region_id' => Yii::t('profile', 'Регион'),
        );
    }

    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
            'AliasBehavior' => array(
                'class' => 'application.components.behaviors.AliasBehavior',
                'attributeCallback' => function($owner) { return $owner->name; }
            ),
        ));
    }
}
