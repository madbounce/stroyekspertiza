<?php

/**
 * This is the model class for table "profile".
 *
 * The followings are the available columns in table 'profile':
 * @property string $id
 * @property integer $type
 * @property string $about
 * @property string $website
 * @property string $region_id
 *
 * The followings are the available model relations:
 * @property Region $region
 * @property Activity[] $activities
 * @property User[] $users
 */
class FreelancerProfile extends Profile
{
    static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function init()
    {
        parent::init();
        $this->type = self::TYPE_FREELANCER;
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);
        return array(
            'condition' => "$alias.type = " . self::TYPE_FREELANCER,
        );
    }

    public function rules()
    {
        return array(
            array('type', 'required'),
            array('type', 'numerical', 'integerOnly' => true),
            array('website, phone, address', 'length', 'max' => 255),
            array('region_id', 'length', 'max' => 10),
            array('about', 'safe'),

            array('id, type, about, website, name, phone, email, address, region_id', 'safe', 'on' => 'search'),

            array('website', 'url'),
            array('activities', 'safe'),
            array('activities', 'default', 'value' => array()),
            array('region_id', 'default', 'value' => null)
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'about' => Yii::t('profile', 'Коротко обо мне'),
            'website' => Yii::t('profile', 'Сайт'),
            'phone' => Yii::t('profile', 'Телефон'),
            'email' => Yii::t('profile', 'E-mail'),
            'address' => Yii::t('profile', 'Адрес'),
            'region_id' => Yii::t('profile', 'Регион'),
        );
    }

    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
            'AliasBehavior' => array(
                'class' => 'application.components.behaviors.AliasBehavior',
                'attributeCallback' => function($owner) { return $owner->user->first_name . $owner->user->last_name; }
            ),
        ));
    }
}
