<?php

/**
 * This is the model class for table "profile".
 *
 * The followings are the available columns in table 'profile':
 * @property string $id
 * @property integer $type
 * @property string $about
 * @property string $website
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property string $region_id
 *
 * The followings are the available model relations:
 * @property Region $region
 * @property Activity[] $activities
 * @property User[] $users
 */
class Profile extends CActiveRecord
{
    const TYPE_FREELANCER = 0;
    const TYPE_COMPANY = 1;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{profile}}';
    }

    protected function instantiate($attributes)
    {
        $class = get_class($this);
        if ($attributes['type'] == self::TYPE_FREELANCER)
            $class = 'FreelancerProfile';
        else
            $class = 'CompanyProfile';

        $model = new $class(null);
        return $model;
    }

    public function relations()
    {
        return array(
            'user' => array(self::HAS_ONE, 'User', 'profile_id'),
            'region' => array(self::BELONGS_TO, 'Region', 'region_id'),
            'activities' => array(self::MANY_MANY, 'Activity', 'profile_activity(profile_id, activity_id)'),
        );
    }

    public function behaviors()
    {
        return array(
            'CAdvancedArBehavior' => array(
                'class' => 'application.extensions.CAdvancedArBehavior'
            ),
            'CommentableBehavior' => array(
                'class' => 'comment.components.CommentableBehavior',
            ),
            'logo' => array(
                'class' => 'image.components.ImageBehavior',
                'tag' => 'logo',
                'multiple' => false,
            ),
            'portfolios' => array(
                'class' => 'portfolio.components.PortfolioImageBehavior',
                'tag' => 'portfolios',
            ),
        );
    }

    /**
     * General
     */

    public function getIsFreelancer()
    {
        return $this->type == self::TYPE_FREELANCER;
    }

    public function getIsCompany()
    {
        return $this->type == self::TYPE_COMPANY;
    }

    public static function getTypeOptions()
    {
        return array(
            self::TYPE_FREELANCER => Yii::t('profile', 'Фрилансер'),
            self::TYPE_COMPANY => Yii::t('profile', 'Компания'),
        );
    }
}