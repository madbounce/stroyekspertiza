<?php

class EmployeeUser extends User
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{user}}';
    }

    public function rules()
    {
        return array(
            array('first_name, last_name, email', 'required'),
            array('email, first_name, last_name, contact_phone, company_post', 'length', 'max' => 255),
            array('email', 'email'),
            array('company_administrator', 'numerical', 'integerOnly' => true),
            array('company_administrator', 'unsafe'),
        );
    }

    public function fire()
    {
        $this->company_post = null;
        $this->profile_id = null;
        $this->company_administrator = 0;
        return $this->save();
    }
}
