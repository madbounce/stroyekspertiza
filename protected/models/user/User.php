<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $verify_code
 * @property integer $status
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $contact_phone
 * @property string $skype
 * @property string $icq
 * @property string $region_id
 * @property string $profile_id
 * @property string $company_post
 * @property integer $company_administrator
 * @property string $identity
 * @property string $provider
 *
 * The followings are the available model relations:
 * @property Comment[] $comments
 * @property Profile $profile
 * @property Region $region
 */
class User extends CActiveRecord
{
    const SCENARIO_REGISTER = 'register';
    const SCENARIO_SOCIAL_REGISTER = 'socialRegister';
    const SCENARIO_UPDATE_INFO = 'updateInfo';
    const SCENARIO_UPDATE_EMAIL = 'updateEmail';
    const SCENARIO_JOIN = 'join';

    const STATUS_NOT_CONFIRMED = 0;
    const STATUS_CONFIRMED = 1;

    public $full_name;
    public $captchaCode;
    public $password_repeat;
    public $offer;
    public $bYear;
    public $bMonth;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{user}}';
    }

    public function rules()
    {
        return array(
			// Register
            array('email, password, first_name, last_name', 'required', 'on' => self::SCENARIO_REGISTER),
            array('password_repeat', 'required', 'on' => self::SCENARIO_REGISTER),
            array('password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => Misc::t('Пароли не совпадают.'), 'on' => self::SCENARIO_REGISTER),
            //array('captchaCode', 'captcha', 'on' => self::SCENARIO_REGISTER),
            array('offer', 'required', 'requiredValue'=>true ,'on' => self::SCENARIO_REGISTER),
            // Social Register
            array('id, email, password, verify_code, status, first_name, middle_name, last_name, contact_phone, skype, icq, region_id, profile_id, identity, provider', 'safe'),
            // Update Info
            array('email, first_name, last_name', 'required', 'on' => self::SCENARIO_UPDATE_INFO),
            // Update Email
            array('email', 'required', 'on' => self::SCENARIO_UPDATE_EMAIL),
            array('verify_code, status, company_administrator', 'unsafe'),
            array('status', 'numerical', 'integerOnly' => true),
            array('email, password, first_name, middle_name, last_name, contact_phone, skype, icq, company_post, identity, provider', 'length', 'max' => 255),
            array('verify_code', 'length', 'max' => 32),
            array('region_id, profile_id', 'length', 'max' => 10),
            array('email', 'email'),
            array('email', 'unique', 'message' => Misc::t('Пользователь с таким E-mail уже зарегистрирован.'), 'except' => self::SCENARIO_SOCIAL_REGISTER),
            array('password', 'length', 'min' => 5),
            array('region_id, profile_id', 'default', 'value' => null),

            array('id, email, full_name', 'safe', 'on' => 'adminSearch'),
        );
    }

    public function relations()
    {
        return array(
            'profile' => array(self::BELONGS_TO, 'Profile', 'profile_id'),
            'comments' => array(self::HAS_MANY, 'Comment', 'user_id'),
            'region' => array(self::BELONGS_TO, 'DicRegions', 'region_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'email' => Misc::t('E-mail'),
            'password' => Misc::t('Пароль'),
            'verify_code' => Misc::t('Код подтверждения'),
            'status' => Misc::t('Подтвержден'),
            'password_repeat' => Misc::t('Повторите пароль'),
            'first_name' => Misc::t('Имя'),
            'middle_name' => Misc::t('Отчество'),
            'last_name' => Misc::t('Фамилия'),
            'contact_phone' => Misc::t('Контактный телефон'),
            'skype' => Misc::t('Скайп'),
            'icq' => 'ICQ',
            'region_id' => Misc::t('Регион'),
            'profile_id' => 'Profile',
            'company_post' => Misc::t('Должность'),
            'address1' => Misc::t('Адрес'),
            'address2' => Misc::t('Адрес 2'),
            'city' => Misc::t('Город'),
            'zip_code' => Misc::t('Почтовый индекс'),
            'd_birth' => Misc::t('Дата рождения'),
             'dateRegistered' => Misc::t('Дата регистрации'),
            'full_name' => Misc::t('Имя'),
            'bYear' => Misc::t('Год рождения'),
            'bMonth' => Misc::t('Месяц рождения')
        );
    }

    public function adminSearch()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('email', $this->email, true);

        if (!empty($this->full_name)) {
            $tokens = explode(' ', $this->full_name);
            $nameCriteria = new CDbCriteria;
            foreach ($tokens as $token) {
                $nameCriteria->compare('first_name', $token, true, 'or');
                $nameCriteria->compare('middle_name', $token, true, 'or');
                $nameCriteria->compare('last_name', $token, true, 'or');
            }
            $criteria->mergeWith($nameCriteria);
        }
        if(isset($_GET['region']) && ($_GET['region'] != '')){
            $param = (int) $_GET['region'];
            $criteria->addCondition('t.region_id =' . $param);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params->pageSize
            )
        ));
    }

    public $emailChanged = false;
    public $oldEmail;

    protected function afterFind()
    {
        parent::afterFind();
        $this->oldEmail = $this->email;

        $this->full_name = implode(' ', array($this->first_name, $this->last_name));
    }

    protected function afterValidate()
    {
        parent::afterValidate();

        if ($this->scenario == self::SCENARIO_REGISTER) {
            $this->status = self::STATUS_NOT_CONFIRMED;
            $this->generateVerifyCode();
        }

        if ($this->scenario == self::SCENARIO_UPDATE_INFO || $this->scenario == self::SCENARIO_UPDATE_EMAIL) {
            $this->emailChanged = $this->oldEmail != $this->email;
            if ($this->emailChanged) {
                $this->status = self::STATUS_NOT_CONFIRMED;
                $this->generateVerifyCode();
            }
        }
    }

    protected function beforeValidate()
    {

        if ($this->scenario == self::SCENARIO_JOIN) {
            if($this->bMonth && $this->bYear)
                $this->d_birth = $this->bYear . '-' . $this->bMonth . '-' . '01 00:00:00';
        }

        return parent::beforeValidate();
    }

    protected function beforeSave()
    {
        if ($this->scenario == self::SCENARIO_REGISTER)
            $this->password = self::encrypt($this->password);

        return parent::beforeSave();
    }

    public function behaviors()
    {
        return array(
            'avatar' => array(
                'class' => 'image.components.ImageBehavior',
                'tag' => 'avatar',
                'multiple' => false,
            ),
        );
    }

    /*
     * General
     */

    public static function encrypt($value)
    {
        return md5($value);
    }

    public static function verify($value, $hash)
    {
        return $hash == md5($value);
    }

    public function generateVerifyCode()
    {
        $this->verify_code = self::encrypt(microtime() . $this->email);
    }

    public function generatePassword($length = 8)
    {
        // start with a blank password
        $password = "";

        // define possible characters - any character in this string can be
        // picked for use in the password, so if you want to put vowels back in
        // or add special characters such as exclamation marks, this is where
        // you should do it
        $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";

        // we refer to the length of $possible a few times, so let's grab it now
        $maxlength = strlen($possible);

        // check for length overflow and truncate if necessary
        if ($length > $maxlength) {
            $length = $maxlength;
        }

        // set up a counter for how many characters are in the password so far
        $i = 0;

        // add random characters to $password until $length is reached
        while ($i < $length) {

            // pick a random character from the possible ones
            $char = substr($possible, mt_rand(0, $maxlength-1), 1);

            // have we already used this character in $password?
            if (!strstr($password, $char)) {
                // no, so it's OK to add it onto the end of whatever we've already got...
                $password .= $char;
                // ... and increase the counter by one
                $i++;
            }

        }

        $this->password = self::encrypt($password);

        return $password;
    }

    public function changePassword($password)
    {
        $this->password = self::encrypt($password);
    }

    public static function confirm($code)
    {
        $model = User::model()->findByAttributes(array('verify_code' => $code, 'status' => self::STATUS_NOT_CONFIRMED));
        if (!empty($model)) {
            $model->status = self::STATUS_CONFIRMED;
            $model->verify_code = null;
            if ($model->save())
                return true;
        }

        return false;
    }

    public static function getByEmail($email)
    {
        return User::model()->find('t.email = :email', array(':email' => $email));
    }


    static public function setFilterChartCriteria($criteria,$filter="",$bool = false){
        if ($filter){
            $p = preg_split('/&/', $filter);
            $f = array();
            foreach ($p as $key)
                $f[preg_replace("/=.+/", "", $key)] = preg_replace("/.+=/", "", $key);
        }
        
        //Регион
        if((isset($_GET['region']) && ($_GET['region'] != '')) 
                || (isset($f['region']) && $f['region'] != '')){
//            $param = (int) $_GET['region'];
            $param = (isset($f['region'])) ? (int) $f['region'] : (int) $_GET['region'];
            $criteria->addCondition('t.region_id =' . $param);
        }

        //Город
        if((isset($_GET['city']) && ($_GET['city'] != ''))
                || (isset($f['city']) && $f['city'] != '')){
//            $param = $_GET['city'];
            $param = (isset($f['city'])) ? $f['city'] : $_GET['city'];
            $criteria->addSearchCondition('t.city', $param);
        }

        //Возраст кошки
        if((isset($_GET['age']) && ($_GET['age'] != '')) 
                || (isset($f['age']) && $f['age'] != '')){
//            $param =  (int) $_GET['age'];
            $param = (isset($f['age'])) ? (int) $f['age'] : (int) $_GET['age'];
            switch($param){
                //до года
                case 1:
                    $cond = '(DATE_SUB(CURDATE(),INTERVAL 1 YEAR) <= subscribe.pet1_birth) OR ' .
                        '(DATE_SUB(CURDATE(),INTERVAL 1 YEAR) <= subscribe.pet2_birth) OR ' .
                        '(DATE_SUB(CURDATE(),INTERVAL 1 YEAR) <= subscribe.pet3_birth) OR ' .
                        '(DATE_SUB(CURDATE(),INTERVAL 1 YEAR) <= subscribe.pet4_birth)';
                    $criteria->addCondition($cond);
                    break;
                //от года до 7
                case 2:
                    $cond = '(DATE_SUB(CURDATE(),INTERVAL 7 YEAR) <= subscribe.pet1_birth) OR ' .
                        '(DATE_SUB(CURDATE(),INTERVAL 7 YEAR) <= subscribe.pet2_birth) OR ' .
                        '(DATE_SUB(CURDATE(),INTERVAL 7 YEAR) <= subscribe.pet3_birth) OR ' .
                        '(DATE_SUB(CURDATE(),INTERVAL 7 YEAR) <= subscribe.pet4_birth)';
                    $criteria->addCondition($cond);
                    break;
                //больше 7
                case 3:
                    $cond = '(DATE_SUB(CURDATE(),INTERVAL 7 YEAR) >= subscribe.pet1_birth) OR ' .
                        '(DATE_SUB(CURDATE(),INTERVAL 7 YEAR) >= subscribe.pet2_birth) OR ' .
                        '(DATE_SUB(CURDATE(),INTERVAL 7 YEAR) >= subscribe.pet3_birth) OR ' .
                        '(DATE_SUB(CURDATE(),INTERVAL 7 YEAR) >= subscribe.pet4_birth)';
                    $criteria->addCondition($cond);
                    break;

            }
           // $criteria->addCondition('t.city', $param);
        }

        //Дата от
        if((isset($_GET['dateFrom']) && ($_GET['dateFrom'] != '')) 
                || (isset($f['dateFrom']) && $f['dateFrom'] != '')){
//            $param = date('Y-m-d H:i:s', strtotime($_GET['dateFrom']));
            $param = (isset($f['dateFrom'])) ? date('Y-m-d H:i:s', strtotime($f['dateFrom'])) : date('Y-m-d H:i:s', strtotime($_GET['dateFrom']));
            $criteria->addCondition("t.dateRegistered >= '$param'");
        }

        //Дата до
        if((isset($_GET['dateTo']) && ($_GET['dateTo'] != '')) 
                || (isset($f['dateTo']) && $f['dateTo'] != '')){
//            $param = date('Y-m-d H:i:s', strtotime($_GET['dateTo']));
            $param = (isset($f['dateTo'])) ? date('Y-m-d H:i:s', strtotime($f['dateTo'])) : date('Y-m-d H:i:s', strtotime($_GET['dateTo']));
            $criteria->addCondition("t.dateRegistered <= '$param'");
        }
    }
}