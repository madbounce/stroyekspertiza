<?php

class ArticlesModule extends CWebModule
{
    public function init()
    {
        $this->setImport(array(
            'articles.models.*',
            'articles.components.*',
        ));
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        } else
            return false;
    }

    public static function link($name, $htmlOptions = array())
    {
        $model = Articles::model()->findByAttributes(array('name' => $name));
        return CHtml::link(CHtml::encode($model->title), array('/articles/default/view', 'name' => $model->name), $htmlOptions);
    }
}
