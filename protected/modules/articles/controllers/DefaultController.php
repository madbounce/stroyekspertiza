<?php

class DefaultController extends FrontEndController
{
    public $layout = '//layouts/main';
	
	public function actionView($url)
    {
        $model = $this->loadModel($url);

        if(!$model)
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $this->pageTitle = $model->meta_title;
        Yii::app()->clientScript->registerMetaTag($model->meta_description, 'description');
        Yii::app()->clientScript->registerMetaTag($model->meta_keywords, 'keywords');

        $view = 'view';

        if($this->getViewFile($url)){
            $view = $url;
        }

        $this->render($view, array('model' => $model));
    }

    public function actionIndex($type)
    {
        if(!$this->getViewFile($type)){
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        }
        $this->render($type, array(
            'type' => $type
        ));
    }

    public function actionPuppy()
    {
        $category = $this->loadModelCategory('puppy');
        $this->render('articles',array('category'=>$category));
    }

    public function actionSearch($type, $subtype = '')
    {
        /**@var $category ArticlesCategory
         * @var $model CActiveDataProvider
         */
        if($subtype)
            $category = $this->loadModelCategory($subtype);
        else
            $category = $this->loadModelCategory($type);

        if(!$category) throw new CHttpException('404');

        if(isset($_GET['filter0']) && is_array($_GET['filter0']))
        {
            Yii::app()->request->cookies['filter0'] = new CHttpCookie('filter0', serialize($_GET['filter0']));
        }else{
            if(YiiBase::app()->request->isAjaxRequest && !isset($_GET['filter0'])) {
                unset(Yii::app()->request->cookies['filter0']);
            }else{
                $value = isset(Yii::app()->request->cookies['filter0']) ? Yii::app()->request->cookies['filter0']->value : '';
                $_GET['filter0'] = unserialize($value);
            }
        }

        $model = Articles::model();//->with('category')->CategoryId($category->id);

        $this->render('search', array(
            'model'=> $model,
            'category' => $category,
            'type' => $type,
            'subtype' => $subtype
        ));

    }

    public function loadModel($name)
    {
        $criteria = new CDbCriteria;
        if (is_numeric($name))
            $criteria->addCondition('id ='.$name);
        else
            $criteria->addCondition('slug ="'. $name.'"');

        $model = Articles::model()->find($criteria);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        return $model;
    }

    public function loadModelCategory($name)
    {
        $criteria = new CDbCriteria;
        if (is_numeric($name))
            $criteria->addCondition('id ='.$name);
        else
            $criteria->addCondition('url ="'. $name.'"');

        $model = ArticlesCategory::model()->find($criteria);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        return $model;
    }
}