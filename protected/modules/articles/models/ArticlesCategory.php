<?php

/**
 * This is the model class for table "{{company_category}}".
 *
 * The followings are the available columns in table '{{notice_category}}':
 * @property integer $id
 * @property string $title
 *
 * The followings are the available model relations:
 * @property Notice[] $notices
 */
class ArticlesCategory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return NoticeCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{articles_category}}';
	}

	public function getModelName()
	{
	    return __CLASS__;
	}

    public function behaviors()
    {
        return array(
            'SortBehavior' => array(
                'class' => 'ext.behaviors.SortBehavior',
                'sortAttribute' => 'sort'
            ),
        );
    }
	
	public function relations()
	{
		return array( 
			'articles' => array(self::HAS_MANY, 'Articles', 'categoryId'),
            'parents' => array(self::HAS_MANY, 'ArticlesCategory', 'parent_id'),
            'parentsId' => array(self::HAS_ONE, 'ArticlesCategory', array('id'=>'parent_id')),
		);
	}


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, parent_id', 'required'),
			array('title', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('sort, url', 'safe'),
			array('id, title, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Категория',
			'title' => 'Название',
			'url' => 'Eng название'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);

        $criteria->order = 't.sort ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
                'pageSize' => 10000
            ),
		));
	}
    public function pId($id = 0)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=>'t.parent_id="'.$id.'"',
        ));
        return $this;
    }

    public function path($path = '')
    {
        if(!$path) return $this;
        $pid = $this->findByAttributes(array('url'=>$path));
        if(!$pid) return $this;


        return $this->pId($pid->id);
    }

    public function toArray($key='id',$value = 'title'){

        $criteria = new CDbCriteria();
        $criteria->order = 't.sort ASC';
        $this->getDbCriteria()->mergeWith($criteria);
        $model = $this->findAll($criteria);

        $items = array();
        foreach($model as $row)
            $items[$row->{$key}] = $row->{$value};

        return $items;
    }

    public static function all(){
        $criteria = new CDbCriteria();
        $criteria->order = 't.sort ASC';
        return self::model()->findAll($criteria);
    }

    public static function listCats($key='id',$value='title')
    {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('url',array('kittens','adultcat'));
        $criteria->order = 't.sort ASC';
        $res = self::model()->findAll($criteria);
        return CHtml::listData($res, $key, $value);
    }

    public static function listDogs($key='id',$value='title')
    {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('url',array('puppy','adultdog'));
        $criteria->order = 't.sort ASC';
        $res = self::model()->findAll($criteria);
        return CHtml::listData($res, $key, $value);
    }

    public static function listBreeders($key='id',$value='title')
    {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('url',array('breeders'));
        $criteria->order = 't.sort ASC';
        $res = self::model()->findAll($criteria);
        return CHtml::listData($res, $key, $value);
    }

    public static function listAll($id='',$pid='')
    {
        $criteria = new CDbCriteria();
        if(!empty($id)) $criteria->addCondition('id <>'.$id);
        if(!empty($pid)) $criteria->addCondition('parent_id <>'.$pid);
        $criteria->order = 't.sort ASC';
        $res = self::model()->findAll($criteria);
        return CHtml::listData($res, 'id', 'title');
    }
}