<?php
    if (($model->isNewRecord) && isset($_GET['cId']))
        $model->categoryId = (int)$_GET['cId'];

    $type = '';
    if(!$model->isNewRecord && isset($model->category)){
        if(!$model->category->parent_id)
            $type = $model->category->url;
        else
            $type = $model->category->parentsId->url;

    } else {
        if($model->categoryId)
        {
            $cat = ArticlesCategory::model()->find($model->categoryId);
            if(!$cat->parent_id)
                $type = $cat->url;
            else
                $type = $cat->parentsId->url;
        }
    }
    $emptyField = array('' => 'не выбрано');
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'article-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'product'
    ),
)); ?>

<script>
    var hide = '<?php echo $type ?>';
    $(document).ready(function(){
        if(!hide)
            $('li.product-params').hide();

        $('#Articles_categoryId').change(function(){
            var cid = $(this).val();
            var cat_type = $('#type');
            $('li.product-params').css('opacity','0.1');
            $.ajax({
                url  : '/backend/articles/admin/AjaxCatList',
                data : {'id':cid},
                type : 'GET'
            }).success(function ( data ) {
                eval('var data='+data);
                cat_type.html(data.cat_type);
                $('li.product-params').show();
                $('li.product-params').css('opacity','1');
            });
        });
    });
</script>

<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textField($model, 'title', array('class' => 'name', 'maxlength' => 255)); ?>
    <div class="checkbox"></div>
</div>

<!-- Левая колонка свойств товара -->
<div id="column_left">

    <!-- Параметры страницы -->
    <div class="block layer param">
        <h2>Параметры страницы</h2>


        <ul>
            <li>
                <? echo $form->labelEx($model, 'categoryId', array('class' => 'property'))?>
                <?php echo $form->dropDownList($model, 'categoryId', CHtml::listData(ArticlesCategory::model()->findAll(), 'id', 'title')); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'url', array('class' => 'property')); ?>
                <div class="page_url">/articles/</div>
                <?php echo $form->textField($model, 'slug', array('class' => 'page_url', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'meta_title', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'meta_title', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'meta_keywords', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'meta_keywords', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'meta_description', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'meta_description', array('class' => 'simpla_inp')); ?>
            </li>
        </ul>
    </div>
</div><!-- Левая колонка свойств товара (The End)-->

<!-- Правая колонка свойств товара -->
<div id="column_right">

    <!-- Изображение категории -->
    <div class="block layer images">
        <h2>Изображения</h2>

        <?php
        $this->widget('application.modules.image.components.widgets.UploadWidget', array(
            'model' => $model,
            'tag' => 'image',
            'action' => array('/articles/admin/uploadImage'),
            'multiple' => true,
            'view' => 'imagesCatalog',
            'uploadButtonText' => Yii::t('articles', 'Загрузить изображение'),
        ));
        ?>
    </div>
</div>
<!-- Правая колонка свойств товара (The End)-->

<div style="clear: both"></div>

<div class="editor-line">
    <?php echo $form->labelEx($model, 'content'); ?>
    <?php //echo $form->textArea($model,'content',array('class'=>'medium')); ?>
    <?php $this->widget('application.extensions.redactorjs.ERedactorWidget', array(
    'model' => $model,
    'attribute' => 'content',
    'options' => array(
        'lang' => 'ru',
        'imageUpload' => CHtml::normalizeUrl(array('/articles/admin/redactorUploadImage'))
    ),
)); ?>
    <?php echo $form->error($model, 'content'); ?>
</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>