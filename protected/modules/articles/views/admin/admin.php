<?php
$dp = $model->adminSearch();
$countAll = $dp->totalItemCount;
?>

<div class="page-content">
    <div class="row two-columns">
        <div class="span9">

            <div class="page-header admin-header inner-header">
                <h3><?php echo Yii::t('app', '{n} новость|{n} новости|{n} новостей', $countAll);?></h3>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => Yii::t('articles', 'Добавить новость'),
                    'size' => 'small',
                    'url' => (!isset($_GET['cId'])) ? array('/articles/admin/create') : array('/articles/admin/create', 'cId' => $_GET['cId'])
                ));
                ?>
            </div>

            <div class="page-content">
                <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
                    'id' => 'articles-grid',
                    'type'=>'striped bordered',
                    'dataProvider' => $dp,
                    //'filter' => $model,
                    //'sortableRows' => true,
                    //'sortableAjaxSave'=>true,
                    //'sortableAttribute'=>'sort',
                    //'sortableAction'=> 'config/admin/sortable',
                    'template' => '{pager}{items}{pager}',
                    'columns' => array(
                        array(
                            'name' => 'title',
                            'type'=>'raw',
                            'value' => 'CHtml::link($data->title, array("/articles/admin/update", "id" => $data->id))',
                            'htmlOptions' => array('encodeLabel'=>false),
                        ),
                        array(
                            'name'=>'categoryId',
                            'type'=>'raw',
                            'value'=>'CHtml::link(@$data->category->title, array("/articles/admin/admin", "cId" => $data->categoryId))',
                        ),
                        array(
                            'name'=>'type',
                            'type'=>'raw',
                            'value'=>'Articles::getTypeList(@$data->category->parentsId->url,$data->type)',
                        ),
                        array(
                            'class' => 'bootstrap.widgets.TbButtonColumn',
                            'template' => '{update}{delete}'
                        ),
                    ),
                )); ?>
            </div>
        </div>
        <div class="span3 sidebar">
            <div class="b-side">
                <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route), 'get', array('class' => 'search-form')); ?>

                <div class="input-append">
                    <?php echo CHtml::textField('search', isset($_GET['search']) ? $_GET['search'] : '', array('class' => 'input-medium search-input'))?>
                    <button class="btn" type="submit"><i class="icon-search"></i></button>
                </div>
                <?php echo CHtml::endForm(); ?>
            </div>

            <div class="b-side">
                <?php
                $categories = array(
                    array('label'=>Yii::t('admin','Все категории'), 'url'=>array('/articles/admin/admin'), 'active' => (!isset($_GET['cId']))),
                );
                $cats = ArticlesCategory::all();
                if($cats){
                    foreach($cats as $cat){
                        $rowCssClass = ($cat->parent_id) ? "child-r" : "parent-r";
                        array_push($categories, array(
                                'label' => $cat->title,
                                'url' => array('/articles/admin/admin', 'cId' => $cat->id),
                                'active' => (isset($_GET['cId']) && ($_GET['cId'] == $cat->id)),
                                'itemOptions' => array('class'=>$rowCssClass),
                            )
                        );
                    }
                }

                $this->widget('bootstrap.widgets.TbMenu', array(
                    'type'=>'list',
                    'items' => $categories
                ));
                ?>
            </div>

        </div>
    </div>
</div>
