<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Новости') => array('/articles/admin/admin'),
    Yii::t('admin', 'Редактирование новости')
);
?>

<div class="page-header">
    <h3><?php echo Yii::t('news', 'Редактирование новости'); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>