<div class="header_image articles">
    <div class="content_header">
        <div class="text_header">
            <h3>СТАТЬИ О ЩЕНКАХ</h3>
            <p>УХОД, ВОСПИТАНИЕ, ЗДОРОВЬЕ, ПИТАНИЕ...</p>
        </div>

        <div class="header_right_menu">
            <ul>
                <li><a href="/articles/default/puppy" class="active">СТАТЬИ</a></li>
                <li><a target="_blank" href="http://www.youtube.com/channel/UCp6hEh5cS8p9r1SgylxbFyQ">ВИДЕО О ВОСПИТАНИИ</a></li>
                <li><a href="/products/dog/puppy">ПОДБОР КОРМА</a></li>
                <li><a href="/page/nutrition">ПИТАНИЕ ЩЕНКОВ</a></li>
                <li><a href="#">ЛИСТ ПОКУПОК</a></li>
                <li><a href="/page/dog_breed_selector">НАЙДИТЕ СВОЮ ИДЕАЛЬНУЮ ПОРОДУ</a></li>
                <li><a href="/page/breedersrecommended">РЕКОМЕНДАЦИИ ЗАВОДЧИКОВ</a></li>
                <li><a href="#">КУПОН</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="content_iner">
    <h2 class="main_title second">УСПЕХ, ЗДОРОВЬЕ И ХАРАКТЕР СОБАКИ НАЧИНАЮТСЯ С ХОЗЯИНА <br/>
        В данном руководстве вы найдете советы и рекомендации по уходу за щенками и собаками.
        Ваша собака отблагодарит вас за заботу преданностью и крепким здоровьем.
    </h2>

<!--    1 => Yii::t('articles', 'Питание'),-->
<!--    2 => Yii::t('articles', 'Забота о здоровье'),-->
<!--    3 => Yii::t('articles', 'Дрессировка'),-->
<!--    4 => Yii::t('articles', 'Поведение'),-->
<!--    5 => Yii::t('articles', 'Истории о собаках'),-->
<!--    6 => Yii::t('articles', 'Нестандартные ситуации'),-->

    <?php
        $criteria = new CDbCriteria();
        $criteria->compare('t.categoryId',$category->id);
        $criteria->compare('t.type',1);

        $articles = Articles::model()->findAll($criteria);
        if(count($articles) > 0)
        {
            echo '<h3 class="lines">ПИТАНИЕ</h3>';

            echo '<ul class="list_articles">';
                foreach($articles as $article)
                {
                    echo '<li>';
//                        echo $article->title;
                        echo CHtml::link($article->title.' >>','/articles/view/'.$article->slug);
                    echo '</li>';
                }
            echo '</ul>';
        }
    ?>
    <?php
        $criteria = new CDbCriteria();
        $criteria->compare('t.categoryId',$category->id);
        $criteria->compare('t.type',2);

        $articles = Articles::model()->findAll($criteria);
        if(count($articles) > 0)
        {
            echo '<h3 class="lines">ЗДОРОВЬЕ</h3>';

            echo '<ul class="list_articles">';
                foreach($articles as $article)
                {
                    echo '<li>';
//                        echo $article->title;
                        echo CHtml::link($article->title.' >>','/articles/view/'.$article->slug);
                    echo '</li>';
                }
            echo '</ul>';
        }
    ?>
    <?php
        $criteria = new CDbCriteria();
        $criteria->compare('t.categoryId',$category->id);
        $criteria->compare('t.type',3);

        $articles = Articles::model()->findAll($criteria);
        if(count($articles) > 0)
        {
            echo '<h3 class="lines">ОБУЧЕНИЕ</h3>';

            echo '<ul class="list_articles">';
                foreach($articles as $article)
                {
                    echo '<li>';
//                        echo $article->title;
                        echo CHtml::link($article->title.' >>','/articles/view/'.$article->slug);
                    echo '</li>';
                }
            echo '</ul>';
        }
    ?>
    <?php
        $criteria = new CDbCriteria();
        $criteria->compare('t.categoryId',$category->id);
        $criteria->compare('t.type',4);

        $articles = Articles::model()->findAll($criteria);
        if(count($articles) > 0)
        {
            echo '<h3 class="lines">ПОВЕДЕНИЕ</h3>';

            echo '<ul class="list_articles">';
                foreach($articles as $article)
                {
                    echo '<li>';
//                        echo $article->title;
                        echo CHtml::link($article->title.' >>','/articles/view/'.$article->slug);
                    echo '</li>';
                }
            echo '</ul>';
        }
    ?>
    <?php
        $criteria = new CDbCriteria();
        $criteria->compare('t.categoryId',$category->id);
        $criteria->compare('t.type',5);

        $articles = Articles::model()->findAll($criteria);
        if(count($articles) > 0)
        {
            echo '<h3 class="lines">ИСТОРИЯ</h3>';

            echo '<ul class="list_articles">';
                foreach($articles as $article)
                {
                    echo '<li>';
//                        echo $article->title;
                        echo CHtml::link($article->title.' >>','/articles/view/'.$article->slug);
                    echo '</li>';
                }
            echo '</ul>';
        }
    ?>
    <?php
        $criteria = new CDbCriteria();
        $criteria->compare('t.categoryId',$category->id);
        $criteria->compare('t.type',6);

        $articles = Articles::model()->findAll($criteria);
        if(count($articles) > 0)
        {
            echo '<h3 class="lines">НЕСТАНДАРТНЫЕ СИТУАЦИИ</h3>';

            echo '<ul class="list_articles">';
                foreach($articles as $article)
                {
                    echo '<li>';
//                        echo $article->title;
                        echo CHtml::link($article->title.' >>','/articles/view/'.$article->slug);
                    echo '</li>';
                }
            echo '</ul>';
        }
    ?>
</div>

<style type="text/css">
    .list_articles li a:hover{
        color: black;
    }
</style>