<?php
$this->breadcrumbs=array(
    Misc::t('Заводчики')
);
?>

<div class="bigDog dog3">
    <h1>РЕКОМЕНДАЦИИ И СОВЕТЫ <br /><small>ПО УХОДУ ЗА СОБАКАМИ</small></h1>
    <div class="article"><a href="/articles/cats">Статьи о кошках </a></div>
</div>

<div class="middle DentaDefense cat">

    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 15,'view'=>'textClickable')); ?>

    <div class="rightSide">
        <div class="theBest">
            <h3>ЗАМЕЧАТЕЛЬНАЯ СОБАКА <br />НАЧИНАЕТСЯ С ЗАМЕЧАТЕЛЬНОГО ХОЗЯИНА</h3>
            В данном разделе вы можете найти рекомендации и советы по уходу за щенками и собаками. Ваша собака отблагодарит вас долгими годами здоровой жизни, полной любви и преданности.
        </div>
        <form class="chooseSearch" action="/articles/search/dogs">
            <div class="searchDog">
                <input name="search" type="text" onblur="this.value=(this.value=='')?this.title:this.value;" onfocus="this.value=(this.value==this.title)?'':this.value;" value="Найти о собаках..." title="Найти о собаках..."/>
                <input type="submit" value="ПОИСК"/>
            </div>
        </form>

        <?php $items = Articles::getTypeList('dogs'); ?>
        <div class="catsDiv">
            <div class="rightCat dog3">
                <img src="<?=Yii::app()->theme->baseUrl;?>/images/dog1.png" />
                <h4>О щенках</h4>
                <?php foreach($items as $key=>$val): ?>
                <div><a href="<?php echo $this->createUrl('/articles/default/search',array('type'=>'dogs','subtype'=>'puppy','filter0[]'=>$key))?>"><?php echo $val; ?></a></div>
                <?php endforeach; ?>
            </div>
            <div class="rightCat dog3">
                <img src="<?=Yii::app()->theme->baseUrl;?>/images/dog2.png" />
                <h4>О cобаках </h4>
                <?php foreach($items as $key=>$val): ?>
                <div><a href="<?php echo $this->createUrl('/articles/default/search',array('type'=>'dogs','subtype'=>'adultdog','filter0[]'=>$key))?>"><?php echo $val; ?></a></div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>