<?php
$this->breadcrumbs=array(
        Misc::t('Уход за кошкой')
    );
?>

    <div class="bigDog cat">
        <h1>РЕКОМЕНДАЦИИ И<br /> СОВЕТЫ <br /><small>ПО УХОДУ ЗА КОШКАМИ</small></h1>
        <div class="article"><a href="/articles/dogs">Статьи о собаках </a></div>
    </div>

    <div class="middle DentaDefense cat">

        <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 15,'view'=>'textClickable')); ?>

        <div class="rightSide">
            <div class="theBest">
                <h3>ЗАМЕЧАТЕЛЬНАЯ КОШКА <br />НАЧИНАЕТСЯ С ЗАМЕЧАТЕЛЬНОГО ХОЗЯИНА</h3>
                В данном разделе вы можете найти рекомендации и советы по уходу за котятами и кошками. Ваша кошка отблагодарит вас долгими годами здоровой жизни, полной любви и преданности.
            </div>
            <form class="chooseSearch" action="/articles/search/cats/">
                <div class="searchDog">
                    <input name="search" type="text" onblur="this.value=(this.value=='')?this.title:this.value;" onfocus="this.value=(this.value==this.title)?'':this.value;" value="Найти о кошках..." title="Найти о кошках..."/>
                    <input type="submit" value="ПОИСК"/>
                </div>
            </form>
            <?php $items = Articles::getTypeList('cats'); ?>
            <div class="catsDiv">
                <div class="rightCat dog3">
                    <img src="<?=Yii::app()->theme->baseUrl;?>/images/cat1.png" />
                    <h4>Статьи о котятах</h4>
                    <? foreach($items as $key=>$val):?>
                    <a href="<?=Yii::app()->createUrl('/articles/default/search',array('type'=>'cats','subtype'=>'kittens','filter0[]'=>$key))?>"><?=$val?></a> <br />
                    <? endforeach?>

                </div>
                <div class="rightCat dog3">
                    <img src="<?=Yii::app()->theme->baseUrl;?>/images/cat2.png" />
                    <h4>Статьи о кошках</h4>
                    <? foreach($items as $key=>$val):?>
                    <a href="<?=Yii::app()->createUrl('/articles/default/search',array('type'=>'cats','subtype'=>'adultcat','filter0[]'=>$key))?>"><?=$val?></a> <br />
                    <? endforeach?>

                </div>
            </div>


        </div>
    </div>