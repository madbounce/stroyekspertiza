<?php
    $parentCategory = isset($category->parentsId) ? $category->parentsId : $category;

    $emptyField = array('' => 'Все');

    Yii::app()->clientScript->registerScript('search', "
        $(document).ready(function(){
            var count = $('#filter0 input:checked').length;
            if(count)
                $('#articles-search').submit();
        });

        $('#filter0 :input').click(function(){
           $('#articles-search').submit();
        });

        $('#subtype :input').click(function(){
            var str = str = $('#articles-search').attr('action');
            str += $(this).val();
            history.pushState({}, '', str);

            $('#articles-search').submit();
        });

        $('#articles-search').submit(function(){
            $.fn.yiiListView.update('articles-form',{
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>

<form id="articles-search" class="chooseSearch" action="/articles/search/<?php echo $parentCategory->url;?>/" method="post">

    <h3>Фильтр поиска</h3>

    <div class="choose">
        <div class="VertLine1 fool">
            <p>Выберите категорию(и):</p>
            <?php
                if(!@$_GET['filter0']) $filter = array_keys(Articles::getTypeList($parentCategory->url));
                else $filter = $_GET['filter0'];
                echo CHtml::checkBoxList('filter0',@$filter, Articles::getTypeList($parentCategory->url),array('class'=>'selectCategory'));
            ?>
        </div>

        <?php if($parentCategory->url == 'cats'): ?>
        <div class="VertLine1" style="width: 125px;">
            <p>Уход за кошкой:</p>
            <?php echo CHtml::radioButtonList('subtype',@$category->url, $emptyField+ArticlesCategory::listCats('url'),array('class'=>'selectCategory')); ?>
        </div>
        <?php endif; ?>

        <?php if($parentCategory->url == 'dogs'): ?>
        <div class="VertLine1" style="width: 125px;">
            <p>Уход за собакой:</p>
            <?php echo CHtml::radioButtonList('subtype',@$category->url, $emptyField+ArticlesCategory::listDogs('url'),array('class'=>'selectCategory')); ?>
        </div>
        <?php endif; ?>

        <?php if($parentCategory->url == 'breeders'): ?>
        <div class="VertLine1" style="width: 125px;">
            <p>Уход за собакой:</p>
            <?php echo CHtml::radioButtonList('subtype',@$category->url, $emptyField+ArticlesCategory::listBreeders('url'),array('class'=>'selectCategory')); ?>
        </div>
        <?php endif; ?>

    </div>
    <div class="searchDog">
        <? $searchPhrase = $parentCategory->url == 'dogs' ? 'Статьи о собаках...':'Статьи о кошках...'?>
        <input name="search" type="text" value="<?php echo @$_GET['search']?>" title="<?=$searchPhrase?>"/>
        <input type="submit" value="ПОИСК"/>
    </div>
</form>
