<?php
    $parentCategory = isset($category->parentsId) ? $category->parentsId : $category;

    $this->breadcrumbs=array(
        $parentCategory->title => array('/articles/default/index','type'=>$parentCategory->url),
        $category->title,
    );

    $this->breadcrumbs_class .= '';
?>
    <?php if($parentCategory->url == 'cats'): ?>

        <div class="bigDog cat">
            <h1>РЕКОМЕНДАЦИИ И<br /> СОВЕТЫ <br /><small>ПО УХОДУ ЗА КОШКАМИ</small></h1>
            <div class="article"><a href="/articles/dogs">Статьи о собаках </a></div>
        </div>

    <?php else: ?>

        <div class="bigDog dog3">
            <h1>РЕКОМЕНДАЦИИ И СОВЕТЫ <br /><small>ПО УХОДУ ЗА СОБАКАМИ</small></h1>
            <div class="article"><a href="/articles/cats">Статьи о кошках </a></div>
        </div>

    <?php endif; ?>

    <div class="middle DentaDefense">

        <div class="" style="margin-top: 20px; float: left;">
            <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 15,'view'=>'textClickable')); ?>
        </div>

        <div class="rightSide">

            <?php $this->renderPartial("filters", array('category'=>$category)) ?>

            <div id="listArticles">
                <div class="items">
                    <?php $this->widget('zii.widgets.CListView', array(
                    'id' => 'articles-form',
                    'ajaxUrl'=>'/articles/'.$type,
                    'dataProvider' => $model->frontSearch($type, $subtype),
                    'itemView' => 'listItem',
                    'itemsTagName' => 'ul',
                    'itemsCssClass' => 'articles-list',
                    'sorterHeader'=>'Сортировать по:',
                    'sortableAttributes'=>array('title'),
                    'template' => '<div class="top">{sorter} {summary} {pager}</div> <hr> {items} <hr> <div class="bottom">{pager}</div>',
                    'pager' => array(
                        'firstPageLabel'=>'&larr;',
                        'prevPageLabel'=>'<',
                        'nextPageLabel'=>'>',
                        'lastPageLabel'=>'&rarr;',
                        'maxButtonCount'=>'10',
                        'header'=>'',
                        //'cssFile'=>false,
                        'cssFile'=>Yii::app()->theme->getBaseUrl(true).'/css/pager.css'
                    ),
                )); ?>
                </div>
            </div>
        </div>
    </div>