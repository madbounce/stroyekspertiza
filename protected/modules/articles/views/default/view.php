<?php
//    $parentCategory = isset($category->parentsId) ? $category->parentsId : $category;
    $subtype = '';
    if(isset($model->category->parentsId)){
        $type = $model->category->parentsId->url;
        $subtype = $model->category->url;
        $this->breadcrumbs=array(
            $model->category->parentsId->title => array('/articles/default/search','type'=>$model->category->parentsId->url),
            $model->category->title => array('/articles/default/search','type'=>$model->category->parentsId->url,'subtype'=>$model->category->url),
            $model->title
        );
    }else{
        $type = $model->category->url;
        $this->breadcrumbs=array(
            $model->category->title => array('/articles/default/search','type'=>$model->category->url),
            $model->title
        );
    }
    $this->breadcrumbs_class .= '';
?>

<div class="middle DentaDefense cat">
    <div class="leftSide sideBoxView">

        <?php if($type == 'dogs'): ?>

            <?php if($subtype == 'puppy'): ?>
                <img src="<?=Yii::app()->theme->baseUrl;?>/images/twoDOgs.png" class="twoDogs"/>
            <?php else: ?>
                <img src="<?=Yii::app()->theme->baseUrl;?>/images/dogAndCat6.png" class="twoDogs"/>
            <?php endif;?>

        <?php endif;?>

        <?php if($type == 'cats'): ?>
            <?php if($subtype == 'kittens'): ?>
		        <img src="<?=Yii::app()->theme->baseUrl;?>/images/kitten_art.png" width="170px" class="twoCats"/>
            <?php else: ?>
                <img src="<?=Yii::app()->theme->baseUrl;?>/images/cats_art.png" class="twoCats"/>
            <?php endif;?>
        <?php endif;?>

        <div class="related">
            <p><?=Misc::t('Похожие статьи');?></p>
            <? $articles = Articles::model()->findAll(array(
                'condition'=>'categoryId='.$model->categoryId.' and id<>'.$model->id,
                'order'=>'update_time DESC',
                'limit'=>5,
                )
            ); ?>
            <ul>
            <? foreach ($articles as $article): ?>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('/articles/default/view', array('url' => $article->slug))?>" title="<?=$article->title?>">
                        <?=$article->title?>
                    </a>
                </li>
            <? endforeach; ?>
            </ul>
        </div>
    </div>

    <div class="rightSide">
        <div class="theBest petDog">
            <h3><?=$model->title;?></h3>
            <div><?=$model->content;?></div>
        </div>
    </div>

</div>