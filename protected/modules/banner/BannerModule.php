<?php

class BannerModule extends CWebModule
{
	public $nameModule='Баннеры';
	public $showFrontEnd = false;
	public $showBackEnd = false;

	public function init()
	{
		if(($theme=Yii::app()->getTheme())!==null) $this->viewPath=$theme->viewPath.'/modules/'.$this->id;
		$this->setImport(array(
			'banner.models.*',
			'banner.components.*',
			'image.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
}
