<div class="hometout">
    <img alt="<?=$banner->title?>" src="http://media.iams.co.uk/en_uk/data_root/_images/callouts/footer/nutriition_matters_bg.gif" title="Iams® ProActive Health™ ">
    <div class="tout_homeimg gamesimg">
        <img width="<?php echo $banner->size->width; ?>" height="<?php echo $banner->size->height; ?>" alt="<?=$banner->title?>" title="Iams® ProActive Health™ для кошек" src="<?php echo Yii::app()->baseUrl."/../images/banners/".$banner->image ?>">
    </div>
    <div class="tout_content gamescontent">
        <p class="heading-color">
        <strong><?=$banner->title?></strong>
        </p>
        <p><?=$banner->text?></p>
        <p class="button">
        <a target="<?php echo $banner->target; ?>" href="<?php echo Yii::app()->createUrl("/banner/".$banner->id) ?>" title="подробнее"><img alt="подробнее" title="подробнее" src="img/see.png">
        </a>
        </p>
    </div>
</div>

<?php $this->widget('modules.banner.components.updateBanner', array('bannerId'=>$banner->id)); ?>