<?php
class rotator extends CWidget
{
	public $categoryId;
	public $sizeId; // Размер баннера
    public $view = 'image';
	public $countBanner = '4';
/*
*	Допустимые значение $sizeId(Можно редактировать в админке)
*
*	ID		Название			Размер
*
*	1		Растяжка			990 X 70
*	2		Мид Растяжка		325 X 70
*	3		Небоскреб			195 X 470
*	4		Мид Небоскреб		195 X 270
*	5		Мид Растяжка LT		325 X 70
*	6		Мид Растяжка CT		325 X 70
*	7		Мид Растяжка RT		325 X 70
*	8		Мид Растяжка LD		325 X 70
*	9		Мид Растяжка CD		325 X 70
*	10		Мид Растяжка RD		325 X 70
*/

	public function run()
	{
		$today = time();

		$path = parse_url($_SERVER['REQUEST_URI']);
		$pathElements = explode("/", $path['path']);

		for($i =0; $i <= count($pathElements); $i++)
		{
			if(is_null($pathElements[$i]) || $pathElements[$i] == "")
			{
				unset($pathElements[$i]);
			}
		}
		$pathElements = array_values($pathElements);

		$page = "/".(isset($pathElements[0]) ? $pathElements[0] : '');


		$criteria = new CDbCriteria();
		$criteria->with = array(
			'categories'=>array(
				'condition'=>'category ='.$this->categoryId,
			),
		);
		$criteria->condition = ' is_active=:isActive AND is_default=:isDefault AND link <> :link';
		$criteria->order = 'weight ASC';
		$criteria->limit = $this->countBanner;
		$criteria->params = array(
			':isActive'=>1,
			':isDefault'=>0,
            ':link'=>'/'.yii::app()->request->getPathInfo(),
		);
		$criteria->together = true;

		$banner = Banner::model()->findAll($criteria);

		if (empty($banner))
		{
			$criteria = new CDbCriteria();
			$criteria->with = array(
				'categories'=>array(
					'condition'=>'category ='.$this->categoryId,
					'group'=>'banner_id',
				),
			);
			$criteria->condition = ' is_active=:isActive AND is_default=:isDefault AND link <> :link';
			$criteria->order = 'last_show_time ASC, count ASC';
			$criteria->limit = $this->countBanner;
			$criteria->params = array(
//				':sizeId'=>$this->sizeId,
				':isActive'=>1,
				':isDefault'=>1,
				':link'=>'/'.yii::app()->request->getPathInfo(),
			);
			$criteria->together = true;

			$default = Banner::model()->findAll($criteria);
		}

		$data = empty($banner) ? $default : $banner;
/*
		if($data->type == 'application/x-shockwave-flash')
		{
			$this->render('flash',array(
				'banners'=>$data,
			));
		}
		elseif(!empty($data))
		{*/
			$this->render($this->view,array(
				'banners'=>$data,
			));
/*		}*/
	}
}
