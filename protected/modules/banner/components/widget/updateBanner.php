<?php
class updateBanner extends CWidget
{
	public $bannerId;
	public $countBanner;

	public function run()
	{
		$now = time();

		$model = Banner::model()->findByPk($this->bannerId);

		$model->last_show_time = $now;
		$model->count++;

		$model->update();
	}
}
