<?php
$cb = count($banners);
foreach($banners as $key=>$banner):
    $lastBanner = ($key == $cb -1);
    ?>
<div class="tout <?= $lastBanner ? ' lastlast' : '' ?>">
    <p><?=$banner->title?></p>
    <?php
    $bSrc = $banner->getPhoto(193,0,'foreground', array('method' => 'resize'));
    if($bSrc)
        echo CHtml::image($bSrc, $banner->title);
    ?>
    <span class="btn_more">
        <a target="<?php echo $banner->target; ?>" href="<?php echo $banner->link?>" title="<?= $banner->button_caption ?>"><?= $banner->button_caption ?></a><em></em>
    </span>
</div>
<?php $this->widget('banner.components.widget.updateBanner', array('bannerId'=>$banner->id, 'countBanner'=>4)); ?>
<? endforeach?>

