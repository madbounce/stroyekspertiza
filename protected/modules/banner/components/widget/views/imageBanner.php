<? foreach($banners as $key=>$banner):
    $lastBanner = ($key == count($banners)-1);
    ?>

<div class="tout">
    <img src="/images/background.gif" alt="Iams® ProActive Health™ Adult Chicken" title="Iams® ProActive Health™ Adult Chicken">

    <div class="tout_img"><img
        src="http://media.iams.co.uk/en_uk/data_root/_images/callouts/footer/products/PKG0972.png"
        alt="Iams® ProActive Health™ Adult Chicken" title="Iams® ProActive Health™ Adult Chicken"></div>
    <div class="tout_content">
        <p><strong><?=$banner->title?></strong></p>

        <p><?=$banner->text?></p>
        <br>
        <br>
        <p class="button">
            <a target="<?php echo $banner->target; ?>" id="detail_see" href="<?php echo $banner->link?>" title="подробнее">
                <?= $banner->button_caption ?>
            </a>
        </p>
    </div>
</div>

<?php $this->widget('banner.components.widget.updateBanner', array('bannerId'=>$banner->id)); ?>
<? endforeach?>