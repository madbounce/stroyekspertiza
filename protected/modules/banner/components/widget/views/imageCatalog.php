<? foreach($banners as $key=>$banner):
    $lastBanner = ($key == count($banners)-1);
    ?>

<div class="tout">
    <img src="<?=$banner->size->getPhoto(275,160,'background')?>">

    <div class="tout_img"><img
        src="<?=$banner->getPhoto(65,118,'foreground')?>"
        alt="<?=$banner->title?>"></div>
    <div class="tout_content">
        <p><strong><?=$banner->title?></strong></p>

        <p><?=$banner->text?></p>
        <br>

        <p class="button">
            <a target="<?php echo $banner->target; ?>" id="detail_see" href="<?php echo $banner->link?>" title="подробнее">
                <?= $banner->button_caption ?>
            </a>
        </p>
    </div>
</div>

<?php $this->widget('banner.components.widget.updateBanner', array('bannerId'=>$banner->id)); ?>
<? endforeach?>