<?php
    $cb = count($banners);
?>
<?php foreach($banners as $key=>$banner): $lastBanner = ($key == $cb -1); ?>

    <div class="windowBuy w<?php echo $key+1?>">
        <?php $bSrc = $banner->getPhoto(193,0,'foreground', array('method' => 'resize'));
            if($bSrc) echo CHtml::image($bSrc, $banner->title);
        ?>
        <h3><?=$banner->title?></h3>
        <a class="aboutWindow" target="<?php echo $banner->target; ?>" href="<?php echo $banner->link?>"><?= $banner->button_caption ?></a>
    </div>

    <?php $this->widget('banner.components.widget.updateBanner', array('bannerId'=>$banner->id, 'countBanner'=>4)); ?>
<? endforeach?>

