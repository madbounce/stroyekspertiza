<?php
$cb = count($banners);
foreach($banners as $key=>$banner):
    $lastBanner = ($key == $cb -1);
    ?>
<div class="sideblock <?= $lastBanner ? ' last_side_banner' : '' ?>">
    <h5><?=CHtml::encode($banner->title)?></h5>
    <?php
    $bSrc = $banner->getPhoto(163,0,'foreground', array('method' => 'resize'));
    if($bSrc)
        echo CHtml::image($bSrc, CHtml::encode($banner->title));
    ?>
    <p><?= CHtml::encode($banner->text)?></p>
    <span class="btn_more pink">
        <a target="<?php echo $banner->target; ?>" href="<?php echo $banner->link?>" title="<?=CHtml::encode($banner->button_caption) ?>"><?=CHtml::encode($banner->button_caption)?></a>
    </span>
</div>
<?php $this->widget('banner.components.widget.updateBanner', array('bannerId'=>$banner->id, 'countBanner'=>4)); ?>
<? endforeach?>