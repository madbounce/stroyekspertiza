<div class="leftBanners">
<?php
$cb = count($banners);
    if(!$cb) echo '&nbsp;';
foreach($banners as $key=>$banner):
    $lastBanner = ($key == $cb -1);
    ?>

    <div class="banner <?= $lastBanner ? ' last' : '' ?>">

        <a target="<?php echo $banner->target; ?>" href="<?php echo $banner->link?>" title="<?= $banner->button_caption ?>">

            <?php $bSrc = $banner->getPhoto(193,0,'foreground', array('method' => 'resize'));
                if($bSrc)
                    echo CHtml::image($bSrc, $banner->title);
                else
                    echo '<h2>'.$banner->title.'</h2>';
            ?>
        </a>


        <?php echo $banner->text; ?>

        <?php if($banner->button_caption): ?>
        <a href="<?php echo $banner->link?>" target="<?php echo $banner->target; ?>" class="LearnMore">
            <?=$banner->button_caption;?>
        </a>
        <?php endif; ?>

    </div>


    <?php $this->widget('banner.components.widget.updateBanner', array('bannerId'=>$banner->id, 'countBanner'=>4)); ?>
<? endforeach?>

</div>
