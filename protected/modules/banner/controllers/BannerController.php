<?php

class BannerController extends BackEndController
{
	public $layout='//layouts/main_banner';
	public $defaultAction='admin';

    public function actions(){
        return array(
			'uploadForegroundXML' => array(
                'class' => 'application.modules.image.components.UploadAction',
                'entity' => 'Slider',
                'tag' => 'foreground',
                'directory' => 'bannerXML',
                'view' => 'application.modules.image.components.widgets.views.imagesCatalog',
                'multiple' => false,
            ),
            'redactorUploadForegroundXML' => array(
                'class' => 'application.extensions.redactorjs.ERedactorUploadAction',
            ),
			'uploadBackgroundXML' => array(
                'class' => 'application.modules.image.components.UploadAction',
                'entity' => 'Slider',
                'tag' => 'background',
                'directory' => 'bannerXML',
                'view' => 'application.modules.image.components.widgets.views.imagesCatalog',
                'multiple' => false,
            ),
            'redactorUploadBackgroundXML' => array(
                'class' => 'application.extensions.redactorjs.ERedactorUploadAction',
            ),
			'uploadNoflashXML' => array(
                'class' => 'application.modules.image.components.UploadAction',
                'entity' => 'Slider',
                'tag' => 'noflash',
                'directory' => 'bannerXML',
                'view' => 'application.modules.image.components.widgets.views.imagesCatalog',
                'multiple' => true,
            ),
            'redactorUploadNoflashXML' => array(
                'class' => 'application.extensions.redactorjs.ERedactorUploadAction',
            ),
			
			'uploadForeground' => array(
                'class' => 'application.modules.image.components.UploadAction',
                'entity' => 'Banner',
                'tag' => 'foreground',
                'directory' => 'banner',
                'view' => 'application.modules.image.components.widgets.views.imagesCatalog',
                'multiple' => false,
            ),
            'redactorUploadForeground' => array(
                'class' => 'application.extensions.redactorjs.ERedactorUploadAction',
            ),
			'uploadBackground' => array(
                'class' => 'application.modules.image.components.UploadAction',
                'entity' => 'Banner',
                'tag' => 'background',
                'directory' => 'banner',
                'view' => 'application.modules.image.components.widgets.views.imagesCatalog',
                'multiple' => false,
            ),
            'redactorUploadBackground' => array(
                'class' => 'application.extensions.redactorjs.ERedactorUploadAction',
            ),
			
            'sortable' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'Banner'
            ),
			'sortableXML' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'Slider'
            )
        );
    }

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('admin','delete','create','update', 'updateXML','xml','sortable','sortableXML','toXML','createXML', 'toggleXML', 'uploadForegroundXML','redactorUploadForegroundXML', 'uploadBackgroundXML','redactorUploadBackgroundXML', 'uploadNoflashXML','redactorUploadNoflashXML','deleteXML','uploadForeground','redactorUploadForeground','uploadBackground','redactorUploadBackground'),
				'users'=>array('@'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	public function actionToggleXML($id,$attribute) {
		$slide = Slider::model()->findByPk($id);
		if($slide->active==1) 
			$slide->active = '0';
		else
			$slide->active = '1';
		$slide->save();
	}
	
	public function actionToXML() {
		Slider::model()->saveXML();
		$this->redirect(array('xml'));
	}
	
	public function actionXml() {
		$this->render('adminXML',array(
		));
	}
	
	public function actionCreate()
	{
		$model=new Banner;

		if(isset($_POST['Banner']))
		{
			$model->attributes=$_POST['Banner'];

			$model->create_time = time();
			$model->update_time = time();

			$model->start_date = strtotime($model->create_time);
			$model->end_date = strtotime('31.12.2999');
			$model->target="_self";

			if($model->save())
			{
                if(count($_POST['photos']) > 0)
                {
                    foreach($_POST['photos'] as $photo)
                    {
                        if($photo)
                        {
                            $image = Image::model()->findByPk($photo);
                            if($image)
                            {
                                $image->entity_id = $model->id;
                                $image->user_id = $this->id;
                                $image->save(false);
                            }
                        }
                    }
                }
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	public function actionCreateXML() {
		$model = new Slider();
		if(isset($_POST['Slider'])) {
			$model->attributes=$_POST['Slider'];
			
			$cta = '';
			if($_POST['Slider']['ctaText1'])
				$cta .= $_POST['Slider']['ctaSize1'].'!+!'.$_POST['Slider']['ctaCol1'].'!+!'.$_POST['Slider']['ctaText1'].'!+!'.$_POST['Slider']['ctaLinkurl1'].'!+!'.$_POST['Slider']['x1'].'!-!';
			if($_POST['Slider']['ctaText2'])
				$cta .= $_POST['Slider']['ctaSize2'].'!+!'.$_POST['Slider']['ctaCol2'].'!+!'.$_POST['Slider']['ctaText2'].'!+!'.$_POST['Slider']['ctaLinkurl2'].'!+!'.$_POST['Slider']['x2'].'!-!';
			if($_POST['Slider']['ctaText3'])
				$cta .= $_POST['Slider']['ctaSize3'].'!+!'.$_POST['Slider']['ctaCol3'].'!+!'.$_POST['Slider']['ctaText3'].'!+!'.$_POST['Slider']['ctaLinkurl3'].'!+!'.$_POST['Slider']['x3'].'!-!';
/*			if($_POST['Slider']['ctaText4'])
				$cta .= $_POST['Slider']['ctaSize4'].'!+!'.$_POST['Slider']['ctaCol4'].'!+!'.$_POST['Slider']['ctaText4'].'!+!'.$_POST['Slider']['ctaLinkurl4'].'!-!';
			if($_POST['Slider']['ctaText5'])
				$cta .= $_POST['Slider']['ctaSize5'].'!+!'.$_POST['Slider']['ctaCol5'].'!+!'.$_POST['Slider']['ctaText5'].'!+!'.$_POST['Slider']['ctaLinkurl5'];*/
			
			$model->cta = $cta;
            if(!$model->template)
                $model->template = 'template1.swf';
			
			
			if($model->save())
			{
//				Slider::model()->saveXML();
                if(count($_POST['photos']) > 0)
                {
                    foreach($_POST['photos'] as $photo)
                    {
                        if($photo)
                        {
                            $image = Image::model()->findByPk($photo);
                            if($image)
                            {
                                $image->entity_id = $model->id;
                                $image->user_id = Yii::app()->user->id;
                                $image->save(false);
                            }
                        }
                    }
                }
				$this->redirect(array('xml'));
			}
			
		}
		$this->render('updateXML',array(
			'model'=>$model,
		));
	}

	public function actionDeleteXML($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$model = Slider::model()->loadData($id);
//			$imagePath = Yii::app()->basePath."/../images/banners/";
//			@unlink($imagePath.$model->image);
			$model->delete();

			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('xml'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	public function actionUpdateXML($id) {
		$model = Slider::model()->loadData($id);

		if(isset($_POST['Slider'])) {
			$model->attributes=$_POST['Slider'];
			
			$cta = '';
			if($_POST['Slider']['ctaText1'])
				$cta .= $_POST['Slider']['ctaSize1'].'!+!'.$_POST['Slider']['ctaCol1'].'!+!'.$_POST['Slider']['ctaText1'].'!+!'.$_POST['Slider']['ctaLinkurl1'].'!+!'.$_POST['Slider']['x1'].'!-!';
			if($_POST['Slider']['ctaText2'])
				$cta .= $_POST['Slider']['ctaSize2'].'!+!'.$_POST['Slider']['ctaCol2'].'!+!'.$_POST['Slider']['ctaText2'].'!+!'.$_POST['Slider']['ctaLinkurl2'].'!+!'.$_POST['Slider']['x2'].'!-!';
			if($_POST['Slider']['ctaText3'])
				$cta .= $_POST['Slider']['ctaSize3'].'!+!'.$_POST['Slider']['ctaCol3'].'!+!'.$_POST['Slider']['ctaText3'].'!+!'.$_POST['Slider']['ctaLinkurl3'].'!+!'.$_POST['Slider']['x3'].'!-!';
/*			if($_POST['Slider']['ctaText4'])
				$cta .= $_POST['Slider']['ctaSize4'].'!+!'.$_POST['Slider']['ctaCol4'].'!+!'.$_POST['Slider']['ctaText4'].'!+!'.$_POST['Slider']['ctaLinkurl4'].'!-!';
			if($_POST['Slider']['ctaText5'])
				$cta .= $_POST['Slider']['ctaSize5'].'!+!'.$_POST['Slider']['ctaCol5'].'!+!'.$_POST['Slider']['ctaText5'].'!+!'.$_POST['Slider']['ctaLinkurl5'];*/
			
			
			$model->cta = $cta;
			
			
			if($model->save())
			{
//				Slider::model()->saveXML();
				$this->redirect(array('xml'));
			}
		}
		$this->render('updateXML',array(
			'model'=>$model,
		));
	}
	
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Banner']))
		{
			$model->attributes=$_POST['Banner'];

			$model->update_time = time();

			$model->start_date = strtotime($model->start_date);
			$model->end_date = strtotime('31.12.2999');
			$model->target = "_self";

			if($model->save())
			{
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$model = $this->loadModel($id);
			$imagePath = Yii::app()->basePath."/../images/banners/";
			@unlink($imagePath.$model->image);
			$model->delete();

			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	

	public function actionAdmin()
	{
		$model=new Banner('search');
		$model->unsetAttributes();
		if(isset($_GET['Banner']))
			$model->attributes=$_GET['Banner'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=Banner::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='banner-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
