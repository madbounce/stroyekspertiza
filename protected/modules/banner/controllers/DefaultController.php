<?php

class DefaultController extends Controller
{
	public $layout='//layouts/column1';

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionRedirect($id)
	{
		$model = Banner::model()->findByPk($id);
		if(!empty ($model))
		{
			$model->go_count++;
			$model->update();

			$this->redirect($model->link);
		}
	}
}