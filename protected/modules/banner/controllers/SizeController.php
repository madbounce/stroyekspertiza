<?php

class SizeController extends BackEndController
{
    public $layout='//layouts/main_banner';
	public $defaultAction='admin';

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('admin','delete','create','update','uploadBackground'),
				'users'=>array('@'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
	
	public function actions() {
		return array(
			'uploadBackground' => array(
                'class' => 'application.modules.image.components.UploadAction',
                'entity' => 'Size',
                'tag' => 'background',
                'directory' => 'banner',
                'view' => 'application.modules.image.components.widgets.views.imagesCatalog',
                'multiple' => false,
            ),
            'redactorUploadBackground' => array(
                'class' => 'application.extensions.redactorjs.ERedactorUploadAction',
            ),
		);
	}

	public function actionCreate()
	{
		$model=new Size;

		if(isset($_POST['Size']))
		{
			$model->attributes=$_POST['Size'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Size']))
		{
			$model->attributes=$_POST['Size'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$this->loadModel($id)->delete();

			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionAdmin()
	{
		$model=new Size('search');
		$model->unsetAttributes();
		if(isset($_GET['Size']))
			$model->attributes=$_GET['Size'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=Size::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='size-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
