-- phpMyAdmin SQL Dump
-- version 3.3.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 16, 2011 at 02:24 PM
-- Server version: 5.1.54
-- PHP Version: 5.3.5-1ubuntu7.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cherepovetz`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banner`
--

CREATE TABLE IF NOT EXISTS `tbl_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `size_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `link` varchar(255) NOT NULL,
  `target` varchar(20) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  `last_show_time` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `is_default` tinyint(4) NOT NULL,
  `show_limit` int(11) NOT NULL DEFAULT '0',
  `go_count` int(11) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_banner_size` (`size_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_banner`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_banners_categories`
--

CREATE TABLE IF NOT EXISTS `tbl_banners_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_banner_id` (`banner_id`),
  KEY `FK_category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_banners_categories`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_banner_category`
--

CREATE TABLE IF NOT EXISTS `tbl_banner_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `page` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page` (`page`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_banner_category`
--

INSERT INTO `tbl_banner_category` (`id`, `title`, `page`) VALUES
(1, 'Главная', '/'),
(2, 'Статьи', '/articles'),
(3, 'Афиша', '/events'),
(4, 'Отдых', '/rest'),
(5, 'Организации', '/companies'),
(6, 'Новости', '/news'),
(7, 'Товары', '/cuppons'),
(8, 'Авто', '/auto'),
(9, 'Недвижимость', '/estate'),
(10, 'Работа', '/jobs'),
(11, 'Объявления', '/notice'),
(12, 'Общение', '/media'),
(13, 'Образование', '/education'),
(14, 'Фото', '/photo');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banner_size`
--

CREATE TABLE IF NOT EXISTS `tbl_banner_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_banner_size`
--

INSERT INTO `tbl_banner_size` (`id`, `title`, `width`, `height`) VALUES
(1, 'Растяжка', 990, 70),
(2, 'Мид Растяжка', 325, 70),
(3, 'Небоскреб', 195, 470),
(4, 'Мид Небоскреб', 195, 270),
(5, 'Мид Растяжка LT', 325, 70),
(6, 'Мид Растяжка CT', 325, 70),
(7, 'Мид Растяжка RT', 325, 70),
(8, 'Мид Растяжка LD', 325, 70),
(9, 'Мид Растяжка CD', 325, 70),
(10, 'Мид Растяжка RD', 325, 70);
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_banner`
--
ALTER TABLE `tbl_banner`
  ADD CONSTRAINT `FK_banner_size` FOREIGN KEY (`size_id`) REFERENCES `tbl_banner_size` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_banners_categories`
--
ALTER TABLE `tbl_banners_categories`
  ADD CONSTRAINT `FK_banner_id` FOREIGN KEY (`banner_id`) REFERENCES `tbl_banner` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_category_id` FOREIGN KEY (`category_id`) REFERENCES `tbl_banner_category` (`id`) ON DELETE CASCADE;
