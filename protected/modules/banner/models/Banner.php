<?php

class Banner extends CActiveRecord
{
	public $categoryIds = array();

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{banner}}';
	}

	public function behaviors()
	{
		return array(
			'foreground' => array(
                'class' => 'image.components.ImageBehavior',
                'tag' => 'foreground',
                'multiple' => false,
            ),
            'CAdvancedArBehavior' => array(
                'class' => 'application.extensions.CAdvancedArBehavior'
            ),
            'SortBehavior' => array(
                'class' => 'ext.behaviors.SortBehavior',
                'sortAttribute' => 'weight'
            ),
        );
	}

	public function rules()
	{
		return array(
			array('text, size_id, link, target, show_limit, weight', 'required'),
			array('category, size_id, create_time, update_time, last_show_time, is_active, is_default, show_limit, count, weight', 'numerical', 'integerOnly'=>true),
			array('title, name, image, link, button_caption', 'length', 'max'=>255),
			array('target', 'length', 'max'=>20),
			array('type', 'length', 'max'=>50),
			array('image', 'file', 'types'=>'jpg, jpeg, png, gif, swf', 'allowEmpty'=>true),
//            array('image_alt', 'file', 'types'=>'jpg, jpeg, png, gif, swf', 'allowEmpty'=>true),
			array('category', 'safe'),

			array('id, category, active, name, title, text, size_id, image, type, create_time, update_time, start_date, end_date, last_show_time, is_active, is_default, show_limit, go_count, count, weight, button_caption', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'categories' => array(self::MANY_MANY, 'Category', '{{banners_categories}}(banner_id, category_id)'),
			'size' => array(self::BELONGS_TO, 'Size', 'size_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Заголовок',
			'text' => 'Текст',
			'categories' => 'Категории',
			'size_id' => 'Размер',
			'image' => 'Баннер',
			'type' => 'Тип',
			'link' => 'Ссылка',
			'target' => 'Тип ссылки',
			'create_time' => 'Создан',
			'update_time' => 'Обновлен',
			'start_date' => 'Начало вращения',
			'end_date' => 'Конец вращения',
			'last_show_time' => 'Дата последнего показа',
			'is_active' => 'Активный',
			'is_default' => 'Заглушка',
			'show_limit' => 'Ограничение по показам',
			'go_count' => 'Количество переходов',
			'count' => 'Количество показов',
			'weight' => 'Порядок сортировки',
            'button_caption' => 'Подпись кнопки',
            'categoryIds' => 'Категории'
		);
	}

	public function xml2array($fname){
		$sxi = new SimpleXmlIterator($fname, null, true);
		return $this->sxiToArray($sxi);
	}
	
	public function sxiToArray($sxi){
		$a = array();
		for( $sxi->rewind(); $sxi->valid(); $sxi->next() ) {
			if(!array_key_exists($sxi->key(), $a)){
			  $a[$sxi->key()] = array();
			}
			if($sxi->hasChildren()){
			  $a[$sxi->key()][] = $this->sxiToArray($sxi->current());
			}
			else{
			  $a[$sxi->key()][] = strval($sxi->current());
			}
		}
		return $a;
	}
	
	public function saveXML($id, $data) {
//		print_r($data);die();
		$xmlDoc = new DOMDocument();
		$xmlDoc->formatOutput = true;
		$xmlDoc->load(Yii::app()->createAbsoluteUrl("/ru_ru/xml/home/hpIamsxml.xml"));
		
		$count = 0;
		
		$features = $xmlDoc->getElementsByTagName('feature');
		$curNode = $features->item($id);
		
		$xmlDoc->getElementsByTagName('feature')->item($id)->getElementsByTagName('cta')->item(0)->getElementsByTagName('ctatext')->item(0)->nodeValue = $data['ctatext'];
		$xmlDoc->getElementsByTagName('feature')->item($id)->getElementsByTagName('cta')->item(0)->getElementsByTagName('linkurl')->item(0)->nodeValue = $data['linkurl'];
		
		$xmlDoc->getElementsByTagName('bodytext')->item($id)->getElementsByTagName('template')->nodeValue = $data['bodytext'];
		


/*
		$backgrounds = $xmlDoc->getElementsByTagName('background');
		$backgrounds->item($id)->nodeValue = $data['background'];

		$linkurl = $xmlDoc->getElementsByTagName('linkurl');
		$linkurl->item($id)->nodeValue = $data['linkurl'];
		
		$ctatext = $xmlDoc->getElementsByTagName('linkurl');
		$linkurl->item($id)->nodeValue = $data['linkurl'];
*/		
//		$subhead = $xmlDoc->getElementsByTagName('subhead');
//		$subhead->item($id)->nodeValue = $data['subhead'];
		
//		$subhead = $xmlDoc->getElementsByTagName('subhead');
//		$subhead->item($id)->nodeValue = $data['subhead'];
		
//		$xmlDoc->save("/ru_ru/xml/home/hpIamsxml.xml");
//die(Yii::app()->request->scriptFile."/hpIamsxml.xml");
		$xmlDoc->save(str_replace('/backend.php','',Yii::app()->request->scriptFile."/ru_ru/xml/home/hpIamsxml.xml"));

	}
	
	public function getXML() {
		$xml_object = $this->xml2array(Yii::app()->createAbsoluteUrl("/ru_ru/xml/home/hpIamsxml.xml"));

		$count = 0;
		$features = array();
		foreach($xml_object['feature'] as $item) {
			$feature['id'] = $count++;
			$feature['bodytext'] = isset($item['bodytext'][0]) ? $item['bodytext'][0]: '';
			$feature['template'] = isset($item['template'][0]) ? $item['template'][0]: '';
			$feature['subhead'] = isset($item['subhead'][0]) ? $item['subhead'][0] : '';
			$feature['background'] = isset($item['background'][0]) ? $item['background'][0] : '';
			$feature['ctatext'] = isset($item['cta'][0]['ctatext'][0]) ? $item['cta'][0]['ctatext'][0] : '';
			$feature['linkurl'] = isset($item['cta'][0]['linkurl'][0]) ? $item['cta'][0]['linkurl'][0] : '';
			
			$features[] = $feature;
		}
		return $features;
	}
	
	public function searchXML() {
		$dataProvider=new CArrayDataProvider($this->getXML(), array(
				'sort'=>array(
					'attributes'=>array(
						 'fileName',
					),
				),
				'pagination'=>array(
					'pageSize'=>8,
				),
			));	
		return $dataProvider;
	}
	
	public function search()
	{
		$criteria=new CDbCriteria;

		//$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('size_id',$this->size_id);
		/*$criteria->compare('image',$this->image,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('target',$this->target,true);
		$criteria->compare('create_time',$this->create_time);
		$criteria->compare('update_time',$this->update_time);
		$criteria->compare('start_date',$this->start_date);
		$criteria->compare('end_date',$this->end_date);
		$criteria->compare('last_show_time',$this->last_show_time);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('is_default',$this->is_default);
		$criteria->compare('show_limit',$this->show_limit);
		$criteria->compare('go_count',$this->go_count);
		$criteria->compare('count',$this->count);
		$criteria->compare('weight',$this->weight);*/

        if(isset($_GET['cId'])){
            $cid = (int)$_GET['cId'];
//            $criteria->join = 'JOIN banners_categories bc ON bc.banner_id = t.id';
            $criteria->addCondition('category = ' . $cid);
        }

        if(isset($_GET['search'])){
            $criteria->addSearchCondition('t.title', $_GET['search']);
        }

        $criteria->order = 't.weight ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getPhoto($width = 100, $height = 100, $tag, $options = null){
//        $image = Image::model()->entity('Slider', $this->id)->tag($tag)->find();
		$image = Image::model()->find("entity='Banner' AND entity_id={$this->id} AND tag='{$tag}'");
        if($image){
            $thumbPath = Yii::app()->baseUrl . ImageHelper::thumb($width, $height, $image->url, $options);
            return $thumbPath;
        } else
            return null;
    }
}