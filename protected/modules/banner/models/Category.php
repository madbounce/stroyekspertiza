<?php

class Category extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{banner_category}}';
	}

	public function rules()
	{
		return array(
			array('title, page', 'required'),
			array('title, page', 'length', 'max'=>100),
			array('page', 'unique'),

			array('id, title, page', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'banners' => array(self::MANY_MANY, 'Banner', '{{banners_categories}}(category_id, banner_id)'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'page' => 'Страница',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('page',$this->page,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function all(){
        $criteria = new CDbCriteria();
        return self::model()->findAll($criteria);
    }
}