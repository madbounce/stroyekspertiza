<?php

class Size extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{banner_size}}';
	}

	public function rules()
	{
		return array(
			array('title, width, height', 'required'),
			array('width, height', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),

			array('id, title, width, height', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'banners' => array(self::HAS_MANY, 'Banner', 'size_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'width' => 'Ширина',
			'height' => 'Высота',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('width',$this->width);
		$criteria->compare('height',$this->height);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getConcatenedSizeTitle()
	{
		return $this->title."(".$this->width."x".$this->height.")";
	}
	
	public function getPhoto($width = 100, $height = 100, $tag, $options = null){
//        $image = Image::model()->entity('Slider', $this->id)->tag($tag)->find();
		$image = Image::model()->find("entity='Size' AND entity_id={$this->id} AND tag='{$tag}'");
        if($image){
            $thumbPath = Yii::app()->baseUrl . ImageHelper::thumb($width, $height, $image->url, $options);
            return $thumbPath;
        } else
            return null;
    }
}