<?php

class Slider extends CActiveRecord
{
	
	public $ctaCol1="";
	public $ctaSize1="";
	public $ctaText1="";
	public $ctaLinkurl1="";
	public $x1="";

	public $ctaCol2="";
	public $ctaSize2="";
	public $ctaText2="";
	public $ctaLinkurl2="";
    public $x2="";
	
	public $ctaCol3="";
	public $ctaSize3="";
	public $ctaText3="";
	public $ctaLinkurl3="";
    public $x3="";
	
	public $ctaCol4="";
	public $ctaSize4="";
	public $ctaText4="";
	public $ctaLinkurl4="";
    public $x4="";

	public $ctaCol5="";
	public $ctaSize5="";
	public $ctaText5="";
	public $ctaLinkurl5="";
    public $x5="";

	public $categoryIds = array();

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{slider}}';
	}

	public function afterSave() {
		parent::afterSave();
		$this->saveXML();
	}
	
	public function behaviors()
	{
		return array(
            'image' => array(
                'class' => 'image.components.ImageBehavior',
                'tag' => 'image',
                'multiple' => true,
            ),
			'CAdvancedArBehavior' => array(
                'class' => 'application.extensions.CAdvancedArBehavior'
            ),
            'SortBehavior' => array(
                'class' => 'ext.behaviors.SortBehavior',
                'sortAttribute' => 'order'
            ),
        );
	}

	public function rules()
	{
		return array(
			array('id, name, template, background, foreground, headlineCol,headlineSize,headline, subheadCol,subheadSize,subhead, bodytextCol,bodytextSize,bodytext, cta, order, active, nonflash, noflashlink, headlineBold, subheadBold, bodytextBold, btn_x', 'safe'),
			array('id, name, template, background, foreground, headlineCol,headlineSize,headline, subheadCol,subheadSize,subhead, bodytextCol,bodytextSize,bodytext, cta, order, active, nonflash, noflashlink, headlineBold, subheadBold, bodytextBold, btn_x', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'categories' => array(self::MANY_MANY, 'Category', '{{banners_categories}}(banner_id, category_id)'),
			'size' => array(self::BELONGS_TO, 'Size', 'size_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'active' => 'Активно',
			'noflashlink'=>'Ссылка',
			'template' => 'Шаблон',
			'background' => 'Фон',
			'foreground' => 'Картинка',
			
			'headline' => 'Заголовок',
			'headlineCol' => 'Цвет',
			'headlineSize' => 'Размер',
			'headlineBold'=>'Жирный',
			
			'subhead' => 'Подзаголовок',
			'subhead Col' => 'Цвет',
			'subhead Size' => 'Размер',
			'subheadBold'=>'Жирный',
			
			'bodytext' => 'Текст',
			'bodytextCol' => 'Цвет',
			'bodytextSize' => 'Размер',
			'bodytextBold'=>'Жирный',

			'ctaCol1' => 'Цвет',
			'ctaSize1' => 'Размер',
			'ctaText1' => 'Текст',
			'ctaLinkurl1' => 'Ссылка',
			'x1' => 'x=',

			'ctaCol2' => 'Цвет',
			'ctaSize2' => 'Размер',
			'ctaText2' => 'Текст',
			'ctaLinkurl2' => 'Ссылка',
            'x2' => 'x=',
			
			'ctaCol3' => 'Цвет',
			'ctaSize3' => 'Размер',
			'ctaText3' => 'Текст',
			'ctaLinkurl3' => 'Ссылка',
            'x3' => 'x=',
			
			'ctaCol4' => 'Цвет',
			'ctaSize4' => 'Размер',
			'ctaText4' => 'Текст',
			'ctaLinkurl4' => 'Ссылка',
            'x4' => 'x=',
			
			'ctaCol5' => 'Цвет',
			'ctaSize5' => 'Размер',
			'ctaText5' => 'Текст',
			'ctaLinkurl5' => 'Ссылка',
            'x5' => 'x=',
		);
	}

	public function color() {
		return array(
			''=>'Черный',
			"White"=>"Белый",
			'Green'=>'Зеленый',
			"Dog - Dark Green"=>"Dog - Темнозеленый",
			"Dog - Green"=>"Dog - зеленый",
			"Dog - Light Green"=>"Dog - Светлозеленый",
			"Dog - Yellow"=>"Dog - Желтый",
			"Cat - Dark Orange"=>"Cat - Темнооранжевый",
			"Cat - Orange"=>"Cat - Оранжевый",
			"Cat - Light Orange"=>"Cat - Светлооранжевый",
			"Cat - Yellow"=>"Cat - Желтый",
			"HN Brown"=>"HN Коричневый",
			"HN Neutral"=>"HN Нейтральный",
			"HN Green"=>"HN Зеленый",
			"HN Orange"=>"HN Оранжевый",
		);
	}
	
	public function createNode($cData, $dom, $nodeF, $text, $size=null, $col = null, $bold = null, $x = null) {
		$node = $dom->createElement($nodeF);
		if(!$cData)
			$node->appendChild($dom->createTextNode($text));
		else {
			$cData = $dom->createCDATASection($text);
			//print_r($cData);
			$node->appendChild($cData);	
		}
		
		if($col !== null) {
			$col1 = $dom->createAttribute('color');
			$col1->value = $col;
			$node->appendChild($col1);
		}
		if($col !== null) {
			$size1 = $dom->createAttribute('size');
			$size1->value = $size;
			$node->appendChild($size1);
		}
        if($bold !== null) {
            $bold1 = $dom->createAttribute('b');
            if($bold == 1)
                $bold1->value = 'true';
            else
                $bold1->value = 'false';
            $node->appendChild($bold1);
        }
		if($x !== null) {
			$x1 = $dom->createAttribute('x');
            $x1->value = $x;

			$node->appendChild($x1);
		}
		return $node;
	}
	
	public function saveXML() {
		$xmlDoc = new DOMDocument();
		$xmlDoc->formatOutput = false;
		
		$xmlDoc->encoding = "utf-8";
		$xmlDoc->version = "1.0";
		
		
		$dom = $xmlDoc->createElement('iamshomepage');
		$dom->appendChild($xmlDoc->createElement('assetSWF', 'ru_ru/'));
		
		$rows = Slider::model()->findAll(array('condition'=>'active=1','order'=>'`order`'));
		foreach($rows as $row){
			$data = $this->loadData($row->id);
			
			$feature = $xmlDoc->createElement('feature', '');
			$feature->appendChild($this->createNode(0,$xmlDoc, 'template', $data->template ? ('/ru_ru/_swf/'.$data->template) : ''));
			$background = Image::model()->find("entity='Slider' AND entity_id={$row->id} and tag='background'");
			if($background) $background = $background->url;
			$feature->appendChild($this->createNode(0,$xmlDoc, 'background', $background ? ($background) : ''));
			$foreground = Image::model()->find("entity='Slider' AND entity_id={$row->id} and tag='foreground'");
			if($foreground) $foreground = $foreground->url;
			$feature->appendChild($this->createNode(0,$xmlDoc, 'foreground', ($foreground ? $foreground : '')));
			
			$feature->appendChild($this->createNode(1,$xmlDoc, 'headline', $data->headline,$data->headlineSize,$data->headlineCol, $data->headlineBold));
			$feature->appendChild($this->createNode(1,$xmlDoc, 'bodytext', $data->bodytext,$data->bodytextSize,$data->bodytextCol,$data->bodytextBold));
			$feature->appendChild($this->createNode(1,$xmlDoc, 'subhead', $data->subhead,$data->subheadSize,$data->subheadCol,$data->subheadBold));

			
			if($data->ctaText1) {
				$ctaNode = $this->createNode(0,$xmlDoc, 'cta', '', $data->ctaSize1, $data->ctaCol1, null, $data->x1);
				$ctaNode->appendChild($this->createNode(1,$xmlDoc,'ctatext',$data->ctaText1));
				$ctaNode->appendChild($this->createNode(0,$xmlDoc,'linkurl',$data->ctaLinkurl1));
				$feature->appendChild($ctaNode);
			}
			if($data->ctaText2) {
				$ctaNode = $this->createNode(0,$xmlDoc, 'cta', '', $data->ctaSize2, $data->ctaCol2, null, $data->x2);
				$ctaNode->appendChild($this->createNode(1,$xmlDoc,'ctatext',$data->ctaText2));
				$ctaNode->appendChild($this->createNode(0,$xmlDoc,'linkurl',$data->ctaLinkurl2));
				$feature->appendChild($ctaNode);
			}
			if($data->ctaText3) {
				$ctaNode = $this->createNode(0,$xmlDoc, 'cta', '', $data->ctaSize3, $data->ctaCol3, null, $data->x3);
				$ctaNode->appendChild($this->createNode(1,$xmlDoc,'ctatext',$data->ctaText3));
				$ctaNode->appendChild($this->createNode(0,$xmlDoc,'linkurl',$data->ctaLinkurl3));
				$feature->appendChild($ctaNode);
			}
/*			if($data->ctaText4) {
				$ctaNode = $this->createNode(0,$xmlDoc, 'cta', '', $data->ctaSize4, $data->ctaCol4);
				$ctaNode->appendChild($this->createNode(1,$xmlDoc,'ctatext',$data->ctaText4));
				$ctaNode->appendChild($this->createNode(0,$xmlDoc,'linkurl',$data->ctaLinkurl4));
				$feature->appendChild($ctaNode);
			}
			if($data->ctaText5) {
				$ctaNode = $this->createNode(0,$xmlDoc, 'cta', '', $data->ctaSize1, $data->ctaCol5);
				$ctaNode->appendChild($this->createNode(1,$xmlDoc,'ctatext',$data->ctaText5));
				$ctaNode->appendChild($this->createNode(0,$xmlDoc,'linkurl',$data->ctaLinkurl5));
				$feature->appendChild($ctaNode);
			}*/

			$dom->appendChild($feature);
		}
		
		$xmlDoc->appendChild($dom);
		
		$xmlDoc->save(str_replace('backend.php','',Yii::app()->request->scriptFile."hpIamsxml.xml"));
//		echo $xmlDoc->saveXML();
//		die();
	}
	
	public function loadData($id) {
		$model = Slider::model()->findByPk($id);
		
		$i = 1;

		foreach(array_filter(explode('!-!',$model->cta)) as $cta) {
			$X='x'.$i;
			$Size='ctaSize'.$i;
			$Col='ctaCol'.$i;
			$Text='ctaText'.$i;
			$Linkurl='ctaLinkurl'.$i++;
			@list($model->$Size,$model->$Col,$model->$Text,$model->$Linkurl, $model->$X) = explode('!+!',$cta);
//			$model->cta[] = $currCta;
		}
		return $model;
	}
/*	
	public function saveXML($id, $data) {
//		print_r($data);die();
		$xmlDoc = new DOMDocument();
		$xmlDoc->formatOutput = true;
		$xmlDoc->load(Yii::app()->createAbsoluteUrl("/ru_ru/xml/home/hpIamsxml.xml"));
		
		$count = 0;
		
		$features = $xmlDoc->getElementsByTagName('feature');
		$curNode = $features->item($id);
		
		$xmlDoc->getElementsByTagName('feature')->item($id)->getElementsByTagName('cta')->item(0)->getElementsByTagName('ctatext')->item(0)->nodeValue = $data['ctatext'];
		$xmlDoc->getElementsByTagName('feature')->item($id)->getElementsByTagName('cta')->item(0)->getElementsByTagName('linkurl')->item(0)->nodeValue = $data['linkurl'];
		
		$xmlDoc->getElementsByTagName('feature')->item($id)->getElementsByTagName('template')->item(0)->getElementsByTagName('linkurl')->item(0)->nodeValue = $data['template'];
		

		$xmlDoc->save(str_replace('/backend.php','',Yii::app()->request->scriptFile."/ru_ru/xml/home/hpIamsxml.xml"));

	}
*/	

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		//$criteria->compare('title',$this->title,true);
		//$criteria->compare('size_id',$this->size_id);
		/*$criteria->compare('image',$this->image,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('target',$this->target,true);
		$criteria->compare('create_time',$this->create_time);
		$criteria->compare('update_time',$this->update_time);
		$criteria->compare('start_date',$this->start_date);
		$criteria->compare('end_date',$this->end_date);
		$criteria->compare('last_show_time',$this->last_show_time);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('is_default',$this->is_default);
		$criteria->compare('show_limit',$this->show_limit);
		$criteria->compare('go_count',$this->go_count);
		$criteria->compare('count',$this->count);
		$criteria->compare('weight',$this->weight);*/

        $criteria->order = 't.order ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getPhoto($width = 100, $height = 100, $tag, $options = null){
//        $image = Image::model()->entity('Slider', $this->id)->tag($tag)->find();
		$image = Image::model()->find("entity='Slider' AND entity_id={$this->id} AND tag='{$tag}'");
        if($image){
            $thumbPath = Yii::app()->baseUrl . ImageHelper::thumb($width, $height, $image->url, $options);
            return $thumbPath;
        } else
            return null;
    }
}