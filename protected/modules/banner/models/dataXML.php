<?php

class dataXML extends CFormModel {

	public $id;
	public $background;
	public $bodytext;
	public $subhead;
	public $ctatext;
	public $linkurl;
	public $template;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('id, background, bodytext, subhead, ctatext, linkurl, template', 'safe'),

		);
	}

	/**
	 * Declares attribute labels.
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'id'=>Yii::t('app', 'ID:'),
			'background'=>Yii::t('app', 'Фон:'),
			'bodytext'=>Yii::t('app', 'Заголовок:'),
			'subhead'=>Yii::t('app', 'Подзаголовок:'),
			'ctatext'=>Yii::t('app', 'Текст:'),
			'linkurl'=>Yii::t('app', 'Ссылка:'),
		);
	}
}
