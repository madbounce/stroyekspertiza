<?php

class sliderForm extends CFormModel {

	public $id;
	public $template;
	public $background;
	public $foreground;
	public $headline;
	public $headlineCol;
	public $headlineSize;
	public $bodytext;
	public $bodytextCol;
	public $bodytextSize;
	public $subhead;
	public $subheadCol;
	public $subheadSize;
	public $cta;
	
	public $ctaCol1="";
	public $ctaSize1="";
	public $ctaText1="";
	public $ctaLinkurl1="";
	
	public $ctaCol2="";
	public $ctaSize2="";
	public $ctaText2="";
	public $ctaLinkurl2="";
	
	public $ctaCol3="";
	public $ctaSize3="";
	public $ctaText3="";
	public $ctaLinkurl3="";
	
	public $ctaCol4="";
	public $ctaSize4="";
	public $ctaText4="";
	public $ctaLinkurl4="";
	
	public $ctaCol5="";
	public $ctaSize5="";
	public $ctaText5="";
	public $ctaLinkurl5="";
	

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('id, foreground, background, bodytext, subhead, cta, headline, template', 'safe'),

		);
	}

	/**
	 * Declares attribute labels.
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'template' => 'Шаблон',
			'background' => 'Фон',
			'foreground' => 'Картинка',
			'headline' => 'Заголовок',
			'subhead' => 'Подзаголовок',
			'bodytext' => 'Текст',
			'cta' => 'Кнопки',
		);
	}
}
