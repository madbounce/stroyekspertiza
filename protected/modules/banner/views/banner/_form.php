<?php
if ($model->isNewRecord && isset($_GET['cId']) && $_GET['cId'])
    $model->category = (int) $_GET['cId'];
?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'banner-form',
    'enableAjaxValidation' => false,
    'htmlOptions'=>array('enctype' => 'multipart/form-data', 'class' => 'product'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textField($model, 'title', array('class' => 'name', 'maxlength' => 255)); ?>
    <div class="checkbox">
        <?php echo $form->checkBox($model, 'is_active')?>
        <?php echo $form->labelEx($model, 'is_active')?>
    </div>
</div>


<div id="column_left">
    <div class="block layer param" style="min-height: 395px;">
    
        <p class="help-block"><?php echo Yii::t('app', 'Поля помеченные <span class="required">*</span> обязательны для заполнения.'); ?></p>
        
        <?php echo $form->dropDownListRow($model, 'category', CHtml::listData(Category::model()->findAll(), 'id','title')); ?>
        <?php echo $form->textFieldRow($model, 'button_caption', array('class' => 'span5', 'maxlength' => 255)); ?>
        <?php echo $form->textAreaRow($model, 'text', array('class' => 'span5')); ?>
        <?php echo $form->dropDownListRow($model, 'size_id', CHtml::listData(Size::model()->findAll(), 'id', 'concatenedSizeTitle'), array('empty'=>'Выбрать...')); ?>
    
        <?php echo $form->textFieldRow($model, 'link', array('class' => 'span5', 'maxlength' => 255)); ?>

    
    </div>
</div>
<div id="column_right">
  	<div class="block layer images" style="min-height: 395px;">
        <h2>Изображения</h2>
        
        <?php $this->widget('application.modules.image.components.widgets.UploadWidget', array(
            'model' => $model,
            'tag' => 'foreground',
            'action' => array('/banner/banner/uploadForeground'),
            'multiple' => false,
            'view' => 'imagesCatalog',
            'uploadButtonText' => Yii::t('news', 'Загрузить изображение'),
        )); ?>
        
    </div>
</div>

<div style="clear: both"></div>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('banner', 'Создать') : Yii::t('banner', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>