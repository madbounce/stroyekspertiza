<?php 
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'catalog-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'class' => 'product'
    ),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textField($model, 'name', array('class' => 'name', 'maxlength' => 255)); ?>
    <div class="checkbox">
        <?php echo $form->checkBox($model, 'active')?>
        <?php echo $form->labelEx($model, 'active')?>
    </div>
</div>

<!-- Левая колонка свойств товара -->
<div id="column_left">

    <!-- Параметры страницы -->
    <div class="block layer param" style="min-height: 395px;">
        <h2>Параметры</h2>
        <ul>
            <li>
                <?php echo $form->labelEx($model, 'template', array('class' => 'property')); ?>
                <?php echo $form->dropDownList($model, 'template', array(""=>"Нет надписей","template1.swf"=>'Надписи справа',"template3.swf"=>'Надписи слева',), array('encode' => false, 'class' => 'simpla_sel')); ?>
			</li>
            <li>
			<?php $this->widget('application.modules.image.components.widgets.UploadWidget', array(
                'model' => $model,
                'tag' => 'noflash',
                'action' => array('/banner/banner/uploadNoflashXML'),
                'multiple' => false,
                'view' => 'imagesCatalog',
                'uploadButtonText' => Yii::t('news', 'Загрузить заглушку'),
            )); ?>
            </li>
            <li>
            	<?php echo $form->labelEx($model, 'noflashlink', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'noflashlink', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
		</ul>
	</div>
    <div class="block layer param">
        <h2>Заголовок</h2>
        <ul>
        	<li>
				<?php echo $form->labelEx($model, 'headlineCol', array('class' => 'property')); ?>
                <?php echo $form->dropDownList($model, 'headlineCol', Slider::model()->color(), array('encode' => false, 'class' => 'simpla_sel')); ?>
        	</li>
            <li>
				<?php echo $form->labelEx($model, 'headlineSize', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'headlineSize', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
        	</li>
			<li>
				<?php echo $form->labelEx($model, 'headline', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'headline', array('class' => 'simpla_inp')); ?>
        	</li>
<!--            <li>
				<?php echo $form->labelEx($model, 'headlineBold', array('class' => 'property')); ?>
                <?php echo $form->checkBox($model, 'headlineBold', array('class' => 'simpla_inp')); ?>
        	</li>-->
		</ul>
	</div>
    <div class="block layer param" style="height:310px;">
        <h2>Текст</h2>
        <ul>
        	<li>
				<?php echo $form->labelEx($model, 'bodytextCol', array('class' => 'property')); ?>
                <?php echo $form->dropDownList($model, 'bodytextCol', Slider::model()->color(), array('encode' => false, 'class' => 'simpla_sel')); ?>
        	</li>
            <li>
				<?php echo $form->labelEx($model, 'bodytextSize', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'bodytextSize', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
        	</li>
			<li>
				<?php echo $form->labelEx($model, 'bodytext', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'bodytext', array('class' => 'simpla_inp')); ?>
        	</li>
<!--            <li>
				<?php echo $form->labelEx($model, 'bodytextBold', array('class' => 'property')); ?>
                <?php echo $form->checkBox($model, 'bodytextBold', array('class' => 'simpla_inp')); ?>
        	</li>-->
		</ul>
	</div>
    <div class="block layer param">
        <h2>Кнопка №2</h2>
        <ul>
        	<li>
				<?php echo $form->labelEx($model, 'ctaCol2', array('class' => 'property')); ?>
                <?php echo $form->dropDownList($model, 'ctaCol2', Slider::model()->color(), array('encode' => false, 'class' => 'simpla_sel')); ?>
                <?php echo $form->textField($model, 'x2', array('class' => 'simpla_inp', 'style' => 'width:50px;', 'maxlength' => 255)); ?>
        	</li>
            <li>
				<?php echo $form->labelEx($model, 'ctaSize2', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'ctaSize2', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
        	</li>
			<li>
				<?php echo $form->labelEx($model, 'ctaText2', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'ctaText2', array('class' => 'simpla_inp')); ?>
        	</li>
            <li>
				<?php echo $form->labelEx($model, 'ctaLinkurl2', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'ctaLinkurl2', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
        	</li>
		</ul>
	</div>
</div>

<!-- Правая колонка свойств товара -->
<div id="column_right">

    <!-- Изображение категории -->
    <div class="block layer images" style="min-height: 395px;">
        <h2>Изображения</h2>
        
        <?php $this->widget('application.modules.image.components.widgets.UploadWidget', array(
            'model' => $model,
            'tag' => 'background',
            'action' => array('/banner/banner/uploadBackgroundXML'),
            'multiple' => false,
            'view' => 'imagesCatalog',
            'uploadButtonText' => Yii::t('news', 'Загрузить фон'),
        )); ?>
        <?php $this->widget('application.modules.image.components.widgets.UploadWidget', array(
            'model' => $model,
            'tag' => 'foreground',
            'action' => array('/banner/banner/uploadForegroundXML'),
            'multiple' => false,
            'view' => 'imagesCatalog',
            'uploadButtonText' => Yii::t('news', 'Загрузить изображение'),
        )); ?>
        
	</div>
    
    <div class="block layer param">
        <h2>Подзаголовок</h2>
        <ul>
        	<li>
				<?php echo $form->labelEx($model, 'subheadCol', array('class' => 'property')); ?>

                <?php echo $form->dropDownList($model, 'subheadCol', Slider::model()->color(), array('encode' => false, 'class' => 'simpla_sel')); ?>
        	</li>
            <li>
				<?php echo $form->labelEx($model, 'subheadSize', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'subheadSize', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
        	</li>
			<li>
				<?php echo $form->labelEx($model, 'subhead', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'subhead', array('class' => 'simpla_inp')); ?>
        	</li>
<!--            <li>
				<?php echo $form->labelEx($model, 'subheadBold', array('class' => 'property')); ?>
                <?php echo $form->checkBox($model, 'subheadBold', array('class' => 'simpla_inp')); ?>
        	</li>-->
		</ul>
	</div>
    
    <div class="block layer param">
        <h2>Кнопка №1</h2>
        <ul>
        	<li>
				<?php echo $form->labelEx($model, 'ctaCol1', array('class' => 'property')); ?>

                <?php echo $form->dropDownList($model, 'ctaCol1', Slider::model()->color(), array('encode' => false, 'class' => 'simpla_sel')); ?>
                <?php echo $form->textField($model, 'x1', array('class' => 'simpla_inp', 'style' => 'width:50px;', 'maxlength' => 255)); ?>
        	</li>
            <li>
				<?php echo $form->labelEx($model, 'ctaSize1', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'ctaSize1', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
        	</li>
			<li>
				<?php echo $form->labelEx($model, 'ctaText1', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'ctaText1', array('class' => 'simpla_inp')); ?>
        	</li>
            <li>
				<?php echo $form->labelEx($model, 'ctaLinkurl1', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'ctaLinkurl1', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
        	</li>
		</ul>
	</div>
    <div class="block layer param">
        <h2>Кнопка №3</h2>
        <ul>
        	<li>
				<?php echo $form->labelEx($model, 'ctaCol3', array('class' => 'property')); ?>
                <?php echo $form->dropDownList($model, 'ctaCol3', Slider::model()->color(), array('encode' => false, 'class' => 'simpla_sel')); ?>
                <?php echo $form->textField($model, 'x3', array('class' => 'simpla_inp', 'style' => 'width:50px;', 'maxlength' => 255)); ?>
        	</li>
            <li>
				<?php echo $form->labelEx($model, 'ctaSize3', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'ctaSize3', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
        	</li>
			<li>
				<?php echo $form->labelEx($model, 'ctaText3', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'ctaText3', array('class' => 'simpla_inp')); ?>
        	</li>
            <li>
				<?php echo $form->labelEx($model, 'ctaLinkurl3', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'ctaLinkurl3', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
        	</li>
		</ul>
	</div>
</div>
    
<div style="clear: both"></div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>
