<?php
$dp = $model->search();
$countAll = $dp->totalItemCount;
?>

<div class="page-content">
    <div class="row two-columns">
        <div class="span9">

            <div class="page-header admin-header inner-header">
                <h3><?php echo Yii::t('app', '{n} баннер|{n} баннера|{n} баннеров', $countAll);?></h3>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => Yii::t('banner', 'Добавить баннер'),
                    'size' => 'small',
                    'url' => (isset($_GET['cId']) && ($_GET['cId']))? array('/banner/banner/create', 'cId' => $_GET['cId']) : array('/banner/banner/create'),
                ));
                ?>
            </div>

            <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
                'id'=>'banner-grid',
                'type' => 'striped bordered',
                'dataProvider'=>$dp,
                //'filter' => $model,
                'enableSorting' => false,
                'sortableRows' => true,
                'sortableAjaxSave'=>true,
                'sortableAttribute'=>'weight',
                'sortableAction'=> 'banner/banner/sortable',
                'template' => '{pager}{items}{pager}',
                'columns'=>array(
                    array(
                        'header' => Yii::t('banner', 'Фото'),
                        'class'=>'bootstrap.widgets.TbImageColumn',
                        'imagePathExpression' => '$data->getPhoto(48,48,"foreground")',
                        'imageOptions' => array('width' => '48px'),
                        'htmlOptions' => array('width' => '48px')
                    ),
                    array(
                        'name' => 'title',
                        'type'=>'raw',
                        'value' => 'CHtml::link($data->title, array("/banner/banner/update", "id" => $data->id))',
                        'htmlOptions' => array('encodeLabel'=>false),
                    ),
                    array(
                        'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template'=>'{delete}',
                    ),

                ),
            )); ?>
        </div>
        <div class="span3 sidebar">
            <div class="b-side">
                <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route), 'get', array('class' => 'search-form')); ?>

                <div class="input-append">
                    <?php echo CHtml::textField('search', isset($_GET['search']) ? $_GET['search'] : '', array('class' => 'input-medium search-input'))?>
                    <button class="btn" type="submit"><i class="icon-search"></i></button>
                </div>
                <?php echo CHtml::endForm(); ?>
            </div>

            <div class="b-side">
                <?php
                $categories = array(
                    array('label'=>Yii::t('catalog','Все категории'), 'url'=>array('/banner/banner/admin'), 'active' => (!isset($_GET['cId']))),
                );
                $cats = Category::all();
                if($cats){
                    foreach($cats as $cat)
                        array_push($categories, array(
                                'label' => $cat->title,
                                'url' => array('/banner/banner/admin', 'cId' => $cat->id),
                                'active' => (isset($_GET['cId']) && ($_GET['cId'] == $cat->id))
                            )
                        );
                }

                $this->widget('bootstrap.widgets.TbMenu', array(
                    'type'=>'list',
                    'items' => $categories
                ));
                ?>
            </div>

        </div>
    </div>
</div>