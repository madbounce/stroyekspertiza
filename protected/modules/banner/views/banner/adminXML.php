<?php
$this->breadcrumbs = array(
    Yii::t('banner', 'Баннеры'),
);
?>
<div class="page-header admin-header">
    <h3><?php echo Yii::t('banner', 'Слайдер')?></h3>
    
    <?php $this->widget('bootstrap.widgets.TbButton', array(
                'label' => Yii::t('catalog', 'Добавить слайд'),
                'size' => 'small',
                'url' => array('/banner/banner/createXML'),
            ));
                ?>
                
     <?php /*$this->widget('bootstrap.widgets.TbButton', array(
                'label' => Yii::t('catalog', 'Обновить XML'),
                'size' => 'small',
                'url' => array('/banner/banner/toXML'),
            ));*/
                ?>
</div>
<div class="page-content">
    <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'id'=>'banner-grid',
    'type' => 'striped bordered',
    'dataProvider'=>Slider::model()->search(),
    //'filter' => $model,
    'sortableRows' => true,
    'sortableAjaxSave'=>true,
    'sortableAttribute'=>'order',
    'sortableAction'=> 'banner/banner/sortableXML',
    'columns'=>array(
        array(
			'header' => 'Фото',
			'class'=>'bootstrap.widgets.TbImageColumn',
			'imagePathExpression' => '$data->getPhoto(48,24,"background")',
			'imageOptions' => array('width' => '48px'),
			'htmlOptions' => array('width' => '24px')
                    ),
		array(
			'name' => 'name',
			'header' => 'Название',
			'type'=>'raw',
			'value' => 'CHtml::link($data->name, array("/banner/banner/updateXML", "id" => $data->id))',
			'htmlOptions' => array('encodeLabel'=>false),
        ),
		array(
//            'class' => 'application.extensions.jtogglecolumn.JToggleColumn',
			'class' => 'bootstrap.widgets.TbToggleColumn',
            'name' => 'active',
            'filter' => array(0 => Misc::t('Нет'), 1 => Misc::t('Да')),
            'checkedButtonLabel' => Misc::t('Отменить подтверждение'),
            'uncheckedButtonLabel' => Misc::t('Подтвердить'),
			'toggleAction'=>'toggleXML',
            'htmlOptions' => array(
                'width' => '60',
            ),
        ),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{delete}',
            'deleteButtonUrl'=>'"deleteXML/".$data["id"]',
        ),
    ),
)); ?>
</div>