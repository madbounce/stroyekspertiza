<div class="page-header">
    <h3><?php echo Yii::t('catalog', 'Редактирование слайда'); ?></h3>
</div>

<div class="page-content">
	<?php echo $this->renderPartial('_formXML', array('model'=>$model)); ?>
</div>