<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'category-form',
    'enableAjaxValidation' => false,
	'htmlOptions' => array(
        'class' => 'product'
    ),
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>

<div class="block layer" style="width: auto; min-height: auto;">

<p class="help-block"><?php echo Yii::t('app', 'Поля помеченные <span class="required">*</span> обязательны для заполнения.'); ?></p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 100)); ?>

<?php echo $form->textFieldRow($model, 'page', array('class' => 'span5', 'maxlength' => 100)); ?>

</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>