<?php
$dp = $model->search();
$countAll = $dp->totalItemCount;
?>
<div class="page-header admin-header">
    <h3><?php echo Yii::t('app', '{n} категория|{n} категории|{n} категорий', $countAll);?></h3>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'label' => Yii::t('translate', 'Добавить категорию'),
    'size' => 'small',
    'url' => array('category/create'),
));
    ?>
</div>

<div class="page-content">
    <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'id'=>'banner-category-grid',
    'type' => 'striped bordered',
    'dataProvider'=>$dp,
    //'filter'=>$model,
    //'sortableRows' => true,
    //'sortableAjaxSave'=>true,
    //'sortableAttribute'=>'weight',
    //'sortableAction'=> 'banner/banner/sortable',
    'template' => '{pager}{items}{pager}',
    'columns'=>array(
        //'id',
        'title',
        'page',
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',

        ),
    ),
)); ?>
</div>