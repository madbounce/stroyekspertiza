<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'size-form',
    'enableAjaxValidation' => false,
	'htmlOptions' => array(
        'class' => 'product'
    ),
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textField($model, 'title', array('class' => 'name', 'maxlength' => 255)); ?>
</div>

<div id="column_left">
    <div class="block layer">
    
        <p class="help-block"><?php echo Yii::t('app', 'Поля помеченные <span class="required">*</span> обязательны для заполнения.'); ?></p>

        <?php echo $form->textFieldRow($model, 'width', array('class' => 'span5')); ?>
        <?php echo $form->textFieldRow($model, 'height', array('class' => 'span5')); ?>
    
    </div>
</div>
<div id="column_right">
	<div class="block layer">
    	<h2>Фон</h2>
        
        <?php $this->widget('application.modules.image.components.widgets.UploadWidget', array(
            'model' => $model,
            'tag' => 'background',
            'action' => array('/banner/size/uploadBackground'),
            'multiple' => false,
            'view' => 'imagesCatalog',
            'uploadButtonText' => Yii::t('news', 'Загрузить фон'),
        )); ?>
	</div>
</div>
	
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
    )); ?>
    </div>

<?php $this->endWidget(); ?>
