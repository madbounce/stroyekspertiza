<?php
$dp = $model->search();
$countAll = $dp->totalItemCount;
?>
<div class="page-header admin-header">
    <h3><?php echo Yii::t('app', '{n} размер|{n} размера|{n} размеров', $countAll);?></h3>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'label' => Yii::t('translate', 'Добавить размер'),
    'size' => 'small',
    'url' => array('/banner/size/create'),
));
    ?>
</div>

<div class="page-content">
    <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'id'=>'banner-size-grid',
    'type' => 'striped bordered',
    'dataProvider'=>$dp,
    //'filter'=>$model,
    //'sortableRows' => true,
    //'sortableAjaxSave'=>true,
    //'sortableAttribute'=>'weight',
    //'sortableAction'=> 'banner/banner/sortable',
    'template' => '{pager}{items}{pager}',
    'columns'=>array(
        //'id',
		array(
			'header' => 'Фото',
			'class'=>'bootstrap.widgets.TbImageColumn',
			'imagePathExpression' => '$data->getPhoto(48,48,"background")',
			'imageOptions' => array('width' => '48px'),
			'htmlOptions' => array('width' => '24px')
            ),
		array(
			'name' => 'title',
			'header' => 'Название',
			'type'=>'raw',
			'value' => 'CHtml::link($data->title, array("/banner/size/update", "id" => $data->id))',
			'htmlOptions' => array('encodeLabel'=>false),
        ),
        'title',
        'width',
        'height',
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',

        ),
    ),
)); ?>
</div>