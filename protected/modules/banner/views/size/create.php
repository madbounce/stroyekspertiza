<?php
$this->breadcrumbs=array(
    $this->module->nameModule=>array('/banner'),
    'Размеры'=>array('admin'),
    'Добавить',
);
?>

<div class="page-header">
    <h3><?php echo Yii::t('config', 'Создание размера'); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>