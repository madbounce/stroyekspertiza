<?php
$this->breadcrumbs=array(
	$this->module->nameModule=>array('/banner'),
	'Размеры'=>array('admin'),
	'Изменить',
);
?>

<div class="page-header">
    <h3><?php echo Yii::t('config', 'Редактирование размера :title', array(':title' => '#' . $model->title)); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>