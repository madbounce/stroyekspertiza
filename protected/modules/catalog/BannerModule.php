<?php

class CatalogModule extends CWebModule
{
	public $nameModule='Баннеры';
	public $showFrontEnd = false;
	public $showBackEnd = false;

	public function init()
	{
		if(($theme=Yii::app()->getTheme())!==null) $this->viewPath=$theme->viewPath.'/modules/'.$this->id;
		$this->setImport(array(
			'catalog.models.*',
			'catalog.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
}
