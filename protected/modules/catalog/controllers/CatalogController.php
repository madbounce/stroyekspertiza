<?php

class CatalogController extends BackEndController
{
	public $layout='//layouts/main_catalog';
	public $defaultAction='admin';

    public function actions(){
        return array(
            'uploadImage' => array(
                'class' => 'application.modules.image.components.UploadAction',
                'entity' => 'Catalog',
                'tag' => 'image',
                'directory' => 'catalog',
                'view' => 'application.modules.image.components.widgets.views.imagesCatalog',
                'multiple' => true,
            ),
            'redactorUploadImage' => array(
                'class' => 'application.extensions.redactorjs.ERedactorUploadAction',
            ),
            'sortable' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'Catalog'
            ),
            'toggle' => 'ext.jtogglecolumn.ToggleAction',
        );
    }

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				//'actions'=>array('admin','delete','create','update','sortable'),
				'users'=>array('@'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	public function actionCreate()
	{
		error_reporting(E_ALL);
		
		$model=new Catalog;

		if(isset($_POST['Catalog']))
		{
			$model->attributes=$_POST['Catalog'];
//			$selectedCategories = !empty($_POST['Catalog']['categoryIds']) ? $_POST['Catalog']['categoryIds'] : array();
//			$model->categories = $selectedCategories;
			$model->photo=CUploadedFile::getInstance($model,'photo');
			$model->promoPhoto=CUploadedFile::getInstance($model,'promoPhoto');

//			$model->create_time = time();
//			$model->update_time = time();

//			$model->start_date = strtotime($model->start_date);
//			$model->end_date = strtotime($model->end_date);

			if($model->save())
			{

				if(!empty($model->photo))
				{
					$imageName = md5($model->id).".".strtolower($model->photo->extensionName);
					$imagePath = Yii::app()->basePath."/../images/Catalogs/";
					$model->photo->saveAs($imagePath.$imageName);
					$model->photo = $imageName;
					$model->save();
				}
				
				if(!empty($model->promoPhoto))
				{
					$imageName = md5($model->id).".".strtolower($model->promoPhoto->extensionName);
					$imagePath = Yii::app()->basePath."/../images/banners/";
					$model->promoPhoto->saveAs($imagePath.$imageName);
					$model->promoPhoto = $imageName;
					$model->save();
				}

				$this->redirect(array('admin'));
			}

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

    public function actionAjaxCatList()
    {
        if(!Yii::app()->request->isAjaxRequest) die();

        $model = CatalogCategory::model()->findByPk(intval($_GET['id']));
        $emptyField = array('' => 'не выбрано');
        if(!$model->parent_id)
            $type = $model->path;
        else
            $type = $model->parentsId->path;

        $data['type'] = $type;
        $data['life_stage'] = CHtml::dropDownList('life_stage','', Catalog::getLifeStageList($type), array('class' => 'life-stage'));
        $data['food_type'] = CHtml::dropDownList('food_type','', $emptyField+Catalog::getFoodTypeList($type), array('class' => 'food_type-stage'));
        $data['kitten'] = CHtml::dropDownList('kitten','', $emptyField+Catalog::getSpecialList($type), array('class' => 'kitten'));
        echo CJSON::encode($data);
    }

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		$currentImage=$model->photo;
		$currentImagePromo=$model->promoPhoto;
		if(isset($_POST['Catalog']))
		{
			$model->attributes=$_POST['Catalog'];

			$uploadedImage=CUploadedFile::getInstance($model,'photo');
			$uploadedImagePromo=CUploadedFile::getInstance($model,'promoPhoto');

			if(empty($uploadedImage))
				$model->photo = $currentImage;
			else
			{
				$model->photo = $uploadedImage;
			}
			
			if(empty($uploadedImagePromo))
				$model->promoPhoto = $currentImagePromo;
			else
			{
				$model->promoPhoto = $uploadedImagePromo;
			}


			if($model->save())
			{
				if(!empty($uploadedImage))
				{
					$imageName = md5($model->id).".".strtolower($model->photo->extensionName);
					$imagePath = Yii::app()->basePath."/../images/Catalogs/";
					@unlink($imagePath.$currentImage);
					$model->photo->saveAs($imagePath.$imageName);
					$model->photo = $imageName;
					$model->save();
				}
				
				if(!empty($uploadedImagePromo))
				{
					$imageName = md5($model->id).".".strtolower($model->promoPhoto->extensionName);
					$imagePath = Yii::app()->basePath."/../images/Catalogs/";
					@unlink($imagePath.$currentImagePromo);
					$model->promoPhoto->saveAs($imagePath.$imageName);
					$model->promoPhoto = $imageName;
					$model->save();
				}
				
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$model = $this->loadModel($id);
			$imagePath = Yii::app()->basePath."/../images/Catalogs/";
			@unlink($imagePath.$model->photo);
			@unlink($imagePath.$model->promoPhoto);
			$model->delete();

			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

    public function actionToggle($id,$attribute) {
        $model = Catalog::model()->findByPk($id);
        if($model){
            if($model->is_new == 1)
                $model->is_new = 0;
            else
                $model->is_new = 1;
        }
        $model->save(false);
    }

	public function actionAdmin()
	{
		$model=new Catalog('search');
		$model->unsetAttributes();
		if(isset($_GET['Catalog']))
			$model->attributes=$_GET['Catalog'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=Catalog::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Catalog-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionUpdateSph(){
        $cats = Catalog::model()->findAll();
        foreach($cats as $cat){
            Search::updateIndex(get_class($cat), $cat->id);
        }
        echo 'ok';
    }
}
