<?php

class DefaultController extends FrontEndController
{

	public $layout = '//layouts/main';
	
	public function actionCatalog(){
		$model = CatalogCategory::model()->findAll();
//		die($this->route);
		$this->render('catalog', array('model'=> $model));
	}
	
	public function actionIndex($type, $subtype = '')
	{
        /**@var $model Catalog
         * @var $model CActiveDataProvider
         */
        $category = CatalogCategory::model()->find("path = '$type'");
		if(!$category) throw new CHttpException('404');
		
        if(!$subtype)
        {
			$ids = array();
			$ids[] = $category->id;
			foreach($category->parents as $subcat) {
				if(isset($subcat->id) && $subcat->id)
				    $ids[] = $subcat->id;
			}
            $model = Catalog::model()->with('category')->InCategoryIds($ids)->frontSearch();

		} else {
            $category = CatalogCategory::model()->pId($category->id)->findByAttributes(array('path'=>$subtype));
            if(!$category) throw new CHttpException('404');

            $model = Catalog::model()->with('category')->CategoryId($category->id)->frontSearch();
            $_GET['filter0'] = $category->path;

		}


        if(Yii::app()->request->isAjaxRequest) {
            echo $this->renderPartial('_product_list', array('model' => $model, 'type' => $type));
            die();
        }

		$this->render($type, array(
            'model'=> $model,
            'type' => $type,
            'subtype' => $subtype
        ));
	}
	
	public function actionView($url)
	{
		$model = Catalog::model()->findByAttributes(array('url'=>$url));
		$this->pageTitle = ($model->metaTitle) ? $model->metaTitle : $model->name;
		Yii::app()->clientScript->registerMetaTag($model->metaDescription, 'description');
		Yii::app()->clientScript->registerMetaTag($model->metaKeywords, 'keywords');
		
		$this->render('product', array('model'=>$model));
	}

	public function actionRedirect($id)
	{
		$model = Banner::model()->findByPk($id);
		if(!empty ($model))
		{
			$model->go_count++;
			$model->update();

			$this->redirect($model->link);
		}
	}

    public function actionRating($id)
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $id = intval($id);
            //$user = User::model()->findByAttributes(array('email'=>$_POST['email']));
            $user = User::model()->find('t.email="'.$_POST['email'].'"');

            if(!$user){
                $res['error'] = 'user_id';
                $res['status'] = 'fail';
                echo CJSON::encode($res);
                Yii::app()->end();
            }
            if(!trim($_POST['text'])){
                $res['error'] = 'text';
                $res['status'] = 'fail';
                echo CJSON::encode($res);
                Yii::app()->end();
            }
            if(!intval(@$_POST['value_1']) || !intval(@$_POST['value_2']) || !intval(@$_POST['value_3'])){
                $res['error'] = 'rating';
                $res['status'] = 'fail';
                echo CJSON::encode($res);
                Yii::app()->end();
            }
            $model = Catalog::model()->findByPk($id);
            if($model){
                $rating = Rating::model()->add($model, $_POST, $user->id);
                if($rating){
                     //$model->addRating($rating);
                    $rating->sendMailConfirm($user);
                    $res['url'] = $this->createUrl('/rating/default/info',array('view'=>'success','id'=>$rating->id));
                    $res['status'] = 'success';
                }else{
                    $res['error'] = 'voice';
                    $res['status'] = 'fail';
                }
            }else{
                $res['error'] = 'catalog';
                $res['status'] = 'fail';
            }

//            $res['status'] = 'info';
            //$res['text'] = $this->renderPartial('_product_rating', array('model' => $model),true);
            //$res['status'] = 'success';
            echo CJSON::encode($res);
            Yii::app()->end();
        }
    }

}