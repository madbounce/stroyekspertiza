<?php

class Catalog extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{catalog}}';
	}

    public function behaviors()
    {
        return array(
            'image' => array(
                'class' => 'application.modules.image.components.ImageBehavior',
                'tag' => 'image',
                'multiple' => true,
            ),
            'SortBehavior' => array(
                'class' => 'ext.behaviors.SortBehavior',
                'sortAttribute' => 'sort'
            ),
        );
    }

    protected function afterSave()
    {
        parent::afterSave();

        Search::updateIndex(get_class($this), $this->id);
    }

	public function rules()
	{
        return array(
            array('name, url, categoryId', 'required'),
            array('description, rating_avg, composition, ingredients, feedingGuidelinesText, additives, bag_size, recommendedFor', 'safe'),
            array('categoryId, sort, is_active, food_type, life_stage, kitten, size, recommended, is_new', 'numerical', 'integerOnly'=>true),
            array('name, metaTitle, metaKeywords, metaDescription', 'length', 'max'=>255),
        );

		/*return array(
			array('name, url', 'required'),
			array('categoryId, variantCode, sizeId, sort, is_active', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),

			array('id, name, categoryId, variantCode, veightId, UPCCode, relationsHp, species, description, altText, feedingGuidelinesText, ingredients, productOverview, recommendedFor, url, metaTitle, photo,shortdesc,'.
				'metaKeywords, metaDescription, promoCTAText, puppy, adult, matureSenior, kitten, dry, size, weightControl, hairballCare, multiCat', 'safe'),
			
			array('id, name, categoryId, variantCode, veightId, UPCCode, relationsHp, species, description, altText, feedingGuidelinesText, ingredients, productOverview, recommendedFor, url, metaTitle'.
				'metaKeywords, metaDescription, promoCTAText, puppy, adult, matureSenior, kitten, dry, size, weightControl, hairballCare, multiCat', 'safe', 'on'=>'search'),
		);*/
	}

    public static function getDogFoodTypeList()
    {
        return array(
//            '' => Yii::t('catalog', 'All Eukanuba Dog Food'),
//            1 => Yii::t('catalog', 'Lorem Ipsum Dolor Imit'),
//            2 => Yii::t('catalog', 'Lorem Ipsum Dolor Imit'),
//            3 => Yii::t('catalog', 'Lorem Ipsum Dolor Imit'),
        );
    }

    public function getDogFoodType(){
        $arr = self::getDogFoodTypeList();
        return $arr[$this->food_type];
    }

    public static function getLifeStageList($type)
    {
        if($type == 'dog'){
            return array(
                1 => Yii::t('catalog', 'Щенок'),
                2 => Yii::t('catalog', 'Взрослая собака'),
                3 => Yii::t('catalog', 'Зрелая и пожилая собака'),
            );
        }
        if($type == 'cat'){
            return array(
                1 => Yii::t('catalog', 'Котенок'),
                2 => Yii::t('catalog', 'Взрослая кошка'),
                3 => Yii::t('catalog', 'Зрелая и пожилая кошка'),
            );
        }
        return array();
    }

    public function getLifeStage($type){
        $arr = self::getLifeStageList($type);
        return $arr[$this->life_stage];
    }

    public static function getFoodTypeList($type='dog')
    {
        if($type == 'dog'){
            return array(
                1 => Yii::t('catalog', 'Сухой'),
                2 => Yii::t('catalog', 'Влажный'),
                3 => Yii::t('catalog', 'Бисквиты'),
            );
        }
        if($type == 'cat'){
            return array(
                1 => Yii::t('catalog', 'Сухой'),
//                2 => Yii::t('catalog', 'Влажный'),
            );
        }
        return array();
    }

    public function getFoodType($type){
        $arr = self::getFoodTypeList($type);
        return $arr[$this->food_type];
    }

    public static function getSizeList()
    {
        return array(
            1 => Yii::t('catalog', 'Мелкий'),
            2 => Yii::t('catalog', 'Средний'),
            3 => Yii::t('catalog', 'Крупный'),
        );
    }

    public function getSize(){
        $arr = self::getSizeList();
        return $arr[$this->size];
    }

    public static function getSpecialList($type = 'dog')
    {
        if($type == 'dog'){
            return array(
                1 => Yii::t('catalog', 'Контроль веса'),
                2 => Yii::t('catalog', 'Чувствительное пищеварение'),
                3 => Yii::t('catalog', 'Чувствительные суставы'),
                4 => Yii::t('catalog', 'Чувствительная кожа'),
            );
        }
        if($type == 'cat'){
            return array(
                1 => Yii::t('catalog', 'Контроль веса'),
                2 => Yii::t('catalog', 'Выведение комков шерсти из желудка'),
            );
        }
        return array();

    }

    public function getSpecial($type){
        $arr = self::getSpecialList($type);
        return $arr[$this->special];
    }

	public function relations()
	{
		return array(
			'category' => array(self::BELONGS_TO, 'CatalogCategory', 'categoryId'),
            'weight' => array(self::BELONGS_TO, 'CatalogWeight', 'weightId'),
            'size' => array(self::BELONGS_TO, 'CatalogSize', 'sizeId'),
			'rating' => array(self::HAS_MANY, 'Rating', 'model_id',
                'on' => 'rating.model_name = "'.get_class($this).'"',
                'together'=>true,
                'condition'=>'status='.Rating::STATUS_ACTIVE.' AND status='.Rating::STATUS_CONFIRM_ACTIVE,
            ),

		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'categoryId' => 'Категория',
            'sort' => 'Сортировка',
            'is_active' => 'Показывать',
            'recommendedFor' => 'Рекомендуется для',
            'url' => 'Ссылка',
            'metaTitle' => 'Заголовок',
            'metaKeywords' => 'Ключевые слова',
            'metaDescription' => 'Описание',
            'rating_avg' => 'Средний рейтинг',
            'description' => 'Обзор продукта',
            'composition' => 'Состав',
            'ingredients' => 'Ингредиенты',
            'feedingGuidelinesText' => 'Руководство к кормлению',
            'additives' => 'Добавки',
            'bag_size' => 'В упаковке',
            'life_stage' => 'Возраст',
            'food_type' => 'Тип корма',
			'recommended' => 'Рекомендуем',
			'is_new' => 'Новый',

//			'variantCode' => 'Код переменной',
//            'sizeId' => 'Вес',
//			'weightId' => 'Вес',
//			'UPCCode' => 'UPCCode',
//			'relationsHp' => 'Подчинение',
//			'species' => 'Назначение',
//			'altText' => 'Текст для главного фото',
//            'productOverview' => 'Обзор',
//			'promoProductPackshotAltText' => 'Текст для промо фото',
//			'promoHeading' => 'promoHeading',
//			'promoDescription' => 'promoDescription',
//			'promoCTAText' => 'promoCTAText',
//			'puppy' => 'puppy',
//			'adult' => 'adult',
//			'matureSenior' => 'matureSenior',
			'kitten' => 'Special',
//			'dry' => 'dry',
//			'wet' => 'wet',
			'size' => 'Size',
//			'weightControl' => 'weightControl',
//			'hairballCare' => 'hairballCare',
//			'multiCat' => 'multiCat',

		);
	}

	public function search()
	{
		$criteria=$this->getDbCriteria();

        if(isset($_GET['cId'])){

            $cid = (int)$_GET['cId'];
            $criteria->addCondition('t.categoryId =' . $cid);
        }

        if(isset($_GET['search'])){
            $criteria->addSearchCondition('name', $_GET['search']);
        }

        $criteria->order = 't.sort ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array('pageSize' => Yii::app()->params->pageSize)
		));
	}

    /**
     * Фильтр на сайте
     * 
     * @return CActiveDataProvider    
     */
    public function frontSearch()
    {
        $criteria = $this->getDbCriteria();

        if(isset($_GET['filter0']) && $_GET['filter0']) //FoodType
        {
            $cid = trim($_GET['filter0']);
            $criteria->addCondition('category.path ="'. $cid.'"');
        }

        if(isset($_GET['filter1']) && $_GET['filter1']) //FoodType
        {
            $foodType = array_map('intval',$_GET['filter1']);
            $criteria->addInCondition('t.food_type' , $foodType);
        }

        if(isset($_GET['filter2']) && $_GET['filter2']) //LifeStages
        {
            $lifeStage = array_map('intval',$_GET['filter2']);
            $criteria->addInCondition('t.life_stage' , $lifeStage);
        }

        if(isset($_GET['filter3']) && $_GET['filter3']) //Size
        {
            $size = array_map('intval',$_GET['filter3']);
            $criteria->addInCondition('t.size' , $size);
        }

        if(isset($_GET['filter4']) && $_GET['filter4']) //Special
        {
            $special = array_map('intval',$_GET['filter4']);
            $criteria->addInCondition('t.kitten' , $special);
        }

//        $criteria->order = 'category.sort ASC';

        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array('pageSize' => 16),
            'sort' => array(
                'defaultOrder' => 'category.sort ASC, t.sort',
            ),
		));
    }

	public function getConcatenedSizeTitle()
	{
		return $this->title."(".$this->width."x".$this->height.")";
	}


    public function InCategoryIds($ids) {
        $criteria = new CDbCriteria;
        $criteria->addInCondition('t.categoryId',$ids);

        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
    
    public function CategoryId($id) {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=>'t.categoryId="'.$id.'"',
        ));
        return $this;
    }

	public function withCategory($type) {
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'category.path="'.$type.'"',
//					'order'=>'id DESC',
		));
		return $this;
	}

    public function getPhoto($width = 100, $height = 100, $options = null){
        $image = Image::model()->entity('Catalog', $this->id)->tag('image')->find();
        if($image){
            $thumbPath = Yii::app()->baseUrl . ImageHelper::thumb($width, $height, $image->url, $options);
            return $thumbPath;
//            return $image->url;
        } else
            return null;
    }


    public function getPhotoNew($width = 100, $height = 100, $options = null){
        $image = Image::model()->entity('Catalog', $this->id)->tag('image')->find();
        if($image){
//            $thumbPath = Yii::app()->baseUrl . ImageHelper::thumb($width, $height, $image->url, $options);
//            return $thumbPath;
            return $image->url;
        } else
            return null;
    }

    public function getUrl()
    {
        return Yii::app()->controller->createUrl('/catalog/default/view',array('url'=>$this->url));
    }

    public function addRating($rating)
    {
        if($rating instanceof Rating)
        {
            $this->rating_avg = round( (($this->rating_avg*$this->voices) + $rating->value_avg) / ($this->voices+1) );
            $this->rating_value_1 = round( (($this->rating_value_1*$this->voices) + $rating->value_1) / ($this->voices+1) );
            $this->rating_value_2 = round( (($this->rating_value_2*$this->voices) + $rating->value_2) / ($this->voices+1) );
            $this->rating_value_3 = round( (($this->rating_value_3*$this->voices) + $rating->value_3) / ($this->voices+1) );
            $this->voices++;
            return $this->save(false);
        }
    }

    public function deleteRating($rating)
    {
        if($rating instanceof Rating)
        {
            if(!$this->voices)
                return true;
            $voices = ($this->voices < 2) ? $this->voices : $this->voices-1;
            $this->rating_avg = round( (($this->rating_avg*$this->voices) - $rating->value_avg) / ($voices) );
            $this->rating_value_1 = round( (($this->rating_value_1*$this->voices) - $rating->value_1) / ($voices) );
            $this->rating_value_2 = round( (($this->rating_value_2*$this->voices) - $rating->value_2) / ($voices) );
            $this->rating_value_3 = round( (($this->rating_value_3*$this->voices) - $rating->value_3) / ($voices) );
            $this->voices--;
            return $this->save(false);
        }
    }

    public function sortsize($size)
    {
        $arr = explode(";",$size);
        $arr = array_map('trim',$arr);

        return $arr;
    }
}