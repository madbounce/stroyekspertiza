<?php

class CatalogCategory extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{catalog_category}}';
	}

    public function behaviors()
    {
        return array(
            'SortBehavior' => array(
                'class' => 'ext.behaviors.SortBehavior',
                'sortAttribute' => 'sort'
            ),
        );
    }

	public function rules()
	{
		return array(
			array('title', 'required'),
			array('composition', 'safe'),
            array('sort', 'numerical', 'integerOnly'=>true),
			array('id, title, path, composition', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'banners' => array(self::MANY_MANY, 'Banner', '{{banners_categories}}(category_id, banner_id)'),
			'parents' => array(self::HAS_MANY, 'CatalogCategory', 'parent_id'),
			'parentsId' => array(self::HAS_ONE, 'CatalogCategory', array('id'=>'parent_id')),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Категория',
			'title' => 'Название',
			'page' => 'Страница',
			'composition' => 'Состав',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		//$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		//$criteria->compare('page',$this->page,true);
        $criteria->order = 't.sort ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array('pageSize' => Yii::app()->params->pageSize)
		));
	}

    public function pId($id = 0)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=>'t.parent_id="'.$id.'"',
        ));
        return $this;
    }

    public function path($path = '')
    {
        if(!$path) return $this;
        $pid = $this->findByAttributes(array('path'=>$path));
        if(!$pid) return $this;


        return $this->pId($pid->id);
    }

    public function toArray($key='id',$value = 'title'){

        $criteria = new CDbCriteria();
        $criteria->order = 't.sort ASC';
        $this->getDbCriteria()->mergeWith($criteria);
        $model = $this->findAll($criteria);
        
        $items = array();
        foreach($model as $row)
            $items[$row->{$key}] = $row->{$value};

        return $items;
    }

    public static function all(){
        $criteria = new CDbCriteria();
        $criteria->order = 't.sort ASC';
        return self::model()->findAll($criteria);
    }

    public static function listAll($id='',$pid='')
    {
        $criteria = new CDbCriteria();
        if(!empty($id)) $criteria->addCondition('id <>'.$id);
        if(!empty($pid)) $criteria->addCondition('parent_id <>'.$pid);
        $criteria->order = 't.sort ASC';
        $res = self::model()->findAll($criteria);
        return CHtml::listData($res, 'id', 'title');
    }
}