<?php

class CatalogWeight extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{catalog_weight}}';
	}

	public function rules()
	{
		return array(
			array('title, weight, weightMaintenance, weightReduction', 'required'),
			array('title', 'length', 'max'=>255),

			array('id, title, weight, weightMaintenance, weightReduction', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'catalog' => array(self::HAS_MANY, 'Catalog', 'size_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'width' => 'Ширина',
			'height' => 'Высота',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('width',$this->width);
		$criteria->compare('height',$this->height);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getConcatenedSizeTitle()
	{
		return $this->title."(".$this->width."x".$this->height.")";
	}
}