<?php
$this->widget('application.extensions.redactorjs.ERedactorWidget', array(
    'model' => $model,
    'attribute' => 'bag_size',
    'options' => array(
        'lang' => 'ru',
        'minHeight' => '200',
        'buttons' => "js:['html', 'bold', 'italic', 'deleted']"
    ),
));
?>