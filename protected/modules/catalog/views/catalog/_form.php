<?php
if ($model->isNewRecord && isset($_GET['cId']) && $_GET['cId'])
    $model->categoryId = (int) $_GET['cId'];
?>
<?php
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'catalog-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
        'class' => 'product'
    ),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textField($model, 'name', array('class' => 'name', 'maxlength' => 255)); ?>
    <div class="checkbox">
        <?php echo $form->checkBox($model, 'is_active')?>
        <?php echo $form->labelEx($model, 'is_active')?>
    </div>
</div>

<div id="product_categories">
    <?php echo $form->dropDownList($model, 'categoryId', CHtml::listData(CatalogCategory::model()->findAll(), 'id','title'),
    array(
        'encode' => false,
        'empty' => Yii::t('app', 'Выберите категорию')
    )); ?>

    <?php //echo $form->labelEx($model, 'recommendedFor', array('class' => 'property')); ?>
    <?php echo $form->textField($model, 'recommendedFor', array('class' => 'span7 simpla_inp', 'placeholder' => 'Рекомендуется для')); ?>
</div>

<!-- Левая колонка свойств товара -->
<div id="column_left">

    <!-- Параметры страницы -->
    <div class="block layer param" style="min-height: 340px;">
        <h2>Параметры страницы</h2>
        <ul>
            <li>
                <?php echo $form->labelEx($model, 'url', array('class' => 'property')); ?>
                <div class="page_url">/catalog/</div>
                <?php echo $form->textField($model, 'url', array('class' => 'page_url', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'metaTitle', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'metaTitle', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'metaKeywords', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'metaKeywords', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'metaDescription', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'metaDescription', array('class' => 'simpla_inp')); ?>
            </li>
        </ul>
    </div><!-- Параметры страницы (The End)-->

</div><!-- Левая колонка свойств товара (The End)-->

<!-- Правая колонка свойств товара -->
<div id="column_right">

    <!-- Изображение категории -->
    <div class="block layer" style="min-height: 340px;">
        <h2>Изображение товара</h2>

        <?php $this->widget('application.modules.image.components.widgets.UploadWidget', array(
            'model' => $model,
            'tag' => 'image',
            'action' => array('/catalog/catalog/uploadImage'),
            'multiple' => true,
            'view' => 'imagesCatalog',
            'uploadButtonText' => Yii::t('news', 'Загрузить изображение'),
        )); ?>
        <div style="clear: both"></div>

        <?php

        $type = '';
        if(!$model->isNewRecord && isset($model->category)){
            if(!$model->category->parent_id)
                $type = $model->category->path;
            else
                $type = $model->category->parentsId->path;

        }
        $emptyField = array('' => 'не выбрано');
        ?>
        <script>
            var hide = '<?php echo $type ?>';
            $(document).ready(function(){
                if(!hide)
                    $('ul.product-params').hide();

                $('select[name="Catalog[categoryId]"]').change(function(){
                    var cid = $(this).val();
                    var life_stage = $('span.life_stage');
                    var food_type = $('span.food_type');
                    var kitten = $('span.kitten');
                    $('ul.product-params').css('opacity','0.1');
                    $.ajax({
                        url  : '/backend/catalog/catalog/AjaxCatList',
                        data : {'id':cid},
                        type : 'GET'
                    }).success(function ( data ) {
                        eval('var data='+data);
                        life_stage.html(data.life_stage);
                        food_type.html(data.food_type);
                        kitten.html(data.kitten);
                        $('ul.product-params').show();
                        $('ul.product-params').css('opacity','1');
                    });
                });
            });
        </script>
        <ul class="product-params">
            <li >
                <?php echo $form->labelEx($model, 'life_stage');?>
                <?php $life_stage = Catalog::getLifeStageList($type); ?>
                <span class="life_stage">
                    <?php echo $form->dropDownList($model, 'life_stage', $life_stage, array('class' => 'life-stage'));?>
                </span>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'food_type')?>
                <?php $food_type = $emptyField+Catalog::getFoodTypeList($type); ?>
                <span class="food_type">
                    <?php echo $form->dropDownList($model, 'food_type', $food_type, array('class' => 'food-type')); ?>
                </span>
            </li>

            <li>
                <?php echo $form->labelEx($model, 'size')?>
                <?php $size = $emptyField+Catalog::getSizeList(); ?>
                <?php echo $form->dropDownList($model, 'size',$size,array('class' => 'size')); ?>
            </li>

            <li>
                <?php echo $form->labelEx($model, 'kitten')?>
                <?php $kitten = $emptyField+Catalog::getSpecialList($type); ?>
                <span class="kitten">
                    <?php echo $form->dropDownList($model, 'kitten', $kitten, array('class' => 'special')); ?>
                </span>
            </li>

            <li>
            	<div class="checkbox">
					<?php echo $form->checkBox($model, 'recommended')?>
                    <?php echo $form->labelEx($model, 'recommended')?>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- Правая колонка свойств товара (The End)-->

<div style="clear: both"></div>
<div class="form-tabs">
<?php
$this->widget('bootstrap.widgets.TbTabs', array(
    'type' => 'tabs',
    'tabs' => array(
        array('label' => 'Обзор', 'content' => $this->renderPartial('_description_tab', array('form' => $form, 'model' => $model), true), 'active' => true),
        array('label' => 'Состав', 'content' => $this->renderPartial('_composition_tab', array('form' => $form, 'model' => $model), true)),
        array('label' => 'Анализ', 'content' => $this->renderPartial('_ingredients_tab', array('form' => $form, 'model' => $model), true)),
        array('label' => 'Руководство к кормлению', 'content' => $this->renderPartial('_guidelines_tab', array('form' => $form, 'model' => $model), true)),
        array('label' => 'Добавки', 'content' => $this->renderPartial('_additives_tab', array('form' => $form, 'model' => $model), true)),
        array('label' => 'В упаковке', 'content' => $this->renderPartial('_bag_size_tab', array('form' => $form, 'model' => $model), true)),
    ),
));
?>
    <?php echo $form->error($model, 'description')?>
    <?php echo $form->error($model, 'composition')?>
    <?php echo $form->error($model, 'ingredients')?>
    <?php echo $form->error($model, 'feedingGuidelinesText')?>
    <?php echo $form->error($model, 'additives')?>
</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>