<?php
$this->widget('application.extensions.redactorjs.ERedactorWidget', array(
    'model' => $model,
    'attribute' => 'feedingGuidelinesText',
    'options' => array(
        'lang' => 'ru',
        'minHeight' => '200',
        'buttons' => "js:['html', 'bold', 'italic', 'deleted']"
    ),
));
?>