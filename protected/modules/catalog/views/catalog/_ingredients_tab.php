<?php
$this->widget('application.extensions.redactorjs.ERedactorWidget', array(
    'model' => $model,
    'attribute' => 'ingredients',
    'options' => array(
        'lang' => 'ru',
        'minHeight' => '200',
        'buttons' => "js:['html', 'bold', 'italic', 'deleted']"
    ),
));
?>