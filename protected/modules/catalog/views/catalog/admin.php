<?php
$dp = $model->search();
$countAll = $dp->totalItemCount;
?>

<div class="page-content">
    <div class="row two-columns">
        <div class="span9">

            <div class="page-header admin-header inner-header">
                <h3><?php echo Yii::t('app', '{n} товар|{n} товара|{n} товаров', $countAll);?></h3>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'label' => Yii::t('catalog', 'Добавить товар'),
                'size' => 'small',
                'url' => (isset($_GET['cId']) && ($_GET['cId']))? array('/catalog/catalog/create', 'cId' => $_GET['cId']) : array('/catalog/catalog/create'),
            ));
                ?>
            </div>

            <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
                'id'=>'catalog-grid',
                'type' => 'striped bordered',
                'dataProvider'=>$dp,
                //'filter' => $model,
                'enableSorting' => false,
                'sortableRows' => true,
                'sortableAjaxSave'=>true,
                'sortableAttribute'=>'sort',
                'sortableAction'=> 'catalog/catalog/sortable',
                'template' => '{pager}{items}{pager}',
                'columns'=>array(
                    array(
                        'header' => 'Фото',
                        'class'=>'bootstrap.widgets.TbImageColumn',
                        'imagePathExpression' => '$data->getPhoto(48,0, array("method" => "resize"))',
                        'imageOptions' => array('width' => '48px'),
                        'htmlOptions' => array('width' => '48px')
                    ),
                    array(
                        'name' => 'name',
                        'type'=>'raw',
                        'value' => 'CHtml::link($data->name, array("/catalog/catalog/update", "id" => $data->id))',
                        'htmlOptions' => array('encodeLabel'=>false),
                    ),
                    array(
                        'class' => 'bootstrap.widgets.TbToggleColumn',
                        'name' => 'is_new',
                        'checkedButtonLabel' => Yii::t('admin', 'Снять отметку'),
                        'uncheckedButtonLabel' => Yii::t('admin', 'Отметить новый товар'),
                        'toggleAction'=>'toggle',
                        'htmlOptions' => array(
                            'width' => '50',
                            'style' => 'text-align:center'
                        ),
                    ),
                    array(
                        'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template'=>'{view}{delete}',
                        'viewButtonUrl' => '"/catalog/default/view?url=" . $data->url',
                        'viewButtonOptions' => array('target' => '_blank')

                    ),
                ),
            )); ?>
        </div>
        <div class="span3 sidebar">
            <div class="b-side">
                <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route), 'get', array('class' => 'search-form')); ?>

                <div class="input-append">
                    <?php echo CHtml::textField('search', isset($_GET['search']) ? $_GET['search'] : '', array('class' => 'input-medium search-input'))?>
                    <button class="btn" type="submit"><i class="icon-search"></i></button>
                </div>
                <?php echo CHtml::endForm(); ?>
            </div>

            <div class="b-side">
            <?php
            $categories = array(
                array('label'=>Yii::t('catalog','Все категории'), 'url'=>array('/catalog/catalog/admin'), 'active' => (!isset($_GET['cId']))),
            );
            $cats = CatalogCategory::all();
            if($cats){
                foreach($cats as $cat){
                    $rowCssClass = ($cat->parent_id) ? "child-r" : "parent-r";
                    array_push($categories, array(
                            'label' => $cat->title,
                            'url' => array('/catalog/catalog/admin', 'cId' => $cat->id),
                            'active' => (isset($_GET['cId']) && ($_GET['cId'] == $cat->id)),
                            'itemOptions' => array('class'=>$rowCssClass),
                         )
                    );
                }
            }

            $this->widget('bootstrap.widgets.TbMenu', array(
                'type'=>'list',
                'items' => $categories
            ));
            ?>
            </div>

        </div>
    </div>
</div>