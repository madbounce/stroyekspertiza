<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'catalog-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'product'
    ),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textField($model, 'name', array('class' => 'name', 'maxlength' => 255)); ?>
    <div class="checkbox">
        <?php echo $form->checkBox($model, 'is_active')?>
        <?php echo $form->labelEx($model, 'is_active')?>
    </div>
</div>

<div id="product_categories">
    <?php echo $form->dropDownList($model, 'categoryId', CHtml::listData(CatalogCategory::model()->findAll(), 'id','title'),
    array(
        'encode' => false,
        'empty' => Yii::t('app', 'Выберите категорию')
    )); ?>
</div>

<!-- Левая колонка свойств товара -->
<div id="column_left">

    <!-- Параметры страницы -->
    <div class="block layer param">
        <h2>Параметры страницы</h2>
        <ul>
            <li>
                <?php echo $form->labelEx($model, 'url', array('class' => 'property')); ?>
                <div class="page_url">/catalog/</div>
                <?php echo $form->textField($model, 'url', array('class' => 'page_url', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'metaTitle', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'metaTitle', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'metaKeywords', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'metaKeywords', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'metaDescription', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'metaDescription', array('class' => 'simpla_inp')); ?>
            </li>
        </ul>
    </div><!-- Параметры страницы (The End)-->

    <!-- Параметры товара -->
    <div class="block layer">
        <h2>Параметры товара</h2>
        <ul>
            <li>
                <?php echo $form->labelEx($model, 'variantCode', array('class' => 'property')); ?>
                <?php echo $form->dropDownList($model, 'variantCode', array(0,1,2,3,4,5,6,), array('encode' => false, 'class' => 'simpla_sel')); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'sizeId', array('class' => 'property')); ?>
                <?php echo $form->dropDownList($model, 'sizeId', CHtml::listData(CatalogSize::model()->findAll(), 'id','title'), array('encode' => false, 'class' => 'simpla_sel')); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'UPCCode', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'UPCCode', array('class' => 'simpla_inp', 'maxlength' => 100)); ?>
            </li>
        </ul>

        <ul>
            <li>
                <?php echo $form->labelEx($model, 'description', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'description', array('class' => 'simpla_inp')); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'recommendedFor', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'recommendedFor', array('class' => 'simpla_inp')); ?>
            </li>
        </ul>
    </div><!-- Параметры товара (The End)-->
</div><!-- Левая колонка свойств товара (The End)-->

<!-- Правая колонка свойств товара -->
<div id="column_right">

    <!-- Изображение категории -->
    <div class="block layer images">
        <h2>Изображение товара</h2>

        <?php $this->widget('application.modules.image.components.widgets.UploadWidget', array(
        'model' => $model,
        'tag' => 'image',
        'action' => array('/catalog/catalog/uploadImage'),
        'multiple' => true,
        'view' => 'imagesCatalog',
        'uploadButtonText' => Yii::t('news', 'Загрузить изображение'),
    )); ?>

    </div>
    <div class="block layer descr">
        <h2>Описание</h2>
        <ul>
            <li>
                <?php echo $form->labelEx($model, 'feedingGuidelinesText', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'feedingGuidelinesText', array('class' => 'simpla_inp')); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'ingredients', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'ingredients', array('class' => 'simpla_inp')); ?>
            </li>
        </ul>
    </div>
</div>
<!-- Правая колонка свойств товара (The End)-->

<div style="clear: both"></div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>





<?php //echo $form->dropDownListRow($model, 'weightId', CHtml::listData(CatalogWeight::model()->findAll(), 'id','weight'), array('encode' => false, 'class' => 'span5')); ?>
<?php //echo $form->textFieldRow($model, 'relationsHp', array('class' => 'span5', 'maxlength' => 100)); ?>
<?php //echo $form->textFieldRow($model, 'species', array('class' => 'span5', 'maxlength' => 100)); ?>

<? //echo $form->labelEx($model,'shortdesc');?>
<?php
/*
$this->widget('application.extensions.redactorjs.ERedactorWidget', array(
        'model' => $model,
        'attribute' => 'shortdesc',
        'options' => array(
            'lang' => 'ru',
            'imageUpload' => CHtml::normalizeUrl(array('/page/admin/redactorUploadImage'))
        ),
    ));
 */
?>
<?php //echo $form->textFieldRow($model, 'altText', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php //echo $form->labelEx($model,'productOverview');?>
<?php
/*
$this->widget('application.extensions.redactorjs.ERedactorWidget', array(
        'model' => $model,
        'attribute' => 'productOverview',
        'options' => array(
            'lang' => 'ru',
            'imageUpload' => CHtml::normalizeUrl(array('/page/admin/redactorUploadImage'))
        ),
));*/
?>

<?php //echo $form->textFieldRow($model, 'promoProductPackshotAltText', array('class' => 'span5', 'maxlength' => 500)); ?>
<?php //echo $form->textFieldRow($model, 'promoHeading', array('class' => 'span5', 'maxlength' => 255)); ?>
<?php //echo $form->textFieldRow($model, 'promoDescription', array('class' => 'span5', 'maxlength' => 500)); ?>
<?php //echo $form->textFieldRow($model, 'promoCTAText', array('class' => 'span5', 'maxlength' => 500)); ?>
<?php //echo $form->checkBoxRow($model, 'puppy', array()); ?>
<?php //echo $form->checkBoxRow($model, 'adult', array()); ?>
<?php //echo $form->checkBoxRow($model, 'matureSenior', array()); ?>
<?php //echo $form->checkBoxRow($model, 'kitten', array()); ?>
<?php //echo $form->checkBoxRow($model, 'dry', array()); ?>
<?php //echo $form->checkBoxRow($model, 'wet', array()); ?>
<?php //echo $form->checkBoxRow($model, 'size', array()); ?>
<?php //echo $form->checkBoxRow($model, 'weightControl', array()); ?>
<?php //echo $form->checkBoxRow($model, 'hairballCare', array()); ?>
<?php //echo $form->checkBoxRow($model, 'multiCat', array()); ?>


<?php //echo $form->fileFieldRow($model,'photo'); ?>
<?php /*
		$this->widget('application.extensions.fancybox.EFancyBox', array(
			'target'=>'a[rel=banner]',
			'config'=>array(
				'titlePosition'=>'over',
				),
			)
		);*/
?>


<?php //echo $form->fileFieldRow($model,'promoPhoto'); ?>
<?php /*
		$this->widget('application.extensions.fancybox.EFancyBox', array(
			'target'=>'a[rel=banner2]',
			'config'=>array(
				'titlePosition'=>'over',
				),
			)
		);*/
?>
<?php
/*
        $this->widget('pictures.components.PicturesEditor',
                  array('mode' =>'AddAjax',
                      'files' => $model->isNewRecord ? false : $model->getFiles()
        ));*/
?>


<?php /*
		$this->widget('pictures.components.PicturesEditor',
				  array('mode' =>'AddAjax',
					  'files' => $model->isNewRecord ? false : $model->getFiles()
		));*/
?>