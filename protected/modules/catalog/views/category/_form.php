<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'category-form',
    'enableAjaxValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
        'class' => 'product'
    ),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textField($model, 'title', array('class' => 'name', 'maxlength' => 100)); ?>
    <div class="checkbox"></div>
</div>

<div id='category'>
    <?php echo $form->dropDownListRow($model, 'parent_id', array('0' => 'Новый раздел')+CatalogCategory::listAll($model->id), array('class' => 'span5', 'maxlength' => 100)); ?>
</div>

   <?php /*
<div>
    <div class="label">
        <?php echo $form->labelEx($model, 'composition', array('class' => '')); ?>
    </div>
    <?php $this->widget('application.extensions.redactorjs.ERedactorWidget', array(
        'model' => $model,
        'attribute' => 'composition',
        'options' => array(
            'lang' => 'ru',
            'minHeight' => '200',
            'buttons' => "js:['html', 'bold', 'italic', 'deleted']"
        ),
    ));?>
</div>
*/ ?>


<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>