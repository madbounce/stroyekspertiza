<?php
$this->breadcrumbs = array(
    Yii::t('catalog', 'Каталог'),
);
$dp = $model->search();
$countAll = $dp->totalItemCount;
?>

<div class="page-header admin-header">
    <h3><?php echo Yii::t('app', '{n} категория|{n} категории|{n} категорий', $countAll);?></h3>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'label' => Yii::t('translate', 'Добавить категорию'),
    'size' => 'small',
    'url' => array('category/create'),
));
    ?>
</div>

<div class="page-content">
    <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
        'id'=>'catalog-category-grid',
        'type' => 'striped bordered',
        'dataProvider'=>$dp,
        //'filter'=>$model,
        'enableSorting' => false,
        'sortableRows' => true,
        'sortableAjaxSave'=>true,
        'sortableAttribute'=>'sort',
        'sortableAction'=> 'catalog/category/sortable',
        'rowCssClassExpression' =>  '($data->parent_id) ? "child-r" : "parent-r"',
        'template' => '{pager}{items}{pager}',
        'columns'=>array(
            array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'title',
                'type'=>'raw',
                'value' => '$data->title',
                'editable' => array(
                    'url' => Yii::app()->controller->createUrl("/catalog/category/editable"),
                    'placement' => 'right',
                    'inputclass' => 'span4',
                    'title' => Yii::t('admin', 'Введите название пункта'),
                    'success' => 'js: function(data) {
                    if(typeof data == "object" && !data.success) return data.msg;
                }'
                ),

            ),
          /*  array(
                'name' => 'title',
                'type'=>'raw',
                'value' => 'CHtml::link($data->title, array("/catalog/category/update", "id" => $data->id))',
                'htmlOptions' => array('encodeLabel'=>false),
            ),*/
            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'template'=>'{update}{delete}',

            ),
        ),
    )); ?>
</div><!-- end of .page-content -->