<?php if($type) : ?>
<script>

    function SendFilterForm()
    {
        if($(this).attr('name') == 'filter0'){
            var str = str = $('#form_filters').attr('action');
            str += $(this).val();
            history.pushState({}, '', str);
        }

        var block = $('#products_list');
        block.css('opacity','0.1');
        $.ajax({
            url  : $('#form_filters').attr('action'),
            data : $('#form_filters').serialize(),
            type : 'GET'
        }).success(function ( data ) {
            block.html(data);
            block.css('opacity','1');
        });

    }

    $(document).ready(function(){
        $('#form_filters :input').click(SendFilterForm);
    });

</script>
<div class="chooseFootPet">
    <form id="form_filters" action="/products/<?php echo $type;?>/">
        <p>Категории продукции</p>
        <?php 
            $val = isset($_GET['filter0']) ? $_GET['filter0'] : null;
            echo CHtml::radioButtonList('filter0', $val, array_merge(array(''=>'Все категории'), CatalogCategory::model()->path($type)->toArray('path')), array('class' => ''));
        ?>

        <?php echo CHtml::hiddenField('type',$type); ?>
        <?php echo CHtml::hiddenField('Catalog_page',1); ?>
        <?php echo CHtml::hiddenField('Catalog_sort',''); ?>

        <?php if($type != 'cat'): ?>
            <p>Тип продукции</p>
            <?php
            $val = isset($_GET['filter1']) ? $_GET['filter1'] : null;
            echo CHtml::checkBoxList('filter1', $val, Catalog::getFoodTypeList($type), array('class' => ''));
            ?>
        <?php endif; ?>


        <p>Возраст</p>
        <?php
        $val = isset($_GET['filter2']) ? $_GET['filter2'] : null;
        echo CHtml::checkBoxList('filter2', $val, Catalog::getLifeStageList($type), array('class' => ''));
        ?>

        <?php if($type != 'cat'): ?>
            <p>Размер</p>
            <?php
            $val = isset($_GET['filter3']) ? $_GET['filter3'] : null;
            echo CHtml::checkBoxList('filter3', $val, Catalog::getSizeList(), array('class' => ''));
            ?>
        <?php endif; ?>

        <p>Особые потребности</p>
        <?php
        $val = isset($_GET['filter4']) ? $_GET['filter4'] : null;
        echo CHtml::checkBoxList('filter4', $val, Catalog::getSpecialList($type), array('class' => ''));
        ?>
    </form>
</div>

<?php endif;?>
