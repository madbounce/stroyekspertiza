<br clear="all">

<div class="user_rate">
    <strong>Общий  </strong>
    <?php echo $this->widget("CStarRating", CMap::mergeArray($params,array('id'=>'avg_'.$data->id,'name'=>'value_avg_'.$data->id,'value'=>$data->value_avg,'htmlOptions' => array('class'=>''))), true);?>
    <br>

    Заметные результаты
    <?php echo $this->widget("CStarRating", CMap::mergeArray($params,array('id'=>'val1_'.$data->id,'name'=>'value_1_'.$data->id,'value'=>$data->value_1)), true);?>
    <br>
    Пищевая ценность
    <?php echo $this->widget("CStarRating", CMap::mergeArray($params,array('id'=>'val2_'.$data->id,'name'=>'value_2_'.$data->id,'value'=>$data->value_2)), true);?>
    <br>
    Наслаждение питомца
    <?php echo $this->widget("CStarRating", CMap::mergeArray($params,array('id'=>'val3_'.$data->id,'name'=>'value_3_'.$data->id,'value'=>$data->value_3)), true);?>
	
</div>

<br clear="all">

<div class="user_review">
	<div class="user_name">
		<p><strong><?php echo $data->user->first_name;?></strong></p>
<?php /*<p>Собака: большая</p>
		<p>Возраст: зрелая</p> */ ?>
		<span class='date-rev'><?php echo Yii::app()->dateFormatter->format("dd MMMM yyyy", $data->created_date) ?></span>
	</div>
	
	<div>
	<p><?php echo $data->text; ?> </p>
	<!--<small>Was this review helpful?</small>
	<span class="quest"><a href="">Да</a></span>
	<span class="quest"><a href="">Нет</a></span>
	<p><a href="">(Report as Inappropriate)</a></p>-->
	</div>


</div>



<hr class="clear"/>