<div class="leftSide">
        <div class="selectOne"><?php echo Misc::t('Выберите питомца');?></div>

        <?php
            $type = isset($type) ? $type : '';
            $showFilters = isset($showFilters) ? $showFilters : true;
            $imgPref =  (isset($type) && $type == 'cat') ? '' : '2';
            $img = Yii::app()->theme->baseUrl.'/images/mini';
        ?>

        <? foreach(CatalogCategory::model()->pId()->all() as $row): ?>
            <?php //@todo в будущем сделать нормально.. Загрузку картинок для категорий и описаний ?>

            <div class="imgDog <? if(isset($type) && $row->path == $type):?>active<?endif;?>">
                <a href="<?php echo Yii::app()->createUrl('/catalog/default/index', array('type' => $row->path));?>">
                    <img src="<? echo $img.'_'.$row->path.$imgPref.'.png'?>" /><br /><span><?=$row->title;?></span>
                </a>
            </div>

        <? endforeach;?>

        <?php if($showFilters) $this->renderPartial('_filters',array('type'=>$type)); ?>
    </div>