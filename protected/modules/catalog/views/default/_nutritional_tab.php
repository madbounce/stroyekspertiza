<div class="pane">
<?php
$this->widget('IamTabs', array(
    'htmlOptions' => array('class' => 'sub-tab'),
    'ulClass' => 'sub-tabs',
    'cssFile' => false,
    'tabs'=>array(
        'tab_composition'=>array(
            'title'=> '<span>' . Misc::t('Состав') . '</span>',
            'content'=> $model->composition,
            'visible' => !empty($model->composition)
        ),
        'tab_ingredients'=>array(
            'title'=> '<span>' .Misc::t('Анализ') . '</span>',
            'content'=>  $model->ingredients,
            'visible' => !empty($model->ingredients)
        ),
        'tab_feeding'=>array(
            'title'=>'<span>' . Misc::t('Руководство') . '</span>',
            'content'=> $model->feedingGuidelinesText,
            'visible' => !empty($model->feedingGuidelinesText)
        ),
        'tab_aditives'=>array(
            'title'=>'<span>' . Misc::t('Добавки') . '</span>',
            'content'=> $model->additives,
            'visible' => !empty($model->additives)
        ),

    ),
))?>
</div>