<?php
//    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/URI.js', CClientScript::POS_HEAD);
?>

<?php
    $model->getData();
    $first = $model->pagination->offset + 1;
    $last  = $first + $model->pagination->pageSize - 1;
    if($last > $model->pagination->itemCount)
        $last = $model->pagination->itemCount;

    function consPages(CPagination $pagination, $c)
    {
        if($pagination->pageCount == 1)
            return '';
        $buttons = array();
        if($pagination->currentPage > 0)
            $buttons[] = CHtml::link('Пред.', $pagination->createPageUrl($c, $pagination->currentPage - 1),array('rel'=>@$_GET['Catalog_page'] - 1));
        
        /*for($i = 0; $i < $pagination->pageCount; $i++)
        {
            if($i !== $pagination->currentPage)
                $buttons[] = CHtml::link($i + 1, $pagination->createPageUrl($c, $i),
                            array('class' => 'pagination_active'));
            else
                $buttons[] = CHtml::link($i + 1, null, array('class' => 'pagination_inactive'));
        }*/
        
        if($pagination->currentPage < ($pagination->pageCount - 1) )
            $buttons[] = CHtml::link('След.', $pagination->createPageUrl($c, $pagination->currentPage + 1),array('rel'=>@$_GET['Catalog_page'] + 1));
        
        return implode('&nbsp;|&nbsp;', $buttons);
    }
?>
<script>
    $(document).ready(function() {

        $('.form_sort a').click( function (e) {
            var name = $(this).attr('rel');
            $('#form_filters input[name="Catalog_sort"]').val(name);
            SendFilterForm();
            return false;
        });
        $('.pagination a').click( function (e) {
            var name = $(this).attr('rel');
            $('#form_filters input[name="Catalog_page"]').val(name);
            SendFilterForm();
            return false;
        });
    });
</script>


    <?php $sort = isset($_GET['sort']) ? $_GET['sort'] : null; ?>
    <div class="sortProduct">
        <span class="form_sort">Сортировать по:
            <a href="javascript:" rel='name' class="<?php if($sort =='name') echo 'active';?>">Все</a> |
            <a href="javascript:" rel="rating" class="<?php if($sort =='rating') echo 'active';?>">С высоким рейтингом</a> |
            <a href="javascript:" rel="view" class="<?php if($sort =='view') echo 'active';?>">Самые популярные</a>
        </span>

        <?php if($model->pagination->pageCount > 1): ?>
        <span class="pagination">
            | <?= consPages($model->pagination, $this); ?>
        </span>
        <?php endif ?>

        <span> Элементы <?=$first?>-<?=$last?> из <?=$model->totalItemCount ?></span>

        <div class="clear"></div>
    </div>

    <?php $this->renderPartial('application.modules.rating.views.init'); ?>

    <div class="sortedProducts">
        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $model,
            'itemView'=>'listItem',
            'itemsTagName' => 'ul',
            'itemsCssClass' => 'product-list',
//            'sorterHeader'=>'Сортировать по:',
//            'sortableAttributes'=>array('all','title', 'price'),
            'template' => '{items}',
        /*    'pager' => array(
                'firstPageLabel'=>'&larr;',
                'prevPageLabel'=>'<',
                'nextPageLabel'=>'>',
                'lastPageLabel'=>'&rarr;',
                'maxButtonCount'=>'10',
                'header'=>'<span>Cтраницы:</span>',
                //'cssFile'=>false,
                'cssFile'=>Yii::app()->theme->getBaseUrl(true).'/css/pager.css'
            ),*/
        )); ?>
    </div>


<div class="sortProduct">

    <?php if($model->pagination->pageCount > 1): ?>
        <span class="pagination">
                | <?= consPages($model->pagination, $this); ?>
            </span>
    <?php endif ?>

    <span> Элементы <?=$first?>-<?=$last?> из <?=$model->totalItemCount ?></span>
    <div class="clear"></div>
</div>
