<?php
    $url = $this->createUrl('/catalog/default/rating', array('id'=>$model->id));

    $params = array(
        "name" => $model->id,
        "id" => $model->id,
        "readOnly" => false,
        "ratingStepSize" => 1,
        "minRating" => 1,
        "maxRating" => 5,
        "allowEmpty" => false,
        "cssFile" => Yii::app()->theme->baseUrl.'/css/stars.css',
		'htmlOptions' => array('class'=>'lines'),
    );
?>

<?php echo CHtml::form('','post',array('id'=>"rating-form")); ?>
    <?php echo CHtml::hiddenField('value',''); ?>
    
    <div class="voice">
        <div class="error" style="display: none;">Вы уже проголосовали</div>
    </div>

    <div class="email">
	<label >E-mail участника клуба</label>
        <?php echo CHtml::textField('email',''); ?>
        <div class="error" style="display: none;">
            Отзывы могут остовлять только зарегистрированные пользователи.
            <a href="<?php echo $this->createUrl('/site/join');?>" target="_blank">Присоединиться</a>
        </div>
    </div>

    <div class="text">
	<label>Несколько слов о продукте</label>
        <?php echo CHtml::textArea('text',''); ?>
        <div class="error" style="display: none;">Поле не может быть пустым</div>
    </div>

    <div class="user_rate">
	    <label> Оценка продукта</label>
        <?php //$this->renderPartial('application.modules.rating.views._stars',array('id'=>'new-rating','class'=>'reviewProduct','readonly'=>false, 'showTitle'=>false));?>
		Заметные результаты
        <?php echo $this->widget("CStarRating", CMap::mergeArray($params,array('id'=>'new_value1','name'=>'value_1')), true);?>
        <br>Пищевая ценность
        <?php echo $this->widget("CStarRating", CMap::mergeArray($params,array('id'=>'new_value1','name'=>'value_2')), true);?>
        <br>Наслаждение питомца
        <?php echo $this->widget("CStarRating", CMap::mergeArray($params,array('id'=>'new_value1','name'=>'value_3')), true);?>
        <div class="error" style="display: none;">Выставьте рейтинг</div>
    </div>

    <div class="send_review">
    <?php echo CHtml::ajaxButton('Отправить', $url, array(
            'type' => 'POST',
            'dataType'=>'json',
            'success'=>'function(data)
            {
                if(data.status == "success"){
                    $("#write-review").hide();
                    document.location.href = data.url;
                }

                if(data.status == "fail"){
                    $("#rating-form .error").hide();
                    if(data.error == "user_id" || data.error == "catalog")
                        $("#rating-form .email .error").show();
                    if(data.error == "text")
                        $("#rating-form .text .error").show();
                    if(data.error == "rating")
                        $("#rating-form .user_rate .error").show();
                    if(data.error == "voice")
                        $("#rating-form .voice .error").show();
                }

            }',
            'async' => true,

        ),
        array(
            'type' => 'submit'
        )); ?>

    </div>
	<!--<div><a href="<?php /*echo $this->createUrl('/site/join'); */?>" target="_blank">Регистрация</a></div>-->
<?php echo CHtml::endForm();?>