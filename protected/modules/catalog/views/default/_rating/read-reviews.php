<?php if(isset($rating) && count($rating)): ?>

    <?php foreach($rating as $data): ?>
        <?php $params = array(
                "name" => $data->id,
                "id" => $data->id,
                "readOnly" => true,
                "ratingStepSize" => 1,
                "minRating" => 1,
                "maxRating" => 5,
                "cssFile" => Yii::app()->theme->baseUrl.'/css/stars.css',
				'htmlOptions' => array('class'=>'lines'),
        ) ?>
        <?php /* $this->renderPartial('application.modules.rating.views._stars',array(
                'id' => 'review-value',
                'dataId'=>$rating->id,
                'rating_avg'=>$rating->value,
                'voices'=>1,
                'class'=>'reviewProduct',
                'readonly'=>true
            ));*/ ?>

        <?php $this->renderPartial('_inc/_block_item',array('data'=>$data,'params'=>$params));?>

    <?php endforeach; ?>

<?php else: ?>

<div>Нет отзывов</div>

<?php endif;?>