
<div class="pane">
    <?php $this->widget('IamTabs', array(
        'htmlOptions' => array('class' => 'sub-tab'),
        'ulClass' => 'sub-tabs',
        'cssFile' => false,
        'tabs'=>array(
            'read-reviews'=>array(
                'id' => 'read-reviews',
                'title'=> '<span>' . Misc::t('Читать отзывы') . '</span>',
                'content'=> $this->renderPartial('_rating/read-reviews', array('rating'=>$model->rating),true),
                //'visible' => !empty($model->composition)
            ),
            'write-review'=>array(
                'id' => "write-review",
                'title'=> '<span>' .Misc::t('Оцените продукт') . '</span>',
                'content'=>  $this->renderPartial('_rating/_write_review', array('model'=>$model),true),
               // 'visible' => !empty($model->ingredients)
            ),
        ),
    ))?>
</div>