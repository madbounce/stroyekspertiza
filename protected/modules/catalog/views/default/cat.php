<?php
$this->breadcrumbs=array(
    Misc::t('Продукция') => array('/catalog/default/catalog'),
    Misc::t('Кошки'),
);
$this->breadcrumbs_class .= 'products_line ';
?>
    <div class="bigDog cat2">
        <h1>
            КОШКИ УНИКАЛЬНЫ<br />КОРМИТЕ ИХ ДОСТОЙНО<br />
            <small>Суперпремиальное питание Eukanuba для кошек</small>
        </h1>
        <div class="article short"><a href="<?php echo $this->createUrl('/catalog/default/index',array('type'=>'dog'))?>">Корм для собак</a></div>
    </div>

    <div class="middle DentaDefense cat" style="margin-top: 24px;">
        <?php $this->renderPartial('_left_block', array('model' => $model,'type' => $type)); ?>

        <div id="products_list" class="rightSide">
            <?php $this->renderPartial('_product_list', array('model' => $model, 'type' => $type)); ?>
        </div>
    </div>
<!-- main content section end--> 