<?php
/**@var $this Controller */
$this->breadcrumbs=array(
    Misc::t('Продукция') => array('/catalog/default/catalog'),
    Misc::t('Собаки'),
);
$this->breadcrumbs_class .= 'products_line ';
?>

    <?php if($subtype == 'puppy'): ?>

        <div class="header_image catalog">
            <div class="content_header">
                <div class="text_header">
                    <h3>КАТАЛОГ ПРОДУКЦИИ</h3>
                    <p>ВЕДУЩИЕ ЗАВОДЧИКИ* РЕКОМЕНДУЮТ ВАШЕМУ ЩЕНКУ КОРМ  EUKANUBA</p>
                    <p class="second">* Победители выставок: CRUFTS, WORLD DOG SHOW, EURO DOG SHOW 2012</p>
                </div>

                <div class="header_right_menu">
                    <ul>
                        <li><a href="/articles/default/puppy">СТАТЬИ</a></li>
                        <li><a target="_blank" href="http://www.youtube.com/channel/UCp6hEh5cS8p9r1SgylxbFyQ">ВИДЕО О ВОСПИТАНИИ</a></li>
                        <li><a href="/products/dog/puppy">ПОДБОР КОРМА</a></li>
                        <li><a href="/page/nutrition" class="active">ПИТАНИЕ ЩЕНКОВ</a></li>
                        <li><a href="/page/shoppinglist">ЛИСТ ПОКУПОК</a></li>
                        <li><a href="/page/dog_breed_selector">НАЙДИТЕ СВОЮ ИДЕАЛЬНУЮ ПОРОДУ</a></li>
                        <li><a href="/page/breedersrecommended">РЕКОМЕНДАЦИИ ЗАВОДЧИКОВ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="bigDog dog4 part2">
            <h1>
                <span>СОБАКИ УНИКАЛЬНЫ</span><br />КОРМИТЕ ИХ ДОСТОЙНО<br />
                <small>Суперпремиальное питание Eukanuba для собак</small>
            </h1>
            <div class="article short"><a href="<?php echo $this->createUrl('/catalog/default/index',array('type'=>'cat'))?>">Корм для кошек</a></div>
        </div>
    <?php endif;?>

    <div class="middle DentaDefense cat" style="margin-top: 23px;">
        <?php $this->renderPartial('_left_block', array('model' => $model, 'type' => $type)); ?>

        <div id="products_list" class="rightSide">
            <?php $this->renderPartial('_product_list', array('model' => $model, 'type' => $type)); ?>
        </div>
    </div>
<!-- main content section end--> 