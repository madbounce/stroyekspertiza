<?
    $name = $data->name;
    $url = Yii::app()->createUrl('/catalog/default/view', array('url' => $data->url));

    $photo = $data->getPhoto(100, 0, array('method' => 'resize'));

    if (!empty($photo))
        $image = CHtml::image('/phpthumb/phpthumb.php?src='.$photo.'&w=100&h=0&zc=1', $name, array('width' => 100,'style'=>'max-height:160px;'));
    else
        $image = CHtml::image(Yii::app()->theme->baseUrl . '/images/no2.gif', '', array('width' => 86));

    $is_new_class = $data->is_new ? 'new' : '';
?>
    <div class="product <?php echo $is_new_class;?>">
        <a href="<?=$url?>" alt="<?=$name?>" title="<?=$name?>" class="name_pr"><?php echo $image; ?></a>
		<div class="new-sign"></div>
		<div class="posit">
            <span></span>
            <a href="<?=$url?>" alt="<?=$name?>" title="<?=$name?>" class="name_pr"><?=$name?></a>
            <br />
            <br />
            <a href="<?=$url?>" alt="<?=$name?>" title="<?=$name?>">УЗНАТЬ БОЛЬШЕ ></a>
        </div>

        <?php $this->renderPartial('application.modules.rating.views._stars',array('data'=>$data,'class'=>'rateProduct','readonly'=>true)); ?>

    </div>

    
    
