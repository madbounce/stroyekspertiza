<div id="MainContent" class="product-listing">
      <div class="copy-top-border"><img height="8" width="956" class="topborder" alt="" src="<?php echo  Yii::app()->theme->baseUrl ?>/images/top-border-dog.png"></div>
      <div id="white-container"><div class="clear-all">&nbsp;</div>
      <div id="breadcrumb"><a title="<?=Misc::t('Главная');?>" href="/"><?=Misc::t('Главная');?></a> / <a title="<?=Misc::t('Продукция');?>" href="/cat-food"><?=Misc::t('Продукция');?></a> /  Cухой корм</div>
<!-- <div id="breadcrumb"><a title="Home" href="/">Home</a> / <a href="#">Cat Food: Find the Right Cat Food for Your Cat at Iams.co.uk</a> / Healthy Cat Food: Buy Iams ProActive Health Cat Food at Iams.co.uk</div> -->
        <div class="clear-all">&nbsp;</div>
        <!--Copy content start -->
        <div class="page-header"><h1>Iams® ProActive Health™</h1> 
          <p>Не просто корма. Питание для здоровой и счастливой жизни. Рационы Iams ProActive Health для кошек помогают поддерживать правильное пищеварение и сильную иммунную защиту организма.<br></p>
        </div>
         
          <div id="bodycontent_area">
          <div class="_col left-callouts-nav">
          <div class="left-callouts">
          &nbsp;
            
             </div>
          </div>
          <div class="_col txt-content">

            <?php $this->renderPartial('_product_list', array('model' => $model, 'filter0' => $filter0, 'filter1' => $filter1, 'type' => $type)); ?>
            
            <div class="clear-all">&nbsp;</div>

          </div>
          
          <div class="clear-all">&nbsp;</div>
          </div>
        </div>
        <div class="clear-all">&nbsp;</div>
      </div>
<!-- main content section end--> 