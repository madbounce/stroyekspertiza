<?php
    /** @var $model Catalog */
    /** @var $this Controller */
    $url = "/catalog/default/view?url=" . $model->url;

    $this->breadcrumbs=array(
        Misc::t('Продукция') => array('/catalog/default/index', 'type' => $model->category->parentsId->path),
        $model->category->title => array('/catalog/default/index', 'type' => $model->category->parentsId->path, 'subtype' => $model->category->path),
        $model->name
    );
    $this->breadcrumbs_class .= 'products_line';

    $photo = $model->getPhoto(220, 0, array('method' => 'resize'),array('quality'=>100));

    if (!empty($photo))
        $image = CHtml::image($photo, $model->name);
    else
        $image = CHtml::image(Yii::app()->theme->baseUrl . '/images/no2.gif', '');


    $params = array(
        "name" => $model->id,
        "id" => $model->id,
        "readOnly" => true,
        "ratingStepSize" => 1,
        "minRating" => 1,
        "maxRating" => 5,
        "cssFile" => Yii::app()->theme->baseUrl.'/css/stars.css',
    );
?>
<script>
    function openTab(tab,tab2)
    {

        $('#yw0 li a.active').removeClass('active');
        $('#yw0 div.view').hide();

        $('#yw2 li a.active').removeClass('active');
        $('#yw2 div.view').hide();

        $('#yw0 ul li.'+tab+' a').addClass('active');
        $('#yw0 div#'+tab).show();

        $('#yw2 ul li a[href="#'+tab2+'"]').addClass('active');
        $('#yw2 div#'+tab2).show();


        return true;
    }
</script>

<?php $this->renderPartial('application.modules.rating.views.init'); ?>

<div class="middle product">
    <div class="leftSide">
        <?php echo $image; ?>
        <? //var_dump($model->sortsize($model->bag_size));?>
		<span class="bag-size">Доступен в весе:<br><br>
            <div style="margin-left: 28PX; text-align: left; ">
            <?php //echo str_replace(array(';',';',','),'<br>',str_replace(array(' ','&nbsp;'),'',$model->bag_size));?>
            <? foreach($model->sortsize($model->bag_size) as $el): ?>
                <? echo $el . "<br>"; ?>
            <? endforeach;?>
            </div>
        </span>
    </div>
    <div class="rightSide">
        <div class="nameOfFood">
            <?php echo $model->name; ?>
            <br>
            <span> <?php echo (trim($model->recommendedFor)) ? 'для: '.mb_strtolower($model->recommendedFor,'utf-8') : ''; ?></span>
        </div>
        <div class="rate">
            <div class="leftSide1">
                <div class="catalog-rating">
                    <?php // $this->renderPartial('application.modules.rating.views._stars',array('data'=>$model,'class'=>'rateProduct','readonly'=>true)); ?>
                    <?php echo $this->widget("CStarRating", CMap::mergeArray($params,array('id'=>'total_rating_avg'.$model->id,'name'=>'total_rating_avg','value'=>$model->rating_avg)), true);?>
                    <span><?php echo Yii::t('catalog','{n} Отзыв|{n} Отзыва|{n} Отзывов', $model->voices);?></span>
                </div>
                <div class="moreAboutRating">
                    <a href="#read-reviews" onclick='openTab("reviews","read-reviews");'>Посмотреть отзывы</a> | <a href="#write-review" onclick='openTab("reviews","write-review");'>Написать отзыв</a>
                </div>
            </div>
            <div class="rightSide1">
                <a href="<?= Config::model()->get('buyNowLink', '');?>" class="buy">Где купить?</a>
            </div>
        </div>

        <?php $this->widget('IamTabs', array(
                'htmlOptions' => array('class' => 'tabs'),
                'cssFile' => false,
                'tabs'=>array(
                    'description'=>array(
                        'title'=> Misc::t('Обзор продукта'),
                        'view'=>'_description_tab',
                        'data'=>array('model'=>$model),
                    ),
                    'nutritional'=>array(
                        'title'=>Misc::t('Питательная ценность'),
                        'view'=>'_nutritional_tab',
                        'data'=>array('model'=>$model),
                        'visible' => ($model->composition || $model->ingredients || $model->feedingGuidelinesText || $model->additives)
                    ),
                    'reviews'=>array(
                        'title'=> Misc::t('Отзывы'),
                        'view'=>'_reviews_tab',
                        'data'=>array('model'=>$model),
                        'itemClass' => 'reviews',
//                        'visible' => ($model->composition || $model->ingredients || $model->feedingGuidelinesText || $model->additives)
                    ),

                ),
            )) ?>
    </div>
</div>
