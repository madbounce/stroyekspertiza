<?php

class CommentableBehavior extends CActiveRecordBehavior
{
    private $entity;

    public function attach($owner)
    {
        parent::attach($owner);

        $this->entity = get_class($this->owner);

        $this->getOwner()->getMetaData()->addRelation('comments',
            array(CActiveRecord::HAS_MANY, 'Comment', 'entity_pk',
                'condition' => 'entity = :entity', 'scopes' => array('language'), 'params' => array(':entity' => $this->entity))
        );

        $this->getOwner()->getMetaData()->addRelation('allComments',
            array(CActiveRecord::HAS_MANY, 'Comment', 'entity_pk',
                'condition' => 'entity = :entity', 'params' => array(':entity' => $this->entity))
        );
    }

    public function addComment($comment)
    {
        $comment->language = Yii::app()->language;
        $comment->entity = $this->entity;
        $comment->entity_pk = $this->owner->primaryKey;

        return $comment->save();
    }

    public function newComment($parent = null)
    {
        $user = User::model()->findByPk(Yii::app()->user->id);
        $comment = new Comment;

        if (!empty($parent))
            $comment->parent_id = $parent->id;
        else
            $comment->parent_id = null;

        if (isset($_POST['Comment'])) {
            $comment->attributes = $_POST['Comment'];
            $comment->user_id = $user->id;

            if ($this->owner->addComment($comment)) {
                Yii::app()->controller->refresh();
            }
        }

        return $comment;
    }

    public function beforeDelete($event)
    {
        parent::beforeDelete($event);

        Yii::import('comment.models.Comment');
        foreach ($this->owner->allComments as $comment)
            $comment->delete();
    }

    public function getCommentDataProvider()
    {
        return new CActiveDataProvider('Comment', array(
            'criteria' => Comment::model()->entity($this->owner)->language()->parent()->getDbCriteria(),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
    }
}
