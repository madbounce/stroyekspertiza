<?php

Yii::import('comment.models.Comment');

class CommentsWidget extends CWidget
{
    public $model;

    public function run()
    {
        $modelLayoutFile = lcfirst(get_class($this->model));
        if ($this->getViewFile($modelLayoutFile))
            $this->render($modelLayoutFile, array('model' => $this->model));
        else
            $this->render('comments', array('model' => $this->model));
    }
}
