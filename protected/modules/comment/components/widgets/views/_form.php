<?php $form = $this->beginWidget('CActiveForm'); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textArea($model, 'comment'); ?>

<?php echo $form->hiddenField($model, 'parent_id'); ?>

<?php echo CHtml::submitButton(Yii::t('comment', 'Отправить')); ?>

<?php $this->endWidget(); ?>