<div class="comment">
    <div class="comment-name">
        <?php echo CHtml::encode($data->user->id); ?>
    </div>
    <div class="comment-comment">
        <?php echo CHtml::encode($data->comment); ?>
    </div>

    <div class="comment-children">
        <?php foreach ($data->children as $child): ?>
        <div class="comment-child">
            <div class="comment-name">
                <?php echo CHtml::encode($child->user->id); ?>
            </div>
            <div class="comment-comment">
                <?php echo CHtml::encode($child->comment); ?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>

    <div class="comment-reply">
        <?php $this->render('_form', array(
            'model' => $model->newComment($data),
        )); ?>
    </div>
</div>