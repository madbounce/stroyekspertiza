<div class="comments-wrapper">
    <?php $dataProvider = $model->getCommentDataProvider(); ?>
    <?php if (empty($dataProvider->data)): ?>
        <h3><?php echo Yii::t('comment', 'Комментариев пока нет'); ?></h3>
    <?php else: ?>

        <h3><?php echo Yii::t('comment', 'Комментарии'); ?></h3>

        <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
            'viewData' => array('model' => $model)
        )); ?>

    <?php endif; ?>

    <?php if (!Yii::app()->user->isGuest): ?>
        <h2><?php echo Yii::t('comment', 'Оставить комментарий'); ?></h2>
        <?php $this->render('_form', array(
            'model' => $model->newComment(),
        )); ?>
    <?php endif; ?>
</div>