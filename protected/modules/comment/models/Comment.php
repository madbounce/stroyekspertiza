<?php

/**
 * This is the model class for table "{{comment}}".
 *
 * The followings are the available columns in table '{{comment}}':
 * @property string $id
 * @property string $parent_id
 * @property string $language
 * @property integer $user_id
 * @property string $entity
 * @property string $entity_pk
 * @property string $create_time
 * @property string $update_time
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property Comment $parent
 * @property Comment[] $comments
 * @property User $user
 */
class Comment extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{comment}}';
    }

    public function rules()
    {
        return array(
            array('language, user_id, entity, entity_pk, comment', 'required'),
            array('parent_id, user_id, entity_pk', 'length', 'max' => 10),
            array('language', 'length', 'max' => 16),
            array('entity', 'length', 'max' => 32),
            array('comment', 'length', 'max' => 512),

            array('id, parent_id, language, user_id, entity, entity_pk, create_time, update_time, comment', 'safe', 'on' => 'search'),

            array('parent_id', 'default', 'value' => null)
        );
    }

    public function relations()
    {
        return array(
            'parent' => array(self::BELONGS_TO, 'Comment', 'parent_id'),
            'children' => array(self::HAS_MANY, 'Comment', 'parent_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'parent_id' => 'Parent',
            'language' => 'Language',
            'user_id' => 'User',
            'entity' => 'Entity',
            'entity_pk' => 'Entity Pk',
            'create_time' => Yii::t('comment', 'Дата создавния'),
            'update_time' => 'Update Time',
            'comment' => Yii::t('comment', 'Комментарий'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('parent_id', $this->parent_id, true);
        $criteria->compare('language', $this->language, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('entity', $this->entity, true);
        $criteria->compare('entity_pk', $this->entity_pk, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('comment', $this->comment, true);

        return new CActiveDataProvider($this->resetScope(), array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
            )
        );
    }

    /**
     * Criteria
     */

    public function getEntityCriteria($entity)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addCondition("$alias.entity = :entity");
        $criteria->addCondition("$alias.entity_pk = :entity_pk");
        $criteria->params = array(
            ':entity' => get_class($entity),
            ':entity_pk' => $entity->primaryKey,
        );
        return $criteria;
    }

    public function getLanguageCriteria($language = null)
    {
        if (empty($language))
            $language = Yii::app()->language;

        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addCondition("$alias.language = :language");
        $criteria->params = array(':language' => $language);
        return $criteria;
    }

    public function getParentCriteria($parent = null)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        if (!empty($parent)) {
            $criteria->addCondition("$alias.parent_id = :parent_id");
            $criteria->params = array(':parent_id' => $parent->id);
        } else {
            $criteria->addCondition("$alias.parent_id IS NULL");
        }
        return $criteria;
    }

    /**
     * Scopes
     */

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);
        return array(
            'order' => "$alias.create_time ASC",
        );
    }

    public function entity($entity)
    {
        $this->getDbCriteria()->mergeWith($this->getEntityCriteria($entity));
        return $this;
    }

    public function language($language = null)
    {
        $this->getDbCriteria()->mergeWith($this->getLanguageCriteria($language));
        return $this;
    }

    public function parent($parent = null)
    {
        $this->getDbCriteria()->mergeWith($this->getParentCriteria($parent));
        return $this;
    }
}