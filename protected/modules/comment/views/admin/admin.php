<?php
$this->breadcrumbs = array(
    Yii::t('comment', 'Комментарии')
);
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'comment-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => '#',
            'name' => 'id',
            'htmlOptions' => array('class' => 'span1'),
        ),
        array(
            'name' => 'comment',
            'htmlOptions' => array('class' => 'span8'),
        ),
        array(
            'name' => 'create_time',
            'htmlOptions' => array('class' => 'span3'),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}'
        ),
    ),
)); ?>