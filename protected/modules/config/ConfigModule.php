<?php

class ConfigModule extends CWebModule
{
    public function init()
    {
        $this->setImport(array(
            'config.models.*',
            'config.components.*',
        ));
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        } else
            return false;
    }
}
