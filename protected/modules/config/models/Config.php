<?php

/**
 * This is the model class for table "config".
 *
 * The followings are the available columns in table 'config':
 * @property string $id
 * @property string $key
 * @property string $value
 * @property string $description
 */
class Config extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{config}}';
    }

    public function rules()
    {
        return array(
            array('key, value, description', 'required'),
            array('key, value, description', 'length', 'max' => 255),
            array('sort', 'numerical', 'integerOnly'=>true),
        );
    }

    public function behaviors(){
        return array(
            'SortBehavior' => array(
                'class' => 'ext.behaviors.SortBehavior',
            ),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'key' => Yii::t('config', 'Ключ'),
            'value' => Yii::t('config', 'Значение'),
            'description' => Yii::t('config', 'Описание'),
            'sort' => Yii::t('config', 'Сортировка'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('key', $this->key, true);
        $criteria->compare('value', $this->value, true);
        $criteria->compare('description', $this->description, true);

        $criteria->order = 'sort DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * General
     */
    public static function get($key, $default = null)
    {
        $model = Config::model()->findByAttributes(array('key' => $key));
        if (!empty($model))
            return $model->value;

        return $default;
    }

    public static function set($key, $value, $description = null)
    {
        $count = Config::model()->updateAll(array('value' => $value), 'key = :key', array(':key' => $key));
        if ($count)
            return true;

        $model = new Config;
        $model->key = $key;
        $model->value = $value;
        $model->description = $description;
        if ($model->save())
            return true;

        return false;
    }
}