<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'config-form',
    'enableAjaxValidation' => true,
	'htmlOptions' => array(
        'class' => 'product'
    ),
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>
<?php echo $form->errorSummary($model); ?>

<div id="column_left">
	<div class="block layer" style="width: auto; min-height: auto;">
		<ul>
			<li>
				<?php echo $form->labelEx($model, 'key', array('class' => 'property')); ?>
				<?php echo $form->textField($model, 'key', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
			</li>
			<li>
				<?php echo $form->labelEx($model, 'value', array('class' => 'property')); ?>
				<?php echo $form->textField($model, 'value', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
			</li>
			<li>
				<?php echo $form->labelEx($model, 'description', array('class' => 'property')); ?>
				<?php echo $form->textField($model, 'description', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
			</li>
		</ul>

	</div>
</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
    )); ?>
</div>

<?php $this->endWidget(); ?>