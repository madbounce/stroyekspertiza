<?php
$dp = $model->search();
$countAll = $dp->totalItemCount;
?>

<div class="page-header admin-header">
    <h3><?php echo Yii::t('app', '{n} параметр|{n} параметра|{n} параметров', $countAll);?></h3>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => Yii::t('translate', 'Добавить параметр'),
        'size' => 'small',
        'url' => array('/config/admin/create'),
    ));
    ?>
</div>
<div class="page-content">
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'id' => 'mail-grid',
    'type'=>'striped bordered',
    'dataProvider' => $dp,
    //'filter' => $model,
    'enableSorting' => false,
    'sortableRows' => true,
    'sortableAjaxSave'=>true,
    'sortableAttribute'=>'sort',
    'sortableAction'=> 'config/admin/sortable',
    'template' => '{pager}{items}{pager}',
    'columns' => array(
        array(
            'name' => 'key',
            'type'=>'raw',
            'value' => 'CHtml::link($data->key, array("/config/admin/update", "id" => $data->id))',
        ),
        'value',
        'description',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}'
        ),
    ),
)); ?>
</div><!-- end of .page-content -->
