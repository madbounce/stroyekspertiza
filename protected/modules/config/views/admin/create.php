<?php
$this->breadcrumbs = array(
    Yii::t('config', 'Настройки') => array('index'),
    Yii::t('config', 'Создание параметра'),
);
?>

<div class="page-header">
    <h3><?php echo Yii::t('config', 'Создание параметра'); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>