<?php
$this->breadcrumbs = array(
    Yii::t('config', 'Настройки') => array('index'),
    $model->key => array('update', 'id' => $model->id),
    Yii::t('config', 'Редактирование параметра'),
);
?>
<div class="page-header">
    <h3><?php echo Yii::t('config', 'Редактирование параметра :id', array(':id' => '#' . $model->key)); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>