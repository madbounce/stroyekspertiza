<?php 
class ActionsWidget extends CWidget
{
	public $type;
	public $actionID;
	protected $viewPath;
	public $assetsPath;
	
	public function run()
	{
		parent::run();

		$assetsPath = dirname(__FILE__).'/../assets';
		$this->assetsPath = Yii::app()->assetManager->publish($assetsPath);
		$this->viewPath = "coupon/default/action";
		if (isset($this->type)) {
			$actionModel = Action::model();

			$criteria = $actionModel->getDbCriteria();
                        $criteria->condition="t.moderation=1 and t.type='".$this->type."'";
                        	
			$pages = Paging::get($actionModel, $criteria,12);
			$actions = $actionModel->findAll($criteria);
//if (count($actions)==0) $this->render('noaction'); else
			$this->render('current', compact('actions', 'pages'));
		} else {
                  
			$actions = Action::model()
				->random($this->actionID)
				->findAll('t.moderation=1');
//			if (count($actions)==0) $this->render('noaction'); else
			$this->render('more', compact('actions'));
		}
	}
}