<?php 
class CommodityCategories extends CWidget
{
	public $mode;
	
	public function run()
	{
		parent::run();
		$categories = CommodityCategory::model()->findAll();
		$requestPageUrl = "coupon/default/request/?category_id=";
		
		$this->render("commodCategories/{$this->mode}", compact('categories', 'requestPageUrl'));
	}
}