<div class="slide2">
	<ul id="mycarousel2" class="jcarousel-skin-tango">
		<?php foreach ($categories as $category) {
			$pageUrl = $requestPageUrl . $category->id;?>
		<li>
			<div class="slide2block">
				<div class="text6">
					<a href="<?php echo $pageUrl;?>"><?php echo $category->title;?></a>
				</div>
				<div class="slide2img">
					<a href="<?php echo $pageUrl;?>"><img src="<?php echo $category->logo;?>" /></a>
				</div>
			</div>
		</li>
		<?php }?>
	</ul>
</div>