<?php $action = $item->action;?>
<div class="list_service_uslug">
	<div class="name_price_service">
		<span class="gray sz13"><?php echo Yii::t('profile', 'Name'); ?>: </span>
		<a href="<?php echo Yii::app()->createUrl("coupon/default/action/?coupon_id={$item->id}");?>" class="name_service_b sz16"><?php echo $action->title;?></a>
	</div>
	<table cellpadding="0" cellspacing="0" class="buyer_deals">
		<tr>
			<td>
				<div class="sz14"><?php echo Yii::t('profile', 'the duration of the coupon'); ?>:</div>
				<div class="gray">
				<?php echo Yii::t('profile', 'beginning'); ?>: <?php echo date('d.m.Y', strtotime($item->created)); ?><br />
				<?php echo Yii::t('profile', 'end'); ?>: <?php echo date('d.m.Y', strtotime($item->expiration_date)); ?>
				</div>
			</td>
			<td>
				<div class="sz14"><?php 
                                
                                           $dd=$action->getVDS();
                                           
                                echo Yii::t('profile', 'Paid'); ?></div>
				<div class="blue"><?php 
                         echo $dd['v'];   ?></div>
			</td>
			<td>
				<div class="sz14"><?php echo Yii::t('profile', 'Discount'); ?></div>
				<div class="blue"><?php echo $dd['d']  ?>%</div>
			</td>
			<td>
				<div class="sz14"><?php echo Yii::t('profile', 'You Save'); ?></div>
				<div class="blue"><?php
                        
                        echo $dd['s'];
                        
                        ?></div>
			</td>
			<td>
				<a target="_blank" class="blue_btn" style="padding: 6px 22px;" href="<?php echo Yii::app()->createUrl("user/profile/coupunprint",array('id'=>$item->id));?>">Print</a>
			</td>
		</tr>
	</table>
</div>