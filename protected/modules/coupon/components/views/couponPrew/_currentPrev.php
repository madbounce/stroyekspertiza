<?php $url = Yii::app()->createUrl("coupon/default/action",array('action_id'=>$action->id)); ?>
<div class="single-deal">
    <i class="skidky"><?php echo $action->discount;?>%</i>
	<div class="img-wrapper">
		<div class="blured-img">
			<a href="<?php echo $url;?>">
            <img style="width:212px;height:143px" src="<?php echo $action->getsrcone(212,143)?>">
         </a>
			<div class="blurHandle">
			   <img src="<?php echo $action->getsrcone(212,143)?>">
				 <? /* canvas width="212" height="141" tabindex="-1" id=""></canvas */ ?>
			</div>
		</div>
		<div class="deal-desc">
			<h3><a href="<?php echo $url;?>"><?php echo CHtml::encode(StringHelper::word_limiter2($action->title,23))?></a></h3>
			<p><?php echo Yii::t('profile', 'Price'); ?>: US$ <?php echo $action->getPrice(); ?></p>
		</div>
	</div>

	<p class="deal-stat"><?php echo Yii::t('profile', 'Expiration date of deal');?> <?php echo Yii::app()->controller->formateDate($action->expiration_date);?></p>

</div>