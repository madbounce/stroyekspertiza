<?php $couponUrl = Yii::app()->createUrl("coupon/default/action/?coupon_id={$coupon->id}");?>
<div class="img-wrapper">
	<div class="blured-img">
		<a href="<?php echo $couponUrl;?>"><img src="/images/<?php echo isset($coupon->photo)&&$coupon->photo<>'' ? "dealphotos/{$coupon->photo}" : 'noboat.jpg';?>"></a>
		<div class="blurHandle">
			 <canvas width="212" height="141" tabindex="-1" id=""></canvas>
		</div>
	</div>
	<div class="deal-desc">
		<h3><a href="<?php echo $couponUrl;?>"><?php echo $coupon->deal->model->title;?></a></h3>
		<p><?php echo Yii::t('profile', 'Year');?>: <?php echo $coupon->deal->year . '  /  ';?>
		<?php echo Yii::t('profile', 'Price');?>: US $<?php echo $coupon->deal->price;?></p>
	</div>
</div>