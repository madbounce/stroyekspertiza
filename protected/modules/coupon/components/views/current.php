<h2><?php echo Yii::t('profile', $this->type); ?></h2>
<?php foreach ($actions as $action)
	echo $this->render('couponPrew/_currentPrev', compact('action'));

$this->widget('CLinkPager', array(
	'pages' => $pages,
	'lastPageLabel' => '&raquo;&raquo;',
	'firstPageLabel' => '&laquo;&laquo;',
	'maxButtonCount' => 10,
	'header' => '',
	'htmlOptions' => array('class' => 'pagination-bl'),
	'nextPageLabel' => '',
	'prevPageLabel' => '',
));