<div class="deal_servise_pr">
	<h2 style="text-align:center"><?php echo Yii::t('profile', 'More products and services');?></h2>
</div>
<div id="offers-scroller">
	<div class="customScrollBox">
		<div class="horWrapper">
			<div class="scroll-container">
				<div class="scroll-content">
					<?php foreach ($actions as $action) {
                                           
						$url = Yii::app()->createUrl($this->viewPath, array('action_id'=>$action->id) );?>
					<div class="single-deal">
						<div class="img-wrapper" style="height: 143px;">
							<div class="blured-img">
                                                         
								<a href="<?php echo $url;?>"><img style="width:212px;height:143px" src="<?php echo $action->getsrcone('auto',143);?>"></a>
								
                                                                <div class="blurHandle">
									 <canvas width="212" height="141" tabindex="-1" id=""></canvas>
								</div>
							</div>
							<div class="deal-desc">
                                                            <h3><a href="<?php echo $url;?>"><?php echo CHtml::encode(StringHelper::word_limiter2($action->title,23))?></a></h3>
								<p><?php echo Yii::t('profile', 'Price'); ?>: US$ <?php echo $action->getPrice(); ?></p>
							</div>
						</div>	
                                           
						
                                            <p class="deal-stat"><?php echo Yii::t('profile', 'Discount');?> <?php echo $action->discount;?>%  /  $<?php echo $action->share_price_without_discount;?></p>
					
                                        </div>
				<?php } ?>
				</div>
			</div>
			<div class="dragger-wrapper">
				<div class="dragger_container">
					<div class="dragger"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php Yii::app()->clientScript->registerScriptFile($this->assetsPath . '/scrollbar/jquery.mCustomScrollbar.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerCssFile($this->assetsPath.'/scrollbar/jquery.mCustomScrollbar.css');?>
<script language="JavaScript"><!--
$(window).load(function(){mCustomScrollbars();});
function mCustomScrollbars(){
	$("#offers-scroller").mCustomScrollbar(
		"horizontal", 500, "easeOutCirc", 1, "fixed", "yes", "yes", 10
	); 
}
$.fx.prototype.cur = function(){
	if ( this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null) ) {
		return this.elem[ this.prop ];
	}
	var r = parseFloat( jQuery.css( this.elem, this.prop ) );
	return typeof r == 'undefined' ? 0 : r;
}
function LoadNewContent(id,file){
	$("#"+id+" .customScrollBox .content").load(file,function(){
		mCustomScrollbars();
	});
}
//--></script>