<?php
Yii::import('application.components.messages.Messages');
Yii::import('application.helpers.MailSender');

class DefaultController extends Controller
{
	public $layout = '//layouts/column1';

	protected function beforeAction($action)
	{
		$assetsPath = dirname(__FILE__).'/../assets';
		$this->assetsPath = Yii::app()->assetManager->publish($assetsPath);

		return parent::beforeAction($action);
	}

	public function filters()
	{
		return array('accessControl');
	}
	
	public function actionIndex($type)
	{
            if ($type=='Products' || $type=='Services')
            {
                $c=new CDbCriteria();
                $c->condition='moderation=1 and type=:t';
                $c->params[':t']=$type;
                $c->order='t.id DESC';
                Yii::import('application.components.listing.*');
                $model=  Action::model();
                Complist::ok(6, $model, $c);
                
		$this->render($type=='Services'?'services':'products',array('models'=>$model->findAll($c)));
                
            } else throw new CHttpException(404, Yii::t('app', 'Error'));
	}
	
	private function getCommodAction()
	{
		$actionId = Yii::app()->getRequest()->getQuery('action_id');
		$couponId = Yii::app()->getRequest()->getQuery('coupon_id');
		if (isset($actionId))
                {
                    if (isset(Yii::app()->session['isadmin']) && Yii::app()->session['isadmin'])
                        $action = Action::model()
				->with('user')
				->findByPk($actionId);
                    else
			$action = Action::model()
				->with('user')
				->findByPk($actionId,'moderation=1');
                }
		else if (isset($couponId)) {
			$coupon = CouponCommodity::model()->findByPk($couponId);
			$action = $coupon->action->with('user');
		}
		
		return $action;		
	}

	// TODO: убрать
	private function savePayment($userId, $sum, $type, $actionId)
	{
		$payment = new Payment();
		$payment->user_id = $userId;
		$payment->value = $sum;
		$payment->type = $type;
		$payment->good_id = $actionId;
		$payment->number = $payment->getLastNumber($userId) + 1;
		
		$payment->save();
	}
	
	/**
	 * actionAction
	 *
	 * @return void
	 */
	public function actionAction()
	{
		$action = $this->getCommodAction();
                if ($action==null)  throw new CHttpException(404,Yii::t ('app', 'not found'));
		$available = $action->checkAvailability($action->id);

		// товар
	//	$commodID = $action->commod_id;
	//	$commodity = Commodity::model()->findByPk($commodID);
		// Send link
		$sendLinkFormModel = MailSender::sendLink();
		// выводим
		$buyForFriendModel = new BuyCouponForm();
                
                
                
                
                            if (isset($_GET['layout']) && $_GET['layout']=='print')
            {
                $this->layout='//layouts/print';
                        $cs=Yii::app()->clientScript;
$cs->scriptMap=array(
    'jquery.js'=>false,
    'jquery.min.js'=>false,
    'jquery.metadata.js'=>false,
);
$print=true;
$v='print';
            } else  
                
            {
                $print=false;
              $v='action';  
            }
                
                
                
		$this->render($v, array(
			'data'=>array(
			//	'commodity' => $commodity,
				'action' => $action,
				'available' => $available,
				'msgModelName' => 'UserMessage',
				'idFrom' => User::getUserId(),
				'sendLinkFormModel' => $sendLinkFormModel,
				'buyForFriendModel' => $buyForFriendModel,
			)
		));	
	}
	
	public function actionPdf()
	{
		$action = $this->getCommodAction();
		// товар
	//	$commodID = $action->commod_id;
	//	$commodity = Commodity::model()->findByPk($commodID);

		PdfCreator::create($this->module, compact('action', 'commodity'));
	}

	/**
	 * List of quotes
	 */
	public function actionGetServiceBids()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$categoryName = $_POST['category'];
			$category = CommodityCategory::model()
				->findByAttributes(Array('title'=>$categoryName));
			
			// только для текущего юзера
			$criteria = new CDbCriteria();
			$criteria->condition = "t.user_id = '" . User::getUserId() . "'";
			$requests = CommodityRequest::model()
				->with(Array('commod_cat'=>Array(
					'condition'=>"commod_cat.id = '{$category->id}'"
				)))
				->with('quotes')
				->findAll($criteria);
			
			$quotes = Array();
			$costs = Array();
			foreach ($requests as $request) {
				foreach ($request->quotes as $quote) {
					$quotes[] = Array(
						'text' => $quote->text,
						'cost' => $quote->cost,
						'company' => $quote->company->title,
						'owner' => $quote->company->author_id,
					);
					$costs[] = $quote->cost;
				}
			}
			array_multisort($costs, $quotes);
			
			Yii::import('system.web.helpers.CJson');
			echo CJson::encode(Array(
				'category' => $category,
				'quotes' => $quotes,
			));
			Yii::app()->end();
		}
	}

	public function actionGetServiceClaims()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$categoryName = $_POST['category'];
			$category = CommodityCategory::model()
				->findByAttributes(Array('title'=>$categoryName));
                        if ($category==null) exit();
			
			$criteria = new CDbCriteria();
			$criteria->condition = "t.category_id = '{$category->id}'";
			$criteria->order = 't.price ';
			$criteria->order .= isset($_POST['order']) ? $_POST['order'] : 'ASC';
			$userClaims = CommodClaim::model()
				//->approved()
				->findAll($criteria);

			$claims = Array();
			// есть ли у юзера такая услуга
			//$participant = false;
			
                       if (Yii::app()->user->isGuest) 
                           $participant = false;
                       else
                       {
                       $participant= Yii::app()->db->createCommand("SELECT count(*) FROM `tbl_commod_cat_user` t WHERE t.`user_id`=".Yii::app()->user->id." and `category_id`=".$category->id)->queryScalar()>0;
                       }
			foreach ($userClaims as &$claim) {
			//	if ($claim->user_id == $user->id)
			//		$participant = true;

				$claimArr = Array(
					'teaser' => $claim->teaser,
					'text' => $claim->text,
					'price' => $claim->price,
					'owner' => $claim->user_id,
                                    'ids'=>$claim->id,
				);
				if (isset($claim->user->company))
					$claimArr['company'] = Array(
						'title' => $claim->user->company->title,
						'id' => $claim->user->company->id
					);

				$claims[] = $claimArr;
			}
			// компания юзера (если есть)
			$userCompId = User::getUserCompanyId();
			// выводим
			$this->renderPartial('claims', compact('participant', 'category', 'claims', 'user'));
			Yii::app()->end();
		}
	}

	public function actionAddClaim()
	{
            if (isset($_POST['commod_cat']))
            {
                $_SERVER['HTTP_REFERER'].='#'.$_POST['commod_cat'];
            }
            if (!Yii::app()->user->isGuest)
            {
		$claim = new CommodClaim();
		$claim->category_id = Yii::app()->request->getPost('commod_cat');
		$claim->user_id = Yii::app()->user->id;
		$claim->price = Yii::app()->request->getPost('commod_price');
		$claim->text = Yii::app()->request->getPost('commod_descr');
			
		if ($claim->save())
			$message = Yii::t('app', 'Qoute added');
		else
			$message = Yii::t('app', 'Qoute not added');
			
		Yii::app()->user->setFlash('message', $message);
            }
		$this->redirect(Yii::app()->request->urlReferrer);
	}

	public function actionRequest()
	{
		$categoryId = intval($_GET['category_id']);
		$category = CommodityCategory::model()->findByPk($categoryId);
		$criteria = new CDbCriteria();
		
	//	$userIds = '';
	//	$users = User::getUsersByCommodCat($categoryId);
	//	if (isset($users)) {
	///		$userIds = Array();
	//		foreach ($users as $user)
	//			$userIds[] = $user->id;
	//		
	//		$userIds = implode(',', $userIds);
	//	}
	//	$companies = Company::model()
	//		->with(Array(
	//			'author'=>Array(
	//				'condition'=>"moderation=1 and author_id IN ('$userIds')"
	//			)))
	//		->findAll();
               
                $c=new CDbCriteria();
                $c->condition='t.moderation=1 and c.category_id='.$categoryId;
                $c->join='INNER JOIN tbl_user u on (u.id=t.author_id) INNER JOIN tbl_commod_cat_user c on (c.user_id=u.id)';
                $companies = Company::model()
			->findAll($c);

		$formModel = new SendRequestForm();
		if (isset($_POST['SendRequestForm'])) {
			$formModel->attributes = $_POST['SendRequestForm'];
                        
                       // $m=new UserMessage();
                       // $m->material_id=$categoryId;
                       // $m->category='Servicesoffer';
                       // $m->id_from=Yii::app()->user->id;
                       // $m->text=$formModel->request;
                        
                        
			if (!Yii::app()->user->isGuest && $formModel->validate()) {
				Yii::import('application.components.MailComponent');
				$mail = new MailComponent();
				$mail->send('sendRequest', Yii::app()->params['adminEmail'], Array(
					'category' => $category->title,
					'request' => $formModel->request
				), Array(), 'Your query is sent');
				// отправка в ЛК
				$request = new CommodityRequest();
				$request->user_id = Yii::app()->user->id;
				$request->commod_cat_id = $categoryId;
				$request->text = $formModel->request;
				$request->save();
                                $formModel->unsetAttributes();
			} else
				Yii::app()->user->setFlash('error', Yii::t('app', 'Query is required'));
		}
		
		$this->render('/default/request', compact(
			'companies',
			'category',
			'formModel'
		));
	}

	public function actionDealNotFit()
	{
		if (Yii::app()->request->isAjaxRequest && !Yii::app()->user->isGuest) {
			
                    $dealFit = DealFit::model()->deleteAll('deal_id=:id and user_id='.Yii::app()->user->id,array(':id'=>$_POST['id']));
			if($dealFit)
				echo 'success';
			
		}
                Yii::app()->end();
	}

	private function setCouponFlag($flag, $val)
	{
		if (Yii::app()->request->isAjaxRequest) {
			$couponDeal = CouponDeal::model()->findByPk($_POST['id']);
			$couponDeal->$flag = $val;
			if ($couponDeal->save())
				echo 'success';
			Yii::app()->end();
		}
	}

	public function actionMarkNotSel()
	{
		$this->setCouponFlag('selected', 0);
	}

	public function actionMarkAsSold()
	{
		$this->setCouponFlag('is_sold', 1);
	}
		
	/**
	 * addToDreamDeal
	 * Раздел «Сделка мечты»
	 * Модуль «Добавить»
	 *
	 * @return void
	 */
	public function actionAddToDreamDeal()
	{
		if (Yii::app()->getRequest()->getIsAjaxRequest() && !Yii::app()->user->getIsGuest()) {
			$dealID = $_POST['id'];
			$dealFit = new DealFit();
			$dealFit->deal_id = $dealID;
			$dealFit->user_id = Yii::app()->user->id;
			
			if ($dealFit->save())
				echo '1'.Yii::t('app', 'Boat added');
			else
				echo '0'.Yii::t('app', 'Boat not added');
		} else
			echo '0'.Yii::t('app', 'Login please');
	}
}