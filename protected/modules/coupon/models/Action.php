<?php
class Action extends ModelWGmap
{
	public $buyFormModel;

	public function tableName()
	{
		return '{{actions}}';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function relations()
	{
		return array(
		//	'commodity'=>array(self::BELONGS_TO, 'Commodity', 'commod_id'),
			'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
			'couponCount'=>array(self::STAT, 'CouponCommodity', 'action_id'),
                    'couponCountUsed'=>array(self::STAT, 'CouponCommodity', 'action_id','condition'=>'used=1'),
			'coupons'=>array(self::HAS_MANY, 'CouponCommodity', 'action_id'),
		);
	}
        
        public function getPrice()
        {
           
           return ($this->price==null || $this->price<=0)?$this->share_price_at_a_discount:$this->price;  
        }
        
/*Value
Discont
You Save*/
        function getVDS() {
            $o=array();
            $o['v']=$this->share_price_without_discount==null?Yii::t ('app', 'Unlimited'):'$'.Yii::app()->controller->numbformat($this->share_price_without_discount);
            $o['d']=$this->discount;
            $o['s']=($this->share_price_without_discount==null)?Yii::t ('app', 'Unlimited'):'$'.Yii::app()->controller->numbformat($this->share_price_without_discount-$this->share_price_at_a_discount);
       return $o;
            }
	
	public function scopes($id=0, $limit=5)
	{
          
	
		return Array(
		
			
			'services' => Array(
				'condition' => "type='Services'",
				'order' => 'expiration_date DESC',
				'limit' => $limit,
			),
			'products' => Array(
				'condition' => "type='Products'",
				'order' => 'expiration_date DESC',
				'limit' => $limit,
			),
		);
	}
        
        public function random($id=null,$limit=6) {
            $this->getDbCriteria()->mergeWith(array(
       'condition' => $id!=null ? "t.id <> $id" : '',
				'order' => 'RAND()',
				'limit' => $limit,
    ));
    return $this;
        }


        public function getActive()
	{
		return (
			strtotime($this->expiration_date) > date("U")
		&& $this->couponCount < $this->coupons_max
		);
			
	}
	
	public function getTimeLeft()
	{
		return Date::getTimeLeft($this->expiration_date);
	}
	
	public function rules()
	{
		return array(
			array('slogan,type, info,title, discount, coupons_max, google_map', 'required'),
			array('coupons_max', 'numerical', 'integerOnly'=>true),
                    array('google_map', 'googlemap'),
                    array('share_price_at_a_discount',  'numerical','min'=>1,'allowEmpty'=>true),
                         array('share_price_without_discount',  'numerical','allowEmpty'=>true,),
                    
                           array('share_price_at_a_discount', 'sliskd'),
			array('share_price_at_a_discount, discount, price', 'numerical'),
			array('expiration_date,expiration_date1,about, photo,seller_info, start', 'safe'),
			array('id, photo, slogan, info, expiration_date, share_price_without_discount, share_price_at_a_discount, discount, price, coupons_max, google_map, start', 'safe', 'on'=>'search'),
		);
	}
        
        public function sliskd($param,$param2) {
            if ($this->share_price_at_a_discount!='' && $this->share_price_without_discount!='')
            {
         if   ($this->share_price_without_discount<$this->share_price_at_a_discount)
             $this->addError($param,Yii::t ('app', 'must be less than'));
            }
        }





        

	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'ID'),
			//'commod_id' => Yii::t('app', 'Goods'),
			'photo' => Yii::t('app', 'Photo'),
			'info' => Yii::t('app', 'Fine Print'),
			'slogan' => Yii::t('app', 'Deal slogan'),
			'expiration_date' => Yii::t('app', 'Expiration date of deal'),
                        'expiration_date1' => Yii::t('app', 'Coupon expiration date'),
			'start' => Yii::t('app', 'Start'),
			'price' => Yii::t('app', 'Price coupon'),
			'share_price_without_discount' => Yii::t('app', 'The share price without discount'),
			'share_price_at_a_discount' => Yii::t('app', 'The share price at a discount'),
			'discount' => Yii::t('app', 'Discount').' %',
			'seller_info' => Yii::t('app', 'Seller information (address & phone number)'),
			'coupons_max' => Yii::t('app', 'The maximum number of coupons'),
			'google_map' => Yii::t('app', 'Google map'),
                  
                  
			//'company_id' => Yii::t('app', 'Seller'),
                    'title'=> Yii::t('app', 'Name'),
                    'type'=>Yii::t('app', 'Type'),
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		
		$criteria->compare('id',$this->id);
		//$criteria->compare('commod_id',$this->commod_id);
		$criteria->compare('photo',$this->photo);
		$criteria->compare('info',$this->info);
		$criteria->compare('expiration_date',$this->expiration_date);
                $criteria->compare('expiration_date1',$this->expiration_date1);
                
		$criteria->compare('start',$this->start);
		$criteria->compare('price',$this->price);
		$criteria->compare('share_price_without_discount',$this->share_price_without_discount);
		$criteria->compare('share_price_at_a_discount',$this->share_price_at_a_discount);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('coupons_max',$this->coupons_max);
		$criteria->compare('slogan',$this->slogan);
		$criteria->compare('google_map',$this->google_map);
                $criteria->compare('type',$this->type);
                
                
		//$criteria->compare('company_id',$this->company_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
                        'pagination'=>array(          'pageSize'=>isset($_GET['itemcount'])?$_GET['itemcount']:20,

      )
		));
	}
	
	/*public function afterFind()
	{
		$this->info = str_replace("\n", "<br/>", $this->info);
		$this->about = str_replace("\n", "<br/>", $this->about);
	}*/
	
	public static function checkAvailability($actionId)
	{
		$action = self::model()
			->with('couponCount')
			->with('coupons')
			->findByPk($actionId);
		
		if ($action->coupons_max <= $action->couponCount)
			return Array(
				'result' => 'error',
				'msg' => Yii::t('app', 'There are not any available coupons'),
			);
		else if (strtotime($action->expiration_date) < time())
			return Array(
				'result' => 'error',
				'msg' => Yii::t('app', 'Coupon is exceeded'),
			);
		else
                    if ($action->user_id==Yii::app ()->user->id)
                    {
                        return Array(
				'result' => 'error',
				'msg' => Yii::t('app', 'you can not buy, because it is your service'),
			);
                        
                    }  else
			return Array(
				'result' => 'ok',
				'msg' => Yii::t('app', 'Coupon is available'),
			);			
	}
	
	public static function createCoupon($type, $actionId, $expirationDate, $userId=0, $isGift=0,$count=1,$data=array())
	{
         $ids=null;   
	for ($i=1;$i<=$count;$i++)
        {
            $coupon = new CouponCommodity();
            
            foreach ($data as $k=>$value) {
                $coupon->$k=$value;
            }
		$coupon->type = $type;
		$coupon->action_id = $actionId;
		$coupon->expiration_date = $expirationDate;
		$coupon->user_id = $userId;
		$coupon->number = '';
		
		$coupon->code = Coupon::getNewCode();
		$coupon->is_gift = $isGift;
               
		
		if ($coupon->save())
                    $ids[]=$coupon->primaryKey;
		else 
                {
                print_r($coupon->getErrors());
                exit();
                    return null;
                }
        }
        return $ids;
	}
        
        public static function sendcuppons($email, $cupoms, $frend=null)
	{
            $params=array();
            if ($frend!=null)
                
            { $email=$frend['email'];
            $params['sender']=$frend['sender'];
            $params['message']=$frend['message'];
            $params['receiver']=$frend['receiver'];
             $t='couponGift';
            } else 
            {
                return 0;
                $t='coupons';
            }
            
$k=CouponCommodity::model()->findAllByPk($cupoms);
if (isset($k[0]))
{
$act=$k[0]->action;
$params['link']=  CHtml::link($act->title, Yii::app()->createAbsoluteUrl('coupon/default/action',array('action_id'=>$act->id)));
}

$code=array();
foreach ($k as $value) {
   $code[]= $value->code;
}
$params['coupons']=  implode(', ', $code);
$params['count']=count($code);


  $tp = new TemplateParams();
		$tp->setSubjectParam('sitename', Yii::app()->params->companyName);
		$tp->setBodyParams($params);
		$mt = MailTemplate::parseTemplate($t, $tp);
		MailSender::sendTemplate($email, $mt);
                
                
             
           
        }
        
        
        
        
                     public function behaviors()
	{
		return array(
                        'imgajaxloa'=>array('class'=>'ext.imgajaxload.modelBehavior',
                            'paramsimg'=>array(
                          'image'=>   array('fildname'=>'photo'),
                             /*  array(
          'files'=>array('class'=>'DealPhotos','fildname'=>'file','fk'=>'deal_id','max'=>50,
                // 'condition'=>"entity='User'",
              'postparam'=>array('name'),
            //  'dopparams'=>array('name'=>'name'),
              
              ),
                                   )*/
                                
                                ),
                           
				));
	}
        
        
        
        
        
             protected function beforeValidate() {

$t=time();

$expiration_date=  strtotime($this->expiration_date);
$expiration_date1=  strtotime($this->expiration_date1);

if ($expiration_date<$t)  $this->addError('expiration_date',  Yii::t('app','date is less than the current date'));
if ($expiration_date1<$t)  $this->addError('expiration_date1',  Yii::t('app','date is less than the current date'));
if ($expiration_date>$expiration_date1)  $this->addError('expiration_date',  Yii::t('app','must be less than Coupon expiration date'));

        
          if (!isset($_POST['iimage']) && $this->getScenario()!='upatr')
                $this->addError('photo',  Yii::t('app','file not loaded'));
                   
           
          
            return parent::beforeValidate();
          
        }
        
        protected function beforeSave() {
            if ($this->share_price_without_discount==null || $this->share_price_at_a_discount==null)
            {
                $this->share_price_without_discount=new CDbExpression('NULL');
                $this->share_price_at_a_discount=new CDbExpression('NULL');
            

            } else
            {
          $this->discount=100-round($this->share_price_at_a_discount/($this->share_price_without_discount/100) );
            }
            return parent::beforeSave();
        }
        
        
        
        
        
         public function tomodredation()
        {
            self::model()->updateByPk($this->id, array('moderation'=>0));
            
               $tp = new TemplateParams();
		$tp->setSubjectParam('sitename', Yii::app()->params->companyName);
                
		$tp->setBodyParams(array(
						'id'=>$this->id,
                   
				'host'=>$_SERVER['HTTP_HOST'],	
					));
                
		$mt = MailTemplate::parseTemplate('moderatortoadminUsluga', $tp);
		MailSender::sendTemplate(Config::model()->getValue('adminEmail'), $mt);
        }
       
        
}