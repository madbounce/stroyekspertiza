<?php
class CommodClaim extends CActiveRecord
{
	private $teaserLength = 300;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return NoticeCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{commod_claims}}';
	}
	
	public function relations()
	{
		return array(
			'commodity_cat'=>array(self::BELONGS_TO, 'CommodityCategory', 'category_id'),
			'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}
	
	public function getTeaser()
	{
		return StringHelper::getTeaser($this->text, $this->teaserLength);
	}
	
	public function rules()
	{
		return array(
			array('price, text', 'required'),
			array('price', 'length', 'max'=>20),
			array('text', 'length', 'max'=>255),
                    array('approved', 'safe', 'on'=>array('admin')),
			array('price, text, category_id, company_id', 'safe'),
			array('id, price, text', 'safe', 'on'=>'search'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'price' => Yii::t('app','Price'),
			'text' => Yii::t('app','Text'),
			'approved' => Yii::t('app','Approved')
		);
	}

	public function scopes()
	{
		return Array(
			'approved' => Array(
				'condition' => "t.approved = '1'"
			),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('price', $this->price, true);
		$criteria->compare('text', $this->text, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
