<?php
class Commodity extends CActiveRecord
{
	public function tableName()
	{
		return '{{commodities}}';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
		
	public function relations()
	{
		return array(
			'company'=>array(self::MANY_MANY, 'Company', '{{actions}}(commod_id, company_id'),
			'category'=>array(self::BELONGS_TO, 'CommodityCategory', 'category_id'),
			'action'=>array(self::HAS_MANY, 'Action', 'commod_id'),
		);
	}
	
	public function scopes()
	{
		return Array(
			'service' => Array(
				'condition' => "commodity.type = 'services'",
			),
			'product' => Array(
				'condition' => "commodity.type = 'products'",
			),
		);
	}

	/*public function getPicture()
	{
		return $this->getImage('picture_id');
	}

	public function getPreview()
	{
		return $this->getImage('prew_id');
	}

	private function getImage($column)
	{
		$picture = (object)Array(
			'id' => '',
			'file' => '/images/commodity/noimage.jpg',
		);
		if (isset($this->picture_id)) {
			$file = File::model()->findByPk($this->$column);
			if (isset($file))
				$picture = $file;
			if (substr($picture->file,0,1)!='/')  // костыль
				$picture->file = '/' . $picture->file;
		}
	
		return $picture;
	}*/

	public function getPicture()
	{
		$picture = (object)Array(
			'id' => '',
			'file' => '/images/commodity/noimage.jpg',
		);
		if (isset($this->picture_id)) {
			$file = File::model()->findByPk($this->picture_id);
			if (isset($file))
				$picture = $file;
			if (substr($picture->file,0,1)!='/')  // костыль
				$picture->file = '/' . $picture->file;
		}
	
		return $picture;
	}

	public static function getList()
	{
		$commoditiesArr = Array();
		$commodities = Commodity::model()->findAll();
		foreach ($commodities as $commodity)
			$commoditiesArr[$commodity->id] = $commodity->title;
	
		return $commoditiesArr;
	}
	
	public function afterFind()
	{
		$this->about = str_replace("\n", "<br/>", $this->about);
	}
}