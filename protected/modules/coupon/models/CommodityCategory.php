<?php
class CommodityCategory extends CActiveRecord
{
	public function tableName()
	{
		return '{{commodity_category}}';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function relations()
	{
		return array(
			'commodities'=>array(self::HAS_MANY, 'Commodity', 'category_id'),
			'users'=>array(self::MANY_MANY, 'User', '{{commod_cat_user}}(category_id, user_id)'),
		);
	}
	
	public function defaultScope()
	{
		return array(
			'order' => 'title ASC',
		);
	}

	public function rules()
	{
		return array(
			array('title', 'required'),
			array('logo', 'file'),
			array('text, about, logo,sort', 'safe')
		);
	}
        
        protected function beforeFind() {
           
            $this->getDbCriteria()->order='sort ASC';
            parent::beforeFind();
        }

	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'ID'),
			'about' => Yii::t('app', 'About'),
			'title' => Yii::t('app', 'Title'),
			'logo' => Yii::t('app', 'Logo'),
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('about', $this->about, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
