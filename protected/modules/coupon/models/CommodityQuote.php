<?php
class CommodityQuote extends CActiveRecord
{
	private $teaserLength = 300;
	
	public function tableName()
	{
		return '{{commodity_quote}}';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
		
	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'user', 'user_id'),
			'request' => array(self::BELONGS_TO, 'CommodityRequest', 'request_id'),
		);
	}

	public function rules()
	{
		return array(
			array('cost', 'required'),
			array('cost', 'numerical','min'=>1),
			array('cost', 'length', 'max'=>20),
			array('cost, text, user_id, request_id', 'safe'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'cost' => Yii::t('app','Cost'),
			'user' => Yii::t('app','User'),
			'text' => Yii::t('app','Text'),
			'approved' => Yii::t('app','Approved')
		);
	}

	public function getCompany()
	{
		return $this->user->company;
	}

	public function getTeaser()
	{
		return StringHelper::getTeaser($this->text, $this->teaserLength);
	}
	
	public function afterFind()
	{
		$this->text = str_replace("\n", "<br/>", $this->text);
	}
	

	public function search()
	{
		$criteria=new CDbCriteria;
		
		$criteria->compare('id', $this->id);
		/*$criteria->compare('request_id', $this->request_id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('cost', $this->cost);*/
//		$criteria->compare('text', $this->text);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}