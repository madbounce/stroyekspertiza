<?php
class CommodityRequest extends CActiveRecord
{
	private $teaserLength = 100;
	
	public function tableName()
	{
		return '{{commodity_request}}';
	}
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function relations()
	{
		return array(
			'user'  => array(self::BELONGS_TO, 'User', 'user_id'),
			'commod_cat'=>array(self::BELONGS_TO, 'CommodityCategory', 'commod_cat_id'),
			'quotes'=>array(self::HAS_MANY, 'CommodityQuote', 'request_id'),
		);
	}
	
	public function getTeaser()
	{
		return StringHelper::getTeaser($this->text, $this->teaserLength);
	}

	public function afterFind()
	{
		$this->text = str_replace("\n", "<br/>", $this->text);
	}

	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'ID'),
			'commod_cat' => Yii::t('app', 'Goods type'),
		);
	}
	
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('commod_cat_id', $this->commod_cat_id);
		$criteria->compare('text', $this->text);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
