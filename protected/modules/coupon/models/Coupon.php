<?php
class Coupon extends CommonRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function scopes()
	{
		$userId = User::getUserId();
		return Array(
			'own' => Array(
				'condition' => "t.user_id = '$userId'"
			),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'ID'),
			'type' => Yii::t('app', 'Type'),
			'user_id' => Yii::t('app', 'User'),
			'created' => Yii::t('app', 'Created'),
                    'expiration_date'=>Yii::t('app', 'Coupon expiration date')
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.user_id',$this->user_id);
                $criteria->compare('t.action_id',$this->action_id);
                
               $x=true;
                if (isset($_GET['expiration_date1']) && !empty($_GET['expiration_date1']))
                {
                   $criteria->addCondition ('expiration_date>=\''.mysql_escape_string ($_GET['expiration_date1'])."'");
                $x=false;
                   
                }
                
                
                if (isset($_GET['expiration_date2']) && !empty($_GET['expiration_date2']))
                {
                   $criteria->addCondition ('expiration_date<=\''.mysql_escape_string ($_GET['expiration_date2'])."'");
                $x=false;
                  
                }
                if ($x)
                 $criteria->compare('expiration_date',$this->expiration_date);
                
               if ((isset($_GET['action_date1']) && !empty($_GET['action_date1']))
                   ||
                   (isset($_GET['action_date2']) && !empty($_GET['action_date2'])))
                   
               {
                   $criteria->join="inner join tbl_actions a on (a.id=t.action_id)";
                     if (isset($_GET['action_date1']) && !empty($_GET['action_date1']))
                     {
                           $criteria->addCondition ('a.expiration_date>=\''.mysql_escape_string ($_GET['action_date1'])."'");
                           
                     }
                     
                     if (isset($_GET['action_date2']) && !empty($_GET['action_date2']))
                     {
                           $criteria->addCondition ('a.expiration_date<=\''.mysql_escape_string ($_GET['action_date2'])."'");
                           
                     }
                     $criteria->select='t.*';
                     $criteria->group='t.id';
                   
               }
               
               
                 if (isset($_GET['usemail']) && !empty($_GET['usemail']))
                {
                   $criteria->addCondition ('u.email=\''.mysql_escape_string ($_GET['usemail'])."'");
             $criteria->join.=' inner join tbl_user u on (t.user_id=u.id)';
               $criteria->group='t.id';
               $criteria->select='t.*';
                }
		
$criteria->compare('t.code',$this->code);
$criteria->compare('t.is_gift',$this->is_gift);
$criteria->compare('t.created',$this->created);
$criteria->compare('t.used',$this->used);


$criteria->compare('t.idpayments',$this->idpayments);

$criteria->compare('t.type',$this->type);


$criteria->order='t.id desc';
		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
                    
                    'pagination'=>array(
          'pageSize'=>isset($_GET['itemcount'])?$_GET['itemcount']:20,
      )
		));
                
                
                
	}

	public function belongs($userId)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => "t.user_id = '$userId'"
		));
		
		return $this;
	}

	public static function getNewCode()
	{
		$codeLength = 6;
		$a = '1234567890ABCDEFGHIJKLMNPQRSTUVWXYZ';
              
                do
                {
		$code = '';
		for ($i = 0; $i < $codeLength; $i++)
			$code .= $a[rand(0, strlen($a) - 1)];
                } while (CouponCommodity::model()->exists("code='".$code."'"));
		
		/*foreach ($coupons as $coupon)
			if ($coupon->code == $code)
				return self::getNewCode($coupons);*/
		
		return $code;
	}
}