<?php
class CouponCommodity extends Coupon
{
	public function tableName()
	{
		return '{{coupon_commodity}}';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function relations()
	{
		return array(
			'user'  => array(self::BELONGS_TO, 'User', 'user_id'),
			'action'  => array(self::BELONGS_TO, 'Action', 'action_id'),
		);
	}
        
        protected function beforeFind() {
            $c=new CDbCriteria;
            $this->getDbCriteria()->mergeWith(array('join'=>'inner join tbl_actions a on(a.id=t.action_id)'));
        }

	public function rules()
	{
		return array(
			array('user_id, action_id, type', 'required'),
			array('user_id, action_id, is_gift', 'numerical', 'integerOnly'=>true),
			array('created, is_gift, type, action_id, code', 'safe'),
			array('id,used,user_id,expiration_date,is_gift,action_id,type,idpayments,created,code', 'safe', 'on'=>'search'),
		);
	}
        
      

}