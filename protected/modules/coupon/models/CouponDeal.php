<?php
class CouponDeal extends Coupon
{
	public function tableName()
	{
		return '{{coupon_deal}}';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		return array(
			array('user_id, type', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('created, type', 'safe'),
			array('id, user_id, created', 'safe', 'on'=>'search'),
		);
	}
	
	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'deal' => array(self::BELONGS_TO, 'Deal', 'deal_id'),
		);
	}
	
	public function getYear()
	{
		return $this->deal->year;
	}
}
