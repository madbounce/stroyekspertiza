<?php
/**
 * Подходящие для клиента лодки
 */
class DealFit extends CActiveRecord
{
     public function rules()
	{
		return array(
			array('user_id,deal_id', 'required'),
		);
	}
        
	public function tableName()
	{
		return '{{deals_fit}}';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function relations()
	{
		return array(
			'deal' => array(self::BELONGS_TO, 'Deal', 'deal_id'),
			'user'  => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		
		$criteria->compare('deal_id', $this->deal_id);
		$criteria->compare('user_id', $this->user_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	
	public function scopes()
	{
		$userId = User::getUserId();
		return Array(
			'own' => Array(
				'condition'=>"t.user_id = '{$userId}'"
			),
		);
	}
}