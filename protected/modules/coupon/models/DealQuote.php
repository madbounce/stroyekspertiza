<?php
class DealQuote extends CommonRecord
{
	public $email;

	public function tableName()
	{
		return '{{deal_quotes}}';
	}

        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		return array(
			//array('phone', 'required'),
			array('phone', 'length', 'max'=>20),
			array('phone', 'safe', 'on'=>'search'),
			array('deal_id, phone', 'safe'),
			array('phone', 'match', 'allowEmpty'=>true, 'pattern'=>'/^[0-9\w\-]+$/', 'message'=>Yii::t('app', 'Incorrect phone')),
			//array('email', 'match', 'pattern'=>'/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/', 'message'=>Yii::t('app', 'Incorrect email')),
		);
	}

	public function relations()
	{
		return array(
			'deal' => array(self::BELONGS_TO, 'Deal', 'deal_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}
        
        public function attributeLabels() {
            return array('phone'=> Yii::t('app', 'Enter your phone number'),
                
                );
        }
       
        
        

	public function search()
	{
		$criteria=new CDbCriteria;
		
		
		$criteria->compare('phone', $this->phone);
		//$criteria->compare('email', $this->email);
		$criteria->compare('deal_id', $this->deal_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
        
        protected function beforeSave() {
           //  $x=self::model()->deleteAll('user_id=:us and deal_id=:d',array(':us'=>$this->user_id,':d'=>$this->deal_id));
             return parent::beforeSave();
        }
        
        public static function proverk($idd)
        {
            $d=Deal::model()->findByPk($idd,'buyer_id  is not null and purchased=0');
            if ($d!=null)
            {
                if (self::model()->exists('deal_id='.$d->id.' and user_id='.$d->buyer_id.' and purchased=1'))
                {
                    $d->purchased=1;
                     $d->setScenario('upatr');
                    if ($d->save())
                    {
                    //отправка письма
                     $tp = new TemplateParams();
		$tp->setSubjectParam('sitename', Yii::app()->params->companyName);
                
		$tp->setBodyParams(array(
						
						'id'=>$d->id,
                    'url'=>'http://'.$_SERVER['HTTP_HOST'].'/admin/deal/scores',
					));
                
		$mt = MailTemplate::parseTemplate('toadmindealquotes', $tp);
		MailSender::sendTemplate(Config::model()->getValue('adminEmail'), $mt);
                        
                    
                    }
                }
            }
        }
}