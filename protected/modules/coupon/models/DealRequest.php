<?php
class DealRequest extends CActiveRecord
{
	public function tableName()
	{
		return '{{deal_requests}}';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function init()
	{
		$this->condition = 'New';
	}
        
        protected function beforeSave() {
            $this->user_id=Yii::app()->user->id;
            return parent::beforeSave();
        }
        
        
        
           public function sendadmin()
        {
            
            
               $tp = new TemplateParams();
		$tp->setSubjectParam('sitename', Yii::app()->params->companyName);
                
                $s=$this->getAttributes();
                $txt='';
                foreach ($s as $key => $value)
                 if ($value!='')
                     $txt.=$this->getAttributeLabel($key).': '.$value.'</br>';
                 
                
                
		$tp->setBodyParams(array(
						'text'=>$txt,
					
					));
                
		$mt = MailTemplate::parseTemplate('dreamdeals', $tp);
		MailSender::sendTemplate(Config::model()->getValue('adminEmail'), $mt);
        }
        
        
        protected function afterSave() {
            
           $this->sendadmin(); 
           // dreamdeals
            parent::afterSave();
        }
	
	public function rules()
	{
		return array(
			array('user_id,model', 'required'),

			array('comments', 'length', 'max'=>255),
			array('price_from, price_to', 'length', 'max'=>20),
			array('year_from, year_to', 'length', 'max'=>4),
			array('price_from, price_to, length_from, length_to, year_from, year_to, model, type, user_id, location,manufacturer_id', 'safe', ),
			array('id, user_id, manufacturer_id, model', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'user'  => array(self::BELONGS_TO, 'User', 'user_id'),
			//'type' => array(self::BELONGS_TO, 'DealType', 'type'),
			'manufacturer'  => array(self::BELONGS_TO, 'DealManufacturer', 'manufacturer_id'),
			//'model' => array(self::BELONGS_TO, 'DealModel', 'model'),
		);
	}

	public function defaultScope()
	{
		$userId = User::getUserId();
		return array(
			'with' => Array('user'=>Array(
				'condition'=>"user.id = '{$userId}'"
			))
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('manufacturer_id',$this->manufacturer_id,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app', 'ID'),
			'type' => Yii::t('app', 'Type'),
			'manufacturer_id' => Yii::t('app', 'Manufacturer'),
			'model' => Yii::t('app', 'Model'),
			'location' => Yii::t('app', 'My location'),
			'comments' => Yii::t('app', 'Comments'),
		);
	}
	
	public function getConditions()
	{
		$conditions = Array('New', 'Used', 'Both');
		$conditionsList = Array();
		foreach ($conditions as $condition)
			$conditionsList[$condition] = Yii::t('deal', $condition);
			
		return $conditionsList;
	}
	
	public function getTypes()
	{
		$types = DealType::model()->findAll();
		$typesList = Array();
		foreach ($types as $type)
			$typesList[$type->id] = $type->title;

		return $typesList;
	}
	
	public function getManufacturers()
	{
           return CHtml::listData(DealManufacturer::model()->findAll(), 'id', 'title');
		/*$manufacturers = DealManufacturer::model()->findAll();
		$manufactList = Array();
		foreach ($manufacturers as $manufact)
			$manufactList[$manufact->id] = $manufact->title;

		return $manufactList;*/
	}
	
	public function getModels($manufacturer)
	{
		$criteria = new CDbCriteria();
		$criteria->with = Array('manufacturer'=>Array(
			'condition'=>"manufacturer_id = '$manufacturer'"
		));
		$models = DealModel::model()->findAll($criteria);
		$modelsList = Array();
		foreach ($models as $model)
			$modelsList[$model->id] = $model->title;

		return $modelsList;
	}
}