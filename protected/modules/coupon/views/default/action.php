<div class=""><div class="main-offer-bl">
<?php 


                
extract($data);

$this->pageTitle = Yii::t('profile', $action->title);

$this->widget('application.components.exbreadcrumbs.EXBreadcrumbs', array(
	'links'=>array(
		Yii::t('profile', 'Current deals') => Yii::app()->createUrl('deal/default/CurrentDeals'),
		$this->pageTitle => Yii::app()->request->url
	),
));


?>
<div class="blue_line"></div>
<ul class="deal-options">
    
    	<!--li class="s-option print-v"><a target="_blank" href="<?php echo Yii::app()->createUrl('coupon/default/action',array('action_id'=>$action->id,'layout'=>'print'));?>"><?php echo Yii::t('app', 'Print version');?></a></li>
	<li class="s-option pdf-v"><a target="_blank" href="<?php echo Yii::app()->createUrl("coupon/default/pdf",array('action_id'=>$action->id));?>"><?php echo Yii::t('app', 'PDF version');?></a></li-->
	<li class="s-option email-share"><a href="javascript:void(0);"><?php echo Yii::t('profile', 'Share with email');?></a></li>
	<li class="s-option ask-quest"><a href="javascript:void(0);"><?php echo Yii::t('profile', 'Ask seller a question');?></a></li>
</ul>

<div class="social-widgets">
                <? $this->widget('WLikes'); ?>
</div>

<div class="deal-media-bl">
	<?php
        echo CHtml::image($action->getsrcone(460,'auto'),'',array('class'=>'deal_servise_pr_photo'));

        
        ?>
	<!-- Блок «еще Услуги/Товары» -->
	<?php $this->widget('coupon.components.ActionsWidget', array('actionID'=>$action->id));?>
</div>
<div class="deal-content-bl deal_content_product" style="margin-left: 12px; width: 486px;">
    <h1 style="margin-bottom: -7px !important"><?php echo CHtml::encode($action->title);?></h1>
    <h3 class=""><?php echo CHtml::encode($action->slogan);?></h3>
	<div class="edit_deal_do">
		<div class="buy_product">
			<?php if ($available['result']=='ok') {?>
                    <input value="<?php echo Yii::t('profile', 'BUY!');?>" name="send-quest" class="send-btn" onclick="window.location.href='<?=Yii::app()->createUrl("/user/payment/pay/action/{$action->id}/gift/0");?>'">
                    
			
			<?php } else {
                            if ($action->user_id==Yii::app()->user->id)
                            {
                                ?>
                    <input value="<?php echo Yii::t('profile', 'BUY!');?>" name="send-quest" class="send-btn" onclick="message('<?=Yii::t('app', 'You can not buy your')?>','')">
                    
                    <?php
                            } else {
                            
                            ?>
                       <input value="<?php echo Yii::t('profile', 'SOLD OUT');?>" name="send-quest" class="send-btn send-btn-noactive" > <!--  onclick="message('<?php echo $available['msg'];?>','<?php echo $available['result'];?>'); return false"  -->
                     

                        
                        
			
			<?php }} ?>
			<span class="price_pr">$ <?php echo Yii::app()->controller->numbformat($action->getPrice())?></span>
		</div>
            <?php 
            if ($action->active) { ?>
            
		<a class="buy_for_fiend" href="javascript:void(0);"><?php echo Yii::t('profile', 'Buy it for a friend!');?></a>
		<?php } ?>
                <hr/>
		<div class="deal-timer">
			<span class="lblue"><?php echo Yii::t('app', 'Time Left To Buy');?></span>
			<span id="timeraction" class="timer">
			<?php                                     $timeLeft = $action->timeLeft;
			
                        
                        echo CHtml::tag('span', array('class'=>'days'), $timeLeft->days, true)   . ' ' . Yii::t('profile', 'days') . ' ' .
			CHtml::tag('span', array('class'=>'hours'), $timeLeft->hours, true)  . ':' . CHtml::tag('span', array('class'=>'mins'), $timeLeft->mins, true). ':' . CHtml::tag('span', array('class'=>'s'), $timeLeft->s, true);?>
			</span>
		</div>
                <script type="text/javascript">
               $('#timeraction').starttime(); 
            </script>
	</div>
	<div class="param_deal_do">
            <?php 
            $dd=$action->getVDS();
            ?>
		<div class="price_variant">
			<div class="lblue"><?php echo Yii::t('profile', 'Value');?></div>
			<div class="gray"><?php  
                        echo $dd['v'] ?></div>
		</div>
		<div class="price_variant">
			<div class="lblue"><?php echo Yii::t('profile', 'Discont');?></div>
			<div class="gray"><?php echo $dd['d']  ?>%</div>
		</div>
		<div class="price_variant">
			<div class="lblue"><?php echo Yii::t('profile', 'You Save');?></div>
                        
			<div class="gray"><?php
                        
                        echo $dd['s'];
                        
                        ?></div>
		</div>
		<div class="lblue sz20"><?php echo $action->couponCount . Yii::t('profile', ' bought');?></div>
		<div class="gray"><?php echo Yii::t('profile', 'Limited quantity available');?></div>
		<?php if ($action->active):?>
		<!--div class="ok_block"><?php echo Yii::t('profile', 'The deal is on!');?></div-->
		<?php endif; ?>
	</div>
	<ul class="tabs">
		<li class="itm"><a class="link" href="#about-tab"><?php echo Yii::t('profile', 'About');?></a></li>
		<li class="itm"><a class="link" href="#tech-inf-tab"><?php echo Yii::t('profile', 'Fine Print');?></a></li>
		<li class="itm"><a class="link" href="#seller-tab"><?php echo Yii::t('profile', 'Seller');?></a></li>
		<li class="itm"><a class="link" href="#quest-tab"><?php echo Yii::t('profile', 'Questions');?></a></li>
	</ul>
	<div class="tab_container">
		<div id="about-tab" class="tab_content">
			
			<p><?php echo nl2br(CHtml::encode($action->about));?></p>
		</div>
		<div id="tech-inf-tab" class="tab_content">
			<p><?php echo nl2br(CHtml::encode($action->info));?></p>
		</div>
		<div id="seller-tab" class="tab_content">
			<h2><?php echo Yii::t('profile', 'Seller information');?></h2>
			<p><?php echo nl2br(CHtml::encode($action->seller_info));?></p>
                        
                        
			<?php 
                        $data=$action->getMapData();
                        if (!empty($data)) :
                       
                            
                           $this->widget('GoogleMap',array('x'=>$data['x'],'y'=>$data['y'],'text'=>isset($data['txt'])?$data['txt']:null,
                               'htmlOptions'=>array('style'=>'width:425px;height:350px')
                               )
                               
                               );
                            ?>

			<?php 
                        
                        endif; ?>
		</div>
		<div id="quest-tab" class="tab_content">
			<h2>Question and Comments from our members</h2>
			
			<?php $this->widget('application.components.messages.Questions', array(
				'category' => 'Productsandservices',
				'materialId' => $action->id,
				'view' => 'questions',
                             'sellerId'=>$action->user_id
			));?>
			<?php
                        
                        
                        
                        
                         $this->widget('Wsendmessage',array('type'=>'Productsandservices','material_id'=>$action->id));
                         
                        
                      //  $this->widget('application.components.messages.LeaveMessage', array(
			//	'view'  => 'action',
			//	'category' => 'action',
			//	'materialId' => $action->id,
			//	'idTo' => $action->user->id,
			//	'idFrom' => $idFrom,
		//	));
                         
                         ?>
		</div>
		<?php $this->widget('application.components.messages.QuestionPopup');?>
	</div>
</div></div></div>
<!-- поп-ап "послать линк (Share with email)" -->
<?php $this->widget('application.components.popups.SendLink', array(
	'formModel' => $sendLinkFormModel,
));?>
<!-- поп-ап "покупка купона для друга" -->
<?php $this->widget('application.components.popups.BuyForFriend', array(
	'formModel' => $buyForFriendModel,
	'action' => $action,
));?>
