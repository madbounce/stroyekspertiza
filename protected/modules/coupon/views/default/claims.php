<h4><?php echo $category['title'];?></h4>
<p class="tab-desc"><?php echo $category['about'];?></p>
<a href="javascript:void(0);" class="quotes-sort<?php if (isset($_POST['order']) && $_POST['order']=='DESC') echo ' DESC1'; ?>"><?php echo Yii::t('app', 'Sort by price');?></a>
<?php if (isset(Yii::app()->request->cookies['user_type']) && Yii::app()->request->cookies['user_type']->value == 'seller'):?>
<a href="javascript:void(0);" class="add-quote<?php if (!$participant){?> hidden<?php }?>"><?php echo Yii::t('app', 'Add quote');?></a>
<?php endif;
foreach ($claims as $claim) {
	$company = $claim['company'];
	$companyId = $company['id']; ?>
<div class="s-quote">
	<h5><?php echo CHtml::encode($company['title']);?><span class="q-offer">$ <?php echo $claim['price'];?></span><a href="javascript:void(0);" tag="<?php echo $claim['ids'];?>" class="send-message"><?php echo Yii::t('app', 'Send message');?></a></h5>
        <p id="teaser<?php echo $claim['ids'];?>"><?php echo nl2br(CHtml::encode($claim['text']));?></p>
	<div>
            <?php 
            if (UserMessage::model()->exists("material_id=".$claim['ids']." and category='Services' and public=1 and  approved=1"))
            {
            ?>
		<a href="javascript:void(0);" class="qa" id="<?php echo $claim['ids'];?>"><?php echo Yii::t('app', 'Q&A');?></a>
                <?php } ?>
                <a style="float:right" tag="teaser<?php echo $claim['ids'];?>" href="javascript:void(0);" class="more1" id="<?php echo $claim['ids'];?>"><?php echo Yii::t('profile', 'More');?></a>
	</div>
</div>
<!-- поп-ап «Вопросы» -->
<div class="q-a-popup hidden qqq<?php echo $claim['ids'];?>">
<a href="javascript:void(0);" id="<?php echo $claim['ids'];?>" class="popup-close"></a>
<?php $this->widget('application.components.messages.Questions', array(
	'category' => 'Services',
	'materialId' => $claim['ids'],
	'view' => 'questions',
));?>
</div>
<?php }?>
<!-- поп-ап "Добавить предложение (add-quote)" -->
<?php $popupId = 'addClaim';?>
<div class="common-popup hidden <?php echo $popupId;?>">
	<a href="javascript:void(0);" class="popup-close send"></a>
	<form style="text-align: left !important;" method=post id='<?php echo $popupId;?>' action='<?php echo Yii::app()->createUrl("/coupon/default/addClaim/");?>'>
		<label for='commod_descr'><?php echo Yii::t('app', 'Your description');?>: </label><br/><textarea name='commod_descr' /><br /><label for='commod_price'><?php echo Yii::t('app', 'Your price');?>: </label><br/><input type="text" name='commod_price' />
		<!--<input name='company_id' type='hidden' value='<?php //echo $userCompId;?>' />-->
		<input name='commod_cat' type='hidden' value='<?php echo $category['id'];?>' />
	</form>
	<div class="one_line profile">
		<input type="submit" value="<?php echo Yii::t('app', 'Add');?>" class="act-submit-btn <?php echo $popupId;?>">
	</div>
</div>
<script language="JavaScript">
	// открываем диалог. окно
	$('.add-quote').click(function(){
            var bl=$('.common-popup.<?php echo $popupId;?>');
		
                
                   bl.css('left',($(this).position().left-45)+'px');     
                 bl.css('top',($(this).position().top+30)+'px');  
                bl.removeClass('hidden');
		$('#shadow').removeClass('hidden');
	});
	// сабмитим форму
	$('.act-submit-btn.<?php echo $popupId;?>').click(function(){
		$('.common-popup.<?php echo $popupId;?> form').submit();
		$('#shadow').addClass('hidden');
	});
        
        $('.more1').setmore({maxlength:80,more:'<?=Yii::t('app','More')?>',hide:'<?=Yii::t('app','Hide')?>'});
</script>