<div class="content"><div class="main-offer-bl">
<?php 


                
extract($data);

$this->pageTitle = Yii::t('profile', $action->title);

$this->widget('application.components.exbreadcrumbs.EXBreadcrumbs', array(
	'links'=>array(
		Yii::t('profile', 'Current deals') => Yii::app()->createUrl('deal/default/CurrentDeals'),
		$this->pageTitle => Yii::app()->request->url
	),
));


?>
<div class="blue_line"></div>




<div class="deal-media-bl">
	<?php
        echo CHtml::image($action->getsrcone(460,'auto'),'',array('class'=>'deal_servise_pr_photo'));

        
        ?>

	
</div>
<div class="deal-content-bl deal_content_product" style="margin-left: 12px; width: 486px;">
    <h1 style="margin-bottom: -7px !important"><?php echo CHtml::encode($action->title);?></h1>
	<h3 class=""><?php echo $action->about;?></h3>
	<div class="edit_deal_do-pr">
		<div class="buy_product">

		<hr/>
		<div class="deal-timer">
			<span class="lblue"><?php echo Yii::t('app', 'Time Left To Buy');?></span>
			<span class="timer">
			<?php $timeLeft = $action->timeLeft;
			echo $timeLeft->days . ' ' . Yii::t('profile', 'days') . ' ' .
			$timeLeft->hours . ':' . $timeLeft->mins;?>
			</span>
		</div>
	</div>
	<div>
		<div class="price_variant">
			<div class="lblue"><?php echo Yii::t('profile', 'Value');?></div>
			<div class="gray"><?php echo  $this->numbformat($action->share_price_at_a_discount);?>$</div>
		</div>
		<div class="price_variant">
			<div class="lblue"><?php echo Yii::t('profile', 'Discont');?></div>
			<div class="gray"><?php echo  $this->numbformat($action->discount);?>%</div>
		</div>
		<div class="price_variant">
			<div class="lblue"><?php echo Yii::t('profile', 'You Save');?></div>
			<div class="gray"><?php echo $this->numbformat($action->share_price_without_discount-$action->share_price_at_a_discount);?>$</div>
		</div>
		<div class="lblue sz20"><?php echo $action->couponCount . Yii::t('profile', ' bought');?></div>
		<div class="gray"><?php echo Yii::t('profile', 'Limited quantity available');?></div>
		<?php if ($action->active):?>
		<div class="ok_block"><?php echo Yii::t('profile', 'The deal is on!');?></div>
		<?php endif; ?>
	</div>
	
	<div class="tab_container-pr">
		<div>
			<h2><?php echo CHtml::encode($action->title);?></h2>
			<p><?php echo CHtml::encode($action->about);?></p>
		</div>
		<div >
			<p><?php echo CHtml::encode($action->info);?></p>
		</div>
		<div >
			<h2><?php echo Yii::t('profile', 'Seller information');?></h2>
			<p><?php echo CHtml::encode($action->seller_info);?></p>
			<?php 
                        $data=$action->getMapData();
                        if (!empty($data)) :
                       
                            
                           $this->widget('GoogleMap',array('x'=>$data['x'],'y'=>$data['y'],'text'=>isset($data['txt'])?$data['txt']:null,
                               'htmlOptions'=>array('style'=>'width:425px;height:350px')
                               )
                               
                               );
                            ?>

			<?php 
                        
                        endif; ?>
		</div>
	

	</div>
</div></div></div>
