
<div class="content uslugs">
<?php $this->pageTitle = $category->title;?>
<div class="main-offer-bl page_uslugs">
	<?php $this->widget('application.components.exbreadcrumbs.EXBreadcrumbs', array(
		'links'=>array(
			$this->pageTitle => Yii::app()->request->url
		),
	));?>	
	<div class="blue_line"></div>
	<h1><?php echo Yii::t('profile', $this->pageTitle);?></h1>
	<div class="boats_service_list">
		<div class="contacts-bl">
			<img src="/images/coupon/us1.png" alt="" />
		</div>
		<div class="contacts-bl">
			<img src="/images/coupon/us2.png" alt="" />
		</div>
		<div class="contacts-bl last">
			<img src="/images/coupon/us3.png" alt="" />
		</div>
	</div>	
	<div class="clr"></div>
	<p>&nbsp;</p>
	
	<p class="main_p">
		<?=Yii::t('app','BoatsGo is the best way to get competitive quotes on marine related work for FREE.')?>
		<ul class="main_p_ul">
			<li><?=Yii::t('app','Post marine service requests related to')?> <span class="lowerc"><?php echo Yii::t('profile', $this->pageTitle);?></span></li>
			<li><?=Yii::t('app','Marine businesses will be notified of your service request and submit their quote for you to review')?></li>
		</ul>
	</p>
        
        <div>
            
            <?php $this->widget('application.components.messages.Questions', array(
				'category' => 'Servicesoffer',
				'materialId' => $category->id,
				'view' => 'questions',
			));?>
        </div>
	<div class="question-form tovar-mess">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'SendRequest-form',
			'enableAjaxValidation' => false
		));?>
			<div class="question-field">
				
					<?php
					//$formModel->request = Yii::t('profile', 'Type text here');
					echo $form->labelEx($formModel, 'request');
					echo $form->textArea($formModel, 'request', Array('tabindex'=>5, 'style'=>'color:#999;width: 975px;height: 127px;','placeholder'=>Yii::t('app', 'Type text here')));
					?>
				
			</div>
			<div><center><input <?php if (Yii::app()->user->isGuest) echo 'onclick="login(true);return false;"' ?> type="submit" tabindex="7" value="<?php echo Yii::t('profile', 'Send');?>" class="send-btn"></div></center>
		<?php $this->endWidget();?>
	</div>
	<?php foreach ($companies as $company) {
		$id = $company->id;?>
	<div class="company_descript">
		<h3><?php echo $company->title;?></h3>
		<div class="info_company">
			<?php if (isset($company->phone)):?>
			<span class="param"><?php echo Yii::t('profile', 'Phone');?>: </span><?php echo  CHtml::encode($company->phone);?> &nbsp;&nbsp;&nbsp;
			<?php endif;?>
			<?php if (isset($company->address)):?>
                        <span class="param2"><?php echo Yii::t('profile', 'Adress');?>: </span><?php echo CHtml::encode($company->address);?>
			<?php endif;?>
		</div>
                <p class="teaser <?php echo $id;?>"><?php echo CHtml::encode($company->title);?></p>
                <?php 
               
                if ($company->about!='')
                {
                     if (strlen($company->about)<200)
                     {
                         ?><p><?php echo CHtml::encode($company->about);?></p><?php
                     } else {
                ?>
                
                <p style="display:none" class="text<?php echo $id;?>"><?php echo CHtml::encode($company->about);?></p>
		<div class="more_info_link">
			<a href="javascript:void(0);" toggle="show" class="more" tag="<?php echo $id;?>"><?php echo Yii::t('profile', 'Read more');?></a>
		</div>
                
                    <?php 
                     }
                
                } 
                ?>
	</div>
	<?php }?>
	
</div>
</div>
<?php Yii::app()->clientScript->registerScriptFile('/assets/js/Teaser.js', CClientScript::POS_END); ?>
<script language="JavaScript">
    $('a.more').click(function(){
        var id=$(this).attr('tag');
        if (($(this).attr('toggle')=='show'))
            {
                $('.text'+id).slideDown();
                $(this).attr('toggle','hide');
                 $(this).html('<?php echo Yii::t('profile', 'Hide');?>');
            } else
                {
                    $('.text'+id).slideUp ();
                    $(this).attr('toggle','show');
                    $(this).html('<?php echo Yii::t('profile', 'Read more');?>');
                    
                }
        
        return false;
    });
    
   
// убираем текст
/*var requestTxtArea = $('[name*="request"]');
var defText = $(requestTxtArea).html();
$(requestTxtArea).focus(function(){
	if ($(requestTxtArea).html()==defText)
		$(requestTxtArea).html('').css('color','#000');
});
// возвращаем текст
$(requestTxtArea).blur(function(){
	if ($(requestTxtArea).html()=='')
		$(requestTxtArea).html(defText).css('color','#999');
});

        $('#SendRequest-form').submit(function(){
           if ($('#SendRequestForm_request').val()=='Type text here') 
               $('#SendRequestForm_request').val('');
        });*/
//</script>