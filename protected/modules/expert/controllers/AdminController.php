<?php

class AdminController extends BackEndController
{
    public $layout = '//layouts/main_content';

    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $model->setScenario(Expert::SCENARIO_ADMIN_ANSWER);

        if (isset($_POST['Expert']) && empty($model->answer_date)){
            $model->attributes = $_POST['Expert'];

            if ($model->save()){

                $toEmail = $model->answer_email;

                //Отправляем ответ пользователю
                Yii::app()->getModule('mail')->send($toEmail, Config::get('infoEmail'), 'expertAnswerMail',
                    array(
                        'siteNameLink' => CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')),
                        'siteName' => Yii::app()->params['siteName'],
                        'username' => $model->userFio(),
                        'question' => $model->question,
                        'expert_answer' => $model->expert_answer,
                    )
                );

                $this->redirect(array('admin'));
            }
        }

        $this->render('view', array(
            'model' => $model,
        ));
    }

    public function actionCreate()
    {
        $model = new Expert();

        $this->performAjaxValidation($model);

        if (isset($_POST['Expert'])) {
            $model->attributes = $_POST['Expert'];
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        if (isset($_POST['Expert'])) {
            $model->attributes = $_POST['Expert'];
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $model = new Expert('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['Expert']))
            $model->attributes = $_GET['Expert'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Expert::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'expert-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
