<?php

class DefaultController extends FrontEndController
{
    public $defaultAction = 'answer';

    public function actionAnswer($id, $code){
        $model = $this->loadModel($id);
        $model->setScenario(Expert::SCENARIO_EXPERT_ANSWER);

        if($code != $model->verify_code)
            throw new CHttpException(404, 'The requested page does not exist.');

        if(empty($model->answer_date)){

            if (isset($_POST['Expert'])){
                $model->attributes = $_POST['Expert'];

                if ($model->save()){

                    $toEmail = $model->answer_email;

                    //Отправляем ответ пользователю
                    Yii::app()->getModule('mail')->send($toEmail, Config::get('infoEmail'), 'expertAnswerMail',
                        array(
                            'siteNameLink' => CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')),
                            'siteName' => Yii::app()->params['siteName'],
                            'username' => $model->userFio(),
                            'question' => $model->question,
                            'expert_answer' => $model->expert_answer,
                        )
                    );

                    $this->setMessage(Misc::t('Информация'), Misc::t('Ваш ответ успешно отправлен!'));
                    $this->redirect(Yii::app()->createAbsoluteUrl('/site/index'));
                }
            }

        }

        $this->render('answer', array('model' => $model));
    }

    public function loadModel($id)
    {
        $model = Expert::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
}