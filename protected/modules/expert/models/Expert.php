<?php

/**
 * This is the model class for table "expert".
 *
 * The followings are the available columns in table 'expert':
 * @property string $id
 * @property string $pet_id
 * @property string $pet_breed
 * @property string $pet_type
 * @property string $cat_name
 * @property string $cat_birth
 * @property string $cat_big_id
 * @property string $cat_food
 * @property string $question
 * @property string $firstname
 * @property string $lastname
 * @property string $address
 * @property integer $region_id
 * @property string $city
 * @property string $answer_email
 * @property string $answer_phone
 * @property string $birth
 * @property string $expert_email
 * @property string $expert_answer
 * @property string $answer_date
 * @property string $verify_code
 * @property string $created_date
 */
class Expert extends CActiveRecord
{

    const SCENARIO_CONTACT_US = 'contact_us';
    const SCENARIO_CONTACT_US_STEP1 = 'contact_us_step1';
    const SCENARIO_EXPERT_ANSWER = 'expert_answer';
    const SCENARIO_ADMIN_ANSWER = 'admin_answer';

    public $verifyCode;
    public $catBMonth;
    public $catBYear;
    public $bMonth;
    public $bYear;
	public $step = 1;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Expert the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'expert';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pet_breed, pet_type, cat_name, question', 'required', 'message'=>'Необходимо заполнить поле {attribute}.', 'on' => self::SCENARIO_CONTACT_US_STEP1),
			array('pet_breed, pet_type, question, firstname, lastname, address, answer_email', 'required', 'message'=>'Необходимо заполнить поле {attribute}.', 'on' => self::SCENARIO_CONTACT_US),
            array('expert_answer', 'required', 'message'=>'Необходимо заполнить поле {attribute}.', 'on' => self::SCENARIO_EXPERT_ANSWER),
            array('expert_answer', 'required', 'message'=>'Необходимо заполнить поле ответ.', 'on' => self::SCENARIO_ADMIN_ANSWER),

			array('region_id, is_admin_answer, bMonth, bYear, catBMonth, catBYear', 'numerical', 'integerOnly'=>true),
			array('pet_id, cat_big_id', 'length', 'max'=>11),
			array('cat_name, pet_breed, pet_type, firstname, lastname, address, city, answer_email, answer_phone, expert_email', 'length', 'max'=>255),
			array('verify_code', 'length', 'max'=>128),
            array('answer_email', 'email'),
			array('cat_birth, cat_food, birth, expert_answer, answer_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, pet_id, cat_name, cat_birth, cat_big_id, cat_food, question, firstname, lastname, address, region_id, city, answer_email, answer_phone, birth, expert_email, expert_answer, answer_date, verify_code, created_date', 'safe', 'on'=>'search'),
            array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'on' => self::SCENARIO_CONTACT_US),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
        return array(
            'pet' => array(self::BELONGS_TO, 'DicPets', 'pet_id'),
            'region' => array(self::BELONGS_TO, 'DicRegions', 'region_id'),
        );
	}

    protected function beforeValidate()
    {

        if ($this->scenario == self::SCENARIO_CONTACT_US) {
            if($this->bMonth && $this->bYear)
                $this->birth = $this->bYear . '-' . $this->bMonth . '-' . '01 00:00:00';

            if($this->catBMonth && $this->catBYear)
                $this->cat_birth = $this->catBYear . '-' . $this->catBMonth . '-' . '01 00:00:00';
        }

        return parent::beforeValidate();
    }

    protected function beforeSave()
    {
        if ($this->scenario == self::SCENARIO_CONTACT_US)
            $this->generateVerifyCode();

        if($this->scenario == self::SCENARIO_EXPERT_ANSWER){
            $this->answer_date = date('Y-m-d H:i:s');
            $this->expert_email = Config::get('expertEmail');
        }

        if($this->scenario == self::SCENARIO_ADMIN_ANSWER){
            $this->answer_date = date('Y-m-d H:i:s');
            $this->is_admin_answer = 1;
        }


        return parent::beforeSave();
    }

    public function generateVerifyCode()
    {
        $this->verify_code = self::encrypt(microtime() . $this->answer_email);
    }

    public static function encrypt($value)
    {
        return md5($value);
    }

    public static function getDogBigList()
    {
        return array(
            1 => Yii::t('expert', 'до 10кг. мелкая'),
            2 => Yii::t('expert', '11-25кг. средняя'),
            3 => Yii::t('expert', '25-40кг. крупная'),
            4 => Yii::t('expert', 'свыше 40кг. гигантская'),
        );
    }

    public static function getCatBigList()
    {
        return array(
            5 => Yii::t('expert', 'Маленькая 0-2 кг'),
            6 => Yii::t('expert', 'Средняя 2-4 кг'),
            7 => Yii::t('expert', 'Большая 4-6 кг'),
            8 => Yii::t('expert', 'Крупная более 6 кг'),
        );
    }

    public function getCatBig(){
        $arr = self::getCatBigList();
        return isset($arr[$this->cat_big_id]) ? $arr[$this->cat_big_id] : '';
    }

    public static function getPetTypeList($id = '')
    {
        $items = array(
            'cat' => Yii::t('expert', 'Кошка'),
            'dog' => Yii::t('expert', 'Собака'),
        );

        if($id)
            return isset($items[$id]) ? $items[$id] : '';

        return $items;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pet_id' => 'Какая порода у Вашего питомца',
			'cat_name' => 'Имя Вашего питомца',
			'pet_breed' => 'Какая порода Вашего питомца',
			'pet_type' => 'Ваш питомец',
			'cat_birth' => 'Дата рождения Вашего питомца',
			'cat_big_id' => 'Какой вес у Вашего питомца',
			'cat_food' => 'Чем питается Ваш питомец',
			'question' => 'Что Вы хотите рассказать экспертам?',
			'firstname' => 'Фамилия',
			'lastname' => 'Имя',
			'address' => 'Адрес',
			'region_id' => 'Регион',
			'city' => 'Город',
			'answer_email' => 'E-mail для ответа',
			'answer_phone' => 'Телефон для ответа',
			'birth' => 'Дата рождения',
			'expert_email' => 'Email эксперта',
			'expert_answer' => 'Ответ эксперта',
			'answer_date' => 'Дата ответа',
			'verify_code' => 'Проверка доступа',
			'created_date' => 'Дата создания',
            'verifyCode' => 'Код проверки',
            'bYear' => Misc::t('Год рождения'),
            'bMonth' => Misc::t('Месяц рождения'),
            'catBYear' => Misc::t('Год рождения питомца'),
            'catBMonth' => Misc::t('Месяц рождения питомца')

		);
	}

    public function userFio(){
        return $this->firstname . ' ' . $this->lastname;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function adminSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('pet_id',$this->pet_id,true);
		$criteria->compare('cat_name',$this->cat_name,true);
		$criteria->compare('cat_birth',$this->cat_birth,true);
		$criteria->compare('cat_big_id',$this->cat_big_id,true);
		$criteria->compare('cat_food',$this->cat_food,true);
		$criteria->compare('question',$this->question,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('region_id',$this->region_id);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('answer_email',$this->answer_email,true);
		$criteria->compare('answer_phone',$this->answer_phone,true);
		$criteria->compare('birth',$this->birth,true);
		$criteria->compare('expert_email',$this->expert_email,true);
		$criteria->compare('expert_answer',$this->expert_answer,true);
		$criteria->compare('answer_date',$this->answer_date,true);
		$criteria->compare('verify_code',$this->verify_code,true);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}