﻿<?php
/* @var $this ExpertController */
/* @var $model Expert */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'expert-asd-form',
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'pet_id'); ?>
        <?php echo $form->textField($model,'pet_id'); ?>
        <?php echo $form->error($model,'pet_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'question'); ?>
        <?php echo $form->textField($model,'question'); ?>
        <?php echo $form->error($model,'question'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'firstname'); ?>
        <?php echo $form->textField($model,'firstname'); ?>
        <?php echo $form->error($model,'firstname'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'lastname'); ?>
        <?php echo $form->textField($model,'lastname'); ?>
        <?php echo $form->error($model,'lastname'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'address'); ?>
        <?php echo $form->textField($model,'address'); ?>
        <?php echo $form->error($model,'address'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'answer_email'); ?>
        <?php echo $form->textField($model,'answer_email'); ?>
        <?php echo $form->error($model,'answer_email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'created_date'); ?>
        <?php echo $form->textField($model,'created_date'); ?>
        <?php echo $form->error($model,'created_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'region_id'); ?>
        <?php echo $form->textField($model,'region_id'); ?>
        <?php echo $form->error($model,'region_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'cat_big_id'); ?>
        <?php echo $form->textField($model,'cat_big_id'); ?>
        <?php echo $form->error($model,'cat_big_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'cat_name'); ?>
        <?php echo $form->textField($model,'cat_name'); ?>
        <?php echo $form->error($model,'cat_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'city'); ?>
        <?php echo $form->textField($model,'city'); ?>
        <?php echo $form->error($model,'city'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'answer_phone'); ?>
        <?php echo $form->textField($model,'answer_phone'); ?>
        <?php echo $form->error($model,'answer_phone'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'expert_email'); ?>
        <?php echo $form->textField($model,'expert_email'); ?>
        <?php echo $form->error($model,'expert_email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'verify_code'); ?>
        <?php echo $form->textField($model,'verify_code'); ?>
        <?php echo $form->error($model,'verify_code'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'cat_birth'); ?>
        <?php echo $form->textField($model,'cat_birth'); ?>
        <?php echo $form->error($model,'cat_birth'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'cat_food'); ?>
        <?php echo $form->textField($model,'cat_food'); ?>
        <?php echo $form->error($model,'cat_food'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'birth'); ?>
        <?php echo $form->textField($model,'birth'); ?>
        <?php echo $form->error($model,'birth'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'expert_answer'); ?>
        <?php echo $form->textField($model,'expert_answer'); ?>
        <?php echo $form->error($model,'expert_answer'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'answer_date'); ?>
        <?php echo $form->textField($model,'answer_date'); ?>
        <?php echo $form->error($model,'answer_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'verifyCode'); ?>
        <?php echo $form->textField($model,'verifyCode'); ?>
        <?php echo $form->error($model,'verifyCode'); ?>
    </div>


    <div class="row buttons">
        <?php echo CHtml::submitButton('Submit'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->