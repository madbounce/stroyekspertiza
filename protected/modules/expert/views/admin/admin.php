<?php
$dp = $model->adminSearch();
$countAll = $dp->totalItemCount;
?>
<div class="page-header admin-header">
    <h3><?php echo Yii::t('app', '{n} вопрос|{n} вопроса|{n} вопросов', $countAll);?></h3>
</div>

<div class="page-content">
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'id' => 'expert-grid',
    'type' => 'striped bordered',
    'dataProvider' => $dp,
    //'filter' => $model,
    'template' => '{pager}{items}{pager}',
    'columns' => array(
        array(
            'header' => 'Дата создания',
            'name' => 'created_date',
            'value' => 'Yii::app()->dateFormatter->format("dd MMMM yyyy", $data->created_date)',
            'htmlOptions' => array('style' => 'width: 120px')
        ),
        array(
            'header' => 'ФИО',
            'name' => 'firstname',
            'value' => '$data->userFio()',
        ),
        array(
            'header' => 'Вопрос',
            'name' => 'question',
        ),
        array(
            'header' => 'Ответ',
            'name' => 'expert_answer',
            'type' => 'raw',
            'value' => function($data) {
                return $data->expert_answer ? '<span class="label label-success">' . Yii::app()->dateFormatter->format("dd MMMM yyyy HH:mm", $data->answer_date) .'</span>' :  '<span class="label label-important">' . Yii::t('expert', 'Не отвечено') . '</span>';
            }
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{delete}'
        ),
    ),
));
?>
</div>