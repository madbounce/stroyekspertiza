<div class="page-header">
    <h3><?php echo Yii::t('expert', 'Просмотр вопроса/ответа'); ?></h3>
    <div class="header-tip"><?php
        if($model->answer_date){
            echo '<span class="label label-success">' . Yii::t('expert', 'Отвечено: ') . Yii::app()->dateFormatter->format("dd MMMM yyyy HH:mm", $model->answer_date) .'</span>';
            if($model->is_admin_answer)
               echo '<span class="label label-success answer-tip">' . Yii::t('expert', 'Ответил администратор сайта ') .'</span>';
            else
                echo '<span class="label label-success answer-tip">' . Yii::t('expert', 'Ответил эксперт: {email}', array('{email}' => $model->expert_email)) .'</span>';
        } else
            echo '<span class="label label-important">' . Yii::t('expert', 'Не отвечено') . '</span>';
        ?></div>
</div>

<div class="page-content">
    <?php $this->widget('bootstrap.widgets.TbDetailView', array(
        'data' => $model,
        'htmlOptions' => array('style'=>'clear:both;'),
        'attributes' => array(
            array(
                'label' => 'Дата создания',
                'name' => 'created_date',
                'value' => Yii::app()->dateFormatter->format("dd MMMM yyyy HH:mm", $model->created_date),
            ),

            array(
                'label' => 'Питомец',
                'name' => 'pet_type',
                'value' => Expert::getPetTypeList($model->pet_type),
            ),
            array(
                'label' => 'Порода питомца',
                'name' => 'pet_breed',
                'value' => $model->pet_breed,
            ),
            array(
                'label' => 'Имя питомца',
                'name' => 'cat_name',
            ),
            array(
                'label' => 'День рождения питомца',
                'name' => 'cat_birth',
                'value' => Yii::app()->dateFormatter->format("LLLL yyyy", $model->cat_birth),
            ),
            array(
                'label' => 'Вес питомца',
                'name' => 'cat_big_id',
                'value' => ($model->cat_big_id) ? $model->getCatBig() : null,
            ),
            array(
                'label' => 'Питание',
                'name' => 'cat_food',
            ),

            array(
                'label' => 'Фамилия',
                'name' => 'firstname',
            ),
            array(
                'label' => 'Имя',
                'name' => 'lastname',
            ),
            array(
                'label' => 'Адрес',
                'name' => 'address',
            ),
            array(
                'label' => 'Регион',
                'name' => 'region_id',
                'value' => ($model->region) ? $model->region->title : null,
            ),
            array(
                'label' => 'Город',
                'name' => 'city',
            ),
            array(
                'label' => 'Дата рождения',
                'name' => 'birth',
                'value' => Yii::app()->dateFormatter->format("LLLL yyyy", $model->birth),
            ),
            array(
                'label' => 'E-mail для ответа',
                'name' => 'answer_email',
            ),
            array(
                'label' => 'Телефон для ответа',
                'name' => 'answer_phone',
                'value' => ($model->answer_phone) ? $model->answer_phone : null,
            ),
            array(
                'label' => 'Вопрос',
                'name' => 'question',
            ),
            array(
                'label' => 'Ответ',
                'name' => 'expert_answer',
                'type' => 'raw',
                'visible' => !empty($model->answer_date)
            ),
        ),
    ));
    ?>
    <?php if(empty($model->answer_date)):?>
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'answer-form',
            'enableAjaxValidation' => false,
            'htmlOptions' => array(
                'class' => 'product'
            ),
        )); ?>

        <?php echo $form->errorSummary($model); ?>
        <h2>Отправить ответ</h2>
        <?php
        $this->widget('application.extensions.redactorjs.ERedactorWidget', array(
            'model' => $model,
            'attribute' => 'expert_answer',
            'options' => array(
                'lang' => 'ru',
                'minHeight' => '200',
                'buttons' => "js:['html', 'bold', 'italic', 'deleted']"
            ),
        ));
        ?>

        <div class="form-actions">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => Yii::t('expert', 'Отправить'),
        )); ?>
        </div>

        <?php $this->endWidget(); ?>
    <?php endif;?>
</div>


