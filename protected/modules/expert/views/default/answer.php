
<div style="margin-top:45px;">

<?php
    $this->breadcrumbs=array(
        Yii::t('expert','Ответ пользователю'),
    );
?>
<?php $this->widget('banner.components.widget.rotator', array('categoryId' => 14,'view'=>'textClickable')); ?>

<div class="fix_page">
    <div class="_col txt-content">
        <h1 class="heading">Ответ пользователю</h1>

        <div class="intro">
            <p class="intro-summary"><strong>Вопрос:</strong> <?php echo $model->question; ?></p>
        </div>
		<br/>
		<br/>

        <div id="expert_answer">

            <?php if (!empty($model->answer_date)): ?>

                <h3 class="heading">Вы уже ответили на вопрос!</h3>
                <div class="intro">
                    <p class="intro-summary"><strong>Ответ:</strong> <?php echo $model->expert_answer; ?></p>
                </div>

            <?php else: ?>

                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'expert-answer-form',
                    'enableAjaxValidation' => false,
                )); ?>

                <?php echo $form->errorSummary($model); ?>

                <div class="form_answer">
                    <?=$form->labelEx($model, 'expert_answer'); ?>
                    <?=$form->textArea($model, 'expert_answer', array('style' => 'width: 540px; height: 170px')); ?>
                    <?=$form->error($model, 'expert_answer'); ?>

                    <div class="clear"></div>
                </div>

                <div class="form_answer">
                    <div class="answer_field" style="float:right;">
                        <?php echo CHtml::submitButton(Yii::t('app', 'Отправить ответ'), array('class' => 'btn', 'id' => 'submit_button')); ?>
                    </div>
                    <div class="clear"></div>
                </div>

                <?php $this->endWidget(); ?>

            <?php endif;?>
        </div>
    </div>
</div>


</div>