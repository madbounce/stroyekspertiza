<?php

class FaqModule extends CWebModule
{
    public function init()
    {
        $this->setImport(array(
            'faq.models.*',
            'faq.components.*',
        ));
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        } else
            return false;
    }

    public static function link($name, $htmlOptions = array())
    {
        $model = Faq::model()->findByAttributes(array('name' => $name));
        return CHtml::link(CHtml::encode($model->title), array('/faq/default/view', 'name' => $model->name), $htmlOptions);
    }
}
