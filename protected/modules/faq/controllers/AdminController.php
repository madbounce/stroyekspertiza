<?php

class AdminController extends BackEndController
{
    public $layout = '//layouts/main_content';

    public function actions()
    {
        return array(
            'redactorUploadImage' => array(
                'class' => 'application.extensions.redactorjs.ERedactorUploadAction',
            ),
            'toggle' => 'ext.jtogglecolumn.ToggleAction',
            'sortable' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'Faq'
            ),
            'sortableCategory' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'FaqCategory'
            )

        );
    }

    public function actionCreate()
    {
        $model = new Faq;

        $this->performAjaxValidation($model);

        if (isset($_POST['Faq'])) {
            $model->attributes = $_POST['Faq'];
            if ($model->validate())
			{
				$model->save();
                $this->redirect(array('admin'));
			}
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, true);

        $this->performAjaxValidation($model);

        if (isset($_POST['Faq'])) {
            $model->attributes = $_POST['Faq'];
            if ($model->validate())
			{
				$model->save();
                $this->redirect(array('admin'));
			}
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $model = new Faq('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['Faq']))
            $model->attributes = $_GET['Faq'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionEditable()
    {
        if (isset($_POST['pk']) && isset($_POST['name']) && isset($_POST['value'])) {
            $model = $this->loadModel($_POST['pk']);
            $model->title = $_POST['value'];

            if ($model->save()) {
                echo CJSON::encode(array('success' => true));
                Yii::app()->end();
            }

            echo CJSON::encode(array('success' => false, 'msg' => $model->getError($_POST['name'])));
            Yii::app()->end();
        }

        echo CJSON::encode(array('success' => false));
        Yii::app()->end();
    }

    public function loadModel($id, $ml = false)
    {
        $model = Faq::model()->findByPk($id);

        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'page-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    public function actionCategories()
    {
        $model = new ArticlesCategory('search');
        $model->unsetAttributes();
        if (isset($_GET['ArticlesCategory']))
            $model->attributes = $_GET['ArticlesCategory'];

        $this->render('admincategory', array(
            'model' => $model,
        ));
    }

    public function actionCreateCategory()
    {
        $model=new ArticlesCategory;

        if(isset($_POST['ArticlesCategory']))
        {
            $model->attributes=$_POST['ArticlesCategory'];
            $model->parent_id = 0;

            if($model->save())
            {
                $this->redirect(array('categories'));
            }
        }

        $this->render('createcategory',array(
            'model'=>$model,
        ));
    }

    public function actionUpdateCategory($id)
    {
        $model=$this->loadCatModel($id);

        if(isset($_POST['ArticlesCategory']))
        {
            $model->attributes=$_POST['ArticlesCategory'];

            if($model->save())
            {
                $this->redirect(array('categories'));
            }
        }

        $this->render('updatecategory',array(
            'model'=>$model,
        ));
    }

    public function actionEditableCategory()
    {
        if (isset($_POST['pk']) && isset($_POST['name']) && isset($_POST['value'])) {
            $model = $this->loadCatModel($_POST['pk']);
            $model->title = $_POST['value'];

            if ($model->save()) {
                echo CJSON::encode(array('success' => true));
                Yii::app()->end();
            }

            echo CJSON::encode(array('success' => false, 'msg' => $model->getError($_POST['name'])));
            Yii::app()->end();
        }

        echo CJSON::encode(array('success' => false));
        Yii::app()->end();
    }

    public function actionDeleteCategory($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadCatModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('categories'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function loadCatModel($id)
    {
        $model = FaqCategory::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
}
