<?php

class DefaultController extends FrontEndController
{
    public $layout = '//layouts/main';
	
	public function actionView($name)
    {
        $model = $this->loadModel($name);

        $this->pageTitle = $model->meta_title;
        Yii::app()->clientScript->registerMetaTag($model->meta_description, 'description');
        Yii::app()->clientScript->registerMetaTag($model->meta_keywords, 'keywords');

        $view = 'view';

        if($this->getViewFile($name)){
            $view = $name;
        }

        $this->render($view, array('model' => $model));
    }

	public function actionIndex($cId = '')
    {
        $cId = $cId ? $cId : 1;
        $model = new Faq();
        $model->cId($cId);
        $category = FaqCategory::model()->findByPk($cId);
        $this->render('index', array('model' => $model, 'category'=>$category));
    }

    public function loadModel($name)
    {
        $criteria = new CDbCriteria;
        if(is_numeric($name))
            $criteria->addCondition('id ='.$name,'or');
        else
            $criteria->addCondition('url ="'. $name.'"', 'or');

        $model = Faq::model()->find($criteria);
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        return $model;
    }
}