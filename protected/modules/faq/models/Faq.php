<?php

/**
 * This is the model class for table "faq".
 *
 * The followings are the available columns in table 'faq':
 * @property string $id
 * @property string $title
 * @property string $content
 *
 */
class Faq extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{faq}}';
    }

    protected function afterSave()
    {
        parent::afterSave();

        Search::updateIndex(get_class($this), $this->id);
    }

    public function rules()
    {
        return array(
            array('categoryId, title, content', 'required'),
            array('title', 'length', 'max' => 255),
            array('categoryId, url, meta_title, meta_description, meta_keywords', 'safe'),
            array('id, categoryId, title, content, url, meta_title, meta_description, meta_keywords', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'category' => array(self::BELONGS_TO, 'FaqCategory', 'categoryId'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'categoryId' => Yii::t('faq', 'Категория'),
            'title' => Yii::t('faq', 'Вопрос'),
            'content' => Yii::t('faq', 'Ответ'),
            'url' => Yii::t('faq', 'Ссылка'),
            'meta_title'=>'Заголовок',
            'meta_description'=>'Описание',
            'meta_keywords'=>'Ключевые слова',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('url', $this->url, true);

        if(isset($_GET['search']) && $_GET['search']){
            $search = '%'.str_replace(' ','%', addcslashes(trim($_GET['search']),'%_')).'%';
            $criteria->addSearchCondition('title', $search , false);
            $criteria->addSearchCondition('content',  $search, false,'or');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 25),
            'sort' => array(
                'defaultOrder' => 'title ASC',
            ),
        ));
    }


    public function adminSearch()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('url', $this->url, true);

        if(isset($_GET['cId'])){
            $cid = (int)$_GET['cId'];
            $criteria->addCondition('t.categoryId =' . $cid);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array('pageSize' => Yii::app()->params->pageSize)
        ));
    }

    public function cId($id = 0)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=>'t.categoryId="'.$id.'"',
        ));
        return $this;
    }
}