<?php

/**
 * This is the model class for table "{{faq_category}}".
 *
 * The followings are the available columns in table '{{faq_category}}':
 * @property integer $id
 * @property string $title
 *
 * The followings are the available model relations:
 */
class FaqCategory extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{faq_category}}';
	}

	public function getModelName()
	{
	    return __CLASS__;
	}

    public function behaviors()
    {
        return array(
            'SortBehavior' => array(
                'class' => 'ext.behaviors.SortBehavior',
                'sortAttribute' => 'sort'
            ),
        );
    }
	
	public function relations()
	{
		return array( 
			'faq' => array(self::HAS_MANY, 'Faq', 'categoryId'),
            'parents' => array(self::HAS_MANY, 'FaqCategory', 'parent_id'),
            'parentsId' => array(self::HAS_ONE, 'FaqCategory', array('id'=>'parent_id')),
		);
	}


	public function rules()
	{
		return array(
			array('title, parent_id', 'required'),
			array('title', 'length', 'max'=>100),
			array('sort, url', 'safe'),
			array('id, title, url', 'safe', 'on'=>'search'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Категория',
			'title' => 'Название',
			'url' => 'Eng название'
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);

        $criteria->order = 't.sort ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
                'pageSize' => 10000
            ),
		));
	}

    public function pId($id = 0)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=>'t.parent_id="'.$id.'"',
        ));
        return $this;
    }

    public function path($path = '')
    {
        if(!$path) return $this;
        $pid = $this->findByAttributes(array('url'=>$path));
        if(!$pid) return $this;


        return $this->pId($pid->id);
    }

    public function toArray($key='id',$value = 'title'){

        $criteria = new CDbCriteria();
        $criteria->order = 't.sort ASC';
        $this->getDbCriteria()->mergeWith($criteria);
        $model = $this->findAll($criteria);

        $items = array();
        foreach($model as $row)
            $items[$row->{$key}] = $row->{$value};

        return $items;
    }

    public static function all(){
        $criteria = new CDbCriteria();
        $criteria->order = 't.sort ASC';
        return self::model()->findAll($criteria);
    }

    public static function listAll($id='',$pid='')
    {
        $criteria = new CDbCriteria();
        if(!empty($id)) $criteria->addCondition('id <>'.$id);
        if(!empty($pid)) $criteria->addCondition('parent_id <>'.$pid);
        $criteria->order = 't.sort ASC';
        $res = self::model()->findAll($criteria);
        return CHtml::listData($res, 'id', 'title');
    }
}