<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'category-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'product'
    ),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textField($model, 'title', array('class' => 'name', 'maxlength' => 255)); ?>
    <div class="checkbox"></div>
</div>

<div id='category'>
    <?php echo $form->dropDownListRow($model, 'parent_id', array('0' => 'Новый раздел')+FaqCategory::listAll($model->id), array('class' => 'span5', 'maxlength' => 100)); ?>
</div>

<div class="line">
    <?php echo $form->labelEx($model,'url'); ?>
    <?php echo $form->textField($model,'url',array('class'=>'medium', 'size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'url'); ?>
</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
</div>
<?php $this->endWidget(); ?>