    <?php
    $dp = $model->adminSearch();
    $countAll = $dp->totalItemCount;
?>
<div class="page-content">
    <div class="row two-columns">
        <div class="span9">

            <div class="page-header admin-header">
                <h3><?php echo Yii::t('app', '{n} вопрос|{n} вопроса|{n} вопросов', $countAll);?></h3>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => Yii::t('translate', 'Добавить вопрос'),
                    'size' => 'small',
                    'url' => array('/faq/admin/create'),
                ));?>
            </div>


            <div class="page-content">
                <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
                    'id' => 'page-grid',
                    'type'=>'striped bordered',
                    'dataProvider' => $dp,
                    'filter' => $model,
                    'enableSorting' => false,
                    'sortableRows' => true,
                    'sortableAjaxSave' => true,
                    'sortableAttribute' => 'sort',
                    'sortableAction' => '/faq/admin/sortable',
                    'template' => '{pager}{items}{pager}',
                    'columns' => array(
                        array(
                            'class' => 'bootstrap.widgets.TbEditableColumn',
                            'name' => 'title',
                            'type'=>'raw',
                            'value' => '$data->title',
                            'editable' => array(
                                'url' => Yii::app()->controller->createUrl("/faq/admin/editable"),
                                'placement' => 'right',
                                'inputclass' => 'span4',
                                'title' => Yii::t('admin', 'Введите название пункта'),
                                'success' => 'js: function(data) {
                                    if(typeof data == "object" && !data.success) return data.msg;
                                }'
                            ),

                        ),
        //                'url',
                        array(
                            'class' => 'bootstrap.widgets.TbButtonColumn',
                            'template' => '{update}{delete}'
                        ),
                    ),
                )); ?>
            </div>
        </div>

        <div class="span3 sidebar">
            <div class="b-side">
                <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route), 'get', array('class' => 'search-form')); ?>

                <div class="input-append">
                    <?php echo CHtml::textField('search', isset($_GET['search']) ? $_GET['search'] : '', array('class' => 'input-medium search-input'))?>
                    <button class="btn" type="submit"><i class="icon-search"></i></button>
                </div>
                <?php echo CHtml::endForm(); ?>
            </div>

            <div class="b-side">
                <?php
                $categories = array(
                    array('label'=>Yii::t('admin','Все категории'), 'url'=>array('/faq/admin/admin'), 'active' => (!isset($_GET['cId']))),
                );
                $cats = FaqCategory::all();
                if($cats){
                    foreach($cats as $cat){
                        $rowCssClass = ($cat->parent_id) ? "child-r" : "parent-r";
                        array_push($categories, array(
                                'label' => $cat->title,
                                'url' => array('/faq/admin/admin', 'cId' => $cat->id),
                                'active' => (isset($_GET['cId']) && ($_GET['cId'] == $cat->id)),
                                'itemOptions' => array('class'=>$rowCssClass),
                            )
                        );
                    }
                }

                $this->widget('bootstrap.widgets.TbMenu', array(
                    'type'=>'list',
                    'items' => $categories
                ));
                ?>
            </div>

        </div>
    </div>
</div>