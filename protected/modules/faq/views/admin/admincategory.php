<?php
$dp = $model->search();
$countAll = $dp->totalItemCount;
?>
<div class="page-content">

    <div class="page-header admin-header">
        <h3><?php echo Yii::t('app', '{n} категория|{n} категории|{n} категорий', $countAll);?></h3>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label' => Yii::t('faq', 'Добавить категорию'),
            'size' => 'small',
            'url' => array('/faq/admin/createcategory'),
        ));
        ?>
    </div>


    <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
        'id'=>'categories-grid',
        'type'=>'striped bordered',
        'dataProvider' => $dp,
        //'filter' => $model,
        'enableSorting' => false,
        'sortableRows' => true,
        'sortableAjaxSave'=>true,
        'sortableAttribute'=>'sort',
        'sortableAction'=> 'faq/sortableCategory',
        'rowCssClassExpression' =>  '($data->parent_id) ? "child-r" : "parent-r"',
        'template' => '{pager}{items}{pager}',
        'columns' => array(
            array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'title',
                'type'=>'raw',
                'value' => '$data->title',
                'editable' => array(
                    'url' => Yii::app()->controller->createUrl("/faq/admin/editableCategory"),
                    'placement' => 'right',
                    'inputclass' => 'span4',
                    'title' => Yii::t('admin', 'Введите название пункта'),
                    'success' => 'js: function(data) {
                    if(typeof data == "object" && !data.success) return data.msg;
                }'
                ),

            ),
            array(
                'name' => 'url',
            ),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => ' {update} {delete}',
                'buttons' => array(
                    'update' => array(
                        'url'=>'Yii::app()->controller->createUrl("/faq/admin/UpdateCategory", array("id"=>$data->primaryKey))',
                    ),
                    'delete' => array(
                        'url'=>'Yii::app()->controller->createUrl("/faq/admin/deletecategory", array("id"=>$data->primaryKey))',
                    ),
                ),
            ),
        ),
    )); ?>
</div>