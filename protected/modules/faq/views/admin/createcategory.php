<div class="page-header">
    <h3><?php echo Yii::t('faq', 'Создание категории'); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_formcategory', array('model' => $model)); ?>
</div>