<div class="page-header">
    <h3><?php echo Yii::t('faq', 'Редактирование категории'); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_formcategory', array('model' => $model)); ?>
</div>