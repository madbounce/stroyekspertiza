<?php
    $this->breadcrumbs=array(
//        Yii::t('faq','FAQ'),
        $category->title
    );
    Yii::app()->clientScript->registerScript('search', "
        $('#faq-search').submit(function(){
            $.fn.yiiListView.update('faq-form',{
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>

<div class="faq">

    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 16,'view'=>'textClickable')); ?>

    <div class="fix_page">

       <form id="faq-search" action="" method="get" >
            <div class="search">
                <input name="search" type="text" onblur="this.value=(this.value=='')?this.title:this.value;" onfocus="this.value=(this.value==this.title)?'':this.value;" value="Поиск..." title="Поиск..."/>
                <input type="submit"  value="Поиск"/>
            </div>
       </form>

        <div class="items">
            <?php $this->widget('zii.widgets.CListView', array(
            'id' => 'faq-form',
            'dataProvider' => $model->search(),
            'itemView' => 'listItem',
            'itemsTagName' => 'ul',
            'itemsCssClass' => 'faq-list',
            'sorterHeader'=>'Сортировать по:',
            'sortableAttributes'=>array('title'),
            'template' => '<div class="top">{sorter} {summary} {pager}</div> <hr> {items} <hr> <div class="bottom">{pager}</div>',
                'pager' => array(
                    'firstPageLabel'=>'&larr;',
                    'prevPageLabel'=>'<',
                    'nextPageLabel'=>'>',
                    'lastPageLabel'=>'&rarr;',
                    'maxButtonCount'=>'10',
                    'header'=>'',
                    //'cssFile'=>false,
                    'cssFile'=>Yii::app()->theme->getBaseUrl(true).'/css/pager.css'
                ),
            )); ?>
        </div>
    </div>
</div>