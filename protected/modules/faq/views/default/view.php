<div class="faq">


<?php
    $this->breadcrumbs=array(
//        Yii::t('faq','FAQ') => array('/faq/default/index'),
        $model->category->title => array('/faq/default/index/cId/'.$model->categoryId),
        strip_tags($model->title),
    );

?>

    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 16,'view'=>'textClickable')); ?>


    <div class="fix_page">
        <div class="staticwrap">
            <h1 class="title"><?php echo $model->title; ?></h1>

            <?php echo $model->content; ?>
        </div>
    </div>

</div>