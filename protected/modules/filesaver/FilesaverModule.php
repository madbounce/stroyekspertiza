<?php
class FileSaverModule extends CWebModule
{
	public $nameModule = 'Файлы';
	public $showFrontEnd = false;
	public $showBackEnd= false;
	
    static public function getImports()
    {
        return array(
            'filesaver.models.*',
            'filesaver.components.*'
        );
    }

    static public function importAll()
    {
        foreach(self::getImports() as $import)
        {
            yii::import($import);
        }
    }


    public function init()
    {
        FileSaverModule::importAll();
    }

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}