<?php
/**
        How to use:

        view:
		 $this->widget('ext.EAjaxUpload.EAjaxUpload',
                 array(
                       'id'=>'uploadFile',
                       'config'=>array(
                                       'action'=>'/controller/upload',
                                       'allowedExtensions'=>array("jpg"),//array("jpg","jpeg","gif","exe","mov" and etc...
                                       'sizeLimit'=>10*1024*1024,// maximum file size in bytes
                                       'minSizeLimit'=>10*1024*1024,// minimum file size in bytes
                                       //'onComplete'=>"js:function(id, fileName, responseJSON){ alert(fileName); }",
                                       //'messages'=>array(
                                       //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
                                       //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
                                       //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
                                       //                  'emptyError'=>"{file} is empty, please select files again without it.",
                                       //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
                                       //                 ),
                                       //'showMessage'=>"js:function(message){ alert(message); }"
                                      )
                      ));

        controller:

	public function actionUpload()
	{
	        Yii::import("ext.EAjaxUpload.qqFileUploader");
	        
                $folder='upload/';// folder for uploaded files
                $allowedExtensions = array("jpg"),//array("jpg","jpeg","gif","exe","mov" and etc...
                $sizeLimit = 10 * 1024 * 1024;// maximum file size in bytes
                $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
                $result = $uploader->handleUpload($folder);
                $result=htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                echo $result;// it's array
	}

*/
class EAjaxUpload extends CWidget
{
	//my modifications
	public $files;
	public $model;
    public $mode = 'edit';
	
    public $id="fileUploader";
	public $postParams=array();
	public $config=array();
	public $css=null;
        
	public function run()
	{
        $assets = dirname(__FILE__).'/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets, false,  -1, YII_DEBUG);
        $this->css=(!empty($this->css))?$this->css:$baseUrl.'/fileuploader.css';
        Yii::app()->clientScript->registerCssFile($this->css);

        if($this->mode == 'edit') {

            if (empty($this->config['action'])) {
                $this->config['action'] = Yii::app()->createUrl('/filesaver/default/upload');
            }

            if(empty($this->config['action'])) {
                 throw new CException('EAjaxUpload: param "action" cannot be empty.');
            }

            if(empty($this->config['allowedExtensions'])) {
                 throw new CException('EAjaxUpload: param "allowedExtensions" cannot be empty.');
            }

            if(empty($this->config['sizeLimit'])){
                throw new CException('EAjaxUpload: param "sizeLimit" cannot be empty.');
            }

            unset($this->config['element']);

            echo '<div id="'.$this->id.'">
                        <noscript><p>Please enable JavaScript to use file uploader.</p></noscript>
                    </div>';

            Yii::app()->clientScript->registerScriptFile($baseUrl . '/fileuploader.js', CClientScript::POS_HEAD);

            $postParams = array('PHPSESSID'=>session_id(),'YII_CSRF_TOKEN'=>Yii::app()->request->csrfToken);

            if(isset($this->postParams))
            {
                $postParams = array_merge($postParams, $this->postParams);
            }


            $script = "
                function delfile(id) {
                    $.ajax({
                            type: 'POST',
                            url: '".Yii::app()->createUrl('/filesaver/default/delete')."',
                            data: 'id='+id+'&YII_CSRF_TOKEN=".Yii::app()->request->csrfToken."',
                            success: function(ret) {
                                ret = eval('(' + ret + ')');
                                if (ret['success']=='true') {
                                    $('.file' + id).remove();
                                    $('#uploadFile' + id).remove();
                                }
                            }
                    });
                    return false;
                }

                function renamefile(id) {
                    $.ajax({
                            type: 'POST',
                            url: '".Yii::app()->createUrl('/filesaver/default/getRenameDialog')."',
                            data: 'id='+id+'&YII_CSRF_TOKEN=".Yii::app()->request->csrfToken."',
                            success: function(ret) {
                               $('body').append(ret);
                               name = $('.file' + id).find('span.qq-upload-file').html();
                               $('#new_file_name').attr('value', name);
                            },
                            complete: function(){
                                $('.loader').hide();
                            }
                    });
                }
                 function changename(id){
                    new_name =  $('#new_file_name').attr('value');
                    $.ajax({
                            type: 'POST',
                            url: '".Yii::app()->createUrl('/filesaver/default/rename')."',
                            data: 'id='+id+'&YII_CSRF_TOKEN=".Yii::app()->request->csrfToken."' + '&name='+new_name,
                            dataType: 'json',
                            success: function(ret) {
                                if(ret.status == 'success'){
                                    $('.file' + id).find('span.qq-upload-file').html(new_name);
                                    $('#rename-file-dialog').dialog('close');
                                } else
                                    alert('Ошибка переименования файла.')
                            },
                    });

                 }
            ";
            Yii::app()->getClientScript()->registerScript('filesaverwidget',$script, CClientScript::POS_HEAD);

            Yii::app()->clientScript->registerScript('change-name-submit', '
                $("#filename_submit").live("click", function() {
                    var id = $(this).attr("data-id");
                    changename(id);
                    return false;
                });
            ', CClientScript::POS_READY);

            $config = array(
                'element'=>'js:document.getElementById("'.$this->id.'")',
                'debug'=>false,
                'multiple'=>false,
                'onComplete'=>"js:function(id, fileName, responseJSON){
                    temp_id = responseJSON.temp_id;
                    size = $('.qq-upload-size').html();
                    $('.qq-upload-size').remove();
                    filename = responseJSON.filename;
                    nameArr = filename.split('.');
                    ext = nameArr.pop();
                    name = responseJSON.original_name;
                    $('.qq-upload-list').append('<li class=\"uploadedFiles\" id=\"uploadFile' + temp_id + '\">' +
                    '<input class=\"file\" type=\"hidden\" name=\"files[]\" value=\"'+responseJSON.temp_id+'\">' +
                    '</li>');
                    switch(ext){
                        case 'zip':
                            icon = 'ico_zip.png';
                            break;
                        case 'doc':
                        case 'docx':
                            icon = 'ico_doc.png';
                            break;
                        case 'png':
                            icon = 'ico_png.png';
                            break;
                        case 'rar':
                            icon = 'ico_rar.png';
                            break;
                        case 'xls':
                            icon = 'ico_xls.png';
                            break;
                         case 'ppt':
                            icon = 'ico_ppt.png';
                            break;
                         case 'txt':
                            icon = 'ico_txt.png';
                            break;
                         case 'pdf':
                            icon = 'ico_pdf.png';
                            break;                            
                        case 'jpg':
                        case 'jpeg':
                            icon = 'ico_jpg.png';
                            break;
                        case 'tiff':
                            icon = 'ico_tiff.png';
                            break;                            
                        default:
                            icon = 'ico_vopros.png';
                    }
                    img = '<img src=\"" . Yii::app()->theme->baseUrl . "/images/' + icon + '\" width=\"41\" height=\"48\" />';
                    $('.qq-upload-success:last').html(
                        '<div class=\"block_file\">' + img +
                        '<a class=\"file-link\" target=\"_blank\" href=\"" . Yii::app()->baseUrl . "/files/temp/' + filename +' \"><span class=\"qq-upload-file\">' + name + '</span></a><br/>' +
                        '<span>.' + ext + ' ' + size + '</span><br/>' +
                        '<a href=\"#\" class=\"red\" onclick=\"delfile(' + temp_id + '); return false;\">удалить</a>&nbsp;' +
                        '<a href=\"#\" class=\"black\" onclick=\"renamefile(' + temp_id + '); return false;\">переименовать</a>' +
                        '</div>'
                    );
                    $('.qq-upload-success:last').addClass('file' + temp_id);
                    $('.uploadedFiles').hide();
                    //$('.qq-upload-button').hide();
                }",
            );

            $config = array_merge($config, $this->config);
            $config['params']=$postParams;
            $config = CJavaScript::encode($config);

            if($this->files){
                $htmlFiles = $this->getFileTemplate();

                Yii::app()->getClientScript()->registerScript("FileUploader_".$this->id, "
                var FileUploader_".$this->id." = new qq.FileUploader($config);
                $('.qq-upload-list').html('" . $htmlFiles . "');
                ",CClientScript::POS_LOAD);


            } else

                Yii::app()->getClientScript()->registerScript("FileUploader_".$this->id, "var FileUploader_".$this->id." = new qq.FileUploader($config);",CClientScript::POS_LOAD);

        } else {
            if($this->files){
                $htmlFiles = $this->getFileTemplate();
                if($htmlFiles !== ''){
                    $html = '<div class="filesblock">';
                    $html .= '<ul class="qq-upload-list">';
                    $html .= $htmlFiles;
                    $html .= '</ul></div>';
                    echo $html;
                }

            }
        }

	}

    private function getFileTemplate(){
        //Формируем шаблон вывода файлов
        $htmlFiles = '';
        foreach($this->files as $f){
            
            $filepath = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . $f->file;
            if(file_exists($filepath)){
                $filename = explode("/",$f->file);  
                $filename = end($filename);
                $tmp = explode('.', $filename);
                $ext = end($tmp);

                $filesize = $this->_format_bytes(filesize($filepath));

                $htmlFiles .= '<li class="qq-upload-success file' . $f->id . '">';
                $htmlFiles .= '<div class="block_file">';

                switch($ext){
                    case 'zip':
                        $htmlFiles .= '<img src="' . Yii::app()->theme->baseUrl . '/images/ico_zip.png" width="41" height="48" alt="ZIP"/>';
                        break;
                    case 'png':
                        $htmlFiles .= '<img src="' . Yii::app()->theme->baseUrl . '/images/ico_png.png" width="41" height="48" alt="PNG"/>';
                        break;
                    case 'doc':
                    case 'docx':
                        $htmlFiles .= '<img src="' . Yii::app()->theme->baseUrl . '/images/ico_doc.png" width="41" height="48" alt="PNG"/>';
                        break;
                     case 'rar':
                        $htmlFiles .= '<img src="' . Yii::app()->theme->baseUrl . '/images/ico_rar.png" width="41" height="48" alt="RAR"/>';
                        break;
                    case 'xls':
                        $htmlFiles .= '<img src="' . Yii::app()->theme->baseUrl . '/images/ico_xls.png" width="41" height="48" alt="XLS"/>';
                        break;
                    case 'ppt':
                        $htmlFiles .= '<img src="' . Yii::app()->theme->baseUrl . '/images/ico_ppt.png" width="41" height="48" alt="ppt"/>';
                        break;
                    case 'txt':
                        $htmlFiles .= '<img src="' . Yii::app()->theme->baseUrl . '/images/ico_txt.png" width="41" height="48" alt="TXT"/>';
                        break;
                     case 'pdf':
                        $htmlFiles .= '<img src="' . Yii::app()->theme->baseUrl . '/images/ico_pdf.png" width="41" height="48" alt="PDF"/>';
                        break;                   
                    case 'jpg':
                     case 'jpeg':   
                        $htmlFiles .= '<img src="' . Yii::app()->theme->baseUrl . '/images/ico_jpg.png" width="41" height="48" alt="JPG"/>';
                        break;
                     case 'tiff':
                        $htmlFiles .= '<img src="' . Yii::app()->theme->baseUrl . '/images/ico_tiff.png" width="41" height="48" alt="TIFF"/>';
                        break;                   
                    default:
                        $htmlFiles .= '<img src="' . Yii::app()->theme->baseUrl . '/images/ico_vopros.png" width="41" height="48" alt="PNG"/>';
                }

                $htmlFiles .= '<a class="file-link" href="' . Yii::app()->baseUrl . '/' . $f->file . '"><span class="qq-upload-file">' . $f->desc . '</span></a><br/>';
                $htmlFiles .= '<span>.' . $ext . ' ' . $filesize . '</span><br/>';

                if($this->mode == 'edit'){
                    $htmlFiles .= '<a class="red" onclick="delfile(' . $f->id . '); return false;" href="#">удалить</a>&nbsp;';
                    $htmlFiles .= '<a class="black" onclick="renamefile(' . $f->id . '); return false;" href="#">переименовать</a>';
                }

                $htmlFiles .= '</div>';
                $htmlFiles .= '</li>';
                //$htmlFiles .= '<li class="uploadedFile" id="uploadFile' . $f->id . '" style="display: none;">';
                //$htmlFiles .= '<input type="hidden" value="'. $f->id .'" name="files[]" class="filesav">';
                //$htmlFiles .= '</li>';
            }
        }
        return $htmlFiles;
    }

    private function _format_bytes($a_bytes)
    {
        if ($a_bytes < 1024) {
            return $a_bytes .' байт';
        } elseif ($a_bytes < 1048576) {
            return round($a_bytes / 1024, 2) .' кб';
        } elseif ($a_bytes < 1073741824) {
            return round($a_bytes / 1048576, 2) . ' мб';
        } elseif ($a_bytes < 1099511627776) {
            return round($a_bytes / 1073741824, 2) . ' гб';
        } elseif ($a_bytes < 1125899906842624) {
            return round($a_bytes / 1099511627776, 2) .' тб';
        } elseif ($a_bytes < 1152921504606846976) {
            return round($a_bytes / 1125899906842624, 2) .' PiB';
        } elseif ($a_bytes < 1180591620717411303424) {
            return round($a_bytes / 1152921504606846976, 2) .' EiB';
        } elseif ($a_bytes < 1208925819614629174706176) {
            return round($a_bytes / 1180591620717411303424, 2) .' ZiB';
        } else {
            return round($a_bytes / 1208925819614629174706176, 2) .' YiB';
        }
    }



}