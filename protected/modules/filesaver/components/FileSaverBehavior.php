<?php
yii::import('filesaver.models.Files');
class FileSaverBehavior extends CActiveRecordBehavior
{
      public $savelocation;
      public $entity;

      public function afterSave($event)
      {
            if(isset($_POST['files']))
            {
                  foreach($_POST['files'] as $f){
                      $File = Files::model()->findByPk($f);

                      $File->savelocation = $this->savelocation;
                      $File->entity = $this->entity;
                      $File->entity_pk = $this->owner->id;

                      $newPath = str_replace('temp',$this->savelocation,$File->file);

                      if (!is_dir(dirname($newPath)))
                          mkdir(dirname($newPath), 0777, true);

                      if (!file_exists(yii::getPathOfAlias('webroot') . '/' . $newPath)) {
                          copy(yii::getPathOfAlias('webroot') . '/' . $File->file,yii::getPathOfAlias('webroot') . '/' . $newPath);
                          @unlink(yii::getPathOfAlias('webroot').'/' . $File->file);
                      }
                      $File->file = $newPath;

                      //Если Файл не переименовывали при первой загрузке поле desc - пустое
                      if($File->desc == ''){
                          $filename = explode("/",$newPath);
                          $filename = end($filename);
                          $tmp = explode('.', $filename);
                          $File->desc = $tmp[0];
                      }

                      $File->save();
                  }
            }
      }

      public function getFilesSaver()
      {
          return Files::model()->findAll('entity=:entity AND entity_pk=:pid', array(':entity' => $this->entity, ':pid' => $this->owner->id));
      }

    public function getHasFiles()
    {
        return Files::model()->exists('entity=:entity AND entity_pk=:pid', array(':entity' => $this->entity, ':pid' => $this->owner->id));
    }
}
