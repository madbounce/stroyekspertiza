<?php
class DefaultController extends Controller
{
    /*
     * Ajax action - удаление файла
     */
	public function actionDelete() {
		if (Yii::app()->request->isAjaxRequest) {
			if (isset($_POST['id'])) {
				$file = Files::model()->findByPk($_POST['id']);
				if (!is_null($file))
				{
					@unlink($file->file);
					list($folder,$module,$filename) = explode("/",$file->file);
                    $filename = substr($filename, 0,strrpos($filename,'.'));
                    $thumbname = glob($folder.'/'.$module.'/'.$filename.'*');
                    array_map("unlink",$thumbname);
					$file->delete();
					echo json_encode(array('success'=>'true'));
				} else {
					echo 'error';
				}				
			}
			Yii::app()->end();
		}
	}

    /*
    * Ajax action - переименование файла
    */
    public function actionRename() {
        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['id'], $_POST['name'])) {
                $file = Files::model()->findByPk((int)$_POST['id']);
                if (!is_null($file)){
                    $newName = $_POST['name']; //Новое имя файла
                    $file->desc = $newName;
                    if ($file->save()){
                        echo json_encode(array('status'=>'success'));
                    } else
                        echo json_encode(array('status'=>'failure'));
                 } else
                    echo json_encode(array('status'=>'failure'));
            }
            Yii::app()->end();
        }
    }
    
 


    public function actionUpload()
    {
        set_time_limit(0);

        Yii::import("filesaver.components.qqFileUploader");
        Yii::import("application.modules.filesaver.models.Files");

        $folder='files/temp/';// folder for uploaded files
        if (!is_dir(Yii::getPathOfAlias('webroot') . '/' . $folder))
            mkdir(Yii::getPathOfAlias('webroot') . '/' . $folder, 0777, true);

        $allowedExtensions = array("png","jpg","jpeg","gif","mov", "doc", "docx", "xls", "xlsx", "odt", "ppt", "pptx", "rar", "zip", "ai", "eps", "psd", "svg", "pdf", "txt");
        $sizeLimit = 200 * 1024 * 1024;// maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);


        $File = new Files();
        $File->user_id = Yii::app()->user->id;
        $File->file = Files::TempPath.'/'.$result['filename'];
        $File->desc = $result['original_name'];
        $File->entity = 'temp';
        $File->entity_pk = -1;

        if($File->save())
        {
            $result['temp_id']=$File->id;
        } else {
            die(print_r($File->getErrors()));
        }


        $jresult=htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $jresult;// it's array
        die();
    }

    public function actionGetRenameDialog(){
        if(Yii::app()->request->isAjaxRequest){
            Yii::app()->clientScript->scriptMap = array(
                'jquery.js' => false,
                'jquery.min.js' => false,
                'jquery.yii.js' => false,
                'jquery-ui.min.js' => false,
            );

            $id = (int) $_POST['id'];
            $html = $this->renderPartial('rename_form', array('id' => $id), true, true);
            echo $html;
            Yii::app()->end();
        }
    }
}