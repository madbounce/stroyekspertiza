<?php

class Files extends CActiveRecord
{
  
    public $savelocation = 'files';
    const TempPath = 'files/temp';

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{files}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, entity, entity_pk', 'required'),
			array('user_id, entity_pk, num, default', 'numerical', 'integerOnly'=>true),
			array('file', 'length', 'max'=>128),
			array('desc', 'length', 'max'=>255),
			array('entity', 'length', 'max'=>25),
			array('insert_date, modarated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, file, desc, insert_date, modarated, entity, entity_pk, num, default', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'file' => 'File',
			'desc' => 'Desc',
			'insert_date' => 'Insert Date',
			'modarated' => 'Modarated',
			'entity' => 'Entity',
			'entity_pk' => 'Entity Pk',
			'num' => 'Num',
			'default' => 'Default',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('insert_date',$this->insert_date,true);
		$criteria->compare('modarated',$this->modarated,true);
		$criteria->compare('entity',$this->entity,true);
		$criteria->compare('entity_pk',$this->entity_pk);
		$criteria->compare('num',$this->num);
		$criteria->compare('default',$this->default);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}