<?php $this->beginWidget('DialogWidget', array(
    'id' => 'rename-file-dialog',
    'title' => '<h1 class="popup-title title" style="font-size: 20px; text-align: center;">' . Yii::t('tender', 'Введите название файла') . '</h1>',
    'autoOpen' => true,
    'options' => array(
        'close' => 'js:function(event, ui) {
             $("#rename-file-dialog").remove();
        }',
    ),
)); ?>

<div class="inviteblock">
    <div class="formline1">
        <?php echo CHtml::textField('new_file_name', null,array('class' => "input200", 'id' => 'new_file_name')); ?>
    </div>

    <?php echo CHtml::link(Yii::t('tender', 'применить'), '#', array(
        'id' => 'filename_submit',
        'class' => 'greenbtn sm',
        'data-id' => $id,
    )); ?>
</div>

<?php $this->endWidget(); ?>