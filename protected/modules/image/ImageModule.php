<?php

class ImageModule extends CWebModule
{
    public $imagePathAlias = 'webroot.images';
    public $tmpPathAlias = 'webroot.images.tmp';

    public $sessionName = '__images';

	public function init()
	{
        if (empty($this->imagePathAlias))
            throw new CException(Yii::t('image', 'ImageModule.imagePathAlias property must be set.'));

        if (empty($this->tmpPathAlias))
            throw new CException(Yii::t('image', 'ImageModule.tmpPathAlias property must be set.'));

		$this->setImport(array(
            'image.ImageModule',
			'image.models.*',
			'image.components.*',
            'image.components.widgets.*',
            'image.extensions.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}

    public static function getUrlOfAlias($alias, $includeBase = true)
    {
        if ($path = Yii::getPathOfAlias($alias))
        {
            $webrootPath = Yii::getPathOfAlias('webroot');
            if(strpos($path, $webrootPath) !== FALSE) {
                $path = substr($path, strlen($webrootPath));
            }
            return str_replace('\\', '/', $includeBase ? Yii::app()->baseUrl . $path : $path);
        }

        return false;
    }

    public static function getPath()
    {
        return Yii::getPathOfAlias(Yii::app()->getModule('image')->imagePathAlias);
    }

    public static function getUrl($includeBase = true)
    {
        return self::getUrlOfAlias(Yii::app()->getModule('image')->imagePathAlias, $includeBase);
    }

    public static function getTmpPath()
    {
        return Yii::getPathOfAlias(Yii::app()->getModule('image')->tmpPathAlias);
    }

    public static function getTmpUrl($includeBase = true)
    {
        return self::getUrlOfAlias(Yii::app()->getModule('image')->tmpPathAlias, $includeBase);
    }

    public static function getImages($entity, $entity_id, $tag, $multiple = true)
    {
        if ($multiple) {
            if (Yii::app()->user->isGuest) {
                $images = Image::model()->entity($entity, $entity_id)->tag($tag)->session(Yii::app()->session->sessionID)->findAll();
            } else {
                $images = Image::model()->entity($entity, $entity_id)->tag($tag)->user(Yii::app()->user->id)->findAll();
            }
        } else {
            if (Yii::app()->user->isGuest) {
                $images = Image::model()->entity($entity, $entity_id)->tag($tag)->session(Yii::app()->session->sessionID)->last()->find();
            } else {
                $images = Image::model()->entity($entity, $entity_id)->tag($tag)->user(Yii::app()->user->id)->last()->find();
            }
        }

        if (!empty($images) && !is_array($images))
            $images = array($images);

        return $images;
    }
}
