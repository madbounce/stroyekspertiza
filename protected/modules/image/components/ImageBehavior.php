<?php

Yii::import('image.ImageModule');

class ImageBehavior extends CActiveRecordBehavior
{
    public $tag;
    public $multiple = true;

    protected $entity;

    public function attach($owner)
    {
        parent::attach($owner);

        $this->entity = get_class($this->owner);

        $this->getOwner()->getMetaData()->addRelation('images',
            array(CActiveRecord::HAS_MANY, 'Image', 'entity_id',
                'condition' => 'entity = :entity', 'params' => array(':entity' => $this->entity))
        );

        if ($this->multiple) {
            $this->getOwner()->getMetaData()->addRelation($this->tag,
                array(CActiveRecord::HAS_MANY, 'Image', 'entity_id',
                    'condition' => 'entity = :entity AND tag = :tag', 'params' => array(':entity' => $this->entity, ':tag' => $this->tag))
            );
        } else {
            $this->getOwner()->getMetaData()->addRelation($this->tag,
                array(CActiveRecord::HAS_ONE, 'Image', 'entity_id',
                    'condition' => 'entity = :entity AND tag = :tag', 'params' => array(':entity' => $this->entity, ':tag' => $this->tag))
            );
        }
    }

    public function afterSave($event)
    {
        $this->saveImages();
    }

    protected function deletePreviousImages()
    {
        $temporaryCriteria = Image::model()->getEntityCriteria($this->entity);
        $temporaryCriteria->mergeWith(Image::model()->getTemporaryCriteria());

        $criteria = Image::model()->getEntityCriteria($this->entity, $this->owner->primaryKey);
        $criteria->mergeWith($temporaryCriteria, false);
        $criteria->mergeWith(Image::model()->getTagCriteria($this->tag));

        if (Yii::app()->user->isGuest) {
            $criteria->mergeWith(Image::model()->getSessionCriteria(Yii::app()->session->sessionID));
            $images = Image::model()->findAll($criteria);
        } else {
            $criteria->mergeWith(Image::model()->getUserCriteria(Yii::app()->user->id));
            $images = Image::model()->findAll($criteria);
        }

        $count = count($images);
        for ($i = 0; $i < $count - 1; $i++)
            $images[$i]->delete();
    }

    protected function saveImages()
    {
        if (!$this->multiple)
            $this->deletePreviousImages();

        if (Yii::app()->user->isGuest) {
            $images = Image::model()->temporary()->entity($this->entity)->tag($this->tag)->session(Yii::app()->session->sessionID)->findAll();
        } else {
            $images = Image::model()->temporary()->entity($this->entity)->tag($this->tag)->user(Yii::app()->user->id)->findAll();
        }

        if (!empty($images)) {
            foreach ($images as $image) {
                $image->entity_id = $this->owner->primaryKey;
                $image->save();
            }
        }
    }

    public function beforeDelete($event)
    {
        parent::beforeDelete($event);

        foreach ($this->owner->images as $image)
            $image->delete();
    }
}
