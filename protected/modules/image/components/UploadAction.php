<?php

Yii::import('image.ImageModule');

class UploadAction extends CAction
{
    public $entity;
	public $entity_id;
    public $tag;
    public $directory = null;
    public $multiple = true;

    public $view;
    public $accessCallback;

    protected function getName()
    {
        return $this->entity . '_' . $this->tag;
    }

    protected function generateFileName($file)
    {
        return md5(microtime() . $file->name) . '.' . $file->getExtensionName();
    }

    public function accessCallback($image)
    {
        $accessCallback = $this->accessCallback;
        $result = $accessCallback($image);
        return $result;
    }

    public function init()
    {
        if (!is_callable($this->accessCallback)) {
            $this->accessCallback = function($image)
            {
                return $image->user_id == Yii::app()->user->id || $image->session == Yii::app()->session->sessionID;
            };
        }
    }

    public function run()
    {
        $this->init();

        if (empty($this->entity))
            throw new CException('UploadAction.entity property must be set.');

        if (empty($this->tag))
            throw new CException('UploadAction.tag property must be set.');

        if (isset($_POST['method']) && $_POST['method'] == 'upload') {
            $files = CUploadedFile::getInstancesByName($this->getName());

			if (!empty($files)) {
	
                foreach ($files as $file) {
                    $filename = $this->generateFileName($file);

                    $uploadPath = !empty($this->directory) ?
                        ImageModule::getPath() . '/' . $this->directory :
                        ImageModule::getPath();

                    if (!is_dir($uploadPath))
                        mkdir($uploadPath, 0777, true);

                    $file->saveAs($uploadPath . '/' . $filename);

                    $img = new Image;
                    $img->entity = $this->entity;
					$img->entity_id = $_POST['entity_id'] ? $_POST['entity_id'] : null;
                    $img->tag = $this->tag;
                    $img->url = !empty($this->directory) ?
                        ImageModule::getUrl(false) . '/' . $this->directory . '/' . $filename :
                        ImageModule::getUrl(false) . '/' . $filename;
                    $img->filename = $filename;
                    $img->size = $file->size;
                    $img->mime_type = $file->type;
                    $img->name = $file->name;
                    if (Yii::app()->user->isGuest)
                        $img->session = Yii::app()->session->sessionID;
                    else
                        $img->user_id = Yii::app()->user->id;

                    if (!$img->save()) {
                        echo CJSON::encode(array('success' => false));
                        Yii::app()->end();
                    }
                }

                echo CJSON::encode(array(
                    'success' => true,
                    'html' => $this->controller->renderPartial($this->view, array(
                        'images' => ImageModule::getImages($this->entity, isset($_POST['entity_id']) ? $_POST['entity_id'] : null, $this->tag, $this->multiple)
                    ), true)
                ));
                Yii::app()->end();
            }
        } else if (isset($_POST['method']) && $_POST['method'] == 'delete') {
            if (isset($_POST['id'])) {
                $model = Image::model()->findByPk($_POST['id']);

                if (!empty($model) && $this->accessCallback($model)) {
                    if ($model->delete()) {
                        echo CJSON::encode(array(
                            'success' => true,
                            'html' => $this->controller->renderPartial($this->view, array(
                                'images' => ImageModule::getImages($this->entity, isset($_POST['entity_id']) ? $_POST['entity_id'] : null, $this->tag, $this->multiple)
                            ), true)
                        ));
                        Yii::app()->end();
                    }
                }

                echo CJSON::encode(array('success' => false));
                Yii::app()->end();
            }
        }
    }
}
