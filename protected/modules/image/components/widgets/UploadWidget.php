<?php

class UploadWidget extends CWidget
{
    public $model;
    public $tag;
    public $action;
    public $multiple = true;

    public $uploadButtonText;
    public $view = 'images';

    protected $_url;

    public function init()
    {
        parent::init();
	Yii::import('application.modules.image.*');

        if (empty($this->uploadButtonText))
            $this->uploadButtonText = Yii::t('image', 'Загрузить');

        $this->_url = CHtml::normalizeUrl($this->action);

        $this->publishAssets();
    }

    public function run()
    {
        $options = array(
            'dataType' => 'json',
            'formData' => array('method' => 'upload', 'entity_id' => $this->model->primaryKey),
            'done' => 'js:function(e, data) {
                if (data.result.success)
                    $(".' . $this->id . ' .uploaded-images").html(data.result.html);
            }'
        );
        $options = CJavaScript::encode($options);
        Yii::app()->clientScript->registerScript(__CLASS__ . "#{$this->id}",
            "jQuery('#{$this->id}').fileupload($options)", CClientScript::POS_READY);

        $this->renderInput();
        $this->renderContent();
    }

    protected function renderInput()
    {
        echo CHtml::tag('input', array(
            'type' => 'file',
            'id'  => $this->id,
            'name' => get_class($this->model) . '_' . $this->tag,
            'multiple' => $this->multiple ? 'multiple' : '',
            'data-url' => $this->_url,
            'style' => 'width: 1px; display: none;')
        );
    }

    protected function getImages()
    {
        return ImageModule::getImages(get_class($this->model), $this->model->primaryKey, $this->tag, $this->multiple);
    }

    protected function renderContent()
    {
        echo '<div class="' . $this->id . '">';
        echo '<div class="uploaded-images">';
        $this->renderUploadedImages($this->getImages());
        echo '</div>';
        $this->renderUploadButton();
        echo '</div>';
    }

    protected function renderUploadedImages($images)
    {
        $this->render($this->view, array('images' => $images));
    }

    protected function renderUploadButton()
    {
        echo CHtml::link($this->uploadButtonText, '#', array('class' => 'upload-button btn btn-primary'));
    }

    public function publishAssets()
    {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.ui.widget.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.iframe-transport.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.fileupload.js', CClientScript::POS_HEAD);

        Yii::app()->clientScript->registerScript(__CLASS__ . "#{$this->id}-button", '
        $(".' . $this->id . ' .upload-button").live("click", function() {
            $("#' . $this->id . '").click();
            return false;
        });
        ', CClientScript::POS_READY);

        Yii::app()->clientScript->registerScript(__CLASS__ . "#{$this->id}-delete", '
        $(".' . $this->id . ' .delete-link").live("click", function() {
            var id = $(this).attr("data-id");
            $.ajax({
                url: "' . $this->_url . '",
                type: "POST",
                data: ' . CJavaScript::encode(array('method' => 'delete', 'id' => 'js:id', 'entity_id' => $this->model->primaryKey)) .',
                dataType: "json",
                success: function(result) {
                    if (result.success)
                        $(".' . $this->id . ' .uploaded-images").html(result.html);
                }
            });
            return false;
        });
        ', CClientScript::POS_READY);
    }
}
