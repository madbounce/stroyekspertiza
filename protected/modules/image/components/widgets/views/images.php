<?php if (!empty($images)): ?>
    <ul class="thumbnails">
        <?php foreach ($images as $image): ?>
            <li class="span3">
                <a href="<?php echo Yii::app()->baseUrl . $image->url; ?>" class="thumbnail" rel="image">
                    <?php echo CHtml::image(Yii::app()->baseUrl . ImageHelper::thumb(200, 150, $image->url, array('method' => 'adaptiveResize'))); ?>
                </a>
                <?php echo CHtml::link(Yii::t('image', 'Удалить'), '#', array('class' => 'delete-link', 'data-id' => $image->id)); ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>