<?php if (!empty($images)): ?>
<ul class="thumbnails">
    <?php foreach ($images as $image): ?>
    <li class="thumb">
        <div class="polaroid">
            <?php echo CHtml::image(Yii::app()->baseUrl . ImageHelper::thumb(100, 75, $image->url, array('method' => 'adaptiveResize')), '' , array('class' => 'img-polaroid')); ?>
            <?php echo CHtml::hiddenField('photos[]',$image->id); ?>
        </div>
        <?php echo CHtml::link(Yii::t('image', 'Удалить'), '#', array('class' => 'delete-link', 'data-id' => $image->id)); ?>
    </li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>
