<?php if (!empty($images)): ?>
<ul class="thumbnails">
    <?php foreach ($images as $image): ?>
    <li class="thumb">
        <div class="polaroid">
            <?php echo CHtml::image(Yii::app()->baseUrl . ImageHelper::thumb(400, 0, $image->url, array('method' => 'resize')), '' , array('class' => 'img-polaroid')); ?>
        </div>
        <?php echo CHtml::link(Yii::t('image', 'Удалить'), '#', array('class' => 'delete-link', 'data-id' => $image->id)); ?>
    </li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>
