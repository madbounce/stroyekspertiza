DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity` varchar(32) DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `tag` varchar(32) DEFAULT NULL,
  `url` varchar(512) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `mime_type` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `create_time` datetime NOT NULL,
  `session` varchar(255) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;