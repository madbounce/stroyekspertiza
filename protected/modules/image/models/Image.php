<?php

/**
 * This is the model class for table "image".
 *
 * The followings are the available columns in table 'image':
 * @property string $id
 * @property string $entity
 * @property string $entity_id
 * @property string $tag
 * @property string $url
 * @property string $filename
 * @property string $size
 * @property string $mime_type
 * @property string $name
 * @property string $create_time
 * @property string $session
 * @property string $user_id
 */
class Image extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{image}}';
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'updateAttribute' => null
            )
        );
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteFile();
            return true;
        }

        return false;
    }

    protected function deleteFile()
    {
        $count = Image::model()->countByAttributes(array('url' => $this->url));
        $path = Yii::getPathOfAlias('webroot') . '/' . $this->url;
        if (is_file($path) && $count == 1)
            unlink($path);
    }

    /**
     * General
     */

    public function thumb($width, $height)
    {
        return Yii::app()->baseUrl . ImageHelper::thumb($width, $height, $this->url, array('method' => 'adaptiveResize'));
    }

    /**
     * Criteria
     */

    public function getEntityCriteria($entity, $entity_id = null, $withTemporary = true)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addCondition("$alias.entity = :entity");
        $criteria->params[':entity'] = $entity;

        if (!empty($entity_id)) {
            if ($withTemporary)
                $criteria->addCondition("$alias.entity_id = :entity_id OR $alias.entity_id IS NULL");
            else
                $criteria->addCondition("$alias.entity_id = :entity_id");
            $criteria->params[':entity_id'] = $entity_id;
        } else {
            $criteria->addCondition("$alias.entity_id IS NULL");
        }

        return $criteria;
    }

    public function getTagCriteria($tag)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addCondition("$alias.tag = :tag");
        $criteria->params[':tag'] = $tag;

        return $criteria;
    }

    public function getSessionCriteria($session)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addCondition("$alias.session = :session");
        $criteria->params[':session'] = $session;

        return $criteria;
    }

    public function getUserCriteria($user_id)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addCondition("$alias.user_id = :user_id");
        $criteria->params[':user_id'] = $user_id;

        return $criteria;
    }

    public function getTemporaryCriteria()
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addCondition("$alias.entity_id IS NULL");
        return $criteria;
    }

    public function getLastCriteria()
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->order = "$alias.create_time DESC";
        return $criteria;
    }

    /**
     * Scopes
     */

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);
        return array(
            'order' => "$alias.create_time",
        );
    }

    public function entity($entity, $entity_id = null, $withTemporary = true)
    {
        $this->getDbCriteria()->mergeWith($this->getEntityCriteria($entity, $entity_id, $withTemporary));
        return $this;
    }

    public function tag($tag)
    {
        $this->getDbCriteria()->mergeWith($this->getTagCriteria($tag));
        return $this;
    }

    public function session($session)
    {
        $this->getDbCriteria()->mergeWith($this->getSessionCriteria($session));
        return $this;
    }

    public function user($user_id)
    {
        $this->getDbCriteria()->mergeWith($this->getUserCriteria($user_id));
        return $this;
    }

    public function temporary()
    {
        $this->getDbCriteria()->mergeWith($this->getTemporaryCriteria());
        return $this;
    }

    public function last()
    {
        $this->getDbCriteria()->mergeWith($this->getLastCriteria());
        return $this;
    }
}