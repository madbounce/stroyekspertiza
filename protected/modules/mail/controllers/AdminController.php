<?php

class AdminController extends BackEndController
{
    public $layout = '//layouts/main_config';

    public function actions()
    {
        return array(
            'sortable' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'Mail'
            )
        );
    }

    public function actionView($id)
    {
        $this->renderPartial('/default/view', array('content' => $this->loadModel($id)->template));
    }

    public function actionCreate()
    {
        $model = new Mail;

        $this->performAjaxValidation($model);

        if (isset($_POST['Mail'])) {
            $model->attributes = $_POST['Mail'];
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, true);

        $this->performAjaxValidation($model);

        if (isset($_POST['Mail'])) {
            $model->attributes = $_POST['Mail'];
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $model = new Mail('search');
        $model->unsetAttributes();
        if (isset($_GET['Mail']))
            $model->attributes = $_GET['Mail'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id, $ml = false)
    {
        if ($ml) {
            $model = Mail::model()->multilang()->findByPk($id);
        } else {
            $model = Mail::model()->findByPk($id);
        }
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'mail-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
