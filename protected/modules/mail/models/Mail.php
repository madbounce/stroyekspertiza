<?php

/**
 * This is the model class for table "mail".
 *
 * The followings are the available columns in table 'mail':
 * @property string $id
 * @property string $name
 * @property string $subject
 * @property string $template
 */
class Mail extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{mail}}';
    }

    public function rules()
    {
        return array(
            array('name, subject, template', 'required'),
            array('name, subject', 'length', 'max' => 255),
            array('sort', 'numerical', 'integerOnly'=>true),

            array('id, name, subject, template', 'safe', 'on' => 'search'),
        );
    }



    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'name' => Yii::t('mail', 'Название'),
            'subject' => Yii::t('mail', 'Тема'),
            'template' => Yii::t('mail', 'Шаблон'),
            'sort' => Yii::t('mail', 'Сортировка'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('subject', $this->subject, true);
        $criteria->compare('template', $this->template, true);

        $criteria->order = 'sort DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors()
    {
        return array(
            'ml' => array(
                'class' => 'translate.extensions.CustomMultilingualBehavior',
                'langTableName' => 'mail_localization',
                'localizedAttributes' => array('subject', 'template'),
                'languages' => TranslateModule::getLanguages(),
                'defaultLanguage' => Yii::app()->sourceLanguage,
            ),
            'SortBehavior' => array(
                'class' => 'ext.behaviors.SortBehavior',
            ),
        );
    }

    /**
     * Scopes
     */

    public function defaultScope()
    {
        return $this->ml->localizedCriteria();
    }
}