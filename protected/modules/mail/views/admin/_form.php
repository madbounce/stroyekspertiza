<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'mail-form',
    'enableAjaxValidation' => true,
	'htmlOptions' => array(
        'class' => 'product'
    ),
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>

<div id="name">
    <?php echo $form->textFieldRow($model, 'name', array('class' => 'span5 name full', 'maxlength' => 255)); ?>
</div>

<div class="block layer" style="width: auto; min-height: auto;">
<h2>Параметры</h2>
<p class="help-block"><?php echo Yii::t('app', 'Поля помеченные <span class="required">*</span> обязательны для заполнения.'); ?></p>

<?php echo $form->errorSummary($model); ?>

<?php //echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php foreach (TranslateModule::getLanguageModels() as $language):
    if ($language->language == Yii::app()->sourceLanguage) $suffix = '';
    else $suffix = '_' . $language->language;
?>

<fieldset>
    <legend><?php echo $language->name; ?></legend>

    <?php echo $form->labelEx($model, 'subject'); ?>
    <?php echo $form->textField($model, 'subject' . $suffix, array('class' => 'span8', 'maxlength' => 255)); ?>
    <?php echo $form->error($model, 'subject' . $suffix); ?>

    <?php echo $form->labelEx($model, 'template'); ?>
    <?php $this->widget('application.extensions.redactorjs.ERedactorWidget', array(
        'model' => $model,
        'attribute' => 'template' . $suffix,
        'options' => array(
            'lang' => 'ru',
        ),
    )); ?>
    <?php echo $form->error($model, 'template' . $suffix); ?>
</fieldset>
<?php endforeach; ?>

</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? Yii::t('mail', 'Создать') : Yii::t('mail', 'Сохранить'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
