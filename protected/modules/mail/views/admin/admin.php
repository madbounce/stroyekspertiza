<?php
$dp = $model->search();
$countAll = $dp->totalItemCount;
?>
<div class="page-header admin-header">
    <h3><?php echo Yii::t('app', '{n} шаблон|{n} шаблона|{n} шаблонов', $countAll);?></h3>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => Yii::t('translate', 'Добавить шаблон'),
        'size' => 'small',
        'url' => array('/mail/admin/create')
    ));
    ?>
</div>

<div class="page-content">
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'id' => 'mail-grid',
    'type'=>'striped bordered',
    'dataProvider' => $dp,
    //'filter' => $model,
    'enableSorting' => false,
    'sortableRows' => true,
    'sortableAjaxSave'=>true,
    'sortableAttribute'=>'sort',
    'sortableAction'=> 'mail/admin/sortable',
    'template' => '{pager}{items}{pager}',
    'columns' => array(
        array(
            'name' => 'name',
            'type'=>'raw',
            'value' => 'CHtml::link($data->name, array("/mail/admin/update", "id" => $data->id))',
        ),
        'subject',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}'
        ),
    ),
)); ?>
</div>