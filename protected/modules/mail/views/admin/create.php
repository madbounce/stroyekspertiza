<?php
$this->breadcrumbs = array(
    Yii::t('mail', 'Почта') => array('admin'),
    Yii::t('mail', 'Создание шаблона'),
);
?>

<div class="page-header">
    <h3><?php echo Yii::t('mail', 'Создание шаблона'); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>