<?php

class LatestNewsWidget extends CWidget
{
    public $limit = 5;

    public function run()
    {
        $items = News::model()->latest($this->limit)->findAll();

        if (!empty($items))
            $this->render('latestNews', array('items' => $items));
    }
}
