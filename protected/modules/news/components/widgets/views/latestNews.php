<div class="news">
    <?php foreach ($items as $item): ?>
        <div class="news-item">
            <div class="date">
                <?php echo Yii::app()->dateFormatter->formatDateTime($item->create_time, 'medium', null); ?>
            </div>

            <div class="title">
                <?php echo CHtml::link(CHtml::encode($item->title), array('/news/default/view', 'id' => $item->id)); ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
