<?php

class DefaultController extends FrontEndController
{
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('News');
        $this->render('index', array('dataProvider' => $dataProvider));
    }

	public function actionView($id)
	{
        $model = $this->loadModel($id);
		$this->render('view', array('model' => $model));
	}

    public function loadModel($id, $ml = false)
    {
        if ($ml) {
            $model = News::model()->multilang()->findByPk($id);
        } else {
            $model = News::model()->findByPk($id);
        }
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        return $model;
    }
}