<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property string $id
 * @property string $title
 * @property string $content_short
 * @property string $content
 * @property string $create_time
 * @property string $update_time
 *
 */
class News extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{news}}';
    }

    public function rules()
    {
        return array(
            array('title, content_short, content', 'required'),
            array('title, content_short', 'length', 'max' => 512),

            array('id, title, content_short, content, create_time, update_time', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => Yii::t('news', 'Заголовок'),
            'content_short' => Yii::t('news', 'Короткое описание'),
            'content' => Yii::t('news', 'Описание'),
            'create_time' => Yii::t('news', 'Дата создания'),
            'update_time' => Yii::t('news', 'Дата обновления'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('content_short', $this->content_short, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
            ),
            'ml' => array(
                'class' => 'translate.extensions.MultilingualBehavior',
                'langTableName' => 'news_localization',
                'localizedAttributes' => array('title', 'content_short', 'content'),
                'languages' => TranslateModule::getLanguages(),
                'defaultLanguage' => Yii::app()->sourceLanguage,
            ),
            'CommentableBehavior' => array(
                'class' => 'comment.components.CommentableBehavior',
            ),
            'image' => array(
                'class' => 'image.components.ImageBehavior',
                'tag' => 'image',
                'multiple' => false,
            ),
        );
    }

    /**
     * Criteria
     */

    public function getLatestCriteria($limit = null)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->order = "$alias.create_time DESC";
        if (!empty($limit))
            $criteria->limit = $limit;

        return $criteria;
    }

    /**
     * Scopes
     */

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);
        $criteria = new CDbCriteria(array(
            'order' => "$alias.create_time DESC",
        ));
        $criteria->mergeWith($this->ml->localizedCriteria());
        return $criteria;
    }

    public function latest($limit = 5)
    {
        $this->getDbCriteria()->mergeWith($this->getLatestCriteria($limit));
        return $this;
    }
}