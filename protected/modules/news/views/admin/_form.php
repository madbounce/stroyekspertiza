<?php /** @var $form TbActiveForm */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'news-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>

<p class="help-block"><?php echo Yii::t('app', 'Поля помеченные <span class="required">*</span> обязательны для заполнения.'); ?></p>

<?php echo $form->errorSummary($model); ?>

<?php echo CHtml::label(Yii::t('news', 'Изображение'), ''); ?>
<?php $this->widget('image.components.widgets.UploadWidget', array(
    'model' => $model,
    'tag' => 'image',
    'action' => array('/news/admin/uploadImage'),
    'multiple' => false,
    'uploadButtonText' => Yii::t('news', 'Загрузить изображение'),
)); ?>

<?php foreach (TranslateModule::getLanguageModels() as $language):
    if ($language->language == Yii::app()->sourceLanguage) $suffix = '';
    else $suffix = '_' . $language->language;
?>

<fieldset>
    <legend><?php echo $language->name; ?></legend>

    <?php echo $form->labelEx($model, 'title'); ?>
    <?php echo $form->textField($model, 'title' . $suffix, array('class' => 'span8')); ?>
    <?php echo $form->error($model, 'title' . $suffix); ?>

    <?php echo $form->labelEx($model, 'content_short'); ?>
    <?php echo $form->textArea($model, 'content_short' . $suffix, array('class' => 'span8', 'rows' => 5)); ?>
    <?php echo $form->error($model, 'content_short' . $suffix); ?>

    <?php echo $form->labelEx($model, 'content'); ?>
    <?php $this->widget('application.extensions.redactorjs.ERedactorWidget', array(
        'model' => $model,
        'attribute' => 'content' . $suffix,
        'options' => array(
            'lang' => 'ru',
            'imageUpload' => CHtml::normalizeUrl(array('/news/admin/redactorUploadImage'))
        ),
    )); ?>
    <?php echo $form->error($model, 'content' . $suffix); ?>
</fieldset>

<?php endforeach; ?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('news', 'Создать') : Yii::t('news', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>
