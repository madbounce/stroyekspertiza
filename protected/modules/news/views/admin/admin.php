<?php
$this->breadcrumbs = array(
    Yii::t('news', 'Новости')
);
?>
<div class="page-header admin-header">
    <h3><?php echo Yii::t('mail', 'Новости')?></h3>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'label' => Yii::t('translate', 'Добавить новость'),
    'size' => 'small',
    'url' => array('/news/admin/create'),
));
    ?>
</div>

<div class="page-content">
    <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'id' => 'news-grid',
    'type'=>'striped bordered',
    'dataProvider' => $model->search(),
    'filter' => $model,
    //'sortableRows' => true,
    //'sortableAjaxSave'=>true,
    //'sortableAttribute'=>'sort',
    //'sortableAction'=> 'config/admin/sortable',
    'columns' => array(
        /*array(
            'header' => '#',
            'name' => 'id',
        ),*/
        'title',
        'content_short',
        'create_time',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} {delete}'
        ),
    ),
    )); ?>
</div>