<?php
$this->breadcrumbs = array(
    Yii::t('news', 'Новости') => array('index'),
    $model->title => array('update', 'id' => $model->id),
    Yii::t('news', 'Редактирование новости'),
);
?>

<div class="page-header">
    <h3><?php echo Yii::t('news', 'Редактирование новости :id', array(':id' => '#' . $model->id)); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>