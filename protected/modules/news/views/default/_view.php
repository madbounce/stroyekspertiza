<div class="news">
    <div class="title"><?php echo CHtml::link(CHtml::encode($data->title), array('/news/default/view', 'id' => $data->id)); ?></div>
    <?php if (!empty($data->image)): ?>
        <div class="image"><?php echo CHtml::image($data->image->thumb(200, 150), 'image'); ?></div>
    <?php endif; ?>
    <div class="content"><?php echo $data->content; ?></div>
</div>