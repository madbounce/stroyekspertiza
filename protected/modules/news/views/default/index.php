<?php

$this->pageTitle = Yii::t('news', 'Новости');

$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
));