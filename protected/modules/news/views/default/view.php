<?php
$this->pageTitle = CHtml::encode($model->title);
?>

<div class="news">
    <div class="title"><?php echo CHtml::encode($model->title); ?></div>
    <?php if (!empty($model->image)): ?>
        <div class="image"><?php echo CHtml::image($model->image->thumb(200, 150), 'image'); ?></div>
    <?php endif; ?>
    <div class="content"><?php echo $model->content; ?></div>

    <div class="comments">
        <?php $this->widget('comment.components.widgets.CommentsWidget',array(
            'model' => $model,
        )); ?>
    </div>
</div>