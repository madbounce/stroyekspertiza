<?php

class NewsletterModule extends CWebModule
{
    public $nameModule = 'Рассылка';
    public $showFrontEnd = false;
    public $showBackEnd = true;

	public function init()
	{
		$this->setImport(array(
			'newsletter.models.*',
			'newsletter.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}

    public static function getNewsletterMaterials($model)
    {
        $materials = array();
        if ($model->type == Newsletter::TYPE_LATEST) {
            foreach ($model->entity_type as $class) {
                if ($class == 'Event')
                    $materials = array_merge($materials, Event::model()->events()->latestByInterval($model->periodicity == Newsletter::PERIODICITY_SINGLE ? '1 DAY' : "1 {$model->periodicity}")->findAll());
                elseif ($class == 'Review')
                    $materials = array_merge($materials, Event::model()->reviews()->latestByInterval($model->periodicity == Newsletter::PERIODICITY_SINGLE ? '1 DAY' : "1 {$model->periodicity}")->findAll());
                else
                    $materials = array_merge($materials, $class::model()->latestByInterval($model->periodicity == Newsletter::PERIODICITY_SINGLE ? '1 DAY' : "1 {$model->periodicity}")->findAll());
            }
        } else {
            foreach ($model->entity_id as $entity) {
                $class = $entity['entity'];
                $id = $entity['entity_pk'];

                if ($class == 'Event')
                    $materials[] = Event::model()->events()->findByPk($id);
                elseif ($class == 'Review')
                    $materials[] = Event::model()->reviews()->findByPk($id);
                else
                    $materials[] = $class::model()->findByPk($id);
            }
        }

        return array_filter($materials);
    }

    public static function getNewsletterUsers($model)
    {
        $userCriteria = new CDbCriteria;
        $userCriteria->addCondition('t.subscribed = 1 AND t.email IS NOT NULL AND t.email <> \'\'');
        if ($model->user_type == 'ALL') {
            $users = User::model()->findAll($userCriteria);
        } else {
            $users = User::model()->registeredPast($model->user_type)->findAll($userCriteria);
        }

        return $users;
    }
}
