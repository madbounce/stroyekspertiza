<?php

class SendCommand extends CConsoleCommand
{
    public function actionIndex()
    {
        $models = Newsletter::model()->unsent()->findAll('active = 1');
        foreach ($models as $model) {
            $materials = NewsletterModule::getNewsletterMaterials($model);
            $users = NewsletterModule::getNewsletterUsers($model);

            if (empty($materials) || empty($users))
                continue;

            $model->users_count = count($users);
            $model->links_count = count($materials);
            $model->conversion_day = 0;
            $model->conversion_two_days = 0;
            $model->conversion_week = 0;
            $model->conversion_total = 0;

            $message = new YiiMailMessage;
            $message->view = 'email';
            $message->setBody(array(
                'models' => $materials,
                'newsletter' => $model,
            ), 'text/html');

            $message->setSubject($model->subject);
            $message->from = Config::model()->getValue('adminEmail');

            foreach ($users as $user) {
                $message->setTo($user->email);
                Yii::app()->mail->send($message);
            }

            $model->last_send_time = Yii::app()->dateFormatter->format('yyyy-MM-dd HH:mm:ss', time());
            $model->save();

            /*
            $message->setSubject($model->subject);
            $message->setTo(Config::model()->getValue('adminEmail'));
            $message->setBcc(CHtml::listData($users, 'id', 'email'));

            $message->from = Config::model()->getValue('adminEmail');

            if (Yii::app()->mail->send($message)) {
                $model->last_send_time = Yii::app()->dateFormatter->format('yyyy-MM-dd hh:mm:ss', time());
                $model->save();
            }
            */
        }
    }
}
