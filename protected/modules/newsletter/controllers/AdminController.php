<?php

class AdminController extends Controller
{
    public $layout = '//layouts/main_user';

    public function actions()
    {
        return array(
            'redactorUploadImage' => array(
                'class' => 'application.extensions.redactorjs.ERedactorUploadAction',
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', 'users' => array('@')),
            array('deny', 'users' => array('*'))
        );
    }

    public function actionCreate()
    {
        $model = new Newsletter;

        $this->performAjaxValidation($model);

        if (isset($_POST['Newsletter'])) {
            $model->attributes = $_POST['Newsletter'];
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        if (isset($_POST['Newsletter'])) {
            $model->attributes = $_POST['Newsletter'];
            if ($model->save())
                $this->redirect(array('admin'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $this->render('admin', array(
            'dataProvider' => new CActiveDataProvider('Newsletter'),
        ));
    }

    public function loadModel($id)
    {
        $model = Newsletter::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'newsletter-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAddEntities() {
        if (Yii::app()->request->isAjaxRequest && isset($_POST['data'])) {
            $models = array();

            foreach ($_POST['data'] as $data) {
                $class = $data[1];
                $model = $class::model()->findByPk($data[0]);
                $models[] = $model;
            }
            $models = array_filter($models);

            if (!empty($models)) {
                $html = array();
                foreach ($models as $model) {
                    $html[] = $this->renderPartial('_entity', array('model' => $model), true);
                }

                echo CJSON::encode(array(
                    'status' => 'success',
                    'html' => implode('', $html),
                ));
            } else {
                echo CJSON::encode(array(
                    'status' => 'failure',
                ));
            }

            Yii::app()->end();
        }
    }


    public function actionSendNewsletters($id, $usertype)
    {

       if(Yii::app()->request->isAjaxRequest){
            if($this->SendNewsletter($id, $usertype)) {
                echo 'ok';
                Yii::app()->end();
            }

            echo 'Произошла ошибка';
            Yii::app()->end();
        }
    }

    private function SendNewsletter($id, $usertype)
    {
        $model = $this->loadModel($id);

        $userCriteria = new CDbCriteria;
        $userCriteria->with = array('subscribe');
        $userCriteria->addCondition('t.email IS NOT NULL AND t.email <> \'\'');

//        switch($usertype){
//            //Подписанные
//            case Newsletter::USER_TYPE_SUBSCRIBED:
//                $userCriteria->addCondition('t.iams_subscribe = 1');
//                break;
//        }
        if ($usertype == Newsletter::USER_TYPE_SUBSCRIBED){
            $userCriteria->addCondition('t.iams_subscribe = 1');
        }elseif(preg_match("/f_\d+/", $usertype)){
            $filter = Filters::model()->findByPk(preg_replace("/f_/", "", $usertype));
            if ($filter)
                User::model()->setFilterChartCriteria($userCriteria, $filter->value);
        }        

        $users = User::model()->findAll($userCriteria);

        if (!empty($model) && !empty($users)){

            $from = Config::get('infoEmail');

            foreach ($users as $user) {
                if($user->email != 'admin')
                    Yii::app()->getModule('mail')->send($user->email, $from, 'newsletter', array(
                        'subject' => $model->subject,
                        'message' => $model->message,
                        'siteNameLink' => CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')),
                    ));
            }

            //save last_send_time
            $model->last_send_time = date('Y-m-d H:i:s');
            $model->save(false);

        }

        return true;
    }
}