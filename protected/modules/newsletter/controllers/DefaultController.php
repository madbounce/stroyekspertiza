<?php

class DefaultController extends Controller
{
    public $layout = '//layouts/column2';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', 'users' => array('*')),
            array('deny', 'users' => array('*'))
        );
    }

    public function actionView($id)
    {
        $model = $this->loadModel($id);

        $this->renderPartial('email', array(
            'newsletter' => $model,
            'models' => NewsletterModule::getNewsletterMaterials($model),
        ));
    }

    public function actionRedirect($newsletter, $entity, $entity_id)
    {
        $newsletter = Newsletter::model()->findByPk($newsletter);
        if (!empty($newsletter)) {
            $model = $entity::model()->findByPk($entity_id);
            if (!empty($model)) {
                $newsletter->increaseVisits();
                $route = mb_strtolower($entity);
                $this->redirect(array("/{$route}/default/view", 'alias' => $model->alias));
            }
        }

        $this->redirect(Yii::app()->homeUrl);
    }

    public function loadModel($id)
    {
        $model = Newsletter::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionSend() {
        $commandPath = Yii::getPathOfAlias('newsletter.commands');
        $runner = new CConsoleCommandRunner();
        $runner->addCommands($commandPath);
        $args = array('yiic', 'send');
        ob_start();
        $runner->run($args);
        echo htmlentities(ob_get_clean(), null, Yii::app()->charset);
    }
}