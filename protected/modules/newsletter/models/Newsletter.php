<?php

/**
 * This is the model class for table "{{newsletter}}".
 *
 * The followings are the available columns in table '{{newsletter}}':
 * @property string $id
 * @property string $start_time
 * @property string $periodicity
 * @property integer $active
 * @property integer $type
 * @property string $entity_type
 * @property string $entity_id
 * @property string $user_type
 * @property string $subject
 * @property string $template
 * @property string $last_send_time
 * @property string $users_count
 * @property string $links_count
 * @property string $conversion_day
 * @property string $conversion_two_days
 * @property string $conversion_week
 * @property string $conversion_total
 * @property string $create_time
 * @property string $update_time
 */
class Newsletter extends CActiveRecord
{
    const PERIODICITY_SINGLE = 'SINGLE';
    const PERIODICITY_DAY = 'DAY';
    const PERIODICITY_WEEK = 'WEEK';
    const PERIODICITY_MONTH = 'MONTH';

    const TYPE_LATEST = 0;
    const TYPE_CUSTOM = 1;

    const USER_TYPE_SUBSCRIBED = 1;
    const USER_TYPE_REGISTERED = 2;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{newsletter}}';
    }

    public function rules()
    {
        return array(
            array('subject, message, title', 'required'),
            array('active, type', 'numerical', 'integerOnly' => true),
            array('periodicity, user_type, subject, title', 'length', 'max' => 255),
            //array('entity_type, entity_id', 'length', 'max'=>1024),
            array('users_count, links_count, conversion_day, conversion_two_days, conversion_week, conversion_total', 'length', 'max' => 10),
            array('template, last_send_time, create_time, update_time, message', 'safe'),

            array('id, start_time, periodicity, active, type, entity_type, entity_id, user_type, subject, template, last_send_time, users_count, links_count, conversion_day, conversion_two_days, conversion_week, conversion_total, create_time, update_time', 'safe', 'on' => 'search'),

            array('entity_type, entity_id', 'safe'),
        );
    }

    public function relations()
    {
        return array();
    }

    public function attributeLabels()
    {
        return array(
            'id' => '#',
            'start_time' => Yii::t('newsletter', 'Дата начала'),
            'periodicity' => Yii::t('newsletter', 'Периодичность'),
            'active' => Yii::t('newsletter', 'Активна'),
            'type' => Yii::t('newsletter', 'Тип'),
            'entity_type' => Yii::t('newsletter', 'Тип материалов'),
            'entity_id' => Yii::t('newsletter', 'Материалы'),
            'user_type' => Yii::t('newsletter', 'Кому рассылать'),
            'subject' => Yii::t('newsletter', 'Тема'),
            'template' => Yii::t('newsletter', 'Шаблон'),
            'last_send_time' => Yii::t('newsletter', 'Дата последней отправки'),
            'users_count' => Yii::t('newsletter', 'Писем'),
            'links_count' => Yii::t('newsletter', 'Ссылок'),
            'conversion_day' => Yii::t('newsletter', 'Переходов / день'),
            'conversion_two_days' => Yii::t('newsletter', 'Переходов / два дня'),
            'conversion_week' => Yii::t('newsletter', 'Переходов / неделю'),
            'conversion_total' => Yii::t('newsletter', 'Всего переходов'),
            'create_time' => Yii::t('newsletter', 'Дата создания'),
            'update_time' => Yii::t('newsletter', 'Дата обновления'),
            'title' =>  Yii::t('newsletter', 'Название'),
            'message' =>  Yii::t('newsletter', 'Сообщение'),
        );
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
            ),
            /*'DateTimeI18NBehavior' => array(
                'class' => 'translate.components.DateTimeI18NBehavior',
                'attributes' => array('start_time')
            ),*/
        );
    }

    protected function afterFind()
    {
        parent::afterFind();

        // $this->entity_type = unserialize($this->entity_type);
        //$this->entity_id = unserialize($this->entity_id);

        if (empty($this->entity_type))
            $this->entity_type = array();
        if (empty($this->entity_id))
            $this->entity_id = array();
    }

    protected function beforeValidate()
    {
        if (!parent::beforeValidate()) return false;

        if (empty($this->entity_type))
            $this->entity_type = array();
        if (empty($this->entity_id))
            $this->entity_id = array();

        $this->entity_type = serialize($this->entity_type);
        $this->entity_id = serialize($this->entity_id);
        return true;
    }

    /**
     * General
     */

    public function getConversion()
    {
        if ($this->users_count == 0 || $this->links_count == 0)
            return 0;
        return $this->conversion_total / ($this->users_count * $this->links_count);
    }

    public function increaseVisits()
    {
        $curTime = new DateTime();
        $createTime = new DateTime($this->create_time);

        $interval = $curTime->diff($createTime)->format('%d');

        $this->conversion_total++;

        if ($interval < 7)
            $this->conversion_week++;

        if ($interval < 2)
            $this->conversion_two_days++;

        if ($interval < 1)
            $this->conversion_day++;

        $this->save();
    }

    public static function getPeriodicityItemList()
    {
        return array(
            self::PERIODICITY_SINGLE => Yii::t('newsletter', 'Один раз'),
            self::PERIODICITY_DAY => Yii::t('newsletter', 'Ежедневно'),
            self::PERIODICITY_WEEK => Yii::t('newsletter', 'Еженедельно'),
            self::PERIODICITY_MONTH => Yii::t('newsletter', 'Ежемесячно'),
        );
    }

    public static function getTypeItemList()
    {
        return array(
            '0' => Yii::t('newsletter', 'Новые материалы'),
            '1' => Yii::t('newsletter', 'Выбранные материалы'),
        );
    }

    public static function getEntityTypeItemList()
    {
        return array(
            'Event' => Yii::t('newsletter', 'События'),
            'Review' => Yii::t('newsletter', 'Обзоры'),
            'Place' => Yii::t('newsletter', 'Места'),
            'Article' => Yii::t('newsletter', 'Делимся знанием'),
            'Recipe' => Yii::t('newsletter', 'Сделать вместе'),
        );
    }

    public static function getUserTypeItemList()
    {
        $list = array();
        
        //фильтры рассылки
        $filters = Filters::model()->findAll(array('order'=>'name ASC'));
        $list[self::USER_TYPE_SUBSCRIBED] = Yii::t('newsletter', 'Подписчикам');
        
        foreach ($filters as $key){
            $list['f_'.$key->id] = $key->name;
        }
        
        $list[self::USER_TYPE_REGISTERED] = Yii::t('newsletter', 'Всем зарегистрированным');
        
        return $list;
        
//        return array(
//            self::USER_TYPE_SUBSCRIBED => Yii::t('newsletter', 'Подписчикам'),
//            self::USER_TYPE_REGISTERED => Yii::t('newsletter', 'Всем зарегистрированным'),
//            //'ALL' => Yii::t('newsletter', 'Всем'),
//            //'WEEK' => Yii::t('newsletter', 'Пользователям зарегистрированным в течении недели'),
//            //'MONTH' => Yii::t('newsletter', 'Пользователям зарегистрированным в течении месяца'),
//            //'YEAR' => Yii::t('newsletter', 'Пользователям зарегистрированным в течении года'),
//        );
    }
    
    public static function getUserTypeItemListForExport()
    {
        $list = array();
        
        //фильтры рассылки
        $filters = Filters::model()->findAll(array('order'=>'name ASC'));
        $list[self::USER_TYPE_REGISTERED] = Yii::t('newsletter', 'Всех зарегистрированных');
        
        foreach ($filters as $key){
            $list['f_'.$key->id] = $key->name;
        }
        
        return $list;
    }     

    /**
     * Criteria
     */

    public function getUnsentCriteria()
    {
        $alias = $this->getTableAlias();

        $criteria = new CDbCriteria;
        //$criteria->addCondition("$alias.periodicity = :single AND $alias.last_send_time IS NULL AND $alias.start_time <= :time");
        //$criteria->addCondition("$alias.periodicity = :day AND (($alias.last_send_time IS NULL AND $alias.start_time <= :time) OR (:time >= $alias.last_send_time + INTERVAL 1 DAY))", 'OR');
        //$criteria->addCondition("$alias.periodicity = :week AND (($alias.last_send_time IS NULL AND $alias.start_time <= :time) OR (:time >= $alias.last_send_time + INTERVAL 1 WEEK))", 'OR');
        //$criteria->addCondition("$alias.periodicity = :month AND (($alias.last_send_time IS NULL AND $alias.start_time <= :time) OR (:time >= $alias.last_send_time + INTERVAL 1 MONTH))", 'OR');

        $criteria->params = array(
            //':single' => self::PERIODICITY_SINGLE,
            //':day' => self::PERIODICITY_DAY,
            //':week' => self::PERIODICITY_WEEK,
            //':month' => self::PERIODICITY_MONTH,
            //':time' => Yii::app()->dateFormatter->format('yyyy-MM-dd HH:mm:ss', time())
        );

        return $criteria;
    }

    /**
     * Scopes
     */

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);
        return array(
            'order' => "$alias.create_time desc",
        );
    }

    public function unsent()
    {
        $this->getDbCriteria()->mergeWith($this->getUnsentCriteria());
        return $this;
    }
}