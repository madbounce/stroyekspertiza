<?php
$index = md5(get_class($model) . $model->id);
$class = get_class($model);
if ($class == 'Event' && $model->isReview) $class = 'Review';
?>
<div class="entity entity-<?php echo $model->id; ?>" data-entity-id="<?php echo $model->id; ?>" data-entity="<?php echo $class; ?>">
    <?php echo CHtml::hiddenField("Newsletter[entity_id][{$index}][entity]", $class); ?>
    <?php echo CHtml::hiddenField("Newsletter[entity_id][{$index}][entity_pk]", $model->id); ?>

    <?php echo CHtml::encode($model->title); ?>

    <span class="cancel-place">
        (<?php echo CHtml::link(Yii::t('newsletter', 'отменить'), '#', array('data-entity-id' => $model->id, 'data-entity' => $class, 'class' => 'remove-place',
        'onclick' => 'removeEntity($(this).attr("data-entity-id"), $(this).attr("data-entity")); return false;')); ?>)
    </span>
</div>