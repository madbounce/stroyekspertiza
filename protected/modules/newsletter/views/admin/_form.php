<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'newsletter-form',
    'enableAjaxValidation' => true,
    //'type' => 'horizontal',
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
        'validateOnType' => false,
    ),
    'htmlOptions' => array('class' => 'product')
)); ?>

<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textField($model, 'title', array('class' => 'name', 'maxlength' => 255)); ?>
    <div class="checkbox"></div>
</div>

<div class="block layer" style="width: auto; min-height: auto;">

<?php echo $form->labelEx($model, 'subject', array('class' => 'property', 'style' => 'width:50px'))?>
<?php echo $form->textField($model, 'subject', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
<?php echo $form->error($model, 'subject'); ?>

</div>

<?php echo $form->labelEx($model, 'message', array('class' => 'property-redactor')); ?>
<?php $this->widget('application.extensions.redactorjs.ERedactorWidget', array(
    'model' => $model,
    'attribute' => 'message',
    'options' => array(
        'lang' => 'ru',
        'imageUpload' => CHtml::normalizeUrl(array('/newsletter/admin/redactorUploadImage'))
    ),
)); ?>
<?php echo $form->error($model, 'message'); ?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>