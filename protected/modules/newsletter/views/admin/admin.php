<div class="page-header admin-header">
    <h3><?php echo Yii::t('app', '{n} рассылка|{n} рассылки|{n} рассылок', $dataProvider->totalItemCount);?></h3>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'label' => Yii::t('newsletter', 'Добавить рассылку'),
    'size' => 'small',
    'url' => array('/newsletter/admin/create'),
));
    ?>
</div>
<div class="page-content">
    <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'id' => 'newsletter-grid',
    'type'=>'striped bordered',
    'template' => '{pager} {items} {pager}',
    'dataProvider' => $dataProvider,
    'columns' => array(
        array(
            'name' => 'title',
            'type'=>'raw',
            'value' => 'CHtml::link($data->title, array("/newsletter/admin/update", "id" => $data->id))',
        ),
        array(
            'header' => Yii::t('newsletter', 'Отправлено'),
            'name' => 'last_send_time',
            'type'=>'raw',
            'value' => function($data) {
                return $data->last_send_time ? '<span class="label label-success">' . Yii::app()->dateFormatter->format("dd MMMM yyyy HH:mm", $data->last_send_time) . '</span>' :  '<span class="label label-important">' . Yii::t('newsletter', 'Нет') . '</span>';
            }
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{send}{delete}',
            'buttons'=>array
            (
                'send' => array
                (
                    'class' => 'bootstrap.widgets.TbButton',
                    'label'=>'рассылка',
                    'url'=>'Yii::app()->controller->createUrl("SendNewsletters", array("id"=>$data->primaryKey))',
                    'click'=>new CJavaScriptExpression('function(){$("#mydialog").dialog("open"); $("#sendurl").val($(this).attr("href")); return false;}'),
                    'icon'=>'envelope'
                )

            )
        ),
    ),
)); ?>
</div>

<?php
$ajax=CHtml::ajax(array(
    'type'=>'GET',
    'url'=>'js:$("#sendurl").val()',
    'beforeSend' => new CJavaScriptExpression('function(){$("#message-newsletter").show();}'),
    'complete' => new CJavaScriptExpression('function(){$("#message-newsletter").hide();}'),
    'data'=>'js:{usertype:$("#usertype").val()}',
    'success'=>new CJavaScriptExpression('function(text){
        $("#mydialog").dialog("close");
        if(text == "ok")
            $("#newsletter-grid").yiiGridView.update("newsletter-grid");
        else
            alert(text);
    }'),
));


$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'mydialog',
    'options'=>array(
        'title'=>'Рассылка',
        'width' => 230,
        'autoOpen'=>false,
        'buttons' => array(
            array('text'=>'Отправить','click'=> 'js:function(){' . $ajax . '}'),
            array('text'=>'Отмена','click'=> 'js:function(){$(this).dialog("close");}'),
        ),
        'modal' => true,
        'resizable' => false,
        'draggable' => false,
        'open' => 'js:function(event, ui) {
            $(".ui-widget-overlay").bind("click", function() { $("#mydialog").dialog("close"); });
         }',
        'position' => array('my' => 'center', 'at' => 'center', 'of' => '#newsletter-grid'),
    ),
));
echo CHtml::hiddenField('sendurl','', array('id' => 'sendurl'));
echo CHtml::dropDownList('usertype', 0, Newsletter::getUserTypeItemList(), array('id' => 'usertype'));
?>
<div id="message-newsletter" style="display: none" class="alert in alert-block fade alert-info"><strong>Рассылка!</strong> Подождите...</div>
<?php
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
