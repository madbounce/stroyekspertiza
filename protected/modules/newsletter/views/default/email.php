<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<div class="wrapper" style="margin: 0;padding:0;max-width:714px;margin:0 auto; font-family:Arial, Helvetica, sans-serif">

    <table>
        <tbody>
        <tr>
            <td style="margin: 0;width:263px;padding: 75px 0 5px 5px; text-align:center">
                <?php echo CHtml::image(Yii::app()->request->hostInfo . Yii::app()->baseUrl . '/images/email/ejik_logo.png', '', array('width' => 129, 'height' => 99)); ?>
            </td>
            <td style="font-size:10px;color:#666666;margin: 0;padding: 75px 0 0;line-height: 20px;vertical-align:bottom; padding-bottom:10px;  color:#666666;">
                <?php echo Yii::t('email', ':ezhikezhik - информационный портал, созданный специально для мам и пап, желающих предоставить своим детям максимум возможностей для разностороннего развития в современном мегаполисе.', array(':ezhikezhik' => '<span style="color:#00b0d8;font-size:19px;font-family: \'PTSans-Regular\';">ezhik</span><span style="color:#f15a29;font-size:19px;font-family: \'PTSans-Regular\';">ezhik</span><span style="color:#f15a29;font-size:19px;font-family: \'PTSans-Regular\';">.ru</span>')); ?>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="actions">
        <tbody>
        <?php foreach ($models as $model): ?>
        <tr>
            <td style="border-top:1px dashed #666666;margin: 0;padding:0;padding:34px 5px 27px;vertical-align:top;width:250px">
                <?php
                $files = $model->getFiles();
                if (!empty($files)) {
                    $first = $files[0];

                    echo CHtml::image(Yii::app()->request->hostInfo . Yii::app()->baseUrl . ImageHelper::thumb(231, 164, '/' . $first->file, array(
                            'method' => 'adaptiveResize',
                            'mask' => Yii::getPathOfAlias('webroot') . '/images/email/bg2.png',
                        )), '',
                        array(
                            'width' => 231,
                            'height' => 164
                        ));
                }
                ?>
            </td>
            <td style="border-top:1px dashed #666666;margin: 0;padding:0;padding:34px 5px 27px;vertical-align:top;  color:#333333;">
                <h2 style="margin: 0;padding:0;color:#41b2d8;font-size:17px; font-weight:normal;">
                    <?php echo CHtml::encode($model->title); ?>
                </h2>
                <p style="margin: 0;padding:0;padding-top: 9px;font-size: 12px;line-height: 20px;padding-bottom: 14px; color:#666666;">
                    <?php echo strip_tags($model->short_description); ?>
                </p>
                <?php echo CHtml::link(Yii::t('email', 'Полная версия на сайте'), Yii::app()->createAbsoluteUrl('/newsletter/default/redirect', array('newsletter' => $newsletter->id, 'entity' => get_class($model), 'entity_id' => $model->id)), array(
                    'style' => "float:right;line-height:24px;padding:0 16px;color:#ffffff;font-size:12px;font-family: 'PTSans-Bold';background:url(" . Yii::app()->request->hostInfo . Yii::app()->baseUrl . "/images/email/link-bg.gif) repeat-x;border-radius:10px;text-decoration:none"
                )); ?>
                <div class="clear"></div>
            </td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td align="right" colspan="2" style="border-top:1px dashed #666666;margin: 0;padding:0;font-size:11px; color:#666666; height:31px;vertical-align:top;padding-top: 19px;">
                <?php echo Yii::t('email', 'Присоединяйтесь'); ?>:
                <?php echo CHtml::link(CHtml::image(Yii::app()->request->hostInfo . Yii::app()->baseUrl . '/images/email/icon-1.gif'), Yii::app()->createAbsoluteUrl('/user/auth/socialLogin', array('service' => 'facebook'))); ?>
                <?php echo CHtml::link(CHtml::image(Yii::app()->request->hostInfo . Yii::app()->baseUrl . '/images/email/icon-4.gif'), Yii::app()->createAbsoluteUrl('/user/auth/socialLogin', array('service' => 'vkontakte'))); ?>
                <?php echo CHtml::link(CHtml::image(Yii::app()->request->hostInfo . Yii::app()->baseUrl . '/images/email/icon-3.gif'), Yii::app()->createAbsoluteUrl('/user/auth/socialLogin', array('service' => 'odnoklassniki'))); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-size:10px;color:#666666; margin: 0;padding:0;line-height: 20px;padding:20px 5px 27px;vertical-align:top;">
                <p>
                    <?php echo Yii::t('email', 'Вы получили это письмо, потому что подписаны на рассылку новостей на :link.', array(':link' => CHtml::link('http://www.ezhikezhik.ru', Yii::app()->createAbsoluteUrl('/event/default/index'), array('style' => 'color:#41b2d8;')))); ?><br>
                    <?php echo Yii::t('email', 'Вы всегда можете :link от рассылки.', array(':link' => CHtml::link(Yii::t('email', 'отписаться'), Yii::app()->createAbsoluteUrl('/user/subscription'), array('style' => 'color:#41b2d8;')))); ?><br>
                    <?php echo Yii::t('email', 'Проблемы с отображением письма? Посмотрите исходную версию на :link.', array(':link' => CHtml::link('http://www.ezhikezhik.ru', Yii::app()->createAbsoluteUrl('/newsletter/default/view', array('id' => $newsletter->id)), array('style' => 'color:#41b2d8;')))); ?><br>
                </p>
            </td>
        </tr>
        </tbody></table>
</div>
</body>
</html>