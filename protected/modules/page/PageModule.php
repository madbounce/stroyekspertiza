<?php

class PageModule extends CWebModule
{
    public function init()
    {
        $this->setImport(array(
            'page.models.*',
            'page.components.*',
        ));
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        } else
            return false;
    }

    public static function link($name, $htmlOptions = array())
    {
        $model = Page::model()->findByAttributes(array('name' => $name));
        return CHtml::link(CHtml::encode($model->title), array('/page/default/view', 'name' => $model->name), $htmlOptions);
    }
}
