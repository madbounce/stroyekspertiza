<?php

class PageWidget extends CWidget
{
    public $name;
    public $hTag = 'h1';

    public function run()
    {
        $model = Page::model()->findByAttributes(array('name' => $this->name));
        if (!empty($model))
            $this->render('page', array('model' => $model, 'hTag' => $this->hTag));
    }
}
