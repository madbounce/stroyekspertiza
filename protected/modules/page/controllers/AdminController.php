<?php

class AdminController extends BackEndController
{
    public $layout = '//layouts/main_content';

    public function actions()
    {
        return array(
            'uploadImage' => array(
                'class' => 'application.modules.image.components.UploadAction',
                'entity' => 'Page',
                'tag' => 'image',
                'directory' => 'page',
                'view' => 'application.modules.image.components.widgets.views.imagesPage',
                'multiple' => false,
            ),
            'redactorUploadImage' => array(
                'class' => 'application.extensions.redactorjs.ERedactorUploadAction',
            ),
            'toggle' => 'ext.jtogglecolumn.ToggleAction',
            'sortable' => array(
                'class'     => 'bootstrap.actions.TbSortableAction',
                'modelName' => 'PageCategory'
            )
        );
    }

	

    public function actionCreate()
    {
        $model = new Page;

        $this->performAjaxValidation($model);

        if (isset($_POST['Page'])) {
            $model->attributes = $_POST['Page'];
			$model->image=CUploadedFile::getInstance($model,'image');
            if ($model->validate())
			{
				if(!empty($model->image))
				{
					$imageName = md5($model->id).".".strtolower($model->image->extensionName);
					$imagePath = Yii::app()->basePath."/../images/banners/";
					$model->image->saveAs($imagePath.$imageName);
					//$model->type = $model->image->type;
					$model->image = $imageName;
					//$model->save();
				}
				$model->save();
                $this->redirect(array('admin'));
			}
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, true);

        $this->performAjaxValidation($model);

        if (isset($_POST['Page'])) {
            $model->attributes = $_POST['Page'];

			$uploadedImage=CUploadedFile::getInstance($model,'image');

			if(!empty($uploadedImage))
			{
				$model->image = $uploadedImage;
			}

            if ($model->validate())
			{
				if(!empty($uploadedImage))
				{
					$imageName = md5($model->id).".".strtolower($model->image->extensionName);
					$imagePath = Yii::app()->basePath."/../images/banners/";
					@unlink($imagePath.$currentImage);
					$model->image->saveAs($imagePath.$imageName);
					$model->image = $imageName;
				}
				$model->save();
                $this->redirect(array('admin'));
			}
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $model = new Page('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['Page']))
            $model->attributes = $_GET['Page'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionCategory()
    {
        $model = new PageCategory('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['PageCategory']))
            $model->attributes = $_GET['PageCategory'];

        $this->render('admin_category', array(
            'model' => $model,
        ));
    }

    public function actionCreateCategory()
    {
        $model = new PageCategory;

        $this->performAjaxValidation($model);

        if (isset($_POST['PageCategory'])) {
            $model->attributes = $_POST['PageCategory'];
            if ($model->save()) {
                if (isset($_POST['more']))
                    $this->redirect(array('createcategory'));
                $this->redirect(array('/page/admin/category'));
            }
        }

        $this->render('create_category', array(
            'model' => $model,
        ));
    }

    public function actionEditCategory($id)
    {
        $model = $this->loadModelCategory($id);

        $this->performAjaxValidation($model);

        if (isset($_POST['PageCategory'])) {
            $model->attributes = $_POST['PageCategory'];
            $model->path = $_POST['PageCategory']['path'];
            $model->parent_id = intval($_POST['PageCategory']['parent_id']);

            if ($model->save()) {
                $this->redirect(array('/page/admin/category'));
            }
        }

        $this->render('update_category', array(
            'model' => $model,
        ));
    }

    public function actionEditable()
    {
        if (isset($_POST['pk']) && isset($_POST['name']) && isset($_POST['value'])) {
            $model = $this->loadModelCategory($_POST['pk']);
            $model->title = $_POST['value'];

            if ($model->save()) {
                echo CJSON::encode(array('success' => true));
                Yii::app()->end();
            }

            echo CJSON::encode(array('success' => false, 'msg' => $model->getError($_POST['name'])));
            Yii::app()->end();
        }

        echo CJSON::encode(array('success' => false));
        Yii::app()->end();
    }

    public function actionDeleteCategory($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModelCategory($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function loadModelCategory($id, $ml = false)
    {
        if ($ml) {
            $model = PageCategory::model()->multilang()->findByPk($id);
        } else {
            $model = PageCategory::model()->findByPk($id);
        }
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        return $model;
    }

    public function loadModel($id, $ml = false)
    {
        if ($ml) {
            $model = Page::model()->multilang()->findByPk($id);
        } else {
            $model = Page::model()->findByPk($id);
        }
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'page-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    public function actionUpdateSph(){
        $pages = Page::model()->findAll();
        foreach($pages as $page){
            Search::updateIndex(get_class($page), $page->id);
        }
        echo 'ok';
    }

}
