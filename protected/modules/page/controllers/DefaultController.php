<?php

class DefaultController extends FrontEndController
{
    public $layout = '//layouts/main';
	
	public function actionView($name)
    {
        $model = $this->loadModel($name);

        Yii::app()->clientScript->registerMetaTag($model->meta_description, 'description');
        Yii::app()->clientScript->registerMetaTag($model->meta_keywords, 'keywords');

        $this->render('view', array('model' => $model));
    }

	public function actionIndex($name, $key = '')
    {
        $model = $this->loadModel($name);

        $this->useSeoWidget = false;
        $this->pageTitle = $model->meta_title ? $model->meta_title : $model->name;
        Yii::app()->clientScript->registerMetaTag($model->meta_description, 'description');
		Yii::app()->clientScript->registerMetaTag($model->meta_keywords, 'keywords');

        $view = 'view';

        if($this->getViewFile($name)){
            $view = $name;
        }

        $this->render($view, array('model' => $model));
    }

    /* О компании */
    public function actionAbout()
    {
        $model = $this->loadModel('about');
        $this->render('about', array('model' => $model));
    }

    /* Контакты */
    public function actionContacts()
    {
        $this->layout = '//layouts/about';
        $this->render('contacts');
    }

    /* Связаться с нами*/
    public function actionFeedback()
    {
        $this->layout = '//layouts/about';

        $contactFormModel = new ContactForm();

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'contact-form') {
            echo CActiveForm::validate($contactFormModel);
            Yii::app()->end();
        }

        if (isset($_POST['ContactForm'])) {
            $contactFormModel->attributes = $_POST['ContactForm'];

            if ($contactFormModel->validate()) {
                $toEmail = Config::get('inboxEmail');
                Yii::app()->getModule('mail')->send($toEmail, Config::get('infoEmail'), 'contactForm',
                    array(
                         'siteNameLink' => CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')),
                         'name' => $contactFormModel->name,
                         'email' => $contactFormModel->email,
                         'phone' => $contactFormModel->phone,
                         'department' => $contactFormModel->department,
                         'reason' => $contactFormModel->reason,
                         'text' => $contactFormModel->text
                    )
                );

                $this->setMessage(Misc::t('Обратная связь'), '<div class="recovery_text">' . Misc::t('Спасибо! Ваше сообщение отправлено. Мы постараемся ответить Вам в ближайшее время. ') . '</div>');
                $contactFormModel->unsetAttributes();
            }
        }

        $model = $this->loadModel('feedback');
        $this->render('feedback', array('model' => $model));
    }

     public function loadModel($name)
    {
        $model = Page::model()->findByAttributes(array('name' => $name));
        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        return $model;
    }
}