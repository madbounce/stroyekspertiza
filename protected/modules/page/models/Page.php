<?php

/**
 * This is the model class for table "page".
 *
 * The followings are the available columns in table 'page':
 * @property string $id
 * @property string $name
 * @property string $title
 * @property string $content
 *
 */
class Page extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{page}}';
    }

    protected function afterSave()
    {
        parent::afterSave();

        Search::updateIndex(get_class($this), $this->id);
    }

    public function rules()
    {
        return array(
            array('name, title, content', 'required'),
            array('name', 'length', 'max' => 64),
            array('title', 'length', 'max' => 512),
            array('image', 'length', 'max' => 255),
			array('image', 'file', 'types'=>'jpg, jpeg, png, gif, swf', 'allowEmpty'=>true),
            array('description,categoryId, case, image, meta_title, meta_description, meta_keywords', 'safe'),
            array('id, name,categoryId, title, content, description, case, image, meta_title, meta_description, meta_keywords', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'category' => array(self::BELONGS_TO, 'PageCategory', 'categoryId'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => Yii::t('page', 'Cсылка'),
            'categoryId' => Yii::t('page', 'Категории'),
            'title' => Yii::t('page', 'Заголовок'),
            'content' => Yii::t('page', 'Содержание'),
            'description' => 'description',
            'meta_title'=>'Заголовок',
            'meta_description'=>'Описание',
            'meta_keywords'=>'Ключевые слова',
            'case' => 'case',
			'image' => Yii::t('page', 'Фон'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('content', $this->content, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors()
    {
        return array(
            'ml' => array(
                'class' => 'translate.extensions.MultilingualBehavior',
                'langTableName' => 'page_localization',
                'localizedAttributes' => array('title', 'content'),
                'languages' => TranslateModule::getLanguages(),
                'defaultLanguage' => Yii::app()->sourceLanguage,
            ),
            'image' => array(
                'class' => 'image.components.ImageBehavior',
                'tag' => 'image',
                'multiple' => false,
            ),
        );
    }

    /**
     * Scopes
     */

    public function defaultScope()
    {
        return $this->ml->localizedCriteria();
    }

    public function getPhoto($width = 915, $height = 0, $options = array('method' => 'resize')){
        $image = Image::model()->entity('Page', $this->id)->tag('image')->find();
        if($image){
            $thumbPath = Yii::app()->baseUrl . ImageHelper::thumb($width, $height, $image->url, $options);
            return $thumbPath;
        } else
            return null;
    }

    public function adminSearch()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('content', $this->content, true);

        if(isset($_GET['categoryId'])){
            $cid = (int)$_GET['categoryId'];
            $criteria->addCondition('t.categoryId =' . $cid);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}