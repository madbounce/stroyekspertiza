<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'page-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'product'
    ),
)); ?>

<?php echo $form->errorSummary($model); ?>

    <div id="name">
        <?php echo $form->textField($model, 'title', array('class' => 'name', 'maxlength' => 255)); ?>
        <div class="checkbox"></div>
    </div>
<div class="block layer" style="width:870px;">
    <div id="category">
        <?php echo $form->dropDownListRow($model, 'categoryId', array('0' => 'Новый раздел')+PageCategory::listAll(), array('class' => 'property', 'maxlength' => 100)); ?>
    </div>
</div>
<!-- Левая колонка -->
<div id="column_left">

    <!-- Параметры страницы -->
    <div class="block layer param">
        <h2>Параметры страницы</h2>
        <ul>
            <li>
                <?php echo $form->labelEx($model, 'name', array('class' => 'property')); ?>
                <div class="page_url">/page/</div>
                <?php echo $form->textField($model, 'name', array('class' => 'page_url', 'maxlength' => 64)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'meta_title', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'meta_title', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'meta_keywords', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'meta_keywords', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'meta_description', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'meta_description', array('class' => 'simpla_inp')); ?>
            </li>
        </ul>
    </div><!-- Параметры страницы (The End)-->

</div><!-- Левая колонка свойств товара (The End)-->

<!-- Правая колонка свойств товара -->
<div id="column_right">

    <!-- Изображение категории -->
    <div class="block layer images">
        <h2>Изображение фон</h2>
        <?php
        $this->widget('application.modules.image.components.widgets.UploadWidget', array(
            'model' => $model,
            'tag' => 'image',
            'action' => array('/page/admin/uploadImage'),
            'multiple' => false,
            'view' => 'imagesPage',
            'uploadButtonText' => Yii::t('page', 'Загрузить изображение'),
        ));
        ?>
    </div>
</div>
<!-- Правая колонка свойств товара (The End)-->

<div style="clear: both"></div>

<?php //echo $form->labelEx($model, 'case'); ?>
<?php //echo $form->textField($model, 'case', array('class' => 'span8')); ?>
<?php //echo $form->error($model, 'case'); ?>


<div class="editor-line">
    <?php echo $form->labelEx($model,'content'); ?>
    <?php //echo $form->textArea($model,'content',array('class'=>'medium')); ?>
    <?php $this->widget('application.extensions.redactorjs.ERedactorWidget', array(
        'model' => $model,
        'attribute' => 'content',
        'options' => array(
            'lang' => 'ru',
            'convertDivs' => false,
            'observeImages' => false,
            'imageUpload' => CHtml::normalizeUrl(array('/page/admin/redactorUploadImage'))
        ),
    )); ?>
    <?php echo $form->error($model,'content'); ?>
</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>