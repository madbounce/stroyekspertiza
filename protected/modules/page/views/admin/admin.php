<?php
$dp = $model->adminSearch();
$countAll = $dp->totalItemCount;
?>
<div class="page-content">
    <div class="row two-columns">
        <div class="span9">
            <div class="page-header admin-header">
                <h3><?php echo Yii::t('app', '{n} страница|{n} страницы|{n} страниц', $countAll);?></h3>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'label' => Yii::t('translate', 'Добавить страницу'),
                'size' => 'small',
                'url' => array('/page/admin/create'),
            ));
                ?>
            </div>
            <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
        'id' => 'page-grid',
        'type'=>'striped bordered',
        'dataProvider' => $dp,
        'filter' => $model,
        //'sortableRows' => true,
        //'sortableAjaxSave'=>true,
        //'sortableAttribute'=>'sort',
        //'sortableAction'=> 'config/admin/sortable',
        'template' => '{pager}{items}{pager}',
        'columns' => array(
            array(
                'name' => 'title',
                'type'=>'raw',
                'value' => 'CHtml::link(strip_tags($data->title), array("/page/admin/update", "id" => $data->id))',
                'htmlOptions' => array('encodeLabel'=>false),
            ),
            array(
                'name' => 'categoryId',
                'type' => 'raw',
                'value' => 'CHtml::link(@$data->category->title, array("/page/admin/admin", "categoryId"=>$data->categoryId))',
                'filter'=>false,
//                'filter'=>PageCategory::listAll(),
                'htmlOptions' => array('encodeLabel'=>false),
            ),
            //'name',
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{update}{delete}'
            ),
        ),
    )); ?>
        </div>
        <div class="span3 sidebar">
           <div class="b-side first-block">
                <?php
                $categories = array(
                    array('label'=>Yii::t('page','Все категории'), 'url'=>array('/page/admin/admin'), 'active' => (!isset($_GET['categoryId']))),
                );
                $cats = PageCategory::all();
                if($cats){
                    foreach($cats as $cat){
                        $rowCssClass = ($cat->parent_id) ? "child-r" : "parent-r";

                        array_push($categories, array(
                                'label' => $cat->title,
                                'url' => array('/page/admin/admin', 'categoryId' => $cat->id),
                                'active' => (isset($_GET['categoryId']) && ($_GET['categoryId'] == $cat->id)),
                                'itemOptions' => array('class'=>$rowCssClass),
                            )
                        );
                    }
                }

                $this->widget('bootstrap.widgets.TbMenu', array(
                    'type'=>'list',
                    'items' => $categories
                ));
                ?>
            </div>

        </div>
    </div>
</div>