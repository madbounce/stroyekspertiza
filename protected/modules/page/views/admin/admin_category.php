<?php
$dp = $model->adminSearch();
$countAll = $dp->totalItemCount;
?>

<div class="page-content">

    <div class="page-header admin-header">
        <h3><?php echo Yii::t('app', '{n} категория|{n} категории|{n} категорий', $countAll);?></h3>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label' => Yii::t('page', 'Добавить категорию'),
            'size' => 'small',
            'url' => array('/page/admin/createcategory'),
        ));
        ?>
    </div>


    <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
        'id' => 'page-grid',
        'type' => 'striped bordered',
        'dataProvider' => $dp,
        'enableSorting' => false,
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'sort',
        'sortableAction' => '/page/admin/sortable',
        'rowCssClassExpression' =>  '($data->parent_id) ? "child-r" : "parent-r"',
        'template' => '{pager}{items}{pager}',
        'columns' => array(
            array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'title',
                'type'=>'raw',
                'value' => '$data->title',
                'editable' => array(
                        'url' => Yii::app()->controller->createUrl("/page/admin/editable"),
                        'placement' => 'right',
                        'inputclass' => 'span4',
                        'title' => Yii::t('admin', 'Введите название пункта'),
                        'success' => 'js: function(data) {
                    if(typeof data == "object" && !data.success) return data.msg;
                }'
                ),

            ),
            array(
                'name' => 'path',
            ),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => ' {update} {delete}',
                'buttons' => array(
                    'update' => array(
                        'url'=>'Yii::app()->controller->createUrl("/page/admin/editcategory", array("id"=>$data->primaryKey))',
                    ),
                    'delete' => array(
                        'url'=>'Yii::app()->controller->createUrl("/page/admin/deletecategory", array("id"=>$data->primaryKey))',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div>