<?php

class PicturesModule extends CWebModule
{
	public $nameModule;
	public $showFrontEnd;
	public $showBackEnd;
	
  static public function getImports()
  {
      return array(
           'pictures.models.*',
		       'pictures.components.*',
		       'pictures.components.helpers.*',
      );
  }

  static public function importAll()
  {
      foreach(self::getImports() as $import)
      {
          yii::import($import);
      }
  }


  public function init()
	{
            PicturesModule::importAll();
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
