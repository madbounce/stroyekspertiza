<?php

class ImageWidget extends CWidget
{
    public $model;
    public $url;
    public $type = 'big';

    private $_files;

    public function getFiles()
    {
        return $this->_files;
    }

    public function init()
    {
        $this->_files = $this->model->getFiles();
    }

    public function run()
    {
        if (!empty($this->_files)) {
            $this->render('image' . ucfirst($this->type));
        }
    }
}
