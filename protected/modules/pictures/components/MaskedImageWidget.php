<?php

class MaskedImageWidget extends CWidget
{
    public $model;
    public $url;
    public $width;
    public $height;
    public $mask;

    private $_files;
    protected $_file;

    public function getFiles()
    {
        return $this->_files;
    }

    public function init()
    {
        $this->_files = $this->model->getFiles();
        if (!empty($this->_files))
            $this->_file = $this->_files[0];
    }

    public function run()
    {
        if (!empty($this->_file)) {
            $this->render('maskedImage');
        }
    }
}

