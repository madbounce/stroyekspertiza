<?php

class PicturesEditor extends CWidget
{
	static $num = 1;

	public $model;
	public $form;

	public $imgClass;

	public $mode;
	public $files;
	public $showonlyfirst = true;

	public $width = 100;
	public $height = 100;

	public $url;
	public $nophoto = true;

	public $wantLink = false;
	public $label ='Изображения';
	public $thumbMethod ='resize'; //adaptiveResize resizePercent
	public $percent = null; // 50% 
	public $selected = 1;
	public $oneImage = false; // если надо загружать один рисунок как для логотипа и заменять его

    public $emptyImages = array(
        'User' => 'images/emptyUser.png',
        //'Event' => 'images/emptyEvent.png',
        //'Place' => 'images/emptyPlace.png',
    );

	public function init()
	{
		yii::import('pictures.models.File');
		yii::import('pictures.components.helpers.ImageHelper');
		$cs = Yii::app()->clientScript;
		$cs->registerCoreScript('jquery');	
		self::publishAssets();
		parent::init();
	}

	public function run()
	{
		if ($this->form)
			$this->form->htmlOptions ['enctype'] = 'multipart/form-data';


		if ($this->model)
		{
			if (!$this->mode)
				$this->mode = $this->model->isNewRecord ? 'Add' : 'Edit';
				
			if($this->mode == 'albumView' || $this->mode == 'AddToAlbumAjax' || $this->mode=='albumGallery')
			 $this->files = $this->model->medias;
			elseif($this->mode == 'AlbumThumb')
			{
			 $this->files = array('image_path'=>$this->model->media_data);
			}
			else	
			 $this->files = $this->model->isNewRecord ? false : $this->model->getFiles();

		}

		if (!$this->mode)
			$this->mode = 'Add';


		$model = new File;

		$params = array(
		  'modelItem'=>$this->model,
			'model' => $model,
			'relid' => self::$num++,
			'showonlyfirst' => $this->showonlyfirst,
			'width' => $this->width,
			'height' => $this->height,
			'url' => $this->url,
			'nophoto' => $this->nophoto,
			'imgClass' => $this->imgClass,
			'wantLink' => $this->wantLink,
			'label' => $this->label,
			'thumbMethod' => $this->thumbMethod,
			'percent' => $this->percent,
			'selected' => $this->selected,
			'oneImage' => $this->oneImage			
		);

		if (is_array($this->files))
			$params['files'] = $this->files;
        else
            $params['files'] = array();

		$this->render($this->mode, $params);
	}
	
	public function publishAssets()
	{
		$assets = dirname(__FILE__).'/assets';
		$baseUrl = Yii::app()->assetManager->publish($assets);
		
		if(is_dir($assets)) {
			//if($this->mode=='noticeView'||$this->mode=='estateView' || $this->mode == 'albumView' || $this->mode=='albumGallery') {
				Yii::app()->clientScript->registerCssFile($baseUrl . '/slider.css');
				Yii::app()->clientScript->registerScriptFile($baseUrl . '/my_slider.js', CClientScript::POS_END);
				Yii::app()->clientScript->registerCssFile($baseUrl . '/jquery.lightbox-0.5.css');
				Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.lightbox-0.5.js', CClientScript::POS_END);
			//}
			if($this->mode=='AddAjax') {
				Yii::app()->clientScript->registerScriptFile($baseUrl . '/jquery.ajaxfileupload.js', CClientScript::POS_END);
			}
		}
	}
	
	
}

?>