<?php
yii::import('pictures.models.File');

class SaveFileBehavior extends CActiveRecordBehavior
{
    public $field_name;
    public $savelocation;
    public $connection;
    public $postname;
    public $entity;
    public $oneImage = false;

    protected function normilizeFile($filePost)
    {
        if (is_array($filePost))
        {
            $i = 0;
            $result = array();
            foreach ($filePost as $id => $file)
            {
                if (substr($id, 0, 2) == 'id')
                {
                    $result[$id] = $file;
                    continue;
                }

                if (isset($file['file']))
                    $result[$i] = array('file' => $file['file']);
                else
                {
                    $result[$i++]['desc'] = $file['desc'];
                }
            }
            return $result;
        }
        throw new Exception('Array expected, but something else given');
    }

    /**
     *  Event handler to save all attached files
     */
    public function afterSave($event)
    {
        //print 'SAVE-FILE-BEHAVIOR:<br>';
       
        if (isset($_POST['File']))
        {
          	// if load ajax files          	
          	if ($_POST['File'][0]!='') {
								foreach ($_POST['File'] as $key => $file) {
									$f = File::model()->findbyPK($file['file']);
									
									if ($key==0) $f->default = 1;
									else  $f->default = 0;		
									
									$newPath = str_replace('temp',$this->savelocation,$f->file);
									
									
									
									if (!file_exists(yii::getPathOfAlias('webroot') . '/' . $newPath)) {
										
									
										copy(yii::getPathOfAlias('webroot') . '/' . $f->file,yii::getPathOfAlias('webroot') . '/' . $newPath);
										@unlink(yii::getPathOfAlias('webroot').'/' . $f->file);
									}
									// Если не надо загружать много файлов а только один, и если он	есть у модели то его заменять							
									if ($this->oneImage)	{
										$fOne = File::model()->find('entity="'.$this->entity.'" and entity_pk='.$this->owner->id);
										
										if ($fOne!=null) {										
											
											$fOne->file = $newPath;						
											if ($fOne->save()) { 
												if ($f->entity=='temp')  {														
													@unlink(yii::getPathOfAlias('webroot').'/' . $f->file);
													$f->delete(); 
												}
											} else echo 'not save';
											
										} else {
											$f->savelocation = $this->savelocation;
											$f->entity = $this->entity;
											$f->entity_pk = $this->owner->id;
											
											$f->num = $key;
											$f->file = $newPath;						
											$f->save();
										}
									}	else {

										
										/*
										print_r($this->savelocation."<br/>");
										print_r($this->entity."<br/>");
										print_r($this->owner->id."<br/>");
										print_r($key."<br/>");
										print_r($newPath."<br/>");
										*/
										
										
										$f->savelocation = $this->savelocation;
										$f->entity = $this->entity;
										$f->entity_pk = $this->owner->id;
										
										$f->num = $key;
										$f->file = $newPath;

										
										if ($f->save()) {
											//print_r("true");
										} else {
											//print_r("false");
											//print_r($f->getErrors());
											
											
										}
										
									}
								}
								return true;
          	}
            $_POST['File'] = $this->normilizeFile($_POST['File']);
            
        }
        return true;
    }

    private $_files;
    public function getFiles()
    {
        if (empty($this->_files))
            $this->_files = File::model()->findAll('entity=:entity AND entity_pk=:pid', array(':entity' => $this->entity, ':pid' => $this->owner->id));

        return $this->_files;
    }

    public function getHasFiles()
    {
        $files = $this->getFiles();
        return !empty($files);
    }

    public function beforeDelete($event)
    {
        $fname = $this->field_name;
        $files = $this->getFiles();

        foreach ($files as $file)
        {
            $file->delete();
        }

        return true;
    }
}
?>