<?php

class SimpleImageWidget extends CWidget
{
    public $file;
    public $width;
    public $height;

    public $url;
    public $rel = 'image';
    public $class = 'image';
    public $maskClass;

    public function run()
    {
        Yii::import('pictures.components.helpers.ImageHelper');

        $this->render('simpleImage');
    }
}
