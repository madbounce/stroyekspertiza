$(function() {
	$("#sliderContent").each(function(){
		//vars
		var conveyor = $(".content-conveyor", $(this)),
		item = $(".item", $(this));
		
		//set length of conveyor
		conveyor.css("width", item.length * parseInt(item.css("width")));		
		var wrapp2 = $(".wrap_sl", $(this));
		var lenght_slid = item.length * parseInt(item.css("width"));
		var vier = parseInt($(".viewer", $(this)).css("width"));
		if(lenght_slid > vier)
		{
			wrapp2.css('display','block');
		}
		else
		{
			wrapp2.css('display','none');
		}
		//config
		var sliderOpts = {
		  max: (item.length * parseInt(item.css("width"))) - parseInt($(".viewer", $(this)).css("width")),
		  slide: function(e, ui) { 
			conveyor.css("left", "-" + ui.value + "px");
		  },
		  change: function(e, ui) {
conveyor.css("left", "-" + ui.value + "px");
}
		};
		//create slider
		 $("#slider2").slider(sliderOpts);
		
		if(typeof setScrool == 'function')
		 setScrool();
									 
									 
	});
});
								  
								  
								  function change_img(path_img, id_a)
									{
										jQuery("#sliderContent .item a").each(function(){
											$(this).attr('class', '');
										});
										jQuery("#sliderContent .item a#"+id_a).attr('class', 'active');
										
										jQuery("#big_image_slide").fadeOut("fast", function(){
										jQuery("#big_image_slide").attr("src", path_img);
										jQuery("#big_image_slide").fadeIn("fast");
										});
										
										//changeComments
										if($.isFunction(changeComments))
										  changeComments(id_a);
									};
									function change_img_prev(path_img, id_a, prev)
                  {
                    jQuery("#sliderContent .item a").each(function(){
                      $(this).attr('class', '');
                    });
                    jQuery("#sliderContent .item a#"+id_a).attr('class', 'active');
                    jQuery("#preview").attr("href",prev);
                    jQuery("#big_image_slide").fadeOut("fast", function(){
                    jQuery("#big_image_slide").attr("src", path_img).load(function() {
                    jQuery("#big_image_slide").fadeIn("fast");
                                                                                });
                    });
                  };