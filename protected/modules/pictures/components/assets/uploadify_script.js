function setuploadify (el,idi)
{
      $(el).uploadify({
        'uploader'  : '<?=Yii::app()->baseUrl . '/themes/' . Yii::app()->theme->name?>/uploadify/uploadify.swf',
        'script'    : '<?=Yii::app()->baseUrl?>/uploadify.php',
        'cancelImg' : '<?=Yii::app()->baseUrl . '/themes/' . Yii::app()->theme->name?>/uploadify/cancel.png',
        'folder'    : '<?=Yii::app()->baseUrl?>/images/products/<?=Yii::app()->user->id?>',
        'multi'       : true,
        'simUploadLimit':22,
        'fileExt'     : '*.jpg;*.png',
        'fileDesc'    : '*.jpg;*.png',
        'auto'      : true, 
        'sizeLimit'   : 52428800,
        'scriptData'  : {'uid':'<?=Yii::app()->user->id?>'},
        'buttonText'  : ' ',
        'buttonImg'   : '<?=Yii::app()->baseUrl . '/themes/' . Yii::app()->theme->name?>/uploadify/tr.png',
        'hideButton'  : true,
        'wmode'       : 'transparent',
        'onComplete'  : function(event, ID, fileObj, response, data2) {
                data=jQuery.parseJSON(response);
                if(typeof(data.error) != 'undefined') {
                        if(data.error !== '') {
                                $('#ajax-error'+idi).text(data.error);
                        } else {
                                if(typeof(data.data) != 'undefined') {
                                        $.each(data.data, function(idx, item) {
                                                 $.get('<?=Yii::app()->baseUrl?>/themes/photobook/frontend/views/user/photo/gallery_item.php?id=' + item.id + '&thumb=' + item.thumb + '&image=' + item.file, function(html) {
                                                         $('#gallery-items'+idi).append(html);
                                                         $(".gallery-item-cost[name='Items["+item.id+"]']", $('#gallery-items'+idi)).attr('tagg', item.id)
                                                         $('.gallery-item[rel="' + item.id + '"]').trigger('imageAdded');
                                                         
                                                 });
                                        });
                                }
                        }
                }

            }
      });
    
}

$(document).ready(function() {
	itblhtml=$('#mainbl').clone(true);
	setuploadify('#file_upload','');
});