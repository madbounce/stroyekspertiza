<?php
/**
 * Image helper functions
 *
 * @author Chris
 * @link http://con.cept.me
 */
class ImageHelper {

    /**
     * Directory to store thumbnails
     * @var string
     */
    const THUMB_DIR = '.tmb';

    /**
     * Create a thumbnail of an image and returns relative path in webroot
     * the options array is an associative array which can take the values
     * quality (jpg quality) and method (the method for resizing)
     *
     * @param int $width
     * @param int $height
     * @param string $img
     * @param array $options
     * @return string $path
     */
    public static function thumb($width, $height, $img, $options = null)
    {

        if(!file_exists($img)){
            $img = str_replace('\\', '/', YiiBase::getPathOfAlias('webroot').$img);
            
            //exit(); 
            if(!file_exists($img)){
               // throw new Exception('Image not found');
               return 0;
            }
        }

        // Jpeg quality
        $quality = 80;
        // Method for resizing
        $method = 'adaptiveResize';
        $mask = null;

        if($options){
            extract($options, EXTR_IF_EXISTS);
        }

        $pathinfo = pathinfo($img);
        $thumb_name = "thumb_".$pathinfo['filename'].'_'.$method.'_'.$width.'_'.$height . (!empty($mask) ? md5($mask) : '') . '.'.$pathinfo['extension'];
        $thumb_path = $pathinfo['dirname'].'/'.self::THUMB_DIR.'/';
       
        if(!file_exists($thumb_path)){
            mkdir($thumb_path);
        }

        if(!file_exists($thumb_path.$thumb_name) || filemtime($thumb_path.$thumb_name) < filemtime($img)){

            Yii::import('ext.phpThumb.PhpThumbFactory');
            $options = array('jpegQuality' => $quality);
            $thumb = PhpThumbFactory::create($img, $options);
            
            
            
            $thumb->{$method}($width, $height);
            //$thumb->createWatermark('images/ideal_estate.png', 'lb', 10);

            $thumb->save($thumb_path.$thumb_name);

            if (!empty($mask))
            {
                $source = imagecreatefromstring(file_get_contents($thumb_path.$thumb_name));
                $maskImage = imagecreatefromstring(file_get_contents($mask));
                self::imagealphamask($source, $maskImage);
                imagepng($source, $thumb_path.$thumb_name);
            }

           /* Yii::app()->ih
                ->load($thumb_path.$thumb_name)
                ->watermark(yii::getPathOfAlias('webroot') . '/images/watermark/200.png', 10, 20, CImageHandler::CORNER_CENTER)
                ->save($thumb_path.$thumb_name);  */
        }

        if (YiiBase::getPathOfAlias('webroot') != '.')
            $relative_path = str_replace(YiiBase::getPathOfAlias('webroot'), '', $thumb_path.$thumb_name);
        else
            $relative_path = $thumb_path.$thumb_name;

        if ($relative_path[0] == '.')
            $relative_path = substr($relative_path, 1);

        return $relative_path;
    }

    static function imagealphamask( &$picture, $mask ) {
        // Get sizes and set up new picture
        $xSize = imagesx( $picture );
        $ySize = imagesy( $picture );
        $newPicture = imagecreatetruecolor( $xSize, $ySize );
        imagesavealpha( $newPicture, true );
        imagefill( $newPicture, 0, 0, imagecolorallocatealpha( $newPicture, 0, 0, 0, 127 ) );

        // Resize mask if necessary
        if( $xSize != imagesx( $mask ) || $ySize != imagesy( $mask ) ) {
            $tempPic = imagecreatetruecolor( $xSize, $ySize );
            imagecopyresampled( $tempPic, $mask, 0, 0, 0, 0, $xSize, $ySize, imagesx( $mask ), imagesy( $mask ) );
            imagedestroy( $mask );
            $mask = $tempPic;
        }

        // Perform pixel-based alpha map application
        for( $x = 0; $x < $xSize; $x++ ) {
            for( $y = 0; $y < $ySize; $y++ ) {
                $alpha = imagecolorsforindex( $mask, imagecolorat( $mask, $x, $y ) );
                $alpha = 127 - floor( $alpha[ 'red' ] / 2 );
                $color = imagecolorsforindex( $picture, imagecolorat( $picture, $x, $y ) );
                imagesetpixel( $newPicture, $x, $y, imagecolorallocatealpha( $newPicture, $color[ 'red' ], $color[ 'green' ], $color[ 'blue' ], $alpha ) );
            }
        }

        // Copy back to original picture
        imagedestroy( $picture );
        $picture = $newPicture;
    }
}