	<script type="text/javascript" src="/themes/iams/front/js/jquery.ajaxfileupload.js"></script>
	<script type="text/javascript">
    $(document).ready(function() {
        $('#fileLoad').live('change',function()
        {
            
            $('#fileLoad').each(function() {
                var name_f = this.value;
                reWin = /.*\\(.*)/;
                var fileTitle = name_f.replace(reWin, "$1");
                reUnix = /.*\/(.*)/;
                fileTitle = fileTitle.replace(reUnix, "$1");
                $('#name_file').attr("value", fileTitle);
								$('.file').attr("value", fileTitle);
                
           });

        });
    });
    

		function deleteImg(id) {
			$.ajax({
					type: "POST",
					url: '<?= Yii::app()->createUrl('pictures/default/delete'); ?>',
					data: 'id='+id+'&YII_CSRF_TOKEN=<?= Yii::app()->request->csrfToken; ?>',
					success: function(ret) {
						var errors='';
						ret = eval('(' + ret + ')');
						if (ret['success']=='true') {
							$('#'+id).remove();					 	
						 	return false;
						}					 
						jQuery.each(ret,function(i, item){
                    errors+='\n'+ item;
                    
            });
            alert(errors); 
						return false;
					}
			});
		}

    
    function ajaxFileUpload() {
			if ($('#fileLoad').val() === '')  {
                                alert('Выберите файл для загрузки');
                                return null;
                            }

			$('#loading')
				.ajaxStart(function(){
					$('#ajax-error').text('');
					$(this).show();
				})
				.ajaxComplete(function(){
					$(this).hide();
				})
			;

			$.ajaxFileUpload({
					url: '<?= Yii::app()->createUrl("pictures/default/ImageAddAjax?method=".$thumbMethod.'&width='.$width.'&height='.$height); ?>',
					secureuri: false,
					fileElementId: 'fileLoad',
					dataType: 'json',

					success: function (data, status) {
						if(typeof(data.error) != 'undefined') {
							if(data.error !== '') {
								//$('#ajax-error').text(data.error);
								alert(data.error);
							} else {
								$("#fileLoad").val("");
								$.get('<?= Yii::app()->createUrl("pictures/default/addImageHtml"); ?>?id=' + data.id + '&thumb=' + data.thumb + '&image=' + data.file, function(html) {
									<? if ($oneImage) : ?>
									$('.illustrationsblock').html(html);
									<? else:?>
									$('.illustrationsblock').append(html);
									<? endif; ?>
									$('#name_file').val('');
									$('.file').val('');
									//$('.gallery-item[rel="' + data.id + '"]').trigger('imageAdded');
								});
							}
						}
					},
					error: function (data, status, e) {
						alert(e);
					}
				}
			);

			return false;
		}
	</script>
	
	<div class="addcompbl">

    <?php if ($label): ?>
        <label class="label1"><?= $label; ?></label>
    <?php endif; ?>

  	<div class="right">
		  <div class="uploadinputsblock1">
			   <input name="" type="text" id="name_file" class="input4 mrg1"/>		
			   <div class="fload_botton">
		     <div class="btn">Обзор</div>	     
			     <input  type="file" size="1" name="fileLoad"  id="fileLoad">	      
			   </div>	   
			   <button type="button" class="blue medium button2" onclick="ajaxFileUpload()" style="opacity: 0.7; "><span>Загрузить изображение</span></button>
			   <div id="loading" style="display:none; float:left;"><img src="/themes/iams/front/images/loading.gif" width="24"></div>
		  </div>
		  <div class="illustrationsblock">
	   		 <? if (isset($files)) : ?>
	   	 			<? foreach ($files as $file) : ?>	
	   	 			<?	$img = 'http://'.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.
		  ImageHelper::thumb($width,$height,'/' . $file->file,array('method' => $thumbMethod)); ?>
	   	 				<div class="illblock" id="img_<?= $file->id; ?>"><img src="<?=  $img; ?>" alt="" width="73" />   	 									
							  <? echo CHtml::activeHiddenField($model,"[]file", array('value'=>$file->id)); ?>
								<a href="javascript:void(0)" onclick="deleteImg('img_<?= $file->id; ?>');$('.mrg1').val('');" class="deletelink1">Удалить</a>
							</div>
							
	   	 			<? endforeach; ?>
	   		 <? endif;?>
		  </div> 
	  </div>
  </div>