<link href="<?php echo Yii::app()->controller->imagePath; ?>css/jquery.lightbox-0.5.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo Yii::app()->controller->imagePath; ?>js/jquery.lightbox-0.5.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->controller->imagePath; ?>js/jquery.ajaxfileupload.js"></script>
	<script type="text/javascript">
    $(document).ready(function() {
        $('#fileLoad').change(function()
        {
            
            $('#fileLoad').each(function() {
                var name_f = this.value;
                reWin = /.*\\(.*)/;
                var fileTitle = name_f.replace(reWin, "$1");
                reUnix = /.*\/(.*)/;
                fileTitle = fileTitle.replace(reUnix, "$1");
                $('#name_file').attr("value", fileTitle);
                
           });

        });
        
        $('#preview').lightBox();
    });
    

		function deleteImg(id) {
			$.ajax({
					type: "POST",
					url: '<?= Yii::app()->createUrl('pictures/default/delete'); ?>',
					data: 'id='+id+'&YII_CSRF_TOKEN=<?= Yii::app()->request->csrfToken; ?>',
					success: function(ret) {
						var errors='';
						ret = eval('(' + ret + ')');
						if (ret['success']=='true') {
							$('#'+id).remove();					 	
						 	return false;
						}					 
						jQuery.each(ret,function(i, item){
                    errors+='\n'+ item;
                    
            });
            alert(errors); 
						return false;
					}
			});
		}

    
    function ajaxFileUpload() {
			if ($('#fileLoad').val() === '')  {
                                alert('Выберите файл для загрузки');
                                return null;
                            }

			$('#loading')
				.ajaxStart(function(){
					$('#ajax-error').text('');
					$(this).show();
				})
				.ajaxComplete(function(){
					$(this).hide();
				})
			;

			$.ajaxFileUpload({
					url: '<?= Yii::app()->createUrl("pictures/default/ImageAddAjax?method=".$thumbMethod.'&width='.$width.'&height='.$height.'&preview=1'); ?>',
					secureuri: false,
					fileElementId: 'fileLoad',
					dataType: 'json',

					success: function (data, status) {
						if(typeof(data.error) != 'undefined') {
							if(data.error !== '') {
								//$('#ajax-error').text(data.error);
								alert(data.error);
							} else {
								$("#fileLoad").val("");
								$.get('<?= Yii::app()->createUrl("pictures/default/addImageHtmlPreview"); ?>?id=' + data.id + '&thumb=' + data.thumb + '&image=' + data.file+ '&preview=' + data.preview, function(html) {
									$('.illustrationsblock').append(html);
									$('#name_file').val('');
									//$('.gallery-item[rel="' + data.id + '"]').trigger('imageAdded');
								});
							}
						}
					},
					error: function (data, status, e) {
						alert(e);
					}
				}
			);

			return false;
		}
	</script>
	
	<div class="addcompbl">
  	<label class="label1"><?= $label; ?></label>
  	<div class="right">
		  <div class="uploadinputsblock1">
			   <input name="" type="text" id="name_file" class="input4 mrg1"/>		
			   <div class="fload_botton">
		     <div class="btn">Обзор</div>	     
			     <input  type="file" size="1" name="fileLoad"  id="fileLoad">	      
			   </div>	   
			   <button type="button" class="blue medium button2" style="float:left;" onclick="ajaxFileUpload()" style="opacity: 0.7; "><span>Загрузить изображение</span></button>
			   <div id="loading" style="display:none; float:left;"><img src="<?= Yii::app()->controller->imagePath; ?>images/loading.gif" width="24"></div>
		  </div>
		  <div class="illustrationsblock">
	   		 <? if ($files) : ?>
	   	 			<? foreach ($files as $file) : ?>	
	   	 			<?	$img = 'http://'.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.
		  ImageHelper::thumb($width,$height,'/' . $file->file,array('method' => 'resize')); ?>
		  			<?	$imgPreview = 'http://'.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.
		  ImageHelper::thumb('800','600','/' . $file->file,array('method' => 'resize')); ?>
	   	 				<div class="illblock" id="img_<?= $file->id; ?>">
	   	 					<a href="<?=$imgPreview?>" id="preview">
	   	 						<img src="<?=  $img; ?>" alt="" width="73" /> 
	   	 					</a>  	 									
							  <? echo CHtml::activeHiddenField($model,"[]file", array('value'=>$file->id)); ?>
								<a href="javascript:void(0)" onclick="deleteImg('img_<?= $file->id; ?>');$('.mrg1').val('');" class="deletelink1">Удалить</a>
							</div>
							
	   	 			<? endforeach; ?>
	   		 <? endif;?>
		  </div> 
	  </div>
  </div>
