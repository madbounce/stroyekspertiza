
	<script type="text/javascript" src="<?php echo Yii::app()->controller->imagePath; ?>js/jquery.ajaxfileupload.js"></script>
	<script type="text/javascript">
    $(document).ready(function() {
        $('#fileLoad').change(function()
        {
            
            $('#fileLoad').each(function() {
                var name_f = this.value;
                reWin = /.*\\(.*)/;
                var fileTitle = name_f.replace(reWin, "$1");
                reUnix = /.*\/(.*)/;
                fileTitle = fileTitle.replace(reUnix, "$1");
                $('#name_file').attr("value", fileTitle);
                
           });

        });
    });
    

		function deleteImg(id) {
			$.ajax({
					type: "POST",
					url: '<?= Yii::app()->createUrl('pictures/default/delete'); ?>',
					data: 'id='+id+'&YII_CSRF_TOKEN=<?= Yii::app()->request->csrfToken; ?>',
					success: function(ret) {
						var errors='';
						ret = eval('(' + ret + ')');
						if (ret['success']=='true') {
							$('#'+id).remove();					 	
						 	return false;
						}					 
						jQuery.each(ret,function(i, item){
                    errors+='\n'+ item;
                    
            });
            alert(errors); 
						return false;
					}
			});
		}

    
    function ajaxFileUpload() {
			if ($('#fileLoad').val() === '') 
                            {
                                alert('Выберите файл для загрузки');
                                return null;
                            }

			$('#loading')
				.ajaxStart(function(){
					$('#ajax-error').text('');
					$(this).show();
				})
				.ajaxComplete(function(){
					$(this).hide();
				})
			;

			$.ajaxFileUpload({
					url: '<?= Yii::app()->createUrl("pictures/default/ImageAddAjax?method=".$thumbMethod.'&width='.$width.'&height='.$height); ?>',
					secureuri: false,
					fileElementId: 'fileLoad',
					dataType: 'json',

					success: function (data, status) {
						if(typeof(data.error) != 'undefined') {
							if(data.error !== '') {
								//$('#ajax-error').text(data.error);
								alert(data.error);
							} else {
								$("#fileLoad").val("");
								$.get('<?= Yii::app()->createUrl("pictures/default/addImageHtml"); ?>?id=' + data.id + '&thumb=' + data.thumb + '&image=' + data.file, function(html) {
									
									$('.complogoblock').html(html);
									$('#name_file').val('');
                                                                        $('.mrg1').val('');
									//$('.gallery-item[rel="' + data.id + '"]').trigger('imageAdded');
								});
							}
						}
					},
					error: function (data, status, e) {
						alert(e);
					}
				}
			);

			return false;
		}
	</script>
	
	<label class="label1">Мое изображение:</label>
	<div class="uploadllogoblock">
	<div class="complogoblock" style="width:74px; height:auto; padding: 0 2px;">
		<? if ($files) : ?>
	   		<? foreach ($files as $file) : ?>	
	   		<?	$img = 'http://'.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.
	ImageHelper::thumb($width,$height,'/' . $file->file,array('method' => $thumbMethod)); ?>
	   	 		<div class="illblock" id="img_<?= $file->id; ?>"><img src="<?=  $img; ?>" alt="" width="73" />   	 									
						<? echo CHtml::activeHiddenField($model,"[]file", array('value'=>$file->id)); ?>
						<a href="javascript:void(0)" onclick="deleteImg('img_<?= $file->id; ?>'); $('.mrg1').val('');" class="deletelink1">Удалить</a>
					</div>
					
	   		<? endforeach; ?>
		 <? endif;?>
	</div>
	<div class="uploadinputsblock1">
	<input name="" type="text" class="input6 mrg1"  readonly="readonly" />
	
	<div class="fload_botton flleft">
		     <div class="btn">Обзор</div>	     
                     <input  onchange="$('.mrg1').val(this.value);"  type="file" size="1" name="fileLoad"  id="fileLoad"/>	      
	</div>	   
	<input name="" type="button" class="button4 btnm3 flleft" onclick="ajaxFileUpload()" value="Загрузить"/>
<div id="loading" style="display:none; float:left;"><img src="<?= Yii::app()->controller->imagePath; ?>images/loading.gif" width="24"></div>
	
<!--        </div>-->