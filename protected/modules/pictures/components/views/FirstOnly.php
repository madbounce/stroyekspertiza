<?php

    if (empty($files)) {
        if (isset($this->emptyImages[get_class($modelItem)])) {
            $image = CHtml::image(Yii::app()->baseUrl . ImageHelper::thumb($width, $height, '/themes/' . Yii::app()->theme->name . '/' . $this->emptyImages[get_class($modelItem)], array('method' => $thumbMethod)), '',
                array(
                    'width' => $width,
                    'height' => $height
                ));
        } else {
            $image = CHtml::image(Yii::app()->request->baseUrl . '/themes/' . Yii::app()->theme->name . '/images/foto_zagl.png', 'No photo', array(
                'width' => $width,
                'height' => $height,
                'border' => 0,
                'class' => $imgClass
            ));
        }
    } else {
        $image = CHtml::image(Yii::app()->baseUrl . ImageHelper::thumb($width, $height, '/' . $files[0]->file, array('method' => $thumbMethod)), '',
            array(
            'width' => $width,
            'height' => $height
            ));
    }

    if ($url)
        echo CHtml::link($image, $url, array('class' => $imgClass, 'title' => !empty($files) ? $files[0]->desc : ''));
    else
        echo $image;

?>