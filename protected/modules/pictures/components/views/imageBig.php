<?php
    $files = $this->files;
    $first = $files[0];
    $other = array_slice($files, 1);
?>

<div class="image-big-wrapper">

    <div class="image-big">

        <?php $this->widget('pictures.components.SimpleImageWidget', array(
            'file' => $first,
            'width' => 420,
            'height' => 205,
            'url' => Yii::app()->baseUrl . '/' . $first->file,
            'rel' => 'image-big',
            'maskClass' => 'event-place-photo-bg'
        )); ?>

    </div>

    <div class="other-images">
        <?php foreach ($other as $file): ?>
            <div class="image">

                <?php $this->widget('pictures.components.SimpleImageWidget', array(
                    'file' => $file,
                    'width' => 80,
                    'height' => 60,
                    'url' => Yii::app()->baseUrl . '/' . $file->file,
                    'rel' => 'image-big',
                    'maskClass' => 'image-small-background'
                )); ?>

            </div>
        <?php endforeach; ?>
    </div>

</div>

<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
        'target' => 'a[rel=image-big]',
        'config' => array(
            'titlePosition' => 'over',
        ),
    )
);
?>

