<?php
    $files = $this->files;
    $first = $files[0];
?>

<div class="image-medium-wrapper">

    <div class="image-medium">

        <?php $this->widget('pictures.components.SimpleImageWidget', array(
            'file' => $first,
            'width' => 194,
            'height' => 119,
            'url' => $this->url,
            'maskClass' => 'image-medium-background'
        )); ?>

    </div>

</div>


