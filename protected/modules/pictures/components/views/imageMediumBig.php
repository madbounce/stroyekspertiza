<?php
$files = $this->files;
$first = $files[0];
?>

<div class="image-medium-big-wrapper">

    <div class="image-medium-big">

        <?php $this->widget('pictures.components.SimpleImageWidget', array(
            'file' => $first,
            'width' => 290,
            'height' => 207,
            'url' => $this->url,
            'maskClass' => 'image-medium-big-background'
        )); ?>

    </div>

</div>