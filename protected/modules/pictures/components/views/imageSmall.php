<?php
$files = $this->files;
$first = $files[0];
?>

<div class="image-small-wrapper">

    <div class="image-small">

        <?php $this->widget('pictures.components.SimpleImageWidget', array(
            'file' => $first,
            'width' => 80,
            'height' => 60,
            'url' => $this->url,
            'maskClass' => 'top_bg_img1'
        )); ?>

    </div>

</div>