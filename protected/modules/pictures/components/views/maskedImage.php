<?php

$image = CHtml::image(Yii::app()->baseUrl . ImageHelper::thumb($this->width, $this->height, '/' . $this->_file->file, array('method' => 'adaptiveResize', 'mask' => $this->mask)), '',
    array(
        'width' => $this->width,
        'height' => $this->height
    ));

if (!empty($this->url))
    echo CHtml::link($image, $this->url);
else
    echo $image;

?>