<?php

$image = CHtml::image(Yii::app()->baseUrl . ImageHelper::thumb($this->width, $this->height, '/' . $this->file->file, array('method' => 'adaptiveResize')), '',
    array(
        'width' => $this->width,
        'height' => $this->height
    ));

if (!empty($this->url))
    echo CHtml::link(!empty($this->maskClass) ? "<div class=\"{$this->maskClass}\"></div>" . $image : $image, $this->url, array('rel' => $this->rel, 'class' => $this->class, 'title' => $this->file->desc));
else
    echo !empty($this->maskClass) ? "<div class=\"{$this->maskClass}\"></div>" . $image : $image;

?>