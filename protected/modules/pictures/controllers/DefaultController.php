<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function actionAddFileHtml() {
		$this->layout = '//layouts/empty';
		$model = new File;
		$this->render('addFileHtml', array('model'=>$model));
	}
	
	public function actionAddImageHtml() {
		$this->layout = '//layouts/emptyImage';
		$model = new File;
		$this->render('addImageHtml', array('model'=>$model));
	}
	
	public function actionAddImageHtmlOne() {
		$this->layout = '//layouts/empty';
		$model = new File;
		$this->render('addImageHtmlOne', array('model'=>$model));
	}
	
	public function actionAddImageHtmlPreview() {
		$this->layout = '//layouts/empty';
		$model = new File;
		$this->render('addImageHtmlPreview', array('model'=>$model));
	}
	
	public function actionDelete() {
		if (Yii::app()->request->isAjaxRequest) {
			if (isset($_POST['id'])) {
				$ids = explode('_',$_POST['id']);
				$file = File::model()->findByPk($ids[1]);
				if (!is_null($file))
				{
					@unlink($file->file);
					@unlink($file->getThumb());
					$file->delete();
					echo json_encode(array('success'=>'true'));
				}	else 
				{ echo 'error';}				
			}
			Yii::app()->end();
		}
		
	}
	
	public function actionDeleteTmp() {
		
		if (!Yii::app()->user->isGuest) {
			$files=array();
			$files = File::model()->findAll('entity="temp" and user_id='.Yii::app()->user->id);
			foreach ($files as $file) {		
				@unlink($file->file);
				@unlink($file->getThumb());
				$file->delete();			
			}				
		}
		Yii::app()->end();
	}
	
	public function actionImageAddAjax()
	{

		Yii::import('application.modules.pictures.components.helpers.*');

		$error = "";
		$msg = "";
		$fileElementName = 'fileLoad';

		if (!empty($_FILES[$fileElementName]['error']))
		{
			$error = $this->_getFileErrors($_FILES[$fileElementName]['error']);
		}
		elseif (empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
			$error = Yii::t('app', 'Файл не был загружен');
		else
		{
			$fileinfo = pathinfo($_FILES[$fileElementName]['name']);
			$ext = $fileinfo['extension'];

			// check file size
			if (@filesize($_FILES[$fileElementName]['tmp_name']) > File::MAX_IMAGE_SIZE)
			{
				echo "{error: '" . Yii::t('app', 'Файл слишком большой') . "'}";
				return;
			}
			//check extension
			if ($ext != 'jpg' && $ext !='jpeg' && $ext != 'png' && $ext != 'gif'&& $ext != 'xlsx')
			{
				echo "{error: '" . Yii::t('app', 'Неподдерживаемый формат изображения') . "'}";
				return;
			}
			// check image
            list($width, $height, $type, $attr) = @getimagesize($_FILES[$fileElementName]['tmp_name']);
            if ($width > File::MAX_IMAGE_WIDTH || $height > File::MAX_IMAGE_HEIGHT)
                echo "{error: '" . Yii::t('app', 'Файл слишком большой') . "'}";
			
			/*if ($type != 1 && $type != 2 && $type != 3)
			{
				echo "{error: '" . Yii::t('app', 'Неподдерживаемый формат изображения') . "'}";
				return;
			}*/

			$upfile = CUploadedFile::getInstanceByName($fileElementName);
			
			$output = array(
				'error'=>$error,
				'msg'=>$msg,
				'name'=>$_FILES[$fileElementName]['name']
			);
		
			if ($upfile)
			{
				$fileName = md5(time() . uniqid()) . '.' . $upfile->extensionName;

				$lqPath = yii::getPathOfAlias('webroot') . '/' . File::LQPath;
				$hqPath = yii::getPathOfAlias('webroot') . '/' . File::HQPath;
				$tempPath = yii::getPathOfAlias('webroot') . '/' . File::TempPath;

				// saving HQ photo
				$res = $upfile->saveAs($tempPath . '/' . $fileName);
				
				if ($res)
				{
					// Make Thumb photo
					if (isset($_GET['width'])) {
						$width = $_GET['width'];
						$height = $_GET['height'];
						$method = $_GET['method'];
					} else {
						$width = '100';
						$height = '100';
						$method = 'adaptiveResize';
					}
	
					$thumb = ImageHelper::thumb($width, $height, $tempPath . '/' . $fileName, array('method' => $method), $lqPath . '/' . ImageHelper::THUMB_DIR . '/', null, false);
					
					if(isset($_GET['preview'])){
						$preview = ImageHelper::thumb('800', '600', $tempPath . '/' . $fileName, array('method' => 'resize'), $lqPath . '/' . ImageHelper::THUMB_DIR . '/', null, false);
						$output['preview'] = $preview;
					}
					
//					if(file_exists(yii::getPathOfAlias('webroot') . '/' . File::TempPath. '/' . $fileName)){
              
          /* Yii::app()->ih
           	->load(yii::getPathOfAlias('webroot') . '/' . File::TempPath. '/' . $fileName)
          	->watermark(yii::getPathOfAlias('webroot') . '/images/watermark/logo.png', 10, 20, CImageHandler::CORNER_RIGHT_BOTTOM)
          	->save(yii::getPathOfAlias('webroot') . '/' . File::TempPath. '/' . $fileName);
          	*/
//          }
					// Make LQ photo
					//ImageHelper::thumb(800, 600, $tempPath . '/' . $fileName, null, $lqPath . '/',  $fileName, true);

					// Add file to db
					
					$f = new File;
					$f->savelocation = $tempPath;
					$f->entity = 'temp';
					$f->entity_pk = -1;
					$f->user_id = Yii::app()->user->id;
					$f->file = File::TempPath . '/' . $fileName;
					if ($f->save())
					{
						$fid = $f->id;
						$msg .= '1';
					} else {
						$output['thumb'] = CActiveForm::validate($f);
					}
				}
			}
		}

		
//		if (!is_null($fileName)) $output['file'] = "/images/products/{$fileName}";
		if (!is_null($fileName)) $output['file'] = '/' . File::TempPath . '/' . $fileName;
		if (!is_null($thumb)) $output['thumb'] = str_replace('/./', '/', $thumb);
		if (!is_null($fid)) $output['id'] = $fid;
		
		echo json_encode($output);
		Yii::app()->end();
	}
	
	public function actionFileAddAjax($attribute='Good')
  {
      /* $f = @fopen(yii::getPathOfAlias('webroot') .'/message.log', "a") or
                                    die("error");    
                    
     	 fputs($f,  '2='.json_encode($_FILES)); 
     	 fclose($f);  */
  	//echo '1=';
  			//print_r($_FILES);
  			
        $file = CUploadedFile::getInstanceByName($attribute);
        // testing if file was uploaded
        if (!$file)
            return;

        // delete file of old picture
        //if (!$this->owner->isNewRecord)
        //    $this->deletePicture($this->owner->id);

        $fileName = md5(time() . uniqid()) . '.' . $file->extensionName;
        //$this->owner->$attribute = $fileName;

        //TODO: refactor into more flexable pathes

        // checks if location is writable
        if (!is_writable(yii::app()->getBasePath() . '/../images/'))
            throw new CHttpException(500, 'Location  is not writable');
        // saves file
        Yii::import('application.modules.goods.models.Good');
       $articul = str_replace('.' . $file->extensionName,'',$file->name);
        //$str = strstr($articul, '-', true);
        $index = strpos($articul,'-'); 
        if ($index==false) {
					$index = strpos($articul,' '); 
        }
        if ($index==false) {
					$index = strpos($articul,'_'); 
        }
       
     	 
        if ($index!==false) { 
        	$str = substr($articul,0,$index);
        	$number = substr($articul, $index+1);
        	$articul= $str;
				} else {
					$number = 0;
				}
        $good = Good::model()->find('articul="'.$articul.'"');
        if ($good!=null)
        	$f = File::model()->find('entity="Good" and entity_pk='.$good->id.' and num='.$number);
        	
        if ($f==null) $f = new File;
        else $fileName = str_replace('images/good/','',$f->file);
        
        $file->saveAs(yii::app()->getBasePath() . '/../images/' .strtolower($attribute).'/'. $fileName);
        
       
				$f->savelocation = 'good';
				$f->entity = 'Good';
				$f->entity_pk = $good->id;
				$f->user_id = Yii::app()->user->id;
				$f->file = 'images/good' . '/' . $fileName;
				if ($f->save())
				{
					//$fid = $f->id;
					//$msg .= '1';
				} else {
					
				}
					
        $error = '';
				$msg = 'Ок';
				$output = array(
						'error'=>$error,
						'msg'=>$msg,
						'file'=>$fileName
					);
			  echo json_encode($output);
			 /* $f = @fopen(yii::getPathOfAlias('webroot') .'/message.log', "a") or
                                    die("error");    
                    
     	 fputs($f,  '2='.json_encode($output)); 
     	 fclose($f);  */
        Yii::app()->end();
  }

}