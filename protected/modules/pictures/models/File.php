<?php

/**
 * This is the model class for table "file".
 *
 * The followings are the available columns in table 'file':
 * @property integer $id
 * @property string $file
 * @property string $desc
 * @property integer $insert_date
 * @property integer $modarated
 */
class File extends CActiveRecord
{
	public $savelocation = 'files';
	
	const HQPath = 'images/hqphotos';
	const LQPath = 'images/products';
	
	const TempPath = 'images/temp';
	
	const HQVideoPath = 'images/hqvideos';
	const LQVideoPath = 'images/videos';

	const ArchivePath = 'images/archives';

	const MAX_IMAGE_SIZE = 2097152; //52428800;
    const MAX_IMAGE_WIDTH = 1024;
    const MAX_IMAGE_HEIGHT = 1024;
	const MAX_VIDEO_SIZE = 524288000;

	const KIND_IMAGE = 0;
	const KIND_VIDEO = 1;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return File the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pictures}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('insert_date', 'required'),
			//array('insert_date', 'numerical', 'integerOnly'=>true),
			array('file', 'length', 'max'=>128),
			array('desc', 'length', 'max'=>255),
            array('no', 'safe'),
			//array('file','file','types'=>'jpeg,jpg,png,gif'),
			//array('file','saveFile'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, file, desc, insert_date', 'safe', 'on'=>'search'),
		);
	}


	public function deleteFile($pic = null)
	{
        /*
        ????? ????????: ????? ?????? ??????????? ?? ????,
        ??? ?????? ???? ??????? ?????????. ?????? ????? ?????????? ????????? ???? ?? ?????.
        ?? ????? ???????? ??????? ????????? ???????? ??????.

        ????? ? ??? ????????? ? ????????? ??
        */
  	$fname = 'file';//$this->field_name;
    if(!$pic)
    	$pic = $this->$fname;
    elseif(is_numeric($pic))
    	$pic = self::model()->findByPK($pic)->$fname;

		if(file_exists(yii::app()->basePath . '/../'.$pic))//images/'.$this->savelocation.'/' . $pic))
		{
			unlink(yii::app()->basePath . '/../' . $pic);//images/'.$this->savelocation.'/' . $pic);
			//delete thumbnails
			 list($folder,$module,$filename) = explode("/",$pic);
       $filename = substr($filename, 0,strrpos($filename,'.')); 
       $thumbname = glob($folder.'/'.$module.'/.tmb/thumb_'.$filename.'*');
       //unlink(yii::app()->basePath . '/../'.$thumbname[0]);
       array_map("unlink",$thumbname);
       //delete from temp
       $tempname = glob($folder.'/temp/.tmb/thumb_'.$filename.'*');
       array_map("unlink",$tempname);
		}
	}

	public function beforeDelete()
	{
		$this->deleteFile();
		return true;
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'file' => 'File',
			'desc' => 'Desc',
			'insert_date' => 'Insert Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);

		$criteria->compare('file',$this->file,true);

		$criteria->compare('desc',$this->desc,true);

		$criteria->compare('insert_date',$this->insert_date);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

	function behaviors() {
	    return array(
/*	        'inserttime' => array(
	            'class' => 'application.extensions.behaviors.InsertTimeBehavior',
	        )*/
	    );
	}


	public function modarate()
	{
		$this->modarated = 1;
		$this->save();
	}
  
  public function getThumb()
	{
		$name = explode('/', $this->file);
		
		$ext = explode('.', $name[2]);
		return str_replace($name[2],'.tmb/thumb_'.$ext[0].'_adaptiveResize_200_155.'.$ext[1],$this->file);
	}
    
}