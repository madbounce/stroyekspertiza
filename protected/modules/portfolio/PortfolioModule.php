<?php

class PortfolioModule extends ImageModule
{
    public function init()
    {
        if (empty($this->imagePathAlias))
            throw new CException(Yii::t('image', 'ImageModule.imagePathAlias property must be set.'));

        if (empty($this->tmpPathAlias))
            throw new CException(Yii::t('image', 'ImageModule.tmpPathAlias property must be set.'));

        $this->setImport(array(
            'image.ImageModule',
            'image.models.*',
            'image.components.*',
            'image.components.widgets.*',
            'image.extensions.*',
            'portfolio.PortfolioModule',
            'portfolio.models.*',
            'portfolio.components.*',
            'portfolio.components.widgets.*',
            'portfolio.extensions.*',
        ));
    }

    public static function getImages($entity, $entity_id, $tag, $multiple = true)
    {
        if ($multiple) {
            if (Yii::app()->user->isGuest) {
                $images = PortfolioImage::model()->entity($entity, $entity_id)->tag($tag)->session(Yii::app()->session->sessionID)->findAll();
            } else {
                $images = PortfolioImage::model()->entity($entity, $entity_id)->tag($tag)->user(Yii::app()->user->id)->findAll();
            }
        } else {
            if (Yii::app()->user->isGuest) {
                $images = PortfolioImage::model()->entity($entity, $entity_id)->tag($tag)->session(Yii::app()->session->sessionID)->last()->find();
            } else {
                $images = PortfolioImage::model()->entity($entity, $entity_id)->tag($tag)->user(Yii::app()->user->id)->last()->find();
            }
        }

        if (!empty($images) && !is_array($images))
            $images = array($images);

        return $images;
    }
}
