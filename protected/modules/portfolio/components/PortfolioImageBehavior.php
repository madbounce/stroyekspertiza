<?php

class PortfolioImageBehavior extends ImageBehavior
{
    public function attach($owner)
    {
        parent::attach($owner);

        $this->entity = get_class($this->owner);

        if ($this->multiple) {
            $this->getOwner()->getMetaData()->addRelation('images',
                array(CActiveRecord::HAS_MANY, 'PortfolioImage', 'entity_id',
                    'condition' => 'entity = :entity', 'params' => array(':entity' => $this->entity))
            );

            $this->getOwner()->getMetaData()->addRelation($this->tag,
                array(CActiveRecord::HAS_MANY, 'PortfolioImage', 'entity_id',
                    'condition' => 'entity = :entity AND tag = :tag', 'params' => array(':entity' => $this->entity, ':tag' => $this->tag))
            );
        } else {
            $this->getOwner()->getMetaData()->addRelation($this->tag,
                array(CActiveRecord::HAS_ONE, 'PortfolioImage', 'entity_id',
                    'condition' => 'entity = :entity AND tag = :tag', 'params' => array(':entity' => $this->entity, ':tag' => $this->tag))
            );
        }
    }

    protected function deletePreviousImages()
    {
        $temporaryCriteria = PortfolioImage::model()->getEntityCriteria($this->entity);
        $temporaryCriteria->mergeWith(PortfolioImage::model()->getTemporaryCriteria());

        $criteria = PortfolioImage::model()->getEntityCriteria($this->entity, $this->owner->primaryKey);
        $criteria->mergeWith($temporaryCriteria, false);
        $criteria->mergeWith(PortfolioImage::model()->getTagCriteria($this->tag));

        if (Yii::app()->user->isGuest) {
            $criteria->mergeWith(PortfolioImage::model()->getSessionCriteria(Yii::app()->session->sessionID));
            $images = PortfolioImage::model()->findAll($criteria);
        } else {
            $criteria->mergeWith(PortfolioImage::model()->getUserCriteria(Yii::app()->user->id));
            $images = PortfolioImage::model()->findAll($criteria);
        }

        $count = count($images);
        for ($i = 0; $i < $count - 1; $i++)
            $images[$i]->delete();
    }

    protected function saveImages()
    {
        if (!$this->multiple)
            $this->deletePreviousImages();

        if (Yii::app()->user->isGuest) {
            $images = PortfolioImage::model()->temporary()->entity($this->entity)->tag($this->tag)->session(Yii::app()->session->sessionID)->findAll();
        } else {
            $images = PortfolioImage::model()->temporary()->entity($this->entity)->tag($this->tag)->user(Yii::app()->user->id)->findAll();
        }

        if (!empty($images)) {
            foreach ($images as $image) {
                $image->entity_id = $this->owner->primaryKey;
                $image->save();
            }
        }
    }
}
