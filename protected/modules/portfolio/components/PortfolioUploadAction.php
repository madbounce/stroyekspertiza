<?php

Yii::import('portfolio.PortfolioModule');

class PortfolioUploadAction extends UploadAction
{
    public $tag = 'portfolios';
    public $view = 'portfolio.components.widgets.views.portfolios';

    public function run()
    {
        $this->init();

        if (empty($this->entity))
            throw new CException('UploadAction.entity property must be set.');

        if (empty($this->tag))
            throw new CException('UploadAction.tag property must be set.');

        if (isset($_POST['method']) && $_POST['method'] == 'upload') {
            $files = CUploadedFile::getInstancesByName($this->getName());
            if (!empty($files)) {
                foreach ($files as $file) {
                    $filename = $this->generateFileName($file);

                    $uploadPath = !empty($this->directory) ?
                        PortfolioModule::getPath() . '/' . $this->directory :
                        PortfolioModule::getPath();

                    if (!is_dir($uploadPath))
                        mkdir($uploadPath, 0777, true);

                    $file->saveAs($uploadPath . '/' . $filename);

                    $img = new PortfolioImage;
                    $img->entity = $this->entity;
                    $img->tag = $this->tag;
                    $img->url = !empty($this->directory) ?
                        PortfolioModule::getUrl(false) . '/' . $this->directory . '/' . $filename :
                        PortfolioModule::getUrl(false) . '/' . $filename;
                    $img->filename = $filename;
                    $img->size = $file->size;
                    $img->mime_type = $file->type;
                    $img->name = $file->name;
                    if (Yii::app()->user->isGuest)
                        $img->session = Yii::app()->session->sessionID;
                    else
                        $img->user_id = Yii::app()->user->id;

                    if (!$img->save()) {
                        echo CJSON::encode(array('success' => false));
                        Yii::app()->end();
                    }
                }

                echo CJSON::encode(array(
                    'success' => true,
                    'html' => $this->controller->renderPartial($this->view, array(
                        'images' => PortfolioModule::getImages($this->entity, isset($_POST['entity_id']) ? $_POST['entity_id'] : null, $this->tag, $this->multiple)
                    ), true)
                ));
                Yii::app()->end();
            }
        } elseif (isset($_POST['method']) && $_POST['method'] == 'delete') {
            if (isset($_POST['id'])) {
                $model = PortfolioImage::model()->findByPk($_POST['id']);

                if (!empty($model) && $this->accessCallback($model)) {
                    if ($model->delete()) {
                        echo CJSON::encode(array(
                            'success' => true,
                            'html' => $this->controller->renderPartial($this->view, array(
                                'images' => PortfolioModule::getImages($this->entity, isset($_POST['entity_id']) ? $_POST['entity_id'] : null, $this->tag, $this->multiple)
                            ), true)
                        ));
                        Yii::app()->end();
                    }
                }

                echo CJSON::encode(array('success' => false));
                Yii::app()->end();
            }
        } elseif (isset($_POST['method']) && $_POST['method'] == 'description') {
            if (isset($_POST['id'])) {
                $model = PortfolioImage::model()->findByPk($_POST['id']);

                if (!empty($model) && $this->accessCallback($model) && isset($_POST['description'])) {
                    $model->description = $_POST['description'];
                    if ($model->save()) {
                        echo CJSON::encode(array(
                            'success' => true,
                            'html' => $this->controller->renderPartial($this->view, array(
                                'images' => PortfolioModule::getImages($this->entity, isset($_POST['entity_id']) ? $_POST['entity_id'] : null, $this->tag, $this->multiple)
                            ), true)
                        ));
                        Yii::app()->end();
                    }
                }
            }
        }
    }
}
