<?php

Yii::import('image.components.widgets.UploadWidget');
Yii::import('portfolio.PortfolioModule');

class PortfolioUploadWidget extends UploadWidget
{
    public $tag = 'portfolios';
    public $view = 'portfolios';
    public $action = array('/upload/portfolios');

    protected function renderContent()
    {
        echo '<div class="' . $this->id . '">';
        echo '<div class="uploaded-images">';
        $this->renderUploadedImages($this->getImages());
        echo '</div>';
        echo '</div>';
    }

    protected function getImages()
    {
        return PortfolioModule::getImages(get_class($this->model), $this->model->primaryKey, $this->tag, $this->multiple);
    }

    public function publishAssets()
    {
        parent::publishAssets();

        Yii::app()->clientScript->registerScript(__CLASS__ . "#{$this->id}-description", '
        $(".' . $this->id . ' .description").live("blur", function() {
            var id = $(this).attr("data-id");
            var description = $(this).val();
            $.ajax({
                url: "' . $this->_url . '",
                type: "POST",
                data: ' . CJavaScript::encode(array('method' => 'description', 'id' => 'js:id', 'entity_id' => $this->model->primaryKey, 'description' => 'js:description')) .',
                dataType: "json",
            });
            return false;
        });
        ', CClientScript::POS_READY);
    }
}
