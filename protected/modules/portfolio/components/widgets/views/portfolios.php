<?php if (!empty($images)): ?>
<div id="portfolios">
    <?php foreach ($images as $image): ?>
    <div class="portfolio-item">
        <div class="number_portfolio"></div>
        <div class="block_portfolio_freelance">
            <?php echo CHtml::activeTextField($image, 'description', array('class' => 'description', 'data-id' => $image->id, 'placeholder' => Yii::t('profile', 'Описание проекта'))); ?>
            <div>
                <?php echo CHtml::image(Yii::app()->baseUrl . ImageHelper::thumb(300, 88, $image->url, array('method' => 'adaptiveResize')), '', array('class' => 'avaimg')); ?>
                <?php echo CHtml::link(Yii::t('image', 'Удалить'), '#', array('class' => 'delete-link', 'data-id' => $image->id)); ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<div class="block_portfolio_freelance">
    <div>
        <?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/new_portfolio.jpg', '', array('width' => 300, 'height' => 88, 'class' => 'upload-button')); ?>
    </div>
    <?php echo CHtml::link(Yii::t('image', 'Загрузить'), '#', array('class' => 'upload-button green load_portf')); ?><br>
</div>

<div class="clear"></div>