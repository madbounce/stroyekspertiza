<?php

/**
 * This is the model class for table "portfolio_image".
 *
 * The followings are the available columns in table 'portfolio_image':
 * @property string $description
 */
class PortfolioImage extends Image
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{portfolio_image}}';
    }

    public function rules()
    {
        return array(
            array('description', 'safe'),
        );
    }
}
