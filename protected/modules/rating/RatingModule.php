<?php

class RatingModule extends CWebModule
{
    public function init()
    {
        $this->setImport(array(
            'rating.models.*',
            'rating.components.*',
        ));
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        } else
            return false;
    }

    public static function link($name, $htmlOptions = array())
    {
        $model = Rating::model()->findByAttributes(array('name' => $name));
        return CHtml::link(CHtml::encode($model->title), array('/rating/default/view', 'name' => $model->name), $htmlOptions);
    }
}
