<?php

class AdminController extends BackEndController
{
    public $layout = '//layouts/main_catalog';

    public function actions(){
        return array(
            'toggle' => 'ext.jtogglecolumn.ToggleAction',
        );
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $rating = $this->loadModel($id);
            $transaction = Yii::app()->db->beginTransaction();
            try
            {
                $rating->loadJoinModel()->deleteRating($rating);
                $rating->delete();

                $transaction->commit();
            }
            catch (Exception $e)
            {
                $transaction->rollBack();
                throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
            }


            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    public function actionIndex()
    {
        $this->redirect(array('admin'));
    }

    public function actionAdmin()
    {
        $model = new Rating('adminSearch');
        $model->unsetAttributes();
        if (isset($_GET['Rating']))
            $model->attributes = $_GET['Rating'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }


    public function actionToggle($id,$attribute) {
        $model = Rating::model()->findByPk($id);
        if($model){
            if($model->status == 1){
                $model->loadJoinModel()->deleteRating($model);
                $model->status = 0;
            }else{
                $model->loadJoinModel()->addRating($model);
                $model->status = 1;
            }
        }
        $model->save(false);
        $model->sendMailStatusInfo();
    }

    public function actionToggleConfirm($id,$attribute)
    {
        /** @var $model Rating */
        $model = Rating::model()->findByPk($id);
        if($model){
            if($model->status_confirm == 1){
                $model->status_confirm = 0;
            }else{
                $model->status_confirm = 1;
            }
        }
        $model->save(false);
    }

    public function loadModel($id, $ml = false)
    {
        $model = Rating::model()->findByPk($id);

        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'page-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
