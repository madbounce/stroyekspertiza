<?php

class DefaultController extends FrontEndController
{
    public $layout = '//layouts/main';

    public function actionConfirm($key)
    {
        $model = Rating::model()->confirm_inactive()->inactive()->findByKey($key);
        if ($model === null) throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $joinModel = $model->loadJoinModel();

        /** TODO delete model Rating */
        if(!$joinModel) throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            //$joinModel->addRating($model);

            $model->status_confirm = 1;
            $model->save();

            $transaction->commit();

            $model->sendMailExpert();
            $this->redirect(array('/rating/default/info','view'=>'confirm','id'=>$model->id));
        }
        catch (Exception $e)
        {
            $transaction->rollBack();
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        }
    }

    public function actionInfo($view,$id)
    {
        if(!$this->getViewFile($view))
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $model = Rating::model()->inactive()->findByPk($id);
        if ($model === null) throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $modelName = $model->model_name;
        $joinModel = $modelName::model()->findByPk($model->model_id);

        if(!$joinModel) throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $this->render($view,array(
            'model' => $model,
            'joinModel' => $joinModel
        ));
    }

    public function actionExpert($type,$key)
    {
        $model = Rating::model()->findByExpertKey($key);
        if ($model === null) throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $modelName = $model->model_name;
        $joinModel = $modelName::model()->findByPk($model->model_id);

        if(!$joinModel) throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $transaction = Yii::app()->db->beginTransaction();
        try
        {
            if($type == 'active'){
                $model->status= Rating::STATUS_ACTIVE;
                $joinModel->addRating($model);
            }elseif($type == 'inactive'){
                $model->status = Rating::STATUS_INACTIVE;
                $joinModel->deleteRating($model);
            }else
                throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

            $model->save();
            $transaction->commit();
            $model->sendMailStatusInfo();
        }
        catch (Exception $e)
        {
            $transaction->rollBack();
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        }
        $this->redirect(array('/rating/default/expertInfo', 'type'=>$type,'key'=>$key));
    }

    public function actionExpertInfo($type,$key)
    {
        $model = Rating::model()->findByExpertKey($key);
        if ($model === null) throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $modelName = $model->model_name;
        $joinModel = $modelName::model()->findByPk($model->model_id);

        if(!$joinModel) throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));

        $this->render('expert',array(
            'model' => $model,
            'joinModel' => $joinModel
        ));
    }

    public function loadModel($id)
    {
        $model = Rating::model()->findByPk($id);

        if ($model === null)
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не существует.'));
        return $model;
    }
}