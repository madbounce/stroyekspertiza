<?php

/**
 * This is the model class for table "rating".
 *
 * The followings are the available columns in table 'rating':
 * @property string $id
 * @property string $model_id
 * @property string $model_name
 * @property string $user_id
 * @property string $text
 * @property integer $value
 *
 * The followings are the available model relations:
 * @property User $user
 */
class Rating extends CActiveRecord
{

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_CONFIRM_INACTIVE = 0;
    const STATUS_CONFIRM_ACTIVE = 1;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className
	 * @return Rating
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rating';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('model_id, user_id, value_1, value_2, value_3, value_avg', 'required'),
			array('value_1, value_2, value_3, value_avg, status, status_confirm', 'numerical', 'integerOnly'=>true),
			array('model_id, user_id', 'length', 'max'=>10),
			array('model_name', 'length', 'max'=>255),
			array('access_key, expert_key', 'length', 'max'=>32),
			array('text', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, model_id, model_name, user_id, text, value_1, value_2, value_3, value_avg,', 'safe', 'on'=>'search'),
			array('id, model_id, model_name, user_id, text, value_1, value_2, value_3, value_avg, status, status_confirm', 'safe', 'on'=>'adminSearch'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

    public function scopes()
    {
        return array(
            'inactive'=>array('condition'=>'status='.self::STATUS_INACTIVE),
            'active'=>array('condition'=>'status='.self::STATUS_ACTIVE),
            'confirm_inactive'=>array('condition'=>'status_confirm='.self::STATUS_CONFIRM_INACTIVE),
            'confirm_active'=>array('condition'=>'status_confirm='.self::STATUS_CONFIRM_ACTIVE),
            'date'=>array('order'=>'create_date DESC'),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'model_id' => 'Model',
			'model_name' => 'Model Name',
			'user_id' => 'Пользователь',
			'text' => 'Текст',
			'value_1' => 'Рейтинг',
			'value_2' => 'Рейтинг',
			'value_3' => 'Рейтинг',
			'value_avg' => 'Рейтинг',
			'status' => 'Статус',
			'status_confirm' => 'Подтверждение',
			'access_key' => 'Ключ доступа',
			'expert_key' => 'Ключ доступа',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('model_id',$this->model_id,true);
		$criteria->compare('model_name',$this->model_name,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('value',$this->value);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function adminSearch()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('model_id', $this->model_id, true);
        $criteria->compare('model_name', $this->model_name, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('value_1', $this->value_1, true);
        $criteria->compare('value_2', $this->value_2, true);
        $criteria->compare('value_3', $this->value_3, true);
        $criteria->compare('value_avg', $this->value_avg, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('status_confirm', $this->status, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function findByKey($key)
    {
        return $this->findByAttributes(array('access_key'=>$key));
    }

    public function findByExpertKey($key)
    {
        return $this->findByAttributes(array('expert_key'=>$key));
    }

    public function findByModel($model,$user_id = null)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('model_id = '.$model->id);
        $criteria->addCondition('model_name = "'.get_class($model).'"');
        if($user_id){
            $criteria->addCondition('user_id = '.$user_id);
            return $this->find($criteria);
        }else{
            return $this->findAll($criteria);
        }
    }

    public function getRows($model, $type, $model_ids='')
    {
        $criteria = new CDbCriteria();

        $criteria->addCondition('`model_name`="' . get_class($model) . '"');
        $criteria->addCondition('`type`="' . $type . '"');

        if ($model_ids){
            if (is_array($model_ids) && count($model_ids)){
                $criteria->addInCondition('`model_id`', $model_ids);
                $data = $this->findAll($criteria);
            }else{
                $criteria->addCondition('`model_id`="' . $model_ids . '"');
                $data = $this->find($criteria);
            }
        }else{
            $criteria->addCondition('`model_id`="' . $model->id . '"');
            $data = $this->find($criteria);
        }

        return $data;
    }

    public function add($model, $data, $user_id)
    {
        $vars = array(
            "user_id"		=> $user_id,
            "model_id"		=> $model->id,
            "model_name"	=> get_class($model),
            "value_1"       => $data['value_1'],
            "value_2"       => $data['value_2'],
            "value_3"       => $data['value_3'],
            "value_avg"     => round(($data['value_1']+$data['value_2']+$data['value_3'])/3),
            "text"          => @$data['text'],
            "created_date"  => date('Y-m-d H:i:s',time()),
            "access_key"    => md5(uniqid()),
            "expert_key"    => md5(uniqid()),
        );

        $model = $this->findByModel($model,$user_id);

        if (!$model)
        {
            $rating = new Rating();
            $rating->attributes = $vars;
            $res = $rating->save();
            return $rating;
        }
        else
        {
            return false;
        }
    }

    public function loadJoinModel()
    {
        $modelName = $this->model_name;
        $joinModel = $modelName::model()->findByPk($this->model_id);
        return $joinModel;
    }

    /**
     * @param $user User
     */
    public function sendMailConfirm($user, $tpl = 'ConfirmRating')
    {
        if($user instanceof User){
            Yii::app()->getModule('mail')->send($user->email, Config::get('infoEmail'), $tpl, array(
                'siteNameLink' => CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')),
                'confirmLink' => CHtml::link(
                    Misc::t('ссылке'),
                    Yii::app()->createAbsoluteUrl('/rating/default/confirm',array('key' => $this->access_key))
                )
            ));
        }
    }


    public function sendMailStatusInfo()
    {
        $joinModel = $this->loadJoinModel();

        if($this->status)
            $tpl = 'StatusActiveInfoRating';
        else
            $tpl = 'StatusInactiveInfoRating';

        Yii::app()->getModule('mail')->send($this->user->email, Config::get('infoEmail'), $tpl, array(
            'siteNameLink' => CHtml::link(Yii::app()->params['siteName'], Yii::app()->request->hostInfo),
            'objectLink' => CHtml::link($joinModel->name, Yii::app()->request->hostInfo . '/products/view/'. $joinModel->url),
        ));
    }

    /**
     * @param $user User
     */
    public function sendMailExpert($tpl = 'ExpertRating')
    {
        $joinModel = $this->loadJoinModel();


        Yii::app()->getModule('mail')->send(Config::get('inboxEmail'), Config::get('infoEmail'), $tpl, array(
            'siteNameLink' => CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')),
            'userName' => $this->user->first_name. ' ' .$this->user->last_name,
            'email' => $this->user->email,
            'text' => $this->text,
            'valueAvg' => $this->value_avg,
            'value1' => $this->value_1,
            'value2' => $this->value_2,
            'value3' => $this->value_3,
            'objectLink' => CHtml::link($joinModel->name, Yii::app()->request->hostInfo . $joinModel->getUrl()),
            'activeLink' => CHtml::link(
                Misc::t('Одобрить'),
                Yii::app()->createAbsoluteUrl('/rating/default/expert/',array('type'=>'active','key'=>$this->expert_key))
            ),
            'inactiveLink' => CHtml::link(
                Misc::t('Отклонить'),
                Yii::app()->createAbsoluteUrl('/rating/default/expert/',array('type'=>'inactive','key'=>$this->expert_key))
            )
        ));

    }

}