<?php
$showTitle = isset($showTitle) ? $showTitle : true;
$class = isset($class) && $class ? $class : '';
$id = isset($id) && $id ? $id : '';



    if(isset($data)){
        $dataId = isset($data->id) ? $data->id : 'new';
        $rating_avg = isset($data->rating_avg) ? $data->rating_avg : 0;
        $voices = isset($data->voices) ? $data->voices : 0;
        $rel = isset($data) ? strtolower(get_class($data)).'_'.$id : '';
    }else{
        $dataId = isset($dataId) ? $dataId : 'new';
        $rating_avg = isset($rating_avg) ? $rating_avg : 0;
        $voices = isset($voices) ? $voices : 0;
        $rel = '';
    }
?>

<div id="<?=$id?>" class="rating <?=$class?>">

    <?php if($showTitle && isset($data)): ?>
    <div class="count-rating">Рейтинг <span><?=$rating_avg?></span> Голосов <span><?=$voices?></span>
        <div class="pointer"></div>
    </div>
    <?php endif;?>

    <div class='rating-stars' rel='<?=$rel?>'>
        <?php for($i=1;$i<=5;$i++): ?>

        <?php
        $check = ($i <= round($rating_avg/ 0.5)*0.5) ? 'checked="checked"' : '';
        $disabled = $readonly ? 'disabled="disabled"' : '';
        ?>
        <input  name="star_<?=$dataId?>" type="radio" class="rating_star" value="<?=$i?>" title="&nbsp;" <?=$disabled?> <?=$check?> />

        <?php endfor; ?>

        <div class="clear"></div>
    </div>

</div>