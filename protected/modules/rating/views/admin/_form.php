<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'page-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'product'
    ),
)); ?>

<?php echo $form->errorSummary($model); ?>

    <div id="name">
        <?php echo $form->textField($model, 'title', array('class' => 'name', 'maxlength' => 255)); ?>
        <div class="checkbox"></div>
    </div>


    <!-- Параметры страницы -->
    <div class="block layer" style="width: inherit;">
        <h2>Параметры страницы</h2>
        <ul>
            <li>
                <?php echo $form->labelEx($model, 'url', array('class' => 'property')); ?>
                <div class="page_url">/faq/</div>
                <?php echo $form->textField($model, 'url', array('class' => 'page_url', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'meta_title', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'meta_title', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'meta_keywords', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'meta_keywords', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'meta_description', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'meta_description', array('class' => 'simpla_inp')); ?>
            </li>
        </ul>
    </div><!-- Параметры страницы (The End)-->


<div style="clear: both"></div>


<div class="editor-line">
    <?php echo $form->labelEx($model,'content'); ?>
    <?php //echo $form->textArea($model,'content',array('class'=>'medium')); ?>
    <?php $this->widget('application.extensions.redactorjs.ERedactorWidget', array(
        'model' => $model,
        'attribute' => 'content',
        'options' => array(
            'lang' => 'ru',
            'convertDivs' => false,
            'observeImages' => false,
            'imageUpload' => CHtml::normalizeUrl(array('/faq/admin/redactorUploadImage'))
        ),
    )); ?>
    <?php echo $form->error($model,'content'); ?>
</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>