<?php
    $dp = $model->adminSearch();
    $countAll = $dp->totalItemCount;
?>

    <div class="page-content">

        <div class="page-header admin-header">
            <h3><?php echo Yii::t('app', '{n} отзыв|{n} отзыва|{n} отзывов', $countAll);?></h3>
        </div>

        <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
            'id' => 'page-grid',
            'type'=>'striped bordered',
            'dataProvider' => $dp,
            //'filter' => $model,
            'template' => '{pager}{items}{pager}',
            'afterAjaxUpdate'=>"function() {
                jQuery('.rating-block input').rating({'readOnly':true});
            }",
            'columns' => array(
                array(
                    'header' => 'Дата создания',
                    'name' => 'created_date',
                    'value' => 'Yii::app()->dateFormatter->format("dd MMMM yyyy", $data->created_date)',
                    'htmlOptions' => array('style' => 'width: 120px')
                ),
                array(
                    'header' => 'ФИО',
//                    'type'=>'raw',
                    'value' => '$data->user->first_name. " ". $data->user->last_name',
                ),
                array(
                    'header' => 'Обьект',
                    'type'=>'raw',
                    'value' => function($data){
                        $model = $data->loadJoinModel();
                        return CHtml::link($model->name,'/products/view/'.$model->url);
                    },
                ),
                array(
                    'name' => 'text',
//                    'type'=>'raw',
                    'value' => '$data->text',
                ),

                array(
                    'name' => 'rating',
                    'type' => 'raw',
                    'value'=>'$this->grid->controller->widget("CStarRating", array(
                        "name" => $data->id,
                        "id" => $data->id,
                        "value" => $data->value_avg,
                        "readOnly" => true,
                        "ratingStepSize" => 1,
                        "minRating" => 1,
                        "maxRating" => 5,
                    ), true) ."<br>". $data->value_1. " | ". $data->value_2. " | ". $data->value_3',
                    'headerHtmlOptions' => array('style' => 'width:85px;'),
                    'htmlOptions' => array('class' => 'rating-block', 'style'=>'text-align:center'),
                    'filter' => false,
                    'sortable' => false,
                ),

                array(
                    'class' => 'bootstrap.widgets.TbToggleColumn',
                    'name' => 'status_confirm',
                    'checkedButtonLabel' => Yii::t('admin', 'Удалить подтверждение'),
                    'uncheckedButtonLabel' => Yii::t('admin', 'Подтвердить'),
                    'toggleAction'=>'toggleConfirm',
                    'htmlOptions' => array(
                        'width' => '50',
                        'style' => 'text-align:center'
                    ),
                ),
                array(
                    'class' => 'bootstrap.widgets.TbToggleColumn',
                    'name' => 'status',
                    'checkedButtonLabel' => Yii::t('admin', 'Отклонить'),
                    'uncheckedButtonLabel' => Yii::t('admin', 'Одобрить'),
                    'toggleAction'=>'toggle',
                    'htmlOptions' => array(
                        'width' => '50',
                        'style' => 'text-align:center'
                    ),
                ),
                array(
                    'class' => 'bootstrap.widgets.TbButtonColumn',
                    'template' => '{delete}',
                    'htmlOptions' => array(
                        'style' => 'text-align:center'
                    ),
                ),
            ),
        )); ?>

    </div>