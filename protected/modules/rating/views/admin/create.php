<div class="page-header">
    <h3><?php echo Yii::t('faq', 'Создание вопроса'); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>