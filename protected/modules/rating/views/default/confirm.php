<?php
    $this->breadcrumbs=array(
        Yii::t('rating','Подтверждение отзыва'),
    );
?>

<div class="confirm-rating" style="margin-top: 45px;">

    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 15,'view'=>'textClickable')); ?>

    <div class="fix_page">
	<h1>Спасибо!</h1>
        <p>Ваш отзыв на странице -</p>
        <div><?php echo CHtml::link($joinModel->name,$joinModel->getUrl()); ?></div>
        <br>
        <p>после проверки модератором, будет добавлен</p>
    </div>

</div>