<?php
    $this->breadcrumbs=array(
        Yii::t('rating','Подтверждение отзыва'),
    );
?>

<div class="confirm-rating" style="margin-top: 45px;">

    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 15,'view'=>'textClickable')); ?>

    <div class="fix_page">

        <?php if($model->status): ?>
            <h1>Вы одобрили отзыв на странице</h1>
        <?php else: ?>
            <h1>Вы отклонили отзыв на странице</h1>
        <?php endif; ?>

        <div><?php echo CHtml::link($joinModel->name,$joinModel->getUrl()); ?></div>
        <br>
        От пользователя <?php echo $model->user->first_name.' '. $model->user->last_name;?> <br>
        Email: <?php echo $model->user->email;?> <br><br>
        Текст: <br>
        <?php echo $model->text;?>
        <br>
        <br>
        Оценка продукта <br>
        Заметные результаты: <?php echo $model->value_1;?><br>
        Пищевая ценность: <?php echo $model->value_2;?><br>
        Наслаждение питомца: <?php echo $model->value_3;?><br>
        <br>
        <br>
    </div>

</div>