<?php
    $this->breadcrumbs=array(
        Yii::t('rating','Подтверждение отзыва'),
    );
?>

<div class="confirm-rating" style="margin-top: 45px;">

    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 15,'view'=>'textClickable')); ?>

    <div class="fix_page">
        <p>
            Ваш отзыв успешно добавлен к продукту -
            <div><?php echo CHtml::link($joinModel->name,$joinModel->getUrl()); ?></div>
        </p>
    </div>

</div>