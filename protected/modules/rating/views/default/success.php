<?php
$this->breadcrumbs=array(
    Yii::t('rating','Подтверждение отзыва'),
);
?>


<div class="success-rating" style="margin-top: 45px;">

    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 15,'view'=>'textClickable')); ?>

    <div class="fix_page">
	<h1>Спасибо!</h1>
        <p>Вы оставили отзыв на странице -</p>
        <div><?php echo CHtml::link($joinModel->name,$joinModel->getUrl()); ?></div>
        <br>
        <p>На Вашу почту выслано письмо для подтверждения.</p>
    </div>

</div>