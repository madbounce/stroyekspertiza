<?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.rating.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/facebox.js');

    $cs=Yii::app()->getClientScript();
    $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/stars.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/facebox.css');
?>

<script>

//    jQuery.facebox({ ajax: 'remote.html' }, 'my-groovy-style');

    var initRating = function (block) {
        $(block).rating({
            'split':1,
            'required':true,
            callback:function (value, link) {
//                var module = $(this).parent('div').attr('rel');
//                var rate = $(this).parent('div').find('input');
//                module = module.split('_');
//                if (!module[0] || !module[1]) return;

                $('input[name="value"]').val(value);
            }
        });

        $(".rating").hover(function () {
            $(this).parent().find(".count-rating").show();
        }, function () {
            $(this).parent().find(".count-rating").hide();
        })
    }
    $(document).ready(function () {
        initRating($('.product-list .product .rating input.rating_star'));
        initRating($('.catalog-rating .rating input.rating_star'));
        initRating($('#review-value.rating input.rating_star'));
        initRating($('#new-rating input.rating_star'));
    })
</script>