<?php

class CategorySearchWidget extends CWidget
{
    public $category;

    public function run()
    {
        $model = new SearchForm;
        $model->unsetAttributes();
        $model->category = $this->category->id;

        $this->render('categorySearch', array(
            'model' => $model,
            'category' => $this->category,
        ));
    }
}