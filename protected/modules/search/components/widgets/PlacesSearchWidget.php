<?php

class PlacesSearchWidget extends CWidget
{
    public function run()
    {
        $model = new PlacesSearchForm;
        $model->unsetAttributes();
        $model->entity = 'Place';

        if (isset($_GET['PlacesSearchForm']))
            $model->attributes = $_GET['PlacesSearchForm'];

        $this->render('placesSearch', array(
            'model' => $model,
        ));
    }
}
