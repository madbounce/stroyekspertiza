<?php

class SearchWidget extends CWidget
{
    public function run()
    {
        $model = new SearchForm;
        $model->unsetAttributes();

        if (isset($_GET['SearchForm']))
            $model->attributes = $_GET['SearchForm'];

        $this->render('search', array(
            'model' => $model,
        ));
    }
}