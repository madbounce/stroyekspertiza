<?php

class SiteSearchWidget extends CWidget
{
    public function run()
    {
        $model = new SearchForm;
        $model->unsetAttributes();
        if (isset($_GET['SearchForm']))
            $model->attributes = $_GET['SearchForm'];

        $this->render('siteSearch', array(
            'model' => $model,
        ));
    }
}
