<?php $form = $this->beginWidget('CActiveForm', array(
    'action' => array('/search/default/index'),
    'method' => 'get',
)); ?>

<h3><?php echo Yii::t('event', 'Поиск в разделе &laquo;:category&raquo;', array(':category' => Yii::t('category', $category->title))); ?><h3>
    <?php echo $form->textField($model, 'keyword'); ?>
    <?php echo $form->hiddenField($model, 'category'); ?>

    <?php echo CHtml::submitButton(false, array('id' => 'search', 'name' => 'search', 'class' => 'short-search')); ?>

<?php $this->endWidget(); ?>