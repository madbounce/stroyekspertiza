<?php
/**
 * @var $form CActiveForm
 */
?>

<?php
Yii::app()->clientScript->registerScriptFile('http://maps.googleapis.com/maps/api/js?sensor=false', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('places-search-map', '
    $("body").append($("#places-map-dialog")[0].outerHTML);
    $("#places-map-dialog").remove();

    function initializePlacesMap() {
        placesMapSearch = new google.maps.Map(document.getElementById("div-places-map-search"), {
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        if (document.getElementById("PlacesSearchForm_latitude").value && document.getElementById("PlacesSearchForm_longitude").value)
            var center = new google.maps.LatLng(document.getElementById("PlacesSearchForm_latitude").value, document.getElementById("PlacesSearchForm_longitude").value);
        else
            var center = new google.maps.LatLng(55.74291999316024, 37.615327715026865);

        var markerSearch = new google.maps.Marker({
            map: placesMapSearch,
            position: center,
        });

        placesMapSearch.setCenter(center);

        google.maps.event.addListener(placesMapSearch, "click", function (event) {
            markerSearch.setPosition(event.latLng);
            placesMapSearch.setCenter(markerSearch.position);
            document.getElementById("PlacesSearchForm_latitude").value = event.latLng.lat();
            document.getElementById("PlacesSearchForm_longitude").value = event.latLng.lng();
        });

        google.maps.event.addListener(placesMapSearch, "resize", function() {
            placesMapSearch.setCenter(markerSearch.position);
        });
    }

    showPlacesMapDialog = function() {
        $(".mess_news_wr").hide("fast");
        $(".mess_news").show();
        $("#places-map-dialog").show();
        $("#places-map-dialog").css("top", ($(window).scrollTop() + 40) + "px");
        google.maps.event.trigger(placesMapSearch, "resize");
        return false;
    }

    hidePlacesMapDialog = function() {
        $("#places-map-dialog").hide();
        $(".mess_news").hide();
        return false;
    }

    hidePlacesMapDialog();

    $("#places-map-dialog .popup-close, .mess_news").click(function() {
        hidePlacesMapDialog();
    });

    ', CClientScript::POS_END);
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'action' => array('/place/default/search'),
    'method' => 'get',
)); ?>

<?php echo $form->labelEx($model, 'keyword', array('class' => 'label_name_type')); ?>
<?php echo $form->textField($model, 'keyword'); ?>
<hr />

<?php echo $form->labelEx($model, 'category', array('class' => 'label_name_type')); ?>
<p class="category">
    <?php echo $form->checkBoxList($model, 'category', Category::getCategoryItems(), array('uncheckValue' => null, 'separator' => ' ', 'labelOptions' => array('class' => 'label_check'))); ?>
</p>
<hr/>

<p>
    <?php $index = 0; ?>
    <?php foreach (Event::getSexItems() as $id => $item): ?>
    <?php $checkbox = CHtml::checkBox('PlacesSearchForm[sex][]', is_array($model->sex) ? in_array($id, $model->sex) : false, array('id' => "PlacesSearchForm_sex_$index", 'value' => $id)); ?>
    <?php echo "<span class=\"span-$index\">" . CHtml::label($checkbox . $item, "PlacesSearchForm_sex_$index", array('class' => 'label_check' . " label-$index")); ?>
    <?php echo CHtml::image($this->owner->imagePath . 'css/i/' . Event::getSexPictureItem($id))  . '</span>'; ?>
    <?php $index++; ?>
    <?php endforeach; ?>

    <?php $id = 'm,g'; $item = Yii::t('search', 'мальчик и девочка'); ?>
    <?php $checkbox = CHtml::checkBox('PlacesSearchForm[sex][]', (isset($_GET['PlacesSearchForm']['sex']) && is_array($_GET['PlacesSearchForm']['sex'])) ? in_array($id, $_GET['PlacesSearchForm']['sex']) : false, array('id' => "SearchForm_sex_$index", 'value' => $id)); ?>
    <?php echo "<span class=\"span-$index\">" . CHtml::label($checkbox . $item, "PlacesSearchForm_sex_$index", array('class' => 'label_check' . " label-$index")); ?>
    <?php echo CHtml::image($this->owner->imagePath . 'css/i/' . Event::getSexPictureItem($id))  . '</span>'; ?>
</p>
<hr/>

<?php echo $form->labelEx($model, 'activity', array('class' => 'label_name_type')); ?>
<p>
    <?php $index = 0; ?>
    <?php foreach (Event::getActivityItems() as $id => $item): ?>
    <?php $checkbox = CHtml::checkBox('PlacesSearchForm[activity][]', is_array($model->activity) ? in_array($id, $model->activity) : false, array('id' => "PlacesSearchForm_activity_$index", 'value' => $id)); ?>
    <?php echo "<span class=\"span-$index\">" . CHtml::label($checkbox . $item, "PlacesSearchForm_activity_$index", array('class' => 'label_check' . " label-$index")); ?>
    <?php echo CHtml::image($this->owner->imagePath . 'css/i/' . Event::getActivityPicture($id)) . '</span><br />'; ?>
    <?php $index++; ?>
    <?php endforeach; ?>
</p>
<hr/>

<?php echo $form->labelEx($model, 'age', array('class' => 'label_name_type')); ?>
<p>
    <?php echo $form->checkBoxList($model, 'age', Event::getAgeItems(),
    array(
        'uncheckValue' => null,
        'separator' => ' ',
        'labelOptions' => array('class' => 'label_check'),
    )
); ?>
</p>
<hr>

<?php echo CHtml::label(Yii::t('search', 'По карте') . ':', false, array('class' => 'label_name_type')); ?>
<?php echo $form->hiddenField($model, 'latitude'); ?>
<?php echo $form->hiddenField($model, 'longitude'); ?>
<p>
    <?php echo CHtml::link(Yii::t('search', 'Выбрать точку на карте'), '#', array('id' => 'map', 'onclick' => "{ initializePlacesMap(); showPlacesMapDialog(); return false; }")); ?>
</p>

<div class="common-popup popup2" id="places-map-dialog" style="display: none;">
    <?php echo CHtml::link('', 'javascript:void(0);', array('class' => 'popup-close')); ?>
    <h3 class="popup-title"><?php echo Yii::t('search', 'Выберите точку на карте'); ?>:</h3>

    <div id="div-places-map-search" style="height: 400px; margin: 20px;""></div>
</div>
<hr/>

<p class="birthday">
    <?php echo $form->checkBox($model, 'birthday', array('uncheckValue' => null)); ?>
    <?php echo $form->labelEx($model, 'birthday', array('class' => 'label_check')); ?>
    <?php echo CHtml::image($this->owner->imagePath . 'css/i/ico9.png'); ?>
</p>
<hr/>

<p>
    <?php echo $form->labelEx($model, 'metro', array('class' => 'label_name_type')); ?>
    <?php echo $form->textField($model, 'metro'); ?>
</p>
<br>
<p>
    <?php echo $form->labelEx($model, 'district', array('class' => 'label_name_type')); ?>
    <?php echo $form->textField($model, 'district'); ?>
</p>

<br>
<?php echo CHtml::submitButton(false, array('id' => 'search', 'name' => 'search')); ?>

<?php $this->endWidget(); ?>
