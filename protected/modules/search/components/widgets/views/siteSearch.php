<?php $form = $this->beginWidget('CActiveForm', array(
    'action' => array('/search/default/index'),
    'method' => 'get',
)); ?>

    <?php echo $form->textField($model, 'keyword', array('class' => 'site-search', 'placeholder' => Yii::t('app', 'Поиск по сайту ...'))); ?>

<?php $this->endWidget(); ?>