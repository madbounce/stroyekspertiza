<?php

class AdminController extends Controller
{
    public $layout = '//layouts/column2';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('admin', 'moderator')),
            array('deny', 'users' => array('*'))
        );
    }

    public function actionSearch($searchKeyword = null)
    {
        $model = new SearchForm;
        $model->unsetAttributes();
        $model->keyword = $searchKeyword;

        $this->render('search', array(
            'model' => $model,
        ));
    }
}
