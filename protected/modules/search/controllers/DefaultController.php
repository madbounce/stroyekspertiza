<?php

class DefaultController extends Controller
{
    public $layout = '//layouts/main';

    public function actionIndex()
    {
        $model = new SearchForm();
        $model->unsetAttributes();

        if (isset($_GET['searchText'])) {
            $model->keyword = $_GET['searchText'];

            //die($model->keyword);
        }
		
//		die(var_dump($model)); 

        $this->render('search', array(
            'model' => $model
        ));
    }

    public function actionAutoComplete($term)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $model = new SearchForm('adminSearch');
            $model->keyword = $term;
            $dataProvider = $model->adminSearch();

            echo CJSON::encode(array(
                'status' => 'success',
                'html' => $this->renderPartial('_autocomplete', array('models' => $dataProvider->data), true),
            ));
            Yii::app()->end();
        }
    }
}