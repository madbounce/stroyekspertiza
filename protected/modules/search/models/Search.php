<?php

/**
 * This is the model class for table "{{search}}".
 *
 * The followings are the available columns in table '{{search}}':
 * @property string $entity
 * @property string $entity_pk
 * @property string $title
 * @property string $sid
 * @property string $short_description
 * @property string $description
 * @property string $additional
 * @property string $address
 * @property string $age
 * @property string $sex
 * @property string $activity
 * @property string $start_time
 * @property string $end_time
 * @property integer $repeating_day_1
 * @property integer $repeating_day_2
 * @property string $category_1
 * @property string $category_2
 * @property string $category_3
 * @property string $latitude_1
 * @property string $longitude_1
 * @property string $latitude_2
 * @property string $longitude_2
 * @property string $latitude_3
 * @property string $longitude_3
 * @property integer $birthday
 * @property string $metro
 * @property string $district
 */
class Search extends SearchActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    protected function instantiate($attributes){
        if (isset($attributes['entity']))
            $class = $attributes['entity'];
        else
            $class = get_class($this);

        $model = new $class(null);

        if (isset($attributes['entity_pk'])) {
            $model = $class::model()->findByPk($attributes['entity_pk']);
        }

        return $model;
    }

    public function tableName()
    {
        return '{{tbl_search_sph}}';
    }

    public function primaryKey()
    {
        //return array('language', 'entity', 'entity_pk');
        return array('entity', 'entity_pk');
    }

    public function rules()
    {
        return array(
            array('entity, entity_pk', 'required'),
            array('entity', 'length', 'max' => 128),
            array('entity_pk', 'length', 'max' => 10),
            array('title', 'length', 'max' => 1024),
            array('description, sid', 'safe'),
            /*
            array('language, entity, entity_pk', 'required'),
            array('repeating_day_1, repeating_day_2, birthday', 'numerical', 'integerOnly' => true),
            array('entity, age, sex, activity', 'length', 'max' => 128),
            array('entity_pk, category_1, category_2, category_3', 'length', 'max' => 10),
            array('title, short_description', 'length', 'max' => 1024),
            array(' metro, district', 'length', 'max' => 512),
            array('latitude_1, longitude_1, latitude_2, longitude_2, latitude_3, longitude_3', 'length', 'max' => 18),
            array('description, additional, address, start_time, end_time, sid', 'safe'),
            */
        );
    }

    public function relations()
    {
        return array();
    }

    public function attributeLabels()
    {
        return array(
            'entity' => 'Entity',
            'entity_pk' => 'Entity Pk',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'additional' => 'Additional',
            'address' => 'Address',
            'age' => 'Age',
            'sex' => 'Sex',
            'activity' => 'Activity',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'repeating_day_1' => 'Repeating Day 1',
            'repeating_day_2' => 'Repeating Day 2',
            'category_1' => 'Category',
            'category_2' => 'Category',
            'category_3' => 'Category',
            'latitude_1' => 'Latitude',
            'longitude_1' => 'Longitude',
            'latitude_2' => 'Latitude',
            'longitude_2' => 'Longitude',
            'latitude_3' => 'Latitude',
            'longitude_3' => 'Longitude',
            'birthday' => 'Birthday',
            'metro' => 'Metro',
            'district' => 'District',
        );
    }

    protected function beforeValidate()
    {
        //$this->convertToMySQLDate();
        //$this->convertItemsToString();

        return parent::beforeValidate();
    }

    protected function convertToMySQLDate()
    {
        if (!empty($this->start_time)) {
            $parsed = CDateTimeParser::parse($this->start_time, 'd.MM.yyyy HH:mm', array('hour' => 0, 'minute' => 0));
            if (!$parsed)
                $parsed = CDateTimeParser::parse($this->start_time, 'd.MM.yyyy', array('hour' => 0, 'minute' => 0));
            $this->start_time = Yii::app()->dateFormatter->format('yyyy-MM-d HH:mm', $parsed);
        } else
            $this->start_time = null;

        if (!empty($this->end_time)) {
            $parsed = CDateTimeParser::parse($this->end_time, 'd.MM.yyyy HH:mm', array('hour' => 0, 'minute' => 0));
            if (!$parsed)
                $parsed = CDateTimeParser::parse($this->end_time, 'd.MM.yyyy', array('hour' => 0, 'minute' => 0));
            $this->end_time = Yii::app()->dateFormatter->format('yyyy-MM-d HH:mm', $parsed);
        } else
            $this->end_time = null;
    }

    protected function convertItemsToString()
    {
        if (!empty($this->sex))
            $this->sex = implode(',', $this->sex);

        if (!empty($this->age))
            $this->age = implode(',', $this->age);

        if (!empty($this->activity))
            $this->activity = implode(',', $this->activity);
    }

    public static function updateIndex($entity, $entity_pk)
    { 
        self::deleteFromIndex($entity, $entity_pk);
        //foreach (TranslateModule::getLanguages() as $lang) {
            //if ($lang == Yii::app()->params['defaultLanguage'])
                $model = $entity::model()->resetScope()->findByPk($entity_pk);
            //else
            //    $model = $entity::model()->resetScope()->localized($lang)->findByPk($entity_pk);

            if (!empty($model)) {
                if ($entity == 'Page') {
                    self::updatePageIndex($model);
                } else if ($entity == 'Catalog') {
                    self::updateCatalogIndex($model);
                } else if ($entity == 'Articles') {
                    self::updateArticlesIndex($model);
                }
            }
        //}
    }

    public static function deleteFromIndex($entity, $entity_pk)
    {
        //foreach (TranslateModule::getLanguages() as $lang) {
//            Search::model()->deleteByPk(array('language' => $lang, 'entity' => $entity, 'entity_pk' => $entity_pk));
		//	SearchSph::model()->deleteByPk(array('language' => $lang, 'entity' => $entity, 'entity_pk' => $entity_pk));
        //}
        SearchSph::model()->deleteByPk(array('language' => "''", 'entity' => $entity, 'entity_pk' => $entity_pk));
    }

    protected static function updatePageIndex($page)
    {
        $modelSph = new SearchSph;

        $modelSph->entity = 'Page';
        $modelSph->entity_pk = $page->id;
        $modelSph->title = $page->title;
        $modelSph->description = strip_tags($page->content);

        $modelSph->save();
    }

    protected static function updateCatalogIndex($product)
    {
        $modelSph = new SearchSph;

        $modelSph->entity = 'Catalog';
        $modelSph->entity_pk = $product->id;
        $modelSph->title = $product->name;
        $modelSph->description = strip_tags($product->description);

        $modelSph->composition = strip_tags($product->composition); //Состав
        $modelSph->feedingGuidelinesText = strip_tags($product->feedingGuidelinesText); //Руководство к кормлению
        $modelSph->additives = strip_tags($product->additives); //Добавки


        $modelSph->save();
    }

    protected static function updateArticlesIndex($article)
    {
        $modelSph = new SearchSph;

        $modelSph->entity = 'Articles';
        $modelSph->entity_pk = $article->id;
        $modelSph->title = $article->title;
        $modelSph->description = strip_tags($article->content);

        $modelSph->save();
    }

    protected static function updateEventIndex($event, $language)
    {

		$modelSph = new SearchSph;
		
		$modelSph->language = $language;
        $modelSph->entity = 'Event';
        $modelSph->entity_pk = $event->id;
		$modelSph->title = $event->title;
        $modelSph->short_description = strip_tags($event->short_description);
        $modelSph->description = strip_tags($event->description);
//		$modelSph->address = implode(';', $address);

        $modelSph->age = $event->age;
        $modelSph->sex = $event->sex;
        $modelSph->activity = $event->activity;

        $modelSph->start_time = $event->start_time;
        $modelSph->end_time = $event->end_time;

        $modelSph->repeating_day_1 = $event->repeating_day_1;
        $modelSph->repeating_day_2 = $event->repeating_day_2;
		
//		$model = new Search;
//        $model->language = $language;
//        $model->entity = 'Event';
//        $model->entity_pk = $event->id;

//        $model->title = MorphologyHelper::addBaseForm($event->title);
//        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($event->short_description));
//        $model->description = MorphologyHelper::addBaseForm(strip_tags($event->description));

        $additional = array($event->tags);
        $address = array();
        foreach ($event->places as $place) {
            $address[] = $place->fullAddress;
            $additional[] = $place->title;
        }
		$modelSph->additional = implode(';', $additional);

//        $model->additional = implode(';', $additional);
		
        
		$modelSph->address = implode(';', $address);
//		$model->modelSph = implode(';', $address);

//        $model->age = $event->age;
//        $model->sex = $event->sex;
//        $model->activity = $event->activity;

//        $model->start_time = $event->start_time;
//        $model->end_time = $event->end_time;

//        $model->repeating_day_1 = $event->repeating_day_1;
//        $model->repeating_day_2 = $event->repeating_day_2;

        foreach ($event->categories as $index => $category) {
            if ($index > 2) break;
            $i = $index + 1;
//            $model->setAttribute("category_$i", $category->id);
			$modelSph->setAttribute("category_$i", $category->id);
        }

        foreach ($event->places as $index => $place) {
            if ($index > 2) break;
            $i = $index + 1;
//            $model->setAttribute("latitude_$i", $place->latitude);
//            $model->setAttribute("longitude_$i", $place->longitude);
			$modelSph->setAttribute("latitude_$i", $place->latitude);
            $modelSph->setAttribute("longitude_$i", $place->longitude);
        }

//        $model->birthday = $event->birthday;
		$modelSph->birthday = $event->birthday;

//        $model->save();
		$modelSph->save();
    }

    protected static function updatePlaceIndex($place, $language)
    {
        $modelSph = new SearchSph;
		$modelSph->language = $language;
        $modelSph->entity = 'Place';
        $modelSph->entity_pk = $place->id;

        $modelSph->title = $place->title;
        $modelSph->short_description = strip_tags($place->short_description);
        $modelSph->description = strip_tags($place->description);
		
		
/*		
		$model = new Search;
        $model->language = $language;
        $model->entity = 'Place';
        $model->entity_pk = $place->id;

        $model->title = MorphologyHelper::addBaseForm($place->title);
        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($place->short_description));
        $model->description = MorphologyHelper::addBaseForm(strip_tags($place->description));
*/
        $additional = array($place->metro, $place->district);
        foreach ($place->events as $event) {
            $additional[] = $event->title;
        }
//        $model->additional = implode(';', $additional);
		$modelSph->additional = implode(';', $additional);

//        $model->address = $place->fullAddress;
		$modelSph->address = $place->fullAddress;

//        $model->repeating_day_1 = $place->repeating_day_1;
//        $model->repeating_day_2 = $place->repeating_day_2;
		$modelSph->repeating_day_1 = $place->repeating_day_1;
        $modelSph->repeating_day_2 = $place->repeating_day_2;

        foreach ($place->categories as $index => $category) {
            if ($index > 2) break;
            $i = $index + 1;
//            $model->setAttribute("category_$i", $category->id);
			$modelSph->setAttribute("category_$i", $category->id);
        }
/*
        $model->latitude_1 = $place->latitude;
        $model->longitude_1 = $place->longitude;

        $model->metro = $place->metro;
        $model->district = $place->district;
*/		
		$modelSph->latitude_1 = $place->latitude;
        $modelSph->longitude_1 = $place->longitude;

        $modelSph->metro = $place->metro;
        $modelSph->district = $place->district;

//        $model->save();
		$modelSph->save();
    }

    protected static function updateArticleIndex($article, $language)
    {
        $modelSph = new SearchSph;
        $modelSph->language = $language;
        $modelSph->entity = 'Article';
        $modelSph->entity_pk = $article->id;

        $modelSph->title = $article->title;
        $modelSph->short_description = strip_tags($article->short_description);
        $modelSph->description = strip_tags($article->description);

		$modelSph->additional = !empty($article->author) ? ($article->author->fullName) : '';
		

        $modelSph->save();
		
/*		
		$model = new Search;
        $model->language = $language;
        $model->entity = 'Article';
        $model->entity_pk = $article->id;

        $model->title = MorphologyHelper::addBaseForm($article->title);
        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($article->short_description));
        $model->description = MorphologyHelper::addBaseForm(strip_tags($article->description));

        $model->additional = $article->author;

        $model->save();
*/
    }
	
    protected static function updateInterviewIndex($interview, $language)
    {
        $modelSph = new SearchSph;
        $modelSph->language = $language;
        $modelSph->entity = 'Interview';
        $modelSph->entity_pk = $interview->id;

        $modelSph->title = $interview->title;
        $modelSph->short_description = strip_tags($interview->short_description);
        $modelSph->description = strip_tags($interview->description);

		$modelSph->additional = !empty($interview->author) ? ($interview->author->fullName) : '';
		

        $modelSph->save();
    }
	

    protected static function updateRecipeIndex($recipe, $language)
    {
        $modelSph = new SearchSph;
		
		$modelSph->language = $language;
        $modelSph->entity = 'Recipe';
        $modelSph->entity_pk = $recipe->id;

        $modelSph->title = $recipe->title;
        $modelSph->short_description = strip_tags($recipe->short_description);

        $modelSph->additional = @implode(';', $recipe->requirements);

        $modelSph->age = $recipe->age;
        $modelSph->sex = $recipe->sex;

        $modelSph->save();
		

/*		
		$model = new Search;
        $model->language = $language;
        $model->entity = 'Recipe';
        $model->entity_pk = $recipe->id;

        $model->title = MorphologyHelper::addBaseForm($recipe->title);
        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($recipe->short_description));

        $model->additional = @implode(';', $recipe->requirements);

        $model->age = $recipe->age;
        $model->sex = $recipe->sex;

        $model->save();
		
		$model = new Search;
        $model->language = $language;
        $model->entity = 'Recipe';
        $model->entity_pk = $recipe->id;

        $model->title = MorphologyHelper::addBaseForm($recipe->title);
        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($recipe->short_description));

        $model->additional = @implode(';', $recipe->requirements);

        $model->age = $recipe->age;
        $model->sex = $recipe->sex;

        $model->save();
		
		$model = new Search;
        $model->language = $language;
        $model->entity = 'Recipe';
        $model->entity_pk = $recipe->id;

        $model->title = MorphologyHelper::addBaseForm($recipe->title);
        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($recipe->short_description));

        $model->additional = @implode(';', $recipe->requirements);

        $model->age = $recipe->age;
        $model->sex = $recipe->sex;

        $model->save();
*/
    }

    protected static function updateMapIndex($map, $language)
    {
        $model = new Search;
        $model->language = $language;
        $model->entity = 'Map';
        $model->entity_pk = $map->id;

        $model->title = MorphologyHelper::addBaseForm($map->title);
        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($map->short_description));

        $additional = array();
        $address = array();
        foreach ($map->places as $place) {
            $address[] = $place->fullAddress;
            $additional[] = $place->title;
        }
        $model->additional = implode(';', $additional);
        $model->address = implode(';', $address);

        foreach ($map->categories as $index => $category) {
            if ($index > 2) break;
            $i = $index + 1;
            $model->setAttribute("category_$i", $category->id);
        }

        foreach ($map->places as $index => $place) {
            if ($index > 2) break;
            $i = $index + 1;
            $model->setAttribute("latitude_$i", $place->latitude);
            $model->setAttribute("longitude_$i", $place->longitude);
        }

        $model->save();
    }
}