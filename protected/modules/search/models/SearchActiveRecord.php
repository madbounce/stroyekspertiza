<?php
/**
 * Created by JetBrains PhpStorm.
 * User: breethe.ua
 * Date: 30.10.12
 * Time: 17:44
 * To change this template use File | Settings | File Templates.
 */
class SearchActiveRecord extends CActiveRecord
{
    /**
     * Без вызова afterFind
     */
    public function populateRecord($attributes,$callAfterFind=true)
    {
        if($attributes!==false)
        {
            $record=$this->instantiate($attributes);
			
			if($record == null) return null;
			
			/*
			echo '<pre>';
			print_r($record);
			echo '</pre>';
			exit;
			*/
			
			
            $record->setScenario('update');
            $record->init();
            $md=$record->getMetaData();
            foreach($attributes as $name=>$value)
            {
                if(property_exists($record,$name))
                    $record->$name=$value;
                else if(isset($md->columns[$name]))
                    $record->_attributes[$name]=$value;
            }
            $record->_pk=$record->getPrimaryKey();
            $record->attachBehaviors($record->behaviors());
            return $record;
        }
        else
            return null;
    }
}
