<?php

class SearchForm extends CFormModel
{
    public $entity;
    public $entity_type;

    public $keyword;

    public $retArr;

    public static $searchResult;

    public $metadata = array('class' => 'SearchForm');

    public function rules()
    {
        return array(
            array('keyword', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'keyword' => Yii::t('search', 'Поиск по ключевым словам'),
        );
    }

    protected function getMatchString($keyword)
    {

//        $baseKeyword = MorphologyHelper::ToBaseForm($keyword);

        $search = ">>\"$keyword\" >\"$baseKeyword\"";

        $tokens = explode(' ', $keyword);
        $tokens2 = array();
        foreach ($tokens as $id => $token) {
            $tokens[$id] = "+$token";
            $tokens2[$id] = "$token";
        }

        $baseTokens = explode(' ', $baseKeyword);
        foreach ($baseTokens as $id => $token) {
            $baseTokens[$id] = "$token";
        }

        $search .= ' <(' . implode(' ', $tokens) . ') <<(' . implode(' ', $tokens2) . ') <<(' . implode(' ', $baseTokens) . ')';

        $search = '"'.str_replace('.','',$keyword).'"';
//die($search);
        return $search;
    }

    public function getResult() {
        $search = Yii::App()->search;
        $search->setSelect('search_eukanuba');

        $search->setArrayResult(false);
        $search->limit(0,10000);


        $keywords = '';
        $keyword = $this->keyword;
        $search->setFieldWeights(array('title'=>100,'description'=>1));

        $keywords = '"'.$keyword.'"~6';
        $search->setMatchMode(SPH_MATCH_EXTENDED);

        $ret = $search->query( $keywords, '*');
        $retArr = array();
        foreach($ret['words'] as $key=>$val) {
            $retArr[] = $key;
        }
        $this->retArr = $retArr;
        return $retArr;
    }

    public function getSearched($data) {
        /*		foreach($this->getResult() as $val) {
              $data = str_replace($val, '<b>'.$val.'</b>',$data);
          }*/
//		$data = str_replace($this->keyword, '<b>'.$this->keyword.'</b>',$data);
        $data = preg_replace("/".$this->keyword."/iu", '<b>$0</b>',$data);
        return $data;
    }

    public function getSearchedCatalog($data, $selectionEnv = true) {
        $allForms = MorphologyHelper::findMAllForms($this->keyword);

        foreach($data as $dat){
            $count = 0;
            $dat = strip_tags($dat);
            $ret = '';
            if($allForms){
                $countForms = 0;
                foreach($allForms as $form){
                    $txt = ($ret) ? $ret : $dat;
                    $ret = preg_replace("/".$form."/iu", '<b>$0</b>', $txt, -1, $countForms);
                    if($countForms)
                        $count += $countForms;
                }
            } else
                $ret = preg_replace("/".$this->keyword."/iu", '<b>$0</b>',$dat, -1, $count);

            if($count > 0)
                break;
        }
        if($selectionEnv)
            $ret = $this->selectionEnv($ret);
        return $ret;
    }

    public function selectionEnv($text){
        $ret = '';
        if($text){
            $pos = mb_strpos($text, "<b>", 0, 'utf8');
            //Если совпадение найдено
            if(($pos !== false)){
                if($pos < 400){
                    $ret = mb_substr($text,0,400,'utf8');
                    $ret .= '...';
                } else {
                    $temp = mb_substr($text,$pos - 200, 400,'utf8');
                    $ret = '...' . trim($temp) . '...';
                }
            }

        }
        $ret = str_replace(array('<...','</...','</b...'),'</b>...',$ret);
        return $ret;
    }

    public static function getKeywordMatchCriteria($keyword, $minMatchRank = 5, $entity = '')
    {

        $search = Yii::App()->search;
        $search->setSelect('*');

        $search->setArrayResult(false);
        $search->limit(0,10000);
//		$search->groupby = array('entity','entity_pk'); 
//		if(isset($entity) && $entity)
//			$search->setFilter('entity',array($entity));

        $search->setFieldWeights(array('title'=>100,'description'=>1));

//		if(count(explode(' ',$keyword)) > 1) {

        $keywords = '"'.$keyword.'"~6';
        $search->setMatchMode(SPH_MATCH_EXTENDED);
        /*		}
                else {

                    $keywords = $keyword;
                    $search->setMatchMode(SPH_MATCH_ALL);
                }
        */
        $resArray = $search->query( $keywords, '*');
        $usl="";
//		die(print_r($resArray));
        $criteria = new CDbCriteria;
        if(count($resArray['matches'])) {
            $ids = implode(',', array_keys($resArray['matches']));
//			die(print_r($ids));
//			$criteria->order = "FIELD (`sid`, $ids)";
            /*
               foreach($resArray['matches'] as $key=>$match) {
                   $find = ViewSearch::model()->findAll(array('condition'=>'t.sid = '.$key));
                   if(isset($find[0]))
                       $usl .= " OR (t.entity='{$find[0]->entity}' AND t.entity_pk = '{$find[0]->entity_pk}') ";
               }
               if(strlen($usl) > 10)
                   $usl = '('.substr($usl,4).')';
               else $usl = '( 1 = 2)';
               */
//            die(print_r($resArray));
            foreach($resArray['matches'] as $key=>$match) {
                $criteria->addCondition('sid = ' . $key, "OR");
            }
            /*
            if ( is_array($resArray['matches']) ) {
                $criteria->addInCondition('sid', $resArray['matches']);
            }
            else {
                $criteria->addCondition('sid = ' . $resArray['matches']);
            }
            */
//            die(print_r($criteria));
        }
        else {
            $usl = '( 1 = 2)';
            $criteria->condition = '( 1 = 2)';
        }
//		die(print_r($criteria));
//		$criteria->condition = $usl;

        return $criteria;
    }

    public function adminSearch()
    {
        $criteria = new CDbCriteria;
        $criteria->select = array('entity', 'entity_pk');
        $criteria->distinct = true;


        if (!empty($this->keyword)) {
            $keywordCriteria = self::getKeywordMatchCriteria($this->keyword,5,(!empty($this->entity))?$this->entity:'');
            if (Search::model()->count($keywordCriteria) == 0)
                $keywordCriteria = self::getKeywordMatchCriteria($this->keyword, 0);

            $criteria->mergeWith($keywordCriteria);
//			die(print_r($criteria));
        }

        return new CActiveDataProvider('Search', array('criteria' => $criteria, 'pagination' => array('pageSize' => 20)));
    }

    public function generalSearch()
    {
        $criteria = new CDbCriteria;
        $criteria->select = array('entity', 'entity_pk', 'sid');

        if (!empty($this->keyword)) {
            $keywordCriteria = self::getKeywordMatchCriteria($this->keyword);
            //if (Search::model()->count($keywordCriteria) == 0)
            //    $keywordCriteria = self::getKeywordMatchCriteria($this->keyword, 0);

            $criteria->mergeWith($keywordCriteria);
        }

//		die(print_r($criteria));

        return new CActiveDataProvider('SearchSph', array('criteria' => $criteria));
    }

    const SEARCH_AREA = 2;

    private function getMinLatitude()
    {
        return $this->latitude - 1.0 / 111 * self::SEARCH_AREA / 2;
    }

    private function getMaxLatitude()
    {
        return $this->latitude + 1.0 / 111 * self::SEARCH_AREA / 2;
    }

    private function getMinLongitude()
    {
        return $this->longitude - 1.0 / 111 * self::SEARCH_AREA / 2 * cos($this->longitude);
    }

    private function getMaxLongitude()
    {
        return $this->longitude + 1.0 / 111 * self::SEARCH_AREA / 2 * cos($this->longitude);
    }
}
