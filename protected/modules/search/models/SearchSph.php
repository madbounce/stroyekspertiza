<?php

/**
 * This is the model class for table "{{search}}".
 *
 * The followings are the available columns in table '{{search}}':
 * @property string $entity
 * @property string $entity_pk
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property string $additional
 * @property string $address
 * @property string $age
 * @property string $sex
 * @property string $activity
 * @property string $start_time
 * @property string $end_time
 * @property integer $repeating_day_1
 * @property integer $repeating_day_2
 * @property string $category_1
 * @property string $category_2
 * @property string $category_3
 * @property string $latitude_1
 * @property string $longitude_1
 * @property string $latitude_2
 * @property string $longitude_2
 * @property string $latitude_3
 * @property string $longitude_3
 * @property integer $birthday
 * @property string $metro
 * @property string $district
 */
class SearchSph extends SearchActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    protected function instantiate($attributes){
        if (isset($attributes['entity']))
            $class = $attributes['entity'];
        else
            $class = get_class($this);

        $model = new $class(null);

        if (isset($attributes['entity_pk'])) {
            $model = $class::model()->findByPk($attributes['entity_pk']);
        }

        return $model;
    }

    public function tableName()
    {
        return '{{tbl_search_sph}}';
    }

    public function primaryKey()
    {
        //return array('language', 'entity', 'entity_pk');
        return array('entity', 'entity_pk');
    }

    public function rules()
    {
        return array(
            array('entity, entity_pk', 'required'),
            array('entity', 'length', 'max' => 128),
            array('entity_pk', 'length', 'max' => 10),
            array('title', 'length', 'max' => 1024),
            array('description', 'safe'),
            /*
            array('language, entity, entity_pk', 'required'),
            array('repeating_day_1, repeating_day_2, birthday', 'numerical', 'integerOnly' => true),
            array('entity, age, sex, activity', 'length', 'max' => 128),
            array('entity_pk, category_1, category_2, category_3', 'length', 'max' => 10),
            array('title, short_description', 'length', 'max' => 1024),
            array(' metro, district', 'length', 'max' => 512),
            array('latitude_1, longitude_1, latitude_2, longitude_2, latitude_3, longitude_3', 'length', 'max' => 18),
            array('description, additional, address, start_time, end_time', 'safe'),
            */
        );
    }

    public function relations()
    {
        return array();
    }

    public function attributeLabels()
    {
        return array(
            'entity' => 'Entity',
            'entity_pk' => 'Entity Pk',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'additional' => 'Additional',
            'address' => 'Address',
            'age' => 'Age',
            'sex' => 'Sex',
            'activity' => 'Activity',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'repeating_day_1' => 'Repeating Day 1',
            'repeating_day_2' => 'Repeating Day 2',
            'category_1' => 'Category',
            'category_2' => 'Category',
            'category_3' => 'Category',
            'latitude_1' => 'Latitude',
            'longitude_1' => 'Longitude',
            'latitude_2' => 'Latitude',
            'longitude_2' => 'Longitude',
            'latitude_3' => 'Latitude',
            'longitude_3' => 'Longitude',
            'birthday' => 'Birthday',
            'metro' => 'Metro',
            'district' => 'District',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('sid', $this->sid, true);
        $criteria->compare('entity', $this->entity, true);
        $criteria->compare('entity_pk', $this->entity_pk, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    protected function beforeValidate()
    {
        //$this->convertToMySQLDate();
        //$this->convertItemsToString();

        return parent::beforeValidate();
    }

    protected function convertToMySQLDate()
    {
        if (!empty($this->start_time)) {
            $parsed = CDateTimeParser::parse($this->start_time, 'd.MM.yyyy HH:mm', array('hour' => 0, 'minute' => 0));
            if (!$parsed)
                $parsed = CDateTimeParser::parse($this->start_time, 'd.MM.yyyy', array('hour' => 0, 'minute' => 0));
            $this->start_time = Yii::app()->dateFormatter->format('yyyy-MM-d HH:mm', $parsed);
        } else
            $this->start_time = null;

        if (!empty($this->end_time)) {
            $parsed = CDateTimeParser::parse($this->end_time, 'd.MM.yyyy HH:mm', array('hour' => 0, 'minute' => 0));
            if (!$parsed)
                $parsed = CDateTimeParser::parse($this->end_time, 'd.MM.yyyy', array('hour' => 0, 'minute' => 0));
            $this->end_time = Yii::app()->dateFormatter->format('yyyy-MM-d HH:mm', $parsed);
        } else
            $this->end_time = null;
    }

    protected function convertItemsToString()
    {
        if (!empty($this->sex))
            $this->sex = implode(',', $this->sex);

        if (!empty($this->age))
            $this->age = implode(',', $this->age);

        if (!empty($this->activity))
            $this->activity = implode(',', $this->activity);
    }

    public static function updateIndex($entity, $entity_pk)
    {
        self::deleteFromIndex($entity, $entity_pk);
        //foreach (TranslateModule::getLanguages() as $lang) {
            //if ($lang == Yii::app()->params['defaultLanguage'])
                $model = $entity::model()->resetScope()->findByPk($entity_pk);
            //else
            //    $model = $entity::model()->resetScope()->localized($lang)->findByPk($entity_pk);

            if (!empty($model)) {
                if ($entity == 'Page') {
                    self::updatePageIndex($model);
                } else if ($entity == 'Catalog') {
                    self::updatePlaceIndex($model);
                } else if ($entity == 'Articles') {
                    self::updateArticlesIndex($model);
                }
                /*
                if ($entity == 'Event') {
                    self::updateEventIndex($model, $lang);
                } else if ($entity == 'Place') {
                    self::updatePlaceIndex($model, $lang);
                } else if ($entity == 'Article') {
                    self::updateArticleIndex($model, $lang);
                } else if ($entity == 'Recipe') {
                    self::updateRecipeIndex($model, $lang);
                } else if ($entity == 'Map') {
                    self::updateMapIndex($model, $lang);
                }
                */
            }
        //}
    }

    public static function deleteFromIndex($entity, $entity_pk)
    {
        //foreach (TranslateModule::getLanguages() as $lang) {
        //    Search::model()->deleteByPk(array('language' => $lang, 'entity' => $entity, 'entity_pk' => $entity_pk));
        //}
        Search::model()->deleteByPk(array('entity' => $entity, 'entity_pk' => $entity_pk));
    }

    protected static function updatePageIndex($page)
    {
        $model = new Search;
        $model->entity = 'Page';
        $model->entity_pk = $page->id;

        $model->title = MorphologyHelper::addBaseForm($page->title);
        $model->description = MorphologyHelper::addBaseForm(strip_tags($page->content));

        $model->save();
    }

    protected static function updateArticlesIndex($article)
    {
        $model = new Search;
        $model->entity = 'Articles';
        $model->entity_pk = $article->id;

        $model->title = MorphologyHelper::addBaseForm($article->title);
        $model->description = MorphologyHelper::addBaseForm(strip_tags($article->content));

        $model->save();
    }

    protected static function updateCatalogIndex($product)
    {
        $model = new Search;
        $model->entity = 'Catalog';
        $model->entity_pk = $product->id;

        $model->title = MorphologyHelper::addBaseForm($product->name);
        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($product->description));

        $model->save();
    }

    protected static function updateEventIndex($event, $language)
    {
        $model = new Search;
        $model->language = $language;
        $model->entity = 'Event';
        $model->entity_pk = $event->id;

        $model->title = MorphologyHelper::addBaseForm($event->title);
        $model->description = MorphologyHelper::addBaseForm(strip_tags($event->description));

        /*
        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($event->short_description));
        $additional = array($event->tags);
        $address = array();
        foreach ($event->places as $place) {
            $address[] = $place->fullAddress;
            $additional[] = $place->title;
        }
        $model->additional = implode(';', $additional);
        $model->address = implode(';', $address);

        $model->age = $event->age;
        $model->sex = $event->sex;
        $model->activity = $event->activity;

        $model->start_time = $event->start_time;
        $model->end_time = $event->end_time;

        $model->repeating_day_1 = $event->repeating_day_1;
        $model->repeating_day_2 = $event->repeating_day_2;

        foreach ($event->categories as $index => $category) {
            if ($index > 2) break;
            $i = $index + 1;
            $model->setAttribute("category_$i", $category->id);
        }

        foreach ($event->places as $index => $place) {
            if ($index > 2) break;
            $i = $index + 1;
            $model->setAttribute("latitude_$i", $place->latitude);
            $model->setAttribute("longitude_$i", $place->longitude);
        }

        $model->birthday = $event->birthday;
        */

        $model->save();
    }

    protected static function updatePlaceIndex($place, $language)
    {
        $model = new Search;
        $model->language = $language;
        $model->entity = 'Place';
        $model->entity_pk = $place->id;

        $model->title = MorphologyHelper::addBaseForm($place->title);
        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($place->short_description));
        $model->description = MorphologyHelper::addBaseForm(strip_tags($place->description));

        $additional = array($place->metro, $place->district);
        foreach ($place->events as $event) {
            $additional[] = $event->title;
        }
        $model->additional = implode(';', $additional);

        $model->address = $place->fullAddress;

        $model->repeating_day_1 = $place->repeating_day_1;
        $model->repeating_day_2 = $place->repeating_day_2;

        foreach ($place->categories as $index => $category) {
            if ($index > 2) break;
            $i = $index + 1;
            $model->setAttribute("category_$i", $category->id);
        }

        $model->latitude_1 = $place->latitude;
        $model->longitude_1 = $place->longitude;

        $model->metro = $place->metro;
        $model->district = $place->district;

        $model->save();
    }

    protected static function updateArticleIndex($article, $language)
    {
        $model = new Search;
        $model->language = $language;
        $model->entity = 'Article';
        $model->entity_pk = $article->id;

        $model->title = MorphologyHelper::addBaseForm($article->title);
        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($article->short_description));
        $model->description = MorphologyHelper::addBaseForm(strip_tags($article->description));

        $model->additional = $article->author;

        $model->save();
    }

    protected static function updateRecipeIndex($recipe, $language)
    {
        $model = new Search;
        $model->language = $language;
        $model->entity = 'Recipe';
        $model->entity_pk = $recipe->id;

        $model->title = MorphologyHelper::addBaseForm($recipe->title);
        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($recipe->short_description));

        $model->additional = @implode(';', $recipe->requirements);

        $model->age = $recipe->age;
        $model->sex = $recipe->sex;

        $model->save();
    }

    protected static function updateMapIndex($map, $language)
    {
        $model = new Search;
        $model->language = $language;
        $model->entity = 'Map';
        $model->entity_pk = $map->id;

        $model->title = MorphologyHelper::addBaseForm($map->title);
        $model->short_description = MorphologyHelper::addBaseForm(strip_tags($map->short_description));

        $additional = array();
        $address = array();
        foreach ($map->places as $place) {
            $address[] = $place->fullAddress;
            $additional[] = $place->title;
        }
        $model->additional = implode(';', $additional);
        $model->address = implode(';', $address);

        foreach ($map->categories as $index => $category) {
            if ($index > 2) break;
            $i = $index + 1;
            $model->setAttribute("category_$i", $category->id);
        }

        foreach ($map->places as $index => $place) {
            if ($index > 2) break;
            $i = $index + 1;
            $model->setAttribute("latitude_$i", $place->latitude);
            $model->setAttribute("longitude_$i", $place->longitude);
        }

        $model->save();
    }
}