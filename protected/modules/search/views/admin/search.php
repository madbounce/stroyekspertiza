<div class="main_body">
    <div class="main_body_wrap">
        <h1 class="h1-1"><?php echo Yii::t('app', 'Поиск'); ?></h1>

        <div class="locationswrap">

            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'search-grid',
                'dataProvider' => $model->adminSearch(),
                'itemsCssClass' => 'table',
                'template' => '{summary} {pager} {items} {pager}',
                'columns' => array(
                    array(
                        'name' => 'id',
                        'header' => '#'
                    ),
                    array(
                        'type' => 'raw',
                        'header' => Yii::t('event', 'Тип'),
                        'value' => function($data) {
                            $class = get_class($data);
                            switch ($class) {
                                case 'Event':
                                    if ($data->isEvent)
                                        return Yii::t('app', 'Событие');
                                    elseif ($data->isReview)
                                        return Yii::t('app', 'Обзор');
                                    break;
                                case 'Place':
                                    return Yii::t('app', 'Место');
                                    break;
                                case 'Article':
                                    return Yii::t('app', 'Статья');
                                    break;
                                case 'Recipe':
                                    return Yii::t('app', 'Мастер-класс');
                                    break;
                            }
                            return CHtml::link(CHtml::encode($data->title), array("/{$class}/admin/update", 'id' => $data->id));
                        }
                    ),
                    array(
                        'name' => 'title',
                        'type' => 'raw',
                        'header' => Yii::t('event', 'Название'),
                        'value' => function($data) {
                            $class = strtolower(get_class($data));
                            return CHtml::link(CHtml::encode($data->title), array("/{$class}/admin/update", 'id' => $data->id));
                        }
                    ),
                    array(
                        'name' => 'short_description',
                        'type' => 'raw',
                        'header' => Yii::t('event', 'Краткое описание'),
                    ),
                ),
            ));
            ?>
        </div>
    </div>
</div>