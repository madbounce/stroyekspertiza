<?php if (!empty($models)): ?>
<?php $data = array(); ?>
<?php foreach ($models as $model): ?>
    <?php
        $data[$model->id] = CHtml::tag('span', array('data-entity-id' => $model->id, 'data-entity' => get_class($model)), CHtml::encode($model->title));
    ?>
<?php endforeach; ?>

<div class="operations">
    <?php echo CHtml::link(Yii::t('app', 'Выбрать все'), '#', array('id' => 'select-entities-all')); ?>
    <?php echo CHtml::link(Yii::t('app', 'Добавить выбранное'), '#', array('id' => 'select-entities-add')); ?>
</div>

<?php echo CHtml::checkBoxList('ids', array(), $data); ?>
<?php else: ?>
    <?php echo Yii::t('place', 'Ничего не найдено.'); ?>
<?php endif; ?>

