<?php $this->pageTitle .= (!empty($this->pageTitle) ? ' — ' : '') . Yii::t('app', 'Поиск'); ?>

<style>

.article-detail #bodycontent_area .txt-content li {
padding-bottom:inherit;	
}

.article-detail #bodycontent_area .txt-content li {
font-family: inherit;
}
</style>

<div id="MainContent" class="article-detail" xmlns="http://www.w3.org/1999/html">
    <div id="white-container">
        <div class="copy-top-border"><img src="<?=Yii::app()->theme->baseUrl;?>/images/top-border-dog.png" alt="" width="956" height="8" title="" class="topborder"></div>
        <div id="bodycontent_area"><div class="clear-all">&nbsp;</div>

            <div id="breadcrumb"><a title="Home" href="/"><?=Yii::t('app','Главная');?></a> / <?=Yii::t('app','Поиск');?></div>
            <div class="_col left-callouts-nav">
                <p class="green_curve"><img src="<?=Yii::app()->theme->baseUrl;?>/images/img_cat_detail.jpg" alt="" title=""></p>

                <div class="left-callouts"> <img src="<?=Yii::app()->theme->baseUrl;?>/images/need_more_help.gif" alt="Need More Help?" title="Need More Help?">
                    <p>Не можете найти то,<br/> что ищете?</p>
                    <div>
                        <a title="<?=Yii::t('app','Свяжитесь с нами');?>" href="/site/contactus" id="contact_btn"><?=Yii::t('app','Связаться');?></a>
                    </div>
                </div>
            </div>

            <div class="_col txt-content">

                <h1 class="heading"><?=Yii::t('app','Результаты поиска');?></h1>
                </br></br>

                <form id="search2" method="get" action="/search" name="search2" class="search-article"  >

                    <div class="search-box">

                        <label for="searchText"><?=Misc::t('Поиск статьи');?></label>
                        <input name="searchText" id="searchText" class="text" type="text" value="<? echo (isset($_GET['searchText']) && $_GET['searchText']) ? $_GET['searchText'] : Yii::t('app','Введите слово для поиска');?>" onblur="if (this.value==''){this.value='Введите слово для поиска'}" onfocus="if (this.value=='Введите слово для поиска') this.value='';">
                        <input type="image" alt="<?=Misc::t('Поиск');?>" title="<?=Misc::t('Поиск');?>" src="<?=Yii::app()->theme->baseUrl;?>/images/btn_search_gary.gif" class="button"">
                    </div>
                    <!-- search box end-->
                </form>

                <div id="search_area">
                    <div class="result_list">
                        <?	$dataProvider = $model->generalSearch(); ?>
                        <?php
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider' => $model->generalSearch(),
                            'ajaxUpdate' => false,
                            'itemView' => '_view',
							'viewData' => array('model'=>$model),
                            'template' => '{pager}{items}{pager}',
                            'id' => 'search-list-view',
                            'pager' => array('class' => 'application.components.widgets.CCustomPager'),
                            'pagerCssClass' => 'pagination',
                        ));
                        ?>
                    </div>
                </div>

                <div class="clear-all">&nbsp;</div>
            </div>
            <div class="clear-all">&nbsp;</div>
        </div><!-- Copy Content end-->
    </div>

<?php
/*
$this->breadcrumbs = array(
    Yii::t('search', 'Поиск')
);
*/
?>

<!--
<div class="list_all_accii">
    <?php
    /*
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $model->generalSearch(),
        'ajaxUpdate' => false,
        'itemView' => '_view',
        'template' => '{items}{pager}',
        'id' => 'search-list-view',
        'pager' => array('class' => 'application.components.widgets.CCustomPager'),
        'pagerCssClass' => 'pagination',
    ));
    */
    ?>
</div>
-->