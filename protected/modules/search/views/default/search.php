<?php $this->pageTitle .= (!empty($this->pageTitle) ? ' — ' : '') . Yii::t('app', 'Поиск'); ?>

<?php
$this->breadcrumbs=array(
    Misc::t('Поиск')
);
?>

<div id="content" style="padding-bottom: 130px;">
        <!--
		<div class="pagination miniLeft">
        	<a href="/"><?=Yii::t('app','Главная');?></a><span>/</span><a href="/" class="thatPage"><?=Yii::t('app','Поиск')?></a>
        </div>
        -->
        <div class="middle searchRezult">
        	<div class="leftSide">
                    <div class="allNews">
                        <div><h2><?=Yii::t('app','Нужна помощь?');?></h2><?=Yii::t('app','Не можете найти то, что ищете?');?>
                        <a href="/site/contactus" class="LearnMore"><?=Yii::t('app','Связаться с нами');?> ></a>
                        </div>
                    </div>
                    </div>
            <div class="rightSide">
            	<h1><?=Yii::t('app','Результаты поиска');?></h1>
                <!--
                <div class="paginator">
                    <a href="/">1</a> | <a href="/">2</a> | <a href="/">3</a> | <a href="/">Ещё &rarr;</a>
                </div>
                -->
            	<form id="search2" method="get" action="/search" name="search2" class="chooseSearch">
                    <div class="searchDog">
                    	<!--
                        <input name="searchText" id="searchText" type="text" onblur="this.value=(this.value=='')?this.title:this.value;" onfocus="this.value=(this.value==this.title)?'':this.value;" value="Search for dog articles..." title="Search for dog articles..."/>
                        -->
                        <input name="searchText" id="searchText" type="text" value="<? echo (isset($_GET['searchText']) && $_GET['searchText']) ? $_GET['searchText'] : Yii::t('app','Введите слово для поиска');?>" onblur="if (this.value==''){this.value='Введите слово для поиска'}" onfocus="if (this.value=='Введите слово для поиска') this.value='';">
                        <input alt="<?=Misc::t('Поиск');?>" type="submit" value="<?=Misc::t('Поиск');?>"/>
                    </div>
                </form>
                <div class="rezultOfSearch">
                    <?	$dataProvider = $model->generalSearch(); ?>
                    <?php
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider' => $model->generalSearch(),
                        'ajaxUpdate' => false,
                        'itemView' => '_view',
                        'viewData' => array('model'=>$model),
                        'template' => '{pager}{items}{pager}',
                        'id' => 'search-list-view',
                        'pager' => array('class' => 'application.components.widgets.CCustomPager'),
          //              'pagerCssClass' => 'pagination',
                        'pagerCssClass' => 'paginator',
                    ));
                    ?>
                    <!--
                	<p>
                    	<a href="/">Несколько кошек в одном доме, или Почему две или три кошки лучше, чем одна</a>
                        ...о проблем, или хотя бы пытаются это делать. Проблема в том, что они часто в этом не преуспевают. Есть решение от Iams – особый рацион, который подходит всем взрослым кошкам, под названием IamsMulti-Cat. Если следовать советам специалистов по питанию и поведению кошек, нужно расставить по дому как минимум столько же мисок с кормом, сколько в доме кошек, и затем дать кошкам возможность о...
                    </p>
                    <p>
                    	<a href="/">Несколько кошек в одном доме, или Почему две или три кошки лучше, чем одна</a>
                        ...о проблем, или хотя бы пытаются это делать. Проблема в том, что они часто в этом не преуспевают. Есть решение от Iams – особый рацион, который подходит всем взрослым кошкам, под названием IamsMulti-Cat. Если следовать советам специалистов по питанию и поведению кошек, нужно расставить по дому как минимум столько же мисок с кормом, сколько в доме кошек, и затем дать кошкам возможность о...
                    </p>
                    -->
                </div>
            </div>
        </div>
        
	</div><!-- #content-->
