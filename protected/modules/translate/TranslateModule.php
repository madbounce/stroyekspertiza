<?php

class TranslateModule extends CWebModule
{
    public $filePathAlias = 'webroot.images.translate';

    public function init()
    {
        $this->setImport(array(
            'translate.models.*',
            'translate.components.*',
        ));

        $this->layoutPath = Yii::getPathOfAlias('translate.views.layouts');
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            return true;
        } else
            return false;
    }

    public static function getUrlOfAlias($alias, $includeBase = true)
    {
        if ($path = Yii::getPathOfAlias($alias))
        {
            $webrootPath = Yii::getPathOfAlias('webroot');
            if(strpos($path, $webrootPath) !== FALSE) {
                $path = substr($path, strlen($webrootPath));
            }
            return str_replace('\\', '/', $includeBase ? Yii::app()->baseUrl . $path : $path);
        }

        return false;
    }

    public static function getFilePath()
    {
        return Yii::getPathOfAlias(Yii::app()->getModule('translate')->filePathAlias);
    }

    public static function getFileUrl($includeBase = true)
    {
        return self::getUrlOfAlias(Yii::app()->getModule('translate')->filePathAlias, $includeBase);
    }

    public static function getLanguages()
    {
        Yii::import('translate.models.*');
        $languages = TranslateLanguage::getLanguages();
        $result = array();
        foreach ($languages as $language) {
            $result[] = $language->language;
        }

        return $result;
    }

    public static function getLanguageModels()
    {
        Yii::import('translate.models.*');
        return TranslateLanguage::getLanguages();
    }

    public static function missingTranslation($event)
    {
        Yii::import('translate.models.*');
        TranslateSourceMessage::create($event->category, $event->message);

        return $event;
    }

    public static function getLocalizedFile($key, $file, $language = null)
    {
        Yii::import('translate.models.*');

        if (empty($language))
            $language = Yii::app()->language;

        $model = TranslateSourceFile::get($key);
        if (!empty($model) && !empty($model->{$language}->file))
            return Yii::app()->baseUrl . $model->{$language}->file;
        else {
            TranslateSourceFile::create($key, $file);
            return $file;
        }
    }
}
