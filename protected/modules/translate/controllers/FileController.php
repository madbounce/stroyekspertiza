<?php

class FileController extends BackEndController
{
    public $layout = 'main';

    public function actionIndex($id = null)
    {
        if (!empty($id)) {
            $language = $this->loadLanguageModel($id);

            $model = new TranslateSourceFile('search');
            if (isset($_GET['TranslateSourceFile']))
                $model->attributes = $_GET['TranslateSourceFile'];

            $this->render('translate', array(
                'currentLanguage' => $language,
                'model' => $model
            ));
        } else
            $this->render('index');
    }

    public function actionUpdate($id, $language)
    {
        $language = $this->loadLanguageModel($language);
        $model = $this->loadModel($id)->{$language->language};

        if (isset($_POST['TranslateFile'])) {
            $model->attributes = $_POST['TranslateFile'];
            if ($model->save())
                $this->redirect(array('index', 'id' => $language->id));
        }

        $this->render('update', array('model' => $model, 'language' => $language));
    }

    public function loadLanguageModel($id)
    {
        $model = TranslateLanguage::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('translate', 'Запрашиваемая страница не существует.'));
        return $model;
    }

    public function loadModel($id)
    {
        $model = TranslateSourceFile::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('translate', 'Запрашиваемая страница не существует.'));
        return $model;
    }
}
