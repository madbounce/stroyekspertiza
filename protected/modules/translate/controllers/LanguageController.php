<?php

class LanguageController extends BackEndController
{
    public $layout = 'main';

    public function actionIndex()
    {
        $this->render('index', array(
            'dataProvider' => new CActiveDataProvider('TranslateLanguage', array(
                'pagination' => array(
                    'pageSize' => 50,
                ),
            )),
        ));
    }

    public function actionCreate()
    {
        $model = new TranslateLanguage;

        $this->performAjaxValidation($model);

        if (isset($_POST['TranslateLanguage'])) {
            $model->attributes = $_POST['TranslateLanguage'];
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function loadModel($id)
    {
        $model = TranslateLanguage::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('translate', 'Запрашиваемая страница не существует.'));
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'translate-language-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
