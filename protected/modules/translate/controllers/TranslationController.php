<?php

class TranslationController extends BackEndController
{
    public $layout = 'main';

    public function actionIndex($id = null)
    {
        if (!empty($id)) {
            $language = $this->loadModel($id);

            $model = new TranslateSourceMessage('search');
            if (isset($_GET['TranslateSourceMessage']))
                $model->attributes = $_GET['TranslateSourceMessage'];

            $this->render('translate', array(
                'currentLanguage' => $language,
                'model' => $model
            ));
        } else
            $this->render('index');
    }

    public function actionTranslate($id, $language)
    {
        $model = TranslateMessage::model()->findByAttributes(array('id' => $id, 'language' => $language));
        if (empty($model))
            throw new CHttpException(404, Yii::t('translate', 'Запрашиваемая страница не существует.'));

        if (isset($_POST['TranslateMessage'])) {
            $model->attributes = $_POST['TranslateMessage'];
            if ($model->save()) {
                echo CJSON::encode(array('status' => true));
                Yii::app()->end();
            }
        }

        echo CJSON::encode(array('status' => false));
        Yii::app()->end();
    }

    public function loadModel($id)
    {
        $model = TranslateLanguage::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('translate', 'Запрашиваемая страница не существует.'));
        return $model;
    }
}
