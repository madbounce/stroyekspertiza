SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `translate_file`;
CREATE TABLE IF NOT EXISTS `translate_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language` varchar(16) NOT NULL,
  `file` varchar(512) DEFAULT NULL,
  UNIQUE KEY `id` (`id`,`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `translate_language`;
CREATE TABLE IF NOT EXISTS `translate_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


DROP TABLE IF EXISTS `translate_message`;
CREATE TABLE IF NOT EXISTS `translate_message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) NOT NULL DEFAULT '',
  `translation` text,
  PRIMARY KEY (`id`,`language`),
  KEY `language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `translate_source_file`;
CREATE TABLE IF NOT EXISTS `translate_source_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(512) NOT NULL,
  `file` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `translate_source_message`;
CREATE TABLE IF NOT EXISTS `translate_source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(32) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `translate_file`
  ADD CONSTRAINT `translate_file_ibfk_1` FOREIGN KEY (`id`) REFERENCES `translate_source_file` (`id`) ON DELETE CASCADE;

ALTER TABLE `translate_message`
  ADD CONSTRAINT `FK_message_source_message` FOREIGN KEY (`id`) REFERENCES `translate_source_message` (`id`) ON DELETE CASCADE;

INSERT INTO `translate_language` (`id`, `language`) VALUES
(1, 'ru'),
(2, 'en');

SET FOREIGN_KEY_CHECKS=1;
