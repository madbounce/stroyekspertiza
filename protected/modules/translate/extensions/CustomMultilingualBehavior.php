<?php

Yii::import('translate.extensions.MultilingualBehavior');

class CustomMultilingualBehavior extends MultilingualBehavior
{
    public $createTable = true;
    public $strict = false;

    public function attach($owner) {
        parent::attach($owner);

        if ($this->createTable) {
            $this->createLangTable();
        }
    }

    protected function createLangTable()
    {
        $langTableName = Yii::app()->db->tablePrefix . $this->langTableName;

        if (!Yii::app()->db->createCommand("SHOW TABLES LIKE '" . $langTableName . "'")->execute()) {
            Yii::app()->db->createCommand()->createTable($langTableName, array(
                $this->localizedPrefix . 'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY',
                $this->langForeignKey => $this->owner->getTableSchema()->getColumn($this->owner->getTableSchema()->primaryKey)->dbType,
                $this->langField => 'varchar(6) NOT NULL',
            ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');

            Yii::app()->db->createCommand()->createIndex($this->langForeignKey, $langTableName, $this->langForeignKey);
            Yii::app()->db->createCommand()->addForeignKey(
                $this->langForeignKey . '_ibfk',
                $langTableName,
                $this->langForeignKey,
                $this->owner->tableName(), 'id', 'CASCADE', 'CASCADE');

            foreach ($this->localizedAttributes as $attribute) {
                $type = $this->owner->getTableSchema()->getColumn($attribute)->dbType;
                Yii::app()->db->createCommand()->addColumn($langTableName, $this->localizedPrefix . $attribute, $type);
            }
        }
    }

    public function localizedCriteria() {
        if ($this->strict && Yii::app()->language != Yii::app()->params['defaultLanguage']) {
            return array(
                'with'=>array(
                    $this->localizedRelation => array('joinType' => 'RIGHT JOIN'),
                ),
                'together' => true,
            );
        } else {
            return parent::localizedCriteria();
        }
    }
}
