<?php

/**
 * This is the model class for table "translate_file".
 *
 * The followings are the available columns in table 'translate_file':
 * @property string $id
 * @property string $language
 * @property string $file
 *
 * The followings are the available model relations:
 * @property TranslateSourceFile $id0
 */
class TranslateFile extends CActiveRecord
{
    public $file;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{translate_file}}';
    }

    public function rules()
    {
        return array(
            array('language', 'required'),
            array('language', 'length', 'max' => 16),
            array('file', 'length', 'max' => 512),

            array('id, language, file', 'safe', 'on' => 'search'),

            array('file', 'file', 'allowEmpty' => true, 'safe' => false),
        );
    }

    public function relations()
    {
        return array(
            'source' => array(self::BELONGS_TO, 'TranslateSourceFile', 'id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'language' => 'Language',
            'file' => Yii::t('translate', 'Перевод изображения'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.language', $this->language, true);
        $criteria->compare('t.file', $this->file, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($file = CUploadedFile::getInstance($this, 'file')) {
                $this->deleteFile();

                $this->file = $file;
                if (!is_dir(TranslateModule::getFilePath()))
                    mkdir(TranslateModule::getFilePath(), 0777, true);
                $this->file->saveAs(TranslateModule::getFilePath() . '/' . $this->file);
                $this->file = TranslateModule::getFileUrl(false) . '/' . $this->file;
            }
            return true;
        }
        return false;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteFile();
            return true;
        }
        return false;
    }

    public function deleteFile()
    {
        $path = TranslateModule::getFilePath() . '/' . $this->file;
        if(is_file($path))
            unlink($path);
    }
}