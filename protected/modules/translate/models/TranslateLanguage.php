<?php

/**
 * This is the model class for table "translate_language".
 *
 * The followings are the available columns in table 'translate_language':
 * @property string $id
 * @property string $language
 */
class TranslateLanguage extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{translate_language}}';
    }

    public function rules()
    {
        return array(
            array('language', 'required'),
            array('language', 'length', 'max' => 16),

            array('id, language', 'safe', 'on' => 'search'),

            array('language', 'unique', 'message' => Yii::t('translate', 'Этот язык уже добавлен.')),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'language' => Yii::t('translate', 'Код'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.language', $this->language, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    protected function afterSave()
    {
        $messages = TranslateSourceMessage::getAll();
        foreach ($messages as $message) {
            $message->createTranslations();
        }
    }

    protected function afterDelete()
    {
        TranslateMessage::model()->deleteAll('language = :language', array(':language' => $this->language));
    }

    /**
     * General
     */

    public function getName()
    {
        return mb_convert_case(Yii::app()->locale->getLanguage($this->language), MB_CASE_TITLE, 'utf-8');
    }

    public static function getLanguages()
    {
        return TranslateLanguage::model()->findAll();
    }

    public static function getAllLanguages()
    {
        $languages = array();
        $ids = Yii::app()->locale->getLocaleIDs();
        foreach ($ids as $code) {
            $lang = Yii::app()->locale->getLanguageID($code);
            if (!isset($languages[$lang]))
                $languages[$lang] = mb_convert_case(Yii::app()->locale->getLanguage($lang), MB_CASE_TITLE, 'utf-8');
        }

        $languages = array_filter($languages);
        asort($languages);
        return $languages;
    }

    public function getTotalMessageCount()
    {
        return TranslateMessage::model()->language($this->language)->count();
    }

    public function getTranslatedMessageCount()
    {
        return TranslateMessage::model()->language($this->language)->translated()->count();
    }

    /**
     * Scopes
     */

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);
        return array(
            'order' => $alias . '.id'
        );
    }
}