<?php

/**
 * This is the model class for table "{{translate_message}}".
 *
 * The followings are the available columns in table '{{translate_message}}':
 * @property integer $id
 * @property string $language
 * @property string $translation
 *
 * The followings are the available model relations:
 * @property TranslateSourceMessage $source
 */
class TranslateMessage extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{translate_message}}';
    }

    public function rules()
    {
        return array(
            array('id', 'numerical', 'integerOnly' => true),
            array('language', 'length', 'max' => 16),
            array('translation', 'safe'),

            array('id, language, translation', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'source' => array(self::BELONGS_TO, 'TranslateSourceMessage', 'id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'language' => 'Language',
            'translation' => 'Translation',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.language', $this->language, true);
        $criteria->compare('t.translation', $this->translation, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Criteria
     */

    public function getLanguageCriteria($language)
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->compare($alias . '.language', $language);
        return $criteria;
    }

    public function getTranslatedCriteria()
    {
        $alias = $this->getTableAlias();
        $criteria = new CDbCriteria;
        $criteria->addCondition("$alias.translation IS NOT NULL AND $alias.translation <> ''");
        return $criteria;
    }

    /**
     * Scopes
     */

    public function language($language)
    {
        $this->getDbCriteria()->mergeWith($this->getLanguageCriteria($language));
        return $this;
    }

    public function translated()
    {
        $this->getDbCriteria()->mergeWith($this->getTranslatedCriteria());
        return $this;
    }
}