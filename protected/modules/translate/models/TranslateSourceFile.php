<?php

/**
 * This is the model class for table "translate_source_file".
 *
 * The followings are the available columns in table 'translate_source_file':
 * @property string $id
 * @property string $key
 * @property string $file
 *
 * The followings are the available model relations:
 * @property TranslateFile $translateFile
 */
class TranslateSourceFile extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{translate_source_file}}';
    }

    public function rules()
    {
        return array(
            array('key, file', 'required'),
            array('key, file', 'length', 'max' => 512),

            array('id, key, file', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        $languages = TranslateLanguage::getLanguages();
        $relations = array();
        foreach ($languages as $language) {
            $relations[$language->language] = array(self::HAS_ONE, 'TranslateFile', 'id', 'condition' => $language->language . '.language = :lang', 'params' => array(':lang' => $language->language));
        }

        return CMap::mergeArray(
            array(
                'translateFile' => array(self::HAS_ONE, 'TranslateFile', 'id'),
            ),
            $relations
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'key' => Yii::t('translate', 'Ключь'),
            'file' => Yii::t('translate', 'Файл'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.key', $this->key, true);
        $criteria->compare('t.file', $this->key, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * General
     */

    public static function get($key)
    {
        return TranslateSourceFile::model()->with('translateFile')->findByAttributes(array('key' => $key));
    }

    public static function getAll()
    {
        return TranslateSourceFile::model()->findAll();
    }

    public static function create($key, $file)
    {
        $exists = TranslateSourceFile::model()->exists('t.key = :key', array(':key' => $key));

        if (!$exists) {
            $transaction = Yii::app()->db->beginTransaction();

            try {
                $model = new TranslateSourceFile;
                $model->key = $key;
                $model->file = $file;
                if ($model->save()) {
                    $model->createTranslations();
                    $transaction->commit();
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }
        }
    }

    public function createTranslations()
    {
        $languages = TranslateLanguage::getLanguages();
        foreach ($languages as $language) {
            if (!isset($this->{$language->language})) {
                $model = new TranslateFile();
                $model->id = $this->id;
                $model->language = $language->language;
                $model->save();
            }
        }
    }
}