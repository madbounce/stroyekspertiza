<?php

/**
 * This is the model class for table "{{translate_source_message}}".
 *
 * The followings are the available columns in table '{{translate_source_message}}':
 * @property integer $id
 * @property string $category
 * @property string $message
 *
 * The followings are the available model relations:
 * @property TranslateMessage[] $translateMessages
 */
class TranslateSourceMessage extends CActiveRecord
{
    public $translated;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{translate_source_message}}';
    }

    public function rules()
    {
        return array(
            array('category', 'length', 'max' => 32),
            array('message', 'safe'),

            array('id, category, message, translated', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        $languages = TranslateLanguage::getLanguages();
        $relations = array();
        foreach ($languages as $language) {
            $relations[$language->language] = array(self::HAS_ONE, 'TranslateMessage', 'id', 'condition' => $language->language . '.language = :lang', 'params' => array(':lang' => $language->language));
        }

        return CMap::mergeArray(
            array(
                'translateMessages' => array(self::HAS_MANY, 'TranslateMessage', 'id'),
            ),
            $relations
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'category' => Yii::t('translate', 'Категория'),
            'message' => Yii::t('translate', 'Строка'),
        );
    }

    public function search($language)
    {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.category', $this->category);
        $criteria->compare('t.message', $this->message, true);

        if ($this->translated != null) {
            $condition = $this->translated ? "{$language->language}.translation IS NOT NULL AND {$language->language}.translation <> ''" :
                "{$language->language}.translation IS NULL OR {$language->language}.translation = ''";
            $criteria->with = array(
                $language->language => array('condition' => $condition),
            );
            $criteria->together = true;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
    }

    /**
     * General
     */

    public static function getAll()
    {
        return TranslateSourceMessage::model()->findAll();
    }

    public static function create($category, $message)
    {
        $exists = TranslateSourceMessage::model()->exists('message = :message AND category = :category', array(
            ':category' => $category,
            ':message' => $message
        ));

        if (!$exists) {
            $transaction = Yii::app()->db->beginTransaction();

            try {
                $model = new TranslateSourceMessage;
                $model->category = $category;
                $model->message = $message;
                if ($model->save()) {
                    $model->createTranslations();
                    $transaction->commit();
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }
        }
    }

    public function createTranslations()
    {
        $languages = TranslateLanguage::getLanguages();
        foreach ($languages as $language) {
            if (!isset($this->{$language->language})) {
                $model = new TranslateMessage;
                $model->id = $this->id;
                $model->language = $language->language;
                if ($language->language == Yii::app()->sourceLanguage)
                    $model->translation = $this->message;
                $model->save();
            }
        }
    }
}