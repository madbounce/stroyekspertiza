<h3><?php echo Yii::t('translate', 'Перевод на :language', array(':language' => $currentLanguage->name)); ?></h3>

<?php foreach (TranslateLanguage::getLanguages() as $language): ?>
<?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => $language->name,
        'type' => $language->id == $currentLanguage->id ? 'primary' : '',
        'url' => array('/translate/file/index', 'id' => $language->id)
    )); ?>
<?php endforeach; ?>

<?php /** @var TbActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm'); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'template' => '{summary} {pager} {items} {pager}',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'header' => '#',
            'name' => 'id',
            'htmlOptions' => array('class' => 'span1'),
        ),
        array(
            'name' => 'key',
            'htmlOptions' => array('class' => 'span2'),
        ),
        array(
            'header' => Yii::t('translate', 'Оригинальное изображение'),
            'type' => 'raw',
            'value' => function($data) {
                return CHtml::link(CHtml::image($data->file), $data->file);
            },
            'htmlOptions' => array('class' => 'span4'),
        ),
        array(
            'header' => Yii::t('translate', 'Перевод изображения'),
            'type' => 'raw',
            'value' => function($data) use ($currentLanguage) {
                if (!empty($data->{$currentLanguage->language}->file))
                    return CHtml::link(CHtml::image(Yii::app()->baseUrl . $data->{$currentLanguage->language}->file), $data->{$currentLanguage->language}->file);
            },
            'htmlOptions' => array('class' => 'span4'),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{translate}',
            'buttons' => array(
                'translate' => array(
                    'label' => Yii::t('translate', 'Перевод'),
                    'icon' => 'th-list',
                    'url' => function($data) use ($currentLanguage) {
                        return Yii::app()->controller->createUrl('/translate/file/update', array('id' => $data->primaryKey, 'language' => $currentLanguage->id));
                    }
                ),
            ),
            'htmlOptions' => array('class' => 'span1'),
        ),
    ),
)); ?>

<?php $this->endWidget(); ?>