<h3><?php echo Yii::t('translate', 'Перевод на :language', array(':language' => $language->name)); ?></h3>

<?php /** @var TbActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'translate-file-form',
    'htmlOptions' => array('class' => 'well', 'enctype' => 'multipart/form-data'),
    'enableAjaxValidation' => false,
)); ?>

<?php echo CHtml::label(Yii::t('translate', 'Оригинальное изображение'), ''); ?>
<?php echo CHtml::link(CHtml::image($model->source->file), $model->source->file); ?>

<?php echo $form->fileFieldRow($model, 'file'); ?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => Yii::t('translate', 'Сохранить'))); ?>
</div>

<?php $this->endWidget(); ?>