<?php /** @var TbActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'translate-language-form',
    'htmlOptions' => array('class' => 'well'),
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>

<?php echo $form->dropDownListRow($model, 'language', TranslateLanguage::getAllLanguages()); ?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => Yii::t('translate', 'Добавить'))); ?>
</div>

<?php $this->endWidget(); ?>