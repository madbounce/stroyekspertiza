<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label' => Yii::t('translate', 'Добавить язык'),
    'type' => 'primary',
    'url' => array('/translate/language/create'),
)); ?>

<?php /** @var TbActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm'); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'template' => '{summary} {pager} {items} {pager}',
    'dataProvider' => $dataProvider,
    'columns' => array(
        array(
            'header' => '#',
            'name' => 'id',
        ),
        'language',
        array(
            'header' => Yii::t('translate', 'Название'),
            'value' => function($data) { return $data->name; }
        ),
        array(
            'header' => Yii::t('translate', 'Переведено'),
            'value' => function($data) {
                $total = $data->getTotalMessageCount();
                $translated = $data->getTranslatedMessageCount();
                if ($total > 0)
                    return "$translated / $total" . ' (' . Yii::app()->numberFormatter->formatPercentage($translated / $total) . ')';
                else
                    return "$translated / $total" . ' (' . Yii::app()->numberFormatter->formatPercentage(0) . ')';
            }
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{translate} {delete}',
            'buttons' => array(
                'translate' => array(
                    'label' => Yii::t('translate', 'Перевод'),
                    'icon' => 'th-list',
                    'url' => 'Yii::app()->controller->createUrl("/translate/translation/index", array("id" => $data->primaryKey))'
                ),
            ),
            'htmlOptions' => array('style' => 'width: 50px'),
        ),
    ),
)); ?>

<?php $this->endWidget(); ?>