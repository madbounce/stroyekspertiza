<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/main'); ?>

<?php $this->widget('bootstrap.widgets.TbMenu', array(
    'type' => 'pills',
    'stacked' => false,
    'items' => array(
        array('label' => Yii::t('translate', 'Языки'), 'url' => array('/translate/language/index')),
        array('label' => Yii::t('translate', 'Перевод текста'), 'url' => array('/translate/translation/index')),
        array('label' => Yii::t('translate', 'Перевод изображений'), 'url' => array('/translate/file/index')),
    ),
)); ?>

<?php echo $content; ?>

<?php $this->endContent(); ?>