<h2><?php echo Yii::t('translate', 'Выберите язык'); ?></h2>

<?php foreach (TranslateLanguage::getLanguages() as $language): ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => $language->name,
        'url' => array('/translate/translation/index', 'id' => $language->id)
    )); ?>
<?php endforeach; ?>