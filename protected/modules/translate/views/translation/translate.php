<script type="text/javascript">
    $(function() {
        $.registerEvents = function() {
            $("input.translation").blur(function() {
                var url = $(this).attr("data-url");
                var data = $(this).parent("td").children("input").serialize();
                <?php echo CHtml::ajax(array(
                    'url' => 'js:url',
                    'type' => 'post',
                    'dataType' => 'json',
                    'data' => 'js:data',
                    'success' => 'function(data) {
                        if (data.status)
                            $.fn.yiiGridView.update("translate-grid");
                    }'
                )); ?>
            });
        }

        $.registerEvents();
    });
</script>

<h3><?php echo Yii::t('translate', 'Перевод на :language', array(':language' => $currentLanguage->name)); ?></h3>

<?php foreach (TranslateLanguage::getLanguages() as $language): ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label' => $language->name,
        'type' => $language->id == $currentLanguage->id ? 'primary' : '',
        'url' => array('/translate/translation/index', 'id' => $language->id)
    )); ?>
<?php endforeach; ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'translate-grid',
    'template' => '{summary} {pager} {items} {pager}',
    'dataProvider' => $model->search($currentLanguage),
    'afterAjaxUpdate' => 'function(id, data) { $.registerEvents(); }',
    'filter' => $model,
    'columns' => array(
        array(
            'header' => '#',
            'name' => 'id',
            'htmlOptions' => array('class' => 'span1'),
        ),
        array(
            'name' => 'category',
            'htmlOptions' => array('class' => 'span2'),
            'filter' => CHtml::listData(TranslateSourceMessage::getAll(), 'category', 'category'),
        ),
        array(
            'name' => 'message',
            'htmlOptions' => array('class' => 'span3'),
        ),
        array(
            'header' => Yii::t('translate', 'Перевод'),
            'type' => 'raw',
            'value' => function($data) use ($currentLanguage) {
                $content = CHtml::activeHiddenField($data->{$currentLanguage->language}, 'id');
                $content .= CHtml::activeHiddenField($data->{$currentLanguage->language}, 'language');
                $content .= CHtml::activeTextField($data->{$currentLanguage->language}, 'translation', array(
                    'class' => 'translation span6',
                    'data-url' => CHtml::normalizeUrl(array('/translate/translation/translate', 'id' => $data->id, 'language' => $currentLanguage->language))
                ));
                return $content;
            },
            'name' => 'translated',
            'filter' => array(true => Yii::t('translate', 'Есть'), false => Yii::t('translate', 'Нету')),
            'htmlOptions' => array('class' => 'span6'),
        ),
    ),
)); ?>