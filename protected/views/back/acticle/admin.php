<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Статьи')
);
?>
<div class="section">
	<div class="box">
	<div class="title">
		<h2>Управление "Баннеры"</h2>
	</div>
		<div class="content">
			<div class="button" style="float: left;padding-left: 20px;">
				<?php echo CHtml::link('Добавить статью', array('/banner/banner/create'), $htmlOptions=array('class'=>'button green medium', 'style'=>'opacity: 0.7;')); ?>
			</div>
			<div class="button" style="float: left;padding-left: 20px;">
				<?php echo CHtml::link('Категории', array('/banner/category'), $htmlOptions=array('class'=>'button green medium', 'style'=>'opacity: 0.7;')); ?>
			</div>
			<div style="clear: both;"></div>

			<?php $this->widget('bootstrap.widgets.TbGridView', array(
				'id'=>'acticle-grid',
				'dataProvider'=>$model->search(),
//				'filter'=>$model,
//				'itemsCssClass'=>'table',
				'columns'=>array(
					'id',
					'title',
					array(
						'name'=>'create_time',
						'value'=>'date("d.m.Y", $data->create_time)',
					),
					array(
						'name'=>'update_time',
						'value'=>'date("d.m.Y", $data->update_time)',
					),
					array(
						'name'=>Mist::t('Категория'),
						'value'=>'$data->category->title',
					),
					array(
//						'class'=>'bootstrap.widgets.TbButtonColumn',
						'class'=>'CButtonColumn',
						'template'=>'{update}{delete}',
//						'updateButtonImageUrl'=>$this->path.'gfx/icon-edit.png',
//						'deleteButtonImageUrl'=>$this->path.'gfx/icon-delete.png',
					),
				),
			)); ?>
		</div>
	</div>
</div>