<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'activity-form',
    'enableAjaxValidation' => false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->dropDownListRow($model, 'parent_id', Activity::getItemOptions(Yii::t('admin', 'Нет')), array('encode' => false, 'class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => Yii::t('admin', 'Создать'),
)); ?>
</div>

<?php $this->endWidget(); ?>
