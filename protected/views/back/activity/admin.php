<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Сферы деятельности')
);

Yii::app()->clientScript->registerScript('search', "
$('select#activity').change(function() {
    $('.search-form form').submit();
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('activity-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label' => Yii::t('admin', 'Добавить сферу деятельности'),
    'type' => 'primary',
    'url' => array('/activity/create'),
)); ?>

<hr>

<div class="search-form">
    <?php /** @var $form TbActiveForm */ ?>
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
)); ?>

    <?php echo $form->dropDownListRow($model, 'parent_id', Activity::getItemOptions(), array('id' => 'activity', 'encode' => false, 'class' => 'span5')); ?>

    <?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'activity-grid',
    'dataProvider' => $model->adminSearch(),
    'filter' => $model,
    'columns' => array(
        'id',
        array(
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'name' => 'name',
            'editable' => array(
                'url' => $this->createUrl('/activity/editable'),
                'placement' => 'right',
                'inputclass' => 'span3',
                'title' => Yii::t('admin', 'Введите имя сферы деятельности'),
                'success' => 'js: function(data) {
                    if(typeof data == "object" && !data.success) return data.msg;
                }'
            )
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}'
        ),
    ),
)); ?>
