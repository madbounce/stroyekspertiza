<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Сферы деятельности') => array('/activity/admin'),
    Yii::t('admin', 'Создание сферы деятельности')
);
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>