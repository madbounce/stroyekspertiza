<?php
if(($model->isNewRecord) && isset($_GET['cId']))
    $model->material_category_id = (int) $_GET['cId'];
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'article-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'product'
    ),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textField($model, 'title', array('class' => 'name', 'maxlength' => 255)); ?>
    <div class="checkbox"></div>
</div>

<div class="block layer param" style="width:870px;min-height: 200px;">

<? echo $form->labelEx($model,'type', array('class' => 'property'))?>
<div id="product_categories">
    <?php echo $form->dropDownList($model, 'type', array(
													'breed'=>'О заводчиказ',
													'veterinarians'=>'О ветеринарах',
													'puppys'=>'О щенках',
													'dogs'=>'О собаках',
													'kittens'=>'О котятах',
													'cats'=>'О кошках'
													)); ?>
</div>
<br /><br /><br />

<label for="Articles_material_category_id" class="property">Категория</label>
<div id="product_categories">
    <?php echo $form->dropDownList($model, 'material_category_id', CHtml::listData(ArticleCategory::model()->findAll(), 'id','title'),
    array(
        'encode' => false,
        'empty' => Yii::t('app', 'Выберите категорию')
    )); ?>
</div>
<br /><br /><br />

<? echo $form->labelEx($model,'age', array('class' => 'property'))?>
<div id="product_categories">
    <?php echo $form->dropDownList($model, 'age', array('До года','Старше года')); ?>
</div>
<br /><br /><br />

<? echo $form->labelEx($model,'size', array('class' => 'property'))?>
<div id="product_categories">
    <?php echo $form->dropDownList($model, 'size', array('Малый','Средний','Большой/Гигантский'));?>
</div>
</div>
<!-- Левая колонка свойств товара -->
<div id="column_left">

    <!-- Параметры страницы -->
    <div class="block layer param">
        <h2>Параметры страницы</h2>
        <ul>
            <li>
                <?php echo $form->labelEx($model, 'url', array('class' => 'property')); ?>
                <div class="page_url">/article/</div>
                <?php echo $form->textField($model, 'url', array('class' => 'page_url', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'meta_title', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'meta_title', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'meta_keywords', array('class' => 'property')); ?>
                <?php echo $form->textField($model, 'meta_keywords', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
            </li>
            <li>
                <?php echo $form->labelEx($model, 'meta_description', array('class' => 'property')); ?>
                <?php echo $form->textArea($model, 'meta_description', array('class' => 'simpla_inp')); ?>
            </li>
        </ul>
    </div><!-- Параметры страницы (The End)-->

    <!-- Параметры статьи -->
    <!--<div class="block layer">
        <h2>Параметры статьи</h2>
        <ul>
            <li>
                <?php //echo $form->labelEx($model, 'topic', array('class' => 'property')); ?>
                <?php //echo $form->textField($model, 'topic', array( 'class' => 'simpla_inp',  'maxlength' => 255)); ?>
            </li>
            <li>
                <?php //echo $form->labelEx($model, 'content_short', array('class' => 'property')); ?>
                <?php //echo $form->textArea($model, 'content_short', array( 'class' => 'simpla_inp')); ?>
            </li>
            <li>
                <?php //echo $form->labelEx($model, 'image_alt', array('class' => 'property')); ?>
                <?php //echo $form->textField($model, 'image_alt', array( 'class' => 'simpla_inp',  'maxlength' => 255)); ?>
            </li>
        </ul>

    </div><!-- Параметры товара (The End)-->

</div><!-- Левая колонка свойств товара (The End)-->

<!-- Правая колонка свойств товара -->
<div id="column_right">

    <!-- Изображение категории -->
    <div class="block layer images">
        <h2>Изображения</h2>

        <?php
        $this->widget('application.modules.image.components.widgets.UploadWidget', array(
            'model' => $model,
            'tag' => 'image',
            'action' => array('/article/uploadImage'),
            'multiple' => true,
            'view' => 'imagesCatalog',
            'uploadButtonText' => Yii::t('article', 'Загрузить изображение'),
        ));
        ?>
    </div>
</div>
<!-- Правая колонка свойств товара (The End)-->

<div style="clear: both"></div>

<div class="editor-line">
    <?php echo $form->labelEx($model,'content'); ?>
    <?php //echo $form->textArea($model,'content',array('class'=>'medium')); ?>
    <?php $this->widget('application.extensions.redactorjs.ERedactorWidget', array(
        'model' => $model,
        'attribute' => 'content',
        'options' => array(
            'lang' => 'ru',
            'imageUpload' => CHtml::normalizeUrl(array('/article/redactorUploadImage'))
        ),
    )); ?>
    <?php echo $form->error($model,'content'); ?>
</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>