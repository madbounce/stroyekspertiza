<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Статьи') => array('/article/admin'),
    Yii::t('admin', 'Создание статьи')
);
?>

<div class="page-header">
    <h3><?php echo Yii::t('news', 'Создание статьи'); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>