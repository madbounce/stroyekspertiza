<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Статьи') => array('/article/admin'),
    Yii::t('admin', 'Редактирование статьи')
);
?>

<div class="page-header">
    <h3><?php echo Yii::t('news', 'Редактирование статьи'); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>