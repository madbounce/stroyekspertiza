<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'regions-form',
    'enableAjaxValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
        'class' => 'product'
    ),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textField($model, 'title', array('class' => 'name', 'maxlength' => 255)); ?>
    <div class="checkbox"></div>
</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => '',
    'htmlOptions' => array(
        'name' => 'more',
    ),
    'label' => Yii::t('admin', 'Сохранить и добавить еще'),
)); ?>
</div>

<?php $this->endWidget(); ?>