<?php
$dp = $model->adminSearch();
$countAll = $dp->totalItemCount;
?>

<div class="page-header admin-header">
    <h3><?php echo Yii::t('app', '{n} регион|{n} региона|{n} регионов', $countAll);?></h3>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'label' => Yii::t('country', 'Добавить регион'),
    'size' => 'small',
    'url' => array('/dicRegions/create'),
));
    ?>
</div>
<div class="page-content">
    <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'id' => 'country-grid',
    'type'=>'striped bordered',
    'dataProvider' => $dp,
    'enableSorting' => false,
    'sortableRows' => true,
    'sortableAjaxSave'=>true,
    'sortableAttribute'=>'sort',
    'sortableAction'=> '/dicRegions/sortable',
    'template' => '{pager}{items}{pager}',
    'columns' => array(
        array(
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'name' => 'title',
            'type'=>'raw',
            'value' => '$data->title',
            'editable' => array(
                'url' => $this->createUrl('/dicRegions/editable'),
                'placement' => 'right',
                'inputclass' => 'span4',
                'title' => Yii::t('admin', 'Введите название региона'),
                'success' => 'js: function(data) {
                    if(typeof data == "object" && !data.success) return data.msg;
                }'
            ),

        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}'
        ),
    ),
)); ?>
</div>