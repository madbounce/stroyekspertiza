<div class="page-header">
    <h3><?php echo Yii::t('config', 'Редактирование региона'); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>