<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'filters-form',
    'enableAjaxValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
        'class' => 'product'
    ),
)); ?>

<div class="block layer" style="width: auto; min-height: auto;">

<p class="help-block"><?php echo Yii::t('app', 'Поля помеченные <span class="required">*</span> обязательны для заполнения.'); ?></p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'value', array('class' => 'span5', 'maxlength' => 255)); ?>

</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
</div>

<?php $this->endWidget(); ?>