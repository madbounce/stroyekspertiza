<?php
$dp = $model->adminSearch();
$countAll = $dp->totalItemCount;
?>

<div class="page-header admin-header">
    <h3><?php echo Yii::t('app', '{n} фильтр|{n} фильтра|{n} фильтров', $countAll);?></h3>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'label' => Yii::t('country', 'Добавить фильтр'),
    'size' => 'small',
    'url' => array('/filters/create'),
));
    ?>
</div>
<div class="page-content">
    <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'id'=>'filters-grid',
    'type' => 'striped bordered',
    'dataProvider'=>$dp,
    'template' => '{pager}{items}{pager}',
    'columns'=>array(
        'name',
        'value',
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
)); ?>
</div>