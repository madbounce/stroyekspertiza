<?php /* @var $this Controller */ ?>
<? $path = Yii::app()->createAbsoluteUrl('../themes/iams/front')?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="<?php echo Yii::app()->language; ?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="<?php echo Yii::app()->language; ?>" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	
    <link rel="stylesheet"  type="text/css" href="<?php echo $path; ?>/css/bootstrap.css" media="screen" />
	<link rel="stylesheet"  type="text/css" href="<?php echo $path; ?>/css/bootstrap-responsive.css" media="screen, projection" />
    <link rel="stylesheet" href="<?php echo $path; ?>/css/styles.css" type="text/css" media="screen" />
<!--<script type="text/javascript" src="http://fast.fonts.com/jsapi/6fa157d1-c2da-4f13-844c-17c9184b0790.js"></script>-->

    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
    <?php Yii::app()->clientScript->registerScriptFile($path . '/js/common.js', CClientScript::POS_HEAD); ?>
    <?php Yii::app()->clientScript->registerScriptFile($path . '/js/bootstrap.min.js', CClientScript::POS_HEAD); ?>
    <?php Yii::app()->clientScript->registerScriptFile($path . '/js/app.js', CClientScript::POS_HEAD); ?>

</head>

<body class="cat">
    <?php echo $content; ?>
</body>
</html>
