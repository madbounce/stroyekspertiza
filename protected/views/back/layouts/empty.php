<?php /* @var $this Controller */ ?>

<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language; ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="<?php echo Yii::app()->language; ?>" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/back.css" media="screen, projection" />

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
<style>
.breadcrumb {
    background-color: #F5F5F5;
    border-radius: 4px 4px 4px 4px;
    list-style: none outside none;
    margin: 0 30px 20px 0;
    padding: 8px 15px;
}

</style>
</head>

<body>
    <?php echo $content; ?>
</body>
</html>
