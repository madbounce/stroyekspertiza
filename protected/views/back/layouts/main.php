<?php /* @var $this Controller */ ?>

<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language; ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="<?php echo Yii::app()->language; ?>" />

    <?php
        $cs=Yii::app()->getClientScript();
        $cs->registerCssFile(Yii::app()->request->baseUrl.'/css/back.css');
    ?>
    <title><?php echo Yii::t('app', 'Панель управления');//CHtml::encode($this->pageTitle); ?></title>

</head>

<body>

<style>
label {
	display: block;
}
</style>
<?php
$cId = Yii::app()->controller->id;
$aId = Yii::app()->controller->action->id;
$mId = (Yii::app()->controller->module) ? Yii::app()->controller->module->id : null;
?>
<?php $this->widget('bootstrap.widgets.TbNavbar', array(
    //'type' => 'inverse',
    'brand' => '',//Yii::app()->name,
    'brandUrl' => Yii::app()->homeUrl,
    'collapse' => true,
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbMenu',
            'items' => array(
				array('label' => Misc::t('Контент'), 'url' => array('/page/admin/index'), 'visible' => !Yii::app()->user->isGuest, 'active' => ($mId == 'page') || ($mId == 'news') || ($cId == 'article') || ($mId == 'expert')),
				array('label' => Misc::t('Пользователи'), 'url' => array('/user/index'), 'visible' => !Yii::app()->user->isGuest, 'active' => ($cId == 'user') || ($mId == 'newsletter') || ($cId == 'foodbuy') || ($cId == 'foodbrands')),
//				array('label' => Misc::t('Баннеры'), 'url' => array('/backend/banner/banner/admin'), 'visible' => !Yii::app()->user->isGuest, 'active' => ($mId == 'banner')),
				array('label' => Misc::t('SEO'), 'url' => array('/backend/seo/admin/admin'), 'visible' => !Yii::app()->user->isGuest, 'active' => ($mId == 'seo')),
				array('label' => Misc::t('Настройки'), 'url' => array('/config/admin/index'), 'visible' => !Yii::app()->user->isGuest, 'active' => ($mId == 'config') || ($mId == 'mail') || ($cId == 'region') || ($cId == 'menu') || ($cId == 'country') || ($cId == 'dicregions')),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbMenu',
            'htmlOptions' => array('class' => 'pull-right'),
            'items' => array(
                array('label' => Misc::t('Вход'), 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                array('label' => Misc::t('Выйти'), 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
            ),
        ),
    ),
)); ?>

<div class="container admin" style="width:940px;">

    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block' => true,
        'fade' => true,
        'closeText' => '&times;',
        'alerts' => array(
            'success' => array('block' => true, 'fade' => true, 'closeText' => '&times;'),
        ),
    )); ?>

    <div class="row">
        <div class="span12">
            <?php echo $content; ?>
        </div>
    </div>
</div>

</body>

</html>
