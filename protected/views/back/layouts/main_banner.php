<?php /* @var $this Controller */ ?>
<?php
$cId = Yii::app()->controller->id;
$aId = Yii::app()->controller->action->id;
$mId = (Yii::app()->controller->module) ? Yii::app()->controller->module->id : null;
?>

<?php $this->beginContent('//layouts/main'); ?>

<?php $this->widget('bootstrap.widgets.TbMenu', array(
    //'type' => 'pills',
    'stacked' => false,
    'items' => array(
        array('label' => Yii::t('banner', 'Баннеры'), 'url' => array('/backend/banner/banner/admin'), 'active' =>($mId == 'banner') && ($cId == 'banner') && (($aId == 'admin') || ($aId == 'create') || ($aId == 'update'))),
        array('label' =>  Yii::t('banner', 'Слайдер'), 'url' => array('/backend/banner/banner/xml'), 'active' =>($mId == 'banner') && ($cId == 'banner') && ($aId == 'xml')),
        array('label' => Yii::t('banner', 'Категории'), 'url' => array('/backend/banner/category/admin'), 'active' =>($mId == 'banner') && ($cId == 'category')),
        array('label' => Yii::t('banner', 'Размеры'), 'url' => array('/backend/banner/size/admin'), 'active' => ($mId == 'banner') && ($cId == 'size')),
    ),
    'htmlOptions' => array('class' => 'nav nav-tabs')
)); ?>

<?php echo $content; ?>

<?php $this->endContent(); ?>