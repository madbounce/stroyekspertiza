<?php /* @var $this Controller */ ?>
<?php
$cId = Yii::app()->controller->id;
$aId = Yii::app()->controller->action->id;
$mId = (Yii::app()->controller->module) ? Yii::app()->controller->module->id : null;
?>

<?php $this->beginContent('//layouts/main'); ?>

<?php $this->widget('bootstrap.widgets.TbMenu', array(
    //'type' => 'pills',
    'stacked' => false,
    'items' => array(
        array('label' => Yii::t('catalog', 'Товары'), 'url' => array('/catalog/catalog/admin'), 'active' =>($mId == 'catalog') && ($cId == 'catalog')),
        array('label' =>  Yii::t('catalog', 'Категории'), 'url' => array('/catalog/category/admin'), 'active' =>($mId == 'catalog') && ($cId == 'category')),
        array('label' => Yii::t('catalog', 'Города'), 'url' => array('/dicCities/admin'), 'active' => ($cId == 'dicCities')),
        array('label' => Yii::t('catalog', 'Oтзывы'), 'url' => array('/rating/admin/index'), 'active' => ($mId == 'rating') && ($cId == 'admin')),
    ),
    'htmlOptions' => array('class' => 'nav nav-tabs')
)); ?>

<?php echo $content; ?>

<?php $this->endContent(); ?>