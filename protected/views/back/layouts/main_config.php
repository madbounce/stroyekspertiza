<?php /* @var $this Controller */ ?>
<?php
$cId = Yii::app()->controller->id;
$aId = Yii::app()->controller->action->id;
$mId = (Yii::app()->controller->module) ? Yii::app()->controller->module->id : null;
?>

<?php $this->beginContent('//layouts/main'); ?>

<?php $this->widget('bootstrap.widgets.TbMenu', array(
    //'type' => 'pills',
    'stacked' => false,
    'items' => array(
        array('label' => Yii::t('config', 'Настройки'), 'url' => array('/config/admin/index'), 'active' => ($mId == 'config')),
        array('label' => Yii::t('config', 'Шаблоны писем'), 'url' => array('/mail/admin/index'), 'active' => ($mId == 'mail')),
//        array('label' => Yii::t('config', 'Меню'), 'url' => array('/menu/admin'), 'active' => ($cId == 'menu')),
//        array('label' => Yii::t('config', 'Регионы'), 'url' => array('/dicRegions/admin'), 'active' => ($cId == 'dicRegions')),
    ),
    'htmlOptions' => array('class' => 'nav nav-tabs')
)); ?>

<?php echo $content; ?>

<?php $this->endContent(); ?>