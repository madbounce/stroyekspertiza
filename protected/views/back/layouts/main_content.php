<?php /* @var $this Controller */ ?>
<?php
$cId = Yii::app()->controller->id;
$aId = Yii::app()->controller->action->id;
$mId = (Yii::app()->controller->module) ? Yii::app()->controller->module->id : null;
?>

<?php $this->beginContent('//layouts/main'); ?>

<?php $this->widget('bootstrap.widgets.TbMenu', array(
    //'type' => 'pills',
    'stacked' => false,
    'items' => array(
        array('label' => Yii::t('content', 'Страницы'), 'url' => array('/page/admin/admin'), 'active' =>($mId == 'page' && (($aId == 'admin') || ($aId == 'create') || ($aId == 'update'))) ),
        array('label' => Yii::t('content', 'Категории Страниц'), 'url' => array('/page/admin/category'), 'active' =>($mId == 'page') && (($aId == 'category') || ($aId == 'createcategory') || ($aId == 'UpdateCategory'))),
//        array('label' =>  Yii::t('content', 'Новости'), 'url' => array('/articles/admin/admin'), 'active' =>($mId == 'articles') && (($aId == 'admin') || ($aId == 'create') || ($aId == 'update'))),
//        array('label' =>  Yii::t('content', 'Категории нововстей'), 'url' => array('/articles/admin/categories'), 'active' =>($mId == 'articles') && (($aId == 'categories') || ($aId == 'createcategory') || ($aId == 'UpdateCategory'))),
//        array('label' => Yii::t('content', 'Вопрос/Ответ'), 'url' =>  array('/expert/admin/index'), 'active' => ($mId == 'expert')),
//        array('label' => Yii::t('content', 'FAQ'), 'url' =>  array('/faq/admin/index'), 'active' => ($mId == 'faq' && $aId=='admin')),
    ),
    'htmlOptions' => array('class' => 'nav nav-tabs')
)); ?>

<?php echo $content; ?>

<?php $this->endContent(); ?>