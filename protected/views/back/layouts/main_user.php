<?php /* @var $this Controller */ ?>
<?php
$cId = Yii::app()->controller->id;
$aId = Yii::app()->controller->action->id;
$mId = (Yii::app()->controller->module) ? Yii::app()->controller->module->id : null;
?>

<?php $this->beginContent('//layouts/main'); ?>

<?php $this->widget('bootstrap.widgets.TbMenu', array(
    //'type' => 'pills',
    'stacked' => false,
    'items' => array(
        array('label' => Yii::t('user', 'Пользователи'), 'url' => array('/user/admin'), 'active' =>($cId == 'user') && ($aId == 'admin' || $aId == 'index' || $aId == 'view')),
//        array('label' => Yii::t('user', 'Рассылка'), 'url' => array('/newsletter/admin/index'), 'active' => ($mId == 'newsletter')),
//        array('label' => Yii::t('user', 'Фильтры'), 'url' => array('/filters/admin'), 'active' => ($cId == 'filters')),
    ),
    'htmlOptions' => array('class' => 'nav nav-tabs')
)); ?>

<?php echo $content; ?>

<?php $this->endContent(); ?>