<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language; ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <meta name="language" content="<?php echo Yii::app()->language; ?>" />
    <style>
        h1{ color: #1d5988; font-size:24px; font-weight:normal; font-family:Verdana, Arial, Helvetica, sans-serif; }
		a { color: #1d5988; }
    </style>
</head>

<body style=" background-color:#D80C8C;">


<table width="682" align="center" style=" border-collapse:collapse; border:none; position:relative;" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <a href="<?php echo Yii::app()->request->hostInfo; ?>" title="СтройЭкспертиза" ><img src="<?php echo Yii::app()->request->hostInfo; ?>/themes/stroy/front/images/logo.png" title="" alt="" width="220px" height="154px"  ></a>
        </td>
    </tr>
    <tr>
        <td style="background-color:#fff; font-family:Verdana, Arial, Helvetica, sans-serif; padding: 1px 25px 150px;">
            <?php echo $content; ?>
            <div style="position:absolute; bottom:10px; left:25px;"><p>Спасибо,<br />
                Команда СтройЭкспертиза</p>
				</div>
        </td>
    </tr>
</table>

</body>
</html>