﻿<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'region-form',
	'htmlOptions' => array(
        'class' => 'product'
    ),
    'enableAjaxValidation' => false,
)); ?>
          
<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textFieldRow($model, 'name', array('class' => 'span5 name full', 'maxlength' => 255)); ?>
</div>

<!--div id="product_categories">
    <select id="Catalog_categoryId" name="Catalog[categoryId]">
		<option value="">Выберите категорию</option>
	</select>
	
	<div class="link">
		<label for="Catalog_url" class="property required">Ссылка <span class="required">*</span></label>          <div class="page_url">/catalog/</div>
		<input type="text" id="Catalog_url" name="" maxlength="255" class="page_url"> 
	</div>
</div-->

<div id="column_left">
	<div class="block layer" style="width: auto; min-height: auto;">
	<h2>Параметры</h2>
	<?php //echo $form->dropDownListRow($model, 'parent_id', Menu::getItemOptions(Yii::t('admin', 'Нет')), array('encode' => false, 'class' => 'span5')); ?>

	<?php //echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

	<?php /*echo $form->dropDownListRow($model, 'parent_id',
					array('0' => 'Новый раздел')+CHtml::listData(Menu::model()->findAll(array('condition'=>"parent_id=0 and place='top'")),'id','name'),
				array('encode' => false, 'class' => 'span5')
			); */?>

	<?php
        if(isset($_GET['place']) && $_GET['place'])
            $model->place = $_GET['place'];

        echo $form->dropDownListRow($model, 'place', array(
					  'top'=>Yii::t('catalog','Верхнее меню'),
					  'footer1'=>Yii::t('catalog','Нижнее меню'),
					  ), 
				array('encode' => false, 'class' => 'span5'
				)); ?>
				
	<?php echo $form->textFieldRow($model, 'url', array('class' => 'span5', 'maxlength' => 255)); ?>

	<?php //echo $form->textFieldRow($model, 'lang', array('class' => 'span5', 'maxlength' => 2)); ?>

	<?php //echo $form->textFieldRow($model, 'order', array('class' => 'span5', 'maxlength' => 2)); ?>

	</div>
</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? Yii::t('admin', 'Создать') : Yii::t('admin', 'Сохранить'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => '',
        'htmlOptions' => array(
            'name' => 'more',
        ),
        'label' => Yii::t('admin', 'Сохранить и добавить еще'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
