﻿<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'region-form',
    'enableAjaxValidation' => false,
)); ?>
          
<?php echo $form->errorSummary($model); ?>

<?php echo $form->dropDownListRow($model, 'parent_id', Menu::getItemOptions(Yii::t('admin', 'Нет')), array('encode' => false, 'class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'url', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'lang', array('class' => 'span5', 'maxlength' => 2)); ?>

<?php echo $form->textFieldRow($model, 'order', array('class' => 'span5', 'maxlength' => 2)); ?>

<?php echo $form->dropDownListRow($model, 'place', array('up'=>'Верхнее','down'=>'Нижнее'), array('encode' => false, 'class' => 'span5')); ?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => Yii::t('admin', 'Создать'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => '',
        'htmlOptions' => array(
            'name' => 'more',
        ),
        'label' => Yii::t('admin', 'Сохранить и добавить еще'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
