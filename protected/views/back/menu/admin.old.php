<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Регионы')
);

Yii::app()->clientScript->registerScript('search', "
$('select#region').change(function() {
    $('.search-form form').submit();
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('region-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label' => Yii::t('admin', 'Добавить меню'),
    'type' => 'primary',
    'url' => array('/menu/create'),
)); ?>

<hr>

<div class="search-form">
    <?php /** @var $form TbActiveForm */ ?>
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>

    <?php echo $form->dropDownListRow($model, 'parent_id', Menu::getItemOptions(), array('id' => 'region', 'encode' => false, 'class' => 'span5')); ?>

    <?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'region-grid',
    'dataProvider' => $model->adminSearch(),
    'filter' => $model,
    'columns' => array(
        array(
			'name'=>'id',
			'htmlOptions'=>array(
				'width' => '40',
			),
		),
        array(
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'name' => 'name',
            'editable' => array(
                'url' => $this->createUrl('/menu/editable'),
                'placement' => 'right',
                'inputclass' => 'span3',
                'title' => Yii::t('admin', 'Введите название'),
                'success'   => 'js: function(data) {
                    if(typeof data == "object" && !data.success) return data.msg;
                }'
            )
        ),
		array(
            'name' => 'url',
        ),
        array(
            'class' => 'application.extensions.jtogglecolumn.JToggleColumn',
            'name' => 'approved',
            'filter' => array(0 =>  Misc::t('Нет'), 1 => Misc::t('Да')),
            'checkedButtonLabel' => Misc::t('Отменить подтверждение'),
            'uncheckedButtonLabel' => Misc::t('Подтвердить'),
			'htmlOptions'=>array(
				'width' => '60',
			),
        ),
		array(
            'class' => 'application.extensions.jtogglecolumn.JToggleColumn',
            'name' => 'place',
            'filter' => array('' =>  Misc::t('Все'),'up' =>  Misc::t('Верхнее'), 'down' => Misc::t('Нижнее')),
            'checkedButtonLabel' => Misc::t('Отменить подтверждение'),
            'uncheckedButtonLabel' => Misc::t('Подтвердить'),
			'htmlOptions'=>array(
				'width' => '60',
			),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => ' {update} {delete}'
        ),
    ),
)); ?>
