<?php
//$place = (!isset($_GET['place'])) ? 'top' : $_GET['place'];
$place = (!isset($_GET['place'])) ? 'footer1' : $_GET['place'];

switch($place){
//    case 'top':
//        $header = Yii::t('menu', 'Верхнее меню');
//        break;
    case 'footer1':
        $header = Yii::t('menu', 'Нижнее меню');
        break;
}
?>

<div class="page-content">
<!--    <div class="row two-columns">-->
        <!--<div class="span9">-->

            <div class="page-header admin-header inner-header">
                <h3><?php echo $header; ?></h3>
                <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => Yii::t('translate', 'Добавить пункт'),
                    'size' => 'small',
                    'url' => (!isset($_GET['place'])) ? array('/menu/create') : array('/menu/create', 'place' => $_GET['place'])
                ));
                ?>
            </div>

            <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
                'id' => 'region-grid',
                'type' => 'striped bordered',
                'dataProvider' => $model->adminSearch($place),
                //'filter' => $model,
                'enableSorting' => false,
                'sortableRows' => true,
                'sortableAjaxSave' => true,
                'sortableAttribute' => 'order',
                'sortableAction' => 'menu/sortable',
                'rowCssClassExpression' =>  '($data->parent_id) ? "child-r" : "parent-r"',
                'columns' => array(
                    array(
                        'class' => 'bootstrap.widgets.TbEditableColumn',
                        'name' => 'name',
                        'type'=>'raw',
                        'value' => '$data->name',
                        'editable' => array(
                                'url' => $this->createUrl('/menu/editable'),
                                'placement' => 'right',
                                'inputclass' => 'span4',
                                'title' => Yii::t('admin', 'Введите название пункта'),
                                'success' => 'js: function(data) {
                            if(typeof data == "object" && !data.success) return data.msg;
                        }'
                        ),

                    ),
                    array(
                        'name' => 'url',
                    ),
                    array(
                        'class' => 'bootstrap.widgets.TbButtonColumn',
                        'template' => ' {update} {delete}'
                    ),
                ),
            ));
            ?>
        <!--</div>-->
<!--        <div class="span3 sidebar">

            <div class="b-side first-block">
                <?php
//                $categories = array(
//                    array('label'=>Yii::t('menu','Верхнее меню'), 'url'=>array('/menu/admin', 'place' => 'top'), 'active' => (!isset($_GET['place'])) || (isset($_GET['place']) && ($_GET['place'] == 'top'))),
//                    array('label'=>Yii::t('menu','Нижнее меню'), 'url'=>array('/menu/admin', 'place' => 'footer1'), 'active' => (isset($_GET['place']) && ($_GET['place'] == 'footer1'))),
//                );

//                $this->widget('bootstrap.widgets.TbMenu', array(
//                    'type'=>'list',
//                    'items' => $categories
//                ));
                ?>
            </div>

        </div>-->
    <!--</div>-->
</div>