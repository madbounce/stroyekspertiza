<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Меню') => array('/menu/admin'),
    Yii::t('admin', 'Создание пункта меню')
);
?>

<div class="page-header">
    <h3><?php echo Yii::t('admin', 'Создание пункта меню'); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>