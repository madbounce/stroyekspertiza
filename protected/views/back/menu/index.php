<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Положение меню')
);

Yii::app()->clientScript->registerScript('search', "
$('select#region').change(function() {
    $('.search-form form').submit();
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('region-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label' => Yii::t('admin', 'Добавить положение меню'),
    'type' => 'primary',
    'url' => array('/menu/allcreate'),
)); ?>

<hr>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'region-grid',
    'dataProvider' => $model->adminSearch(),
    'filter' => $model,
    'columns' => array(
        array(
			'name'=>'id',
			'htmlOptions'=>array(
				'width' => '40',
			),
		),
        array(
            'name' => 'name',
        ),
		array(
            'class' => 'application.extensions.jtogglecolumn.JToggleColumn',
            'name' => 'place',
            'filter' => array('' =>  Misc::t('Все'),'up' =>  Misc::t('Верхнее'), 'down' => Misc::t('Нижнее')),
            'checkedButtonLabel' => Misc::t('Отменить подтверждение'),
            'uncheckedButtonLabel' => Misc::t('Подтвердить'),
			'htmlOptions'=>array(
				'width' => '60',
			),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => ' {update} {delete}'
        ),
    ),
)); ?>
