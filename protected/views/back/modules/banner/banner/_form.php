<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'banner-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype' => 'multipart/form-data'),
)); ?>
	<?php // echo $form->errorSummary($model); ?>

	<div class="line">
		<?php echo $form->labelEx($model,'categories'); ?>
		<?php echo $form->error($model,'categories'); ?>
	</div>

	<div class="line">
		<?php echo $form->checkBoxList($model, 'categoryIds', CHtml::listData(Category::model()->findAll(), 'id','title')); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('class'=>'medium', 'size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'size_id'); ?>
		<?php echo $form->dropDownList($model, 'size_id', CHtml::listData(Size::model()->findAll(), 'id', 'concatenedSizeTitle'), array('empty'=>'Выбрать...')); ?>
		<?php echo $form->error($model,'size_id'); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'image'); ?>
		<?php echo $form->fileField($model,'image'); ?>
		<?php 
			$this->widget('application.extensions.fancybox.EFancyBox', array(
				'target'=>'a[rel=banner]',
				'config'=>array(
					'titlePosition'=>'over',
					),
				)
			);
		?>
		<?php if(!$model->isNewRecord AND !empty($model->image)): ?>
			<a rel="banner" href="#<?php echo CHtml::encode($model->id); ?>">Показать баннер</a>
			<div style="display:none">
				<div id="<?php echo CHtml::encode($model->id); ?>">
					<?php if($model->type == 'application/x-shockwave-flash'): ?>
						<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,115,0" width="<?php echo $model->size->width; ?>" height="<?php echo $model->size->height; ?>">
							<param name="movie" value="<?php echo Yii::app()->baseUrl."/../images/banners/".$model->image ?>">
							<param name="quality" value="best">
							<embed name="cherepovetz" src="<?php echo Yii::app()->baseUrl."/../images/banners/".$model->image ?>" quality="best" width="<?php echo $model->size->width; ?>" height="<?php echo $model->size->height; ?>" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>
						</object>
					<?php else: ?>
						<img width="<?php echo $model->size->width; ?>" height="<?php echo $model->size->height; ?>" border=0 src="<?php echo Yii::app()->baseUrl."/../images/banners/".$model->image ?>">
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
		<?php echo $form->error($model,'image'); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'link'); ?>
		<?php echo $form->textField($model,'link',array('class'=>'medium', 'size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'link'); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'target'); ?>
		<?php echo $form->dropDownList($model, 'target', array('_blank'=>'_blank', '_self'=>'_self')); ?>
		<?php echo $form->error($model,'target'); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'start_date'); ?>
		<?php
			$model->start_date = empty($model->start_date) ? date("d.m.Y", time()) : date("d.m.Y", $model->start_date);
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'language'=>Yii::app()->getLanguage(),
				'model'=>$model,
				'attribute'=>'start_date',
				'value'=>$model->start_date,
				'options'=>array(
					'showAnim'=>'fold',
					'dateFormat'=>'dd.mm.yy',
				),
				'htmlOptions'=>array(
					'style'=>'height:20px;'
				),
			));
		?>
		<?php echo $form->error($model,'start_date'); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'end_date'); ?>
		<?php
			$model->end_date = empty($model->end_date) ? date("d.m.Y", time()) : date("d.m.Y", $model->end_date);
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'language'=>Yii::app()->getLanguage(),
				'model'=>$model,
				'attribute'=>'end_date',
				'value'=>$model->end_date,
				'options'=>array(
					'showAnim'=>'fold',
					'dateFormat'=>'dd.mm.yy',
				),
				'htmlOptions'=>array(
					'style'=>'height:20px;'
				),
			));
		?>
		<?php echo $form->error($model,'end_date'); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'show_limit'); ?>
		<?php echo $form->textField($model,'show_limit',array('class'=>'medium', 'size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'show_limit'); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'is_active'); ?>
		<?php echo $form->checkBox($model,'is_active'); ?>
		<?php echo $form->error($model,'is_active'); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'is_default'); ?>
		<?php echo $form->checkBox($model,'is_default'); ?>
		<?php echo $form->error($model,'is_default'); ?>
	</div>

	<div class="line">
		<p class="note">Поля отмеченные <span class="required">*</span> обязательны для заполнения.</p>
	</div>

	<div class="line button">
		<button type="submit" class="green big"><span><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></span></button>
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div>