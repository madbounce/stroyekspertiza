<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php // echo $form->errorSummary($model); ?>

	<div class="line">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('class'=>'medium', 'size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'page'); ?>
		<?php echo $form->textField($model,'page',array('class'=>'medium', 'size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'page'); ?>
	</div>

	<div class="line">
		<p class="note">Поля отмеченные <span class="required">*</span> обязательны для заполнения.</p>
	</div>

	<div class="line button">
		<button type="submit" class="green big"><span><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></span></button>
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div>