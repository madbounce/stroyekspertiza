<?php
$this->breadcrumbs=array(
	$this->module->nameModule=>array('/banner'),
	'Категории'=>array('admin'),
	'Управление',
);
?>
<div class="section">
	<div class="box">
	<div class="title">
		<h2>Управление "Баннеры"</h2>
	</div>
		<div class="content">
			<div class="button" style="float: left;padding-left: 20px;">
				<?php echo CHtml::link('Добавить категорию', array('/admin/banner/category/create'), $htmlOptions=array('class'=>'button green medium', 'style'=>'opacity: 0.7;')); ?>
			</div>
			<div style="clear: both;"></div>

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'banner-category-grid',
				'dataProvider'=>$model->search(),
//				'filter'=>$model,
				'itemsCssClass'=>'table',
				'columns'=>array(
					'id',
					'title',
					'page',
					array(
						'class'=>'CButtonColumn',
						'template'=>'{update}{delete}',
						'updateButtonImageUrl'=>$this->path.'gfx/icon-edit.png',
						'deleteButtonImageUrl'=>$this->path.'gfx/icon-delete.png',
					),
				),
			)); ?>
		</div>
	</div>
</div>