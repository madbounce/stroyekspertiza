<?php
$this->breadcrumbs=array(
	$this->module->nameModule=>array('/banner'),
	'Баннеры'=>array('admin'),
	'Добавить',
);
?>

<div class="section">
	<div class="box">
	<div class="title">
		<h2>Добавить баннер</h2>
	</div>
		<div class="content">
			<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
		</div>
	</div>
</div>