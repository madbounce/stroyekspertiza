<?php
$this->breadcrumbs=array(
	$this->module->nameModule,
);
?>
<div class="section">
	<div class="box">
	<div class="title">
		<h2>Управление "Баннеры"</h2>
	</div>
		<div class="content">
			<div class="button" style="float: left;padding-left: 20px;">
				<?php echo CHtml::link('Баннеры', array('/banner'), $htmlOptions=array('class'=>'button green medium', 'style'=>'opacity: 0.7;')); ?>
			</div>
			<div class="button" style="float: left;padding-left: 20px;">
				<?php echo CHtml::link('Категории', array('/admin/banner/category'), $htmlOptions=array('class'=>'button green medium', 'style'=>'opacity: 0.7;')); ?>
			</div>
			<div class="button" style="float: left;padding-left: 20px;">
				<?php echo CHtml::link('Размеры', array('/admin/banner/size'), $htmlOptions=array('class'=>'button green medium', 'style'=>'opacity: 0.7;')); ?>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>
</div>