<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'size-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php // echo $form->errorSummary($model); ?>

	<div class="line">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('class'=>'medium', 'size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'width'); ?>
		<?php echo $form->textField($model,'width',array('class'=>'medium')); ?>
		<?php echo $form->error($model,'width'); ?>
	</div>

	<div class="line">
		<?php echo $form->labelEx($model,'height'); ?>
		<?php echo $form->textField($model,'height',array('class'=>'medium')); ?>
		<?php echo $form->error($model,'height'); ?>
	</div>

	<div class="line">
		<p class="note">Поля отмеченные <span class="required">*</span> обязательны для заполнения.</p>
	</div>

	<div class="line button">
		<button type="submit" class="green big"><span><?php echo $model->isNewRecord ? 'Добавить' : 'Сохранить'; ?></span></button>
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div>