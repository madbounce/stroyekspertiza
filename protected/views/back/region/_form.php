﻿<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'region-form',
	'htmlOptions' => array(
        'class' => 'product'
    ),
    'enableAjaxValidation' => false,
)); ?>
          
<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textFieldRow($model, 'name', array('class' => 'span5 name full', 'maxlength' => 255)); ?>
</div>

<div id="column_left">
	<div class="block layer" style="width: auto; min-height: auto;">
	<h2>Параметры</h2>

	<?php //echo $form->dropDownListRow($model, 'parent_id', Region::getItemOptions(Yii::t('admin', 'Нет')), array('encode' => false, 'class' => 'span5')); ?>

	<?php //echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

	<?php echo $form->textFieldRow($model, 'url', array('class' => 'span5', 'maxlength' => 255)); ?>

	<?php //echo $form->textFieldRow($model, 'lang', array('class' => 'span5', 'maxlength' => 2)); ?>

	<?php //echo $form->textFieldRow($model, 'order', array('class' => 'span5', 'maxlength' => 2)); ?>

	<?php //echo $form->dropDownListRow($model, 'lr', array('0'=>'Нет','l'=>'left','r'=>'right'), array('encode' => false, 'class' => 'span5')); ?>

	</div>
</div>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
)); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => '',
        'htmlOptions' => array(
            'name' => 'more',
        ),
        'label' => Yii::t('admin', 'Сохранить и добавить еще'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
