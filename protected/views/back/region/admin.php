<?php
$dp = $model->adminSearch();
$countAll = $dp->totalItemCount;
?>
<div class="page-header admin-header">
    <h3><?php echo Yii::t('app', '{n} страна|{n} страны|{n} стран', $countAll);?></h3>
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'label' => Yii::t('translate', 'Добавить страну'),
        'size' => 'small',
        'url' => array('/region/create')
    ));
    ?>
</div>

<div class="page-content">
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'id' => 'region-grid',
    'type' => 'striped bordered',
    'dataProvider' => $dp,
    //'filter' => $model,
    'enableSorting' => false,
    'sortableRows' => true,
    'sortableAjaxSave' => true,
    'sortableAttribute' => 'order',
    'sortableAction' => 'region/sortable',
    'template' => '{pager}{items}{pager}',
    'rowCssClassExpression' =>  '($data->parent_id) ? "child-r" : "parent-r"',
    'columns' => array(
        array(
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'name' => 'name',
            'type'=>'raw',
            'value' => '$data->name',
            'editable' => array(
                'url' => $this->createUrl('/region/editable'),
                'placement' => 'right',
                'inputclass' => 'span4',
                'title' => Yii::t('admin', 'Введите имя страны'),
                'success' => 'js: function(data) {
                    if(typeof data == "object" && !data.success) return data.msg;
                }'
            ),

        ),
        array(
            'name' => 'url',
        ),
        array(
            'class' => 'bootstrap.widgets.TbToggleColumn',
            'name' => 'approved',
            'filter' => array(0 => Yii::t('admin', 'Нет'), 1 => Yii::t('admin', 'Да')),
            'checkedButtonLabel' => Yii::t('admin', 'Сделать неактивным'),
            'uncheckedButtonLabel' => Yii::t('admin', 'Сделать активным'),
            'toggleAction'=>'toggle',
            'htmlOptions' => array(
                'width' => '60',
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => ' {update} {delete}'
        ),
    ),
));
?>
</div>