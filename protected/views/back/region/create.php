<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Страны') => array('/region/admin'),
    Yii::t('admin', 'Создание страны')
);
?>

<div class="page-header">
    <h3><?php echo Yii::t('admin', 'Создание страны'); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>