<?php
$this->breadcrumbs = array(
    Yii::t('mail', 'Страны') => array('/region/admin'),
    Yii::t('mail', 'Редактирование страны'),
);
?>

<div class="page-header">
    <h3><?php echo Yii::t('mail', 'Редактирование страны :id', array(':id' => '#' . $model->id)); ?></h3>
</div>

<div class="page-content">
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>