<?php
if(($model->isNewRecord) && isset($_GET['city_id']))
    $model->city_id = (int) $_GET['city_id'];

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'shop-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
        'class' => 'product'
    ),
));
?>

<?php echo $form->errorSummary($model); ?>

<div id="name">
    <?php echo $form->textField($model, 'title', array('class' => 'name', 'maxlength' => 255)); ?>
    <div class="checkbox"></div>
</div>

  <div class="block layer" style="width: auto; min-height: auto;">

      <h2>Параметры магазина</h2>
      <ul>
          <li>
              <?php echo $form->labelEx($model, 'city_id', array('class' => 'property')); ?>
              <?php echo $form->dropDownList($model, 'city_id', CHtml::listData(DicCities::model()->findAll(), 'id','title'), array(
                  'encode' => false,
                  'class' => 'simpla_sel',
                  'empty' => Yii::t('app', 'Выберите город')
              )); ?>
          </li>
          <li>
              <?php echo $form->labelEx($model, 'address', array('class' => 'property')); ?>
              <?php echo $form->textField($model, 'address', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
          </li>
          <li>
              <?php echo $form->labelEx($model, 'latitude', array('class' => 'property')); ?>
              <?php echo $form->textField($model, 'latitude', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
              <?php echo $form->error($model, 'latitude'); ?>
          </li>
          <li>
              <?php echo $form->labelEx($model, 'longitude', array('class' => 'property')); ?>
              <?php echo $form->textField($model, 'longitude', array('class' => 'simpla_inp', 'maxlength' => 255)); ?>
              <?php echo $form->error($model, 'longitude'); ?>
          </li>
      </ul>

</div>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? Yii::t('config', 'Создать') : Yii::t('config', 'Сохранить'),
    ));
    ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => '',
        'htmlOptions' => array(
            'name' => 'more',
        ),
        'label' => Yii::t('admin', 'Сохранить и добавить еще'),
    ));
    ?>
</div>

<?php $this->endWidget(); ?>

