<?php
$dp = $model->adminSearch();
$countAll = $dp->totalItemCount;
$cityId = (!isset($_GET['city_id'])) ? null : $_GET['city_id'];
?>

<div class="page-content">
    <div class="row two-columns">
        <div class="span9">

            <div class="page-header admin-header inner-header">
                <h3><?php echo Yii::t('app', '{n} магазин|{n} магазина|{n} магазинов', $countAll);?></h3>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => Yii::t('catalog', 'Добавить магазин'),
                    'size' => 'small',
                    'url' => ($cityId) ? array('/shop/create', 'city_id' => $cityId) : array('/shop/create'),
                ));
                ?>
                <?php
                /*$this->widget('bootstrap.widgets.TbButton', array(
                    'label' => Yii::t('catalog', 'Экспорт'),
                    'size' => 'small',
                    'url' => array('/shop/export'),
                ));*/
                ?>

                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => Yii::t('catalog', 'Импорт'),
                    'size' => 'small',
                    'url' => array('/shop/import'),
                ));
                ?>
            </div>

            <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
                'id'=>'shop-grid',
                'type' => 'striped bordered',
                'dataProvider'=>$dp,
                //'filter' => $model,
                'enableSorting' => false,
                'sortableRows' => true,
                'sortableAjaxSave'=>true,
                'sortableAttribute'=>'sort',
                'sortableAction'=> '/shop/sortable',
                'template' => '{pager}{items}{pager}',
                'columns'=>array(
                    array(
                        'name' => 'title',
                        'type'=>'raw',
                        'value' => 'CHtml::link($data->title, array("/shop/update", "id" => $data->id))',
                        'htmlOptions' => array('encodeLabel'=>false),
                    ),
                    'address',
                    array(
                        'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template'=>'{delete}',
                        //'viewButtonUrl' => '"/catalog/default/view?url=" . $data->url',
                        //'viewButtonOptions' => array('target' => '_blank')

                    ),
                ),
            )); ?>
        </div>
        <div class="span3 sidebar">
            <div class="b-side">
                <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route), 'get', array('class' => 'search-form')); ?>

                <div class="input-append">
                    <?php echo CHtml::textField('search', isset($_GET['search']) ? $_GET['search'] : '', array('class' => 'input-medium search-input'))?>
                    <button class="btn" type="submit"><i class="icon-search"></i></button>
                </div>
                <?php echo CHtml::endForm(); ?>
            </div>

            <div class="b-side">
            <?php
            $categories = array(
                array('label'=>Yii::t('catalog','Все города'), 'url'=>array('/shop/admin'), 'active' => (!isset($_GET['city_id']))),
            );
            $cities = DicCities::all();
            if($cities){
                foreach($cities as $city)
                    array_push($categories, array(
                            'label' => $city->title,
                            'url' => array('/shop/admin', 'city_id' => $city->id),
                            'active' => (isset($_GET['city_id']) && ($_GET['city_id'] == $city->id))
                         )
                    );
            }

            $this->widget('bootstrap.widgets.TbMenu', array(
                'type'=>'list',
                'items' => $categories
            ));
            ?>
            </div>

        </div>
    </div>
</div>