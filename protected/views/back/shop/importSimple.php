<div class="page-header">
    <h3><?php echo Yii::t('shop', 'Импорт магазинов'); ?></h3>
</div>

<div class="page-content">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'shop-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'class' => 'product'
        ),
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php if (isset($messages)): ?>
    <div class="block layer" style="width: auto; min-height: auto;">

        <h2>Лог импорта</h2>
        <ul>
            <li>
                <?php echo $messages; ?>
            </li>
        </ul>

    </div>


    <?php endif; ?>

    <div id="name">
        <?php echo $form->fileField($model, 'file', array('class' => 'name', 'maxlength' => 255)); ?>
    </div>

    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' =>Yii::t('admin', 'Импортировать')
        ));
        ?>
    </div>
    <?php $this->endWidget(); ?>
</div>







