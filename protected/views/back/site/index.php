<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>

<?php $this->beginWidget('bootstrap.widgets.TbHeroUnit', array(
    'heading' => Yii::app()->name,
)); ?>

<p>
    <?php echo Misc::t('Добро пожаловать в панель управления СтройЭкспертиза.'); ?>
</p>

<p class="info">© <?php echo date('Y');?> Стройэкспертиза <br> Все права защищены</p>
<?php $this->endWidget(); ?>
