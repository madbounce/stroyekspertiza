<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var BootActiveForm $form  */

$this->pageTitle = Yii::app()->name . ' - ' . Misc::t('Вход');
$this->breadcrumbs = array(
    Misc::t('Вход'),
);
?>

<h1><?php echo Misc::t('Вход'); ?></h1>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'login-form',
    'htmlOptions' => array('class' => 'well'),
)); ?>

<?php echo $form->textFieldRow($model, 'username', array('class' => 'span3')); ?>
<?php echo $form->passwordFieldRow($model, 'password', array('class' => 'span3')); ?>
<?php echo $form->checkboxRow($model, 'rememberMe'); ?>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => Misc::t('Вход'))); ?>

<?php $this->endWidget(); ?>
