<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Тендеры')
);
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'tender-grid',
    'dataProvider' => $model->adminSearch(),
    'filter' => $model,
    'columns' => array(
        'id',
        'title',
        array(
            'header' => Yii::t('admin', 'Автор'),
            'value' => function($data) {
                if ($data->createdByCompany)
                    return $data->company->name;
                else
                    return $data->user->full_name;
            }
        ),
        array(
            'name' => 'public',
            'value' => function($data) {
                return $data->public ? Yii::t('admin', 'Да') : Yii::t('admin', 'Нет');
            },
            'filter' => array(0 => Yii::t('admin', 'Нет'), 1 => Yii::t('admin', 'Да')),
        ),
        array(
            'name' => 'canceled',
            'value' => function($data) {
                return $data->canceled ? Yii::t('admin', 'Да') : Yii::t('admin', 'Нет');
            },
            'filter' => array(0 => Yii::t('admin', 'Нет'), 1 => Yii::t('admin', 'Да')),
        ),
        array(
            'type' => 'raw',
            'name' => 'region_id',
            'value' => function($data) { return isset($data->region) ? $data->region->name : null; },
            'filter' => CHtml::activeDropDownList($model, 'region_id', Region::getItemOptions(), array('encode' => false)),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {delete}',
        ),
    ),
)); ?>
