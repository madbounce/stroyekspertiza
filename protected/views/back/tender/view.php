<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Тендеры') => array('admin'),
    '#' . $model->id,
);
?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'title',
        array(
            'label' => Yii::t('admin', 'Автор'),
            'value' => $model->createdByCompany ? $model->company->name : $model->user->full_name,
        ),
        array(
            'name' => 'public',
            'value' => $model->public ? Yii::t('admin', 'Да') : Yii::t('admin', 'Нет'),
        ),
        array(
            'name' => 'canceled',
            'value' => $model->canceled ? Yii::t('admin', 'Да') : Yii::t('admin', 'Нет'),
        ),
        'description',
        'finish_time',
        'end_time',
        array(
            'name' => 'cost',
            'value' => $model->formattedCost
        ),
        array(
            'name' => 'region_id',
            'value' => isset($model->region) ? $model->region->name : null,
        ),
    ),
)); ?>
