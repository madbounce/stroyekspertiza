<?php
$this->Widget('ext.highcharts.HighchartsWidget', array(
    'options'=>array(
        'chart'=> array(
            'renderTo'=> $container,
            'plotBackgroundColor'=> null,
            'plotBorderWidth'=> null,
            'plotShadow'=> false
        ),
        'title' => array('text' => $title),
        'tooltip'=>array(
            'formatter'=>'js:function() { return "<b>"+ this.point.name +"</b>: "+ Math.round(this.percentage*100)/100 +" %"; }'
        ),
        'lang' => array(
            'printButtonTitle' => Yii::t('app', 'Печать'),
            'exportButtonTitle' =>  Yii::t('app', 'Експорт изображения'),
            'loading' => Yii::t('app', 'Загрузка'),
            'resetZoom' =>  Yii::t('app', 'По умолчанию'),
        ),
        'plotOptions'=>array(
            'pie'=>array(
                'allowPointSelect'=> true,
                'cursor'=>'pointer',
                'dataLabels'=>array(
                    'enabled'=> true,
                    'color'=>'#000000',
                    'connectorColor'=>'#000000',
                    'formatter'=>'js:function() { return "<b>"+ this.point.name +"</b>:"+Math.round(this.percentage*100)/100 +" %"; }'
                )
            )
        ),

        'series' => array(
            array(
                'type'=>'pie',
                'data' => $data
            ),

        )
    )
));