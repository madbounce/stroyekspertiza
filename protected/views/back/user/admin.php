<?php
$dp = $model->adminSearch();
$countAll = $dp->totalItemCount;
?>


<div class="page-content">
    <div class="row two-columns">
        <div class="span9">

            <div class="page-header admin-header inner-header">
                <h3><?php echo Yii::t('app', '{n} пользователь|{n} пользователя|{n} пользователей', $countAll);?></h3>
                <?php
                /*$this->widget('bootstrap.widgets.TbButton', array(
                    'label' => Yii::t('translate', 'Экспорт'),
                    'size' => 'small',
                    'url' => array('export')
                ));*/
                ?>
                <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => Yii::t('translate', 'Экспорт'),
                    'size' => 'small',
                    'url' => array('export'),
                    'htmlOptions' => array(
                      'onClick' => new CJavaScriptExpression('$("#mydialog").dialog("open"); $("#sendurl").val($(this).attr("href")); return false'),  
                    ),
                ));
                ?>

                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'label' => Yii::t('catalog', 'Импорт'),
                    'size' => 'small',
                    'url' => array('/user/import'),
                ));
                ?>                
            </div>

            <?php $this->widget('bootstrap.widgets.TbGridView', array(
                'id' => 'user-grid',
                'type'=>'striped bordered',
                'dataProvider' => $dp,
                'filter' => $model,
                'template' => '{pager}{items}{pager}',
                'columns' => array(
                    array(
                        'name' => 'dateRegistered',
                        'filter' => false,
                        'value' => 'Yii::app()->dateFormatter->format("dd MMMM yyyy", $data->dateRegistered)',
                        'htmlOptions' => array('style' => 'width: 120px')
                    ),
                    array(
                        'name' => 'email',
                        'filter' => false,
                    ),
                    array(
                        'name' => 'full_name',
                        'filter' => false,
                        'header'=>'Имя Фамилия',
                    ),
                    array(
                        'class' => 'bootstrap.widgets.TbButtonColumn',
                        'template' => '{view} {delete}',
                    ),
                ),
            ));
            ?>
        </div>
        <div class="span3 sidebar">

            <div class="b-side first-block">
                <div class="well filter-block">
                    <?php echo CHtml::form(Yii::app()->createUrl($this->route), 'get', array('class' => 'filter-form'))?>
                    <?php echo CHtml::label('Регион', 'region'); ?>
                    <?php echo CHtml::dropDownList('region', isset($_GET['region']) ? $_GET['region'] : '', DicRegions::listAll(),
                        array(
                            'id' => 'region',
                            'empty' => '---',
                            'style' => 'width:170px'
                        ))
                    ?>
                    <?php echo CHtml::submitButton('Применить фильтр', array('class' => 'btn submit-filter'))?>
                    <?php echo CHtml::endForm();?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'mydialog',
    'options'=>array(
        'title'=>'Экспорт',
        'width' => 230,
        'autoOpen'=>false,
        'buttons' => array(
            array('text'=>'OK','click'=> "js:function(){"."window.location=$('#sendurl').val()+'?id='+$('#usertype').val()"."; $(this).dialog('close');}"),
            array('text'=>'Отмена','click'=> 'js:function(){$(this).dialog("close");}'),
        ),
        'modal' => true,
        'resizable' => false,
        'draggable' => false,
        'open' => 'js:function(event, ui) {
            $(".ui-widget-overlay").bind("click", function() { $("#mydialog").dialog("close"); });
         }',
        'position' => array('my' => 'center', 'at' => 'center', 'of' => '.page-header'),
    ),
));
echo CHtml::hiddenField('sendurl','', array('id' => 'sendurl'));
echo CHtml::dropDownList('usertype', 0, Newsletter::getUserTypeItemListForExport(), array('id' => 'usertype'));
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>