<?php
$this->breadcrumbs = array(
    Yii::t('admin', 'Пользователи')
);
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'user-grid',
    'dataProvider' => $model->adminSearch(),
    'filter' => $model,
    'columns' => array(
        'id',
        'email',
        'full_name',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {delete}',
        ),
    ),
)); ?>
