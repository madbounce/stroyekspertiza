<div class="page-header admin-header">
    <h3><?php echo Yii::t('user', 'Статистика'); ?></h3>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
    'label' => Yii::t('country', 'Создать фильтр из выбранного'),
    'size' => 'small',
    'url' => array('/backend/filters/create?'.preg_replace('/.+\?/', '', Yii::app()->request->url)),
));
    ?>     
</div>

<div class="page-content user-stat">
    <div class=" filter-stat" style="clear: both">
        <?php echo CHtml::form(Yii::app()->createUrl($this->route), 'get', array('class' => 'filter-form-stat'))?>
        <div class="raw">
            <div class="filter-raw">
                <?php echo CHtml::label('Регион', 'region'); ?>
                <?php echo CHtml::dropDownList('region', isset($_GET['region']) ? $_GET['region'] : '', DicRegions::listAll(),
                array(
                    'id' => 'region',
                    'empty' => '---',
                    'style' => 'width:170px'
                ))
                ?>
            </div>
            <div class="filter-raw">
                <?php echo CHtml::label('Город', 'city'); ?>
                <?php echo CHtml::textField('city', isset($_GET['city']) ? $_GET['city'] : '',
                array(
                    'id' => 'city',
                    'style' => 'width:170px'
                ));
                ?>
            </div>
            <div class="filter-raw">
                <?php echo CHtml::label('Возраст кошки', 'age'); ?>
                <?php echo CHtml::dropDownList('age', isset($_GET['age']) ? $_GET['age'] : '',
                    array(
                      1 => 'до года',
                      2 => 'от года до 7',
                      3 => 'больше 7'
                    ),
                array(
                    'id' => 'age',
                    'empty' => '---',
                    'style' => 'width:100px'
                ))
                ?>
            </div>
            <div class="filter-raw">
                <?php
                echo CHtml::label('Дата заполнения анкеты', 'dateFrom');
                echo CHtml::label('от', 'dateFrom', array('style' => 'display:inline'));
                $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                    'name'=>'dateFrom',
                    'value' => isset($_GET['dateFrom']) ? $_GET['dateFrom'] : '',
                    'language' => 'ru',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=>'dd-mm-yy',
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:20px; width:70px; display:inline'
                    ),
                ));
                ?>
                <?php
                echo CHtml::label('до', 'dateTo', array('style' => 'display:inline'));
                $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                    'name'=>'dateTo',
                    'value' => isset($_GET['dateTo']) ? $_GET['dateTo'] : '',
                    'language' => 'ru',
                    'options'=>array(
                        'showAnim'=>'fold',
                        'dateFormat'=>'dd-mm-yy',
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:20px; width:70px; display:inline'
                    ),
                ));
                ?>

            </div>
            <div style="clear: both; padding-top: 10px; font-style: italic;">Только для создания фильтра</div>
            <div class="filter-raw">
                <?php echo CHtml::label('Место покупки корма', 'foodbuy'); ?>
                <?php echo CHtml::dropDownList('petFoodBuyWhen', isset($_GET['petFoodBuyWhen']) ? $_GET['petFoodBuyWhen'] : '',
                Subscribers::getPetFoodBuyWhenList(),
                array(
                    'id' => 'foodbuy',
                    'empty' => '---',
                    'style' => 'width:100px'
                ));
                ?>
            </div>
            <div class="filter-raw">
                <?php echo CHtml::label('Кормят ли сухими Eukanuba', 'wetFoodFreq'); ?>
                <?php echo CHtml::dropDownList('wetFoodFreq', isset($_GET['wetFoodFreq']) ? $_GET['wetFoodFreq'] : '',
                Subscribers::getWetFoodFreqList(),
                array(
                    'id' => 'foodbuy',
                    'empty' => '---',
                    'style' => 'width:170px'
                ));
                ?>
            </div>
            <div class="filter-raw">
                <?php echo CHtml::label('Кормят ли влажными Eukanuba', 'dryFoodFreq'); ?>
                <?php echo CHtml::dropDownList('dryFoodFreq', isset($_GET['dryFoodFreq']) ? $_GET['dryFoodFreq'] : '',
                Subscribers::getDryFoodFreqList(),
                array(
                    'id' => 'foodbuy',
                    'empty' => '---',
                    'style' => 'width:170px'
                ));
                ?>
            </div>
            <div class="filter-raw">
                <?php echo CHtml::label('Какими конкурентами кормят', 'foodbuy'); ?>
                <?php echo CHtml::dropDownList('foodbuy', isset($_GET['foodbuy']) ? $_GET['foodbuy'] : '',
                Subscribers::getDryFoodBrandsList(),
                array(
                    'id' => 'foodbuy',
                    'empty' => '---',
                    'style' => 'width:100px'
                ));
                ?>
            </div>

            <div class="filter-raw button-filter-raw">
                <?php echo CHtml::submitButton('Применить', array('class' => 'btn submit-filter'))?>
            </div>
        </div>
        <?php echo CHtml::endForm();?>
   </div>

   <div class="charts" style="clear: both">
       <?php $data = $this->getDataChart(); ?>
       <h3><span id="count-all"><?php echo $data['countAll']?></span> <?php echo Yii::t('app', 'член клуба|члена клуба|членов клуба', array($data['countAll']));?></h3>
       <div id="chart-dry-food-freq" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
       <div id="chart-wet-food-freq" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
       <div id="chart-dry-food-brands" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
       <div id="chart-pet-food-by-when" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
       <?php

            $this->renderPartial('_chart', array('title' => 'Как часто вы кормите своего питомца сухими кормами Eukanuba?', 'data' => $data['dryFoodFreq'], 'container' => 'chart-dry-food-freq'));
            $this->renderPartial('_chart', array('title' => 'Как часто вы кормите своего питомца влажными кормами Eukanuba?', 'data' => $data['wetFoodFreq'], 'container' => 'chart-wet-food-freq'));
            $this->renderPartial('_chart', array('title' => 'Какие еще сухие корма вы даете своему питомцу?', 'data' => $data['dryFoodBrands'], 'container' => 'chart-dry-food-brands'));
            $this->renderPartial('_chart', array('title' => 'Где вы обычно покупаете корм для вашего питомца?', 'data' => $data['petFoodBuyWhen'], 'container' => 'chart-pet-food-by-when'));
       ?>
   </div>
</div>

