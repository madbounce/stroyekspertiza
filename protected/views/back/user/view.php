<div class="page-header">
    <h3 class="test22"><?php echo Yii::t('user', 'Просмотр пользователя'); ?></h3>
</div>

<div class="page-content">
	<div class="block layer" style="width: auto; min-height: auto;">
    <?php $this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        array(
            'name' => 'dateRegistered',
            'value' => Yii::app()->dateFormatter->format("dd MMMM yyyy", $model->dateRegistered),
        ),
        'email',
        array(
			'name' => 'full_name',
			'header'=>'Имя Фамилия',
		),
        'address1',
        'address2',
        'city',
        //'zip_code',
        array(
            'name' => 'd_birth',
            'value' => Yii::app()->dateFormatter->format("LLLL yyyy", $model->d_birth),
        ),
        array(
            'name' => 'region_id',
            'value' => isset($model->region) ? $model->region->title : null,
        ),
    ),
)); ?>
	</div>
</div>

