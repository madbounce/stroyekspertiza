<h2>Благодарим Вас за регистрацию на <?php echo CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')); ?></h2>
<div>
    Чтобы подтвердить Ваш e-mail перейдите по <?php echo CHtml::link(Misc::t('ссылке'), Yii::app()->createAbsoluteUrl('/auth/confirm', array('code' => $model->verify_code))); ?>.<br>
    После подтверждения Вы сможете авторизироватся на сайте с помощью введенных Вами логина и пароля.
</div>