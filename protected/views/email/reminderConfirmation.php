<h2>Восстановление пароля <?php echo CHtml::link(Yii::app()->params['siteName'], Yii::app()->createAbsoluteUrl('/site/index')); ?></h2>
<div>
    Чтобы сменить Ваш пароль перейдите по <?php echo CHtml::link(Misc::t('ссылке'), Yii::app()->createAbsoluteUrl('/auth/restorePassword', array('code' => $model->verify_code))); ?>.
</div>