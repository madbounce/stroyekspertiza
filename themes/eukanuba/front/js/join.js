jQuery(function($) {
    $('#hasPetfalse').click(function () {
        $('#dont-have-pets').show();
        $('#have-pets').hide();
    });

    $('#hasPettrue').click(function () {
        $('#dont-have-pets').hide();
        $('#have-pets').show();
    });

    $('#cat2').change(function () {
        var val = $(this).val();
        $('#addPet').hide();
        $('.pet-info-row').hide();
        for (i = 1; i <= val; i++) {
            $('#b-pet' + i).show();
        }
        if (val < 4)
            $('#addPet').show();
    });

    $('#addPet').click(function () {
        var val = $('#cat2').val();
        var newVal = parseInt(val) + 1;
        if (newVal <= 4) {
            $('#cat2').val(newVal);
            $('#cat2').trigger('change');
        }
        return false;
    });

    $('.remove-pet').click(function () {
        var val = $('#cat2').val();
        var newVal = parseInt(val) - 1;
        if (newVal >= 1) {
            $('#cat2').val(newVal);
            $('#cat2').trigger('change');
        }
        return false;
    });

    $('#submit_step2').click(function () {
        $('#step3-field').val(1);
        return true;
    });
});