<script type="text/javascript">
    $(function(){
        $("a.reply-link").click(function() {
            $(this).hide();
            $(this).next(".reply51").show();
            return false;
        });
    });
</script>

<div class="zayavka-comments">
    <?php $dataProvider = $model->getCommentDataProvider(); ?>
    <?php if (!empty($dataProvider->data)): ?>

        <h2><?php echo $commentsText; ?></h2>
        <?php $this->widget('CustomListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
            'viewData' => array('model' => $model, 'buttonText' => isset($buttonText) ? $buttonText : Yii::t('comment', 'Отправить'))
        )); ?>

    <?php elseif (isset($noCommentsText)): ?>
        <h2><?php echo $noCommentsText; ?></h2>
    <?php endif; ?>

    <?php if (!Yii::app()->user->isGuest): ?>
        <div class="block_otziv_add">
            <h2><?php echo $addCommentText; ?></h2>
            <?php $this->render('_form', array(
                'model' => $model->newComment(),
                'buttonText' => isset($buttonText) ? $buttonText : Yii::t('comment', 'Отправить'),
            )); ?>
        </div>
    <?php endif; ?>
</div>