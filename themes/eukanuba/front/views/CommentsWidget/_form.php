<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'comment-form',
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textArea($model, 'comment'); ?>

<?php echo $form->hiddenField($model, 'parent_id'); ?>

<?php echo CHtml::submitButton($buttonText, array('class' => 'but_send')); ?>

<?php $this->endWidget(); ?>