<div class="block_otziv parent">
    <div class="block_name">
        <div class="otziv_date"><?php echo Helper::formatCommentTime($data->create_time); ?></div>

        <a href="#">
            <?php $this->widget('AvatarWidget', array('model' => $data->user, 'width' => 49, 'height' => 49)); ?>
            <big><?php echo CHtml::encode($data->user->full_name); ?></big><br>
        </a>

        <div class="clear"></div>
    </div>

    <div class="block_content">
        <p><?php echo CHtml::encode($data->comment); ?></p>

        <?php foreach ($data->children as $child): ?>

        <div class="block_otziv">
            <div class="block_name">
                <div class="otziv_date"><?php echo Helper::formatCommentTime($child->create_time); ?></div>

                <a href="#">
                    <?php $this->widget('AvatarWidget', array('model' => $child->user, 'width' => 49, 'height' => 49)); ?>
                    <big><?php echo CHtml::encode($child->user->full_name); ?></big><br>
                </a>

                <div class="clear"></div>
            </div>

            <div class="block_content">
                <p><?php echo CHtml::encode($child->comment); ?></p>
            </div>
        </div>

        <?php endforeach; ?>

        <?php echo CHtml::link(Yii::t('comment', 'ответить'), '#', array('class' => 'reply-link')); ?>

        <div class="reply51" style="display: none;">
            <?php $this->render('_form', array(
                'model' => $model->newComment($data),
                'buttonText' => isset($buttonText) ? $buttonText : Yii::t('comment', 'Отправить'),
            )); ?>
        </div>
    </div>
</div>