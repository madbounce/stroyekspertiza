<?php $this->render('_comments', array(
    'model' => $model,
    'noCommentsText' => Yii::t('comment', 'Отзывов пока нет'),
    'commentsText' => Yii::t('comment', 'Отзывы о заявке'),
    'addCommentText' => Yii::t('comment', 'Оставить отзыв'),
)); ?>