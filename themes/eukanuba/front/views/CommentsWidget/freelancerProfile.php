<?php $this->render('_comments', array(
    'model' => $model,
    'noCommentsText' => Yii::t('comment', 'Отзывов пока нет'),
    'commentsText' => Yii::t('comment', 'Отзывы о фрилансере'),
    'addCommentText' => Yii::t('comment', 'Отзыв о фрилансере'),
    'noCommentsText' => Yii::t('comment', 'Отзывов нет'),
)); ?>