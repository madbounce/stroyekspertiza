<?php $this->render('_comments', array(
    'model' => $model,
    'noCommentsText' => Yii::t('comment', 'Комментариев пока нет'),
    'commentsText' => Yii::t('comment', 'Обсуждение новости'),
    'addCommentText' => Yii::t('comment', 'Оставить комментарий'),
)); ?>