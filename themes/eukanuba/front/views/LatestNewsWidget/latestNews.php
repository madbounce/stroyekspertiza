<div class="last_news">
    <h1><?php echo Yii::t('news', 'Последние новости'); ?></h1>

    <?php foreach ($items as $item): ?>
        <div class="news_date"><?php echo Yii::app()->dateFormatter->format('dd MMMM', $item->create_time); ?></div>
        <div class="news_title"><?php echo CHtml::link(CHtml::encode($item->title), array('/news/default/view', 'id' => $item->id)); ?></div>
    <?php endforeach; ?>
</div>