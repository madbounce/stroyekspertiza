<?php
/**
 * Created by JetBrains PhpStorm.
 * User: slavik
 * Date: 19.11.13
 * Time: 11:42
 * To change this template use File | Settings | File Templates.
 */
?>

    <div class="header_right_menu">
        <ul>
            <li><a href="/articles/default/puppy">СТАТЬИ</a></li>
            <li><a href="/page/video">ВИДЕО О ВОСПИТАНИИ</a></li>
            <li><a href="/products/dog/puppy">ПОДБОР КОРМА</a></li>
            <li><a href="/page/nutrition">ПИТАНИЕ ЩЕНКОВ</a></li>
            <li><a href="/page/shoppinglist">ЛИСТ ПОКУПОК</a></li>
            <li><a href="/page/dog_breed_selector" class="active">НАЙДИТЕ СВОЮ ИДЕАЛЬНУЮ ПОРОДУ</a></li>
            <li><a href="/page/breedersrecommended">РЕКОМЕНДАЦИИ ЗАВОДЧИКОВ</a></li>
        </ul>
    </div>