<div class="header_image articles">
    <div class="content_header">
        <div class="text_header">
            <h3>СТАТЬИ О ЩЕНКАХ</h3>
            <p>УХОД, ВОСПИТАНИЕ, ЗДОРОВЬЕ, ПИТАНИЕ...</p>
        </div>

        <div class="header_right_menu">
            <ul>
                <li><a href="/articles/default/puppy">СТАТЬИ</a></li>
                <li><a href="/page/video">ВИДЕО О ВОСПИТАНИИ</a></li>
                <li><a href="/products/dog/puppy">ПОДБОР КОРМА</a></li>
                <li><a href="/page/nutrition">ПИТАНИЕ ЩЕНКОВ</a></li>
                <li><a href="/page/shoppinglist">ЛИСТ ПОКУПОК</a></li>
                <li><a href="/page/dog_breed_selector" class="active">НАЙДИТЕ СВОЮ ИДЕАЛЬНУЮ ПОРОДУ</a></li>
                <li><a href="/page/breedersrecommended">РЕКОМЕНДАЦИИ ЗАВОДЧИКОВ</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="content_iner">
    <h2 class="main_title second">ПРЕВОСХОДНАЯ СОБАКА НАЧИНАЕТСЯ С ХОРОШЕГО ХОЗЯИНА <br/>
        В данном руководстве вы найдете советы и рекомендации по уходу за щенками и собаками.
        Ваша собака отблагодарит вас за заботу <br/> преданностью и здоровьем.
    </h2>

    <h3 class="lines">
        ПИТАНИЕ
    </h3>

    <ul class="list_articles">
        <li>
            10 причин, чтобы выбрать корм Eukanuba для щенков
            <a href="#">>></a>
        </li>

        <li>
            Полезная альтернатива лакомству
            <a href="#">>></a>
        </li>
    </ul>

    <h3 class="lines">
        ЗДОРОВЬЕ
    </h3>

    <ul class="list_articles">
        <li>
            Неотъемлемая часть развития щенка
            <a href="#">>></a>
        </li>

        <li>
            Визит к ветеринару
            <a href="#">>></a>
        </li>

        <li>
            10 главных советов по уходу за щенком
            <a href="#">>></a>
        </li>

        <li>
            Забота о здоровье щенка
            <a href="#">>></a>
        </li>

        <li>
            Медицинская книжка щенка
            <a href="#">>></a>
        </li>

        <li>
            Диаграмма развития щенка
            <a href="#">>></a>
        </li>

        <li>
            Здоровье вашей собаки: почему так важен уход за шерстью
            <a href="#">>></a>
        </li>

        <li>
            Жизненно важные системы щенка
            <a href="#">>></a>
        </li>
    </ul>

    <h3 class="lines">
        ОБУЧЕНИЕ
    </h3>

    <ul class="list_articles">
        <li>
            С чего начать
            <a href="#">>></a>
        </li>

        <li>
            35 способов сделать щенка счастливым
            <a href="#">>></a>
        </li>

        <li>
            Забота о щенке – нагрузки и здоровые суставы
            <a href="#">>></a>
        </li>
    </ul>

    <div class="clear"></div>
</div>