<?
$this->breadcrumbs=array(
		'Статьи о кошках'
    );
?>

<?php //Yii::app()->clientScript->registerMetaTag($model->meta_description,'description');?>


<script type="text/javascript">
    function printPage() 
    {
        //var href = location.href;
        var mywindow = window.open('/en_uk/jsp/pet_health/print-view.jsp', 'my_div', 'height=600,width=900,scrollbars=yes');
        mywindow.print();
    }
</script>

<div class="bigDog cat">
        	<h1>РЕКОМЕНДАЦИИ И<br /> СОВЕТЫ <br /><small>ПО УХОДУ ЗА КОШКАМИ</small></h1>
            <div class="article"><a href="/article/articles/dogs">Статьи о собаках </a></div>
        </div>
<div class="middle DentaDefense cat">

            <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 15,'view'=>'textClickable')); ?>

            <div class="rightSide">
            	<div class="theBest">
                	<h3>ЗАМЕЧАТЕЛЬНАЯ КОШКА <br />НАЧИНАЕТСЯ С ЗАМЕЧАТЕЛЬНОГО ХОЗЯИНА.</h3>
                    В данном разделе вы можете найти рекомендации и советы по уходу за котятами и кошками. Ваша кошка отблагодарит вас долгими годами здоровой жизни, полной любви и преданности.
                </div>
            	<form class="chooseSearch">
                    <div class="searchDog">
                    	<input type="text" onblur="this.value=(this.value=='')?this.title:this.value;" onfocus="this.value=(this.value==this.title)?'':this.value;" value="Найти о кошках..." title="Найти о кошках..."/>
                        <input type="submit" value="ПОИСК"/>
                    </div>
                </form>
                <? $cats = ArticleCategory::model()->findAll('parent_id=31');?>
                <div class="catsDiv">
                	<div class="leftCat dog3">
                    	<img src="<?=Yii::app()->theme->baseUrl;?>/images/cat1.png" />
                        <h4>Статьи о котятах</h4>
                        <? foreach($cats as $cat):?>
                        <a href="<?=Yii::app()->createUrl('/article/articles/',array('type'=>'kittens','id'=>$cat->id))?>"><?=$cat->title?></a> <br />
                        <? endforeach?>

                    </div>
                    <div class="rightCat dog3">
                    	<img src="<?=Yii::app()->theme->baseUrl;?>/images/cat2.png" />
                        <h4>Статьи о кошках</h4>
                        <? foreach($cats as $cat):?>
                        <a href="<?=Yii::app()->createUrl('/article/articles/',array('type'=>'cats','id'=>$cat->id))?>"><?=$cat->title?></a> <br />
                        <? endforeach?>

                    </div>
                </div>
            </div>
        </div>