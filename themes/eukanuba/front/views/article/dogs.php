<?
$this->breadcrumbs=array(
        'Статьи о собаках'
    );
?>
<?php //Yii::app()->clientScript->registerMetaTag($model->meta_description,'description');?>


<script type="text/javascript">
    function printPage() 
    {
        //var href = location.href;
        var mywindow = window.open('/en_uk/jsp/pet_health/print-view.jsp', 'my_div', 'height=600,width=900,scrollbars=yes');
        mywindow.print();
    }
</script>
    <div class="bigDog dog3">
        <h1>РЕКОМЕНДАЦИИ И СОВЕТЫ <br /><small>ПО УХОДУ ЗА СОБАКАМИ</small></h1>
        <div class="article"><a href="/article/articles/cats">Статьи о кошках </a></div>
    </div>

    <div class="middle DentaDefense cat">

        <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 15,'view'=>'textClickable')); ?>

        <div class="rightSide">
            <div class="theBest">
                <h3>ЗАМЕЧАТЕЛЬНАЯ СОБАКА <br />НАЧИНАЕТСЯ С ЗАМЕЧАТЕЛЬНОГО ХОЗЯИНА</h3>
                В данном разделе вы можете найти рекомендации и советы по уходу за щенками и собаками. Ваша собака отблагодарит вас долгими годами здоровой жизни, полной любви и преданности.
            </div>
            <form class="chooseSearch">
                <div class="searchDog">
                    <input type="text" onblur="this.value=(this.value=='')?this.title:this.value;" onfocus="this.value=(this.value==this.title)?'':this.value;" value="Найти о собаках..." title="Найти о собаках..."/>
                    <input type="submit" value="ПОИСК"/>
                </div>
            </form>
            <?
                $cats1 = ArticleCategory::model()->findAll('parent_id=24');
                $cats2 = ArticleCategory::model()->findAll('parent_id=31');
            ?>
            <div class="catsDiv">
                <div class="leftCat dog3">
                    <img src="<?=Yii::app()->theme->baseUrl;?>/images/dog1.png" />
                    <h4>О щенках</h4>
                   
                </div>
                <div class="rightCat dog3">
                    <img src="<?=Yii::app()->theme->baseUrl;?>/images/dog2.png" />
                    <h4>О cобаках </h4>

                </div>
            </div>
        </div>
    </div>