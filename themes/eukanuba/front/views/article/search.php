<form class="chooseSearch" action="/article/search/<?=$_GET['type']?>" method="post">
    <h3>Narrow Results</h3>
    <div class="choose">
        <div class="VertLine1">
            <p>Выберите категорию(и):</p>
            <? $categoris = ArticleCategory::model()->findAll();?>
            <? foreach($categoris as $category):?>
            <span class="lev"><input type="checkbox" name="categories[]" value="<?=$category->id?>" class="selectCategory"/> <span><?=$category->title?></span></span>
            <? endforeach?>
        </div>
        <div class="VertLine2">
            <p>Выберите возраст(ы):</p>
            <span><input type="checkbox" name="age[]" value="2" class="selectCategory" /> Взрослый (1-6)</span>
            <span><input type="checkbox" name="age[]" value="1" class="selectCategory"  /> <?=$_GET['type'] == 'pets' ? 'Щенок' : 'Котенок'?></span>
        </div>
        <div class="VertLine3">
            <p>Выберите размер(ы) упаковки:</p>
            <span><input type="checkbox" name="size[]" value="2" class="selectCategory" /> Средний</span>
            <span><input type="checkbox" name="size[]" value="1" class="selectCategory" /> Малый</span>
            <span><input type="checkbox" name="size[]" value="3" class="selectCategory" /> Большой/ Гигантский</span>
        </div>
    </div>
    <div class="searchDog">
        <? $searchPhrase = $_GET['type'] == 'pets' ? 'Статьи о собаках...':'Статьи о кошках...'?>
        <input type="text" onblur="this.value=(this.value=='')?this.title:this.value;" onfocus="this.value=(this.value==this.title)?'':this.value;" value="<?=$searchPhrase?>" title="<?=$searchPhrase?>"/>
        <input type="submit" value="SEARCH"/>
    </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    $('.selectCategory').click(function(){
		$.ajax({
			url  : $('form.chooseSearch').attr('action'),
			data : $('form.chooseSearch').serialize(),
			type : 'GET',
			}).success(function ( data ) {
			  $('#listArticles').html(data);
			});
	});
});
</script>