
<?php Yii::app()->clientScript->registerMetaTag($model->meta_description,'description');?>


<script type="text/javascript">
    function printPage() 
    {
        //var href = location.href;
        var mywindow = window.open('/en_uk/jsp/pet_health/print-view.jsp', 'my_div', 'height=600,width=900,scrollbars=yes');
        mywindow.print();
    }
</script>

<div class="middle DentaDefense cat">
	<div class="leftSide">
            	<img src="<?=Yii::app()->theme->baseUrl;?>/images/twoDOgs.png" class="twoDogs"/>
                <div class="related">
                	<p>Related Articles</p>
                    <ul>
                    	<li>Water: an Essential Nutrient</li>
                        <li>The Influence of Diet on the Puppy’s Developing Immune  System</li>
                        <li>Breed-and Size-specific Nutrition</li>
                        <li>Care and Feeding of a<br /> Lactating Bitch
</li>
                    </ul>
                </div>        
            </div>
            
	<div id="white-container">

		<div class="copy-top-border">
			<img
				src="<?=Yii::app()->theme->baseUrl;?>/images/top-border-dog.png"
				alt="" width="956" height="8" class="topborder" />
		</div>
		<div id="bodycontent_area">
			
			<div class="clear-all">&nbsp;</div>
				
<div id="breadcrumb"><a title = 'Home' href='/'>Home</a> / Multi-Cat Households</div>
			<div class="clear-all">&nbsp;</div>
			<div class="_col left-callouts-nav">
				<p class="green_curve">
					<img src='<?=Yii::app()->theme->baseUrl;?>/images/img_cat_detail.jpg'
						alt="Multi-Cat Households" title="Multi-Cat Households" width="241"
						height="351" />
				</p>
				
					
						
						<div class="yellow-box">
		<div><p><strong>Related Article</strong></p>
			<ul>
			<li><a href="/pet-health/cat-article/the-importance-of-protein-fat-and-fibre" title="The Importance of Protein, Fat and Fiber">The Importance of Protein, Fat and Fiber</a></li>
			
			<li><a href="/pet-health/cat-article/chicken-the-complete-protein-source-for-your-cat" title="Chicken -- The Complete Protein Source for Your Cat">Chicken -- The Complete Protein Source for Your Cat</a></li>
			
			<li><a href="/pet-health/cat-article/healthy-skin-and-coat-for-your-cat" title="Health Healthy Skin and Coat for Your Cat">Health Healthy Skin and Coat for Your Cat</a></li>
			
			<li><a href="/pet-health/cat-article/what-your-cats-trying-to-tell-you" title="What Your Cat's Trying to Tell You">What Your Cat's Trying to Tell You</a></li>
			</ul>   
		 </div>
	  </div>


					
				
					
				
			</div>
			<div class="_col txt-content">
				<h2 class="heading">
					Multi-Cat Households
<br/>
<small><?=$model->title;?></small>
				</h2>
				<div id="printDiv">
					<div class="print-page">
						<a href="javascript:printPage()" title="Print"><img
							src="http://media.iams.co.uk/en_uk/data_root/_images/global/print_btn.gif"
							alt="Print" title="Print"/> </a> <img
							src="http://media.iams.co.uk/en_uk/data_root/_images/global/print_this_page.gif"
							class="print-tooltip" alt="Print" title="Print"/>
					</div>
				</div> 
				
				<?=$model->content;?>
<a href="#top" class="returntop" title="Return to Top">Return to Top</a>
				<div class="clear-all">&nbsp;</div>
				
			</div>
			<div class="clear-all">&nbsp;</div>
		</div>
		<!-- Copy Content end-->
	</div>
</div>
