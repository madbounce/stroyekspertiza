<?php
$this->breadcrumbs=array(
    Misc::t('Уход') => array('/article/articles'),
    $model->title
);
$this->breadcrumbs_class .= '';
?>

<script type="text/javascript">
    function printPage() 
    {
        //var href = location.href;
        var mywindow = window.open('/en_uk/jsp/pet_health/print-view.jsp', 'my_div', 'height=600,width=900,scrollbars=yes');
        mywindow.print();
    }
</script>

<div class="middle DentaDefense cat">
        	<div class="leftSide sideBoxView">
            	<img src="<?=Yii::app()->theme->baseUrl;?>/images/twoDOgs.png" class="twoDogs"/>
                <div class="related">
                	<p><?=Misc::t('Похожие статьи');?></p>
                    <? $articles = Articles::model()->findAll(array(
							'condition'=>'material_category_id='.$model->material_category_id.' and id<>'.$model->id,
							'order'=>'update_time DESC',
							'limit'=>5,
							)
						); ?>
                    <ul>
                    <? foreach ($articles as $article): ?>
                    	<li><a href="/article/view/id/<?=$article->slug;?>" title="<?=$article->title?>">
                <?=$article->title?>
                </a></li>
					<? endforeach; ?>
                    </ul>
                </div>        
            </div>
            <div class="rightSide">
            	<div class="theBest petDog">
                	<h3><?=$model->title;?></h3>
                    <div>
                    <?=$model->content;?><br />
                    </div>
                </div>
            </div>
        </div>