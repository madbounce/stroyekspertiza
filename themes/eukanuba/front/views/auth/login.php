<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Misc::t('Вход');
$this->breadcrumbs = array(
    Misc::t('Вход'),
);
?>

<div class="main-cont-wrapper">
    <h1 class="reg-title"><?php echo Misc::t('Вход'); ?></h1>

    <div class="login_form">
        <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'action' => array('/auth/login'),
        'htmlOptions' => array('class' => 'form_auth'),
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'afterValidate'=>"js:function(form, data, hasError) {
                if (hasError) {
                    $(form).parent().parent().find('.error_message').show();
                    return false;
                } else {
                    $(form).parent().parent().find('.error_message').hide();
                    return true;
                }
            }",
        )
    )); ?>
        <div class="form-line">
            <?php echo CHtml::label(Misc::t('Ваш e-mail или логин'), CHtml::activeId($model, 'username')); ?>
            <span class="form-field">
            <?php echo $form->emailField($model, 'username', array(
                'tabindex' => 0,
                'class' => 'text_input'
            )); ?>
            </span>
            <?php echo $form->error($model, 'username', array('hideErrorMessage' => true)); ?>
        </div>
        <div class="form-line">
            <?php echo $form->label($model, 'password'); ?>
            <?php //echo CHtml::label(Misc::t('Пароль'), CHtml::activeId($model, 'password')); ?>
            <span class="form-field">
            <?php echo $form->passwordField($model, 'password', array(
             
                'tabindex' => 0,
                'class' => 'text_input'
            )); ?>
            </span>
            <?php echo $form->error($model, 'password', array('hideErrorMessage' => true)); ?>
        </div>
        <div class="form-line remember_me_check">
            <?php echo $form->checkBox($model, 'rememberMe');?>
            <span><?php echo $form->labelEx($model, 'rememberMe'); ?></span>
        </div>

        <?php echo CHtml::link(Misc::t('Забыли свой пароль?'), array('/auth/reminder'), array('class' => 'forgot_link', 'id' => 'dialog-recovery')); ?>
        <a href="#" class="help_link"><?php echo Misc::t('Помощь')?></a>
        <?php echo CHtml::submitButton(Misc::t('Войти'), array('class' => 'login_btn')); ?>
        <?php $this->endWidget(); ?>
    </div>
    <div class="right_dialog_block">
        <p><?php echo Misc::t('Вы также можете использовать для входа такие учетные записи'); ?>:</p>
        <?php $this->widget('LoginzaWidget'); ?>
    </div>
</div>
