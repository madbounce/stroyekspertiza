<?php
/* @var $this BidController */
/* @var $model Bid */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'bid-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>

<p>
    <?php echo $form->labelEx($model, 'cost'); ?><br/>
    <?php echo $form->textField($model, 'cost', array('size' => 16, 'maxlength' => 16, 'class' => 'input200')); ?>
    <?php echo $form->dropDownList($model, 'currency', Helper::getCurrencies()); ?>
    <?php echo $form->error($model, 'cost'); ?>
</p>

<p>
    <?php echo $form->labelEx($model, 'term'); ?><br/>
    <?php echo $form->textField($model, 'term', array('size' => 10, 'maxlength' => 10, 'class' => 'input200')); ?>
    <?php echo $form->error($model, 'term'); ?>
</p>

<p>
    <?php echo $form->labelEx($model, 'phone'); ?><br/>
    <?php echo $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 255, 'class' => 'input200')); ?>
    <?php echo $form->error($model, 'phone'); ?>
</p>

<p>
    <?php echo $form->labelEx($model, 'email'); ?><br/>
    <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 255, 'class' => 'input200')); ?>
    <?php echo $form->error($model, 'email'); ?>
</p>

<p>
    <?php $this->widget('filesaver.components.EAjaxUpload', array(
    'files' => $model->isNewRecord ? false : $model->getFilesSaver(),
    'id' => 'uploadFile',
    'config' => array(
        //'multiple'=>true,
        'allowedExtensions' => array("png", "jpg", "jpeg", "gif", "mov", "doc", "docx", "xls", "xlsx", "odt", "ppt", "pptx", "rar", "zip", "ai", "eps", "psd", "svg", "pdf", "txt"),
        'sizeLimit' => 200 * 1024 * 1024,
        'minSizeLimit' => 4,
    )
)); ?>
</p>

<div class="clear"></div><br>

<h2><?php echo $form->labelEx($model, 'comment'); ?></h2>
<?php echo $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 50, 'class' => 'textarea700')); ?>
<?php echo $form->error($model, 'comment'); ?>

<div class="clear"></div>
<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('tender', 'Подать заявку') : Yii::t('tender', 'Изменить заявку'), array('class' => 'but_zayavka')); ?>

<?php $this->endWidget(); ?>