<?php
/* @var $this BidController */
/* @var $model Bid */
?>

<?php $this->pageTitle = Yii::t('tender', 'Редактирование заявки на тендер'); ?>

<h2 class="title_zayavka"><big><?php echo Yii::t('tender', 'Редактирование заявки на тендер'); ?></big><br>
    <?php echo CHtml::encode($model->tender->title); ?>
</h2>

<?php $this->renderPartial('_form', array('model' => $model)); ?>