<?php
/* @var $this BidController */
/* @var $model Bid */

$this->pageTitle = Yii::t('tender', 'Заявка № :id', array(':id' => $model->id));
?>

<div class="mainleft">
    <div class="headerblock">
        <h2 class="title_zayavka n1"><?php echo Yii::t('tender', 'Заявка № :id', array(':id' => $model->id)); ?></h2>
        <div class="clear"></div>
        <span class="small">
            <?php echo Yii::t('tender', 'Последнее обновление: :time', array(
                ':time' => Yii::app()->dateFormatter->formatDateTime(!empty($model->update_time) ? $model->update_time : $model->create_time)
            )); ?>
        </span>
    </div>
    <div class="clear"></div>
    <div class="zayavkablock">
        <p class="grey1">
            <?php echo Yii::t('tender', 'К тендеру: :tender', array(':tender' => CHtml::link(CHtml::encode($model->tender->title), array('/tender/view', 'id' => $model->tender->id)))); ?>
        </p>
        <div class="zayavkatb">
            <div class="line1"><?php echo Yii::t('tender', 'Срок выполнения'); ?>: <span><?php echo Yii::t('tender', '{n} рабочий день|{n} рабочих дня|{n} рабочих дней|{n} рабочих дня', $model->term); ?></span>
            </div>
            <div class="line1"><?php echo Yii::t('tender', 'Бюджет: :cost', array(':cost' => '<span>' . $model->formattedCost . '</span>')); ?></div>
        </div>
        <h2><?php echo Yii::t('tender', 'Сообщение'); ?></h2>
        <p class="just">
            <?php echo CHtml::encode($model->comment); ?>
        </p>
    </div>

    <div class="fileswrap">
        <?php $this->widget('filesaver.components.EAjaxUpload', array(
            'files'=>$model->getFilesSaver(),
            'mode' => 'show',
        )); ?>
    </div>

    <div class="buttonswrap">
        <?php if (Yii::app()->user->id == $model->tender->user->id): ?>
            <?php if (Yii::app()->user->checkAccess('cancelBid', array('bid' => $model))): ?>
                <?php echo CHtml::link(Yii::t('tender', 'Отклонить заявку'), array('/bid/cancel', 'id' => $model->id), array('class' => 'otklonit')); ?>
            <?php endif; ?>
            <?php if (Yii::app()->user->checkAccess('acceptBid', array('bid' => $model))): ?>
                <?php echo CHtml::link(Yii::t('tender', 'Назначить победителем'), array('/bid/accept', 'id' => $model->id), array('class' => 'winner')); ?>
            <?php endif; ?>
        <?php else: ?>
            <?php if (Yii::app()->user->checkAccess('updateBid', array('bid' => $model))): ?>
                <?php echo CHtml::link(Yii::t('tender', 'Редактировать заявку'), array('/bid/update', 'id' => $model->id), array('class' => 'edit')); ?>
                <?php echo CHtml::link(Yii::t('tender', 'Отозвать заявку'), array('/bid/cancel', 'id' => $model->id), array('class' => 'otklonit')); ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <div class="comments">
        <?php $this->widget('comment.components.widgets.CommentsWidget',array(
            'model' => $model,
        )); ?>
    </div>
</div>
<div class="mainright">
    <?php $this->widget('AuthorWidget', array('model' => $model->user)); ?>
</div>

</div>
