<?php $menus = Menu::model()->getMenuItems('footer1'); ?>

<div id="footer">
    <?php if($menus):?>
    <ul>
        <?php foreach ($menus as $menu): ?>
        <li id="<?php echo $menu->alias;?>" class="">
            <a href="<?=$menu->url;?>" class="" title="<?=$menu->name;?>" target="_blank"><?=$menu->name;?></a>
        </li>
        <?php endforeach; ?>

    </ul>
    <?php endif; ?>
    
    <div class="copyright">Линия помощи: 8-800-555-15-88 (Звонок бесплатный по всей России)</div> 
    
	<div class="copyright"  style="margin-top: 15px;">© 2014 ЗАО "Валта Пет Продактс" официальный дистрибьютор P&amp;G Pet Care в России</div>
</div><!-- #footer -->
