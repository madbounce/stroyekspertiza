
<div id="header">
    <div class='welcome_container'>         
        <div class="welcome">
            <span>Добро пожаловать на eukanuba.ru</span>  
            <ul>
                <li><a href=" /auth/register">Регистрация</a></li>
            </ul>
        </div>
    </div><!--END .welcome_container-->
    <div class="socialNetworkOut">
        <div class="socialNetworkIn">
            <div class="sticker"><a href="/"><img src="<?php echo Yii::app()->theme->baseUrl?>/images/sticker.png"></a></div>
			<div class="top-icons">
                    <a class="connect" target="_blank" href="<?php echo Config::model()->get('socialVkLink', '');?>"></a>
                    <a class="connect" target="_blank" href="<?php echo Config::model()->get('socialFbLink', '');?>"></a>
            </div>
        </div>
    </div>
    <div class="navigation">
        <div class="search">
            <?php $this->renderPartial('//common/top_search'); ?>
        </div>
        <!--begin menu part-->
        <div class="TopMenu">
            <?php $this->renderPartial('//common/top_menu'); ?>
        </div>
        <!--end menu part-->
    </div>
</div>
