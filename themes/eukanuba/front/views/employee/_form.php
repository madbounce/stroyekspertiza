<?php /** @var $form CActiveForm */ ?>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'employee-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>

<?php echo CHtml::hiddenField('id', $model->id); ?>

<div class="formline1">
    <?php echo $form->labelEx($model, 'first_name', array('class' => 'l1')); ?>
    <?php echo $form->textField($model, 'first_name', array('class' => 'input200')); ?>
    <?php echo $form->error($model, 'first_name'); ?>
</div>

<div class="formline1">
    <?php echo $form->labelEx($model, 'last_name', array('class' => 'l1')); ?>
    <?php echo $form->textField($model, 'last_name', array('class' => 'input200')); ?>
    <?php echo $form->error($model, 'last_name'); ?>
</div>

<div class="formline1">
    <?php echo $form->labelEx($model, 'email', array('class' => 'l1')); ?>
    <?php echo $form->textField($model, 'email', array('class' => 'input200')); ?>
    <?php echo $form->error($model, 'email'); ?>
</div>

<div class="formline1">
    <?php echo $form->labelEx($model, 'contact_phone', array('class' => 'l1')); ?>
    <?php echo $form->textField($model, 'contact_phone', array('class' => 'input200')); ?>
    <?php echo $form->error($model, 'contact_phone'); ?>
</div>

<div class="formline1">
    <?php echo $form->labelEx($model, 'company_post', array('class' => 'l1')); ?>
    <?php echo $form->textField($model, 'company_post', array('class' => 'input200')); ?>
    <?php echo $form->error($model, 'company_post'); ?>
</div>

<?php echo CHtml::link(Yii::t('profile', 'применить'), '#', array('class' => 'greenbtn sm', 'style' => 'margin-top:20px; width:101px;', 'submit' => '')) ?>

<?php $this->endWidget(); ?>