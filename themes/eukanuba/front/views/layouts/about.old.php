<?php $this->beginContent('//layouts/base'); ?>

<div class="page">
    <?php $this->renderPartial('//common/header'); ?>
    <div class="top_inner_content_block">
        <div class="center_wrap">
            <div class="cloud_block">
                <?php
                    switch(Yii::app()->controller->action->id){
                        case 'feedback':
                            echo CHtml::image(Yii::app()->theme->baseUrl . '/images/feedback_pic.png',  $this->pageTitle , array('class' => 'feedback_pic'));
                            echo '<div class="feedback_text">';
                            echo '<h5>' . Misc::t('Наш адрес') . ':</h5>';
                            echo '<p>' . Misc::t(Config::get('infoAddress')). '</p>';
                            echo '<p>' . Misc::t('Телефон') . ': ' . Config::get('infoPhone') . '</p>';
                            echo '</div>';
                            break;
                        default:
                            echo CHtml::image(Yii::app()->theme->baseUrl . '/images/about_pic.png', Misc::t('Написать нам'), array('class' => 'about_pic'));
                            break;
                    }
                ?>
            </div>
            <h1><?php echo $this->pageTitle ;?></h1>
            <h4><?php echo Config::get('infoPhone')?></h4>
            <p><?php echo CHtml::link( Config::get('infoEmail'), 'mailto:' .  Config::get('infoEmail'), array('title' => Misc::t('Связаться с нами'))) ?></p>
            <?php
            $img = CHtml::image(Yii::app()->theme->baseUrl . '/images/mail_icon.png', Misc::t('Написать нам'));
            echo CHtml::link($img . Misc::t('Написать нам'), array('/page/default/feedback'), array('class' => 'mail_us_btn', 'title' => Misc::t('Написать нам')));
            ?>
            <div class="topcontent_menu">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                        array('label' => Misc::t('О компании'), 'url'=>array('/page/default/about'), 'linkOptions' => array('title' => Misc::t('О компании'))),
                        array('label'=>Misc::t('Вакансии'), 'url'=>array('/page/default/jobs'), 'linkOptions' => array('title' => Misc::t('Вакансии'))),
                        array('label'=>Misc::t('Контакты'), 'url'=>array('/page/default/contacts'), 'linkOptions' => array('title' => Misc::t('Контакты'))),
                        array('label'=>Misc::t('Связаться с нами'), 'url'=>array('/page/default/feedback'), 'linkOptions' => array('title' => Misc::t('Связаться с нами'))),
                    ),
                ));
                ?>
            </div>
        </div>
    </div>

    <?php echo $content; ?>

    <div class="footer_guarantor"></div>
</div>

<?php $this->renderPartial('//common/footer'); ?>

<?php $this->endContent(); ?>