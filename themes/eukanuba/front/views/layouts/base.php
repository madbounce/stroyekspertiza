<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="<?php echo Yii::app()->language; ?>" />
    <meta name="module" content="<? echo Yii::app()->request->getPathInfo()?>" />
    <meta name="route" content="<? echo Yii::app()->controller->route?>" />

    <?php if(((
                isset(Yii::app()->controller->id) && isset(Yii::app()->controller->getModule()->id)
            ) && (
                (Yii::app()->controller->getModule()->id == 'articles'  && $this->route == 'articles/default/view') ||
                (Yii::app()->controller->getModule()->id == 'faq'       && $this->route =='faq/default/view') ||
                (Yii::app()->controller->getModule()->id == 'page'      && $this->route =='page/default/index') ||
                (Yii::app()->controller->getModule()->id == 'catalog'   && $this->route =='catalog/default/view')
            ) && (
                trim($this->pageTitle)
            )
        )):?>

        <title><?php echo $this->pageTitle; ?></title>

    <?php else:?>
        <?php $this->widget('seo.components.SEOWidget', array('mode'=>'0')); ?>
    <?php endif?>

    <?php
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/js.js', CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/select.js', CClientScript::POS_END);
    ?>

    <link href="<?php echo Yii::app()->theme->baseUrl . '/css/styles.css' ?>" rel="stylesheet" >
    <!--[if lte IE 9]><link rel="stylesheet" media="all" type="text/css" href="<?php echo Yii::app()->theme->baseUrl . '/css/styleIE.css' ?>" /><![endif]-->
    <!--[if IE 8]><link href= "<?php echo Yii::app()->theme->baseUrl.'/css/ie8.css';?>" rel= "stylesheet" media= "all" /><![endif]-->

<style type="text/css">@font-face {font-family:'Bliss Cyrillic Bold';src:url('http://f.fontdeck.com/f/1/U000ekNyejAAACzS93nyxPqbDCbS7P1xwxNEzUwrkeEIiFzsTLvrQjSjbnkJHSeY3uo.eot');src:url('http://f.fontdeck.com/f/1/U000ekNyejAAACzS93nyxPqbDCbS7P1xwxNEzUwrkeEIiFzsTLvrQjSjbnkJHSeY3uo.eot?')format('embedded-opentype'),url('http://f.fontdeck.com/f/1/U000ekNyejAAACzS93nyxPqbDCbS7P1xwxNEzUwrkeEIiFzsTLvrQjSjbnkJHSeY3uo.woff')format('woff'),url('http://f.fontdeck.com/f/1/U000ekNyejAAACzS93nyxPqbDCbS7P1xwxNEzUwrkeEIiFzsTLvrQjSjbnkJHSeY3uo.ttf')format('opentype'),url('undefined')format('svg');font-weight:bold;font-style:normal;}@font-face {font-family:'Bliss Cyrillic Light';src:url('http://f.fontdeck.com/f/1/Uk9hNGd0RGkAAD8T6Qld9X1F3iFtvfGpBhZMtuuKz8qlTxZAzUmlYejhneviNVlgWDc.eot');src:url('http://f.fontdeck.com/f/1/Uk9hNGd0RGkAAD8T6Qld9X1F3iFtvfGpBhZMtuuKz8qlTxZAzUmlYejhneviNVlgWDc.eot?')format('embedded-opentype'),url('http://f.fontdeck.com/f/1/Uk9hNGd0RGkAAD8T6Qld9X1F3iFtvfGpBhZMtuuKz8qlTxZAzUmlYejhneviNVlgWDc.woff')format('woff'),url('http://f.fontdeck.com/f/1/Uk9hNGd0RGkAAD8T6Qld9X1F3iFtvfGpBhZMtuuKz8qlTxZAzUmlYejhneviNVlgWDc.ttf')format('opentype'),url('undefined')format('svg');font-weight:200;font-style:normal;}</style>
</head>
<body class="no-js <?php echo yii::app()->request->getPathInfo()=='' ? 'body-home' : ''?>">
    <?php echo $content; ?>
</body>
</html>