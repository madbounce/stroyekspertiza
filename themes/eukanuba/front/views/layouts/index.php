<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/base'); ?>
    <?php $this->renderPartial('//common/header'); ?>
<div class="middle">
    <!--left banner-->
    <div class="main_banner">
        <img src="images/main_banner.png" title="Eukanuba" width="114" height="673">
    </div>
    <!--main part-->
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span11 offset1 content_part">
                <!--begin top part-->
                <div class="row-fluid">
                    <div class="span6 offset6">
                        <form id="search" class="pull-right" method="get" action="/search" name="search" >

                            <input name="searchText" id="searchText" class="text" type="text" value="Search.." onblur="if (this.value==''){this.value='Search..'}" onfocus="if (this.value=='Search..') this.value='';">

                            <input type="submit" value="GO" id="searchSubmit" >
                        </form>
                        <div class="select_country custom_select pull-right" >
                            <select class="selectpicker" >
                                <option>Select a Country</option>
                                <option>1</option>
                                <option>1</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!--end top part-->
                <!--begin menu part-->
                <div class="row-fluid">
                    <div class="span12 " id="menu">
                        <ul class="nav nav-pills">
                            <li><a href="" title="">о нас</a></li>
                            <li><a href="" title="">продукция</a></li>
                            <li><a href="" title="">уход</a></li>
                            <li><a href="" title="">профессионалы</a></li>
                            <li><a href="" title="">сообщество</a></li>
                        </ul>
                    </div>
                </div>
                <!--end menu part-->
                <!--slider part-->
                <div class="slider_flash">
                    <img src="i/flash.png" title="Eukanuba">
                </div>
                <!--end slider part-->
                <!-- small main page banners-->
                <div class="row-fluid touts">

                    <div class="tout">
                        <p>Find Eukanuba Near You</p>
                        <img src="i/tout1.png"/>
                        <span class="btn_more">
                            <a href="" title="">buy now</a><em></em>
                        </span>
                    </div>

                    <div class="tout ">
                        <p>Shop</p>
                        <img src="i/tout2.png">
                        <span class="btn_more">
                            <a href="" title="">buy now</a><em></em>
                        </span>

                    </div>
                    <div class="tout">
                        <p>PET IDOL on YouTube</p>
                        <img src="i/tout3.png">
                        <span class="btn_more">
                            <a href="" title="">learn more</a><em></em>
                        </span>

                    </div>
                    <div class="tout">
                        <p>Cleans with</p>
                        <img src="i/tout4.png">
                        <span class="btn_more">
                            <a href="" title="">learn more</a><em></em>
                        </span>

                    </div>
                </div>
                <!-- end main page banners-->
                <!-- footer menu-->
                <?php $this->renderPartial('//common/footer'); ?>
                <!-- end  footer menu-->
            </div>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>