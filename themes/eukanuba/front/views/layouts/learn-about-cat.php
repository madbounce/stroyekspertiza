<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<?php
  switch($_GET['name']){
      case 'about_us':
          $class = 'about-us';
          break;
      case 'nutritional_philosophy':
          $class = 'NutritionalPhilosophy';
          break;
      case 'about_iams':
           $class = 'About-Iams';
        break;
      default:
           $class = 'b-page-cont';
  }
?>
<div id="MainContent" class="<?php echo $class;?> pet-health-landing">
    <div class="copy-top-border"><img src="<?=Yii::app()->theme->baseUrl;?>/images/top-border-dog.png" alt="" width="956" height="8" class="topborder" title=""></div>
    <div id="white-container"><div class="clear-all">&nbsp;</div>
            <?php echo $content; ?>
    </div><!-- Copy Content end-->
</div>
<?php $this->endContent(); ?>