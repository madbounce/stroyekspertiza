<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="MainContent" class="Sequel-lpage">
    <div class="copy-top-border"><img src="<?=Yii::app()->theme->baseUrl;?>/images/top-border-dog.png" alt="" width="956" height="8" class="topborder" title=""></div>
    <div id="white-container"><div class="clear-all">&nbsp;</div>
        <?php echo $content; ?>
    </div><!-- Copy Content end-->
</div>
<?php $this->endContent(); ?>