<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/base'); ?>

<div id="wrapper">

    <!--begin top header-->
    <?php $this->renderPartial('//common/header'); ?>
    <!--end top header-->

    <div id="content" class="<?php echo yii::app()->request->getPathInfo()=='' ? 'homePage' : ''?>">

        <?php if(isset($this->breadcrumbs)):?>
            <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                'htmlOptions' => array('class'=>$this->breadcrumbs_class),
                'separator' => '/',
                'homeLink'=>CHtml::link(Misc::t('Главная'),array('/site/index')),
                'links'=>$this->breadcrumbs,
        )); ?><!-- breadcrumbs -->
        <?php endif?>

        <div class="middle">
            <!--begin main part-->
            <?php echo $content ?>
            <!--end main part-->
        </div>

    </div><!-- #content-->
</div><!-- #wrapper -->

<?php $this->renderPartial('//common/footer'); ?>


<?php $this->endContent(); ?>
