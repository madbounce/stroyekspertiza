<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/base'); ?>

<div class="page">
    <?php $this->renderPartial('//common/header'); ?>
    <?php //$this->widget('ProfileWidget', array('type' => ProfileWidget::TYPE_PRIVATE)); ?>

    <div class="content_block inner_content">
        <div class="center_wrap">
            <h1><?php echo Misc::t('Личный кабинет')?></h1>
            <?php //echo $content; ?>
        </div>
    </div>
    <div class="footer_guarantor"></div>
</div>

<?php $this->renderPartial('//common/footer'); ?>

<?php $this->endContent(); ?>