<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="MainContent" class="site-map">
    <div id="white-container">
        <div class="copy-top-border"><img src="<?=Yii::app()->theme->baseUrl;?>/images/top-border-dog.png" alt="" width="956" height="8" title="" class="topborder"></div>
        <div id="bodycontent_area">
            <div class="clear-all">&nbsp;</div>

            <?php echo $content; ?>

        </div>
        <div class="clear-all">&nbsp;</div>
    </div><!-- Copy Content end-->
</div>
<?php $this->endContent(); ?>