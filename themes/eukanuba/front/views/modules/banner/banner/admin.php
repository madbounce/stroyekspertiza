<?php
$this->breadcrumbs=array(
	$this->module->nameModule=>array('/banner'),
	'Баннеры'=>array('admin'),
	'Управление',
);
?>
<div class="section">
	<div class="box">
	<div class="title">
		<h2>Управление "Баннеры"</h2>
	</div>
		<div class="content">
			<div class="button" style="float: left;padding-left: 20px;">
				<?php echo CHtml::link('Добавить баннер', array('/backend/banner/banner/create'), $htmlOptions=array('class'=>'button green medium', 'style'=>'opacity: 0.7;')); ?>
			</div>
			<div class="button" style="float: left;padding-left: 20px;">
				<?php echo CHtml::link('Категории', array('//backend/banner/category'), $htmlOptions=array('class'=>'button green medium', 'style'=>'opacity: 0.7;')); ?>
			</div>
			<div class="button" style="float: left;padding-left: 20px;">
				<?php echo CHtml::link('Размеры', array('/admin/banner/size'), $htmlOptions=array('class'=>'button green medium', 'style'=>'opacity: 0.7;')); ?>
			</div>
			<div style="clear: both;"></div>

			<?php $this->widget('bootstrap.widgets.TbGridView', array(
				'id'=>'banner-grid',
				'dataProvider'=>$model->search(),
//				'filter'=>$model,
//				'itemsCssClass'=>'table',
				'columns'=>array(
					'id',
					'title',
					array(
						'name'=>'size_id',
						'value'=>'$data->size->concatenedSizeTitle',
					),
//					'type',
					array(
						'name'=>'create_time',
						'value'=>'date("d.m.Y", $data->create_time)',
					),
					array(
						'name'=>'update_time',
						'value'=>'date("d.m.Y", $data->update_time)',
					),
					array(
						'name'=>'start_date',
						'value'=>'date("d.m.Y", $data->start_date)',
					),
					array(
						'name'=>'end_date',
						'value'=>'date("d.m.Y", $data->end_date)',
					),
//					'last_show_time',
//					'is_active',
//					'is_default',
					'show_limit',
					'go_count',
					'count',
					array(
						'class'=>'bootstrap.widgets.TbButtonColumn',
						'template'=>'{update}{delete}',
//						'updateButtonImageUrl'=>$this->path.'gfx/icon-edit.png',
//						'deleteButtonImageUrl'=>$this->path.'gfx/icon-delete.png',
					),
				),
			)); ?>
		</div>
	</div>
</div>