<?php
$this->breadcrumbs=array(
	$this->module->nameModule=>array('/banner'),
	'Категории'=>array('admin'),
	'Добавить',
);
?>

<div class="section">
	<div class="box">
	<div class="title">
		<h2>Добавить категорию</h2>
	</div>
		<div class="content">
			<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
		</div>
	</div>
</div>