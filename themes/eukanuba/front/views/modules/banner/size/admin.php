<?php
$this->breadcrumbs=array(
	$this->module->nameModule=>array('/banner'),
	'Размеры'=>array('admin'),
	'Управление',
);
?>

<div class="section">
	<div class="box">
	<div class="title">
		<h2>Управление "Баннеры"</h2>
	</div>
		<div class="content">
			<div class="button" style="float: left;padding-left: 20px;">
				<?php echo CHtml::link('Добавить размер', array('/admin/banner/size/create'), $htmlOptions=array('class'=>'button green medium', 'style'=>'opacity: 0.7;')); ?>
			</div>
			<div style="clear: both;"></div>

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'banner-size-grid',
				'dataProvider'=>$model->search(),
//				'filter'=>$model,
				'itemsCssClass'=>'table',
				'columns'=>array(
					'id',
					'title',
					'width',
					'height',
					array(
						'class'=>'CButtonColumn',
						'template'=>'{update}{delete}',
						'updateButtonImageUrl'=>$this->path.'gfx/icon-edit.png',
						'deleteButtonImageUrl'=>$this->path.'gfx/icon-delete.png',
					),
				),
			)); ?>
		</div>
	</div>
</div>