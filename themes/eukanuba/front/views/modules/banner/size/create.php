<?php
$this->breadcrumbs=array(
	$this->module->nameModule=>array('/banner'),
	'Размеры'=>array('admin'),
	'Добавить',
);
?>

<div class="section">
	<div class="box">
	<div class="title">
		<h2>Добавить размер</h2>
	</div>
		<div class="content">
			<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
		</div>
	</div>
</div>