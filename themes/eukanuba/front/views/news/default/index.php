<?php
$this->pageTitle = Yii::t('news', 'Новости');
?>

<?php $dataProvider->pagination->pageSize = 5; ?>

<div class="main-cont-wrapper">
<h1 class="reg-title"><?php echo Yii::t('news', 'Новости'); ?></h1>
<div class="block_full_news">
    <?php $this->widget('application.components.CustomListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
    )); ?>
</div>