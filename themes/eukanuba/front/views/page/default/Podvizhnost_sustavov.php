<?php
    $this->breadcrumbs=array(
        strip_tags($model->title),
    );
?>


<div class="<?php echo $model->name?> ">

    <div>
		<div class="food">
						<img src="<?php echo Yii::app()->theme->baseUrl?>/images/petFood5.png" class="red">
						<ul class="firstUl">
								<li><a href="<?=Yii::app()->baseUrl;?>/site/buymap" class="pack">Где купить? </a></li>
								<?php $this->renderPartial('_inc/vets_menu');?>
							</li>
						</ul>
		</div>
        
    </div>

    <div class="fix_page">
        <div class="staticwrap">
			    <div class="AboutFood turquoise">
            	<div class="shortInfo">
                	<div class="leftSide">
					<img src="<?php echo Yii::app()->theme->baseUrl?>/images/veterenary.png" class="veterenary">
                    	<div class="renalCond turquoise">Joint Mobility</div>
                        <div class="liverCond turquoise">RECOVERY</div>
						<img src="<?php echo Yii::app()->theme->baseUrl?>/images/dogAndCat5.png" class="red">
                        
                        <div class="advantage turquoise">
                        	<ul>
                                <li>Здоровые зубы и десны</li>
                            	<li>Отличное состояние кожи и шерсти</li>
                                <li>Оптимальное пищеварение</li>
                                <li>Поддержание веса</li>
                                <li>Сильная имунная система</li>
                                
                            </ul>
                        </div>
                	</div>
                    </div>
                    <div class="rightSide">
					<h2>ПОДВИЖНОСТЬ СУСТАВОВ</h2>
					<h2>Показания</h2>

<p><b>Проблемы с суставами/подвижностью</b></p>

<ul class="bonus">
<li> Остеоартрит</li>
<li> Ортопедические процедуры</li>
<li> Травмы суставов</li>
</ul>

                    </div>
					<br clear="all">
					
					<?php echo  $model->content; ?>

            </div>
			

        </div>
    </div>

</div>