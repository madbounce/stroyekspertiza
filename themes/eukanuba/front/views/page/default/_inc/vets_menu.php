<div class="related">
<p>Диеты</p>
    <?php $this->widget('zii.widgets.CMenu', array(
        'items'=>array(
            array(
                'label' => Misc::t('При ожирении'),
                'url'=>array('/page/default/index','name'=>'pri_ozhirenii'),
                'linkOptions' => array('title' => Misc::t('При ожирении'))
            ),
            array(
                'label' => Misc::t('При кишечных расстройствах'),
                'url'=>array('/page/default/index','name'=>'pri_kishechnyh_rasstrojstvah'),
                'linkOptions' => array('title' => Misc::t('При кишечных расстройствах'))
            ),
            array(
                'label' => Misc::t('При воспалительных заболеваниях кожи'),
                'url'=>array('/page/default/index','name'=>'vospalitel\'nye_zabolevanija_kozhi'),
                'linkOptions' => array('title' => Misc::t('При воспалительных заболеваниях кожи'))
            ),
            array(
                'label' => Misc::t('При заболеваниях суставов'),
                'url'=>array('/page/default/index','name'=>'Podvizhnost_sustavov'),
                'linkOptions' => array('title' => Misc::t('При заболеваниях суставов'))
            ),
            array(
                'label' => Misc::t('Для контроля веса при диабете'),
                'url'=>array('/page/default/index','name'=>'kontrol_vesa_pri_diabete'),
                'linkOptions' => array('title' => Misc::t('Для контроля веса при диабете'))
            ),
            array(
                'label' => Misc::t('При мочекаменной болезни струвитного или оксалатного типа'),
                'url'=>array('/page/default/index','name'=>'mochekamennaja_bolezn'),
                'linkOptions' => array('title' => Misc::t('При мочекаменной болезни струвитного или оксалатного типа'))
            ),
            array(
                'label' => Misc::t('При заболеваниях почек'),
                'url'=>array('/page/default/index','name'=>'zabolevanija_pochek'),
                'linkOptions' => array('title' => Misc::t('При заболеваниях почек'))
            ),
        ),
    ));?>
</div>