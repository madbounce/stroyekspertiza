<?php
$this->pageTitle = Misc::t('Контакты');
?>
<div class="content_block">
    <div class="center_wrap">
        <div class="content_left_block">
            <?php $this->widget('page.components.widgets.PageWidget', array('name' => 'contactsLeft', 'hTag' => 'h1'));?>
        </div>
        <div class="right_block">
            <?php $this->widget('page.components.widgets.PageWidget', array('name' => 'contactsRight', 'hTag' => 'h5'));?>
            <div class="small_map">
                <?php $this->widget('ContactGMapWidget', array('address' => Config::get('infoAddress'))); ?>
            </div>
        </div>
    </div>
</div>