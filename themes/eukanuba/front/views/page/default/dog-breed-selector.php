<?php
$this->breadcrumbs=array(
    strip_tags($model->title),
);

$background = $model->getPhoto();
?>


<div class="<?php echo $model->name?>">


    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 12,'view'=>'textClickable')); ?>


    <div class="fix_page">
        <div class="staticwrap">
            <?php //<h1 class="title"><?php echo $model->title; ?><?php //</h1> ?>
            <?php //echo $model->content; ?>

            <div id="content_wrapper">

                <!-- JavaScript  -->

                <!-- Load and run the Flash -->
                <script src="http://media.eukanuba.co.uk/js/swfobject.js" type="text/javascript"></script>
                <script type="text/javascript">
                    // Define flash var objects
                    var flashvars = {
                    'xmlPath': '/ru-RU/file/flash/.jspx',
                    'locale': 'ru-RU',
                    'mediaPrefix': 'http://media.eukanuba.co.uk',

                    'BreedLogicPath': 'file/flash/breedLogic.jspx',
                    'InfoQuestionPath': 'file/flash/breedInfoQuestions.jspx',
                    'LANG': 'JP',
                    'Debug': 'False',
                    'CallOutPath': 'file/flash/flashCallouts.jspx'
                    };
                    var params = {
                    'wmode': 'transparent',
                    'allowscriptaccess': 'always',
                    'allowfullscreen': 'false'
                    };
                    var attributes = {};

                    swfobject.embedSWF(
                        "/media/BreedMatch_v2.swf", // Flash path
                        "flash_hero", // Target ID to replace with flash
                        "800", // width
                        "400", //height
                        "9.0.0", // Required flash version
                        "http://media.eukanuba.co.uk/js/expressInstall.swf", // Path to flash updater
                        flashvars, params, attributes); // Pass in flash vars
                </script>

            </div>
        </div>
    </div>

</div>