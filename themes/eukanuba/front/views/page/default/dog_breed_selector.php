<?php
//Yii::app()->clientScript->registerScriptFile("http://media.eukanuba.co.uk/js/swfobject.js", CClientScript::POS_HEAD);
?>

<script type="text/javascript">
    var localePath = 'ru-RU';
    var localeName = 'ru_RU';
    var serverUrl = 'http://media.eukanuba.com';
    var mediaServerUrl = 'http://media.eukanuba.com';
    var localeCountry = 'RU';
</script>
<style media="screen" type="text/css">#flash_hero {visibility:hidden}</style></head>

<div class="header_image articles_s">
    <div class="content_header">
        <div class="text_header">
            <h3>Подбор породы</h3>
            <p>Найдите породу, которая подходит именно вам</p>
        </div>
        
        <i class="ico"></i>

        <div class="header_right_menu">
            <ul>
                <li><a href="/articles/default/puppy">СТАТЬИ</a></li>
                <li><a target="_blank" href="http://www.youtube.com/channel/UCp6hEh5cS8p9r1SgylxbFyQ">ВИДЕО О ВОСПИТАНИИ</a></li>
                <li><a href="/products/dog/puppy">ПОДБОР КОРМА</a></li>
                <li><a href="/page/nutrition">ПИТАНИЕ ЩЕНКОВ</a></li>
                <li><a href="/page/shoppinglist">ЛИСТ ПОКУПОК</a></li>
                <li><a href="/page/dog_breed_selector" class="active">НАЙДИТЕ СВОЮ ИДЕАЛЬНУЮ ПОРОДУ</a></li>
                <li><a href="/page/breedersrecommended">РЕКОМЕНДАЦИИ ЗАВОДЧИКОВ</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="<?php echo $model->name?>">


    <?php //$this->widget('banner.components.widget.rotator', array('categoryId' => 12,'view'=>'textClickable')); ?>


    <div class="fix_page top">
        <div class="staticwrap">
            <?php //<h1 class="title"><?php echo $model->title; ?><?php //</h1> ?>
            <?php //echo $model->content; ?>

            <!--  Content Wrapper. Start -->
            <div id="content_wrapper">

                <!-- Hero. Start -->
                <div id="flash_hero" class="">

                    <div id="noflash">


                        <h2>Alternative Flash Content</h2>
                        <p id="flash_alternative_notice">
                            <strong>Please note:</strong> the alternative Flash content below is the same as that found inside the Flash movie. Therefore, some extraneous content maybe also be included.
                        </p>
                        <div id="flash_alternative">
                            <p>null</p>
                        </div>

                        <div id="noflash_message">
                        <strong>!</strong>
                            <p>
                                This site is enhanced with Flash. Please <a href="http://get.adobe.com/flashplayer/">install Flash</a>.
                            </p>
                            <noscript>
                                <p>
                                    Please <a href="http://base.google.com/support/bin/answer.py?hl=en-uk&amp;answer=23852">enable JavaScript</a> to fully experience this site.
                                </p>
                            </noscript>
                        </div>
                    </div>
                </div>
                <!-- Hero. End -->

		<!-- JavaScript  -->

	    <!-- Load and run the Flash -->
	    <script type="text/javascript" src="http://media.eukanuba.com/en_us/data_root/_script/swfobject.js"></script>
	    <script type="text/javascript">
	    	// Define flash var objects
			var flashvars = {
			'xmlPath': 'http://eukanuba.com/ru-RU/file/flash/.jspx',
			'locale': 'ru-RU',
			'mediaPrefix': 'http://media.eukanuba.com',

			'BreedLogicPath': '/file/flash/breedLogic.xml',
			'InfoQuestionPath': '/file/flash/breedInfoQuestions.xml',
			'LANG': 'JP',
			'Debug': 'False',
			'CallOutPath': '/file/flash/flashCallouts.xml'
	    	};
			var params = {
			'wmode': 'transparent',
			'allowscriptaccess': 'always',
			'allowfullscreen': 'false'
			};
			var attributes = {};

			swfobject.embedSWF(
				"http://eukanuba.com/en_us/data_root/media/BreedMatch_v2.swf", // Flash path
				"flash_hero", // Target ID to replace with flash
				"800", // width
				"400", //height
				"9.0.0", // Required flash version
				"http://media.eukanuba.com/js/expressInstall.swf", // Path to flash updater
				flashvars, params, attributes); // Pass in flash vars
		</script>

</div> <!--  Content Wrapper. End -->

        </div>
    </div>

</div>