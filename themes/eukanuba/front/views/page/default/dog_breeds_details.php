<?php
//Yii::app()->clientScript->registerScriptFile("http://media.eukanuba.co.uk/js/swfobject.js", CClientScript::POS_HEAD);
?>

<?php
$this->breadcrumbs=array(
    strip_tags($model->title),
);

$background = $model->getPhoto();
?>

<script type="text/javascript">
    var localePath = 'ru-RU';
    var localeName = 'ru_RU';
    var serverUrl = 'http://media.eukanuba.com';
    var mediaServerUrl = 'http://media.eukanuba.com';
    var localeCountry = 'RU';
</script>
<style media="screen" type="text/css">#flash_hero {visibility:hidden}</style></head>


<div class="<?php echo $model->name?>">


    <?php //$this->widget('banner.components.widget.rotator', array('categoryId' => 12,'view'=>'textClickable')); ?>


    <div class="fix_page">
        <div class="staticwrap">
            <?php //<h1 class="title"><?php echo $model->title; ?><?php //</h1> ?>
            <?php //echo $model->content; ?>

            <!--  Content Wrapper. Start -->
            <div id="content_wrapper">

            <!-- Hero. Start -->
            <div id="flash_hero" class="">

                <div id="noflash">


                    <h2>Alternative Flash Content</h2>
                    <p id="flash_alternative_notice">
                        <strong>Please note:</strong> the alternative Flash content below is the same as that found inside the Flash movie. Therefore, some extraneous content maybe also be included.
                    </p>
                    <div id="flash_alternative">
                        <p><div id="general">
                <div id="headline">BREEDOPEDIA</div>
                <div id="background"><img src="/images/common/breedopedia/BreedOPedia3.jpg" /></div>
                <div id="form" action="dog-breeds-listing.jspx" method="get">
                    <select name="category">
                        <option value="alphabetic" action="dog-breeds-listing.jspx"></option>
                        <option value="breedtype" action="dog-breeds-listing.jspx"></option>
                        <option value="breedsize" action="dog-breeds-listing.jspx"></option>
                    </select>
                </div>
                <div id="pageNav">
                    <a tabID="Overview"><![CDATA[����������]]></a>
                    <a tabID="Breed Story"><![CDATA[�������������� ������������]]></a>
                    <a tabID="Standards"><![CDATA[������������������]]></a>
                    <a tabID="Temperament"><![CDATA[����������������������]]></a>
                    <a tabID="Good To Know"><![CDATA[���������������� ��������������������]]></a>
                </div>
                <div id="utility">
                    <img src="/images/common/breedopedia/icon_pdf.png"/>
                    <img src="/images/common/breedopedia/icon_print.png"/>
                    <a href="/ru-RU/dog-breed-encyclopedia.jspx"><img src="/images/common/breedopedia/icon_email.png"/></a>
                </div>
                <div id="pointer"><h1>We apologize for the inconvenience.</h1><p>This section has not been converted to your language.</p></div>
                <div id="backButton">
                    <a href="dog-breeds-listing.jspx"><![CDATA[����������]]></a>
                    <div id="param1">page</div>
                    <div id="param2">file</div>
                </div>
            </div></p>
                    </div>

                    <div id="noflash_message">
                    <strong>!</strong>
                        <p>
                            This site is enhanced with Flash. Please <a href="http://get.adobe.com/flashplayer/">install Flash</a>.
                        </p>
                        <noscript>
                            <p>
                                Please <a href="http://base.google.com/support/bin/answer.py?hl=en-uk&amp;answer=23852">enable JavaScript</a> to fully experience this site.
                            </p>
                        </noscript>
                    </div>
                </div>
            </div>

            <!-- Hero. End -->

                    <!-- JavaScript  -->

                    <!-- Load and run the Flash -->
                    <script type="text/javascript" src="http://media.eukanuba.com/js/swfobject.js"></script>
                    <script type="text/javascript">
                        // Define flash var objects
                        var flashvars = {
                        'xmlPath': '/file/flash/DogBreedsDetails_general.xml',
                        'locale': 'ru-RU',
                        'mediaPrefix': 'http://media.eukanuba.com',

                        'LANG': 'RU',
                        'generalXMLPath': '/file/flash/DogBreedsDetails_general.xml',
                        'calloutXMLPath': '/file/flash/flashCallouts.xml',
                        //'dogXMLPath': '/file/flash/breeds/dog/'+"<? //echo $breed?>"+'.xml'
                        'dogXMLPath': 'http://eukanuba.com/ru-RU/file/flash/breeds/dog/'+"<? echo $breed?>"+'.jspx'
                        };
                        var params = {
                        'wmode': 'transparent',
                        'allowscriptaccess': 'always',
                        'allowfullscreen': 'false'
                        };
                        var attributes = {};

                        swfobject.embedSWF(
                            "http://eukanuba.com/media/BreedOPedia_Step3.swf", // Flash path
                            "flash_hero", // Target ID to replace with flash
                            "800", // width
                            "400", //height
                            "9.0.0", // Required flash version
                            "http://media.eukanuba.com/js/expressInstall.swf", // Path to flash updater
                            flashvars, params, attributes); // Pass in flash vars
                    </script>

            </div> <!--  Content Wrapper. End -->

        </div>
    </div>

</div>