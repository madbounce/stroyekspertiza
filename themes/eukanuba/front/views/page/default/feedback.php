<?php
$this->pageTitle = Misc::t('Связатся с нами');
?>
<div class="content_block inner_content">
    <div class="center_wrap">
        <h1><?php echo CHtml::encode($model->title); ?></h1>
        <?php echo $model->content; ?>
        <div class="form_block">
            <?php $this->widget('ContactFormWidget', array('action' => Yii::app()->createUrl('/page/default/feedback'))); ?>
        </div>
    </div>
</div>   