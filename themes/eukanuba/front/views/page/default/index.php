<?php Yii::app()->clientScript->registerMetaTag($model->description,'description');?>
<?
	$content = $model->content;
	foreach($params as $key => $param)
		$content = str_replace('%'.$key.'%',$param,$content);
	
?>
<div id="breadcrumb"><a title="Home" href="/">Главная</a> / <?php echo $model->title; ?></div>
<div class="_col left-callouts-nav">
    <p class="green_curve"><img src="<?=Yii::app()->theme->baseUrl;?>/images/img_cat_detail.jpg" alt="" title=""></p>

    <div class="left-callouts"> <img src="<?=Yii::app()->theme->baseUrl;?>/images/need_more_help.gif" alt="Need More Help?" title="Need More Help?">
        <p>Не можете найти то, что ищете?</p>
        <div>
            <a title="Связаться" href="/site/contactus" id="contact_btn">Связаться</a>
        </div>
    </div>
</div>

<div class="_col txt-content">
    <h1 class="heading"><?php echo $model->title; ?></h1>
    <? echo $content; ?>
</div>
<div class="clear-all">&nbsp;</div>





