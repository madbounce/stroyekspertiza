<?php Yii::app()->clientScript->registerMetaTag($model->description,'description');?>
<?
$content = $model->content;
foreach($params as $key => $param)
    $content = str_replace('%'.$key.'%',$param,$content);

?>
<div id="breadcrumb"><a title="Home" href="/">Главная</a> / <?php echo $model->title; ?></div>
<div class="_col txt-content">
    <h1 class="heading"><?php echo $model->title; ?></h1>
    <? echo $content; ?>
</div>
<div class="clear-all">&nbsp;</div>