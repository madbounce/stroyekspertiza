<?php
$this->pageTitle = Misc::t('Вакансии');
?>
<div class="content_block">
    <div class="center_wrap">
        <div class="content_left_block">
            <?php echo $model->content; ?>
        </div>
        <div class="right_block">
            <?php $this->widget('page.components.widgets.PageWidget', array('name' => 'advantagesBlock', 'hTag' => 'h5'));?>
        </div>
    </div>
</div>