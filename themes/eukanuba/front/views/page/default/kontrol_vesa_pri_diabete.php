<?php
    $this->breadcrumbs=array(
        strip_tags($model->title),
    );
?>


<div class="<?php echo $model->name?> ">

    <div>
		<div class="food">
						<img src="<?php echo Yii::app()->theme->baseUrl?>/images/petFood6.png" class="red">
						<ul class="firstUl">
								<li><a href="<?=Yii::app()->baseUrl;?>/site/buymap" class="pack">Где купить? </a></li>
								<?php $this->renderPartial('_inc/vets_menu');?>
							</li>
						</ul>
		</div>
        
    </div>

    <div class="fix_page">
        <div class="staticwrap">
			    <div class="AboutFood vinous">
            	<div class="shortInfo">
                	<div class="leftSide">
					<img src="<?php echo Yii::app()->theme->baseUrl?>/images/veterenary.png" class="veterenary">
                    	<div class="renalCond vinous">Weight /Diabetic Control</div>
                        <div class="liverCond vinous">RECOVERY</div>
						<img src="<?php echo Yii::app()->theme->baseUrl?>/images/dogAndCat6.png" class="red">
                        
                        <div class="advantage vinous">
                        	<ul>
                            	<li>Отличное состояние кожи и шерсти</li>
                                <li>Оптимальное пищеварение</li>
                                <li>Сильная имунная система</li>
                                <li>Здоровые зубы и десны</li>
                            </ul>
                        </div>
                    </div>
                    <div class="rightSide">
					<h2>КОНТРОЛЬ ВЕСА ПРИ ДИАБЕТЕ</h2>
					<h2>Показания </h2>

<ul class="bonus">
<li>Диабет</li>
<li>Персистирующая гипергликемия</li>
<li>Ожирение / избыточный вес у собак</li>
<li>Проблемы у собак пожилого возраста (избыточный вес, нарушения метаболизма глюкозы)</li>
</ul>

                    </div>
					<br clear="all">
					
					<?php echo  $model->content; ?>

            </div>
			    </div>

        </div>
    </div>

</div>