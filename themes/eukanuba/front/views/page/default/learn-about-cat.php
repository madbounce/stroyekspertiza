<?php Yii::app()->clientScript->registerMetaTag($model->description,'description');?>
<?
$content = $model->content;
foreach($params as $key => $param)
    $content = str_replace('%'.$key.'%',$param,$content);

?>
<div id="breadcrumb"><a title="Главная" href="/">Главная</a> / <?php echo $model->title; ?></div>
<div class="clear-all">&nbsp;</div>

<?php
$background = $model->getPhoto();
if($background):
    list($width, $height) = getimagesize(Yii::app()->request->hostInfo . $background);
    ?>
    <div class="page-header" style="background: url('/themes/iams/front/images/img_pet_hm_cat.jpg') left no-repeat; width: 915px; ; height: 425px;" >
<?php else:?>
    <div class="page-header">
<?php endif;?>
    <h1><?php echo $model->title; ?></h1>
    <p><strong>Iam’s cat care resource center</strong>You’re all about helping your cat live a happy, healthy life. So are we. Iams has all kind of information to share about cat nutrition and health.</p>
</div>
<div id="bodycontent_area">
    <div class="_col left-callouts-nav">
        &nbsp;
    </div>
    <div class="_col txt-content">
        <?php echo $content; ?>
    </div>
    <div class="clear-all">&nbsp;</div>
</div>
<div class="clear-all">&nbsp;</div>
