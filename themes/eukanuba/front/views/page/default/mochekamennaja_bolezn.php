<?php
    $this->breadcrumbs=array(
        strip_tags($model->title),
    );
?>


<div class="<?php echo $model->name?> ">

    <div>
		<div class="food">
						<img src="<?php echo Yii::app()->theme->baseUrl?>/images/petFood7.png" class="red">
						<ul class="firstUl">
							<li><a href="<?=Yii::app()->baseUrl;?>/site/buymap" class="pack">Где купить? </a></li>
								<?php $this->renderPartial('_inc/vets_menu');?>
							</li>
						</ul>
		</div>
        
    </div>

    <div class="fix_page">
        <div class="staticwrap">
			    <div class="AboutFood yellow_brown">
            	<div class="shortInfo">
                	<div class="leftSide">
					<img src="<?php echo Yii::app()->theme->baseUrl?>/images/veterenary.png" class="veterenary">
                    	<div class="renalCond yellow_brown">Urinary Oxalate/Struvite  </div>
                        <div class="liverCond yellow_brown">RECOVERY</div>
						<img src="<?php echo Yii::app()->theme->baseUrl?>/images/dogAndCat7.png" class="red">
                        
                        <div class="advantage yellow_brown">
                        	<ul>
                            	<li>Отличное состояние кожи и шерсти</li>
                                <li>Оптимальное пищеварение</li>
                                <li>Сильная имунная система</li>
                                <li>Здоровые зубы и десны</li>
                                <li>Контроль веса</li>
                            </ul>
                        </div>
                    </div>
                    <div class="rightSide">
					<h2>МОЧЕКАМЕННАЯ БОЛЕЗНЬ СТРУВИТНОГО И ОКСАЛАТНОГО ТИПОВ</h2>
					<h2>Показания</h2>

<ul class="bonus">
<li>Мочекаменная болезнь струвитного типа</li>
<li>
Струвитная кристаллурия и уролитиаз
</li>
<li>Мочекаменная болезнь оксалатного типа</li>
<li>
Кальций оксалатная кристаллурия и уролитиаз
</li>
</ul>

                    </div>
					<br clear="all">
					
					<?php echo  $model->content; ?>

            </div>
			

        </div>
    </div>

</div>
</div>