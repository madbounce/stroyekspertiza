<?php
    $this->breadcrumbs=array(
        strip_tags($model->title),
    );

$background = $model->getPhoto();
?>
<?php
if($background):
    list($width, $height) = getimagesize(Yii::app()->request->hostInfo . $background);

    ?>
<style>
    .bigDog.about2{
        background: url('<?php echo $background?>') left no-repeat;
        width: <?php echo $width?>px; ;
        height: <?php echo $height?>px;
        top:0px;
    }
</style>
<?php endif; ?>

<div class="<?php echo $model->name?>">

    <div class="bigDog">
        <h1><?php echo $model->title; ?></h1>
    </div>



    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 12,'view'=>'textClickable')); ?>


    <div class="fix_page">
        <div class="staticwrap">
            <?php //<h1 class="title"><?php echo $model->title; ?><?php //</h1> ?>

            <?php //echo $model->content; ?>



            <div class="zubi">
                	<p class="pOne"><span>Теперь в каждой упаковке</span><br />Зубы чище с каждым<br />приемом пищи</p>
                    <p class="pTwo">3D DentaDefense&trade;<br />Уменьшает образования<br />зубного камня до 80% *</p>
                </div>
                <div class="exclusive">
                	<img src="<?php echo Yii::app()->theme->baseUrl?>/images/pack.png" />
                	<p>Эксклюзивный продукт Eukanuba</p>
                    Разработанные совместно с экспертами в области здоровья полости рта из Oral B® и Crest® гранулы Eukanuba 3D DentaDefense™ удаляют налет с зубов, кроме того, они содержат чистящие микрокристаллы, позволяющие существенно сократить отложения зубного камня даже в труднодоступных местах, сохраняя зубы здоровыми..<br />
					<small>*Результаты у разных собак могут варьироваться<br />
					**Кроме рационов для щенков</small>
                </div>
                <div class="effect">
                    <div>
                    	<img src="<?php echo Yii::app()->theme->baseUrl?>/images/crest.png" />
                        ЧИСТИТ
                    </div>
                    <p><img src="<?php echo Yii::app()->theme->baseUrl?>/images/oral1.png" /><img src="<?php echo Yii::app()->theme->baseUrl?>/images/oral2.png" /></p>
                      Эффективно очищает налет с зубов во время жевательных движений.
                </div>
                <div class="effect">
                    <div>
                    	<img src="<?php echo Yii::app()->theme->baseUrl?>/images/oralB.png" />
                        ЗАЩИЩАЕТ
                    </div>
                    <p><img src="<?php echo Yii::app()->theme->baseUrl?>/images/oral3.png" /><img src="<?php echo Yii::app()->theme->baseUrl?>/images/oral4.png" /></p>
                     Гранулы 3D DentaDefense™ удаляют зубной камень даже в труднодоступных местах во время и после еды
                </div>
                <div class="effect">
                    <div class="oneLine">
                        ПОДДЕРЖИВАЕТ
                    </div>
                    <p><img src="<?php echo Yii::app()->theme->baseUrl?>/images/oral5.png" /></p>
                     Кальций необходим для формирования и поддержания здоровья зубов 
                </div>
                <div class="why">
                	<h3>Почему здоровье полости рта так важно? </h3>
                   Знаете ли вы, что к трем годам почти 85% собак сталкиваются с проблемами полости рта? Ваша собака, как и вы, может страдать от образования налета и зубного камня.
                </div>
                <div class="why">
                	<h3>Три шага к здоровым зубам и деснам</h3>
                    Следующие профилактические меры помогут вашей собаке сохранить зубы и десны здоровыми:
					<BR/><BR/>
                    <ul>
                    	<li>Профессиональный осмотр и чистка зубов в ветеринарной клинике</li>
                        <li>Ежедневная чистка зубов</li>
                        <li>Рационы, обеспечивающие дополнительное очищение зубов, такие как корма Eukanuba 3D DentaDefense™</li>
                    </ul>
                </div>
        </div>
    </div>

</div>