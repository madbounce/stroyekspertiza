<?php
    $this->breadcrumbs=array(
        strip_tags($model->title),
    );

$background = $model->getPhoto();
?>
<?php
if($background):
    list($width, $height) = getimagesize(Yii::app()->request->hostInfo . $background);

    ?>
<style>
    .bigDog.about2{
        background: url('<?php echo $background?>') left no-repeat;
        width: <?php echo $width?>px; ;
        height: <?php echo $height?>px;
        top:0px;
    }
</style>
<?php endif; ?>

<div class="<?php echo $model->name?>">

    <div class="bigDog best">
        <h1><?php echo $model->title; ?></h1>
    </div>


    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 12,'view'=>'textClickable')); ?>
 <!--  <div class="middle DentaDefense">
            <div class="rightSide black">
  				<div class="aboutEkanuba">
                	<p>ABOUT THE EUKANUBA WORLD CHALLENGE</p>
                    The American Kennel Club (AKC), the Federation Cynologique Internationale (FCI) and
                    Eukanuba share a common passion: to showcase the excellence of dogs.<br /><br />
                    The Eukanuba World Challenge offers top dogs from around the world the unique 
                    opportunity to participate in the AKC/Eukanuba National Championship in California. All eligible dogs may compete to win the $50,000 prize for Best in Show.<br /><br />
                    Dogs also compete for the title of Eukanuba World Challenge Champion, with a $10,000
                    prize; additional prizes are $3,000 for the runner-up and $2,000 for first runner-up.<br /><br />
                    In 2011, the 42 selected dogs, representing countries from around the globe, receive an 
                    allexpenses paid* trip to compete at the 2011 AKC/Eukanuba National Championship and/or the 2011 Eukanuba World Challenge at the Orange County Convention Center in Orlando, Florida, USA, December 17th � 18th, 2011<br />
                    <small>* Terms and conditions apply.</small>
                </div>      
                <div class="aboutEkanuba ie8">
                	<p>2011 EUKANUBA WORLD CHALLENGE<br />RULES & REGULATIONS</p>
                    Terms AND CONDITIONS FOR DOG OWNERS WISHING TO PARTICIPATE IN THE
                    2011 EUKANUBA WORLD CHALLENGE<br /><br />
                    PRINT THIS PAGE<br /><br />
                    The national kennel club of each country will be responsible for nominating its country�s top dog plus one reserve dog. The following terms outline the 2011 Eukanuba World<br /><br />
                    <a href="/">GENERAL TERMS AND CONDITIONS ></a>
                </div>                
            </div>
        </div> -->
        

    <div class="fix_page">
        <div class="staticwrap">
            <?php echo $model->content; ?>
        </div>
    </div>

</div>