<?php
    $this->breadcrumbs=array(
        strip_tags($model->title),
    );
?>


<div class="<?php echo $model->name?> ">

    <div>
        <div class="food">
                        <img src="<?php echo Yii::app()->theme->baseUrl?>/images/petFood2.png" class="red">
                        <ul class="firstUl">
                                <li><a href="<?=Yii::app()->baseUrl;?>/site/buymap" class="pack">Где купить? </a></li>
                                <?php $this->renderPartial('_inc/vets_menu');?>
                            </li>
                        </ul>
        </div>
        
    </div>

    <div class="fix_page">
        <div class="staticwrap">
                <div class="AboutFood green">
                <div class="shortInfo">
                    <div class="leftSide">
                    <img src="<?php echo Yii::app()->theme->baseUrl?>/images/veterenary.png" class="veterenary">
                        <div class="renalCond green">RESTRICTED CALORIE</div>
                        <div class="liverCond green">RECOVERY</div>
                        <img src="<?php echo Yii::app()->theme->baseUrl?>/images/dogAndCat2.png" class="red">
                        
                        <div class="advantage green">
                            <ul>
                                <li>Отличное состояние кожи и шерсти</li>
                <li>Оптимальное пищеварение</li>
                                <li>Сильная имунная система</li>
                <li>Здоровые зубы и десны</li>
                                </ul>
                        </div>
                    </div>
                    <div class="rightSide">
                    <h2>RESTRICTED CALORIE ПРИ ОЖИРЕНИИ</h2>
                                        <h2>Показания</h2>

<ul class="bonus">
<li>Ожирение и избыточный вес</li>
<li>Ожирение и диабет</li>
<li>Проблемы с суставами и их подвижностью, для которых актуальна потеря веса</li>
<li>Дисплазия тазобедренного сустава</li>
<li> Остеоартрит</li>
</ul>

                    </div>
                    <br clear="all">

                    
                    <?php echo  $model->content; ?>

            </div>
            

        </div>
    </div>
    </div>
</div>