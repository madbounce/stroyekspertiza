<?php
    $this->breadcrumbs=array(
        strip_tags($model->meta_title),
    );

$background = $model->getPhoto();
?>
<?php
if($background):
    list($width, $height) = getimagesize(Yii::app()->request->hostInfo . $background);

    ?>
<style>
    .bigDog.about2{
        background: url('<?php echo $background?>') left no-repeat;
        width: <?php echo $width?>px; ;
        height: <?php echo $height?>px;
        top:0px;
    }
</style>
<?php endif; ?>

<div class="<?php echo $model->name?>">

    <div class="bigDog man">
        <h1><?php echo $model->title; ?></h1>
    </div>


    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 18,'view'=>'textClickable')); ?>


    <div class="fix_page">

		  <div class="middle DentaDefense cat">

            <div class="rightSide welcomeBlack">

                <div class="catsDiv">
                	<div class="leftCat dog3" style="width: 257px;margin-right: 10px;">
					<img src="<?php echo Yii::app()->theme->baseUrl ?>/images/welcome1.png" width="251" height="158"/>
                        <h4>Заводчики</h4>
                        Откройте для себя полезную информацию, познавательные статьи новейшие достижения в сфере питания
собак, способные развить лучшие качества породы.<br /><br />
                        <a href="/articles/search/breeders">Статьи для заводчиков</a>
						<a href="/page/partners">  Наши партнеры среди заводчиков</a><br />
						<a href="/page/club"> Клуб заводчиков</a><br />
						<a href="/products/dog/breeders">Продукция для заводчиков</a><br />
						<a href="/page/eukanuba_world_challenge">Мировое первенство Eukanuba</a><br />
						
                    </div>
                    <div class="rightCat dog3">
					<img src="<?php echo Yii::app()->theme->baseUrl ?>/images/welcome2.png" width="251" height="158"/>
                        <h4>Ветеринары</h4>
                        Узнайте, как Eukanuba помогает ветеринарам с помощью питания улучшить общее
здоровье собак и кошек.<br /><br />
                       
						<a href="/page/pri_ozhirenii" style="margin-top: 18px;">Restricted Calorie при ожирении</a><br />
						<a href="/page/vospalitel'nye_zabolevanija_kozhi"> Dermatosis при воспалительных заболеваниях кожи</a><br />
						<a href="/page/Podvizhnost_sustavov"> Joint Mobility при заболеваниях суставов</a><br />
						<a href="/page/zabolevanija_pochek"> Renal при заболеваниях почек</a><br />
						<a href="/page/kontrol_vesa_pri_diabete"> Weight /Diabetic Control для контроля веса при диабете</a><br />
						<a href="/page/pri_kishechnyh_rasstrojstvah">Intestinal при кишечных расстройствах</a><br />
						<a href="/page/mochekamennaja_bolezn"> Urinary Struvite & Urinary Oxalate при мочекаменной болезни струвитного или оксалатного типа</a><br />

                    </div>
                </div>
            </div>
        </div>
		
		
		<div class="staticwrap">
            <?php echo $model->content; ?>
        </div>
    </div>

</div>