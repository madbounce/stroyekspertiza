<div class="header_image landing_page">
    <div class="content_header">
        <div class="text_header">
            <h3>Подготовьте его для правильного старта в жизнь. </h3>
            <p>
            <!-- Все, что вам нужно знать, чтобы помочь вашего новому члену семьи <br/> стать великолепной собакой -->
            100% полноценный и сбалансированный корм Eukanuba - лучший старт жизни
            </p>

        </div>

        <div class="header_right_menu">
            <ul>
                <li><a href="/articles/default/puppy">СТАТЬИ</a></li>
                <li><a href="http://www.youtube.com/channel/UCp6hEh5cS8p9r1SgylxbFyQ" target="_blank">ВИДЕО О ВОСПИТАНИИ</a></li>
                <li><a href="/products/dog/puppy">ПОДБОР КОРМА</a></li>
                <li><a href="/page/nutrition">ПИТАНИЕ ЩЕНКОВ</a></li>
                <li><a href="/page/shoppinglist">ЛИСТ ПОКУПОК</a></li>
                <li><a href="/page/dog_breed_selector" class="active">НАЙДИТЕ СВОЮ ИДЕАЛЬНУЮ ПОРОДУ</a></li>
                <li><a href="/page/breedersrecommended">РЕКОМЕНДАЦИИ ЗАВОДЧИКОВ</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="content_iner">
    <ul class="landing_page_top_list">

        <li>
            <span>
                <h4>РУКОВОДСТВО ПО ВЫРАЩИВАНИЮ ЩЕНКА</h4>
                <p class="red">Всё, что нужно знать о щенке</p>
            </span>

            <div>
                <img src="<?php echo Yii::app()->theme->baseUrl?>/images/guide.png" alt="">
            </div>
            <p>Статьи о здоровье, обучении и уходе. Как помочь щенку на начальном этапе жизни.</p>
            <button>СКАЧАТЬ</button>
        </li>

        <li>
            <span>
                <h4>ПИТАНИЕ EUKANUBA </h4>
                <p class="red">Полноценное и сбалансированное питание для вашего питомца</p>
            </span>

            <div>
                <img src="<?php echo Yii::app()->theme->baseUrl?>/images/pic_22.png" alt="">
            </div>
            <p>
                Eukanuba поддерживает здоровый иммунитет, полученый с материнским молоком.
            </p>
            <!-- <p>Мы поддерживаем здоровый иммунитет, полученный с материнским молоком.</p> -->
            <button onclick="window.location.href = '/products/dog/puppy'">КАТАЛОГ ПРОДУКЦИИ</button>
        </li>

        <li>
            <span>
                <h4>ПРИСОЕДИНЯЙТЕСЬ К НАМ В СОЦИАЛЬНЫХ СЕТЯХ</h4>
                <p class="red">Ставьте лайки и обменивайтесь опытом</p>
            </span>

            <div>
                <img src="<?php echo Yii::app()->theme->baseUrl?>/images/share.png" alt="">
            </div>
            <p>Присоединяйтесь к нашему сообществу и используйте полученный опыт.</p>
            <button onclick="window.open('https://www.facebook.com/EukanubaRussia?fref=ts','_blank')">ПРИСОЕДИНИТЬСЯ</button>
        </li>
    </ul>
    
    <span class="title_landing_page">
        <h3>ВЕДУЩИЕ ЗАВОДЧИКИ РЕКОМЕНДУЮТ ВАШЕМУ ЩЕНКУ КОРМА EUKANUBA</h3>
        <p class="red">Чтобы узнать, почему они доверяют Eukanuba,<a href="#">НАЖМИТЕ СЮДА</a></p>
    </span>

    <div class="landing_page_video">
                <?php
        /**
         * Created by JetBrains PhpStorm.
         * User: slavik
         * Date: 19.11.13
         * Time: 14:22
         * To change this template use File | Settings | File Templates.
         */
            Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jwplayer/jwplayer.js', CClientScript::POS_HEAD);
        ?>

        <div id="video" style="width: 100%; height: 300px"></div>

        <script type="text/javascript">
            $(document).ready(function(){
                jwplayer("video").setup({
                    width: 'auto',
                    playlist: [
                        {
                            file: "http://www.youtube.com/watch?v=9O3tqPGdhoQ",
                            image: "http://i1.ytimg.com/vi/9O3tqPGdhoQ/mqdefault.jpg",
                            title: "Чувствительность щенка",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 11/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=dowkhHcQo1o",
                            image: "http://i1.ytimg.com/vi/dowkhHcQo1o/mqdefault.jpg",
                            title: "Как найти хорошего заводчика",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 5/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=YNY9UQdlxY4",
                            image: "http://i1.ytimg.com/vi/YNY9UQdlxY4/mqdefault.jpg",
                            title: "Основы дисциплины",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 4/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=tSdxqVLGI4c",
                            image: "http://i1.ytimg.com/vi/tSdxqVLGI4c/mqdefault.jpg",
                            title: "Как подготовиться к появлению щенка",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 2/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=s-HQJE5dDm4",
                            image: "http://i1.ytimg.com/vi/s-HQJE5dDm4/mqdefault.jpg",
                            title: "Охотничьи собаки",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 13/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=cHGoww6b-Ts",
                            image: "http://i1.ytimg.com/vi/cHGoww6b-Ts/mqdefault.jpg",
                            title: "Основные команды",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 7/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=-gWWJSFLxY0",
                            image: "http://i1.ytimg.com/vi/-gWWJSFLxY0/mqdefault.jpg",
                            title: "Кормление щенка",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 3/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=7QSuHFQCBqU",
                            image: "http://i1.ytimg.com/vi/7QSuHFQCBqU/mqdefault.jpg",
                            title: "Питание щенка",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 1/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=evWn6QC4oEA",
                            image: "http://i1.ytimg.com/vi/evWn6QC4oEA/mqdefault.jpg",
                            title: "Здоровье вашего щенка",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 10/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=rXsODzmZkrI",
                            image: "http://i1.ytimg.com/vi/rXsODzmZkrI/mqdefault.jpg",
                            title: "Четвероногие топ-модели",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 12/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=WxioH-SOYrQ",
                            image: "http://i1.ytimg.com/vi/WxioH-SOYrQ/mqdefault.jpg",
                            title: "Уход за щенком",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 9/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=n18PSABAOV4",
                            image: "http://i1.ytimg.com/vi/n18PSABAOV4/mqdefault.jpg",
                            title: "Как научить щенка общаться",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 8/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=XZeT_Kv7NQA",
                            image: "http://i1.ytimg.com/vi/XZeT_Kv7NQA/mqdefault.jpg",
                            title: "Как приучить собаку к клетке или переноске",
                            description:'Программа из цикла Eukanuba TV "Обучение щенка", эпизод 6/14'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=pKQQcVryg10",
                            image: "http://i1.ytimg.com/vi/pKQQcVryg10/mqdefault.jpg",
                            title: "Eukanuba 3 D DentaDefense"
                        },
                        {
                            file: "http://www.youtube.com/watch?v=9q8sU-a7EQU",
                            image: "http://i1.ytimg.com/vi/9q8sU-a7EQU/mqdefault.jpg",
                            title: "3 D DentaDefense Eukanuba"
                        },
                        {
                            file: "http://www.youtube.com/watch?v=28qrgL7Jm2Q",
                            image: "http://i1.ytimg.com/vi/28qrgL7Jm2Q/mqdefault.jpg",
                            title: "Полезные пребиотики Iams",
                            description:'Корм для кошек Iams содержит полезные пребиотики, которые устраняют проблемы пищеварения вашей кошки, и она чувствует себя здоровой и счастливой.'
                        },
                        {
                            file: "http://www.youtube.com/watch?v=jj2uvUbZ5IA",
                            image: "http://i1.ytimg.com/vi/jj2uvUbZ5IA/mqdefault.jpg",
                            title: "Eukanuba - система защиты зубов для собаки"
                        }
                    ],
                    listbar: {
                        position: 'right',
                        size: 320
                    }
                });
            });
        </script>
    </div>

    <ul class="landing_page_foot_list">

        <li>
            <div><img src="<?php echo Yii::app()->theme->baseUrl?>/images/puppy_articles.png" alt=""></div>
            <h3>СТАТЬИ О ЩЕНКАХ</h3>
            <p class="red">Уход, обучение, здоровье, питание…</p>
            <p>Обо всем, начиная с появления щенка в доме до рекомендаций по укрреплению его здоровья</p>
            <button onclick="window.location.href = '/articles/default/puppy'">УЗНАЙТЕ БОЛЬШЕ</button>
        </li>

        <li>
            <div><img src="<?php echo Yii::app()->theme->baseUrl?>/images/breedmatch.png" alt=""></div>
            <h3>ВАШ ИДЕАЛЬНЫЙ <br/> ЧЕТВЕРОНОГИЙ ДРУГ</h3>
            <p class="red">Найдите породу, которая вам подходит!</p>
            <p>Ответьте на несколько вопросов, чтобы подобрать щенка, характер которого соответствует вашему!</p>
            <button onclick="window.location.href = '/page/dog_breed_selector'">НАЧАТЬ ПОИСК</button>
        </li>

        <li>
            <div><img src="<?php echo Yii::app()->theme->baseUrl?>/images/shopping_list.png" alt=""></div>
            <h3>ЗА ПОКУПКАМИ</h3>
            <p class="red">Все для радушного приема.</p>
            <p>Позаботьтесь о том, чтобы у щенка было все, чтобы обжиться в новом доме.</p>
            <button onclick="window.location.href = 'shoppinglist'">УЗНАЙТЕ БОЛЬШЕ</button>
        </li>

        <li>
            <div><img src="<?php echo Yii::app()->theme->baseUrl?>/images/newsletter.png" alt=""></div>
            <h3>НОВОСТНАЯ РАССЫЛКА</h3>
            <p class="red">Полезные рекомендации по уходу за щенком</p>
            <p>Поскольку потребности щенка от месяца к месяцу меняются, вам необходимо  все знать о них</p>
            <button onclick="window.location.href = '/site/joinPuppy'">ПОДПИСАТЬСЯ</button>
        </li>
    </ul>
</div>