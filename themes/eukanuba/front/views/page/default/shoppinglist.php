<div class="header_image shoppinglist_header">
    <div class="content_header">
        <div class="text_header">
            <h3>ЛИСТ ПОКУПОК</h3>
            <p>ВСЕ, <br/> ДЛЯ РАДУШНОГО ПРИЕМА</p>
        </div>

        <div class="header_right_menu">
            <ul>
                <li><a href="/articles/default/puppy">СТАТЬИ</a></li>
                <li><a target="_blank" href="http://www.youtube.com/channel/UCp6hEh5cS8p9r1SgylxbFyQ">ВИДЕО О ВОСПИТАНИИ</a></li>
                <li><a href="/products/dog/puppy">ПОДБОР КОРМА</a></li>
                <li><a href="/page/nutrition">ПИТАНИЕ ЩЕНКОВ</a></li>
                <li><a href="/page/shoppinglist" class="active">ЛИСТ ПОКУПОК</a></li>
                <li><a href="/page/dog_breed_selector">НАЙДИТЕ СВОЮ ИДЕАЛЬНУЮ ПОРОДУ</a></li>
                <li><a href="/page/breedersrecommended">РЕКОМЕНДАЦИИ ЗАВОДЧИКОВ</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="content_iner">
    <h2 class="main_title second">
        ЩЕНОК ТОЛЬКО ЧТО РАССТАЛСЯ С МАМОЙ, БРАТИКАМИ И СЕСТРЕНКАМИ? В ЭТОТ ПЕРИОД ЕМУ НУЖНА ОСОБАЯ ЗАБОТА. ПОЗАБОТЬТЕСЬ О ТОМ, ЧТОБЫ ВСЁ БЫЛО ГОТОВО ДЛЯ РАДУШНОГО ПРИЕМА ПИТОМЦА.
    </h2>

    <div class="shopping_list_bg">
        <div>
            <ul>
                <li><h3>Для кормления</h3></li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Миска для воды
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Миска для корма
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Корм
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Лакомства
                    </label>
                </li>
            </ul>

            <ul>
                <li><h3>Для игры</h3></li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Кости
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Пищащие мягкие игрушки
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Игрушки для жевания
                    </label>
                </li>
            </ul>
        </div>

        <div>
            
            <ul>
                <li><h3>Для ухода</h3></li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Шампунь
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Щетка и расческа
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Кусачки для когтей
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Зубная щетка и паста
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Салфетки
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Раствор для ухода за ушами
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Ватные ушные шарики
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Пятновыводитель/освежитель воздуха
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Средства от блох и клещей
                    </label>
                </li>
            </ul>

            <ul>
                <li><h3>Специально для щенка</h3></li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Поводок
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Адресник
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Клетка
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Манеж
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Пеленки
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Клетка для перевозки
                    </label>
                </li>

                <li>
                    <label>
                        <input type="checkbox" />
                        Подстилка/лежанка
                    </label>
                </li>
            </ul>
        </div>
    </div>

    <button class="print_button" onclick="window.print();">Печать</button>
</div>

<style type="text/css" media='print'>
    #footer, #header,.header_image,.main_title.second, .print_button {display: none;}
</style>