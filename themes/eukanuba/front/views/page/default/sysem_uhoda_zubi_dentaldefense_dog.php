<?php
    $this->breadcrumbs=array(
        strip_tags($model->title),
    );
?>


<div class="<?php echo $model->name?>" style="margin-top: 45px;">

    <div class="vets_menu">
        <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 15,'view'=>'textClickable')); ?>

        <?php $this->renderPartial('_inc/_left_vhsd');?>
    </div>

    <div class="fix_page">
        <div class="staticwrap">
            <h1 class="title"><?php echo $model->title; ?></h1>

            <?php echo $model->content; ?>
        </div>
    </div>

</div>