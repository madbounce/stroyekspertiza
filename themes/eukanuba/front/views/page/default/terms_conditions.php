<?php
    $this->breadcrumbs=array(
        strip_tags($model->title),
    );
?>


<div class="<?php echo $model->name?>" style="margin-top: 45px;">

    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 12,'view'=>'textClickable')); ?>

    <div class="fix_page">
        <div class="staticwrap">
            <h1 class="title"><?php echo $model->title; ?></h1>

            <?php echo $model->content; ?>
        </div>
    </div>

</div>