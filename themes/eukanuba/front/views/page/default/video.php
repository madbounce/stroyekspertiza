<div class="header_image landing_page">
    <div class="content_header">
        <div class="text_header">
            <h3>ЛУЧШИЙ СТАРТ В ЖИЗНИ</h3>
            <p>Со 100% полноценным и сбалансированным кормом Eukanuba</p>
            <p>Все, что необходимо знать, чтобы вырастить здоровую, сильную собаку</p>
        </div>

        <div class="header_right_menu">
            <ul>
                <li><a href="/articles/default/puppy">СТАТЬИ</a></li>
                <li><a href="/page/video">ВИДЕО О ВОСПИТАНИИ</a></li>
                <li><a href="/products/dog/puppy">ПОДБОР КОРМА</a></li>
                <li><a href="/page/nutrition">ПИТАНИЕ ЩЕНКОВ</a></li>
                <li><a href="/page/shoppinglist">ЛИСТ ПОКУПОК</a></li>
                <li><a href="/page/dog_breed_selector" class="active">НАЙДИТЕ СВОЮ ИДЕАЛЬНУЮ ПОРОДУ</a></li>
                <li><a href="/page/breedersrecommended">РЕКОМЕНДАЦИИ ЗАВОДЧИКОВ</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="landing_page_video">
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: slavik
 * Date: 19.11.13
 * Time: 14:22
 * To change this template use File | Settings | File Templates.
 */
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jwplayer/jwplayer.js', CClientScript::POS_HEAD);
?>

<div id="video" style="width: 100%; height: 300px"></div>

<script type="text/javascript">
    $(document).ready(function(){
        jwplayer("video").setup({
            width: 'auto',
            playlist: [{
                file: "http://www.youtube.com/watch?v=9O3tqPGdhoQ",
                image: "http://i1.ytimg.com/vi/9O3tqPGdhoQ/mqdefault.jpg",
                title: "Чувствительность щенка"
            },{
                file: "http://www.youtube.com/watch?v=dowkhHcQo1o",
                image: "http://i1.ytimg.com/vi/dowkhHcQo1o/mqdefault.jpg",
                title: "Как найти хорошего заводчика"
            },{
                file: "http://www.youtube.com/watch?v=YNY9UQdlxY4",
                image: "http://i1.ytimg.com/vi/YNY9UQdlxY4/mqdefault.jpg",
                title: "Основы дисциплины"
            }],
            listbar: {
                position: 'right',
                size: 320
            }
        });
    });
</script>

</div>