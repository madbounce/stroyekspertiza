<?php
    $this->breadcrumbs=array(
        strip_tags($model->title),
    );

$background = $model->getPhoto();
?>
<?php
if($background):
    list($width, $height) = getimagesize(Yii::app()->request->hostInfo . $background);

    ?>
<style>
    .bigDog.about2{
        background: url('<?php echo $background?>') left no-repeat;
        width: <?php echo $width?>px; ;
        height: <?php echo $height?>px;
        top:0px;
    }
</style>
<?php endif; ?>

<div class="pages <?php echo $model->name?>">

    <?php if($background): ?>
    <div class="bigDog about2">
        <h1><?php echo $model->title; ?></h1>
    </div>
    <?php endif; ?>


    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 12,'view'=>'textClickable')); ?>


    <div class="fix_page">
        <div class="staticwrap">

            <?php if(!$background): ?>
                <h1 class="title"><?php echo $model->title; ?></h1>
            <?php endif; ?>

            <?php echo $model->content; ?>
        </div>
    </div>

</div>