<?php
    $this->breadcrumbs=array(
        strip_tags($model->title),
    );
?>


<div class="<?php echo $model->name?> ">

    <div>
		<div class="food">
						<img src="<?php echo Yii::app()->theme->baseUrl?>/images/petFood4.png" class="red">
						<ul class="firstUl">
								<li><a href="<?=Yii::app()->baseUrl;?>/site/buymap" class="pack">Где купить? </a></li>
								<?php $this->renderPartial('_inc/vets_menu');?>
							</li>
						</ul>
		</div>
        
    </div>

    <div class="fix_page">
        <div class="staticwrap">
			    <div class="AboutFood purple">
            	<div class="shortInfo">
                	<div class="leftSide">
					<img src="<?php echo Yii::app()->theme->baseUrl?>/images/veterenary.png" class="veterenary">
                    	<div class="renalCond purple">Dermatosis FP</div>
                        <div class="liverCond purple">RECOVERY</div>
						<img src="<?php echo Yii::app()->theme->baseUrl?>/images/dogAndCat4.png" class="red">
                        
                        <div class="advantage purple">
                        	<ul>
                                <li>Сильная имунная система</li>
                                <li>Здоровые зубы и десны</li>
                            </ul>
                        </div>
                    </div>
                    <div class="rightSide">
					<h2>ВОСПАЛИТЕЛЬНЫЕ ЗАБОЛЕВАНИЯ КОЖИ</h2>
					<h2>Показания</h2> <p><b>Аллергия / проблемы с кожей и шерстью</b></p> <ul class="bonus"> <li>Кожные заболевания, сопровождающиеся воспалением</li> <li>Зудящий дерматит (атопический, на укусы блох, контактный)</li> <li>Пищевая аллергия/ пищевая непереносимость</li> <li> Хронический наружный отит</li> <li> Перианальная фистула</li> </ul><br>

                    </div>
					<br clear="all">
					
					<?php echo  $model->content; ?>

            </div>
			    </div>

        </div>
    </div>

</div>