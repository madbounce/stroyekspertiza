<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'company-profile-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>

<div class="block_profile_freelance">
    <?php $this->widget('SelectActivitiesWidget', array('model' => $model, 'attribute' => 'activities')); ?>

    <div class="clear"></div>

    <p>
        <strong><?php echo $form->labelEx($model, 'name'); ?>:</strong><br/>
        <?php echo $form->textField($model, 'name'); ?>
        <?php echo $form->error($model, 'name'); ?>
    </p>

    <p>
        <strong><?php echo $form->labelEx(Yii::app()->user->model, 'company_post'); ?>:</strong><br/>
        <?php echo $form->textField(Yii::app()->user->model, 'company_post'); ?>
        <?php echo $form->error(Yii::app()->user->model, 'company_post'); ?>

    <p>
        <strong><?php echo $form->labelEx($model, 'phone'); ?>:</strong><br/>
        <?php echo $form->textField($model, 'phone'); ?>
        <?php echo $form->error($model, 'phone'); ?>
    </p>

    <p>
        <strong><?php echo $form->labelEx($model, 'email'); ?>:</strong><br/>
        <?php echo $form->textField($model, 'email'); ?>
        <?php echo $form->error($model, 'email'); ?>
    </p>

    <p>
        <strong><?php echo $form->labelEx($model, 'address'); ?>:</strong><br/>
        <?php echo $form->textField($model, 'address'); ?>
        <?php echo $form->error($model, 'address'); ?>
    </p>

    <p>
        <strong><?php echo $form->labelEx($model, 'website'); ?>:</strong><br/>
        <?php echo $form->textField($model, 'website'); ?>
        <?php echo $form->error($model, 'website'); ?>
    </p>

    <p>
        <strong><?php echo $form->labelEx($model, 'about'); ?>:</strong><br/>
        <?php echo $form->textArea($model, 'about'); ?>
        <?php echo $form->error($model, 'about'); ?>
    </p>

    <p>
        <strong><?php echo $form->labelEx($model, 'region_id'); ?>:</strong>
        <?php $this->widget('RegionWidget', array(
            'model' => $model,
            'attribute' => 'region_id'
        )); ?>
    </p>

    <p>
        <strong><?php echo CHtml::label(Yii::t('profile', 'Логотип'), ''); ?>:</strong><br/>
        <?php $this->widget('LogoUploadWidget', array(
            'model' => $model,
            'action' => array('/upload/logoCompany'),
            'multiple' => false,
        )); ?>
    </p>

    <p id="portfolio">
        <strong><?php echo CHtml::label(Yii::t('profile', 'Портфолио'), ''); ?>:</strong><br/>
        <?php $this->widget('portfolio.components.widgets.PortfolioUploadWidget', array(
            'model' => $model,
            'action' => array('/upload/companyPortfolios'),
        )); ?>
    </p>
</div>

<?php echo CHtml::submitButton(Misc::t('Сохранить'), array('class' => 'but_green saveProfileButton')); ?>

<?php $this->endWidget(); ?>