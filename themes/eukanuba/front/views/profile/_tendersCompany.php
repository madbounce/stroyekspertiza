<div class="b-tenders-column">
    <?php $this->widget('StatusWidget', array('model' => $model, 'attribute' => 'status', 'grid' => 'tender-grid-personal')); ?>
    <div class="clear"></div>

    <h3 class="b-tenders-title"><?php echo Yii::t('profile', 'Персональные тендеры'); ?></h3>
    <?php $this->widget('CustomGridView', array(
        'id' => 'tender-grid-personal',
        'dataProvider' => $model->profileStatusSearch(),
        'columns' => $columns,
    )); ?>
</div>

<div class="b-tenders-column b-tender-right-col">
    <?php $this->widget('StatusWidget', array('model' => $model, 'attribute' => 'status', 'grid' => 'tender-grid-company')); ?>
    <div class="clear"></div>

    <h3 class="b-tenders-title"><?php echo Yii::t('profile', 'Тендеры компании'); ?></h3>
    <?php $this->widget('CustomGridView', array(
        'id' => 'tender-grid-company',
        'dataProvider' => $model->profileStatusSearch(true),
        'columns' => $columns,
    )); ?>
</div>