<?php $this->widget('StatusWidget', array('model' => $model, 'attribute' => 'status', 'grid' => 'tender-grid')); ?>
<div class="clear"></div>

<h3 class="b-tenders-title"><?php echo Yii::t('profile', 'Персональные тендеры'); ?></h3>
<?php $this->widget('CustomGridView', array(
    'id' => 'tender-grid',
    'dataProvider' => $model->profileStatusSearch(),
    'columns' => $columns,
)); ?>