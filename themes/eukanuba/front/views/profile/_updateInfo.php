<div class="inviteblock">

    <?php /** @var $form CActiveForm */ ?>
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'update-info-form',
        'action' => array('/profile/updateInfo'),
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )); ?>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'first_name', array('class' => 'l1')); ?>
        <?php echo $form->textField($model, 'first_name', array('class' => 'input200')); ?>
        <?php echo $form->error($model, 'first_name'); ?>
    </div>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'last_name', array('class' => 'l1')); ?>
        <?php echo $form->textField($model, 'last_name', array('class' => 'input200')); ?>
        <?php echo $form->error($model, 'last_name'); ?>
    </div>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'email', array('class' => 'l1')); ?>
        <?php echo $form->textField($model, 'email', array('class' => 'input200')); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'contact_phone', array('class' => 'l1')); ?>
        <?php echo $form->textField($model, 'contact_phone', array('class' => 'input200')); ?>
        <?php echo $form->error($model, 'contact_phone'); ?>
    </div>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'skype', array('class' => 'l1')); ?>
        <?php echo $form->textField($model, 'skype', array('class' => 'input200')); ?>
        <?php echo $form->error($model, 'skype'); ?>
    </div>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'icq', array('class' => 'l1')); ?>
        <?php echo $form->textField($model, 'icq', array('class' => 'input200')); ?>
        <?php echo $form->error($model, 'icq'); ?>
    </div>

    <?php $this->widget('AvatarUploadWidget', array(
        'model' => $model,
        'action' => array('/upload/avatar'),
        'multiple' => false,
    )); ?>

    <?php echo CHtml::link(Yii::t('profile', 'применить'), '#', array('class' => 'greenbtn sm', 'style' => 'margin-top:20px; width:101px;', 'submit' => '')) ?>

    <?php $this->endWidget(); ?>

</div>