<?php
$this->pageTitle = Yii::t('profile', 'Мои заявки');
?>

<h1 class="title_zayavka"><big><?php echo Yii::t('profile', 'Мои заявки'); ?></big></h1>

<div id="tenders" class="clearfix">
    <?php $this->widget('StatusWidget', array('model' => $model, 'attribute' => 'status', 'grid' => 'bid-grid')); ?>
    <div class="clear"></div>

    <?php $this->widget('CustomGridView', array(
        'id' => 'bid-grid',
        'dataProvider' => $model->profileStatusSearch(),
        'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'title',
                'value' => function($data) {
                    $output = '<div class="line_date">' . CHtml::encode($data->create_time) . '</div>';
                    $output .= CHtml::link(Yii::t('tender', 'Заявка № :id', array(':id' => $data->id)), array('/bid/view', 'id' => $data->id));
                    return $output;
                },
                'htmlOptions' => array('style' => 'width: 420px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'cost',
                'value' => function($data) {
                    $output = '<div class="line_date"><div class="gray">' . Yii::t('tender', 'На тендер') . '</div>';
                    $output .= CHtml::link(CHtml::encode($data->tender->title), array('/tender/view', 'id' => $data->tender->id)) . '</div>';
                    return $output;
                },
                'htmlOptions' => array('style' => 'width: 185px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'finish_time',
                'value' => function($data) {
                    $output = '<div class="line_date"><div class="gray">' . Yii::t('tender', 'Итоги подводятся') . '</div>';
                    $output .= '<div>' . $data->tender->end_time . '</div>';
                    return $output;
                },
                'htmlOptions' => array('style' => 'width: 240px'),
            ),
            array(
                'type' => 'raw',
                'name' => 'cost',
                'value' => function($data) {
                    $output = '<div class="line_date"><div class="gray">' . Yii::t('tender', 'Макс. бюджет') . '</div>';
                    $output .= $data->formattedCost . '</div>';
                    return $output;
                },
                'htmlOptions' => array('style' => 'width: 125px', 'class' => 'lasttd'),
            ),
        ),
    )); ?>
</div>