<div class="mail_successful no_builder">
    <big><?php echo Yii::t('profile', 'Вы не подтвердили Ваш E-mail'); ?></big>
    <p>
        <?php echo Yii::t('profile', 'Проверьте почту и следуйте указаниям в письме.'); ?><br>
    </p>
</div>