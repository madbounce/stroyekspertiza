<?php /** @var $form CActiveForm */ ?>

<script type="text/javascript">
    $(function() {
        $("form#company-profile-form").hide();

        $(".profile-type-radio:even").addClass('freelancer');
        $(".profile-type-radio:odd").addClass('company');

        $(".profile-type-radio.freelancer").click(function() {
            $("form#company-profile-form").hide();
            $("form#freelancer-profile-form").show();
        });

        $(".profile-type-radio.company").click(function() {
            $("form#freelancer-profile-form").hide();
            $("form#company-profile-form").show();
        });
    });
</script>

<h2 class="title_zayavka"><big><?php echo Yii::t('profile', 'Профиль'); ?></big></h2>

<p>
    <?php echo CHtml::label(Yii::t('profile', 'Я работаю от имени'), 'type'); ?>:
    <?php echo CHtml::radioButtonList('type', Profile::TYPE_FREELANCER, Profile::getTypeOptions(), array('separator' => ' ', 'class' => 'profile-type-radio')); ?>
</p>

<?php $this->renderPartial('_formFreelancer', array('model' => $freelancer)); ?>

<?php $this->renderPartial('_formCompany', array('model' => $company)); ?>