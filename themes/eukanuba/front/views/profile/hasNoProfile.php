<div class="mail_successful no_builder">
    <big><?php echo Yii::t('profile', 'У вас не создан профиль подрядчика'); ?></big>
    <p>
        <?php echo Yii::t('profile', 'Создайте профиль подрядчика, чтобы другие компании могли пригласить вас в тендер или смотреть вашу контактную информацию'); ?><br>
        <?php echo CHtml::link(Yii::t('profile', 'создать'), array('/profile/create'), array('class' => 'but_green')); ?>
    </p>
</div>