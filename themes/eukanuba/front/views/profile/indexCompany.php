<div class="cont_left">
    <h2><?php echo Yii::t('profile', 'Я работаю от имени'); ?></h2>
    <div class="my_brand">
        <div class="man_rating">3.5<?php // TODO Рейтинг ?></div>
        <?php //$this->widget('LogoWidget', array('model' => $model->profile, 'width' => 153, 'height' => 153)); ?>

        <big><?php echo CHtml::encode($model->profile->name); ?></big>
        <div class="my_brand_content">
            <?php if (!empty($model->profile->region)): ?>
                <?php echo Yii::t('profile', 'регион: :region', array(':region' => CHtml::encode($model->profile->region->name))); ?><br>
            <?php endif; ?>

            <?php if (!empty($model->company_post)): ?>
                <?php echo Yii::t('profile', 'моя должность: :post', array(':post' => CHtml::encode($model->company_post))); ?><br>
            <?php endif; ?>

            <?php if (!empty($model->profile->website)): ?>
                <?php echo Yii::t('profile', 'cайт: :website', array(':website' => CHtml::encode($model->profile->website))); ?><br>
            <?php endif; ?>

            <?php if (!empty($model->profile->phone)): ?>
                <?php echo Yii::t('profile', 'телефон: :phone', array(':phone' => CHtml::encode($model->profile->phone))); ?><br>
            <?php endif; ?>

            <?php if (!empty($model->profile->email)): ?>
                <?php echo Yii::t('profile', 'e-mail: :email', array(':email' => CHtml::encode($model->profile->email))); ?><br>
            <?php endif; ?>
        </div>
        <div class="clear"></div>

        <?php echo CHtml::link(Yii::t('profile', 'Перейти к профилю'), Helper::profileUrl($model->profile), array('class' => 'button bg_green')); ?>
        <?php if (Yii::app()->user->checkAccess('manageCompany', array('profile' => $model->profile))): ?>
            <?php echo CHtml::link(Yii::t('profile', 'Редактировать'), array('/profile/update'), array('class' => 'button bg_blue')); ?>
        <?php endif; ?>
        <?php // TODO Операции (удалить) ?>
        <?php if (Yii::app()->user->checkAccess('company', array('profile' => $model->profile))): ?>
            <?php echo CHtml::link(Yii::t('profile', 'Уволиться'), array('/profile/fire'), array('class' => 'button bg_grey fire')); ?>
        <?php endif; ?>
        <?php if (Yii::app()->user->checkAccess('manageCompany', array('profile' => $model->profile))): ?>
            <?php echo CHtml::link(Yii::t('profile', 'Удалить компанию'), '#', array('class' => 'button bg_red')); ?>
        <?php endif; ?>
    </div>

    <h2><?php echo Yii::t('profile', 'О компании'); ?></h2>
    <p class="company-description">
        <?php echo CHtml::encode($model->profile->about); ?>
    </p>
</div>

<div class="cont_right">
    <h2><?php echo Yii::t('profile', 'Сотрудники компании'); ?></h2>

    <?php $parts = array_chunk($model->profile->employees, 2, true); ?>
    <?php foreach ($parts as $employees): ?>
        <?php foreach ($employees as $employee): ?>
            <div class="block_worker">
                <div class="worker_img">
                    <?php //$this->widget('AvatarWidget', array('model' => $employee, 'width' => 49, 'height' => 49)); ?>
                </div>
                <div class="worker_info">
                    <big><?php echo CHtml::encode($employee->full_name); ?></big><br>
                    <span class="grey"><?php echo CHtml::encode($employee->company_post); ?></span>
                    <div>
                        <?php echo CHtml::encode($employee->email); ?><br>
                        <?php echo CHtml::encode($employee->contact_phone); ?>
                    </div>

                    <?php if (Yii::app()->user->checkAccess('manageCompany', array('profile' => $model->profile))): ?>
                        <?php echo CHtml::link(Yii::t('profile', 'уволить'), '#', array('class' => 'employee-delete red', 'data-id' => $employee->id)); ?>
                        <?php echo CHtml::link(Yii::t('profile', 'редактировать'), '#', array('class' => 'employee-update', 'data-id' => $employee->id)); ?>
                    <?php endif; ?>
                </div>
                <div class="clear"></div>
            </div>
        <?php endforeach; ?>

        <?php if (count($employees) == 2): ?>
            <div class="clear"></div>
        <?php endif; ?>
    <?php endforeach; ?>

    <?php if (Yii::app()->user->checkAccess('manageCompany', array('profile' => $model->profile))): ?>
        <div class="block_worker">
            <div class="worker_img"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/worker_new.gif" width="50" height="57" alt=""></div>
            <div class="worker_new">
                <?php echo CHtml::link(Yii::t('profile', 'Пригласить сотрудника'), '#', array('class' => 'employee-create')); ?>
            </div>
            <div class="clear"></div>
        </div>

        <?php //$this->widget('EmployeeWidget'); ?>
    <?php endif; ?>

    <div class="clear"></div>

    <?php// $this->widget('PortfolioWidget', array('model' => $model->profile)); ?>
</div>
<div class="clear"></div>


<?php Yii::app()->clientScript->registerScript('fire-dialog', '
$("a.fire").live("click", function() {
    $("#fire-dialog").dialog("open");
    return false;
});
$("#fire-dialog a.yes").live("click", function() {
    var url = ' . CJavaScript::encode(CHtml::normalizeUrl(array('/profile/fire'))) . ';
    window.location.href = url;
    return false;
});
$("#fire-dialog a.no").live("click", function() {
    $("#fire-dialog").dialog("close");
    return false;
});
', CClientScript::POS_READY); ?>

<?php $this->beginWidget('DialogWidget', array(
    'id' => 'fire-dialog',
    'title' => '<h1 class="popup-title title" style="font-size: 20px; text-align: center;">' . Yii::t('profile', 'Увольнение из компании') . '</h1>',
    'options' => array('width' => 320),
)); ?>

<div id="fire-dialog-content" class="inviteblock">
    <?php echo Yii::t('profile', 'Вы действительно хотите уволиться из компании?'); ?>
    <div class="buttonswrap pop">
        <?php echo CHtml::link(Yii::t('profile', 'Да'), '#', array('class' => 'chose yes')) ?>
        <?php echo CHtml::link(Yii::t('profile', 'Нет'), '#', array('class' => 'otklonit no')) ?>
    </div>
</div>

<?php $this->endWidget(); ?>