<div class="cont_left">
    <h2><?php echo CHtml::encode($model->full_name); ?></h2>
    <div class="my_brand">
        <?php echo CHtml::link(Yii::t('profile', 'Перейти к профилю'), Helper::profileUrl($model->profile), array('class' => 'button bg_green')); ?>
        <?php echo CHtml::link(Yii::t('profile', 'Редактировать'), array('/profile/update'), array('class' => 'button bg_blue')); ?>
    </div>

    <h2><?php echo Yii::t('profile', 'О фрилансере'); ?></h2>
    <p>
        <?php echo CHtml::encode($model->profile->about); ?>
    </p>
</div>
<div class="cont_right">
    <?php //$this->widget('PortfolioWidget', array('model' => $model->profile)); ?>
</div>
<div class="clear"></div>