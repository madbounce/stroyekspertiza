<?php $this->pageTitle = Misc::t('Поздравляем! Вы в b2all'); ?>

<div class="fix_page">
    <div class="new_work">
        <big><?php echo Misc::t('Поздравляем! Вы в b2all<br> <span>начните работать с системой</span>'); ?></big>
        <div class="as_customer">
            <div><?php echo Misc::t('Как заказчик'); ?></div>
            <?php echo CHtml::link(Misc::t('Создайте список подрядчиков'), '#'); ?><br>
            <?php echo CHtml::link(Misc::t('Создайте тендер'), array('/tender/create')); ?>
        </div>
        <div class="as_builder">
            <div><?php echo Misc::t('Как подрядчик'); ?></div>
            <?php echo CHtml::link(Misc::t('Разместите информацию о компании'), array('/profile/create')); ?><br>
            <?php //echo CHtml::link(Misc::t('Предложите свои услуги'), '#'); ?>
        </div>
        <div class="clear"></div>
    </div>
</div>