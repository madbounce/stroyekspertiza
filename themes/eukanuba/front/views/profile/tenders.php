<?php
$this->pageTitle = Yii::t('profile', 'Мои тендеры');
?>

<h1 class="title_zayavka"><big><?php echo Yii::t('profile', 'Мои тендеры'); ?></big></h1>

<?php $columns = array(
    array(
        'type' => 'raw',
        'name' => 'title',
        'value' => function($data) {
            $output = '<div class="line_date">' . CHtml::encode($data->create_time) . '</div>';
            $output .= CHtml::link(CHtml::encode($data->title), array('/tender/view', 'id' => $data->id));
            return $output;
        },
        'htmlOptions' => array('style' => 'width: 420px'),
    ),
    array(
        'type' => 'raw',
        'name' => 'cost',
        'value' => function($data) {
            $output = '<div class="line_date"><div class="gray">' . Yii::t('tender', 'Макс. бюджет') . '</div>';
            $output .= $data->formattedCost . '</div>';
            return $output;
        },
        'htmlOptions' => array('style' => 'width: 185px'),
    ),
    array(
        'type' => 'raw',
        'name' => 'finish_time',
        'value' => function($data) {
            $output = '<div class="line_date"><div class="gray">' . Yii::t('tender', 'Итоги подводятся') . '</div>';
            $output .= '<div>' . $data->end_time . '</div>';
            return $output;
        },
        'htmlOptions' => array('style' => 'width: 240px'),
    ),
    array(
        'type' => 'raw',
        'value' => function($data) {
            $output = '<div class="line_date"><div>' . Yii::t('tender', 'Заявки: :count', array(':count' => count($data->actualBids))) . '</div>';
            // TODO Количество новых заявок
            $output .= '<div class="green">' . Yii::t('tender', '(новых :count)', array(':count' => count($data->actualBids))) . '</div>';
            return $output;
        },
        'htmlOptions' => array('style' => 'width: 125px', 'class' => 'lasttd'),
    ),
); ?>

<div id="tenders" class="clearfix">
    <?php if (Yii::app()->user->model->isCompany): ?>
        <?php $this->renderPartial('_tendersCompany', array('model' => $model, 'columns' => $columns)); ?>
    <?php else: ?>
        <?php $this->renderPartial('_tendersFreelancer', array('model' => $model, 'columns' => $columns)); ?>
    <?php endif; ?>
</div>