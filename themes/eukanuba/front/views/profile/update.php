<h2 class="title_zayavka"><big><?php echo Yii::t('profile', 'Редактирование профилья'); ?></big></h2>

<?php if ($model->isFreelancer): ?>
    <?php $this->renderPartial('_formFreelancer', array('model' => $model)); ?>
<?php else: ?>
    <?php $this->renderPartial('_formCompany', array('model' => $model)); ?>
<?php endif; ?>