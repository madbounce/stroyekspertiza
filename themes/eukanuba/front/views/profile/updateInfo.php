<?php /** @var $form CActiveForm */ ?>

<h2 class="title_zayavka">
    <big><?php echo Yii::t('profile', 'Введите личные данные'); ?></big>
</h2>

<div class="block_profile_freelance">
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'update-info-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>

    <?php echo $form->labelEx($model, 'first_name'); ?>:
    <div class="clear"></div>
    <?php echo $form->textField($model, 'first_name', array('class' => 'input_txt')); ?><br/>
    <?php echo $form->error($model, 'first_name'); ?>

    <?php echo $form->labelEx($model, 'last_name'); ?>:
    <div class="clear"></div>
    <?php echo $form->textField($model, 'last_name', array('class' => 'input_txt')); ?><br/>
    <?php echo $form->error($model, 'last_name'); ?>

    <?php echo $form->labelEx($model, 'email'); ?>:
    <div class="clear"></div>
    <?php echo $form->textField($model, 'email', array('class' => 'input_txt', 'AUTOCOMPLETE' => 'OFF')); ?><br/>
    <?php echo $form->error($model, 'email'); ?>

    <?php echo $form->labelEx($model, 'contact_phone'); ?>:
    <div class="clear"></div>
    <?php echo $form->textField($model, 'contact_phone', array('class' => 'input_txt')); ?><br/>
    <?php echo $form->error($model, 'contact_phone'); ?>

    <?php echo $form->labelEx($model, 'skype'); ?>
    <div class="clear"></div>
    <?php echo $form->textField($model, 'skype', array('class' => 'input_txt')); ?><br/>
    <?php echo $form->error($model, 'skype'); ?>

    <?php echo $form->labelEx($model, 'icq'); ?>
    <div class="clear"></div>
    <?php echo $form->textField($model, 'icq', array('class' => 'input_txt')); ?><br/>
    <?php echo $form->error($model, 'icq'); ?>

    <?php echo CHtml::submitButton(Yii::t('profile', 'Сохранить'), array('class' => 'but_green saveProfileButton')); ?>

<?php $this->endWidget(); ?>
</div>