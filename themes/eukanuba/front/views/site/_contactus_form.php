<?php
    /**
     * @var $form CActiveForm
     */
?>
<div id="contact">
    <div class="eukanuba">
        <? if ($model->step == 1): ?>
            <h1><span style="color:#E73097;">ШАГ 1:</span> Расскажите о Вашем питомце</h1>
        <? else: ?>
            <h1><span style="color:#E73097;">ШАГ 2:</span> Контактная информация</h1>
        <? endif?>

        <p class="req"><span class="pink">*</span>Обязательное поле</p>


        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'tell_about_pet',
            'action' => '/site/contactus#step2',
            'enableAjaxValidation' => false,
        )); ?>

        <span style="color:#F00"><?php //echo $form->errorSummary($model); ?></span>


<?=$form->hiddenField($model, 'step')?>
<div <? if ($model->step == 2) echo 'style="display:none;"'?>>

    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'pet_type'); ?>
            </div>
        </div>
        <div class="question_field">
            <?=$form->radioButtonList($model, 'pet_type',Expert::getPetTypeList(),array('class'=>'radiobtn')); ?>
        </div>
        <?=$form->error($model, 'pet_type',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>
    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'pet_breed'); ?>
            </div>
        </div>
        <div class="question_field">
            <?=$form->textField($model, 'pet_breed'); ?>
        </div>
        <?=$form->error($model, 'pet_breed',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>
    <?php /*
    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'pet_id'); ?>
            </div>
        </div>
        <div class="question_field">
            <div class="petPoroda"><?=$form->dropDownList($model, 'pet_id', CHtml::listData(DicPets::all(), 'id', 'title'), array('empty' => 'Выберите породу', 'class'=>'selectBlock pet'), array('onchange' => '')); ?></div>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#Expert_pet_id').change(function () {
                        if (($('#Expert_pet_id').val() > 0) && ($('#Expert_question').val().length > 0))
                            $("#submit_button").removeAttr("disabled");
                        else
                            $("#submit_button").attr("disabled", "disabled");
                    })
                    $('#Expert_question').keypress(function () {
                        if (($('#Expert_pet_id').val() > 0) && ($('#Expert_question').val().length > 0))
                            $("#submit_button").removeAttr("disabled");
                        else
                            $("#submit_button").attr("disabled", "disabled");
                    })
//								$("#submit_button").removeAttr("disabled");
                });
            </script>
        </div>
        <?=$form->error($model, 'pet_id'); ?>
        <div class="clear"></div>
    </div>
     */ ?>
    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'cat_name'); ?>
            </div>
        </div>
        <div class="question_field">
            <?=$form->textField($model, 'cat_name'); ?>
        </div>
        <?=$form->error($model, 'cat_name',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>


    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'cat_birth'); ?>
            </div>
        </div>
        <div class="question_field">
            <div class="mBirthDay"><?=$form->dropDownList($model, 'catBMonth', Misc::monthes(), array('empty' => 'Месяц', 'style' => 'width: 80px', 'class'=>'selectBlock month')); ?></div>
            <div class="yBirthDay"><?=$form->dropDownList($model, 'catBYear', Misc::years(1988), array('empty' => 'Год', 'style' => 'width: 80px', 'class'=>'selectBlock year')); ?></div>
        </div>
        <?=$form->error($model, 'cat_birth',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>

    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'cat_big_id'); ?>
            </div>
        </div>
        <div class="question_field">
            <div id="CatPoroda" class="petPoroda"><?=$form->dropDownList($model, 'cat_big_id', Expert::getCatBigList(), array('empty' => 'Выберите вес', 'class'=>'selectBlock weight')); ?></div>
            <div id="DogPoroda" style="display: none" class="petPoroda"><?=$form->dropDownList($model, 'cat_big_id', Expert::getDogBigList(), array('empty' => 'Выберите вес', 'class'=>'selectBlock weight')); ?></div>
        </div>
        <?=$form->error($model, 'cat_big_id',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>

    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'cat_food'); ?>
            </div>
        </div>
        <div class="question_field">
            <?=$form->textField($model, 'cat_food'); ?>
        </div>
        <?=$form->error($model, 'cat_food',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>

    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'question'); ?>
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i><font size="1">Максимум 4000 символов</font></i>
            </div>
        </div>
        <div class="question_field">
            <?=$form->textArea($model, 'question'); ?>
        </div>
        <?=$form->error($model, 'question',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>
</div>
<a id="step2"></a>

<div <? if ($model->step == 1) echo 'style="display:none;"'?>>
    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'firstname'); ?>
            </div>
        </div>
        <div class="question_field">
            <?=$form->textField($model, 'firstname'); ?>
        </div>
        <?=$form->error($model, 'firstname',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>

    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'lastname'); ?>
            </div>
        </div>
        <div class="question_field">
            <?=$form->textField($model, 'lastname'); ?>
        </div>
        <?=$form->error($model, 'lastname',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>

    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'region_id'); ?>
            </div>
        </div>
        <div class="question_field">
            <div class="state"><?=$form->dropDownList($model, 'region_id', DicRegions::listAll(), array('empty' => 'Выберите регион', 'class'=>'selectBlock region')); ?></div>
        </div>
        <?=$form->error($model, 'region_id',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>

    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'city'); ?>
            </div>
        </div>
        <div class="question_field">
            <?=$form->textField($model, 'city'); ?>
        </div>
        <?=$form->error($model, 'city',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>

    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'address'); ?>
            </div>
        </div>
        <div class="question_field">
            <?=$form->textField($model, 'address'); ?>
        </div>
        <?=$form->error($model, 'address',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>

    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'answer_email'); ?>
            </div>
        </div>
        <div class="question_field">
            <?=$form->textField($model, 'answer_email'); ?>
        </div>
        <?=$form->error($model, 'answer_email',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>

    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'answer_phone'); ?>
            </div>
        </div>
        <div class="question_field">
            <?=$form->textField($model, 'answer_phone'); ?>
        </div>
        <?=$form->error($model, 'answer_phone',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>

    <div class="form_question">
        <div class="question_label">
            <div class="label_container">
                <?=$form->labelEx($model, 'birth'); ?>
            </div>
        </div>
        <div class="question_field">
            <div class="mBirthDay"><?=$form->dropDownList($model, 'bMonth', Misc::monthes(), array('empty' => 'Месяц', 'style' => 'width: 80px', 'class'=>'selectBlock month')); ?></div>
            <div class="yBirthDay"><?=$form->dropDownList($model, 'bYear', Misc::years(1950), array('empty' => 'Год', 'style' => 'width: 80px', 'class'=>'selectBlock year')); ?></div>
        </div>
        <?=$form->error($model, 'bMonth',array('class'=>'error')); ?>
        <?=$form->error($model, 'bYear',array('class'=>'error')); ?>
        <div class="clear"></div>
    </div>
    <div class="verify">
        <?=$form->labelEx($model, 'verifyCode'); ?>
        <? $this->widget('CCaptcha'); ?>
        <?=$form->textField($model, 'verifyCode'); ?>
        <?=$form->error($model, 'verifyCode',array('class'=>'error')); ?>

    </div>
</div>



    <? if ($model->step == 1): ?>
        <div class="form_question">
            <div class="question_field" style="float:right;">
                <?php echo CHtml::submitButton(Yii::t('app', 'Далее'), array('class' => 'btn', 'id' => 'submit_button')); ?>
            </div>
            <div class="clear"></div>
        </div>
    <? else: ?>
        <div class="form_question">
            <div class="question_field" style="float:right;">
                <?php echo CHtml::submitButton(Yii::t('app', 'Отправить'), array('class' => 'btn', 'id' => 'submit_button')); ?>
            </div>
            <div class="clear"></div>
        </div>
    <? endif?>

<?php $this->endWidget(); ?>

</div>

</div>


<script type="text/javascript">
    $('document').ready(
            function(){
                $('input:radio').click(
                        function(){
                            rrr = $('#tell_about_pet .radiobtn:checked').val();
                            //alert(rrr);
                            if (rrr == 'cat') {
                                $('#DogPoroda').hide();
                                $('#CatPoroda').show();
                            }
                            if (rrr == 'dog') {
                                $('#CatPoroda').hide();
                                $('#DogPoroda').show();
                            }
                        }
                );
            }
    );

</script>