<div id="have-pets"<?php echo (!$model->havePet) ? ' style="display: none"' : '';?>>
    <p>
        <?=Misc::t("Расскажите нам о Ваших питомцах, чтобы мы могли высылать Вам новости в соответствии с вашими интересами.");?>
    </p>

    <div class="_col last_" id="hasCat">
        <div class="select-count-pets">
            <label for="cat2" style="float: left;margin-right: 5px;">У меня дома: <small class="req"></small></label>

            <div id="havePets"><?php echo $form->dropDownList($model, 'catCount',
            array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
            ),
            array(
                'id' => 'cat2',
                'style' => 'float: left',
				'class' => 'selectBlock'
            )
        );
            ?></div>
            <?=$form->labelEx($model, 'catCount',  array('style' => 'float: left; ', 'label' => '&nbsp;животных')); ?>
        </div>

        <div class="pets-info">
            <div class="pet-info-row" id="b-pet1">
                <p style="float: left;margin-right: 30px;margin-top: 0;" class="mypet">
                    <?php echo $form->labelEx($model, 'pet1_name'); ?>
                    <?php echo $form->textField($model, 'pet1_name', array('maxlength' => 50, 'class' => 'txt')); ?>
                    <?php echo $form->error($model, 'pet1_name',array('class'=>'error')); ?>
                </p>
                <p class="birthdaycol">
                    <?php echo $form->labelEx($model, 'pet1_birth'); ?>
                    <br>
                    <div class="mBirthDay"><?php echo $form->dropDownList($model, 'pet1Month', Misc::monthes(), array('empty' => 'Месяц', 'class' => 'txt selectBlock month')); ?></div>
                   <div class="yBirthDay"> <?php echo $form->dropDownList($model, 'pet1Year', Misc::years(1988), array('empty' => 'Год', 'class' => 'txt selectBlock year')); ?></div>
                    <?php echo $form->error($model, 'pet1_birth',array('class'=>'error')); ?>
                </p>
            </div>
            <div class="pet-info-row" id="b-pet2" <?php echo ($model->catCount < 2) ? ' style="display: none"' : '';?>>
                <hr>
                <p style="float: left;margin-right: 30px;margin-top: 0; " class="mypet">
                    <?php echo $form->labelEx($model, 'pet2_name'); ?> <span class="required">*</span><a href="#" class="remove-pet"> Удалить </a>
                    <?php echo $form->textField($model, 'pet2_name', array('maxlength' => 50, 'class' => 'txt')); ?>
                    <?php echo $form->error($model, 'pet2_name',array('class'=>'error')); ?>
                </p>
                <p class="birthdaycol">
                    <?php echo $form->labelEx($model, 'pet2_birth'); ?> <span class="required">*</span>
                    <br>
                    <div class="mBirthDay"><?php echo $form->dropDownList($model, 'pet2Month', Misc::monthes(), array('empty' => 'Месяц', 'class' => 'txt selectBlock month')); ?></div>
                     <div class="yBirthDay"><?php echo $form->dropDownList($model, 'pet2Year', Misc::years(1988), array('empty' => 'Год', 'class' => 'txt selectBlock year')); ?></div>
                    <?php echo $form->error($model, 'pet2_birth',array('class'=>'error')); ?>
                </p>
            </div>
            <div class="pet-info-row" id="b-pet3" <?php echo ($model->catCount < 3) ? ' style="display: none"' : '';?>>
                <hr>
                <p style="float: left;margin-right: 30px;margin-top: 0; >
                    <?php echo $form->labelEx($model, 'pet3_name'); ?> <span class="required">*</span><a href="#" class="remove-pet"> Удалить </a>
                    <?php echo $form->textField($model, 'pet3_name', array('maxlength' => 50, 'class' => 'txt')); ?>
                    <?php echo $form->error($model, 'pet3_name',array('class'=>'error')); ?>
                </p>
                <p class="birthdaycol">
                    <?php echo $form->labelEx($model, 'pet3_birth'); ?> <span class="required">*</span>
                    <br>
                    <div class="mBirthDay"><?php echo $form->dropDownList($model, 'pet3Month', Misc::monthes(), array('empty' => 'Месяц', 'class' => 'txt selectBlock month')); ?></div>
                     <div class="yBirthDay"><?php echo $form->dropDownList($model, 'pet3Year', Misc::years(1988), array('empty' => 'Год', 'class' => 'txt selectBlock year')); ?></div>
                    <?php echo $form->error($model, 'pet3_birth',array('class'=>'error')); ?>
                </p>
            </div>
            <div class="pet-info-row" id="b-pet4" <?php echo ($model->catCount < 4) ? ' style="display: none"' : '';?>>
                <hr>
                <p style="float: left;margin-right: 30px;margin-top: 0;>
                    <?php echo $form->labelEx($model, 'pet4_name'); ?> <span class="required">*</span><a href="#" class="remove-pet"> Удалить </a>
                    <?php echo $form->textField($model, 'pet4_name', array('maxlength' => 50, 'class' => 'txt')); ?>
                    <?php echo $form->error($model, 'pet4_name',array('class'=>'error')); ?>
                </p>
                <p class="birthdaycol">
                    <?php echo $form->labelEx($model, 'pet4_birth'); ?> <span class="required">*</span>
                    <br>
                    <div class="mBirthDay"><?php echo $form->dropDownList($model, 'pet4Month', Misc::monthes(), array('empty' => 'Месяц', 'class' => 'txt selectBlock month')); ?></div>
                    <div class="yBirthDay"><?php echo $form->dropDownList($model, 'pet4Year', Misc::years(1988), array('empty' => 'Год', 'class' => 'txt selectBlock year')); ?></div>
                    <?php echo $form->error($model, 'pet4_birth',array('class'=>'error')); ?>
                </p>
            </div>
            <div class="add-pet">
                <a href="#" id="addPet" style="display: inline;">Добавить</a>
            </div>
        </div>

    </div>


    <div id="finish-have-pet">
        <p class="content" id="content2">
            <?php echo Misc::t("Чем больше мы знаем о Вас, тем лучше мы можем составить предложение.");?>
        </p>


        <p class="buttonbar" id="buttonbar">
            <input type="submit" src="<?=Yii::app()->theme->baseUrl;?>/images/btn_answer2.gif" alt="Ответить еще на несколько вопросов" title="Ответить еще на несколько вопросов" id="submit_step2" class="button" value="Ответить еще на несколько вопросов">
            или
            <input type="submit" class="button" value="Закончить" id="finish_step2" title="Закончить">
        </p>
    </div>


</div>