<div id="dont-have-pets" <?php echo ($model->havePet !== 0) ? ' style="display: none"' : '';?>>
    <p>
        <?=Misc::t("Вы подумываете о том, чтобы завести нового домашнего любимца? Мы вышлем Вам полезные советы и информацию по питанию домашних животных, уходу за ними, дрессировке и обучению. Завести какого питомца Вам бы хотелось? Можно указать несколько вариантов.");?>
    </p>

    <div class="bottom-check">

        <?=$form->checkBox($model, 'petTypeCat', array('id' => 'petTypeOption-2')); ?>
        <label for="petTypeOption-2" class="checkboxLabel"><?=Misc::t("Кошка");?></label>

        <?=$form->checkBox($model, 'petTypeKitten', array('id' => 'petTypeOption-4')); ?>
        <label for="petTypeOption-4" class="checkboxLabel"><?=Misc::t("Котенок");?></label>

        <?=$form->checkBox($model, 'petTypeDog', array('id' => 'petTypeOption-1')); ?>
        <label for="petTypeOption-1" class="checkboxLabel"><?=Misc::t("Собака");?></label>

        <?=$form->checkBox($model, 'petTypePup', array('id' => 'petTypeOption-3')); ?>
        <label for="petTypeOption-3" class="checkboxLabel"><?=Misc::t("Щенок");?></label>

    </div>

    <div class="bottom-check last_">
        <label for="selectone"
               class="none"><?=Misc::t("Как скоро вы планируете завести нового питомца?");?></label>
        <?=$form->dropDownList($model, 'petBuyPlan', Subscribers::getPetByPlanList(), array(
            'id' => 'selectone',
            'empty' => Misc::t('Выберите один вариант'),
            'class' => 'selectBlock'
        )); ?>

    </div>

    <div id="finish-dont-have-pet">
        <p class="buttonbar1s" id="buttonbar1s">
            <?=CHtml::submitButton(Misc::t('Закончить'), array('class' => 'button bgbtn', 'alt' => Misc::t('Закончить'), 'title' => Misc::t('Закончить'))); ?>
        </p>
    </div>
</div>