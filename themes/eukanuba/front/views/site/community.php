<div id="MainContent" class="community">
	<div class="copy-top-border">
		<img src="<?=Yii::app()->theme->baseUrl;?>/images/top-border-dog.png" alt="" width="956" height="8" class="topborder" title="">
	</div>
	<div id="white-container"><div class="clear-all">&nbsp;</div>
			
<div id="breadcrumb"><a title="Home" href="/">Главная</a> / Сообщество любителей кошек</div>
		<div class="clear-all">&nbsp;</div>
		<div class="page-header">
	
		<h1>Откройте для себя<br/>
		Сообщество Iams</h1>
		<p>Присоединяйтесь и общайтесь в наших группах в социальных сетях и делитесь
		новостями. Станьте частью мира любителей кошек и Iams.</p>
		<!--a href="#" title="See Details" class="know_more">Узнать больше</a-->
		</div>
		<div id="bodycontent_area">
			
			<div class="facebook">
				
				<div class="part_pop" style=" width: 47%;">
				<p>
					<img src="<?=Yii::app()->theme->baseUrl;?>/images/facebook_thumb.png" alt="Facebook" title="Facebook">
				</p>
				<p style="margin-bottom: 10px;"><strong>Станьте поклонником Iams на Facebook.</strong></p>
<p>Делитесь фотографиями домашних любимцев и историями о них, получайте новости о программах Iams, которые помогают кошкам во всем мире.</p>
				<p class="become-a-fan">
				<a href="<?=Config::get('socialFbLink');?>" title="Присоединиться" target="_blank">Присоединиться</a>
				</p>
				</div>
				<div class="part_pop" style=" margin-left: 30px; width: 43%;">
				<p>
					<img src="<?=Yii::app()->theme->baseUrl;?>/images/vk_thumb.png" alt="VK" title="Vkontakte">
				</p>
				<p style="margin-bottom: 10px;"><strong>Станьте поклонником Iams вКонтакте.</strong></p>
<p>Участвуйте в акциях, получайте последние новости, общайтесь на интересные темы в дружном сообществе Iams вКонтакте.</p>
				<p class="become-a-fan vk">
				<a href="<?=Config::get('socialVkLink');?>" title="Присоединиться" target="_blank">Присоединиться</a>
				</p>
				</div>
			</div>
			<!-- end facebook section-->
			
			
			
			<div class="clear-all">&nbsp;</div>
		</div>
		<div class="clear-all">&nbsp;</div>
	</div>
	<!-- Copy Content end-->
</div>
