<?php
    $this->breadcrumbs=array(
        'Свяжитесь с нами',
    );

?>

    <script>
        $(document).ready(function(){$('.selectBlock.pet').sSelect({defaultText: '<?php echo Misc::t('Выберите породу')?>'})});
        $(document).ready(function(){$('.selectBlock.weight').sSelect({defaultText: '<?php echo Misc::t('Выберите вес')?>'})});
        $(document).ready(function(){$('.selectBlock.year').sSelect({defaultText: '<?php echo Misc::t('Год')?>'})});
        $(document).ready(function(){$('.selectBlock.month').sSelect({defaultText: '<?php echo Misc::t('Месяц')?>'})});
		$(document).ready(function(){$('.selectBlock.region').sSelect({defaultText: '<?php echo Misc::t('Выберите регион')?>'})});
    </script>
	
	<div class="contactus">

		<?php $this->widget('banner.components.widget.rotator', array('categoryId' => 14,'view'=>'textClickable')); ?>

		<div class="fix_page">
			<h1 class="heading">Свяжитесь с нами</h1>

			<div class="intro">
				<?php if($page)
					echo $page->content;
				?>
			</div>
			<br/><br/><br/>
			<h3 class="heading">Расскажите о Вашем питомце</h3>
			<?php $this->renderPartial('_contactus_form',array('model'=>$model)); ?>
		</div>
	
	</div>