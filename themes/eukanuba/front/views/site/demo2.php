<?php
Yii::import('ext.EGMap.*');
$gMap = new EGMap();
$gMap->zoom = 9;
$gMap->width = 555;
$gMap->height = 400;

$mapTypeControlOptions = array(
    'position'=> EGMapControlPosition::RIGHT_TOP,
    'style'=>EGMap::MAPTYPECONTROL_STYLE_HORIZONTAL_BAR
);
$gMap->mapTypeId = EGMap::TYPE_ROADMAP;
$gMap->mapTypeControlOptions = $mapTypeControlOptions;
//var_dump($center)
/*if (isset($center))
    $gMap->setCenter($center['lat'], $center['lng']);
else
    $gMap->setCenter(55.75, 37.50);*/

$icon = new EGMapMarkerImage(Yii::app()->theme->baseUrl . '/images/zoo2.png');

$icon->setSize(32, 37);
$icon->setAnchor(16, 16.5);
$icon->setOrigin(0, 0);

$infoBoxOptions = array(
    'alignBottom' => true,
    'boxClass' => '"info-box"',
    'boxStyle' => null,
    'closeBoxMargin' => '"2px"',
    'enableEventPropagation' => null,
    'infoBoxClearance' => null,
    'isHidden' => null,
    'pane' => '"floatPane"',
    'boxStyle' => array(
        'width' => '"250px"',
        // 'opacity' => 0.85,
        'zIndex' => 99999
    ),
    'pixelOffset' => new EGMapSize(-50, -25),
    'infoBoxClearance' => new EGMapSize(1, 1),
    'closeBoxMargin' => '"10px 2px 2px 2px"'

);



foreach($model as $shops)
{
    foreach ($shops as $shop)
    {
        //$info_window_a = new EGMapInfoWindow('<div class="map-box">' . $shop->title  . '</br>' . $shop->address . '</div>');
        $boxContent = '<div class="popover-map top">
            <div class="arrow-map"></div>
            <h3 class="popover-title-map">' . $shop->title . '</h3>
            <div class="popover-content-map">
              <p>' . $shop->address . '</p>
            </div>
          </div>';

        $infoBox = new EGMapInfoBox($boxContent);
        $infoBox->setOptions($infoBoxOptions);

        $update = false; //($shop->city_id == 63);

        if ((isset($shop->latitude) && $shop->latitude && isset($shop->longitude) && $shop->longitude) && (!$update)){
            $marker = new EGMapMarker($shop->latitude, $shop->longitude, array('title' => $shop->title  . ' - ' . $shop->city->title.', '.$shop->address,'icon'=>$icon));
            $marker->addHtmlInfoBox($infoBox);
            $gMap->addMarker($marker);
        } else {
            //Если координаты не установлены пытаемся определить их по адресу ресторана
            $rest_address = $shop->city->title . ',' . $shop->address;

            if (!empty($rest_address)) {
                // Create geocoded address
                $geocoded_address = new EGMapGeocodedAddress($rest_address);
                $geocoded_address->geocode($gMap->getGMapClient());
                $lat = $geocoded_address->getLat();
                $lng = $geocoded_address->getLng();

                if($lat && $lng){
                    $marker = new EGMapMarker($lat, $lng, array('title' => $shop->title  . ' - ' . $shop->city->title.', '.$shop->address,'icon'=>$icon));
                    $marker->addHtmlInfoBox($infoBox);
                    $gMap->addMarker($marker);
                    //save to db
                    $shop->latitude = $lat;
                    $shop->longitude = $lng;
                    $shop->save(false);

                }
            }
        }

    }
}
//$center = $gMap->getMarkersCenterCoord();
//$gMap->setCenter($center->getLatitude(), $center->getLongitude());
$gMap->centerAndZoomOnMarkers(0.2, 10);
$gMap->renderMap();

