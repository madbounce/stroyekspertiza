<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery-ui-tabs-rotate.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('slider', '
    $("#featured_slider").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
');
?>
<div class="top_content_block">
    <div class="center_wrap">
        <h1>Бесплатные входящие звонки в Россию на номера 800, 804 по всему миру</h1>
        <div id="featured_slider" >
            <ul class="ui-tabs-nav">
                <li class="ui-tabs-nav-item" id="nav-fragment-1"><span class="active_img"><a href="#fragment-1"><span class="bold">Софтфон "ServicePhone"</span> <p>Удобный интерфейс, легкость в пользовании, все необходимые свойства</p></a></span></li>
                <li class="ui-tabs-nav-item" id="nav-fragment-2"><span class="active_img"><a href="#fragment-2"><span class="bold">100 рублей на вашем балансе!</span> <p>Купи IP-телефон со стикером 8800 и звони сразу!</p></a></span></li>
                <li class="ui-tabs-nav-item" id="nav-fragment-3"><span class="active_img"><a href="#fragment-3"><span class="bold">Прямой городской номер</span> <p>Для сокращения расходов на межгород, Это технология передачи голоса через сеть Интернет.</p></a></span></li>
            </ul>

            <!-- First Content -->
            <div id="fragment-1" class="ui-tabs-panel">
                <div class="info_pic"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/top_pic_1.png" alt="" /></div>
                <div class="info" >
                    <h2>Софтфон "ServicePhone"</h2>
                    <p>С <span class="s36">5</span>-й минуты бесплатно</p>
                    <a href="#" class="more_btn">Подробнее</a>
                </div>
            </div>

            <!-- Second Content -->
            <div id="fragment-2" class="ui-tabs-panel ui-tabs-hide">

            </div>

            <!-- Third Content -->
            <div id="fragment-3" class="ui-tabs-panel ui-tabs-hide">

            </div>

        </div>
    </div>
</div>