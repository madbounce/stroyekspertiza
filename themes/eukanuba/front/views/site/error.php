<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = Misc::t('Iams - Ошибка');
?>
<div id="breadcrumb"><a title="Главная" href="/">Главная</a> / Ошибка</div>
<div class="_col left-callouts-nav">
    <p class="green_curve"><img src="<?=Yii::app()->theme->baseUrl;?>/images/img_cat_detail.jpg" alt="" title=""></p>

    <div class="left-callouts"> <img src="<?=Yii::app()->theme->baseUrl;?>/images/need_more_help.gif" alt="Need More Help?" title="Need More Help?">
        <p>Не можете найти то, что ищете?</p>
        <div>
            <a title="Связаться" href="/site/contactus" id="contact_btn">Связаться</a>
        </div>
    </div>
</div>

<div class="_col txt-content">
    <h1 class="heading"><?php echo Misc::t('Ошибка :code', array(':code' => $code)); ?></h1>
    <?php echo CHtml::encode($message); ?>
</div>
<div class="clear-all">&nbsp;</div>