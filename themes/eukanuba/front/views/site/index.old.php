<?php
/* @var $this SiteController */

$this->pageTitle = Misc::t('IpLife');
?>
<?php $this->renderPartial('slider'); ?>
<div class="content_block">
    <div class="center_wrap">
        <h1><?php echo CHtml::link(Misc::t('Зарегистрируйся'), array('/auth/register'), array('class' => 'dialog-registration', 'title' => Misc::t('Регистрация'))); ?> <?php echo Misc::t('прямо сейчас и получи со скидкой'); ?>:</h1>
        <ul class="discounts_block">
            <li>
                <div class="discount_icon"><img src="<?php echo  Yii::app()->theme->baseUrl ?>/images/star_pic.png" /></div>
                <?php echo Misc::t('Номер в зоне 8 800 или 8 804'); ?>
            </li>
            <li>
                <div class="discount_icon"><img src="<?php echo  Yii::app()->theme->baseUrl ?>/images/five_pic.png" /></div>
                <?php echo Misc::t('Бесплатных номеров'); ?>
            </li>
            <li>
                <div class="discount_icon"><img src="<?php echo  Yii::app()->theme->baseUrl ?>/images/microphone_pic.png" /></div>
                <?php echo Misc::t('Запиши голосовое приветствие'); ?>
            </li>
            <li>
                <div class="discount_icon"><img src="<?php echo  Yii::app()->theme->baseUrl ?>/images/forwarding_pic.png" /></div>
                <?php echo Misc::t('Настройку переадресации'); ?>
            </li>
        </ul>
    </div>
</div>
<div class="features_block">
    <div class="center_wrap">
        <div class="feature">
            <h3><?php echo CHtml::link(Misc::t('«8800» Виртуальный офис'), '/#', array('title' => Misc::t('«8800» Виртуальный офис'))); ?></h3>
            <img src="<?php echo  Yii::app()->theme->baseUrl ?>/images/virtual_ofice_pic.png" alt="<?php echo Misc::t('«8800» Виртуальный офис')?>"/>
            <p><?php echo Misc::t('Виртальный офис поможет Вам быстро получить доступ к всем услугам.'); ?></p>
        </div>
        <div class="feature">
            <h3><?php echo CHtml::link(Misc::t('Прямой городской номер'), '/#', array('title' => Misc::t('Прямой городской номер'))); ?></h3>
            <img src="<?php echo  Yii::app()->theme->baseUrl ?>/images/phone_number_pic.png" alt="<?php echo Misc::t('Прямой городской номер')?>"/>
            <p><?php echo Misc::t('SIP-телефония поможет Вам быстро получить доступ к всем услугам');?></p>
        </div>
        <div class="feature">
            <h3><?php echo CHtml::link(Misc::t('«8800» Softphone'), '/#', array('title' => Misc::t('«8800» Softphone'))); ?></h3>
            <img src="<?php echo  Yii::app()->theme->baseUrl ?>/images/softphone_pic.png" alt="<?php echo Misc::t('«8800» Softphone')?>"/>
            <p><?php echo Misc::t('Softphone поможет Вам быстро получить доступ к всем услугам');?></p>
        </div>
        <div class="feature">
            <h3><?php echo CHtml::link(Misc::t('«8800» Виртуальная АТС'), '/#', array('title' => Misc::t('«8800» Виртуальная АТС'))); ?></h3>
            <img src="<?php echo  Yii::app()->theme->baseUrl ?>/images/ats_pic.png" alt="<?php echo Misc::t('«8800» Виртуальная АТС')?>"/>
            <p><?php echo Misc::t('Виртуаяльная АТС поможет Вам быстро получить доступ к всем услугам');?></p>
        </div>
    </div>
</div>
<div class="content_block">
    <div class="center_wrap">
        <div class="content_about">
            <div class="about_feature">
                <img src="<?php echo  Yii::app()->theme->baseUrl ?>/images/connect_pic.png" alt="<?php echo Misc::t('Как подключиться?')?>"/>
                <?php echo CHtml::link(Misc::t('Как подключиться?'), '/#', array('title' => Misc::t('Как подключиться?'))); ?>
            </div>
            <div class="about_feature">
                <img src="<?php echo  Yii::app()->theme->baseUrl ?>/images/price_pic.png" alt="<?php echo Misc::t('Тарифы')?>"/>
                <?php echo CHtml::link(Misc::t('Тарифы'), '/#', array('title' => Misc::t('Тарифы'))); ?>
            </div>
            <div class="about_feature">
                <img src="<?php echo  Yii::app()->theme->baseUrl ?>/images/partners_pic.png" alt="<?php echo Misc::t('Партнерская программа')?>"/>
                <?php echo CHtml::link(Misc::t('Партнерская программа'), '/#', array('title' => Misc::t('Партнерская программа'))); ?>
            </div>
        </div>
        <div class="share_icons">
			<div style="display: inline-block; overflow: hidden; ">
				<span class="share-text"  style="display: block; float: left; line-height: 25px;
     " ><?php echo Misc::t('Поделиться');?>:</span>
				<?php $this->widget('LikeWidget', array('url' => Yii::app()->createAbsoluteUrl('/'))); ?>
			</div>
		</div>
    </div>
</div>