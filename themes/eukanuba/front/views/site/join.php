<?php
    $this->breadcrumbs=array(
        Misc::t('Присоединиться'),
    );
?>
<script>
    $(document).ready(function(){$('.selectBlock.region').sSelect({defaultText: '<?php echo Misc::t('Выберите регион')?>'})});
    $(document).ready(function(){$('.selectBlock.month').sSelect({defaultText: '<?php echo Misc::t('Месяц')?>'})});
    $(document).ready(function(){$('.selectBlock.year').sSelect({defaultText: '<?php echo Misc::t('Год')?>'})});
</script>

<div class="news">
    <div class="allNews">

        <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 11,'view'=>'imageClickable')); ?>

        <div>
            <h2><?=Misc::t('Ваша дата рождения');?></h2>
            <?=Misc::t('Почему мы спрашиваем Вашу дату рождения? Потому что по закону есть ограничения, которые относятся к определенным возрастным группам. Поэтому мы обязаны попросить Вас указать дату рождения.');?>
        </div>
        <div class="ie8">
            <h2><?=Misc::t('Сохранность ваших персональных данных');?></h2>
            <?=Misc::t('Мы, как дистрибьютор обязуемся беречь и не разглашать Ваши персональные данные, которые мы получаем от Вас, наших клиентов.');?>
        </div>
    </div>
    <a href="<?php echo Yii::app()->createUrl('page/terms_conditions')?>" target="_blank" class="FindMore">
        <?=Misc::t('Наша политика');?>
    </a>
</div>


<div class="youInfo">

    <p>
        <?=Misc::t('Вступите в клуб Eukanuba, чтобы получать эксклюзивную информационную рассылку от наших ветеринаров и экспертов с полезными советами, новостями и предложениями. Информационная рассылка будет составлена с учетом возраста и потребностей Вашего питомца, чтобы Ваш любимец был счастливым и здоровым на каждом этапе жизни. Мы также будем информировать Вас о появлении новых продуктов.');?>
    </p>

    <? $form = $this->beginWidget('CActiveForm', array(
        'id' => 'regFormStep1',
        'enableAjaxValidation' => false,
    )); ?>

        <h2><?=Misc::t('ШАГ 1: ВАША ИНФОРМАЦИЯ');?></h2>
        <div class="textPOsition one">
            <?=$form->labelEx($model, 'first_name',array('class'=>'error_required')); ?>
            <?=$form->textField($model, 'first_name',array('maxlength'=>'50','class'=>'txt_error','inputContainer'=>'div.error')); ?>
            <?=$form->error($model, 'first_name',array('class'=>'error')); ?>
        </div>
        <div class="textPOsition">
            <?=$form->labelEx($model, 'last_name',array('class'=>'error_required')); ?>
            <?=$form->textField($model, 'last_name',array('maxlength'=>'50','class'=>'txt_error')); ?>
            <?=$form->error($model, 'last_name',array('class'=>'error')); ?>
        </div>
        <div class="textPOsition one">
            <?=$form->labelEx($model, 'address1',array('class'=>'error_required')); ?>
            <?=$form->textField($model, 'address1',array('maxlength'=>'100','class'=>'txt_error')); ?>
            <?=$form->error($model, 'address1',array('class'=>'error')); ?>
        </div>
        <div class="textPOsition">
            <?=$form->labelEx($model, 'address2',array('class'=>'error_required')); ?>
            <?=$form->textField($model, 'address2',array('maxlength'=>'100','class'=>'txt_error')); ?>
            <?=$form->error($model, 'address2',array('class'=>'error')); ?>
        </div>
        <div class="textPOsition one">
            <?=$form->labelEx($model, 'city',array('class'=>'error_required')); ?>
            <?=$form->textField($model, 'city',array('maxlength'=>'50','class'=>'txt_error')); ?>
            <?=$form->error($model, 'city',array('class'=>'error')); ?>
        </div>
        <div class="textPOsition">
            <div id="State">
            <?=$form->labelEx($model, 'region_id',array('class'=>'error_required')); ?>
            <?=$form->dropDownList($model, 'region_id', DicRegions::listAll(), array('empty' => Misc::t('Выберите регион'),'class'=>'selectBlock region')); ?>
            <?=$form->error($model, 'region_id',array('class'=>'error')); ?>
            </div>
        </div>

        <div class="line"></div>
        <div class="textPOsition one">
            <?=$form->labelEx($model, 'email',array('class'=>'error_required')); ?>
            <?=$form->textField($model, 'email',array('maxlength'=>'50','class'=>'txt_error')); ?>
            <?=$form->error($model, 'email',array('class'=>'error')); ?>
        </div>
        <div class="textPOsition">
            <?=$form->labelEx($model, 'd_birth',array('class'=>'error_required')); ?><br>
            <div id="BirthM"><?=$form->dropDownList($model, 'bMonth', Misc::monthes(), array('empty' => Misc::t('Месяц'), 'style' => 'width: 80px','class'=>'selectBlock month')); ?></div>
            <div id="BirthY"><?=$form->dropDownList($model, 'bYear', Misc::years(1950), array('empty' => Misc::t('Год'), 'style' => 'width: 80px','class'=>'selectBlock year')); ?></div>
            <?=$form->error($model, 'd_birth',array('class'=>'error')); ?>
        </div>
		<div class="line"></div>
        <div class="emailMe">
            <p><?=Misc::t('Получать предложения и новости по электронной почте');?></p>

            <div class="checkMail">
                <?=$form->checkBox($model, 'iams_subscribe',array('class'=>'ignore', 'checked' => 'checked')); ?>
                <?=$form->labelEx($model, 'iams_subscribe'); ?>
            </div>
            <div class="aboutCheck"><?=Misc::t('Да! Я хочу получать данные, включая специальные предложения и информацию о промо-акциях, регулярное информационное письмо, наполненное новостями о продуктах, и особых скидках, предназначенных именно для меня. Время от времени я также могу получать информацию об образцах продукции, возможностях исследований, а также данные о захватывающих программах, предлагаемых в содружестве с магазинами.');?></div>
        </div>

        <div style="margin-top: 20px;">
            <?php echo CHtml::submitButton(Misc::t('Далее'), array('class'=>'button bgbtn', 'alt'=>'Далее','title'=>'Далее')); ?>
        </div>

    <? $this->endWidget(); ?>
</div>