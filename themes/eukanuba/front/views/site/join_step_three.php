<?php
    $this->breadcrumbs=array(
        Yii::t('site','Присоединиться'),
    );
?>
<script type="text/javascript">

        $(document).ready(function(){
            $('#Subscribers_dryFoodFreq').sSelect();
            $('#Subscribers_wetFoodFreq').sSelect();
            $('#Subscribers_petFoodBuyWhen').sSelect();

            $('#Subscribers_dryFoodBrands_9').click(function(){
                if($('#Subscribers_dryFoodBrands_9').attr('checked') == 'checked') {
                    $('#Subscribers_dryFoodBrandsOther').prop('disabled', false);
                }
                else {
                    $('#Subscribers_dryFoodBrandsOther').attr('disabled','disabled');
                    $('#Subscribers_dryFoodBrandsOther').val('');
                }
            });
        });
    </script>

    <div class="news">
        <div class="allNews">
            <div>
                <h2><?=Misc::t('О Вашем питомце');?></h2>
                <?=Misc::t('Почему мы спрашиваем дату рождения Вашей кошки? Чем больше мы знаем о Вашем любимце, тем более точные советы и рекомендации по продуктам, подходящим Вашей кошке в соответствии с ее возрастом и потребностями, мы можем предложить Вашему вниманию. Все это - для того, чтобы помогать питомцам жить долго и счастливо.');?>
            </div>
            <div class="ie8">
                <h2><?=Misc::t('Сохранность ваших персональных данных');?></h2>
                <?=Misc::t('Мы как дистрибьютор обязуемся беречь и не разглашать ваши персональные данные, которые мы получаем от вас, наших клиентов.');?>
            </div>
        </div>
        <a href="<?php echo Yii::app()->createUrl('page/terms_conditions')?>" target="_blank" class="FindMore">
            <?=Misc::t('Наша политика');?>
        </a>
    </div>



    <div class="join step2">

        <h2 class="heading">
            <?=Misc::t('Дополнительные вопросы про Ваших питомцев');?>
        </h2>


        <p class="content"><?php echo Misc::t('Если Вы поделитесь дополнительной информацией о Вашем питомце, нам будет проще создать для вас улучшенные программы, продукты и давать Вам именно необходимые советы. Вместе мы поможем питомцам жить долго и счастливо.')?></p>

        <hr>

        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'regFormStep3',
            'enableAjaxValidation' => false,
        ));?>

            <span class="small"> <?=Misc::t('Обязательные поле');?> <small>*</small></span>

            <div class="dryFoodFreq">

                <?php echo $form->labelEx($model, 'dryFoodFreq', array('style' => 'width:500px;'))?>
                <?php echo $form->dropDownList($model, 'dryFoodFreq', Subscribers::getDryFoodFreqList(),
                    array(
                         'empty' => Misc::t('Выберите один вариант'),
                         'style' => 'width:250px'
                         )
                    );
                ?>
                <?php echo $form->error($model, 'dryFoodFreq', array('class' => 'error'))?>
            </div>

            <div class="wetFoodFreq">
                <?php echo $form->labelEx($model, 'wetFoodFreq', array('style' => 'width:500px;'))?>
                <?php echo $form->dropDownList($model, 'wetFoodFreq', Subscribers::getDryFoodFreqList(),
                    array(
                        'empty' => Misc::t('Выберите один вариант'),
                        'style' => 'width:250px'
                        )
                    );
                ?>
                <?php echo $form->error($model, 'wetFoodFreq', array('class' => 'error'))?>
            </div>

            <div class="dryFoodBrands"
                <?php echo $form->labelEx($model, 'dryFoodBrands', array('style' => 'width:500px;'))?>
                <div class="multi-check">
                    <?php echo $form->checkBoxList($model, 'dryFoodBrands', Subscribers::getDryFoodBrandsList(),array('separator'=>'',))?>
                    <?php echo $form->textField($model, 'dryFoodBrandsOther',
                            array(
                                'style' => "width:130px; margin-left:140px;position: relative;top: -21px;left: 74px;",
                                'disabled' => 'disabled',
                            )
                    );?>
                </div>
            </div>

            <hr style="margin-top:0;">

            <div class="petFoodBuyWhen">
                <?php echo $form->labelEx($model, 'petFoodBuyWhen')?>
                <?php echo $form->dropDownList($model, 'petFoodBuyWhen', Subscribers::getPetFoodBuyWhenList(),
                    array(
                        'empty' => Misc::t('Выберите один вариант'),
                        'style' => 'width:250px'
                ));?>
                <?php echo $form->error($model, 'petFoodBuyWhen', array('class' => 'error'))?>

            </div>
            <p><?php echo Misc::t('Спасибо Вам, что уделили время для ответа на эти дополнительные вопросы.');?></p>

            <p class="buttonbar1s" id="buttonbar1s">
                <?=CHtml::submitButton(Misc::t('Закончить'), array('class' => 'button bgbtn', 'alt' => Misc::t('Закончить'), 'title' => Misc::t('Закончить'))); ?>
            </p>

        <?php $this->endWidget(); ?>
    </div>
