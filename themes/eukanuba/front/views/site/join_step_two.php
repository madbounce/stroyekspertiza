<?php
$js = <<<JS
    $('#hasPetfalse').click(function(){
        $('#dont-have-pets').show();
        $('#have-pets').hide();
    });

    $('#hasPettrue').click(function(){
        $('#dont-have-pets').hide();
        $('#have-pets').show();
    });

    $('#cat2').change(function(){
        var val = $(this).val();
        $('#addPet').hide();
        $('.pet-info-row').hide();
        for(i=1;i<=val;i++){
            $('#b-pet' + i).show();
        }
        if(val < 4)
            $('#addPet').show();
    });

    $('#addPet').click(function(){
        var val = $('#cat2').val();
        var newVal = parseInt(val) + 1;
        if(newVal <= 4){
            $('#cat2').val(newVal);
            $('#cat2').trigger('change');
        }
        return false;
    });

    $('.remove-pet').click(function(){
        var val = $('#cat2').val();
        var newVal = parseInt(val) - 1;
        if(newVal >= 1){
            $('#cat2').val(newVal);
            $('#cat2').trigger('change');
        }
        return false;
    });

    $('#submit_step2').click(function(){
        $('#step3-field').val(1);
        return true;
    })
JS;

Yii::app()->clientScript->registerScript('join_step_two', $js);
?>


<div id="MainContent" class="Register-step1">
    <div class="copy-top-border">
        <img src="<?=Yii::app()->theme->baseUrl;?>/images/top-border-dog.png" alt="" width="956" height="8" class="topborder">
    </div>

    <div id="white-container">
        <div id="breadcrumb"><a title="Главная" href="/"><?=Misc::t('Главная');?></a> / <?=Misc::t('Присоединиться');?></div>

        <div class="page-header">
            <h1><?=Misc::t('Присоединиться');?></h1>
            <p><?php echo Misc::t('Подпишитесь и получайте ценные советы и информацию по питанию, здоровью, поведению, обучению, дрессировке и уходу от наших экспертов.');?></p>
        </div>

        <div id="bodycontent_area">
            <div class="_col left-callouts-nav">
                <div class="related-articles">
                    <div class="top-border">
                        <p><?=Misc::t('О Вашем питомце');?></p>
                        <?=Misc::t('Почему мы спрашиваем дату рождения Вашей кошки? Чем больше мы знаем о Вашем любимце, тем более точные советы и рекомендации по продуктам, подходящим Вашей кошке в соответствии с ее возрастом и потребностями, мы можем предложить Вашему вниманию. Все это - для того, чтобы помогать питомцам жить долго и счастливо.');?>
                    </div>
                </div>
            </div>

            <div class="_col txt-content">
                <div class="Reg-form2">
                    <h2 class="heading">
                        <?=Misc::t('Шаг 2');?>: <?=Misc::t('Информация о Вашем питомце');?>
                        <span><small>*</small> <?=Misc::t('Обязательное поле');?> </span>
                    </h2>

                    <div class="clear-all"></div>

                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'regFormStep2',
                        'enableAjaxValidation' => false,
                    ));
                    ?>

                    <h3>Пожалуйста, выберите <small class="req">*</small></h3>

                    <p class="top-option">
                        <?=$form->radioButton($model, 'havePet', array('id' => 'hasPetfalse', 'value' => 0)); ?>
                        <label for="hasPetfalse"><?=Misc::t("У меня сейчас нет ни одного питомца.");?></label>
                        <?=$form->radioButton($model, 'havePet', array('id' => 'hasPettrue', 'value' => 1)); ?>
                        <label for="hasPettrue"><?=Misc::t("Cейчас один или более питомцев.");?></label>
                    </p>

                    <div class="clear-all">&nbsp;</div>

                    <div id="have-pets"<?php echo (!$model->havePet) ? ' style="display: none"' : '';?>>
                        <p class="content" id="content">
                            <?=Misc::t("Расскажите нам о Ваших питомцах, чтобы мы могли высылать Вам новости в соответствии с вашими интересами.");?>
                        </p>

                        <div class="_col last_" id="hasCat">
                            <div class="select-count-pets">
                                <label for="cat2" style="float: left">У меня дома: <small class="req">*</small></label>

                                <?php echo $form->dropDownList($model, 'catCount',
                                    array(
                                        '1' => '1',
                                        '2' => '2',
                                        '3' => '3',
                                        '4' => '4',
                                    ),
                                    array(
                                        'id' => 'cat2',
                                        'style' => 'float: left'
                                    )
                                );
                                ?>
                                <?=$form->labelEx($model, 'catCount',  array('style' => 'float: left', 'label' => 'животных')); ?>
                            </div>

                            <div class="pets-info">
                                <div class="pet-info-row" id="b-pet1">
                                    <p>
                                        <?php echo $form->labelEx($model, 'pet1_name'); ?>
                                        <?php echo $form->textField($model, 'pet1_name', array('maxlength' => 50, 'class' => 'txt')); ?>
                                        <?php echo $form->error($model, 'pet1_name',array('class'=>'error')); ?>
                                    </p>
                                    <p class="birthdaycol">
                                        <?php echo $form->labelEx($model, 'pet1_birth'); ?>
                                        <br>
                                        
                                        <?php echo $form->dropDownList($model, 'pet1Month', Misc::monthes(), array('empty' => 'Месяц', 'class' => 'txt')); ?>
                                        <?php echo $form->dropDownList($model, 'pet1Year', Misc::years(1988), array('empty' => 'Год', 'class' => 'txt')); ?>
                                        <?php echo $form->error($model, 'pet1_birth',array('class'=>'error')); ?>
                                    </p>
                                </div>
                                <div class="pet-info-row" id="b-pet2" <?php echo ($model->catCount < 2) ? ' style="display: none"' : '';?>>
                                    <hr>
                                    <p>
                                        <?php echo $form->labelEx($model, 'pet2_name'); ?> <span class="required">*</span><a href="#" class="remove-pet"> Удалить </a>
                                        <?php echo $form->textField($model, 'pet2_name', array('maxlength' => 50, 'class' => 'txt')); ?>
                                        <?php echo $form->error($model, 'pet2_name',array('class'=>'error')); ?>
                                    </p>
                                    <p class="birthdaycol">
                                        <?php echo $form->labelEx($model, 'pet2_birth'); ?> <span class="required">*</span>
                                        <br>
                                        <?php echo $form->dropDownList($model, 'pet2Month', Misc::monthes(), array('empty' => 'Месяц', 'class' => 'txt')); ?>
                                        <?php echo $form->dropDownList($model, 'pet2Year', Misc::years(1988), array('empty' => 'Год', 'class' => 'txt')); ?>
                                        <?php echo $form->error($model, 'pet2_birth',array('class'=>'error')); ?>
                                    </p>
                                </div>
                                <div class="pet-info-row" id="b-pet3" <?php echo ($model->catCount < 3) ? ' style="display: none"' : '';?>>
                                    <hr>
                                    <p>
                                        <?php echo $form->labelEx($model, 'pet3_name'); ?> <span class="required">*</span><a href="#" class="remove-pet"> Удалить </a>
                                        <?php echo $form->textField($model, 'pet3_name', array('maxlength' => 50, 'class' => 'txt')); ?>
                                        <?php echo $form->error($model, 'pet3_name',array('class'=>'error')); ?>
                                    </p>
                                    <p class="birthdaycol">
                                        <?php echo $form->labelEx($model, 'pet3_birth'); ?> <span class="required">*</span>
                                        <br>
                                        <?php echo $form->dropDownList($model, 'pet3Month', Misc::monthes(), array('empty' => 'Месяц', 'class' => 'txt')); ?>
                                        <?php echo $form->dropDownList($model, 'pet3Year', Misc::years(1988), array('empty' => 'Год', 'class' => 'txt')); ?>
                                        <?php echo $form->error($model, 'pet3_birth',array('class'=>'error')); ?>
                                    </p>
                                </div>
                                <div class="pet-info-row" id="b-pet4" <?php echo ($model->catCount < 4) ? ' style="display: none"' : '';?>>
                                    <hr>
                                    <p>
                                        <?php echo $form->labelEx($model, 'pet4_name'); ?> <span class="required">*</span><a href="#" class="remove-pet"> Удалить </a>
                                        <?php echo $form->textField($model, 'pet4_name', array('maxlength' => 50, 'class' => 'txt')); ?>
                                        <?php echo $form->error($model, 'pet4_name',array('class'=>'error')); ?>
                                    </p>
                                    <p class="birthdaycol">
                                        <?php echo $form->labelEx($model, 'pet4_birth'); ?> <span class="required">*</span>
                                        <br>
                                        <?php echo $form->dropDownList($model, 'pet4Month', Misc::monthes(), array('empty' => 'Месяц', 'class' => 'txt')); ?>
                                        <?php echo $form->dropDownList($model, 'pet4Year', Misc::years(1988), array('empty' => 'Год', 'class' => 'txt')); ?>
                                        <?php echo $form->error($model, 'pet4_birth',array('class'=>'error')); ?>
                                    </p>
                                </div>
                                <div class="add-pet">
                                    <a href="#" id="addPet" style="display: inline;">Добавить</a>
                                </div>
                            </div>

                            <div class="clear-all">&nbsp;</div>
                        </div>

                        <div class="clear-all">&nbsp;</div>

                        <div id="finish-have-pet">
                            <p class="content" id="content2">
                                <?php echo Misc::t("Чем больше мы знаем о Вас, тем лучше мы можем составить предложение.");?>
                            </p>


                            <p class="buttonbar" id="buttonbar">
                                <input type="image" src="<?=Yii::app()->theme->baseUrl;?>/images/btn_answer2.gif" alt="Ответить еще на несколько вопросов" title="Ответить еще на несколько вопросов" id="submit_step2" class="button">
                                или
                                <input type="submit" class="button" value="Закончить" id="finish_step2" title="Закончить">
                            </p>
                        </div>

                    </div>
                    <div id="dont-have-pets" <?php echo ($model->havePet !== 0) ? ' style="display: none"' : '';?>>
                        <p id="content1">
                            <?=Misc::t("Вы подумываете о том, чтобы завести нового домашнего любимца? Мы вышлем Вам полезные советы и информацию по питанию домашних животных, уходу за ними, дрессировке и обучению. Завести какого питомца Вам бы хотелось? Можно указать несколько вариантов.");?>
                        </p>

                        <div class="bottom-check">

                            <?=$form->checkBox($model, 'petTypeCat', array('id' => 'petTypeOption-2')); ?>
                            <label for="petTypeOption-2" class="checkboxLabel"><?=Misc::t("Взрослый");?></label>

                            <?=$form->checkBox($model, 'petTypeKitten', array('id' => 'petTypeOption-4')); ?>
                            <label for="petTypeOption-4" class="checkboxLabel"><?=Misc::t("Котенок");?></label>

                        </div>

                        <div class="bottom-check last_">
                            <label for="selectone"
                                   class="none"><?=Misc::t("Как скоро вы планируете завести нового питомца?");?></label>
                            <?=$form->dropDownList($model, 'petBuyPlan', Subscribers::getPetByPlanList(), array(
                                'id' => 'selectone',
                                'empty' => Misc::t('Выберите один вариант'))
                        ); ?>

                        </div>
                        <div class="clear-all">&nbsp;</div>

                        <div id="finish-dont-have-pet">
                            <p class="buttonbar1s" id="buttonbar1s">
                                <?=CHtml::submitButton(Misc::t('Закончить'), array('class' => 'button bgbtn', 'alt' => Misc::t('Закончить'), 'title' => Misc::t('Закончить'))); ?>
                            </p>
                        </div>
                    </div>

                    <?php //echo $form->errorSummary($model);?>
                    <?php echo CHtml::hiddenField('step3', 0, array('id' => 'step3-field'))?>
                    <?php $this->endWidget(); ?>

                </div>

            </div>
            <div class="clear-all">&nbsp;</div>
        </div>
        <div class="clear-all">&nbsp;</div>
    </div>
</div>