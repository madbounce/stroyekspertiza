<?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/join.js');

    $this->breadcrumbs=array(
        Misc::t('Присоединиться') => array('site/join'),
        Misc::t('Шаг 2: Информация о Вашем питомце'),
    );
?>

    <script>
        $(document).ready(function(){$('#selectone.selectBlock').sSelect({defaultText: '<?php echo Misc::t('Выберите один вариант')?>'})});
        $(document).ready(function(){$('.selectBlock.year').sSelect({defaultText: '<?php echo Misc::t('Год')?>'})});
        $(document).ready(function(){$('.selectBlock.month').sSelect({defaultText: '<?php echo Misc::t('Месяц')?>'})});
        $(document).ready(function(){$('#cat2').sSelect({defaultText: '<?php echo Misc::t('1')?>'})});
    </script>

<div class="header_image articles">
    <div class="content_header">
        <div class="text_header">
            <h3>СТАТЬИ О ЩЕНКАХ</h3>
            <p>УХОД, ВОСПИТАНИЕ, ЗДОРОВЬЕ, ПИТАНИЕ...</p>
        </div>

        <div class="header_right_menu">
            <ul>
                <li><a href="/articles/default/puppy">СТАТЬИ</a></li>
                <li><a href="/page/video">ВИДЕО О ВОСПИТАНИИ</a></li>
                <li><a href="/products/dog/puppy">ПОДБОР КОРМА</a></li>
                <li><a href="/page/nutrition">ПИТАНИЕ ЩЕНКОВ</a></li>
                <li><a href="/page/shoppinglist">ЛИСТ ПОКУПОК</a></li>
                <li><a href="/page/dog_breed_selector">НАЙДИТЕ СВОЮ ИДЕАЛЬНУЮ ПОРОДУ</a></li>
                <li><a href="/page/breedersrecommended">РЕКОМЕНДАЦИИ ЗАВОДЧИКОВ</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="content_iner">
    <div class="news">
        <div class="allNews">
            <div>
                <h2><?=Misc::t('О Вашем щенке');?></h2>
                <?=Misc::t('Почему мы спрашиваем дату рождения Вашего щенка? Чем больше мы знаем о Вашем щенке, тем более точные советы и рекомендации по продуктам, подходящим Вашему щенку в соответствии с ее возрастом и потребностями, мы можем предложить Вашему вниманию. Все это - для того, чтобы помогать питомцам жить долго и счастливо.');?>
            </div>
            <div class="ie8">
                <h2><?=Misc::t('Сохранность ваших персональных данных');?></h2>
                <?=Misc::t('Мы как дистрибьютор обязуемся беречь и не разглашать ваши персональные данные, которые мы получаем от вас, наших клиентов.');?>
            </div>
        </div>
        <a href="<?php echo Yii::app()->createUrl('page/terms_conditions')?>" target="_blank" class="FindMore">
            <?=Misc::t('Наша политика');?>
        </a>
    </div>

    <div class="join step2">
        <h2><?=Misc::t('Шаг 2: Информация о Вашем щенке');?></h2>

        <?php $form = $this->beginWidget('CActiveForm', array(
            'id' => 'regFormStep2',
            'enableAjaxValidation' => false,
        ));?>
        <p class="top-option">
            <?=$form->radioButton($model, 'havePet', array('id' => 'hasPetfalse', 'value' => 0)); ?>
            <label for="hasPetfalse"><?=Misc::t("У меня сейчас нет ни одного щенка.");?></label>
            <?=$form->radioButton($model, 'havePet', array('id' => 'hasPettrue', 'value' => 1)); ?>
            <label for="hasPettrue"><?=Misc::t("Cейчас один или более щенков.");?></label>
        </p>

        <div class="block_one">
            <?php $this->renderPartial('_step2_block1', array('form'=>$form,'model'=>$model));?>
        </div>

        <div class="block_two">
            <?php $this->renderPartial('_step2_block2', array('form'=>$form,'model'=>$model));?>
        </div>

        <?php echo CHtml::hiddenField('step3', 0, array('id' => 'step3-field'))?>

        <? $this->endWidget(); ?>

    </div>
</div>