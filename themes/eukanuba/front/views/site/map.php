<?php
    $this->breadcrumbs = array(
        Misc::t('Адреса магазинов'),
    );
?>
<div  style="margin-top: 45px;">
    <?php $this->widget('banner.components.widget.rotator', array('categoryId' => 12,'view'=>'textClickable')); ?>



    <!-- MAP CONTENT   стили копируйте не все только /*new FOR MAP*/ -->
    <div class="_col fix_page map" style="padding: 0px; width: 556px;">
        <h1 class="heading shops">Адреса магазинов</h1>

        <div class="container_select">
            <a name="top"></a>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'map-form',
                'enableAjaxValidation' => false,
                //'method'=> 'get',
                'htmlOptions' => array(//'enctype' => 'multipart/form-data',
                    //'class' => 'product'
                ),
            ));
            ?>

            <?php echo CHtml::dropDownList('city_id', $params['city_id'],
                CHtml::listData($cities, 'id', 'title'),
                array('onchange' => "document.location.href = '/site/buymap/'+$(this).val();")
            ); ?>

            <?php $this->endWidget(); ?>

        </div>


        <div id="map_canvas">
            <?php $this->renderPartial('demo2', array('model' => $model)); ?>
        </div>

        <? foreach ($model as $city => $shops): ?>
        <table class="shops_add">
            <th colspan="2"><?=$city;?></th>
            <? foreach ($shops as $shop): ?>
            <tr>
                <td style="width:50%;"><?=$shop->title;?></td>
                <td><?=$shop->address;?></td>
            </tr>
            <? endforeach; ?>
        </table>
        <a href="#top" class="returntop" title="Вернуться к началу">Вернуться к началу</a>
        <? endforeach; ?>

        <script type="text/javascript">
            $(document).ready(function () {
                $('.shops_add tr:odd td').addClass('odd');
                $('.shops_add tr:even td').addClass('even');

                $('.choose_city').click(function () {
                    if ($('.cities_list').css('display') == 'none') {
                        $('.cities_list').css('display', 'block');
                    }
                    else {
                        $('.cities_list').css('display', 'none');
                    }

                    if ($('.choose_city').hasClass('active')) {
                        $('.choose_city').removeClass('active');
                    }
                    else {
                        $('.choose_city').addClass('active');
                    }

                });

                $('.cities_list div').each(function () {
                    $(this).click(function () {
                        var a = $(this).html();
                        $('.choose_city').text(a);
                        $('.cities_list').css('display', 'none');
                        $('.choose_city').removeClass('active');
                    });
                });
            });
        </script>

    </div>
    <!-- END MAP CONTENT -->
</div>