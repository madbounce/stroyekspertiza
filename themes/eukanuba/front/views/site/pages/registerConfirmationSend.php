<?php $this->pageTitle = Misc::t('Регистрация'); ?>

<div class="mail_successful">
    <h1><?php echo Misc::t('Поздравляем!<span>Вы практически стали членом сообщества IpLife</span>'); ?></h1>
    <p>
        <?php echo Misc::t('Письмо с подтверждением отправлено вам на почту.'); ?>
        <br />
        <?php echo Misc::t('Следуйте указаниям в письме.'); ?>
    </p>
</div>