<?php $this->pageTitle = Misc::t('Подтверждение аккаунта'); ?>

<div class="mail_successful">
    <h1>
        <?php echo Misc::t('Поздравляем!<span>Вы успешно подтвердили Ваш e-mail.</span>'); ?>
    </h1>
    <p>
        <?php echo Misc::t('Теперь Вы можете войти на сайт'); ?><br>
        <?php echo CHtml::link(Misc::t('войти'), Yii::app()->user->loginUrl, array('class' => 'dialog-login')); ?>
    </p>
</div>