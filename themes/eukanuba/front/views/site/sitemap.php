<link  rel="stylesheet"  type="text/css" href="<? echo Yii::app()->theme->baseUrl;?>/css/jquery.treeview.css"  media="screen" />
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.treeview.js', CClientScript::POS_HEAD); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.cookie.js', CClientScript::POS_HEAD); ?>

<div class="breadcrumbs ">
	<a href="/">Главная</a>/<span>Карта сайта</span>
</div>

<div class="sitemap">

<div class="leftBanners">
    <div class="banner  last">
        <a target="_self" href="http://eukanuba.i-d-web.com/article/articles/cats" title="УЗНАТЬ БОЛЬШЕ &gt;">
			<h2>ЗАБОТА СОГЛАСНО ВОЗРАСТУ</h2>
		</a>
        Корма для животных старшего возраста помогает зрелым и пожилым собакам и кошкам чувствовать себя превосходно
         <a href="http://eukanuba.i-d-web.com/article/articles/cats" target="_self" class="LearnMore">УЗНАТЬ БОЛЬШЕ &gt
		 </a>       
    </div>   
</div>
<div class="_col fix_page map" style="padding: 0px; width: 556px;">
                    
	


          <div class="_col txt-content">

              <h1 class="heading">Карта сайта</h1>

              <h3 class="heading">Инструменты и Утилиты</h3>
              <!-- First Include -->

              <ul class="sitemaptop">
                    <li><a href="/site/join" title="Присоединиться">Присоединиться</a></li>
                    <li><a href="/site/contactus" title="Свяжитесь с нами">Свяжитесь с нами</a></li>
                    <li><a href="/site/buymap" title="Где купить">Где купить</a></li>
                    <li><a href="/search?searchText=+" title="Поиск Iams">Поиск Eukanuba</a></li>
              </ul>
        	  <div class="clear-all">&nbsp;</div>

              <h3 class="heading">Содержание</h3>
              <!-- Second Include -->

			  <div class="Sitemap-left">

                    <h3><?=$content[0]->name;?></h3>
                    <? if (isset($content[0]->submenu)): ?>
                    <ul>
                        <? foreach($content[0]->submenu as $sub): ?>
                            <li> <a href="<?=$sub->url;?>" title="<?=$sub->name;?>"><?=$sub->name;?></a> </li>
                        <? endforeach; ?>
                    </ul>
                    <? endif; ?>

                    <h3><?=$content[1]->name;?></h3>
                    <? if (isset($content[1]->submenu)): ?>
                    <!--<ul id="navigationProducts" class="treeview">
                        <li title="Для кошек" class="expandable">
                        <div class="hitarea expandable-hitarea "></div>
                            <a href="#" title="Для кошек" class="">Для кошек</a>-->
                            <ul id="navigationProducts">
                                <? $cnt = count($content[1]->submenu)-1; ?>
                                <? foreach($content[1]->submenu as $key => $sub): ?>
                                    <? $type = substr($sub->url,strripos($sub->url,'/')+1);?>
                                    <li title="<?=$sub->name;?>" class="expandable <?echo (($cnt==$key)?'lastExpandable':'');?>">
                                        <div class="hitarea expandable-hitarea <?echo (($cnt==$key)?'lastExpandable-hitarea':'');?>"></div>
                                        <a href="<?=$sub->url;?>" title="<?=$sub->name;?>"><?=$sub->name;?></a>
                                        <? $products = Catalog::model()->with('category')->findAll(array('condition'=>'category.path="'.$type.'"'));?>
                                        <? $cnt2 = count($products)-1; ?>
                                        <ul style="display: none;">
                                            <? foreach($products as $pr => $product): ?>
                                                <li <? if ($pr==$cnt2) echo 'class="last"'; ?> ><a href="<?php echo Yii::app()->createUrl('/catalog/default/view', array('url' => $product->url));?>" title="<?=$product->name;?>"><?=$product->name;?></a></li>
                                            <? endforeach; ?>
                                        </ul>
                                    </li>
                                <? endforeach; ?>
                            </ul>
                       <!-- </li>
                    </ul>-->
                    <? endif; ?>

              </div>

              <div class="Sitemap-right">

                    <h3><?=$content[2]->name;?></h3>
                    <ul id="navigationPatHealth" class="treeview">
                        <li title="Статьи" class="collapsable"><div class="hitarea collapsable-hitarea"></div>
                            <a href="#" title="Статьи" class="">Статьи</a>
                            <ul style="display: none;">
                                <? $cnt = count($content[2]->submenu)-1; ?>
                                <? foreach($content[2]->submenu as $key => $sub): ?>
                                    <li title="Статьи" <? if ($key==$cnt) echo 'class="last"'; ?>>
                                        <a href="<?=$sub->url;?>" title="<?=$sub->name;?>"><?=$sub->name;?></a>
                                    </li>
                                <? endforeach; ?>
                            </ul>
                        </li>
                    </ul>
					
					
					<h3><a href="<?=Yii::app()->createUrl('/page/professionals')?>">Професcионалы</a></h3>
                    <ul>
						<li>
							<a href="/articles/search/breeders" class="submenu" >Статьи для заводчиков</a>
						</li>
						<li>
							<a href="/page/partners" class="submenu" >Наши партнеры среди заводчиков </a>
						</li>
						<li>
							 <a href="/page/club " class="submenu" >Клуб заводчиков</a>
						</li>
						<li>
							<a href="/page/eukanuba_world_challenge" class="submenu" >Мировое первенство Eukanuba</a>
						</li>
						<li>
							 <a href="/products/dog/breeders" class="submenu" >Продукция для заводчиков</a>
						</li>
					</ul>
					
					
                    <h3><a href="<?=$content[3]->url;?>" title="<?=$content[3]->name;?>"><?=$content[3]->name;?></a></h3>

              </div>


        </div>

    </div>

    </div><!-- Copy Content end-->
</div>


<script>
$(document).ready(function(){
    
// first example
$("#navigationPatHealth").treeview({
    animated:"normal",
    collapsed: true
    //persist: "cookie"
});


});
</script>
