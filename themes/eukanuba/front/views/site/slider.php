<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery-ui-tabs-rotate.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('slider', '
    $("#featured_slider").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
');
?>
<div class="top_content_block">
    <div class="center_wrap">
        <h1><?php echo Misc::t('Бесплатные входящие звонки в Россию на номера 800, 804 по всему миру');?></h1>
        <div id="featured_slider" >
            <ul class="ui-tabs-nav">
                <li class="ui-tabs-nav-item" id="nav-fragment-1"><span class="active_img"><a href="#fragment-1" title="<?php echo Misc::t('Софтфон &quot;ServicePhone&quot;');?>"><span class="bold"><?php echo Misc::t('Софтфон &quot;ServicePhone&quot;');?></span> <p><?php echo Misc::t('Удобный интерфейс, легкость в пользовании, все необходимые свойства');?></p></a></span></li>
                <li class="ui-tabs-nav-item" id="nav-fragment-2"><span class="active_img"><a href="#fragment-2" title="<?php echo Misc::t('100 рублей на вашем балансе!');?>"><span class="bold"><?php echo Misc::t('100 рублей на вашем балансе!');?></span> <p><?php echo Misc::t('Купи IP-телефон со стикером 8800 и звони сразу!');?></p></a></span></li>
                <li class="ui-tabs-nav-item" id="nav-fragment-3"><span class="active_img"><a href="#fragment-3" title="<?php echo Misc::t('Прямой городской номер');?>"><span class="bold"><?php echo Misc::t('Прямой городской номер');?></span> <p><?php echo Misc::t('Для сокращения расходов на межгород, Это технология передачи голоса через сеть Интернет.');?></p></a></span></li>
            </ul>

            <!-- First Content -->
            <div id="fragment-1" class="ui-tabs-panel">
                <div class="info_pic"><img src="<?php echo Yii::app()->theme->baseUrl ?>/images/top_pic_1.png" alt="<?php echo Misc::t('Софтфон "ServicePhone"')?>" /></div>
                <div class="info" >
                    <h2><?php echo Misc::t('Софтфон "ServicePhone"');?></h2>
                    <p><?php echo Misc::t('С <span class="s36">5</span>-й минуты бесплатно');?></p>
                    <?php echo CHtml::link(Misc::t('Подробнее'), '/#', array('class' => 'more_btn', 'title' => Misc::t('Подробнее'))); ?></a>
                </div>
            </div>

            <!-- Second Content -->
            <div id="fragment-2" class="ui-tabs-panel ui-tabs-hide">

            </div>

            <!-- Third Content -->
            <div id="fragment-3" class="ui-tabs-panel ui-tabs-hide">

            </div>

        </div>
    </div>
</div>