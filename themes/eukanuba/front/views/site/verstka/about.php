                <!-- content wrap part-->

                <div class="row-fluid MainContent" id="aboutUs">
                    <!-- breadcrumb-->
                    <div class="span12">
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a> <span class="divider">/</span></li>
                            <li><a href="#">Eukanuba Dog Food</a> <span class="divider">/</span></li>
                            <li class="active">Eukanuba ProActive HealthTM </li>
                        </ul>
                     </div>
                    <!-- big banner-->
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="promo">
                                <h1>all about
                                    <span>eukanuba</span></h1>
                                <img src="<?php echo Yii::app()->theme->baseUrl . '/i/about_img.png' ?>"/>

                            </div>
                        </div>
                    </div>
                    <!-- content-->
                    <div class="row-fluid">
                        <!-- sidebar-->
                        <div class="span4 leftsidebar">
                            <div class="sideblock">
                                 <h5>Care for your Senior</h5>
                                <p>Lorem ipsum dolor sit amet, com nsectetur adipiscing elit. Pelle ntesque facilisis tortor vitae tellus egestas rhoncus. Lorem ipsum dolor sit amet, consecte tur adipiscing.</p>
                                <span class="btn_more pink">
                                    <a href="" title="">learn more ></a>
                                </span>
                            </div>

                        </div>
                        <div class="span8 contentplace">
                            <h3>ABOUT EUKANUBA</h3>
                            <p>Lorem ipsum dolor sit amet, com nsectetur adipiscing elit. Pelle ntesque facilisis tortor vitae tellus ege stas rho ncus. Lorem ipsum dolor sit amet, com nsect etur adipiscing elit. Pelle ntesque facilisis tortor vitae tellus egestas.</p>
                            <hr class="bs-docs-separator">
                            <table class="contentTable">
                                <tr>
                                    <td>
                                        <img src="<?php echo Yii::app()->theme->baseUrl . '/i/about_img1.png' ?>"/>
                                        <h4>Nutritional Philosophy</h4>
                                        <p>Lorem ipsum dolor sit amet, com nsectetur
                                            adipiscing elit. Pelle ntesque facilisis tortor vitae tellus ege stas rho ncus. Lorem ipsum dolor sit amet, com nsect etur adipiscing elit. Pelle ntesque facilisis tortor vitae tellus egestas.</p>
                                        <a class="link_more" href="" title="">learn more ></a>

                                    </td>
                                    <td>
                                        <img src="<?php echo Yii::app()->theme->baseUrl . '/i/about_img2.png' ?>"/>
                                        <h4>Nutritional Philosophy</h4>
                                        <p>Lorem ipsum dolor sit amet, com nsectetur
                                            adipiscing elit. Pelle ntesque facilisis tortor vitae tellus ege stas rho ncus. Lorem ipsum dolor sit amet, com nsect etur adipiscing elit. Pelle ntesque facilisis tortor vitae tellus egestas.</p>
                                        <a class="link_more" href="" title="">learn more ></a>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo Yii::app()->theme->baseUrl . '/i/about_img3.png' ?>"/>
                                        <h4>Nutritional Philosophy</h4>
                                        <p>Lorem ipsum dolor sit amet, com nsectetur
                                            adipiscing elit. Pelle ntesque facilisis tortor vitae tellus ege stas rho ncus. Lorem ipsum dolor sit amet, com nsect etur adipiscing elit. Pelle ntesque facilisis tortor vitae tellus egestas.</p>
                                        <a class="link_more" href="" title="">learn more ></a>

                                    </td>
                                    <td>
                                        <img src="<?php echo Yii::app()->theme->baseUrl . '/i/about_img4.png' ?>"/>
                                        <h4>Nutritional Philosophy</h4>
                                        <p>Lorem ipsum dolor sit amet, com nsectetur
                                            adipiscing elit. Pelle ntesque facilisis tortor vitae tellus ege stas rho ncus. Lorem ipsum dolor sit amet, com nsect etur adipiscing elit. Pelle ntesque facilisis tortor vitae tellus egestas.</p>
                                        <a class="link_more" href="" title="">learn more ></a>

                                    </td>
                                </tr>

                            </table>


                        </div>
                    </div>
                </div>

                <!-- end content wrap part->