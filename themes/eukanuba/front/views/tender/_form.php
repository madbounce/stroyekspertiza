<?php /** @var $form CActiveForm */ ?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'tender-form',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
)); ?>

<div class="tenderblock1">
    <?php if (Yii::app()->user->model->isCompany && $model->isNewRecord): ?>
        <div class="formline">
            <?php echo CHtml::label(Yii::t('tender', 'Создатель тендера'), 'creator', array('class' => 'l1')); ?>
            <?php echo CHtml::dropDownList('creator', $model->createdByCompany ? Tender::USER_TYPE_COMPANY : Tender::USER_TYPE_USER, array(
                Tender::USER_TYPE_USER => Yii::app()->user->model->full_name,
                Tender::USER_TYPE_COMPANY => Yii::app()->user->model->profile->name,
            )); ?>
        </div>
    <?php endif; ?>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'title', array('class' => 'l1')); ?>
        <?php echo $form->textField($model, 'title', array('class' => 'input200 i1')); ?>

        <?php echo $form->checkBox($model, 'public', array('class' => 'c1')); ?>
        <?php echo CHtml::label(Yii::t('tender', 'Публичный тендер (все могут видеть и участвовать в тендере)'), CHtml::activeId($model, 'public'), array('class' => 'l2')); ?>

        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'description', array('class' => 'l1')); ?>
        <?php echo $form->textArea($model, 'description', array('class' => 'textarea700')); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>

    <div class="formline1" style="overflow: hidden;">
        <?php $this->widget('filesaver.components.EAjaxUpload', array(
            'files' => $model->isNewRecord ? false : $model->getFilesSaver(),
            'id' => 'uploadFile',
            'config' => array(
                //'multiple'=>true,
                'allowedExtensions' => array("png", "jpg", "jpeg", "gif", "mov", "doc", "docx", "xls", "xlsx", "odt", "ppt", "pptx", "rar", "zip", "ai", "eps", "psd", "svg", "pdf", "txt"),
                'sizeLimit' => 200 * 1024 * 1024,
                'minSizeLimit' => 4,
            )
        )); ?>
    </div>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'finish_time', array('class' => 'l1')); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'finish_time',
            'language' => Yii::app()->language,
            'options' => array(
                'showAnim' => 'fold',
                'minDate' => 1,
                'dateFormat' => LocaleHelper::getJQueryDateFormat(),
            ),
            'htmlOptions' => array(
                'class' => 'input200',
            ),
        )); ?>
        <?php echo $form->error($model, 'finish_time'); ?>
    </div>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'end_time', array('class' => 'l1')); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'end_time',
            'language' => Yii::app()->language,
            'options' => array(
                'showAnim' => 'fold',
                'minDate' => 1,
                'dateFormat' => LocaleHelper::getJQueryDateFormat(),
            ),
            'htmlOptions' => array(
                'class' => 'input200',
            ),
        )); ?>
        <?php echo $form->error($model, 'finish_time'); ?>
    </div>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'cost', array('class' => 'l1')); ?>
        <?php echo $form->textField($model, 'cost', array('class' => 'input200 i2')); ?>
        <div class="selwr">
            <?php echo $form->dropDownList($model, 'currency', Helper::getCurrencies(), array('class' => 'select2')); ?>
        </div>
        <?php echo $form->error($model, 'cost'); ?>
    </div>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'activities', array('class' => 'l1')); ?>
        <?php $this->widget('SelectActivitiesWidget', array('model' => $model, 'attribute' => 'activities')); ?>
        <?php echo $form->error($model, 'activities'); ?>
    </div>

    <div class="formline1">
        <?php echo $form->labelEx($model, 'region_id', array('class' => 'l1')); ?>
        <?php $this->widget('RegionWidget', array('model' => $model, 'attribute' => 'region_id')); ?>
        <?php echo $form->error($model, 'region_id'); ?>
    </div>
</div>

<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('tender', 'Создать тендер') : Yii::t('tender', 'Изменить тендер'), array('class' => 'but_zayavka mar1')); ?>

<?php $this->endWidget(); ?>