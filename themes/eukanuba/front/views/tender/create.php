<?php $this->pageTitle = Yii::t('tender', 'Создание тендера'); ?>

<h2 class="title_zayavka"><big><?php echo Yii::t('tender', 'Шаг 1. Добавление тендера'); ?></big></h2>

<?php $this->renderPartial('_form', array('model' => $model)); ?>