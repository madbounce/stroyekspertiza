<?php $this->pageTitle = Yii::t('tender', 'Редактирование тендера'); ?>

<h2 class="title_zayavka"><big><?php echo Yii::t('tender', 'Редактирование тендера'); ?></big></h2>

<?php $this->renderPartial('_form', array('model' => $model)); ?>