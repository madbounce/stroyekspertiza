<?php $this->pageTitle = CHtml::encode($model->title); ?>

<div class="mainleft">
    <h2 class="title_zayavka n2"><?php echo CHtml::encode($model->title); ?></h2>
    <p class="zayavkastatus"><?php echo Yii::t('tender', 'Статус'); ?>:&nbsp;
        <span><?php echo CHtml::encode($model->statusText); ?></span>
    </p>
    <div class="zayavkablock">
        <div class="zayavkatb">
            <div class="line1"><?php echo Yii::t('tender', 'Срок подведения итогов'); ?>: <span><?php echo Yii::app()->dateFormatter->formatDateTime($model->finish_time, 'medium', null); ?></span></div>
            <div class="line1"><?php echo Yii::t('tender', 'Срок сдачи проекта'); ?>: <span><?php echo Yii::app()->dateFormatter->formatDateTime($model->end_time, 'medium', null); ?></span></div>
            <div class="line1"><?php echo Yii::t('tender', 'Максимальный бюджет'); ?>: <span><?php echo $model->formattedCost; ?></span></div>
        </div>
        <p class="just">
            <?php echo CHtml::encode($model->description); ?>
        </p>
    </div>
    <?php if (!empty($model->hasFiles)): ?>
        <div class="fileswrap">
            <h2><?php echo Yii::t('tender', 'Файлы'); ?>:</h2>
            <?php $this->widget('filesaver.components.EAjaxUpload', array(
                'files' => $model->getFilesSaver(),
                'mode' => 'show',
            )); ?>
        </div>
    <?php endif; ?>
    <div class="buttonswrap">
        <?php if (Yii::app()->user->checkAccess('updateTender', array('tender' => $model))): ?>
            <?php echo CHtml::link(Yii::t('tender', 'Редактировать тендер'), array('/tender/update', 'id' => $model->id), array('class' => 'edit')); ?>
            <?php // TODO Пригласить подрядчика ?>
            <?php echo CHtml::link(Yii::t('tender', 'Закрыть тендер'), array('/tender/cancel', 'id' => $model->id), array('class' => 'otklonit')); ?>
        <?php elseif (Yii::app()->user->checkAccess('createBid', array('tender' => $model))): ?>
            <?php echo CHtml::link(Yii::t('tender', 'Оставить заявку'), array('/bid/create', 'id' => $model->id), array('class' => 'edit')); ?>
        <?php endif; ?>
    </div>

    <div class="comments">
        <?php $this->widget('comment.components.widgets.CommentsWidget',array(
            'model' => $model,
        )); ?>
    </div>
</div>

<div class="mainright">
    <?php $this->widget('AuthorWidget', array('model' => $model->user)); ?>

    <?php if (!empty($model->actualBids)): ?>
        <div class="drugie-zayavki-block">
            <h3><?php echo Yii::t('tender', 'Заявки'); ?></h3>
            <?php foreach ($model->actualBids as $bid): ?>
            <div class="zayavkasmallbl">
                <div class="zayavkasmallleft">
                    <?php echo CHtml::link(Yii::t('tender', 'Заявка № :id', array(':id' => $bid->id)), array('/bid/view', 'id' => $bid->id)); ?><br>
                    <?php echo CHtml::encode($bid->user->full_name); ?>
                </div>
                <div class="zayavkasmalldate">
                    <?php echo Yii::app()->dateFormatter->formatDateTime($bid->create_time, 'medium', null); ?><br>
                    <?php echo Yii::app()->dateFormatter->formatDateTime($bid->create_time, null, 'medium'); ?>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>