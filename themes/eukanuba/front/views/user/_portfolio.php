<div class="portfolio-title"><?php echo Yii::t('profile', 'Портфолио'); ?></div>
<div style="width: 533px; height: auto;">
    <div class="portfolio-brief"><?php echo CHtml::encode($model->description); ?></div>
    <div class="portfolio-img">
        <?php echo CHtml::image($model->thumb(533, 400)); ?>
    </div>
</div>
<br>