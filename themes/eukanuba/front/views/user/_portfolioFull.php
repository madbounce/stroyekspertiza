<div class="portfolio-title"><?php echo Yii::t('profile', 'Портфолио'); ?></div>
<ul id="portfolio-carousel" style="width: 533px; height: auto;">
    <?php foreach ($model->portfolios(array('order' => 'create_time DESC')) as $portfolio): ?>
        <li style="width: 533px; height: auto;">
            <div class="portfolio-brief"><?php echo CHtml::encode($portfolio->description); ?></div>
            <div class="portfolio-img">
                <?php echo CHtml::image($portfolio->thumb(533, 400)); ?>
            </div>
        </li>
    <?php endforeach; ?>
</ul>

<div class="slider-contrl popup" style="width: 500px;">
    <div class="jcarousel-control">
        <?php $count = count($model->portfolios); ?>
        <?php for ($i = 0; $i < $count; $i++): ?>
            <?php echo CHtml::link('<span>' . ($i + 1) . '</span>', '#', $i == 0 ? array('class' => 'active') : array()); ?>
        <?php endfor; ?>
    </div>
</div>