<?php
$this->pageTitle = Yii::t('profile', 'Компания :name', array(':name' => $model->profile->name));
?>

<?php $this->widget('ProfileActivitiesWidget', array(
    'model' => $model->profile,
)); ?>

<div class="freelance_cont">
    <h2><?php echo Yii::t('profile', 'О компании'); ?></h2>
    <p>
        <?php echo CHtml::encode($model->profile->about); ?>
    </p>

    <h2><?php echo Yii::t('profile', 'Сотрудники компании'); ?></h2>

    <?php $parts = array_chunk($model->profile->employees, 2, true); ?>
    <?php foreach ($parts as $employees): ?>
        <?php foreach ($employees as $employee): ?>
        <div class="block_worker">
            <div class="worker_img">
                <?php $this->widget('AvatarWidget', array('model' => $employee, 'width' => 49, 'height' => 49)); ?>
            </div>
            <div class="worker_info">
                <big><?php echo CHtml::encode($employee->full_name); ?></big><br>
                <span class="grey"><?php echo CHtml::encode($employee->company_post); ?></span>
                <div>
                    <?php echo CHtml::encode($employee->email); ?><br>
                    <?php echo CHtml::encode($employee->contact_phone); ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <?php endforeach; ?>
        <div class="clear"></div>
    <?php endforeach; ?>

    <div class="clear"></div>

    <div class="comments">
        <?php $this->widget('comment.components.widgets.CommentsWidget', array(
            'model' => $model->profile,
        )); ?>
    </div>
</div>
<div class="freelance_right">
    <?php $this->widget('PortfolioWidget', array('model' => $model->profile, 'type' => PortfolioWidget::TYPE_PUBLIC)); ?>
</div>
<div class="clear"></div>