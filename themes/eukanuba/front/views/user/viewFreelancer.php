<?php
$this->pageTitle = Yii::t('profile', 'Фрилансер :name', array(':name' => $model->full_name));
?>

<?php $this->widget('ProfileActivitiesWidget', array(
    'model' => $model->profile,
)); ?>

<div class="freelance_cont">
    <h2><?php echo Yii::t('profile', 'О фрилансере'); ?></h2>
    <p>
        <?php echo CHtml::encode($model->profile->about); ?>
    </p>

    <div class="comments">
        <?php $this->widget('comment.components.widgets.CommentsWidget',array(
            'model' => $model->profile,
        )); ?>
    </div>
</div>
<div class="freelance_right">
    <?php $this->widget('PortfolioWidget', array('model' => $model->profile, 'type' => PortfolioWidget::TYPE_PUBLIC)); ?>
</div>
<div class="clear"></div>