<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Misc::t('Восстановление пароля');
$this->breadcrumbs = array(
    Misc::t('Восстановление пароля'),
);
?>

<div class="main-cont-wrapper">
    <h1 class="reg-title"><?php echo Misc::t('Восстановление пароля'); ?></h1>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'restore-password-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'validateOnType' => false,
        ),
    ));
    ?>

    <div class="block_registration">
        <?php echo $form->error($model, 'new_password', array('class' => 'ltr_error')); ?>
        <?php echo Misc::t('Новый пароль'); ?><span class="red">*</span>:
        <div class="clear"></div>
        <?php echo $form->passwordField($model, 'new_password', array('class' => 'input_txt', 'AUTOCOMPLETE' => 'OFF')); ?><br/>

        <?php echo $form->error($model, 'new_password_repeat', array('class' => 'ltr_error')); ?>
        <?php echo Misc::t('Повторите новый пароль'); ?><span class="red">*</span>:
        <div class="clear"></div>
        <?php echo $form->passwordField($model, 'new_password_repeat', array('class' => 'input_txt', 'AUTOCOMPLETE' => 'OFF')); ?><br/>
    </div>

    <input type="submit" value="<?php echo Misc::t('Сменить пароль'); ?>" class="but_register"/>

    <p class="grey"><span class="red">*</span> - <?php echo Misc::t('поля обязательные для заполнения'); ?></p>
</div>
<?php $this->endWidget(); ?>
