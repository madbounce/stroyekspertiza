<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 17.09.14
 * Time: 12:41
 */

?>

<div class="nav">
    <div><a href="/" class="<?php if(Yii::app()->controller->id == 'site' && Yii::app()->controller->action->id == 'index') echo 'active';?>">О компании</a></div>
    <div><a href="/page/services" class="<?php if(Yii::app()->request->requestUri == '/page/services') echo 'active';?>">Услуги</a></div>
    <div><a href="/page/news" class="<?php if(Yii::app()->request->requestUri == '/page/news') echo 'active';?>">новости</a></div>
    <div><a href="/page/guide" class="<?php if(Yii::app()->request->requestUri == '/page/guide') echo 'active';?>">руководство</a></div>
    <div><a href="/page/accreditation" class="<?php if(Yii::app()->request->requestUri == '/page/accreditation') echo 'active';?>">аккредитация</a></div>
    <div><a href="/page/regulations" class="<?php if(Yii::app()->request->requestUri == '/page/regulations') echo 'active';?>">регламент</a></div>
    <div><a href="/page/objects" class="<?php if(Yii::app()->request->requestUri == '/page/objects') echo 'active';?>">объекты</a></div>
    <div><a href="/page/partners" class="<?php if(Yii::app()->request->requestUri == '/page/partners') echo 'active';?>">партнеры</a></div>
    <div><a href="/page/how_to_get" class="<?php if(Yii::app()->request->requestUri == '/page/how_to_get') echo 'active';?>">как добраться</a></div>
</div>