<div class="footer">
    <a class="logo-mini" href="/">
        <img  alt="" src="<?php echo Yii::app()->theme->baseUrl;?>/images/logo-footer.png">
    </a>
    <div class="copyright">
        © <?php echo date('Y');?> Стройэкспертиза <br> Все права защищены
    </div>
    <div class="adress">
        <?php echo Config::get('address');?>
    </div>
    <div class="phones">
        <?php
            $phones = Config::get('phone');
            $lists = explode(',',$phones);
            foreach($lists as $phone)
            {
                echo '<span>'.$phone.'</span>';
            }

        ?>
    </div>
</div>