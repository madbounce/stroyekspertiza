<div class="header ">
    <div class="wrapper">
        <a class="logo" href="/"><img  alt="" src="<?php echo Yii::app()->theme->baseUrl;?>/images/logo.png"></a>
        <div class="contacts">
            <div class="title bold">Контакты</div>
            <div class="adr"><?php echo Config::get('address');?></div>
            <div class="ph"><?php echo Config::get('phone');?></div>
            <div class="msg"><?php echo Config::get('infoEmail');?></div>
        </div>
        <div class="expert-info">
            <div class="title bold">Негосударственная экспертиза</div>
            <ul>
                <li>Проектной документации</li>
                <li>Результатов инженерных изысканий</li>
                <li>Сметной документации</li>
            </ul>
        </div>
    </div>
</div>
