<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="<?php echo Yii::app()->language; ?>" />
    <meta name="module" content="<? echo Yii::app()->request->getPathInfo()?>" />
    <meta name="route" content="<? echo Yii::app()->controller->route?>" />

    <?php if($this->useSeoWidget ){?>
        <?php
            $this->widget('seo.components.SEOWidget', array('mode'=>'0'));
        ?>
    <?php } else {?>
        <title><?php echo $this->pageTitle;?></title>
    <?php }?>

    <?php
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
    ?>
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <link  type="text/css" rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/css/mainstyle.css">
    <!--[if lt IE 9]>
    <script  type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
    <?php echo $content; ?>

    <!-- Yandex.Metrika informer -->
    <a href="https://metrika.yandex.ru/stat/?id=26547114&amp;from=informer"
       target="_blank" rel="nofollow"><img src="https:///bs.yandex.ru/informer/26547114/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                                           style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:26547114,lang:'ru'});return false}catch(e){}"/></a>
    <!-- /Yandex.Metrika informer -->

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter26547114 = new Ya.Metrika({id:26547114,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https:///mc.yandex.ru/watch/26547114" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src="https://e.mail.ru/g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-55613920-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>