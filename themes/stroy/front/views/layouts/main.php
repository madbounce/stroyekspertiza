<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/base'); ?>

<div class="sitewrap">
    <!--begin top header-->
    <?php $this->renderPartial('//common/header'); ?>
    <!--end top header-->
    <div class="main-block wrapper">

        <div class="main-content">

            <?php $this->renderPartial('//common/_left_menu'); ?>

            <div class="section <?php if(Yii::app()->controller->id == 'site' && Yii::app()->controller->action->id == 'index') echo ''; else echo 'not-left';?>">

                <?php echo $content ?>

            </div>
            <?php
                if(Yii::app()->controller->id == 'site' && Yii::app()->controller->action->id == 'index')
                    $this->renderPartial('//common/_lk_column');
            ?>

        </div>

        <?php $this->renderPartial('//common/footer'); ?>

    </div>
</div><!-- #wrapper -->

<?php $this->endContent(); ?>
