<div class="block_news">
    <?php if (!empty($data->image)): ?>
        <div class="news_img">
            <?php echo CHtml::image($data->image->thumb(96, 96), 'image'); ?>
        </div>
    <?php endif; ?>

    <div class="news_date"><?php echo Yii::app()->dateFormatter->format('dd MMMM', $data->create_time); ?></div>

    <?php echo CHtml::link(CHtml::encode($data->title), array('/news/default/view', 'id' => $data->id)); ?>

    <p>
        <?php echo CHtml::encode($data->content_short); ?>
    </p>
    <div class="clear"></div>
</div>