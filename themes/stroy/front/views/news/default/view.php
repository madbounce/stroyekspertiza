<?php
$this->pageTitle = CHtml::encode($model->title);
?>

<div class="main-cont-wrapper">
    <h1 class="reg-title"><?php echo Yii::t('news', 'Новости'); ?></h1>
    <div class="block_full_news">
        <div class="news_date"><?php echo Yii::app()->dateFormatter->format('dd MMMM', $model->create_time); ?></div>

        <h1><?php echo CHtml::encode($model->title); ?></h1>

        <p>
            <?php echo $model->content; ?>
        </p>

        <?php $this->widget('LikeWidget', array('url' => Yii::app()->createAbsoluteUrl('/news/default/view', array('id' => $model->id)))); ?>

        <p>
            <?php echo CHtml::link(Yii::t('news', 'Вернуться ко всем новостям'), array('/news/default/index')); ?>
        </p>

        <div class="comments">
            <?php $this->widget('comment.components.widgets.CommentsWidget',array(
                'model' => $model,
            )); ?>
        </div>
    </div>

    <div class="last_news_margin"></div>
    <?php $this->widget('news.components.widgets.LatestNewsWidget'); ?>
</div>